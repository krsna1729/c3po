/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


#include <signal.h>
#include <iostream>

#include "fd.h"

#include "options.h"
#include "csvproc.h"
#include "statsctf.h"
#include "serror.h"

#include "tests.h"

StatsCtf stats("c3poctf");
CSVProcessor processor(stats);



void handler(int signo, siginfo_t *pinfo, void *pcontext)
{
   if ( processor.isRunning() )
   {
      std::cout << "caught signal (" << signo << ") posting quit message" << std::endl;
      processor.quit();
   }
   if ( stats.isRunning() )
   {
      std::cout << "caught signal (" << signo << ") posting quit message" << std::endl;
      stats.quit();
   }

}

void initHandler()
{
   struct sigaction sa;
   sa.sa_flags = SA_SIGINFO;
   sa.sa_sigaction = handler;
   sigemptyset(&sa.sa_mask);
   int signo = SIGINT;
   if (sigaction(signo, &sa, NULL) == -1)
      SError::throwRuntimeExceptionWithErrno( "Unable to register timer handler" );
}

int main(int argc, char **argv)
{
   if ( !Options::parse( argc, argv ) )
   {
      std::cout << "Options::parse() failed" << std::endl;
      return 1;
   }

   /////////////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////
   std::cout << "Options::jsonConfig            : " << Options::jsonConfig() << std::endl;
   std::cout << "Options::dataPrefix            : " << Options::dataPrefix() << std::endl;
   std::cout << "Options::dataPath              : " << Options::dataPath() << std::endl;
   std::cout << "Options::archivePath           : " << Options::archivePath() << std::endl;
   std::cout << "Options::trackingPath          : " << Options::trackingPath() << std::endl;
   std::cout << "Options::currentExtension      : " << Options::currentExtension() << std::endl;
   std::cout << "Options::skipRows              : " << Options::skipRows() << std::endl;
   std::cout << "Options::idleDuration          : " << Options::idleDuration() << std::endl;
   std::cout << "Options::maxACRs               : " << Options::maxACRs() << std::endl;
   std::cout << "Options::acaSaveFrequency      : " << Options::acaSaveFrequency() << std::endl;
   std::cout << "Options::trackingExtension     : " << Options::trackingExtension() << std::endl;
   std::cout << "Options::diameterConfiguration : " << Options::diameterConfiguration() << std::endl;
   std::cout << "Options::memcachedServer       : " << Options::memcachedServer() << std::endl;
   std::cout << "Options::memcachedPort         : " << Options::memcachedPort() << std::endl;
   std::cout << "Options::originHost            : " << Options::originHost() << std::endl;
   std::cout << "Options::originRealm           : " << Options::originRealm() << std::endl;
   std::cout << "Options::statsFrequency        : " << Options::statsFrequency() << std::endl;

   std::cout << "Options::runTests              : " << Options::runTests() << std::endl;

   /////////////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////

   if ( Options::runTests() )
   {
      {
         FDEngine fd;
         fd.setConfigFile("conf/ctf.conf");

         if ( fd.init() )
         {
            rf::Application app(stats);
            fd.advertiseSupport( app.getDict().app(), 0, 1 );
            Tests::fdTest1(app);
            fd.uninit();
         }
      }

      std::cout << "*****************************" << std::endl << "Tests::syslogTest()" << std::endl;
      Tests::syslogTest();
      std::cout << "*****************************" << std::endl << "Tests::dateTimeTest()" << std::endl;
      Tests::dateTimeTest();
      std::cout << "*****************************" << std::endl << "Tests::timerTest()" << std::endl;
      Tests::timerTest();
      std::cout << "*****************************" << std::endl << "Tests::trackTest()" << std::endl;
      Tests::trackTest();
      std::cout << "*****************************" << std::endl << "Tests::splitTest()" << std::endl;
      Tests::splitTest();
      std::cout << "*****************************" << std::endl << "Tests::queueTest()" << std::endl;
      Tests::queueTest();
      std::cout << "*****************************" << std::endl << "Tests::threadest()" << std::endl;
      Tests::threadTest();
      std::cout << "*****************************" << std::endl << "Tests::directoryTest()" << std::endl;
      Tests::directoryTest();
      std::cout << "*****************************" << std::endl << "Tests::pathTest()" << std::endl;
      Tests::pathTest();
      std::cout << "*****************************" << std::endl << "Tests::csvTest()" << std::endl;
      Tests::csvTest();

      return 0;
   }

   /////////////////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////

   // initizlie the signal handler
   initHandler();

   stats.setInterval(Options::statsFrequency());
   //Initialize stats
   stats.init( NULL );

   // initialize and start the processor thread
   processor.init( NULL );

   stats.join();

   // wait for the thread to finish
   processor.join();



   return 0;
}
