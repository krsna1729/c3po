/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <sys/time.h>
#include "statsctf.h"


StatsCtf::StatsCtf(std::string c3poctf) :
   SStats(c3poctf),
   m_nbacr_sent(0),
   m_nbacr_received(0),
   m_handle_timesum(0),
   m_handle_timenb(0)
{}

StatsCtf::~StatsCtf(){

}


void StatsCtf::getConsolidatedPeriodStat(std::map<std::string, std::string>& keyValues){
   std::stringstream sstm;
   sstm << m_nbacr_sent;
   keyValues["nbacr_sent"]  = sstm.str();
   sstm.str(std::string());
   sstm << m_nbacr_received;
   keyValues["nbacr_received"]  = sstm.str();
   sstm.str(std::string());
   sstm << calculateAverageHandleTime();;
   keyValues["msg_handle_avg"] = sstm.str();
   sstm.str(std::string());

   for (std::map<std::string, int>::const_iterator it = m_nbacr_peer.begin(); it != m_nbacr_peer.end(); ++it){
      sstm << it->second;
      keyValues["nbacr_" + it->first] = sstm.str();
      sstm.str(std::string());
   }
}


void StatsCtf::addStat(SEventThreadMessage &msg){
   std::cout << msg.getId() << std::endl;
   switch ( msg.getId() )
   {
      case STAT_ACR_SENT:
         ++m_nbacr_sent;
         break;
      case STAT_ACR_RECEIVED:
         ++m_nbacr_received;
         break;
      case STAT_HANDLE_TIME:
         m_handle_timesum += (double long)((HandleTimeMessage&)msg).getElapsedMilisecs()/1000;
         ++m_handle_timenb;
         break;
      case STAT_ACR_PEER:
         addAcrPeer((ACRPeerMessage&)msg);
         break;
      default:
         break;
   }
}

void StatsCtf::addAcrPeer(ACRPeerMessage& msg){
   std::map<std::string,int>::iterator it = m_nbacr_peer.find(msg.getComposedId());
   if(it != m_nbacr_peer.end()){
      ++(it->second);
   }
   else{
      m_nbacr_peer[msg.getComposedId()] = 1;
   }
}

stime_t StatsCtf::calculateAverageHandleTime(){
   if(m_handle_timenb <= 0){
      return 0;
   }
   return (stime_t)(((m_handle_timesum)/m_handle_timenb)*1000);
}

void StatsCtf::resetStats(){
   m_nbacr_sent      = 0;
   m_nbacr_received  = 0;
   m_handle_timesum  = 0;
   m_handle_timenb   = 0;
   m_nbacr_peer.clear();
}
