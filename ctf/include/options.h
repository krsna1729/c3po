/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef __OPTIONS_H
#define __OPTIONS_H

#include <stdint.h>
#include <string>

class Options
{
public:

   static bool parse( int argc, char **argv );
   static bool parseInputOptions( int argc, char **argv );
   static bool parseJson();
   static bool validateOptions();
   
   static const std::string &jsonConfig() { return m_jsoncfg; }
   static const std::string &dataPrefix() { return m_dataprefix; }
   static const std::string &dataPath() { return m_datapath; }
   static const std::string &archivePath() { return m_archivepath; }
   static const std::string &trackingPath() { return m_trackingpath; }
   static const std::string &currentExtension() { return m_currentextension; }
   static const uint32_t skipRows() { return m_skiprows; }
   static const long idleDuration() { return m_idledur; }
   static const uint32_t maxACRs() { return m_maxacrs; }
   static const int acaSaveFrequency() { return m_acasavefrequency; }
   static const std::string &trackingExtension() { return m_trackingextension; }
   static const std::string &diameterConfiguration() { return m_diameterconfiguration; }
   static const std::string &memcachedServer() { return m_memcachedserver; }
   static const int memcachedPort() { return m_memcachedport; }
   static const std::string &originHost() { return m_originhost; }
   static const std::string &originRealm() { return m_originrealm; }
   static const uint32_t statsFrequency() { return m_statsfrequency; }

   static const bool runTests() { return (options & runtests); }

private:

   enum OptionsSelected {
     jsoncfg               = 0x01,
     dataprefix            = 0x02,
     datapath              = 0x04,
     archivepath           = 0x08,
     trackingpath          = 0x10,
     currentextension      = 0x20,
     trackingextension     = 0x40,
     diameterconfiguration = 0x80,
     memcachedserver       = 0x100,
     memcachedport         = 0x200,
     originhost            = 0x400,
     originrealm           = 0x800,
     skiprows              = 0x1000,
     idledur               = 0x2000,
     maxacrs               = 0x4000,
     acasavefrequency      = 0x8000,
     statsfrequency        = 0x10000,
     runtests              = 0x20000,
   };

   static void help();

   static const int JSONFILEBUFFER;
   static int options;

   static std::string m_jsoncfg;
   static std::string m_dataprefix;
   static std::string m_datapath;
   static std::string m_archivepath;
   static std::string m_trackingpath;
   static std::string m_currentextension;
   static uint32_t m_skiprows;
   static long m_idledur;
   static uint32_t m_maxacrs;
   static int m_acasavefrequency;
   static std::string m_trackingextension;
   static std::string m_diameterconfiguration;
   static std::string m_memcachedserver;
   static int m_memcachedport;
   static std::string m_originhost;
   static std::string m_originrealm;
   static uint32_t m_statsfrequency;
};

#endif // #define __OPTIONS_H
