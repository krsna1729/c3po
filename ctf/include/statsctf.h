/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef __STATSCTF_H
#define __STATSCTF_H

#include <sstream>
#include "sstats.h"
#include "stimer.h"


const uint16_t STAT_ACR_SENT        =  ETM_USER + 2;
const uint16_t STAT_ACR_RECEIVED    =  ETM_USER + 3;
const uint16_t STAT_HANDLE_TIME     =  ETM_USER + 4;
const uint16_t STAT_ACR_PEER        =  ETM_USER + 5;


class HandleTimeMessage : public SEventThreadMessage
{
public:
   HandleTimeMessage( stime_t milisec)
      : SEventThreadMessage( STAT_HANDLE_TIME ),
        m_elapsedmilisec(milisec)
   {
   }

   stime_t getElapsedMilisecs() { return m_elapsedmilisec; }

private:
   HandleTimeMessage();

   stime_t m_elapsedmilisec;
};

class ACRPeerMessage : public SEventThreadMessage
{
public:
   ACRPeerMessage(const std::string &id, uint16_t port)
      : SEventThreadMessage( STAT_ACR_PEER ),
        m_id(id),
        m_port(port)
   {
      std::stringstream sstm;
      sstm << id << ":" << port;
      m_comp_id = sstm.str();
   }

   std::string &getId() { return m_id; }
   uint16_t getPort() { return m_port; }
   std::string& getComposedId(){ return m_comp_id; }

private:
   ACRPeerMessage();

   std::string m_id;
   uint16_t m_port;
   std::string m_comp_id;
};


class StatsCtf : public SStats
{

public:

   StatsCtf(std::string c3poctf);
   ~StatsCtf();



   void getConsolidatedPeriodStat(std::map<std::string, std::string>& keyValues);

   void addStat(SEventThreadMessage &msg);

   void resetStats();

private:

   stime_t calculateAverageHandleTime();
   void addAcrPeer(ACRPeerMessage& msg);

   int m_nbacr_sent;
   int m_nbacr_received;
   double long m_handle_timesum;
   int m_handle_timenb;
   std::map<std::string, int> m_nbacr_peer;

};


#endif // #define __TESTS_H
