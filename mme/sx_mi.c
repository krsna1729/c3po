/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
/********************************************************************20**

     Name:    Diameter SGD Convergence Layer

     Type:    C source file

     Desc:    Upper and Management Interface primitives.

     File:    sx.c

     Sid:

     Prg:     rakesh kumar suman

*********************************************************************21*/


/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"

#include "aqfd.h"
#include "aqfd.x"


#include "lsx.h"
#include "lsx.x"
#include "sx_err.h"
#include "sx.h"
#include "sx.x"

#define SXSXMV 1            /* Diameter T6a - main version */
#define SXSXMR 0            /* Diameter T6a - main revision */
#define SXSXBV 0            /* Diameter T6a - branch version */
#define SXSXBR 0            /* Diameter T6a - branch revision */

#define SXSXPN "1000161"    /* Diameter T6a - part number */

PRIVATE CONSTANT SystemId sId ={
   SXSXMV,                    /* main version */
   SXSXMR,                    /* main revision */
   SXSXBV,                    /* branch version */
   SXSXBR,                    /* branch revision */
   SXSXPN,                    /* part number */
};
PRIVATE S16 sxCfgGen ARGS((SxGenCfg *sxGen));


/* public variable declarations */

PUBLIC SxCb  sxCb;         /* Epc SGD control block */


/* interface function to system service */


/*
*
*       Fun:    sxActvInit
*
*       Desc:   Called from SSI to initialize Diameter.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   sx.c
*
*/
#ifdef SS_MULTIPLE_PROCS
#ifdef ANSI
PUBLIC S16 sxActvInit
(
ProcId procId,         /* procId */
Ent entity,            /* entity */
Inst inst,             /* instance */
Region region,         /* region */
Reason reason,         /* reason */
Void **xxCb           /* Protocol Control Block */
)
#else
PUBLIC S16 sxActvInit(procId,entity, inst, region, reason, xxCb)
ProcId procId;         /* procId */
Ent entity;            /* entity */
Inst inst;             /* instance */
Region region;         /* region */
Reason reason;         /* reason */
Void **xxCb;           /* Protocol Control Block */
#endif
#else /* SS_MULTIPLE_PROCS */
#ifdef ANSI
PUBLIC S16 sxActvInit
(
Ent entity,            /* entity */
Inst inst,             /* instance */
Region region,         /* region */
Reason reason          /* reason */
)
#else
PUBLIC S16 sxActvInit(entity, inst, region, reason)
Ent entity;            /* entity */
Inst inst;             /* instance */
Region region;         /* region */
Reason reason;         /* reason */
#endif
#endif /* SS_MULTIPLE_PROCS */
{
   TRC2(sxActvInit);

#ifdef SS_MULTIPLE_PROCS
   SXDBGP(DBGMASK_SI, (sxCb.init.prntBuf,
          "sxActvInit(ProcId(%d), Ent(%d), Inst(%d), Region(%d), Reason(%d))\n",
           procId, entity, inst, region, reason));
#else /* SS_MULTIPLE_PROCS */
   SXDBGP(DBGMASK_SI, (sxCb.init.prntBuf,
          "sxActvInit(Ent(%d), Inst(%d), Region(%d), Reason(%d))\n",
           entity, inst, region, reason));
   SX_ZERO(&sxCb, sizeof(SxCb));
#endif /* SS_MULTIPLE_PROCS */

   /* initialize sxCb */

   sxCb.init.ent        = entity;
   sxCb.init.inst       = inst;
   sxCb.init.region     = region;
   sxCb.init.reason     = reason;
   sxCb.init.cfgDone    = FALSE;
   sxCb.init.pool       = 0;
#ifdef SS_MULTIPLE_PROCS
   sxCb.init.procId = procId;
#else /* SS_MULTIPLE_PROCS */
   sxCb.init.procId = SFndProcId();
#endif /* SS_MULTIPLE_PROCS */
   sxCb.init.acnt       = FALSE;
   sxCb.init.usta       = TRUE;
   sxCb.init.trc        = FALSE;

#ifdef DEBUGP
   sxCb.init.dbgMask    = 0;
#ifdef SX_DEBUG
   sxCb.init.dbgMask = 0xFFFFFFFF;
#endif
#endif

   /* Used at SX---->UZ */
   sxCb.uzPst.dstProcId = SFndProcId();
   sxCb.uzPst.srcProcId = SFndProcId();
   sxCb.uzPst.dstEnt    = (Ent)ENTUZ;
   sxCb.uzPst.dstInst   = (Inst)0;
   sxCb.uzPst.srcEnt    = (Ent)ENTSX;
   sxCb.uzPst.srcInst   = (Inst)0;
   sxCb.uzPst.prior     = (Prior)VBSM_MSGPRIOR;
   sxCb.uzPst.route     = (Route)RTESPEC;
   sxCb.uzPst.event     = (Event)EVTNONE;
   sxCb.uzPst.region    = (Region)vbSmCb.init.region;
   sxCb.uzPst.pool      = (Pool)vbSmCb.init.region;
   sxCb.uzPst.selector  = (Selector)VBSM_SXSMSEL;

   RETVALUE(ROK);
} /* end of sxActvInit */

/*
*
*       Fun:    SxMiLsxCfgReq
*
*       Desc:   Configure the layer. Responds with a SxMiLsxCfgCfm
*               primitive.
*
*       Ret:    ROK     - ok
*
*       Notes:  
*
*       File:   sx.c
*
*/
#ifdef ANSI
PUBLIC S16 SxMiLsxCfgReq
(
Pst             *pst,           /* post structure */
SxMngmt         *cfg            /* configuration structure */
)
#else
PUBLIC S16 SxMiLsxCfgReq(pst, cfg)
Pst             *pst;           /* post structure */
SxMngmt         *cfg;           /* configuration structure */
#endif
{
   SxMngmt      cfmMsg;
   S16          ret;


   TRC3(SxMiLsxCfgReq);

#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&sxCbPtr)) !=
      ROK)
   {
      SXLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESX004,(ErrVal)0,pst->dstInst,
            "SxMiLsxCfgReq() failed, cannot derive sxCb");
      RETVALUE(FALSE);
   }
   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf,
      "---------EPC DNS------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf,
          "SxMiLsxCfgReq(pst, elmnt(%d))\n",
           cfg->hdr.elmId.elmnt));

   SX_ZERO(&cfmMsg, sizeof (SxMngmt));

   if (!sxCb.init.cfgDone)
      sxCb.init.lmPst.intfVer = pst->intfVer;

   switch (cfg->hdr.elmId.elmnt)
   {
      case STGEN:            /* general configuration */
         ret = sxCfgGen(&cfg->t.cfg.s.sxGen);
         break;

      default:               /* invalid */
         ret = LCM_REASON_INVALID_ELMNT;
         break;
   }

   /* issue configuration confirm */
   sxSendLmCfm(pst, TCFG, &cfg->hdr, (U16)(ret == LCM_REASON_NOT_APPL ?
               LCM_PRIM_OK : LCM_PRIM_NOK), (U16)ret, &cfmMsg);

   RETVALUE(ROK);
} /* SxMiLsxCfgReq() */


/*
*
*       Fun:    SxMiLsx:StsReq
*
*       Desc:   Get statistics information. Statistics are
*               returned by a SxMiLsxStsCfm primitive. The
*               statistics counters can be reset using the action
*               parameter:
*                  ZEROSTS      - zero statistics counters
*                  NOZEROSTS    - do not zero statistics counters
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sx.c
*
*/
#ifdef ANSI
PUBLIC S16 SxMiLsxStsReq
(
Pst             *pst,           /* post structure */
Action          action,         /* action to be done */
SxMngmt         *sts            /* statistics structure */
)
#else
PUBLIC S16 SxMiLsxStsReq(pst, action, sts)
Pst             *pst;           /* post structure */
Action          action;         /* action to be done */
SxMngmt         *sts;           /* statistics structure */
#endif
{
   SxMngmt      cfmMsg;

   TRC3(SxMiLsxStsReq);
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&sxCbPtr)) !=
      ROK)
   {
      SXLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt, ESX005,(ErrVal)0,pst->dstInst,
            "SxMiLsxStsReq() failed, cannot derive sxCb");
      RETVALUE(FALSE);
   }
   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf,
      "---------EPC DNS------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf,
          "SxMiLsxStsReq(pst, action(%d), elmnt(%d))\n",
           action, sts->hdr.elmId.elmnt));
   SXDBGP(DBGMASK_SI, (sxCb.init.prntBuf,
          "SGetDateTime(DateTime(%p))\n", (Ptr)&cfmMsg.t.sts.dt));

   /* initialize the confirm structure */
   SX_ZERO(&cfmMsg, sizeof (SxMngmt));
   SGetDateTime(&cfmMsg.t.sts.dt);

   if(!sxCb.init.cfgDone)
   {
      SXDBGP(DBGMASK_LYR, (sxCb.init.prntBuf,
         "SxMiLsxStsReq(): general configuration not done\n"));
      sxSendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_NOK, LCM_REASON_GENCFG_NOT_DONE,
               &cfmMsg);

      RETVALUE(ROK);
   }

   switch (sts->hdr.elmId.elmnt)
   {
      case STGEN:            /* general statistics */
         sxGetGenSts(&cfmMsg.t.sts.s.genSts);
         if (action == ZEROSTS)
            sxZeroGenSts();
         break;

      default:
         sxSendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_NOK,
                     LCM_REASON_INVALID_ELMNT, &cfmMsg);
         RETVALUE(ROK);
   }

   /* issue a statistics confirm */
   sxSendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_OK, LCM_REASON_NOT_APPL,
               &cfmMsg);

   RETVALUE(ROK);
} /* SxMiLsxStsReq() */


/*
*
*       Fun:    SxMiLsxCntrlReq
*
*       Desc:   Control the specified element: enable or diable
*               trace and alarm (unsolicited status) generation,
*               delete or disable a SAP or a group of SAPs, enable
*               or disable debug printing.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sx.c
*
*/
#ifdef ANSI
PUBLIC S16 SxMiLsxCntrlReq
(
Pst             *pst,           /* post structure */
SxMngmt         *ctl            /* pointer to control structure */
)
#else
PUBLIC S16 SxMiLsxCntrlReq(pst, ctl)
Pst             *pst;           /* post structure */
SxMngmt         *ctl;           /* pointer to control structure */
#endif
{
   Header       *hdr;
   SxMngmt      cfmMsg;
   S16          ret;

   TRC3(SxMiLsxCntrlReq);

/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&sxCbPtr)) !=
      ROK)
   {
      SXLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESX006,(ErrVal)0,pst->dstInst,
            "SxMiLsxCntrlReq() failed, cannot derive sxCb");
      RETVALUE(FALSE);
   }
   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */
   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf,
          "SxMiLsxCntrlReq(pst, elmnt(%d))\n", ctl->hdr.elmId.elmnt));
   SXDBGP(DBGMASK_SI, (sxCb.init.prntBuf,
          "SGetDateTime(DateTime(%p))\n", (Ptr)&ctl->t.cntrl.dt));

   hdr = &ctl->hdr;
   SX_ZERO(&cfmMsg, sizeof (SxMngmt));
   SGetDateTime(&(ctl->t.cntrl.dt));

  /*hi002.105 - Check if Stack is Configured */
   if((!sxCb.init.cfgDone) && (hdr->elmId.elmnt != STGEN))
   {
      SXDBGP(DBGMASK_LYR, (sxCb.init.prntBuf,
         "SxMiLsxCntrlReq(): general configuration not done\n"));
      /* issue a control confirm */
      /* hi003.105 to send cntrl cfm if gen cfg not done */
      sxSendLmCfm(pst, TCNTRL, hdr, LCM_PRIM_NOK, LCM_REASON_GENCFG_NOT_DONE,
               &cfmMsg);

      RETVALUE(ROK);
   }

   switch (hdr->elmId.elmnt)
   {
      case STGEN:
         ret = sxCntrlGen(pst, ctl, hdr);
         break;

      default:
         SXLOGERROR_INT_PAR(ESX007, hdr->elmId.elmnt, 0,
            "SxMiLsxCntrlReq(): bad element in control request");
         ret = LCM_REASON_INVALID_ELMNT;
         break;
   }

   if (ret == LSX_REASON_OPINPROG)
   {
      sxSendLmCfm(pst, TCNTRL, hdr, LCM_PRIM_OK_NDONE,
                  LCM_REASON_NOT_APPL, &cfmMsg);
      RETVALUE(ROK);
   }

   /* issue a control confirm primitive */
   sxSendLmCfm(pst, TCNTRL, hdr, (U16)(ret == LCM_REASON_NOT_APPL ?
               LCM_PRIM_OK : LCM_PRIM_NOK), (U16)ret, &cfmMsg);

   RETVALUE(ROK);
} /* SxMiLsxCntrlReq() */


/*
*
*       Fun:    SxMiLsxStaReq
*
*       Desc:   Get status information. Responds with a
*               SxMiLsxStaCfm primitive.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sx.c
*
*/
#ifdef ANSI
PUBLIC S16 SxMiLsxStaReq
(
Pst             *pst,           /* post structure */
SxMngmt         *sta            /* management structure */
)
#else
PUBLIC S16 SxMiLsxStaReq(pst, sta)
Pst             *pst;           /* post structure */
SxMngmt         *sta;           /* management structure */
#endif
{
   Header       *hdr;
   SxMngmt      cfmMsg;

   TRC3(SxMiLsxStaReq);

#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&sxCbPtr)) !=
      ROK)
   {
      SXLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESX009,(ErrVal)0,pst->dstInst,
            "SxMiLsxStaReq() failed, cannot derive sxCb");
      RETVALUE(FALSE);
   }
   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf,
      "---------EPC DNS------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf, "SxMiLsxStaReq(pst, elmnt(%d))\n",
           sta->hdr.elmId.elmnt));

   hdr = &(sta->hdr);
   SX_ZERO(&cfmMsg, sizeof (SxMngmt));

   /* hi002.105 - fill the date and time */
   SGetDateTime(&cfmMsg.t.ssta.dt);

   switch (hdr->elmId.elmnt)
   {
      case STSID:               /* system ID */
         sxGetSid(&cfmMsg.t.ssta.s.sysId);
         break;

      default:
         SXLOGERROR_INT_PAR(ESX010, hdr->elmId.elmnt, 0,
            "SxMiLsxStaReq(): invalid element in status request");
         sxSendLmCfm(pst, TSSTA, hdr, LCM_PRIM_NOK,
                     LCM_REASON_INVALID_ELMNT, &cfmMsg);
         RETVALUE(ROK);
   }

   /* issue a status confirm primitive */
   sxSendLmCfm(pst, TSSTA, hdr, LCM_PRIM_OK, LCM_REASON_NOT_APPL,
               &cfmMsg);

   RETVALUE(ROK);
} /* SxMiLsxStaReq() */

/*
*
*       Fun:    SxMiLsyOFA
*
*       Desc:   Processes an OFA message received from the AQ layer.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sx.c
*
*/
#ifdef ANSI
PUBLIC S16 SxMiLsxOFA
(
)
#else
PUBLIC S16 SxMiLsxOFA()
#endif
{
   S16 ret;
   VbMmeMoData *modata = NULL;
   VbMmeUeCb *ueCb = NULL;

   TRC3(SxMiLsxOFA);

   /* pop modata */
   modata = (VbMmeMoData*)aqQueuePop(&aqCb.ofaQueue);
   if (!modata) /* nothing to process */
      return ROK;

   /* lookup ueCb */
   ret = vbMmeUtlFndUeCbOnImsi(modata->imsi.val, modata->imsi.len, &ueCb);

   if (ret == ROK) /* found the ueCb */
   {
      SX_DBG_INFO((sxCb.init.prntBuf, "ueCb found processing OFA"));

      /* send the CPDATA to the UE */
      ret = vbMmeSndCpData(ueCb, &modata->cp, &modata->resp );
   }
   else
   {
      SX_DBG_INFO((sxCb.init.prntBuf, "ueCb not found for imsi [%s]", modata->imsi.val));
   }

  vbMmeUtlFreeMoData(modata);

   RETVALUE(ret);
} /* SxMiLsxOFA() */

/*
*
*       Fun:    SxMiLsyTFR
*
*       Desc:   Processes an TFR message received from the AQ layer.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sx.c
*
*/
#ifdef ANSI
PUBLIC S16 SxMiLsxTFR
(
)
#else
PUBLIC S16 SxMiLsxTFR()
#endif
{
   S16 ret;
   VbMmeMtData *mtdata = NULL;
   VbMmeUeCb *ueCb = NULL;

   TRC3(SxMiLsxTFR);

   /* pop modata */
   mtdata = (VbMmeMtData*)aqQueuePop(&aqCb.tfrQueue);
   if (!mtdata) /* nothing to process */
      return ROK;

   /* lookup ueCb */
   ret = vbMmeUtlFndUeCbOnImsi(mtdata->imsi.val, mtdata->imsi.len, &ueCb);

   if (ret == ROK) /* found the ueCb */
   {
      SX_DBG_INFO((sxCb.init.prntBuf, "ueCb found processing TRA"));

      if (vbMmeGetNextTioAndMsgRef(ueCb, mtdata) <= MAX_TIO_VALUE)
      {
         /* send the CPDATA/RPDATA to the UE */
         ret = vbMmeSndCpData(ueCb, &mtdata->cp, &mtdata->rqst );
      }
   }
   else
   {
      SX_DBG_INFO((sxCb.init.prntBuf, "ueCb not found for imsi [%*s]", mtdata->imsi.len, mtdata->imsi.val));
      aqfdSendTFA(mtdata->tfr, 0, 5001 /* DIAMETER_ERROR_USER_UNKNOWN */, 0, "", 0);
      vbMmeUtlFreeMtData(ueCb, mtdata);
   }

   RETVALUE(ret);
} /* SxMiLsxTFR() */

/*
*
*       Fun:    sxCfgGen
*
*       Desc:   Perform general configuration of Diameter. Must be done
*               after sxActvInit() is called, but before any other
*               interaction with Diameter. Reserves static memory for
*               the layer with SGetSMem(), allocates required
*               memory, sets up data structures and starts the
*               receive mechanism (threads/timer/permanent task).
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*
*       Notes:  None
*
*       File:   sx.c
*
*/
#ifdef ANSI
PUBLIC S16 sxCfgGen
(
SxGenCfg        *sxGen          /* management structure */
)
#else
PUBLIC S16 sxCfgGen(sxGen)
SxGenCfg        *sxGen;         /* management structure */
#endif
{
   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    sxSendLmCfm
*
*       Desc:   Sends configuration, control, statistics and status
*               confirms to the layer manager.
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   sx.c
*
*/
#ifdef ANSI
PUBLIC Void sxSendLmCfm
(
Pst             *pst,           /* post */
U8              cfmType,        /* confirm type */
Header          *hdr,           /* header */
U16             status,         /* confirm status */
U16             reason,         /* failure reason */
SxMngmt         *cfm            /* management structure */
)
#else
PUBLIC Void sxSendLmCfm(pst, cfmType, hdr, status, reason, cfm)
Pst             *pst;           /* post */
U8              cfmType;        /* confirm type */
Header          *hdr;           /* header */
U16             status;         /* confirm status */
U16             reason;         /* failure reason */
SxMngmt         *cfm;           /* management structure */
#endif
{
   Pst          cfmPst;         /* post structure for confimation */

   TRC2(sxSendLmCfm);

   SX_ZERO(&cfmPst, sizeof (Pst));

   cfm->hdr.elmId.elmnt = hdr->elmId.elmnt;
   cfm->hdr.transId     = hdr->transId;

   cfm->cfm.status = status;
   cfm->cfm.reason = reason;

   /* fill up post for confirm */
   cfmPst.srcEnt        = sxCb.init.ent;
   cfmPst.srcInst       = sxCb.init.inst;
   cfmPst.srcProcId     = sxCb.init.procId;
   cfmPst.dstEnt        = pst->srcEnt;
   cfmPst.dstInst       = pst->srcInst;
   cfmPst.dstProcId     = pst->srcProcId;
   cfmPst.selector      = hdr->response.selector;
   cfmPst.prior         = hdr->response.prior;
   cfmPst.route         = hdr->response.route;
   cfmPst.region        = hdr->response.mem.region;
   cfmPst.pool          = hdr->response.mem.pool;

   switch (cfmType)
   {
      case TCFG:
         SxMiLsxCfgCfm(&cfmPst, cfm);
         break;

      case TSTS:
         SxMiLsxStsCfm(&cfmPst, cfm);
         break;

      case TCNTRL:
         SxMiLsxCntrlCfm(&cfmPst, cfm);
         break;

      case TSSTA:
         SxMiLsxStaCfm(&cfmPst, cfm);
         break;

      default:
         SXDBGP(DBGMASK_LYR, (sxCb.init.prntBuf,
                  "sxSendLmCfm(): unknown parameter cfmType\n"));
         break;
   }

   RETVOID;
} /* sxSendLmCfm() */

/*
*
*       Fun:    sxGetGenSts
*
*       Desc:   Get general statistics.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  Statistics counters are sampled unlocked.
*
*       File:   sx.c
*
*/
#ifdef ANSI
PUBLIC S16 sxGetGenSts
(
SxGenSts        *genSts         /* statistics returned */
)
#else
PUBLIC S16 sxGetGenSts(genSts)
SxGenSts        *genSts;        /* statistics returned */
#endif
{
   TRC2(sxGetGenSts);

   SX_ZERO(genSts, sizeof (SxGenSts));

   /* get receive counters from each group */
   genSts->numErrors = 0;

   RETVALUE(ROK);
} /* sxGetGenSts() */


/*
*
*       Fun:    sxZeroGenSts
*
*       Desc:   Reset general statistics.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   sx.c
*
*/
#ifdef ANSI
PUBLIC S16 sxZeroGenSts
(
Void
)
#else
PUBLIC S16 sxZeroGenSts()
#endif
{
   TRC2(sxZeroGenSts);

   /* zero the error stats */
   SX_ZERO_ERRSTS();

   RETVALUE(ROK);
} /* sxZeroGenSts() */

/*
*
*       Fun:    sxCntrlGen
*
*       Desc:   General control requests are used to enable/disable
*               alarms or debug printing and also to shut down
*               Diameter.
*
*       Ret:    LCM_REASON_NOT_APPL             - ok
*               LSX_REASON_OPINPROG             - ok, in progress
*               LCM_REASON_INVALID_ACTION       - failure
*               LCM_REASON_INVALID_SUBACTION    - failure
*               LSX_REASON_DIFF_OPINPROG        - failure
*
*       Notes:  None
*
*       File:   sx.c
*
*/
#ifdef ANSI
PUBLIC S16 sxCntrlGen
(
Pst             *pst,           /* post structure */
SxMngmt         *cntrl,         /* control request */
Header          *hdr            /* header */
)
#else
PUBLIC S16 sxCntrlGen(pst, cntrl, hdr)
Pst             *pst;           /* post structure */
SxMngmt         *cntrl;         /* control request */
Header          *hdr;           /* header */
#endif
{
   S16          ret;
   U8           action, subAction;
   Bool         invSubAction = FALSE;

   TRC2(sxCntrlGen);

   action = cntrl->t.cntrl.action;
   subAction = cntrl->t.cntrl.subAction;

   switch (action)
   {
      case ASHUTDOWN:
         ret = ROK;
         RETVALUE(LCM_REASON_NOT_APPL);


      case AENA:
         if (subAction == SAUSTA)
            sxCb.init.usta = TRUE;
#ifdef DEBUGP
         else if (subAction == SADBG)
            sxCb.init.dbgMask |= cntrl->t.cntrl.ctlType.sxDbg.dbgMask;
#endif
         else
            invSubAction = TRUE;
         break;


      case ADISIMM:
         if (subAction == SAUSTA)
            sxCb.init.usta = FALSE;
#ifdef DEBUGP
         else if (subAction == SADBG)
            sxCb.init.dbgMask &= ~(cntrl->t.cntrl.ctlType.sxDbg.dbgMask);
#endif
         else
            invSubAction = TRUE;
         break;


      default:
         SXLOGERROR_INT_PAR(ESX011, (ErrVal)action, 0,
               "sxCntrlGen(): Unknown or unsupported action");
         RETVALUE(LCM_REASON_INVALID_ACTION);
   }

   if (invSubAction)
   {
      SXLOGERROR_INT_PAR(ESX012, (ErrVal)subAction, 0,
            "sxCntrlGen(): invalid sub-action specified");
      RETVALUE(LCM_REASON_INVALID_SUBACTION);
   }

   RETVALUE(LCM_REASON_NOT_APPL);
} /* sxCntrlGen() */

/*
*
*       Fun:   sxGetSid
*
*       Desc:  Get system id consisting of part number, main version and
*              revision and branch version and branch.
*
*       Ret:   TRUE      - ok
*
*       Notes: None
*
*       File:  sx.c
*
*/
#ifdef ANSI
PUBLIC S16 sxGetSid
(
SystemId *s                 /* system id */
)
#else
PUBLIC S16 sxGetSid(s)
SystemId *s;                /* system id */
#endif
{
   TRC2(sxGetSid)

   s->mVer = sId.mVer;
   s->mRev = sId.mRev;
   s->bVer = sId.bVer;
   s->bRev = sId.bRev;
   s->ptNmb = sId.ptNmb;

   RETVALUE(TRUE);

} /* sxGetSid */

/*
	*
	*       Fun:    sxSendAlarm
*
*       Desc:   Send an unsolicited status indication (alarm) to
*               layer manager.
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   sx.c
*
*/
#ifdef ANSI
PUBLIC Void sxSendAlarm
(
U16             cgy,            /* alarm category */
U16             evnt,           /* event */
U16             cause,          /* cause for alarm */
SxAlarmInfo     *info           /* alarm information */
)
#else
PUBLIC Void sxSendAlarm(cgy, evnt, cause, info)
U16             cgy;            /* alarm category */
U16             evnt;           /* event */
U16             cause;          /* cause for alarm */
SxAlarmInfo     *info;          /* alarm information */
#endif
{
   SxMngmt      sm;             /* Management structure */

   TRC2(sxSendAlarm);

   /* do nothing if unconfigured */
   if (!sxCb.init.cfgDone)
   {
      SXDBGP(DBGMASK_LYR, (sxCb.init.prntBuf,
               "sxSendAlarm(): general configuration not done\n"));
      RETVOID;
   }

   /* send alarms only if configured to do so */
   if (sxCb.init.usta)
   {
      SX_ZERO(&sm, sizeof (SxMngmt));

      sm.hdr.elmId.elmnt        = TUSTA;
      sm.hdr.elmId.elmntInst1   = SX_UNUSED;
      sm.hdr.elmId.elmntInst2   = SX_UNUSED;
      sm.hdr.elmId.elmntInst3   = SX_UNUSED;

      sm.t.usta.alarm.category  = cgy;
      sm.t.usta.alarm.event     = evnt;
      sm.t.usta.alarm.cause     = cause;

      cmMemcpy((U8 *)&sm.t.usta.info,
               (U8 *)info, sizeof (SxAlarmInfo));

      (Void)SGetDateTime(&sm.t.usta.alarm.dt);

#ifdef SX_MULTI_THREADED
   SX_LOCK(&sxCb.lmPstLock);
#endif
      SxMiLsxStaInd(&(sxCb.init.lmPst), &sm);
#ifdef SX_MULTI_THREADED
   SX_UNLOCK(&sxCb.lmPstLock);
#endif
   }

   RETVOID;
} /* sxSendAlarm() */

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      rks   1. initial release.
*********************************************************************91*/
