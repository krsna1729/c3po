/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter convergence Layer

     Type:     C include file

     Desc:     Defines and macros used by Diameter.

     File:     aq.h

     Sid:

     Prg:      bw

*********************************************************************21*/


#ifndef __AQH__
#define __AQH__

#define AQLAYERNAME     "DIAMETER_CONVERGENCE_LAYER"

#ifdef AQ_MULTI_THREADED
#define AQ_STSK_PRI             13
#endif

#define EVTINTGOACTV            42


/* conCb.state: connection states */
#define HI_ST_SRV_LISTEN        0x0     /* initial TCP server state */
#define HI_ST_AW_CON_RSP        0x1     /* awaiting ConRsp */
#define HI_ST_CLT_CONNECTING    0x2     /* client connecting */
#define HI_ST_CONNECTED         0x3     /* data transfer state */
#define HI_ST_CONNECTED_NORD    0x4     /* after closing read side */
#define HI_ST_CONNECTED_NOWR    0x5     /* after closing write side */
#define HI_ST_CONNECTED_NORDWR  0x6     /* after closing both sides */
#define HI_ST_DISCONNECTING     0x7     /* disconnect request sent */
#define HI_ST_CONNECTING        0x8     /* data transfer state */


/* miscellaneous defines */
#define AQ_PRNTBUF_SIZE         128     /* size of print buffer */
#define AQ_UNUSED               -1      /* for all unused fields */
#define AQ_SRVCTYPE_MASK        4

/* Macros.
 */

/* DEBUGP */
#define AQDBGP(_msgClass, _arg)                                 \
   DBGP(&(aqCb.init), AQLAYERNAME, _msgClass, _arg)

/* zero out a buffer */
#define AQ_ZERO(_str, _len)                                     \
   cmMemset((U8 *)_str, 0, _len);

/* allocate and zero out a static buffer */
#define AQ_ALLOC(_size, _datPtr)                                \
{                                                               \
   S16 _ret;                                                    \
   _ret = SGetSBuf(aqCb.init.region, aqCb.init.pool,            \
                   (Data **)&_datPtr, _size);                   \
   if (_ret == ROK)                                             \
      cmMemset((U8*)_datPtr, 0, _size);                         \
   else                                                         \
      _datPtr = NULLP;                                          \
}

/* free a static buffer */
#define AQ_FREE(_size, _datPtr)                                 \
   SPutSBuf(aqCb.init.region, aqCb.init.pool,                   \
            (Data *)_datPtr, _size);

/* statistics */
#if (defined(AQ_MULTI_THREADED)  &&  defined(AQ_STS_LOCKS))

#define AQ_INC_ERRSTS(_stat)                                    \
{                                                               \
   AQ_LOCK(&aqCb.errStsLock);                                   \
   _stat++;                                                     \
   AQ_UNLOCK(&aqCb.errStsLock);                                 \
}

#define AQ_INC_TXSTS(_sap, _stat)                               \
{                                                               \
   //AQ_LOCK(&_sap->txStsLock);                                   \
   //_stat++;                                                     \
   //AQ_UNLOCK(&_sap->txStsLock);                                 \
}

/*hi028.201: Handled statistics properly*/
#define AQ_DEC_TXSTS(_sap, _stat)                               \
{                                                               \
   //AQ_LOCK(&_sap->txStsLock);                                   \
   //_stat--;                                                     \
   //AQ_UNLOCK(&_sap->txStsLock);                                 \
}

#endif

#ifdef HI_MULTI_THREADED

/* acquire a mutex */
#define AQ_LOCK(_ptr_lock)                                      \
{                                                               \
   if (SLock(_ptr_lock) != ROK)                                 \
   {                                                            \
      AqAlarmInfo _alInfo;                                      \
      _alInfo.type = LAQ_ALARMINFO_TYPE_NTPRSNT;                \
      aqSendAlarm(LCM_CATEGORY_INTERNAL, LCM_EVENT_INV_EVT,     \
                  LAQ_CAUSE_LOCK_ERR, &_alInfo);                \
      AQDBGP(DBGMASK_LYR, (aqCb.init.prntBuf, "SLock() failed\n")); \
   }                                                            \
}

/* release a mutex */
#define AQ_UNLOCK(_ptr_lock)                                    \
{                                                               \
   if (SUnlock(_ptr_lock) != ROK)                               \
   {                                                            \
      AqAlarmInfo _alInfo;                                      \
      _alInfo.type = LAQ_ALARMINFO_TYPE_NTPRSNT;                \
      aqSendAlarm(LCM_CATEGORY_INTERNAL, LCM_EVENT_INV_EVT,     \
                  LAQ_CAUSE_UNLOCK_ERR, &_alInfo);              \
      AQDBGP(DBGMASK_LYR, (aqCb.init.prntBuf, "SUnlock() failed\n")); \
   }                                                            \
}

#endif // #ifdef HI_MULTI_THREADED

/* statistics */
#if (defined(AQ_MULTI_THREADED)  &&  defined(AQ_STS_LOCKS))

#define AQ_INC_ERRSTS(_stat)                                    \
{                                                               \
   AQ_LOCK(&aqCb.errStsLock);                                   \
   _stat++;                                                     \
   AQ_UNLOCK(&aqCb.errStsLock);                                 \
}

#define HI_INC_TXSTS(_stat)                                     \
{                                                               \
   AQ_LOCK(&aqCb.txStsLock);                                    \
   _stat++;                                                     \
   AQ_UNLOCK(&aqCb.txStsLock);                                  \
}

/*hi028.201: Handled statistics properly*/
#define HI_DEC_TXSTS(__stat)                                    \
{                                                               \
   AQ_LOCK(&aqCb.txStsLock);                                   \
   _stat--;                                                     \
   AQ_UNLOCK(&aqCb.txStsLock);                                 \
}

#define AQ_ADD_TXSTS(_stat, _val)                         \
{                                                               \
   AQ_LOCK(&aqCb.txStsLock);                                   \
   _stat += _val;                                               \
   HI_UNLOCK(&aqCb.txStsLock);                                 \
}

#if 0
#define AQ_INC_TXSTSMSG(_sap, _conCb)                           \
{                                                               \
   HI_LOCK(&_sap->txStsLock);                                   \
   if ((_conCb->srvcType & 0x0F) == HI_SRVC_TLS)                \
      _sap->txSts.numTxTlsMsg++;                                \
   else if (_conCb->flag & HI_FL_TCP)                           \
      _sap->txSts.numTxTcpMsg++;                                \
   else if (_conCb->flag & HI_FL_UDP)                           \
      _sap->txSts.numTxUdpMsg++;                                \
   else                                                         \
      _sap->txSts.numTxRawMsg++;                                \
   HI_UNLOCK(&_sap->txStsLock);                                 \
}
#endif

#define AQ_ZERO_ERRSTS()                                        \
{                                                               \
   AQ_LOCK(&aqCb.errStsLock);                                   \
   AQ_ZERO(&aqCb.errSts, sizeof(AqErrSts));                     \
   AQ_UNLOCK(&aqCb.errStsLock);                                 \
}

#define HI_ZERO_TXSTS()                                         \
{                                                               \
   AQ_LOCK(&aqCb.txStsLock);                                    \
   aqCb.txSts.numTxMsg = 0;                                     \
   AQ_UNLOCK(&aqCb.txStsLock);                                  \
}

#else  /* AQ_MULTI_THREADED  &&  AQ_STS_LOCKS */


#define AQ_INC_ERRSTS(_stat)                                    \
   _stat++;

#define AQ_INC_TXSTS(_stat)                                     \
   _stat++;

#define AQ_DEC_TXSTS(_stat)                                     \
   _stat--;

#define AQ_ADD_TXSTS(_stat, _val)                               \
   _stat += _val;

#if 0
#define HI_INC_TXSTSMSG(_sap, _conCb)                           \
{                                                               \
   if ((_conCb->srvcType & 0x0F) == HI_SRVC_TLS)                \
      _sap->txSts.numTxTlsMsg++;                                \
   else if (_conCb->flag & HI_FL_TCP)                           \
      _sap->txSts.numTxTcpMsg++;                                \
   else if (_conCb->flag & HI_FL_UDP)                           \
      _sap->txSts.numTxUdpMsg++;                                \
   else                                                         \
      _sap->txSts.numTxRawMsg++;                                \
}
#endif

#define AQ_ZERO_ERRSTS()                                        \
   AQ_ZERO(&aqCb.errSts, sizeof(AqErrSts));

#define AQ_ZERO_TXSTS()                                         \
   aqCb.txSts.numTxMsg = 0;

//#endif

#endif /* AQ_MULTI_THREADED  &&  AQ_STS_LOCKS */


#endif /* __AQH__ */


/********************************************************************30**

         End of file:

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

