/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Stack Manager - portable Diameter T6A

     Type:     C source file

     Desc:     Code for T6A Convergence Layer  layer management
               primitives.

     File:     smswptmi.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/*

Layer management provides the necessary functions to control and
monitor the condition of each protocol layer.

The following functions are provided in this file:

     SmMiLswCfgReq      Configure Request
     SmMiLswStaReq      Status Request
     SmMiLswStsReq      Statistics Request
     SmMiLswCntrlReq    Control Request

It is assumed that the following functions are provided in the
stack management body files:

     SmMiLswStaInd      Status Indication
     SmMiLswStaCfm      Status Confirm
     SmMiLswStsCfm      Statistics Confirm
     SmMiLswTrcInd      Trace Indication

*/


/* header include files (.h) */

#include "envopt.h"        /* environment options */
#include "envdep.h"        /* environment dependent */
#include "envind.h"        /* environment independent */
#include "gen.h"           /* general layer */
#include "cm5.h"           /* common timer */
#include "ssi.h"           /* system services */
#include "cm_inet.h"       /* common sockets */
#include "lsw.h"           /* SW layer management */
#include "cm_err.h"        /* common error */
#include "smsw_err.h"      /* SW error */

/* header/extern include files (.x) */

#include "gen.x"           /* general layer */
#include "ssi.x"           /* system services */
#include "cm5.x"           /* common timer */
#include "cm_inet.x"       /* common sockets */
#include "lsw.x"           /* sw layer management */

#define  MAXSWMI  2

#ifndef  LCSMSWMILSW
#define  PTSMSWMILSW
#else
#ifndef   SW
#define  PTSMSWMILSW
#endif
#endif

#ifdef PTSMSWMILSW
PRIVATE S16 PtMiLswCfgReq    ARGS((Pst *pst, SwMngmt *cfg));
PRIVATE S16 PtMiLswStsReq    ARGS((Pst *pst, Action action, SwMngmt *sts));
PRIVATE S16 PtMiLswCntrlReq  ARGS((Pst *pst, SwMngmt *cntrl));
PRIVATE S16 PtMiLswStaReq    ARGS((Pst *pst, SwMngmt *sta));
#endif


/*
the following matrices define the mapping between the primitives
called by the layer management interface of TCP UDP Convergence Layer
and the corresponding primitives in TUCL

The parameter MAXSWMI defines the maximum number of layer manager
entities on top of TUCL . There is an array of functions per primitive
invoked by TCP UDP Conbvergence Layer. Every array is MAXSWMI long
(i.e. there are as many functions as the number of service users).

The dispatching is performed by the configurable variable: selector.
The selector is configured during general configuration.

The selectors are:

   0 - loosely coupled (#define LCSMSWMILSW) 2 - Lsw (#define SW)

*/


/* Configuration request primitive */

PRIVATE LswCfgReq SmMiLswCfgReqMt[MAXSWMI] =
{
#ifdef LCSMSWMILSW
   cmPkLswCfgReq,          /* 0 - loosely coupled  */
#else
   PtMiLswCfgReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef SW
   SwMiLswCfgReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLswCfgReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Control request primitive */

PRIVATE LswCntrlReq SmMiLswCntrlReqMt[MAXSWMI] =
{
#ifdef LCSMSWMILSW
   cmPkLswCntrlReq,          /* 0 - loosely coupled  */
#else
   PtMiLswCntrlReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef SW
   SwMiLswCntrlReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLswCntrlReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Statistics request primitive */

PRIVATE LswStsReq SmMiLswStsReqMt[MAXSWMI] =
{
#ifdef LCSMSWMILSW
   cmPkLswStsReq,          /* 0 - loosely coupled  */
#else
   PtMiLswStsReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef SW
   SwMiLswStsReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLswStsReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Status request primitive */

PRIVATE LswStaReq SmMiLswStaReqMt[MAXSWMI] =
{
#ifdef LCSMSWMILSW
   cmPkLswStaReq,          /* 0 - loosely coupled  */
#else
   PtMiLswStaReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef SW
   SwMiLswStaReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLswStaReq,          /* 1 - tightly coupled, portable */
#endif
};


/*
*     layer management interface functions
*/

/*
*
*       Fun:   Configuration request
*
*       Desc:  This function is used to configure  TUCL
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smswptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLswCfgReq
(
Pst *spst,                /* post structure */
SwMngmt *cfg              /* configure */
)
#else
PUBLIC S16 SmMiLswCfgReq(spst, cfg)
Pst *spst;                /* post structure */
SwMngmt *cfg;             /* configure */
#endif
{
   TRC3(SmMiLswCfgReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLswCfgReqMt[spst->selector])(spst, cfg);
   RETVALUE(ROK);
} /* end of SmMiLswCfgReq */



/*
*
*       Fun:   Statistics request
*
*       Desc:  This function is used to request statistics from
*              TUCL
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smswptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLswStsReq
(
Pst *spst,                /* post structure */
Action action,
SwMngmt *sts              /* statistics */
)
#else
PUBLIC S16 SmMiLswStsReq(spst, action, sts)
Pst *spst;                /* post structure */
Action action;
SwMngmt *sts;             /* statistics */
#endif
{
   TRC3(SmMiLswStsReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLswStsReqMt[spst->selector])(spst, action, sts);
   RETVALUE(ROK);
} /* end of SmMiLswStsReq */


/*
*
*       Fun:   Control request
*
*       Desc:  This function is used to send control request to
*              TUCL
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smswptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLswCntrlReq
(
Pst *spst,                 /* post structure */
SwMngmt *cntrl            /* control */
)
#else
PUBLIC S16 SmMiLswCntrlReq(spst, cntrl)
Pst *spst;                 /* post structure */
SwMngmt *cntrl;           /* control */
#endif
{
   TRC3(SmMiLswCntrlReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLswCntrlReqMt[spst->selector])(spst, cntrl);
   RETVALUE(ROK);
} /* end of SmMiLswCntrlReq */


/*
*
*       Fun:   Status request
*
*       Desc:  This function is used to send a status request to
*              TUCL
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smswptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLswStaReq
(
Pst *spst,                /* post structure */
SwMngmt *sta              /* status */
)
#else
PUBLIC S16 SmMiLswStaReq(spst, sta)
Pst *spst;                /* post structure */
SwMngmt *sta;             /* status */
#endif
{
   TRC3(SmMiLswStaReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLswStaReqMt[spst->selector])(spst, sta);
   RETVALUE(ROK);
} /* end of SmMiLswStaReq */

#ifdef PTSMSWMILSW

/*
 *             Portable Functions
 */

/*
*
*       Fun:   Portable configure Request- TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smswptmi.c
*
*/

#ifdef ANSI
PRIVATE S16 PtMiLswCfgReq
(
Pst *spst,                  /* post structure */
SwMngmt *cfg                /* configure */
)
#else
PRIVATE S16 PtMiLswCfgReq(spst, cfg)
Pst *spst;                  /* post structure */
SwMngmt *cfg;               /* configure */
#endif
{
  TRC3(PtMiLswCfgReq)

  UNUSED(spst);
  UNUSED(cfg);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMSWLOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
              ERRCLS_DEBUG, ESMSW002, 0, "PtMiLswCfgReq () Failed");
#endif

  RETVALUE(ROK);
} /* end of PtMiLswCfgReq */



/*
*
*       Fun:   Portable statistics Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smswptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLswStsReq
(
Pst *spst,                  /* post structure */
Action action,
SwMngmt *sts                /* statistics */
)
#else
PRIVATE S16 PtMiLswStsReq(spst, action, sts)
Pst *spst;                  /* post structure */
Action action;
SwMngmt *sts;               /* statistics */
#endif
{
  TRC3(PtMiLswStsReq)

  UNUSED(spst);
  UNUSED(action);
  UNUSED(sts);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMSWLOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
                ERRCLS_DEBUG, ESMSW003, 0, "PtMiLswStsReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLswStsReq */


/*
*
*       Fun:   Portable control Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smswptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLswCntrlReq
(
Pst *spst,                  /* post structure */
SwMngmt *cntrl              /* control */
)
#else
PRIVATE S16 PtMiLswCntrlReq(spst, cntrl)
Pst *spst;                  /* post structure */
SwMngmt *cntrl;             /* control */
#endif
{
  TRC3(PtMiLswCntrlReq)

  UNUSED(spst);
  UNUSED(cntrl);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMSWLOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
             ERRCLS_DEBUG, ESMSW004, 0, "PtMiLswCntrlReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLswCntrlReq */


/*
*
*       Fun:   Portable status Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smswptmi.c
*
*/

#ifdef ANSI
PRIVATE S16 PtMiLswStaReq
(
Pst *spst,                  /* post structure */
SwMngmt *sta                /* status */
)
#else
PRIVATE S16 PtMiLswStaReq(spst, sta)
Pst *spst;                  /* post structure */
SwMngmt *sta;               /* status */
#endif
{
  TRC3(PtMiLswStaReq);

  UNUSED(spst);
  UNUSED(sta);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMSWLOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
                ERRCLS_DEBUG,ESMSW005, 0, "PtMiLswStaReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLswStaReq */

#endif /* PTSMSWMILSW */


/********************************************************************30**

         End of file:     smswptmi.c@@/main/5 - Mon Mar  3 20:09:32 2008

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      asa  1. initial release.
*********************************************************************91*/
