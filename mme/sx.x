/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

/********************************************************************20**

     Name:     Diameter T6A Interface Convergence Layer

     Type:     C include file

     Desc:     Structures, variables, types and prototypes.

     File:     sx.x

     Sid:

     Prg:      rakesh kumar suman

*********************************************************************21*/



#ifndef __SXX__
#define __SXX__

#ifdef __cplusplus
extern "C" {
#endif

/* Reception statistics.
 */
typedef struct sxRxSts
{
   StsCntr      numRxMsg;
} SxRxSts;


/* Transmission statistics.
 */
typedef struct sxTxSts
{
   StsCntr      numTxMsg;
} SxTxSts;


/* Error statistics.
 */
typedef struct sxErrSts
{
   StsCntr      t6aErr;
} SxErrSts;

/* Layer manager control requests can be pending on a SAP when
 * communication with the SX threads are required before a confirm
 * can be sent.
 */
typedef struct sxPendOp
{
   Bool         flag;           /* operation pending? */
   Header       hdr;            /* control request header */
   U8           action;         /*    "       "    action */
   Elmnt        elmnt;          /* STTSAP or STGRTSAP */
   U16          numRem;         /* for ASHUTDOWN/STGRTSAP requests */
   Pst          lmPst;          /* for ASHUTDOWN response */
} SxPendOp;

#ifdef SX_MULTI_THREADED

/* Upper interface primitives can be invoked by multiple threads.
 * Each primitive must therefore have its own Pst.
 */
typedef struct sxUiPsts
{
   Pst          uiConCfmPst;
   Pst          uiConIndPst;
   Pst          uiFlcIndPst;
   Pst          uiDatIndPst;
   Pst          uiUDatIndPst;
   Pst          uiDiscIndPst;
   Pst          uiDiscCfmPst;

} SxUiPsts;

#endif /* SX_MULTI_THREADED */

/* SX layer control block.
 */
typedef struct _sxCb
{
   TskInit      init;           /* task initialization structure */

   SxGenCfg     cfg;            /* general configuration */
   SxErrSts     errSts;         /* general error statistics */
   SxRxSts      rxSts;
   SxTxSts      txSts;

   SxPendOp     pendOp;         /* control request pending */

   Pst          uzPst;
} SxCb;

EXTERN SxCb sxCb;

/* Message to a group thread.
 */
typedef struct sxThrMsg
{
   S16          type;           /* message type */
   SpId         spId;           /* for the relevant SAP */
   UConnId      spConId;        /* for the relevant connection */
   union
   {
      Reason    reason;         /* for indications */
      Action    action;         /* for confirms */
   } disc;
} SxThrMsg;

/* management related */
EXTERN Void sxSendLmCfm         ARGS((Pst *pst, U8 cfmType,
                                      Header *hdr, U16 status,
                                      U16 reason, SxMngmt *cfm));
EXTERN S16  sxGetGenSts         ARGS((SxGenSts *genSts));
EXTERN S16  sxZeroGenSts        ARGS((Void));
EXTERN S16  sxCntrlGen          ARGS((Pst *pst, SxMngmt *cntrl, Header *hdr));
EXTERN Void sxSendAlarm         ARGS((U16 cgy, U16 evnt, U16 cause,
                                      SxAlarmInfo *info));

EXTERN S16  sxGetSid            ARGS((SystemId *sid));

EXTERN S16 SxMiLsxOFA           ARGS((Void));
EXTERN S16 SxMiLsxTFR           ARGS((Void));

/******************************************************************************/
/******************************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* __SXX__ */

/********************************************************************30**

         End of file:     

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      rks   1. initial release.
*********************************************************************91*/

