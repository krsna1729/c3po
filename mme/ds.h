/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     S6A convergence Layer

     Type:     C include file

     Desc:     Defines and macros used by Diameter.

     File:     ds.h

     Sid:

     Prg:      bw

*********************************************************************21*/


#ifndef __DSH__
#define __DSH__

#define DSLAYERNAME     "S6A_CONVERGENCE_LAYER"

#define EVTINTGOACTV            42


/* miscellaneous defines */
#define DS_PRNTBUF_SIZE         128     /* size of print buffer */
#define DS_UNUSED               -1      /* for all unused fields */
#define DS_SRVCTYPE_MASK        4

/* Macros.
 */

/* DEBUGP */
#define DSDBGP(_msgClass, _arg)                                 \
   DBGP(&(dsCb.init), DSLAYERNAME, _msgClass, _arg)

#define DS_DBG_INFO(_arg)                                       \
   DBGP(&(dsCb.init), DSLAYERNAME, LDS_DBGMASK_INFO, _arg)

/* zero out a buffer */
#define DS_ZERO(_str, _len)                                     \
   cmMemset((U8 *)_str, 0, _len);

/* allocate and zero out a static buffer */
#define DS_ALLOC(_size, _datPtr)                                \
{                                                               \
   S16 _ret;                                                    \
   _ret = SGetSBuf(dsCb.init.region, dsCb.init.pool,            \
                   (Data **)&_datPtr, _size);                   \
   if (_ret == ROK)                                             \
      cmMemset((U8*)_datPtr, 0, _size);                         \
   else                                                         \
      _datPtr = NULLP;                                          \
}

/* free a static buffer */
#define DS_FREE(_size, _datPtr)                                 \
   SPutSBuf(dsCb.init.region, dsCb.init.pool,                   \
            (Data *)_datPtr, _size);

/* statistics */
#if (defined(DS_MULTI_THREADED)  &&  defined(DS_STS_LOCKS))

#define DS_INC_ERRSTS(_stat)                                    \
{                                                               \
   DS_LOCK(&dsCb.errStsLock);                                   \
   _stat++;                                                     \
   DS_UNLOCK(&dsCb.errStsLock);                                 \
}

#define DS_INC_TXSTS(_sap, _stat)                               \
{                                                               \
   //DS_LOCK(&_sap->txStsLock);                                   \
   //_stat++;                                                     \
   //DS_UNLOCK(&_sap->txStsLock);                                 \
}

/*hi028.201: Handled statistics properly*/
#define DS_DEC_TXSTS(_sap, _stat)                               \
{                                                               \
   //DS_LOCK(&_sap->txStsLock);                                   \
   //_stat--;                                                     \
   //DS_UNLOCK(&_sap->txStsLock);                                 \
}

#endif

/* statistics */
#if (defined(DS_MULTI_THREADED)  &&  defined(DS_STS_LOCKS))

#define DS_INC_ERRSTS(_stat)                                    \
{                                                               \
   DS_LOCK(&dsCb.errStsLock);                                   \
   _stat++;                                                     \
   DS_UNLOCK(&dsCb.errStsLock);                                 \
}

#define HI_INC_TXSTS(_stat)                                     \
{                                                               \
   DS_LOCK(&dsCb.txStsLock);                                    \
   _stat++;                                                     \
   DS_UNLOCK(&dsCb.txStsLock);                                  \
}

/*hi028.201: Handled statistics properly*/
#define HI_DEC_TXSTS(__stat)                                    \
{                                                               \
   DS_LOCK(&dsCb.txStsLock);                                   \
   _stat--;                                                     \
   DS_UNLOCK(&dsCb.txStsLock);                                 \
}

#define DS_ADD_TXSTS(_stat, _val)                         \
{                                                               \
   DS_LOCK(&dsCb.txStsLock);                                   \
   _stat += _val;                                               \
   HI_UNLOCK(&dsCb.txStsLock);                                 \
}

#define DS_ZERO_ERRSTS()                                        \
{                                                               \
   DS_LOCK(&dsCb.errStsLock);                                   \
   DS_ZERO(&dsCb.errSts, sizeof(SyErrSts));                     \
   DS_UNLOCK(&dsCb.errStsLock);                                 \
}

#define DS_ZERO_TXSTS()                                         \
{                                                               \
   DS_LOCK(&dsCb.txStsLock);                                    \
   dsCb.txSts.numTxMsg = 0;                                     \
   DS_UNLOCK(&dsCb.txStsLock);                                  \
}

#else  /* DS_MULTI_THREADED  &&  DS_STS_LOCKS */


#define DS_INC_ERRSTS(_stat)                                    \
   _stat++;

#define DS_INC_TXSTS(_stat)                                     \
   _stat++;

#define DS_DEC_TXSTS(_stat)                                     \
   _stat--;

#define DS_ADD_TXSTS(_stat, _val)                               \
   _stat += _val;

#define DS_ZERO_ERRSTS()                                        \
   DS_ZERO(&dsCb.errSts, sizeof(SyErrSts));

#define DS_ZERO_TXSTS()                                         \
   dsCb.txSts.numTxMsg = 0;

#define DS_TRC2(_arg)                                                   \
Txt __tapa_fun[PRNTSZE];                                                \
sprintf(__tapa_fun,#_arg);                                              \
{                                                                       \
   if (dsCb.init.dbgMask & (LVB_DBGMASK_TRC))                        \
   {                                                                    \
      VB_PRNTTIMESTAMP(dsCb.init)                                    \
      VB_PRNT_LAYER(dsCb.init, VB_LYR_NAME)                                 \
      VB_PRNT_FILE_LINE(dsCb.init)                                  \
      VBPRNT(dsCb.init, (dsCb.init.prntBuf," Entering\n"));                     \
      TRC2(_arg);                                                       \
   }                                                                    \
}


#endif /* DS_MULTI_THREADED  &&  DS_STS_LOCKS */


#endif /* __DSH__ */


/********************************************************************30**

         End of file:

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

