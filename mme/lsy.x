/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     S6A

     Type:     C header file

     Desc:     Structures, variables, typedefs and prototypes
               required at the management interface.

     File:     lsyG.x

     Sid:

     Prg:      bw

*********************************************************************21*/

#ifndef __LSYX__
#define __LSYX__

#ifdef __cplusplus
extern "C" {
#endif

/* General configuration.
 */
typedef struct syGenCfg
{
   Pst          lmPst;          /* for layer manager */
   Pst          mmePst;

   Region       initRegion;
   Pool         initPool;
} SyGenCfg;

/* General statistics.
 */
typedef struct syGenSts
{
   StsCntr      numErrors;     /* num of errors */
} SyGenSts;

typedef struct sySta
{
   State        state;          /* S6A state */
} SySta;

/* Unsolicited status indication.
 */
typedef struct syAlarmInfo
{
   SpId         spId;           /* sap Id */
   U8           type;           /* which member of inf is present */
   union
   {
      State     state;          /* TSAP state */
      State     conState;       /* connection state */
      Mem       mem;            /* region/pool (resource related) */
      U8        parType;        /* parameter type */
   } inf;

} SyAlarmInfo;

#ifdef DEBUGP
/* Debug control.
 */
typedef struct syDbgCntrl
{
   U32          dbgMask;        /* Debug mask */

} SyDbgCntrl;
#endif


/* Trace control.
 */
typedef struct syTrcCntrl
{
   SpId         sapId;          /* sap Id */
   S16          trcLen;         /* length to trace */

} SyTrcCntrl;



/* Configuration.
 */
typedef struct syCfg
{
   union
   {
      SyGenCfg  syGen;          /* general configuration */
   } s;

} SyCfg;


/* Statistics.
 */
typedef struct sySts
{
   DateTime     dt;             /* date and time */
   Duration     dura;           /* duration */

   union
   {
      SyGenSts  genSts;         /* general statistics */
   } s;

} SySts;

/* Solicited status.
 */
typedef struct sySsta
{
   DateTime     dt;             /* date and time */

   union
   {
      SystemId  sysId;          /* system id */
      SySta  sySta;             /* diameter status */
   } s;

} SySsta;

/* Unsolicited status.
 */
typedef struct syUsta
{
   CmAlarm      alarm;          /* alarm */
   SyAlarmInfo  info;           /* alarm information */

} SyUsta;


/* Trace.
 */
typedef struct syTrc
{
   DateTime     dt;            /* date and time */
   U16          evnt;          /* event */
} SyTrc;

/* Control.
 */
typedef struct syCntrl
{
   DateTime     dt;             /* date and time */
   U8           action;         /* action */
   U8           subAction;      /* sub action */

   union
   {
      SyTrcCntrl        trcDat;         /* trace length */
      ProcId            dstProcId;      /* destination procId */
      Route             route;          /* route */
      Priority          priority;       /* priority */

#ifdef DEBUGP
      SyDbgCntrl        syDbg;          /* debug printing control */
#endif

   } ctlType;

} SyCntrl;

/* Management.
 */
typedef struct syMngmt
{
   Header       hdr;            /* header */
   CmStatus     cfm;            /* response status/confirmation */

   union
   {
      SyCfg     cfg;            /* configuration */
      SySts     sts;            /* statistics */
      SySsta    ssta;           /* solicited status */
      SyUsta    usta;           /* unsolicited status */
      SyTrc     trc;            /* trace */
      SyCntrl   cntrl;          /* control */

   } t;
} SyMngmt;



/* Layer management interface primitive types.
 */
typedef S16  (*LsyCfgReq)       ARGS((Pst *pst, SyMngmt *cfg));
typedef S16  (*LsyCfgCfm)       ARGS((Pst *pst, SyMngmt *cfg));
typedef S16  (*LsyCntrlReq)     ARGS((Pst *pst, SyMngmt *cntrl));
typedef S16  (*LsyCntrlCfm)     ARGS((Pst *pst, SyMngmt *cntrl));
typedef S16  (*LsyStsReq)       ARGS((Pst *pst, Action action,
                                      SyMngmt *sts));
typedef S16  (*LsyStsCfm)       ARGS((Pst *pst, SyMngmt *sts));
typedef S16  (*LsyStaReq)       ARGS((Pst *pst, SyMngmt *sta));
typedef S16  (*LsyStaInd)       ARGS((Pst *pst, SyMngmt *sta));
typedef S16  (*LsyStaCfm)       ARGS((Pst *pst, SyMngmt *sta));
typedef S16  (*LsyTrcInd)       ARGS((Pst *pst, SyMngmt *trc,
                                      Buffer *mBuf));


/* Layer management interface primitives.
 */
//#ifdef SY
EXTERN S16  SyMiLsyCfgReq       ARGS((Pst *pst, SyMngmt *cfg));
EXTERN S16  SyMiLsyCfgCfm       ARGS((Pst *pst, SyMngmt *cfg));
EXTERN S16  SyMiLsyCntrlReq     ARGS((Pst *pst, SyMngmt *cntrl));
EXTERN S16  SyMiLsyCntrlCfm     ARGS((Pst *pst, SyMngmt *cntrl));
EXTERN S16  SyMiLsyStsReq       ARGS((Pst *pst, Action action,
                                      SyMngmt *sts));
EXTERN S16  SyMiLsyStsCfm       ARGS((Pst *pst, SyMngmt *sts));
EXTERN S16  SyMiLsyStaReq       ARGS((Pst *pst, SyMngmt *sta));
EXTERN S16  SyMiLsyStaCfm       ARGS((Pst *pst, SyMngmt *sta));
EXTERN S16  SyMiLsyStaInd       ARGS((Pst *pst, SyMngmt *sta));
EXTERN S16  SyMiLsyTrcInd       ARGS((Pst *pst, SyMngmt *trc,
                                      Buffer *mBuf));

EXTERN S16 SyMiLsyAIA           ARGS((Buffer *mBuf));
EXTERN S16 SyMiLsyPUA           ARGS((Buffer *mBuf));
EXTERN S16 SyMiLsyULAAttachRequest ARGS((Buffer *mBuf));
EXTERN S16 SyMiLsyIDR           ARGS((Void));

//#endif /* SY */

#ifdef SM
EXTERN S16  SmMiLsyCfgReq       ARGS((Pst *pst, SyMngmt *cfg));
EXTERN S16  SmMiLsyCfgCfm       ARGS((Pst *pst, SyMngmt *cfg));
EXTERN S16  SmMiLsyCntrlReq     ARGS((Pst *pst, SyMngmt *cntrl));
EXTERN S16  SmMiLsyCntrlCfm     ARGS((Pst *pst, SyMngmt *cntrl));
EXTERN S16  SmMiLsyStsReq       ARGS((Pst *pst, Action action,
                                      SyMngmt *sts));
EXTERN S16  SmMiLsyStsCfm       ARGS((Pst *pst, SyMngmt *sts));
EXTERN S16  SmMiLsyStaReq       ARGS((Pst *pst, SyMngmt *sta));
EXTERN S16  SmMiLsyStaInd       ARGS((Pst *pst, SyMngmt *sta));
EXTERN S16  SmMiLsyStaCfm       ARGS((Pst *pst, SyMngmt *sta));
EXTERN S16  SmMiLsyTrcInd       ARGS((Pst *pst, SyMngmt *trc,
                                      Buffer *mBuf));
#endif /* SM */


/* Packing and unpacking functions.
 */
EXTERN S16  cmPkLsyCfgReq       ARGS((Pst *pst, SyMngmt *cfg));
EXTERN S16  cmPkLsyCfgCfm       ARGS((Pst *pst, SyMngmt *cfg));
EXTERN S16  cmPkLsyCntrlReq     ARGS((Pst *pst, SyMngmt *cntrl));
EXTERN S16  cmPkLsyCntrlCfm     ARGS((Pst *pst, SyMngmt *cntrl));
EXTERN S16  cmPkLsyStsReq       ARGS((Pst *pst, Action action, SyMngmt *sts));
EXTERN S16  cmPkLsyStsCfm       ARGS((Pst *pst, SyMngmt *sts));
EXTERN S16  cmPkLsyStaReq       ARGS((Pst *pst, SyMngmt *sta));
EXTERN S16  cmPkLsyStaInd       ARGS((Pst *pst, SyMngmt *sta));
EXTERN S16  cmPkLsyTrcInd       ARGS((Pst *pst, SyMngmt *trc, Buffer *mBuf));
EXTERN S16  cmPkLsyStaCfm       ARGS((Pst *pst, SyMngmt *sta));

EXTERN S16  cmUnpkLsyCfgReq     ARGS((LsyCfgReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsyCfgCfm     ARGS((LsyCfgCfm func, Pst *pst,
                                    Buffer *mBuf));
EXTERN S16  cmUnpkLsyCntrlReq   ARGS((LsyCntrlReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsyCntrlCfm   ARGS((LsyCntrlCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsyStsReq     ARGS((LsyStsReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsyStsCfm     ARGS((LsyStsCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsyStaReq     ARGS((LsyStaReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsyStaInd     ARGS((LsyStaInd func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsyStaCfm     ARGS((LsyStaCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsyTrcInd     ARGS((LsyTrcInd func, Pst *pst,
                                      Buffer *mBuf));
/* Layer manager activation functions.
 */
EXTERN S16  smSyActvTsk         ARGS((Pst *pst, Buffer *mBuf));
EXTERN S16  smSyActvInit        ARGS((Ent ent, Inst inst,
                                      Region region, Reason reason));

#ifdef SS_MULTIPLE_PROCS
EXTERN S16 syActvInit       ARGS((ProcId procId,
                                  Ent entity,
                                  Inst inst,
                                  Region region,
                                  Reason reason,
                                  Void **xxCb));
#else /* SS_MULTIPLE_PROCS */

EXTERN S16  syActvInit          ARGS((Ent ent, Inst inst,
                                      Region region, Reason reason));
#endif /* SS_MULTIPLE_PROCS */
EXTERN S16  syActvTsk           ARGS((Pst *pst, Buffer *mBuf));

#ifdef __cplusplus
}
#endif

#endif /* __LSYX__ */


/********************************************************************30**

         End of file:     lhi.x@@/main/10 - Tue Feb  1 16:04:21 2011

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat     init                  description
----------- --------- ---- -----------------------------------------------
1.1         ---       bw   1. initial release.
*********************************************************************91*/

