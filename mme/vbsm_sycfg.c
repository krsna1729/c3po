/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:    Trillium LTE CNE - MME Controller Module

     Type:    C Include file

     Desc:    This file contains the vb application source code

     File:    vbsm_sycfg.c

     Sid:

     Prg:     bw
*********************************************************************21*/

/* Header include files (.h) */
#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "cm_os.h"

#include "sct.h"           /* SCT interface defines           */

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
/* #include "sz_err.h"*/        /* S1AP Error */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "cm_os.x"

#include "sct.x"           /* SCT interface defines           */

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"
#include "lsy.h"
#include "lsy.x"

#include <freeDiameter/freeDiameter-host.h>
#include <freeDiameter/libfdcore.h>
#include <freeDiameter/libfdproto.h>

#include "aqfd.h"
#include "aqfd.x"

#include "sy.h"
#include "sy.x"
#include "sy_err.h"

#ifdef __cplusplus
EXTERN "C" {
#endif /* __cplusplus */

/* #define VB_SM_HI_CONFIGURED  (STGEN | STTSAP) */

#define VB_SM_SY_CONFIGURED (STGEN)

U8      vbSmSyCfg = 0;

/* Function prototypes */
PRIVATE Void vbMmeLsySyGenCfg ARGS ((Void));

/*
 *
 *       Fun:    vbMmeSyCfg - configure SY
 *
 *       Desc:
 *
 *       Ret:    Void
 *
 *       Notes:  None
 *
 *       File:   vbsm_sycfg.c
 *
 */

#ifdef ANSI
PUBLIC Void vbMmeSyCfg
(
Void
)
#else
PUBLIC Void vbMmeSyCfg()
#endif /* ANSI */
{
   SM_TRC2(vbMmeSyCfg);

   VBSM_DBG_INFO((VBSM_PRNTBUF,"Sending MME SyCfg...\n"));
   vbSmCb.syPst.event = EVTLSYCFGREQ;

   vbMmeLsySyGenCfg();

   RETVOID;
}
/*
 *
 *       Fun:    vbMmeLsySyGenCfg - fill in default genCfg for SY
 *
 *       Desc:
 *
 *       Ret:    Void
 *
 *       Notes:  None
 *
 *       File:   vbsm_sycfg.c
 *
 */

#ifdef ANSI
PRIVATE Void vbMmeLsySyGenCfg
(
Void
)
#else
PRIVATE Void vbMmeLsySyGenCfg()
#endif /* ANSI */
{
   SyMngmt     syMgt;
   SyGenCfg    *cfg;

   SM_TRC2(vbMmeLsySyGenCfg);

   cmMemset((U8 *)&syMgt, 0, sizeof(SyMngmt));
   vbSmDefHdr(&syMgt.hdr, ENTSY, STGEN, VBSM_SYSMSEL);

   cfg = &syMgt.t.cfg.s.syGen;

   cfg->lmPst.srcProcId = SFndProcId();
   cfg->lmPst.dstProcId = SFndProcId();
   cfg->lmPst.srcEnt = (Ent)ENTSY;
   cfg->lmPst.dstEnt = (Ent)ENTSM;
   cfg->lmPst.srcInst = (Inst)0;
   cfg->lmPst.dstInst = (Inst)0;

   cfg->lmPst.prior = (Prior)VBSM_MSGPRIOR;
   cfg->lmPst.route = (Route)RTESPEC;
   cfg->lmPst.event = (Event)EVTNONE;
   cfg->lmPst.region = (Region)vbSmCb.init.region;
   cfg->lmPst.pool = (Pool)vbSmCb.init.pool;
   cfg->lmPst.selector = (Selector)VBSM_SYSMSEL;

   cfg->mmePst.srcProcId = SFndProcId();
   cfg->mmePst.dstProcId = SFndProcId();
   cfg->mmePst.srcEnt = (Ent)ENTSY;
   cfg->mmePst.dstEnt = (Ent)ENTVB;
   cfg->mmePst.srcInst = (Inst)0;
   cfg->mmePst.dstInst = (Inst)0;

   cfg->mmePst.prior = (Prior)VBSM_MSGPRIOR;
   cfg->mmePst.route = (Route)RTESPEC;
   cfg->mmePst.event = (Event)EVTNONE;
   cfg->mmePst.region = (Region)vbSmCb.init.region;
   cfg->mmePst.pool = (Pool)vbSmCb.init.pool;
   cfg->mmePst.selector = (Selector)VBSM_SYSMSEL;
   cfg->mmePst.event = EVTLSYAIA;

   (Void)SmMiLsyCfgReq(&vbSmCb.syPst, &syMgt);
   VBSM_DBG_INFO((VBSM_PRNTBUF,"Sent MME syGenCfg...\n"));
   RETVOID;
} /* end of vbMmeLsySyGenCfg() */

/*
 *      FUN:   vbMmeSyShutDwn
 *
 *      Desc:  Brings the SY to the state before configuration
 *
 *      Ret:   void
 *
 *      Notes: None
 *
 *      File:  vbsm_sycfg.c
 *
 *
 */
#ifdef ANSI
PUBLIC S16 vbMmeSyShutDwn
(
Void
)
#else
PUBLIC S16 vbMmeSyShutDwn()
#endif /* ANSI */
{
   SyMngmt              syMgt;
   S16                  ret = ROK;

   SM_TRC2(vbMmeSyShutDwn);

   cmMemset((U8 *)&syMgt, 0, sizeof(SyMngmt));
   vbSmDefHdr(&syMgt.hdr, ENTSY, STGEN, VBSM_SYSMSEL);

   syMgt.t.cntrl.action = ASHUTDOWN;
   syMgt.t.cntrl.subAction = SAELMNT;

   vbSmCb.syPst.event = EVTLSYCNTRLREQ;
   (Void)SmMiLsyCntrlReq(&vbSmCb.syPst, &syMgt);
   VBSM_DBG_INFO((VBSM_PRNTBUF,"Sent MME sy Shutdown CntrlReq...\n"));

   RETVALUE(ret);
}

/*
*
*       Fun:   Configuration Confirm
*
*       Desc:  This function is used by Layer to present configuration confirm
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_sycfg.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLsyCfgCfm
(
Pst     *pst,          /* post structure */
SyMngmt *cfm           /* configuration */
)
#else
PUBLIC S16 SmMiLsyCfgCfm(pst, cfm)
Pst     *pst;          /* post structure */
SyMngmt *cfm;          /* configuration */
#endif
{
   SM_TRC2(SmMiLsyCfgCfm);

   VBSM_DBG_INFO((VBSM_PRNTBUF,"Received MME sy CfgCfm with elmnt(%d) - status(%d)...\n",cfm->hdr.elmId.elmnt,cfm->cfm.status));
   if (cfm->cfm.status == LCM_PRIM_OK)
   {
      vbSmSyCfg |=  cfm->hdr.elmId.elmnt;
      switch (cfm->hdr.elmId.elmnt)
      {
         case STGEN:
         {
            vbSmSyCfg |=  cfm->hdr.elmId.elmnt;
            VBSM_DBG_INFO((VBSM_PRNTBUF,"Received MME sy Gen CfgCfm...\n"));
            break;
         }
         default:
            VBSM_DBG_ERROR((VBSM_PRNTBUF,"Invalid elemt(%d)...\n",cfm->hdr.elmId.elmnt));
        break;
      }

      if (vbSmSyCfg == VB_SM_SY_CONFIGURED)
      {
         VBSM_DBG_INFO((VBSM_PRNTBUF,"SmMiLsyCfgCfm: VB_SM_SY_CONFIGURED\n"));
         vbMmeSendMsg(EVTVBS6ACFGDONE);
      }
      else
      {
           VBSM_DBG_INFO((VBSM_PRNTBUF,"Received MME CfgCfm Pending...\n"));
      }
   }
   else
   {
      VBSM_DBG_INFO((VBSM_PRNTBUF,"Received MME CfgCfm .NOT OK..\n"));
   }

   RETVALUE(ROK);
} /* end of SmMiLsyCfgCfm */



/*
*
*       Fun:   Control Confirm
*
*       Desc:  This function is used by  Layer to present control confirm
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_sycfg.c
*
*/
#ifdef ANSI
PUBLIC S16 SmMiLsyCntrlCfm
(
Pst     *pst,          /* post structure */
SyMngmt *cfm           /* control */
)
#else
PUBLIC S16 SmMiLsyCntrlCfm(pst, cfm)
Pst     *pst;          /* post structure */
SyMngmt *cfm;          /* control */
#endif
{
   SM_TRC2(SmMiLsyCntrlCfm)

   if (cfm->cfm.status == LCM_PRIM_OK)
   {
      VBSM_DBG_INFO((VBSM_PRNTBUF,"Received control confirmation OK from Diameter...\n"));
   }
   else
   {
      VBSM_DBG_INFO((VBSM_PRNTBUF,"Received control confirmation NOT OK from Diameter...\n"));
   }
   RETVALUE(ROK);
} /* end of SmMiLsyCntrlCfm */

/*
*
*       Fun:   Status Indication
*
*       Desc:  This function is used by Layer to present  unsolicited status
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_sycfg.c
*
*/
#ifdef ANSI
PUBLIC S16 SmMiLsyStaInd
(
Pst     *pst,           /* post structure */
SyMngmt *usta           /* unsolicited status */
)
#else
PUBLIC S16 vbsm_sycfg(pst, usta)
Pst     *pst;           /* post structure */
SyMngmt *usta;          /* unsolicited status */
#endif
{
   SM_TRC2(SmMiLsyStaInd);

   UNUSED(pst);
   UNUSED(usta);

   RETVALUE(ROK);
} /* end of SmMiLhiStaInd */

/*
*
*       Fun:   Trace Indication
*
*       Desc:  This function is used by  Layer to present trace
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_sycfg.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLsyTrcInd
(
Pst *pst,               /* post structure */
SyMngmt *trc,           /* trace */
Buffer *mBuf            /* message buffer */
)
#else
PUBLIC S16 SmMiLsyTrcInd(pst, trc, mBuf)
Pst *pst;               /* post structure */
SyMngmt *trc;           /* trace */
Buffer *mBuf;           /* message buffer */
#endif
{
   SM_TRC2(SmMiLsyTrcInd);

   UNUSED(pst);
   UNUSED(trc);

   RETVALUE(ROK);
} /* end of SmMiLhiTrcInd */

/*
*
*       Fun:   Statistics Confirm
*
*       Desc:  This function is used by Layer to present solicited statistics
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_sycfg.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLsyStsCfm
(
Pst       *pst,         /* post structure */
SyMngmt   *sts          /* confirmed statistics */
)
#else
PUBLIC S16 SmMiLsyStsCfm(pst, sts)
Pst       *pst;         /* post structure */
SyMngmt   *sts;         /* confirmed statistics */
#endif
{
   SM_TRC2(SmMiLsyStsCfm);

   UNUSED(pst);
   UNUSED(sts);

   RETVALUE(ROK);
} /* end of SmMiLsyStsCfm */

/*
*
*       Fun:   Status Confirm
*
*       Desc:  This function is used by Layer to present solicited status
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_sycfg.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLsyStaCfm
(
Pst     *pst,           /* post structure */
SyMngmt *sta             /* confirmed status */
)
#else
PUBLIC S16 SmMiLsyStaCfm(pst, sta)
Pst     *pst;           /* post structure */
SyMngmt *sta;            /* confirmed status */
#endif
{
   SM_TRC2(SmMiLsyStaCfm);

   UNUSED(pst);
   UNUSED(sta);

   RETVALUE(ROK);
} /* end of SmMiLsyStaCfm */

#ifdef __cplusplus
}
#endif /* __cplusplus */

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************60**
        Revision history:
*********************************************************************61*/


/********************************************************************90**

     ver       pat    init                  description
------------ -------- ---- ----------------------------------------------
/main/1      ---     bw         1. Initial version.
***********************************************************************/
