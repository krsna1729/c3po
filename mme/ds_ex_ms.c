/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:    S6A Convergence Layer

     Type:    C source file

     Desc:    External interface to SSI.

     File:    ds_ex_ms.c

     Sid:

     Prg:     bw

*********************************************************************21*/


/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"
#include "lsy.h"
#include "lsy.x"

#include "lds.h"
#include "lds.x"

#include "ds_err.h"
#include "ds.h"
#include "ds.x"

/*
*
*       Fun:    dsActvTsk
*
*       Desc:   Process received events.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   ds_ex_ms.c
*
*/
#ifdef ANSI
PUBLIC S16 dsActvTsk
(
Pst             *pst,           /* post */
Buffer          *mBuf           /* message buffer */
)
#else
PUBLIC S16 dsActvTsk(pst, mBuf)
Pst             *pst;           /* post */
Buffer          *mBuf;          /* message buffer */
#endif
{
   S16          ret = ROK;

   TRC3(dsActvTsk);

#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId, pst->dstEnt, pst->dstInst,
                          (Void **)&dsCbPtr)) != ROK)
   {
      SYLOGERROR_DEBUGPST(pst->dstProcId, pst->dstEnt, EDS001,
            (ErrVal)0, pst->dstInst,
            "dsActvTsk() failed, cannot derive dsCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_MI, (dsCb.init.prntBuf,
      "---------EPC DNS------(proc(%d),entt(%d),inst(%d),event(%d),srcEnt(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst,pst->event,pst->srcEnt));
#endif  /* SS_MULTIPLE_PROCS */

   /* check the message source */
   switch (pst->srcEnt)
   {
#ifdef LCDSMILDS
      /* stack manager primitive */
      case ENTSM:
      {
         switch (pst->event)
         {
            case EVTLDSCFGREQ:
               ret = cmUnpkLdsCfgReq(DsMiLdsCfgReq, pst, mBuf);
               break;
            case EVTLDSSTSREQ:
               ret = cmUnpkLdsStsReq(DsMiLdsStsReq, pst, mBuf);
               break;
            case EVTLDSCNTRLREQ:
               ret = cmUnpkLdsCntrlReq(DsMiLdsCntrlReq, pst, mBuf);
               break;
            case EVTLDSSTAREQ:
               ret = cmUnpkLdsStaReq(DsMiLdsStaReq, pst, mBuf);
               break;
            default:
               DSLOGERROR_INT_PAR(EDS002, pst->event, pst->dstInst,
                  "dsActvTsk(): Invalid event from layer manager");
               SPutMsg(mBuf);
               ret = RFAILED;
               break;
         }
         break;
      }
#endif /* LCSYMILSY */

      default:
         DSLOGERROR_INT_PAR(EDS003, pst->event, pst->dstInst,
            "dsActvTsk(): Invalid source entity");
         SPutMsg(mBuf);
         ret = RFAILED;
         break;
   }

   SExitTsk();

   RETVALUE(ret);
} /* end of dsActvTsk */


#ifdef ANSI
PUBLIC S16 SmMiLdsInitAttachSgw
(
Pst          *pst,          /* post structure */
DnsQueryData *qd            /* configuration */
)
#else
PUBLIC S16 SmMiLdsInitAttachSgw(pst, qd)
Pst          *pst;          /* post structure */
DnsQueryData *qd;           /* configuration */
#endif
{
   S16 ret;
   VbMmeUeCb *ueCb = NULLP;

   DS_TRC2(SmMiLdsInitAttachSgw);
   VBSM_DBG_INFO((VBSM_PRNTBUF,"Received MME Vb DnsSgw with - success(%d)..\n",\
         qd->success));

   /* lookup the ue */
   ret = vbMmeUtlFndUeCbOnImsi(qd->imsi, qd->imsilen, &ueCb);
   if ( ret == ROK )
   {

      /* if success, save the address */
      if ( qd->success )
      {
         memcpy( &ueCb->ueCtxt.sgwAddr, &qd->addr, sizeof(ueCb->ueCtxt.sgwAddr) );
         ueCb->ueCtxt.sgwAddrValid = TRUE;
      }

      /* flag the dns lookup as being complete */
      VB_MME_ATTCH_CHECK_ALL( ueCb, VB_MME_ATTCH_SGW_RECEIVED );
   }
   else
   {
      VBSM_DBG_INFO((VBSM_PRNTBUF, "ueCb not found for imsi [%*s]", qd->imsilen, qd->imsi));
      ret  = vbMmeSndAttachReject(ueCb, CM_EMM_IMSI_UNKNOWN, 0);
   }

   /* free qd */
   if ( qd )
      free( qd );

   RETVALUE(ret);
} /* end of SmMiLvbDnsSgw */

/********************************************************************30**

         End of file:     ds_ex_ms.c@@/main/6 - Mon Mar  3 20:09:50 2008

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat     init                  description
----------- --------- ---- -----------------------------------------------
1.1         ---       bw   1. initial release.
*********************************************************************91*/

