/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter Convergence Layer

     Type:     C source file

     Desc:     freeDiameter initialization.

     File:     aqfd_ai.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */
#include "cm_os.h"

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */
#include "cm_os.x"

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"

#include "lsy.h"
#include "lsy.x"

#include <freeDiameter/freeDiameter-host.h>
#include <freeDiameter/libfdcore.h>
#include <freeDiameter/libfdproto.h>

#include "aqfd.h"
#include "aqfd.x"

#include "sy.h"
#include "sy.x"
#include "sy2.x"
#include "sy_err.h"

EXTERN int aqfdCreateIDA ARGS((SyIDR *idr, struct msg **ida));
EXTERN int aqfdAddMonitoringEventConfigStatus ARGS((SyMonitoringEventConfiguration *mec, struct msg *m, vendor_id_t vndid, S32 res));
EXTERN int aqfdAddMonitoringEventConfigReport ARGS((SyMonitoringEventConfiguration *mec, struct msg *m, vendor_id_t vndid, S32 res));

PRIVATE S16 aqfdParseAuthenticationInfo ARGS((struct avp *avp, SyAIA *aia));

/*
 *  Thread local Pst related variables and init routine
 */
static __thread int _pstInitialized;
static __thread Pst _pst;

PRIVATE Void _initPst ARGS((void));

Void _initPst()
{
   if (!_pstInitialized)
   {
      memcpy( &_pst, &aqCb.cfg.syPst, sizeof(_pst) ); 
      _pstInitialized = 1;
   }
}



/*
*
*       Fun:    aqfd_idr_cb
*
*       Desc:   Insert-Subscriber-Data-Request
*
*       Ret:    0
*
*       Notes:  None
*
*       File:   aqfd_id.c
*
*       The Insert-Subscriber-Data-Request (IDR) command, indicated by
*       the Command-Code field set to 319 and the 'R' bit set in the
*       Command Flags field, is sent from HSS to MME or SGSN.
*
*       < Insert-Subscriber-Data-Request > ::= <Diameter-Header: 319, REQ, PXY, 16777251>
*  		   <Session-Id>
*          [DRMP]
*          [Vendor-Specific-Application-Id]
*          {Auth-Session-State}
*          {Origin-Host}
*          {Origin-Realm}
*          {Destination-Host}
*          {Destination-Realm}
*          {User-Name}
*          *[Supported-Features]
*          {Subscription-Data}
*          [IDR-Flags]
*          *[Reset-ID]
*          *[AVP]
*          *[Proxy-Info]
*          *[Route-Record]
*
*
*/
#ifdef ANSI
PUBLIC int aqfd_idr_cb
(
struct msg ** msg,
struct avp * pavp,
struct session * sess,
void * data,
enum disp_action * act
)
#else
PUBLIC int aqfd_idr_cb(msg, pavp, sess, data, act)
struct msg ** msg;
struct avp * pavp;
struct session * sess;
void * data;
enum disp_action * act;
#endif
{
   S16 ret;
   S8 imsi[VB_HSS_IMSI_LEN + 1];
   struct msg   *qry = NULL;
   struct avp   *avp = NULL;
   SyIDR     	 *idr = NULL;

   qry = *msg;
   *msg = NULL;

AQFD_DUMP_MESSAGE(qry);

   /* allocate the idr message */
   idr = (SyIDR*)malloc( sizeof(*idr) );
   memset( (void*)idr, 0, sizeof(*idr) );
   idr->subscription_data = (SySubscriptionData*)malloc( sizeof(SySubscriptionData) );
   memset( (void*)idr->subscription_data, 0, sizeof(*idr->subscription_data) );

   AQCHECK_MSG_GET_AVP_STR(qry, aqDict.avp_User_Name, imsi, sizeof(imsi), goto err);
   aqfdImsi2Binary(imsi, idr->imsi.val, &idr->imsi.len);

   /* get Subscription-Data */
   AQCHECK_MSG_FIND_AVP(qry, aqDict.avp_Subscription_Data, avp, );
   if ( avp )
   {
      if ( aqfdParseSubscriptionData(avp, idr->subscription_data) != LCM_REASON_NOT_APPL)
      {
         goto err;
      }
   }

   /* add the original message on the sy structure*/
   idr->msg = qry;

   goto fini1;

err:
   free(idr);
   goto fini2;

fini1:
   /* push idr */
   aqQueuePush(&aqCb.idrQueue, idr);

   _initPst();
   _pst.event = EVTLSYIDR;
   {
      Buffer *buf = NULL;
      if(SGetMsg(aqCb.cfg.syPst.region, aqCb.cfg.syPst.pool, &buf) != ROK)
         LOG_E("Error %d returned from SGetMsg() sending ULA notification", ret);
      else
      {
         if ((ret = SPstTsk(&_pst, buf)) != ROK)
         {
            LOG_E("Error %d returned from SPstTsk() sending ULA notification", ret);
            SPutMsg(buf);
         }
      }
   }

fini2:
   return 0;
}

/*
*
*       Fun:    aqfdSendIDA
*
*       Desc:   Insert-Subscriber-Data-Answer
*
*       Ret:    0
*
*       Notes:  None
*
*       File:   aqfd_id.c
*
*       Insert-Subscriber-Data-Answer ::= <Diameter-Header: 319, PXY, 16777251>
*         <Session-Id>
*         [DRMP]
*         [Vendor-Specific-Application-Id]
*         *[Supported-Features]
*         [Result-Code]
*         [Experimental-Result]
*         {Auth-Session-State}
*         {Origin-Host}
*         {Origin-Realm}
*         [IMS-Voice-Over-PS-Sessions-Supported]
*         [Last-UE-Activity-Time]
*         [RAT-Type]
*         [IDA-Flags]
*         [EPS-User-State]
*         [EPS-Location-Information]
*         [Local-Time-Zone]
*         [Supported-Services]
*         *[Monitoring-Event-Report]
*         *[Monitoring-Event-Config-Status]
*         *[AVP]
*         [Failed-AVP]
*         *[Proxy-Info]
*         *[Route-Record]
*
*/


#ifdef ANSI
PUBLIC int aqfdCreateIDA
(
SyIDR *idr,
struct msg **ida
)
#else
PUBLIC int aqfdCreateIDA(idr,ida)
SyIDR *idr;
struct msg **ida;
#endif
{
   *ida = idr->msg;

   /* construct the answer from the original request */
   AQCHECK_MSG_NEW_ANSWER_FROM_REQ(fd_g_config->cnf_dict, (*ida));

   /* The Session-Id AVP is copied by freediameter if present*/

   /* Add Origin-Host, Origin-Realm, Origin-State-Id AVPS at the end of the message */
   AQCHECK_MSG_ADD_ORIGIN( *ida );

   AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Auth_Session_State, *ida, MSG_BRW_LAST_CHILD, 1 );

   return LCM_REASON_NOT_APPL;
}

#ifdef ANSI
PUBLIC int aqfdSendIDA
(
struct msg * ida,
vendor_id_t vndid,
S32 res
)
#else
PUBLIC int aqfdSendIDA(msg, vndid, res)
struct msg * ida;
vendor_id_t vndid;
S32 res;
#endif
{
   if ( vndid == 0 ) {
      AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Result_Code, ida, MSG_BRW_LAST_CHILD, res );
   }
   else {
      struct avp *a;

      AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Experimental_Result, ida, MSG_BRW_LAST_CHILD, a );
      AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Vendor_Id, a, MSG_BRW_LAST_CHILD, vndid );
      AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Experimental_Result_Code, a, MSG_BRW_LAST_CHILD, res );
   }

AQFD_DUMP_MESSAGE(ida);

   /* send the message */
   AQCHECK_MSG_SEND( &ida, NULL, NULL );

   return LCM_REASON_NOT_APPL;
}

#ifdef ANSI
PUBLIC int aqfdAddMonitoringEventConfigStatus
(
SyMonitoringEventConfiguration *mec,
struct msg *m,
vendor_id_t vndid,
S32 res
)
#else
PUBLIC int aqfdAddMonitoringEventConfigStatus(mec, m, vndid, res)
SyMonitoringEventConfiguration *mec;
struct msg *m;
vendor_id_t vndid;
S32 res;
#endif
{
   struct avp *mecs;

   if (m)
   {
      AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Monitoring_Event_Config_Status, m, MSG_BRW_LAST_CHILD, mecs );
      AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_SCEF_ID, mecs, MSG_BRW_LAST_CHILD, mec->scef_id->val );
      AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_SCEF_Reference_ID, mecs, MSG_BRW_LAST_CHILD, mec->scef_reference_id );

      struct avp *srpt;

      AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Service_Report, mecs, MSG_BRW_LAST_CHILD, srpt );
      AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Node_Type, srpt, MSG_BRW_LAST_CHILD, 1 /* MME */ );

      struct avp *sres;
      AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Service_Result, srpt, MSG_BRW_LAST_CHILD, sres );
      if ( vndid != 0 )
         AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Vendor_Id, sres, MSG_BRW_LAST_CHILD, vndid );
      AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Service_Result_Code, sres, MSG_BRW_LAST_CHILD, res );
   }

   return LCM_REASON_NOT_APPL;
}

#if 0
#ifdef ANSI
PUBLIC int aqfdAddMonitoringEventConfigReport
(
SyMonitoringEventConfiguration *mec,
struct msg *m,
vendor_id_t vndid,
S32 res
)
#else
PUBLIC int aqfdAddMonitoringEventConfigReport(mec, m, vndid, res)
SyMonitoringEventConfiguration *mec;
struct msg *m;
vendor_id_t vndid;
S32 res;
#endif
{
   struct avp *mer;

   if ( m )
   {
      AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Monitoring_Event_Report, m, MSG_BRW_LAST_CHILD, mer );
      AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_SCEF_ID, mer, MSG_BRW_LAST_CHILD, mec->scef_id->val );
      AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_SCEF_Reference_ID, mer, MSG_BRW_LAST_CHILD, mec->scef_reference_id );

      struct avp *srpt;

      AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Service_Report, mer, MSG_BRW_LAST_CHILD, srpt );
      AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Node_Type, srpt, MSG_BRW_LAST_CHILD, 1 /* MME */ );

      struct avp *sres;
      AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Service_Report, srpt, MSG_BRW_LAST_CHILD, sres );
      if ( vndid != 0 )
         AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Vendor_Id, sres, MSG_BRW_LAST_CHILD, vndid );
      AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Service_Result_Code, sres, MSG_BRW_LAST_CHILD, res );
   }

   return LCM_REASON_NOT_APPL;
}
#endif

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision History:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      jb   1. initial release.
*********************************************************************91*/

