/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*!\file mpnumber.h
 * \brief Multi-precision numbers, headers.
 * \author Bob Deblier <bob.deblier@pandora.be>
 * \ingroup MP_m
 */

#ifndef _MPNUMBER_H
#define _MPNUMBER_H

#include <mp.h>

#ifdef __cplusplus
# include <iostream>
#endif

#ifdef __cplusplus
struct BEECRYPTAPI mpnumber
#else
struct _mpnumber
#endif
{
	size_t	size;
	mpw*	data;

#ifdef __cplusplus
	static const mpnumber ZERO;
	static const mpnumber ONE;

	mpnumber();
	mpnumber(unsigned int);
	mpnumber(const mpnumber&);
	~mpnumber();

	const mpnumber& operator=(const mpnumber&);
	bool operator==(const mpnumber&) const throw ();
	bool operator!=(const mpnumber&) const throw ();

	void wipe();

	size_t bitlength() const throw ();
#endif
};

#ifndef __cplusplus
typedef struct _mpnumber mpnumber;
#else
BEECRYPTAPI
std::ostream& operator<<(std::ostream&, const mpnumber&);
/*
BEECRYPTAPI
std::istream& operator>>(std::istream&, mpnumber&);
*/
#endif

#ifdef __cplusplus
extern "C" {
#endif

BEECRYPTAPI
void mpnzero(mpnumber*);
BEECRYPTAPI
void mpnsize(mpnumber*, size_t);
BEECRYPTAPI
void mpninit(mpnumber*, size_t, const mpw*);
BEECRYPTAPI
void mpnfree(mpnumber*);
BEECRYPTAPI
void mpncopy(mpnumber*, const mpnumber*);
BEECRYPTAPI
void mpnwipe(mpnumber*);

BEECRYPTAPI
void mpnset   (mpnumber*, size_t, const mpw*);
BEECRYPTAPI
void mpnsetw  (mpnumber*, mpw);

BEECRYPTAPI
int mpnsetbin(mpnumber*, const byte*, size_t);
BEECRYPTAPI
int mpnsethex(mpnumber*, const char*);

BEECRYPTAPI
int  mpninv(mpnumber*, const mpnumber*, const mpnumber*);

/*!\brief Truncate the mpnumber to the specified number of (least significant) bits.
 */
BEECRYPTAPI
size_t mpntrbits(mpnumber*, size_t);
BEECRYPTAPI
size_t mpnbits(const mpnumber*);

#ifdef __cplusplus
}
#endif

#endif
