/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*!\file hmac.h
 * \brief HMAC algorithm, headers.
 * \author Bob Deblier <bob.deblier@pandora.be>
 * \ingroup HMAC_m
 */

#ifndef _HMAC_H
#define _HMAC_H

#include <beecrypt.h>

/*!\ingroup HMAC_m
 */

#ifdef __cplusplus
extern "C" {
#endif

/* not used directly as keyed hash function, but instead used as generic methods */

BEECRYPTAPI
int hmacSetup (      byte*,       byte*, const hashFunction*, hashFunctionParam*, const byte*, size_t);
BEECRYPTAPI
int hmacReset (const byte*,              const hashFunction*, hashFunctionParam*);
BEECRYPTAPI
int hmacUpdate(                          const hashFunction*, hashFunctionParam*, const byte*, size_t);
BEECRYPTAPI
int hmacDigest(             const byte*, const hashFunction*, hashFunctionParam*, byte*);

#ifdef __cplusplus
}
#endif

#endif
