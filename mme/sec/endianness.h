/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


#ifndef _ENDIANNESS_H
#define _ENDIANNESS_H

#include <beecrypt.h>
#include <sys/param.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <inttypes.h>

#if defined(__cplusplus) || HAVE_INLINE

static inline int16_t _swap16(int16_t n)
{
	if(htons(0x1234) != 0x1234)
    {
	    return (    ((n & 0xff) << 8) |
				((n & 0xff00) >> 8) );
	}
	else
	{
				return n;
	}
}
# define swap16(n) _swap16(n)

static inline uint16_t _swapu16(uint16_t n)
{
	if(htons(0x1234) != 0x1234)
    {
	  return (    ((n & 0xffU) << 8) |
				((n & 0xff00U) >> 8) );
	}
	else
	{
	  return n;
	}
}
# define swapu16(n) _swap16(n)

# ifdef __arch__swab32
#  define swap32(n) __arch__swab32(n)
#  define swapu32(n) __arch__swab32(n)
# else

static inline int32_t _swap32(int32_t n)
{
	if(htons(0x1234) != 0x1234)
    {
	     return (    ((n & 0xff) << 24) |
				((n & 0xff00) << 8) |
				((n & 0xff0000) >> 8) |
				((n & 0xff000000) >> 24) );
	}
	else
	{
				return n;
	}
}
#  define swap32(n) _swap32(n)

static inline uint32_t _swapu32(uint32_t n)
{
	if(htons(0x1234) != 0x1234)
    {
	  return (    ((n & 0xffU) << 24) |
				((n & 0xff00U) << 8) |
				((n & 0xff0000U) >> 8) |
				((n & 0xff000000U) >> 24) );
	}
	else
	{
				return n;
	}
}
#  define swapu32(n) _swapu32(n)

# endif

# ifdef __arch__swab64
#  define swap64(n) __arch__swab64(n)
#  define swapu64(n) __arch__swab64(n)
# else

static inline int64_t _swap64(int64_t n)
{
	if(htons(0x1234) != 0x1234)
    {
	     return (    ((n & ((int64_t) 0xff)      ) << 56) |
				((n & ((int64_t) 0xff) <<  8) << 40) |
				((n & ((int64_t) 0xff) << 16) << 24) |
				((n & ((int64_t) 0xff) << 24) <<  8) |
				((n & ((int64_t) 0xff) << 32) >>  8) |
				((n & ((int64_t) 0xff) << 40) >> 24) |
				((n & ((int64_t) 0xff) << 48) >> 40) |
				((n & ((int64_t) 0xff) << 56) >> 56) );
	}
	else
	{
				return n;
	}
}
#  define swap64(n) _swap64(n)

static inline uint64_t _swapu64(uint64_t n)
{
	if(htons(0x1234) != 0x1234)
    {
	     return (    ((n & ((uint64_t) 0xff)      ) << 56) |
				((n & ((uint64_t) 0xff) <<  8) << 40) |
				((n & ((uint64_t) 0xff) << 16) << 24) |
				((n & ((uint64_t) 0xff) << 24) <<  8) |
				((n & ((uint64_t) 0xff) << 32) >>  8) |
				((n & ((uint64_t) 0xff) << 40) >> 24) |
				((n & ((uint64_t) 0xff) << 48) >> 40) |
				((n & ((uint64_t) 0xff) << 56) >> 56) );
	}
	else
	{
				return n;
	}
}
#  define swapu64(n) _swapu64(n)

# endif

#else
BEECRYPTAPI
 int16_t swap16 (int16_t);
BEECRYPTAPI
uint16_t swapu16(uint16_t);
BEECRYPTAPI
 int32_t swap32 (int32_t);
BEECRYPTAPI
uint32_t swapu32(uint32_t);
BEECRYPTAPI
 int64_t swap64 (int64_t);
BEECRYPTAPI
uint64_t swapu64(uint64_t);
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif
