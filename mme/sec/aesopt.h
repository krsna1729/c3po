/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*!\file aesopt.h
 * \brief AES block cipher, assembler-optimized routines, headers.
 * \author Bob Deblier <bob.deblier@telenet.be>
 * \ingroup BC_aes_m
 */

#ifndef _AESOPT_H
#define _AESOPT_H

#include "beecrypt.h"
#include "aes.h"

#ifdef __cplusplus
extern "C" {
#endif

#if WIN32
# if defined(_MSC_VER) && defined(_M_IX86)
/* this space intentionally left blank */
# elif __INTEL__ && __MWERKS__
# endif
#endif

#if defined(__GNUC__)
# if defined(OPTIMIZE_I586) || defined(OPTIMIZE_I686)
# endif
#endif

#if defined(__INTEL_COMPILER)
# if defined(OPTIMIZE_I586) || defined(OPTIMIZE_I686)
# endif
#endif

#if defined(__SUNPRO_C) || defined(__SUNPRO_CC)
/* this space intentionally left blank */
#endif               

#ifdef __cplusplus
}
#endif

#endif

