/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*!\file endianness.c
 * \brief Endian-dependant encoding/decoding.
 * \author Bob Deblier <bob.deblier@pandora.be>
 */

#define BEECRYPT_DLL_EXPORT

#if HAVE_CONFIG_H
# include "config.h"
#endif

#if HAVE_ENDIAN_H && HAVE_ASM_BYTEORDER_H
# include <endian.h>
#endif

#include <endianness.h>

#undef swap16
#undef swapu16
#undef swap32
#undef swapu32
#undef swap64
#undef swapu64

int16_t swap16(int16_t n)
{
	return (    ((n & 0xff) << 8) |
				((n & 0xff00) >> 8) );
}

uint16_t swapu16(uint16_t n)
{
	return (    ((n & 0xffU) << 8) |
				((n & 0xff00U) >> 8) );
}

int32_t swap32(int32_t n)
{
	return (    ((n & 0xff) << 24) |
				((n & 0xff00) << 8) |
				((n & 0xff0000) >> 8) |
				((n & 0xff000000) >> 24) );
}

uint32_t swapu32(uint32_t n)
{
	return (    ((n & 0xffU) << 24) |
				((n & 0xff00U) << 8) |
				((n & 0xff0000U) >> 8) |
				((n & 0xff000000U) >> 24) );
}

int64_t swap64(int64_t n)
{
	return (    ((n & (((int64_t) 0xff)      )) << 56) |
				((n & (((int64_t) 0xff) <<  8)) << 40) |
				((n & (((int64_t) 0xff) << 16)) << 24) |
				((n & (((int64_t) 0xff) << 24)) <<  8) |
				((n & (((int64_t) 0xff) << 32)) >>  8) |
				((n & (((int64_t) 0xff) << 40)) >> 24) |
				((n & (((int64_t) 0xff) << 48)) >> 40) |
				((n & (((int64_t) 0xff) << 56)) >> 56) );
}

uint64_t swapu64(uint64_t n)
{
	return (    ((n & (((uint64_t) 0xff)      )) << 56) |
				((n & (((uint64_t) 0xff) <<  8)) << 40) |
				((n & (((uint64_t) 0xff) << 16)) << 24) |
				((n & (((uint64_t) 0xff) << 24)) <<  8) |
				((n & (((uint64_t) 0xff) << 32)) >>  8) |
				((n & (((uint64_t) 0xff) << 40)) >> 24) |
				((n & (((uint64_t) 0xff) << 48)) >> 40) |
				((n & (((uint64_t) 0xff) << 56)) >> 56) );
}
