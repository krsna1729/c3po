/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


#ifndef _BEECRYPT_GNU_H
#define _BEECRYPT_GNU_H

#include <inttypes.h>
/*#include <stdint.h>*/


#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <unistd.h>
#include <dlfcn.h>

typedef pthread_cond_t bc_cond_t;
typedef pthread_mutex_t bc_mutex_t;
typedef pthread_t bc_thread_t;













#if defined(__GNUC__)
# if !defined(__GNUC_PREREQ__)
#  define __GNUC_PREREQ__(maj, min) (__GNUC__ > (maj) || __GNUC__ == (maj) && __GNUC_MINOR__ >= (min))
# endif
#else
# define __GNUC__ 0
# define __GNUC_PREREQ__(maj, min) 0
#endif

/* WARNING: overriding this value is dangerous; some assembler routines
 * make assumptions about the size set by the configure script
 */
#if !defined(MP_WBITS)
# define MP_WBITS	64U
#endif

#endif
