/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*!\file aes.h
 * \brief AES block cipher, as specified by NIST FIPS 197.
 * \author Bob Deblier <bob.deblier@telenet.be>
 * \ingroup BC_m BC_aes_m
 */

#ifndef _AES_H
#define _AES_H

#include "beecrypt.h"
#include "aesopt.h"

/*!\brief Holds all the parameters necessary for the AES cipher.
 * \ingroup BC_aes_m
 */
#ifdef __cplusplus
struct BEECRYPTAPI aesParam
#else
struct _aesParam
#endif
{
	/*!\var k
	 * \brief Holds the key expansion.
	 */
	uint32_t k[64];
	/*!\var nr
	 * \brief Number of rounds to be used in encryption/decryption.
	 */
	uint32_t nr;
	/*!\var fdback
	 * \brief Buffer to be used by block chaining or feedback modes.
	 */
	uint32_t fdback[4];
};

#ifndef __cplusplus
typedef struct _aesParam aesParam;
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*!\var aes
 * \brief Holds the full API description of the AES algorithm.
 */
extern const BEECRYPTAPI blockCipher aes;

/*!\fn int aesSetup(aesParam* ap, const byte* key, size_t keybits, cipherOperation op)
 * \brief This function performs the cipher's key expansion.
 * \param ap The cipher's parameter block.
 * \param key The key value.
 * \param keybits The number of bits in the key; legal values are:
 *  128, 192 and 256.
 * \param op ENCRYPT or DECRYPT.
 * \retval 0 on success.
 * \retval -1 on failure.
 */
BEECRYPTAPI
int			aesSetup   (aesParam* ap, const byte* key, size_t keybits, cipherOperation op);

/*!\fn int aesSetIV(aesParam* ap, const byte* iv)
 * \brief This function sets the Initialization Vector.
 * \note This function is only useful in block chaining or feedback modes.
 * \param ap The cipher's parameter block.
 * \param iv The initialization vector; may be null.
 * \retval 0 on success.
 */
BEECRYPTAPI
int			aesSetIV   (aesParam* ap, const byte* iv);

/*!\fn int aesSetCTR(aesParam* ap, const byte* nivz, size_t counter)
 * \brief This function sets the CTR mode counter.
 * \note This function is only useful in CTR modes.
 * \param ap The cipher's parameter block.
 * \param nivz The concatenation of Nonce, IV, and padding Zeroes.
 * \param counter The counter.
 * \retval 0 on success.
 */
BEECRYPTAPI
int			aesSetCTR  (aesParam* ap, const byte* nivz, size_t counter);

/*!\fn aesEncrypt(aesParam* ap, uint32_t* dst, const uint32_t* src)
 * \brief This function performs the raw AES encryption; it encrypts one block
 *  of 128 bits.
 * \param ap The cipher's parameter block.
 * \param dst The ciphertext; should be aligned on 32-bit boundary.
 * \param src The cleartext; should be aligned on 32-bit boundary.
 * \retval 0 on success.
 */
BEECRYPTAPI
int			aesEncrypt (aesParam* ap, uint32_t* dst, const uint32_t* src);

/*!\fn aesDecrypt(aesParam* ap, uint32_t* dst, const uint32_t* src)
 * \brief This function performs the raw AES decryption; it decrypts one block
 *  of 128 bits.
 * \param ap The cipher's parameter block.
 * \param dst The cleartext; should be aligned on 32-bit boundary.
 * \param src The ciphertext; should be aligned on 32-bit boundary.
 * \retval 0 on success.
 */
BEECRYPTAPI
int			aesDecrypt (aesParam* ap, uint32_t* dst, const uint32_t* src);

BEECRYPTAPI
uint32_t*	aesFeedback(aesParam* ap);

#ifdef __cplusplus
}
#endif

#endif

