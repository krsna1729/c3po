/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*!\file hmacsha256.c
 * \brief HMAC-SHA-256 message digest algorithm.
 * \author Bob Deblier <bob.deblier@pandora.be>
 * \ingroup HMAC_m HMAC_sha256_m
 */

#define BEECRYPT_DLL_EXPORT

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <hmacsha256.h>
#include <beecrypt.h>

/*!\addtogroup HMAC_sha256_m
 * \{
 */

const keyedHashFunction hmacsha256 = {
	"HMAC-SHA-256",
	sizeof(hmacsha256Param),
	64,
	32,
	64,
	512,
	32,
	(keyedHashFunctionSetup) hmacsha256Setup,
	(keyedHashFunctionReset) hmacsha256Reset,
	(keyedHashFunctionUpdate) hmacsha256Update,
	(keyedHashFunctionDigest) hmacsha256Digest
};

int hmacsha256Setup (hmacsha256Param* sp, const byte* key, size_t keybits)
{
	return hmacSetup(sp->kxi, sp->kxo, &sha256, &sp->sparam, key, keybits);
}

int hmacsha256Reset (hmacsha256Param* sp)
{
	return hmacReset(sp->kxi, &sha256, &sp->sparam);
}

int hmacsha256Update(hmacsha256Param* sp, const byte* data, size_t size)
{
	return hmacUpdate(&sha256, &sp->sparam, data, size);
}

int hmacsha256Digest(hmacsha256Param* sp, byte* data)
{
	return hmacDigest(sp->kxo, &sha256, &sp->sparam, data);
}

/*!\}
 */
