/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*!\file mpopt.h
 * \brief Multi-precision integer optimization definitions.
 * \author Bob Deblier <bob.deblier@pandora.be>
 * \ingroup MP_m
 */

#ifndef _MPOPT_H
#define _MPOPT_H

#if WIN32
# if __MWERKS__ && __INTEL__
# elif defined(_MSC_VER) && defined(_M_IX86)
#  define ASM_MPZERO
#  define ASM_MPFILL
#  define ASM_MPEVEN
#  define ASM_MPODD
#  define ASM_MPADDW
#  define ASM_MPSUBW
#  define ASM_MPADD
#  define ASM_MPSUB
#  define ASM_MPMULTWO
#  define ASM_MPDIVTWO
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
# endif
#endif

#if defined(__DECC)
# if defined(OPTIMIZE_ALPHA)
#  define ASM_MPADD
#  define ASM_MPSUB
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
# endif
#endif

#if defined(__GNUC__)
# if defined(OPTIMIZE_ALPHA)
#  define ASM_MPADD
#  define ASM_MPSUB
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
# elif defined(OPTIMIZE_ARM)
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
# elif defined(OPTIMIZE_I386) || defined(OPTIMIZE_I486) || defined(OPTIMIZE_I586) || defined(OPTIMIZE_I686)
#  define ASM_MPZERO
#  define ASM_MPFILL
#  define ASM_MPEVEN
#  define ASM_MPODD
#  define ASM_MPADD
#  define ASM_MPADDW
#  define ASM_MPSUB
#  define ASM_MPSUBW
#  define ASM_MPMULTWO
#  define ASM_MPDIVTWO
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
#  define ASM_MPPNDIV
# elif defined(OPTIMIZE_IA64)
#  define ASM_MPZERO
#  define ASM_MPCOPY
#  define ASM_MPADD
#  define ASM_MPSUB
#  define ASM_MPMULTWO
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
# elif defined(OPTIMIZE_M68K)
#  define ASM_MPADD
#  define ASM_MPSUB
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
# elif defined(OPTIMIZE_POWERPC) || defined(OPTIMIZE_POWERPC64)
#  define ASM_MPSETMUL
#  define ASM_MPADD
#  define ASM_MPADDW
#  define ASM_MPSUB
#  define ASM_MPSUBW
#  define ASM_MPMULTWO
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
# elif defined(OPTIMIZE_S390X)
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
# elif defined(OPTIMIZE_SPARCV8)
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
# elif defined(OPTIMIZE_SPARCV8PLUS)
#  define ASM_MPADDW
#  define ASM_MPSUBW
#  define ASM_MPADD
#  define ASM_MPSUB
#  define ASM_MPMULTWO
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
# elif defined(OPTIMIZE_X86_64)
#  define ASM_MPZERO
#  define ASM_MPFILL
#  define ASM_MPEVEN
#  define ASM_MPODD
#  define ASM_MPADD
#  define ASM_MPADDW
#  define ASM_MPSUB
#  define ASM_MPSUBW
#  define ASM_MPDIVTWO
#  define ASM_MPMULTWO
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
# endif
#endif

#if defined(__IBMC__)
# if defined(OPTIMIZE_POWERPC) || defined(OPTIMIZE_POWERPC64)
#  define ASM_MPSETMUL
#  define ASM_MPADDW
#  define ASM_MPSUBW
#  define ASM_MPADD
#  define ASM_MPSUB
#  define ASM_MPMULTWO
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
# endif
#endif

#if defined(__INTEL_COMPILER)
# if defined(OPTIMIZE_I386) || defined(OPTIMIZE_I486) || defined(OPTIMIZE_I586) || defined(OPTIMIZE_I686)
#  define ASM_MPZERO
#  define ASM_MPFILL
#  define ASM_MPEVEN
#  define ASM_MPODD
#  define ASM_MPADDW
#  define ASM_MPSUBW
#  define ASM_MPADD
#  define ASM_MPSUB
#  define ASM_MPMULTWO
#  define ASM_MPDIVTWO
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
#  define ASM_MPPNDIV
# endif
#endif

#if defined(__SUNPRO_C) || defined(__SUNPRO_CC)
# if defined(OPTIMIZE_SPARCV8)
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
# elif defined(OPTIMIZE_SPARCV8PLUS)
#  define ASM_MPADDW
#  define ASM_MPSUBW
#  define ASM_MPADD
#  define ASM_MPSUB
#  define ASM_MPMULTWO
#  define ASM_MPSETMUL
#  define ASM_MPADDMUL
#  define ASM_MPADDSQRTRC
# endif
#endif

#endif
