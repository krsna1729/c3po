/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*!\file hmacsha256.h
 * \brief HMAC-SHA-256 message authentication code, headers.
 * \author Bob Deblier <bob.deblier@pandora.be>
 * \ingroup HMAC_m HMAC_sha256_m
 */

#ifndef _HMACSHA256_H
#define _HMACSHA256_H

#include "hmac.h"
#include "sha256.h"

/*!\ingroup HMAC_sha256_m
 */
typedef struct
{
	sha256Param sparam;
	byte kxi[64];
	byte kxo[64];
} hmacsha256Param;

#ifdef __cplusplus
extern "C" {
#endif

extern BEECRYPTAPI const keyedHashFunction hmacsha256;

BEECRYPTAPI
int hmacsha256Setup (hmacsha256Param*, const byte*, size_t);
BEECRYPTAPI
int hmacsha256Reset (hmacsha256Param*);
BEECRYPTAPI
int hmacsha256Update(hmacsha256Param*, const byte*, size_t);
BEECRYPTAPI
int hmacsha256Digest(hmacsha256Param*, byte*);

#ifdef __cplusplus
}
#endif

#endif
