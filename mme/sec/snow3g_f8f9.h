/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef __SNOW_3G_F8F9__
#define __SNOW_3G_F8F9__ 1

extern void f8Snow3g(u8 *key, u32 count, u32 bearer, u32 dir, u8 *data, u32 length);
extern u32  f9Snow3g(u8 *key, u32 count, u32 fresh,  u32 dir, u8 *data, u32 length);

#endif
