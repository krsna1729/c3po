/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*!\file hmac.c
 * \brief HMAC algorithm.
 *
 * \see RFC2104 HMAC: Keyed-Hashing for Message Authentication.
 *                    H. Krawczyk, M. Bellare, R. Canetti.
 *
 * \author Bob Deblier <bob.deblier@pandore.be>
 * \ingroup HMAC_m
 */

#define BEECRYPT_DLL_EXPORT

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <hmac.h>
#include <endianness.h>

/*!\addtogroup HMAC_m
 * \{
 */

#define HMAC_IPAD	0x36
#define HMAC_OPAD	0x5c

int hmacSetup(byte* kxi, byte* kxo, const hashFunction* hash, hashFunctionParam* param, const byte* key, size_t keybits)
{
	register unsigned int i;

	size_t keybytes = keybits >> 3;

	/* if the key is too large, hash it first */
	if (keybytes > hash->blocksize)
	{
		/* if the hash digest is too large, this doesn't help; this is really a sanity check */
		if (hash->digestsize > hash->blocksize)
			return -1;

		if (hash->reset(param))
			return -1;

		if (hash->update(param, key, keybytes))
			return -1;

		if (hash->digest(param, kxi))
			return -1;

		memcpy(kxo, kxi, keybytes = hash->digestsize);
	}
	else if (keybytes > 0)
	{
		memcpy(kxi, key, keybytes);
		memcpy(kxo, key, keybytes);
	}
	else
		return -1;

	for (i = 0; i < keybytes; i++)
	{
		kxi[i] ^= HMAC_IPAD;
		kxo[i] ^= HMAC_OPAD;
	}

	for (i = keybytes; i < hash->blocksize; i++)
	{
		kxi[i] = HMAC_IPAD;
		kxo[i] = HMAC_OPAD;
	}

	return hmacReset(kxi, hash, param);
}

int hmacReset(const byte* kxi, const hashFunction* hash, hashFunctionParam* param)
{
	if (hash->reset(param))
		return -1;
	if (hash->update(param, kxi, hash->blocksize))
		return -1;

	return 0;
}

int hmacUpdate(const hashFunction* hash, hashFunctionParam* param, const byte* data, size_t size)
{
	return hash->update(param, data, size);
}

int hmacDigest(const byte* kxo, const hashFunction* hash, hashFunctionParam* param, byte* data)
{
	if (hash->digest(param, data))
		return -1;
	if (hash->update(param, kxo, hash->blocksize))
		return -1;
	if (hash->update(param, data, hash->digestsize))
		return -1;
	if (hash->digest(param, data))
		return -1;

	return 0;
}

/*!\}
 */
