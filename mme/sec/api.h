/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*!\file api.h
 * \brief BeeCrypt API, portability headers.
 * \author Bob Deblier <bob.deblier@pandora.be>
 */

#ifndef _BEECRYPT_API_H
#define _BEECRYPT_API_H

#if defined(_WIN32) && !defined(WIN32)
# define WIN32 1
#endif

#if WIN32
# if !__CYGWIN32__
#  include <win.h>
# else
#  include <gnu.h>
# endif
# ifdef BEECRYPT_DLL_EXPORT
#  define BEECRYPTAPI __declspec(dllexport)
# else
#  define BEECRYPTAPI __declspec(dllimport)
# endif
# ifdef BEECRYPT_CXX_DLL_EXPORT
#  define BEECRYPTCXXAPI __declspec(dllexport)
#  define BEECRYPTCXXTEMPLATE
# else
#  define BEECRYPTCXXAPI __declspec(dllimport)
#  define BEECRYPTCXXTEMPLATE extern
# endif
#else
# include <gnu.h>
# define BEECRYPTAPI
# define BEECRYPTCXXAPI
#endif

#ifndef ROTL32
# define ROTL32(x, s) (((x) << (s)) | ((x) >> (32 - (s))))
#endif
#ifndef ROTR32
# define ROTR32(x, s) (((x) >> (s)) | ((x) << (32 - (s))))
#endif
#ifndef ROTR64
# define ROTR64(x, s) (((x) >> (s)) | ((x) << (64 - (s))))
#endif

typedef uint8_t		byte;

typedef int8_t		javabyte;
typedef int16_t		javashort;
typedef int32_t		javaint;
typedef int64_t		javalong;

typedef uint16_t	javachar;

#if (MP_WBITS == 64)
typedef uint64_t	mpw;
typedef uint32_t	mphw;
#elif (MP_WBITS == 32)
# if HAVE_UINT64_T
#  define HAVE_MPDW 1
typedef uint64_t	mpdw;
# endif
typedef uint32_t	mpw;
typedef uint16_t	mphw;
#else
# error
#endif

#endif
