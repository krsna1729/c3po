/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter Convergence Layer

     Type:     C source file

     Desc:     freeDiameter initialization.

     File:     aqfd_ai.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */
#include "cm_os.h"

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */
#include "cm_os.x"

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"

#include "lsy.h"
#include "lsy.x"

#include <freeDiameter/freeDiameter-host.h>
#include <freeDiameter/libfdcore.h>
#include <freeDiameter/libfdproto.h>

#include "aqfd.h"
#include "aqfd.x"

#include "sy.h"
#include "sy.x"
#include "sy_err.h"

PRIVATE S16 aqfdParseAuthenticationInfo ARGS((struct avp *avp, SyAIA *aia));

/*
 *  Thread local Pst related variables and init routine
 */
static __thread int _pstInitialized;
static __thread Pst _pst;

static void _initPst()
{
   if (!_pstInitialized)
   {
      memcpy( &_pst, &aqCb.cfg.syPst, sizeof(_pst) ); 
      _pstInitialized = 1;
   }
}

/*
*
*       Fun:    aqfdSendAIR
*
*       Desc:   Perform general configuration of freeDiameter. Must be done
*               after aqActvInit() is called, but before any other
*               interaction with Diameter.
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*               LAQ_REASON_FD_CORE_INITIALIZE_FAIL  - failure
*               LAQ_REASON_FD_CORE_PARSECONF_FAIL   - failure
*               LAQ_REASON_FD_CORE_START_FAIL       - failure
*
*       Notes:  None
*
*       File:   aqfd_init.c
*
*      The Authentication-Information-Request (AIR) command, indicated by
*      the Command-Code field set to 318 and the 'R'
*      bit set in the Command Flags field, is sent from MME or SGSN to HSS.
*
*      < Authentication-Information-Request> ::= < Diameter Header: 318, REQ, PXY, 16777251 >
*         < Session-Id >
*         [ Vendor-Specific-Application-Id ]
*         { Auth-Session-State }
*         { Origin-Host }
*         { Origin-Realm }
*         [ Destination-Host ]
*         { Destination-Realm }
*         { User-Name }
*        *[ Supported-Features ]
*         [ Requested-EUTRAN-Authentication-Info ]
*         [ Requested-UTRAN-GERAN-Authentication-Info ]
*         { Visited-PLMN-Id }
*        *[ AVP ]
*        *[ Proxy-Info ]
*        *[ Route-Record ]
*/
#ifdef ANSI
PUBLIC S16 aqfdSendAIR
(
VbMmeUeCb *ueCb,
U8 *auts
)
#else
PUBLIC S16 aqfdSendAIR(ueCb, U8 *autn)
VbMmeUeCb *ueCb;
U8 *autn;
#endif
{
   struct avp *avp;
   struct msg *msg;
   S8 imsi[VB_HSS_IMSI_LEN + 1];

   aqfdImsi2Str(ueCb->ueCtxt.ueImsi, ueCb->ueCtxt.ueImsiLen, imsi);

   /* create new session id */
   AQCHECK_FCT_2( aqfdCreateSessionId(ueCb) );

   /* construct the message */
   AQCHECK_MSG_NEW( aqDict.cmdAIR, msg );

   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_Session_Id, msg, MSG_BRW_LAST_CHILD, ueCb->ueCtxt.ueHssCtxt.sessId );
   AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Auth_Session_State, msg, MSG_BRW_LAST_CHILD, 1 );
   AQCHECK_MSG_ADD_ORIGIN( msg );
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_Destination_Host, msg, MSG_BRW_LAST_CHILD, vbSmCb.cfgCb.hssHost );
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_Destination_Realm, msg, MSG_BRW_LAST_CHILD, vbSmCb.cfgCb.hssRealm );
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_User_Name, msg, MSG_BRW_LAST_CHILD, imsi );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Visited_PLMN_Id, msg, MSG_BRW_LAST_CHILD, ueCb->ueCtxt.tai.plmnId.plmnId, 3 );

   AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Requested_EUTRAN_Authentication_Info, msg, MSG_BRW_LAST_CHILD, avp );
   AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Number_Of_Requested_Vectors, avp, MSG_BRW_LAST_CHILD, 1 );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Immediate_Response_Preferred, avp, MSG_BRW_LAST_CHILD, 0 );

   if (auts != NULL)
      AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Re_Synchronization_Info, avp, MSG_BRW_LAST_CHILD, auts, VB_AUTS_SIZE );

AQFD_DUMP_MESSAGE(msg);

   /* send the message */
   AQCHECK_MSG_SEND( &msg, NULL, NULL );

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfd_aia_cb
*
*       Desc:   Authentication-Info-Answer call back
*
*       Ret:    0
*
*       Notes:  None
*
*       File:   aqfd_ai.c
*
*/
#ifdef ANSI
PUBLIC int aqfd_aia_cb
(
struct msg ** msg,
struct avp * pavp,
struct session * sess,
void * data,
enum disp_action * act
)
#else
PUBLIC int aqfd_aia_cb(msg, pavp, sess, data, act)
struct msg ** msg,
struct avp * pavp,
struct session * sess,
void * data,
enum disp_action * act
#endif
{
   S16 ret;
   S8 imsi[VB_HSS_IMSI_LEN + 1];
   struct msg   *ans = NULL;
   struct msg   *qry = NULL;
   struct avp   *avp = NULL;
   SyAIA     	*aia = NULL;
#ifdef PERFORMANCE_TIMING
   stimer_t     tmp_time;
   stimer_t     tmp_cb_start;
#endif

   ans = *msg;

AQFD_DUMP_MESSAGE(ans);

   /* retrieve the original query associated with the answer */
   CHECK_FCT (fd_msg_answ_getq (ans, &qry));

   /* allocate the aia message */
   aia = (SyAIA*)malloc(sizeof(*aia));

   memset((void*)aia, 0, sizeof(*aia));

   /* retrieve the IMSI from the original request */
   AQCHECK_MSG_GET_AVP_STR(qry, aqDict.avp_User_Name, imsi, sizeof(imsi), goto err);

   aqfdImsi2Binary(imsi, aia->imsi.val, &aia->imsi.len);

   /* get Result-Code */
   AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_Result_Code, avp, );
   if (avp)
   {
      /* Result-Code found */
      AQCHECK_AVP_GET_U32(aqDict.avp_Result_Code, avp, aia->result, );
      if (!aia->result)
      {
         aia->result = ER_DIAMETER_UNABLE_TO_COMPLY;
         goto fini1;
      }
   }
   else
   {
      AqExperimentalResult er;

      /* Result-Code not found, check for Experimental-Result */
      AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_Experimental_Result, avp, );
      if (!avp)
      {
         aia->result = ER_DIAMETER_INVALID_AVP_VALUE;
         goto fini1;
      }

      if (aqfdParseExperimentalResult(avp, &er) != LCM_REASON_NOT_APPL)
      {
         aia->result = ER_DIAMETER_INVALID_AVP_VALUE;
         goto fini1;
      }

      aia->result = er.result_code;
   }

   if (aia->result == ER_DIAMETER_SUCCESS)
   {
      /* get Authentication-Info */
      AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_Authentication_Info, avp, );
      if (avp)
      {
         if (aqfdParseAuthenticationInfo(avp, aia) != LCM_REASON_NOT_APPL)
            aia->result = ER_DIAMETER_INVALID_AVP_VALUE;
      }
      else
      {
         aia->result = ER_DIAMETER_MISSING_AVP;
      }
   }

   goto fini1;

err:
   free(aia);
   goto fini2;

fini1:
   /* notify SY of the arrival if the AIA */
   _initPst();
   _pst.event = EVTLSYAIA;
   {
      Buffer *buf = NULL;
      if((ret = SGetMsg(aqCb.cfg.syPst.region, aqCb.cfg.syPst.pool, &buf)) != ROK)
         LOG_E("Error %d returned from SGetMsg() sending AIA notification", ret);
      else
      {
         cmPkPtr((PTR)aia, buf);
#ifdef PERFORMANCE_TIMING
         STIMER_GET_CURRENT_TP(tmp_time);
         aia->aia_resp_time = tmp_time;
#endif
         if ((ret = SPstTsk(&_pst, buf)) != ROK)
         {
            LOG_E("Error %d returned from SPstTsk() sending AIA notification", ret);
            SPutMsg(buf);
         }
      }
   }

fini2:
   fd_msg_free( *msg );
   *msg = NULL;
   return 0;
}

/*
*
*       Fun:    aqfdParseAuthenticationInfo
*
*       Desc:
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*
*       Notes:  None
*
*       File:   aqfd_ai.c
*
*/
#ifdef ANSI
PRIVATE S16 aqfdParseAuthenticationInfo
(
struct avp *avp,
SyAIA *aia
)
#else
PRIVATE S16 aqfdParseAuthenticationInfo(avp, aia)
struct avp *avp;
SyAIA *aia;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   AQCHECK_FCT(fd_msg_avp_hdr(avp, &hdr), LAQ_REASON_FD_MSG_AVP_HDR);
   if (hdr->avp_code != aqDict.davp_Authentication_Info.avp_code)
      return LAQ_REASON_FD_E_UTRAN_VECTOR;

   /* find the first E-UTRAN-Vector avp */
   AQCHECK_FCT(fd_msg_browse(avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr(child_avp, &hdr);

      if (hdr->avp_code == aqDict.davp_E_UTRAN_Vector.avp_code)
         break;

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   if (!child_avp)
      return LAQ_REASON_FD_MISSING_E_UTRAN_VECTOR;

   /* iterate through the E-UTRAN-Vector child avp's */
   AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if (hdr->avp_code == aqDict.davp_RAND.avp_code) {
         if (hdr->avp_value->os.len > sizeof(aia->vector.rand.val))
            return LAQ_REASON_FD_RAND_INVALID_LENGTH;
         aia->vector.rand.len = hdr->avp_value->os.len;
         memcpy(aia->vector.rand.val, hdr->avp_value->os.data, aia->vector.rand.len);
      } else if (hdr->avp_code == aqDict.davp_XRES.avp_code) {
         if (hdr->avp_value->os.len > sizeof(aia->vector.xres.val))
            return LAQ_REASON_FD_XRES_INVALID_LENGTH;
         aia->vector.xres.len = hdr->avp_value->os.len;
         memcpy(aia->vector.xres.val, hdr->avp_value->os.data, aia->vector.xres.len);
      } else if (hdr->avp_code == aqDict.davp_AUTN.avp_code) {
         if (hdr->avp_value->os.len > sizeof(aia->vector.autn.val))
            return LAQ_REASON_FD_XRES_INVALID_LENGTH;
         aia->vector.autn.len = hdr->avp_value->os.len;
         memcpy(aia->vector.autn.val, hdr->avp_value->os.data, aia->vector.autn.len);
      } else if (hdr->avp_code == aqDict.davp_KASME.avp_code) {
         if (hdr->avp_value->os.len > sizeof(aia->vector.kasme.val))
            return LAQ_REASON_FD_XRES_INVALID_LENGTH;
         aia->vector.kasme.len = hdr->avp_value->os.len;
         memcpy(aia->vector.kasme.val, hdr->avp_value->os.data, aia->vector.kasme.len);
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision History:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

