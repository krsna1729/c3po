/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:    EPC DNS Convergence Layer

     Type:    C source file

     Desc:    Upper and Management Interface primitives.

     File:    ds.c

     Sid:

     Prg:     bw

*********************************************************************21*/


/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"
#include "lsy.h"
#include "lsy.x"

#include "aqfd.h"
#include "aqfd.x"

#include "sy.h"
#include "sy.x"
#include "sy_err.h"
#include "sy2.x"

#include "lds.h"
#include "lds.x"
#include "ds_err.h"
#include "ds.h"
#include "ds.x"

#define DSSWMV 1            /* Diameter - main version */
#define DSSWMR 0            /* Diameter - main revision */
#define DSSWBV 0            /* Diameter - branch version */
#define DSSWBR 0            /* Diameter - branch revision */

#define DSSWPN "1000161"    /* Diameter - part number */

PRIVATE CONSTANT SystemId sId ={
   DSSWMV,                    /* main version */
   DSSWMR,                    /* main revision */
   DSSWBV,                    /* branch version */
   DSSWBR,                    /* branch revision */
   DSSWPN,                    /* part number */
};
PRIVATE S16 dsCfgGen ARGS((DsGenCfg *dsGen));


/* public variable declarations */

PUBLIC DsCb  dsCb;         /* Epc DNS control block */


/* interface function to system service */


/*
*
*       Fun:    syActvInit
*
*       Desc:   Called from SSI to initialize Diameter.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef SS_MULTIPLE_PROCS
#ifdef ANSI
PUBLIC S16 dsActvInit
(
ProcId procId,         /* procId */
Ent entity,            /* entity */
Inst inst,             /* instance */
Region region,         /* region */
Reason reason,         /* reason */
Void **xxCb           /* Protocol Control Block */
)
#else
PUBLIC S16 dsActvInit(procId,entity, inst, region, reason, xxCb)
ProcId procId;         /* procId */
Ent entity;            /* entity */
Inst inst;             /* instance */
Region region;         /* region */
Reason reason;         /* reason */
Void **xxCb;           /* Protocol Control Block */
#endif
#else /* SS_MULTIPLE_PROCS */
#ifdef ANSI
PUBLIC S16 dsActvInit
(
Ent entity,            /* entity */
Inst inst,             /* instance */
Region region,         /* region */
Reason reason          /* reason */
)
#else
PUBLIC S16 dsActvInit(entity, inst, region, reason)
Ent entity;            /* entity */
Inst inst;             /* instance */
Region region;         /* region */
Reason reason;         /* reason */
#endif
#endif /* SS_MULTIPLE_PROCS */
{
   TRC2(dsActvInit);

#ifdef SS_MULTIPLE_PROCS
   DSDBGP(DBGMASK_SI, (dsCb.init.prntBuf,
          "dsActvInit(ProcId(%d), Ent(%d), Inst(%d), Region(%d), Reason(%d))\n",
           procId, entity, inst, region, reason));
#else /* SS_MULTIPLE_PROCS */
   DSDBGP(DBGMASK_SI, (dsCb.init.prntBuf,
          "dsActvInit(Ent(%d), Inst(%d), Region(%d), Reason(%d))\n",
           entity, inst, region, reason));
   DS_ZERO(&dsCb, sizeof(DsCb));
#endif /* SS_MULTIPLE_PROCS */

   /* initialize dsCb */

   dsCb.init.ent        = entity;
   dsCb.init.inst       = inst;
   dsCb.init.region     = region;
   dsCb.init.reason     = reason;
   dsCb.init.cfgDone    = FALSE;
   dsCb.init.pool       = 0;
#ifdef SS_MULTIPLE_PROCS
   dsCb.init.procId = procId;
#else /* SS_MULTIPLE_PROCS */
   dsCb.init.procId = SFndProcId();
#endif /* SS_MULTIPLE_PROCS */
   dsCb.init.acnt       = FALSE;
   dsCb.init.usta       = TRUE;
   dsCb.init.trc        = FALSE;

#ifdef DEBUGP
   dsCb.init.dbgMask    = 0;
#ifdef DS_DEBUG
   dsCb.init.dbgMask = 0xFFFFFFFF;
#endif
#endif

   /* Used at DS---->UZ */
   dsCb.uzPst.dstProcId = SFndProcId();
   dsCb.uzPst.srcProcId = SFndProcId();
   dsCb.uzPst.dstEnt    = (Ent)ENTUZ;
   dsCb.uzPst.dstInst   = (Inst)0;
   dsCb.uzPst.srcEnt    = (Ent)ENTDS;
   dsCb.uzPst.srcInst   = (Inst)0;
   dsCb.uzPst.prior     = (Prior)VBSM_MSGPRIOR;
   dsCb.uzPst.route     = (Route)RTESPEC;
   dsCb.uzPst.event     = (Event)EVTNONE;
   dsCb.uzPst.region    = (Region)vbSmCb.init.region;
   dsCb.uzPst.pool      = (Pool)vbSmCb.init.region;
   dsCb.uzPst.selector  = (Selector)VBSM_DSSMSEL;

   RETVALUE(ROK);
} /* end of dsActvInit */

/*
*
*       Fun:    DsMiLdsCfgReq
*
*       Desc:   Configure the layer. Responds with a DsMiLdsCfgCfm
*               primitive.
*
*       Ret:    ROK     - ok
*
*       Notes:  
*
*       File:   ds.c
*
*/
#ifdef ANSI
PUBLIC S16 DsMiLdsCfgReq
(
Pst             *pst,           /* post structure */
DsMngmt         *cfg            /* configuration structure */
)
#else
PUBLIC S16 DsMiLdsCfgReq(pst, cfg)
Pst             *pst;           /* post structure */
DsMngmt         *cfg;           /* configuration structure */
#endif
{
   DsMngmt      cfmMsg;
   S16          ret;


   TRC3(DsMiLdsCfgReq);

#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&dsCbPtr)) !=
      ROK)
   {
      DSLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,EDS004,(ErrVal)0,pst->dstInst,
            "DsMiLdsCfgReq() failed, cannot derive dsCb");
      RETVALUE(FALSE);
   }
   DSDBGP(DBGMASK_MI, (dsCb.init.prntBuf,
      "---------EPC DNS------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   DSDBGP(DBGMASK_MI, (dsCb.init.prntBuf,
          "DsMiLdsCfgReq(pst, elmnt(%d))\n",
           cfg->hdr.elmId.elmnt));

   DS_ZERO(&cfmMsg, sizeof (DsMngmt));

   if (!dsCb.init.cfgDone)
      syCb.init.lmPst.intfVer = pst->intfVer;

   switch (cfg->hdr.elmId.elmnt)
   {
      case STGEN:            /* general configuration */
         ret = dsCfgGen(&cfg->t.cfg.s.dsGen);
         break;

      default:               /* invalid */
         ret = LCM_REASON_INVALID_ELMNT;
         break;
   }

   /* issue configuration confirm */
   dsSendLmCfm(pst, TCFG, &cfg->hdr, (U16)(ret == LCM_REASON_NOT_APPL ?
               LCM_PRIM_OK : LCM_PRIM_NOK), (U16)ret, &cfmMsg);

   RETVALUE(ROK);
} /* DsMiLdsCfgReq() */


/*
*
*       Fun:    DsMiLds:StsReq
*
*       Desc:   Get statistics information. Statistics are
*               returned by a DsMiLdsStsCfm primitive. The
*               statistics counters can be reset using the action
*               parameter:
*                  ZEROSTS      - zero statistics counters
*                  NOZEROSTS    - do not zero statistics counters
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   ds.c
*
*/
#ifdef ANSI
PUBLIC S16 DsMiLdsStsReq
(
Pst             *pst,           /* post structure */
Action          action,         /* action to be done */
DsMngmt         *sts            /* statistics structure */
)
#else
PUBLIC S16 DsMiLdsStsReq(pst, action, sts)
Pst             *pst;           /* post structure */
Action          action;         /* action to be done */
DsMngmt         *sts;           /* statistics structure */
#endif
{
   DsMngmt      cfmMsg;

   TRC3(DsMiLdsStsReq);
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&dsCbPtr)) !=
      ROK)
   {
      DSLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt, EDS005,(ErrVal)0,pst->dstInst,
            "DsMiLdwStsReq() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   DSDBGP(DBGMASK_MI, (dsCb.init.prntBuf,
      "---------EPC DNS------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   DSDBGP(DBGMASK_MI, (dsCb.init.prntBuf,
          "DsMiLdsStsReq(pst, action(%d), elmnt(%d))\n",
           action, sts->hdr.elmId.elmnt));
   DSDBGP(DBGMASK_SI, (dsCb.init.prntBuf,
          "SGetDateTime(DateTime(%p))\n", (Ptr)&cfmMsg.t.sts.dt));

   /* initialize the confirm structure */
   DS_ZERO(&cfmMsg, sizeof (DsMngmt));
   SGetDateTime(&cfmMsg.t.sts.dt);

   if(!dsCb.init.cfgDone)
   {
      DSDBGP(DBGMASK_LYR, (dsCb.init.prntBuf,
         "DsMiLdsStsReq(): general configuration not done\n"));
      dsSendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_NOK, LCM_REASON_GENCFG_NOT_DONE,
               &cfmMsg);

      RETVALUE(ROK);
   }

   switch (sts->hdr.elmId.elmnt)
   {
      case STGEN:            /* general statistics */
         dsGetGenSts(&cfmMsg.t.sts.s.genSts);
         if (action == ZEROSTS)
            dsZeroGenSts();
         break;

      default:
         dsSendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_NOK,
                     LCM_REASON_INVALID_ELMNT, &cfmMsg);
         RETVALUE(ROK);
   }

   /* issue a statistics confirm */
   dsSendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_OK, LCM_REASON_NOT_APPL,
               &cfmMsg);

   RETVALUE(ROK);
} /* DsMiLdsStsReq() */


/*
*
*       Fun:    DsMiLdsCntrlReq
*
*       Desc:   Control the specified element: enable or diable
*               trace and alarm (unsolicited status) generation,
*               delete or disable a SAP or a group of SAPs, enable
*               or disable debug printing.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 DsMiLdsCntrlReq
(
Pst             *pst,           /* post structure */
DsMngmt         *ctl            /* pointer to control structure */
)
#else
PUBLIC S16 DsMiLdsCntrlReq(pst, ctl)
Pst             *pst;           /* post structure */
DsMngmt         *ctl;           /* pointer to control structure */
#endif
{
   Header       *hdr;
   DsMngmt      cfmMsg;
   S16          ret;

   TRC3(DsMiLdsCntrlReq);

/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&syCbPtr)) !=
      ROK)
   {
      DSLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,EDS006,(ErrVal)0,pst->dstInst,
            "DsMiLdsCntrlReq() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   DSDBGP(DBGMASK_MI, (syCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */
   DSDBGP(DBGMASK_MI, (syCb.init.prntBuf,
          "DsMiLdsCntrlReq(pst, elmnt(%d))\n", ctl->hdr.elmId.elmnt));
   DSDBGP(DBGMASK_SI, (syCb.init.prntBuf,
          "SGetDateTime(DateTime(%p))\n", (Ptr)&ctl->t.cntrl.dt));

   hdr = &ctl->hdr;
   DS_ZERO(&cfmMsg, sizeof (DsMngmt));
   SGetDateTime(&(ctl->t.cntrl.dt));

  /*hi002.105 - Check if Stack is Configured */
   if((!syCb.init.cfgDone) && (hdr->elmId.elmnt != STGEN))
   {
      DSDBGP(DBGMASK_LYR, (syCb.init.prntBuf,
         "DsMiLdsCntrlReq(): general configuration not done\n"));
      /* issue a control confirm */
      /* hi003.105 to send cntrl cfm if gen cfg not done */
      dsSendLmCfm(pst, TCNTRL, hdr, LCM_PRIM_NOK, LCM_REASON_GENCFG_NOT_DONE,
               &cfmMsg);

      RETVALUE(ROK);
   }

   switch (hdr->elmId.elmnt)
   {
      case STGEN:
         ret = dsCntrlGen(pst, ctl, hdr);
         break;

      default:
         DSLOGERROR_INT_PAR(EDS007, hdr->elmId.elmnt, 0,
            "DsMiLdsCntrlReq(): bad element in control request");
         ret = LCM_REASON_INVALID_ELMNT;
         break;
   }

   if (ret == LSY_REASON_OPINPROG)
   {
      dsSendLmCfm(pst, TCNTRL, hdr, LCM_PRIM_OK_NDONE,
                  LCM_REASON_NOT_APPL, &cfmMsg);
      RETVALUE(ROK);
   }

   /* issue a control confirm primitive */
   dsSendLmCfm(pst, TCNTRL, hdr, (U16)(ret == LCM_REASON_NOT_APPL ?
               LCM_PRIM_OK : LCM_PRIM_NOK), (U16)ret, &cfmMsg);

   RETVALUE(ROK);
} /* DsMiLdsCntrlReq() */


/*
*
*       Fun:    DsMiLdsStaReq
*
*       Desc:   Get status information. Responds with a
*               DsMiLdsStaCfm primitive.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 DsMiLdsStaReq
(
Pst             *pst,           /* post structure */
DsMngmt         *sta            /* management structure */
)
#else
PUBLIC S16 DsMiLdsStaReq(pst, sta)
Pst             *pst;           /* post structure */
DsMngmt         *sta;           /* management structure */
#endif
{
   Header       *hdr;
   DsMngmt      cfmMsg;

   TRC3(DsMiLdsStaReq);

#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&dsCbPtr)) !=
      ROK)
   {
      DSLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,EDS009,(ErrVal)0,pst->dstInst,
            "DsMiLdsStaReq() failed, cannot derive dsCb");
      RETVALUE(FALSE);
   }
   DSDBGP(DBGMASK_MI, (dsCb.init.prntBuf,
      "---------EPC DNS------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   DSDBGP(DBGMASK_MI, (dsCb.init.prntBuf, "DsMiLdsStaReq(pst, elmnt(%d))\n",
           sta->hdr.elmId.elmnt));

   hdr = &(sta->hdr);
   DS_ZERO(&cfmMsg, sizeof (DsMngmt));

   /* hi002.105 - fill the date and time */
   SGetDateTime(&cfmMsg.t.ssta.dt);

   switch (hdr->elmId.elmnt)
   {
      case STSID:               /* system ID */
         syGetSid(&cfmMsg.t.ssta.s.sysId);
         break;

      default:
         DSLOGERROR_INT_PAR(EDS010, hdr->elmId.elmnt, 0,
            "DsMiLdsStaReq(): invalid element in status request");
         dsSendLmCfm(pst, TSSTA, hdr, LCM_PRIM_NOK,
                     LCM_REASON_INVALID_ELMNT, &cfmMsg);
         RETVALUE(ROK);
   }

   /* issue a status confirm primitive */
   dsSendLmCfm(pst, TSSTA, hdr, LCM_PRIM_OK, LCM_REASON_NOT_APPL,
               &cfmMsg);

   RETVALUE(ROK);
} /* DsMiLdsStaReq() */

/*
*
*       Fun:    dsCfgGen
*
*       Desc:   Perform general configuration of Diameter. Must be done
*               after dsActvInit() is called, but before any other
*               interaction with Diameter. Reserves static memory for
*               the layer with SGetSMem(), allocates required
*               memory, sets up data structures and starts the
*               receive mechanism (threads/timer/permanent task).
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*               LSY_REASON_FD_CORE_INITIALIZE_FAIL  - failure
*               LSY_REASON_FD_CORE_PARSECONF_FAIL   - failure
*               LSY_REASON_FD_CORE_START_FAIL       - failure
*
*       Notes:  None
*
*       File:   ds.c
*
*/
#ifdef ANSI
PUBLIC S16 dsCfgGen
(
DsGenCfg        *dsGen          /* management structure */
)
#else
PUBLIC S16 dsCfgGen(dsGen)
DsGenCfg        *dsGen;         /* management structure */
#endif
{
   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    dsSendLmCfm
*
*       Desc:   Sends configuration, control, statistics and status
*               confirms to the layer manager.
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   ds.c
*
*/
#ifdef ANSI
PUBLIC Void dsSendLmCfm
(
Pst             *pst,           /* post */
U8              cfmType,        /* confirm type */
Header          *hdr,           /* header */
U16             status,         /* confirm status */
U16             reason,         /* failure reason */
DsMngmt         *cfm            /* management structure */
)
#else
PUBLIC Void dsSendLmCfm(pst, cfmType, hdr, status, reason, cfm)
Pst             *pst;           /* post */
U8              cfmType;        /* confirm type */
Header          *hdr;           /* header */
U16             status;         /* confirm status */
U16             reason;         /* failure reason */
DsMngmt         *cfm;           /* management structure */
#endif
{
   Pst          cfmPst;         /* post structure for confimation */

   TRC2(dsSendLmCfm);

   DS_ZERO(&cfmPst, sizeof (Pst));

   cfm->hdr.elmId.elmnt = hdr->elmId.elmnt;
   cfm->hdr.transId     = hdr->transId;

   cfm->cfm.status = status;
   cfm->cfm.reason = reason;

   /* fill up post for confirm */
   cfmPst.srcEnt        = dsCb.init.ent;
   cfmPst.srcInst       = dsCb.init.inst;
   cfmPst.srcProcId     = dsCb.init.procId;
   cfmPst.dstEnt        = pst->srcEnt;
   cfmPst.dstInst       = pst->srcInst;
   cfmPst.dstProcId     = pst->srcProcId;
   cfmPst.selector      = hdr->response.selector;
   cfmPst.prior         = hdr->response.prior;
   cfmPst.route         = hdr->response.route;
   cfmPst.region        = hdr->response.mem.region;
   cfmPst.pool          = hdr->response.mem.pool;

   switch (cfmType)
   {
      case TCFG:
         DsMiLdsCfgCfm(&cfmPst, cfm);
         break;

      case TSTS:
         DsMiLdsStsCfm(&cfmPst, cfm);
         break;

      case TCNTRL:
         DsMiLdsCntrlCfm(&cfmPst, cfm);
         break;

      case TSSTA:
         DsMiLdsStaCfm(&cfmPst, cfm);
         break;

      default:
         DSDBGP(DBGMASK_LYR, (syCb.init.prntBuf,
                  "dsSendLmCfm(): unknown parameter cfmType\n"));
         break;
   }

   RETVOID;
} /* dsSendLmCfm() */

/*
*
*       Fun:    dsGetGenSts
*
*       Desc:   Get general statistics.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  Statistics counters are sampled unlocked.
*
*       File:   ds.c
*
*/
#ifdef ANSI
PUBLIC S16 dsGetGenSts
(
DsGenSts        *genSts         /* statistics returned */
)
#else
PUBLIC S16 dsGetGenSts(genSts)
DsGenSts        *genSts;        /* statistics returned */
#endif
{
   TRC2(dsGetGenSts);

   DS_ZERO(genSts, sizeof (DsGenSts));

   /* get receive counters from each group */
   genSts->numErrors = 0;

   RETVALUE(ROK);
} /* dsGetGenSts() */


/*
*
*       Fun:    dsZeroGenSts
*
*       Desc:   Reset general statistics.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 dsZeroGenSts
(
Void
)
#else
PUBLIC S16 dsZeroGenSts()
#endif
{
   TRC2(dsZeroGenSts);

   /* zero the error stats */
   DS_ZERO_ERRSTS();

   RETVALUE(ROK);
} /* dsZeroGenSts() */

/*
*
*       Fun:    syCntrlGen
*
*       Desc:   General control requests are used to enable/disable
*               alarms or debug printing and also to shut down
*               Diameter.
*
*       Ret:    LCM_REASON_NOT_APPL             - ok
*               LSY_REASON_OPINPROG             - ok, in progress
*               LCM_REASON_INVALID_ACTION       - failure
*               LCM_REASON_INVALID_SUBACTION    - failure
*               LSY_REASON_DIFF_OPINPROG        - failure
*
*       Notes:  None
*
*       File:   ds.c
*
*/
#ifdef ANSI
PUBLIC S16 dsCntrlGen
(
Pst             *pst,           /* post structure */
DsMngmt         *cntrl,         /* control request */
Header          *hdr            /* header */
)
#else
PUBLIC S16 dsCntrlGen(pst, cntrl, hdr)
Pst             *pst;           /* post structure */
DsMngmt         *cntrl;         /* control request */
Header          *hdr;           /* header */
#endif
{
   S16          ret;
   U8           action, subAction;
   Bool         invSubAction = FALSE;

   TRC2(dsCntrlGen);

   action = cntrl->t.cntrl.action;
   subAction = cntrl->t.cntrl.subAction;

   switch (action)
   {
      case ASHUTDOWN:
         ret = ROK;
         RETVALUE(LCM_REASON_NOT_APPL);


      case AENA:
         if (subAction == SAUSTA)
            dsCb.init.usta = TRUE;
#ifdef DEBUGP
         else if (subAction == SADBG)
            dsCb.init.dbgMask |= cntrl->t.cntrl.ctlType.dsDbg.dbgMask;
#endif
         else
            invSubAction = TRUE;
         break;


      case ADISIMM:
         if (subAction == SAUSTA)
            dsCb.init.usta = FALSE;
#ifdef DEBUGP
         else if (subAction == SADBG)
            dsCb.init.dbgMask &= ~(cntrl->t.cntrl.ctlType.dsDbg.dbgMask);
#endif
         else
            invSubAction = TRUE;
         break;


      default:
         DSLOGERROR_INT_PAR(EDS011, (ErrVal)action, 0,
               "syCntrlGen(): Unknown or unsupported action");
         RETVALUE(LCM_REASON_INVALID_ACTION);
   }

   if (invSubAction)
   {
      DSLOGERROR_INT_PAR(EDS012, (ErrVal)subAction, 0,
            "syCntrlGen(): invalid sub-action specified");
      RETVALUE(LCM_REASON_INVALID_SUBACTION);
   }

   RETVALUE(LCM_REASON_NOT_APPL);
} /* dsCntrlGen() */

/*
*
*       Fun:   dsGetSid
*
*       Desc:  Get system id consisting of part number, main version and
*              revision and branch version and branch.
*
*       Ret:   TRUE      - ok
*
*       Notes: None
*
*       File:  ds.c
*
*/
#ifdef ANSI
PUBLIC S16 dsGetSid
(
SystemId *s                 /* system id */
)
#else
PUBLIC S16 dsGetSid(s)
SystemId *s;                /* system id */
#endif
{
   TRC2(dsGetSid)

   s->mVer = sId.mVer;
   s->mRev = sId.mRev;
   s->bVer = sId.bVer;
   s->bRev = sId.bRev;
   s->ptNmb = sId.ptNmb;

   RETVALUE(TRUE);

} /* syGetSid */

/*
*
*       Fun:    dsSendAlarm
*
*       Desc:   Send an unsolicited status indication (alarm) to
*               layer manager.
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   ds.c
*
*/
#ifdef ANSI
PUBLIC Void dsSendAlarm
(
U16             cgy,            /* alarm category */
U16             evnt,           /* event */
U16             cause,          /* cause for alarm */
DsAlarmInfo     *info           /* alarm information */
)
#else
PUBLIC Void dsSendAlarm(cgy, evnt, cause, info)
U16             cgy;            /* alarm category */
U16             evnt;           /* event */
U16             cause;          /* cause for alarm */
DsAlarmInfo     *info;          /* alarm information */
#endif
{
   DsMngmt      sm;             /* Management structure */

   TRC2(sySendAlarm);

   /* do nothing if unconfigured */
   if (!dsCb.init.cfgDone)
   {
      DSDBGP(DBGMASK_LYR, (dsCb.init.prntBuf,
               "dsSendAlarm(): general configuration not done\n"));
      RETVOID;
   }

   /* send alarms only if configured to do so */
   if (dsCb.init.usta)
   {
      DS_ZERO(&sm, sizeof (DsMngmt));

      sm.hdr.elmId.elmnt        = TUSTA;
      sm.hdr.elmId.elmntInst1   = DS_UNUSED;
      sm.hdr.elmId.elmntInst2   = DS_UNUSED;
      sm.hdr.elmId.elmntInst3   = DS_UNUSED;

      sm.t.usta.alarm.category  = cgy;
      sm.t.usta.alarm.event     = evnt;
      sm.t.usta.alarm.cause     = cause;

      cmMemcpy((U8 *)&sm.t.usta.info,
               (U8 *)info, sizeof (SyAlarmInfo));

      (Void)SGetDateTime(&sm.t.usta.alarm.dt);

#ifdef SY_MULTI_THREADED
   SY_LOCK(&dsCb.lmPstLock);
#endif
      DsMiLdsStaInd(&(dsCb.init.lmPst), &sm);
#ifdef SY_MULTI_THREADED
   SY_UNLOCK(&dsCb.lmPstLock);
#endif
   }

   RETVOID;
} /* dsSendAlarm() */

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/
