/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

/********************************************************************20**

     Name:     Diameter SGD convergence Layer

     Type:     C include file

     Desc:     Defines and macros used by Diameter.

     File:     sx.h

     Sid:

     Prg:      rakesh kumar suman

*********************************************************************21*/


#ifndef __SXH__
#define __SXH__

#define SXLAYERNAME     "SGD_CONVERGENCE_LAYER"

#define EVTINTGOACTV            42


/* miscellaneous defines */
#define SX_PRNTBUF_SIZE         128     /* size of print buffer */
#define SX_UNUSED               -1      /* for all unused fields */
#define SX_SRVCTYPE_MASK        4

/* Macros.
 */

/* DEBUGP */
#define SXDBGP(_msgClass, _arg)                                 \
   DBGP(&(sxCb.init), SXLAYERNAME, _msgClass, _arg)

#define SX_DBG_INFO(_arg)                                       \
   DBGP(&(sxCb.init), SXLAYERNAME, LSX_DBGMASK_INFO, _arg)

/* zero out a buffer */
#define SX_ZERO(_str, _len)                                     \
   cmMemset((U8 *)_str, 0, _len);

/* allocate and zero out a static buffer */
#define SX_ALLOC(_size, _datPtr)                                \
{                                                               \
   S16 _ret;                                                    \
   _ret = SGetSBuf(sxCb.init.region, sxCb.init.pool,            \
                   (Data **)&_datPtr, _size);                   \
   if (_ret == ROK)                                             \
      cmMemset((U8*)_datPtr, 0, _size);                         \
   else                                                         \
      _datPtr = NULLP;                                          \
}

/* free a static buffer */
#define SX_FREE(_size, _datPtr)                                 \
   SPutSBuf(sxCb.init.region, sxCb.init.pool,                   \
            (Data *)_datPtr, _size);

/* statistics */
#if (defined(SX_MULTI_THREADED)  &&  defined(SX_STS_LOCKS))

#define SX_INC_ERRSTS(_stat)                                    \
{                                                               \
   SX_LOCK(&sxCb.errStsLock);                                   \
   _stat++;                                                     \
   SX_UNLOCK(&sxCb.errStsLock);                                 \
}

#define SX_INC_TXSTS(_sap, _stat)                               \
{                                                               \
   //SX_LOCK(&_sap->txStsLock);                                   \
   //_stat++;                                                     \
   //SX_UNLOCK(&_sap->txStsLock);                                 \
}

/*hi028.201: Handled statistics properly*/
#define SX_DEC_TXSTS(_sap, _stat)                               \
{                                                               \
   //SX_LOCK(&_sap->txStsLock);                                   \
   //_stat--;                                                     \
   //SX_UNLOCK(&_sap->txStsLock);                                 \
}

#endif

/* statistics */
#if (defined(SX_MULTI_THREADED)  &&  defined(SX_STS_LOCKS))

#define SX_INC_ERRSTS(_stat)                                    \
{                                                               \
   SX_LOCK(&sxCb.errStsLock);                                   \
   _stat++;                                                     \
   SX_UNLOCK(&sxCb.errStsLock);                                 \
}

#define HI_INC_TXSTS(_stat)                                     \
{                                                               \
   SX_LOCK(&sxCb.txStsLock);                                    \
   _stat++;                                                     \
   SX_UNLOCK(&sxCb.txStsLock);                                  \
}

/*hi028.201: Handled statistics properly*/
#define HI_DEC_TXSTS(__stat)                                    \
{                                                               \
   SX_LOCK(&sxCb.txStsLock);                                   \
   _stat--;                                                     \
   SX_UNLOCK(&sxCb.txStsLock);                                 \
}

#define SX_ADD_TXSTS(_stat, _val)                         \
{                                                               \
   SX_LOCK(&sxCb.txStsLock);                                   \
   _stat += _val;                                               \
   HI_UNLOCK(&sxCb.txStsLock);                                 \
}

#define SX_ZERO_ERRSTS()                                        \
{                                                               \
   SX_LOCK(&sxCb.errStsLock);                                   \
   SX_ZERO(&sxCb.errSts, sizeof(SxErrSts));                     \
   SX_UNLOCK(&sxCb.errStsLock);                                 \
}

#define SX_ZERO_TXSTS()                                         \
{                                                               \
   SX_LOCK(&sxCb.txStsLock);                                    \
   sxCb.txSts.numTxMsg = 0;                                     \
   SX_UNLOCK(&sxCb.txStsLock);                                  \
}

#else  /* SX_MULTI_THREADED  &&  SX_STS_LOCKS */


#define SX_INC_ERRSTS(_stat)                                    \
   _stat++;

#define SX_INC_TXSTS(_stat)                                     \
   _stat++;

#define SX_DEC_TXSTS(_stat)                                     \
   _stat--;

#define SX_ADD_TXSTS(_stat, _val)                               \
   _stat += _val;

#define SX_ZERO_ERRSTS()                                        \
   SX_ZERO(&sxCb.errSts, sizeof(SxErrSts));

#define SX_ZERO_TXSTS()                                         \
   sxCb.txSts.numTxMsg = 0;

#define SX_TRC2(_arg)                                                   \
Txt __tapa_fun[PRNTSZE];                                                \
sprintf(__tapa_fun,#_arg);                                              \
{                                                                       \
   if (sxCb.init.dbgMask & (LVB_DBGMASK_TRC))                        \
   {                                                                    \
      VB_PRNTTIMESTAMP(sxCb.init)                                    \
      VB_PRNT_LAYER(sxCb.init, VB_LYR_NAME)                                 \
      VB_PRNT_FILE_LINE(sxCb.init)                                  \
      VBPRNT(sxCb.init, (sxCb.init.prntBuf," Entering\n"));                     \
      TRC2(_arg);                                                       \
   }                                                                    \
}


#endif /* SX_MULTI_THREADED  &&  SX_STS_LOCKS */


#endif /* __SXH__ */


/********************************************************************30**

         End of file:

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      rks   1. initial release.
*********************************************************************91*/

