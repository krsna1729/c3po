/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/



/********************************************************************20**

     Name:     Diameter

     Type:     C header file

     Desc:     Structures, variables, typedefs and prototypes
               required at the management interface.

     File:     laq.x

     Sid:

     Prg:      bw

*********************************************************************21*/



#ifndef __LAQX__
#define __LAQX__

#ifdef __cplusplus
extern "C" {
#endif

/* General configuration.
 */
typedef struct aqGenCfg
{
   Pst      lmPst;          /* for layer manager */
   Pst      swPst;          /* for T6A layer */
   Pst      sxPst;          /* for SGD layer */
   Pst      syPst;          /* for S6A layer */

   Region       initRegion;
   Pool         initPool;
} AqGenCfg;

/* General statistics.
 */
typedef struct aqGenSts
{
   StsCntr      numErrors;     /* num of errors */
   StsCntr      numAqReqMsg;    /* num of transmitted diameter messages */
   StsCntr      numAqAnsMsg;    /* num of received diameter messages */
} AqGenSts;

typedef struct aqSta
{
   State        state;          /* diameter state */
} AqSta;

/* Unsolicited status indication.
 */
typedef struct aqAlarmInfo
{
   SpId         spId;           /* sap Id */
   U8           type;           /* which member of inf is present */
   union
   {
      State     state;          /* TSAP state */
      State     conState;       /* connection state */
      Mem       mem;            /* region/pool (resource related) */
      U8        parType;        /* parameter type */
   } inf;

} AqAlarmInfo;

#ifdef DEBUGP
/* Debug control.
 */
typedef struct aqDbgCntrl
{
   U32          dbgMask;        /* Debug mask */

} AqDbgCntrl;
#endif


/* Trace control.
 */
typedef struct aqTrcCntrl
{
   SpId         sapId;          /* sap Id */
   S16          trcLen;         /* length to trace */

} AqTrcCntrl;



/* Configuration.
 */
typedef struct aqCfg
{
   union
   {
      AqGenCfg  aqGen;          /* general configuration */
   } s;

} AqCfg;


/* Statistics.
 */
typedef struct aqSts
{
   DateTime     dt;             /* date and time */
   Duration     dura;           /* duration */

   union
   {
      AqGenSts  genSts;         /* general statistics */
   } s;

} AqSts;
/* Solicited status.
 */
typedef struct aqSsta
{
   DateTime     dt;             /* date and time */

   union
   {
      SystemId  sysId;          /* system id */
      AqSta  aqSta;             /* diameter status */
   } s;

} AqSsta;

/* Unsolicited status.
 */
typedef struct aqUsta
{
   CmAlarm      alarm;          /* alarm */
   AqAlarmInfo  info;           /* alarm information */

} AqUsta;


/* Trace.
 */
typedef struct aqTrc
{
   DateTime     dt;            /* date and time */
   U16          evnt;          /* event */
} AqTrc;

/* Control.
 */
typedef struct aqCntrl
{
   DateTime     dt;             /* date and time */
   U8           action;         /* action */
   U8           subAction;      /* sub action */

   union
   {
      AqTrcCntrl        trcDat;         /* trace length */
      ProcId            dstProcId;      /* destination procId */
      Route             route;          /* route */
      Priority          priority;       /* priority */

#ifdef DEBUGP
      AqDbgCntrl        aqDbg;          /* debug printing control */
#endif

   } ctlType;

} AqCntrl;

/* Management.
 */
typedef struct aqMngmt
{
   Header       hdr;            /* header */
   CmStatus     cfm;            /* response status/confirmation */

   union
   {
      AqCfg     cfg;            /* configuration */
      AqSts     sts;            /* statistics */
      AqSsta    ssta;           /* solicited status */
      AqUsta    usta;           /* unsolicited status */
      AqTrc     trc;            /* trace */
      AqCntrl   cntrl;          /* control */

   } t;

} AqMngmt;



/* Layer management interface primitive types.
 */
typedef S16  (*LaqCfgReq)       ARGS((Pst *pst, AqMngmt *cfg));
typedef S16  (*LaqCfgCfm)       ARGS((Pst *pst, AqMngmt *cfg));
typedef S16  (*LaqCntrlReq)     ARGS((Pst *pst, AqMngmt *cntrl));
typedef S16  (*LaqCntrlCfm)     ARGS((Pst *pst, AqMngmt *cntrl));
typedef S16  (*LaqStsReq)       ARGS((Pst *pst, Action action,
                                      AqMngmt *sts));
typedef S16  (*LaqStsCfm)       ARGS((Pst *pst, AqMngmt *sts));
typedef S16  (*LaqStaReq)       ARGS((Pst *pst, AqMngmt *sta));
typedef S16  (*LaqStaInd)       ARGS((Pst *pst, AqMngmt *sta));
typedef S16  (*LaqStaCfm)       ARGS((Pst *pst, AqMngmt *sta));
typedef S16  (*LaqTrcInd)       ARGS((Pst *pst, AqMngmt *trc,
                                      Buffer *mBuf));


/* Layer management interface primitives.
 */
//#ifdef AQ
EXTERN S16  AqMiLaqCfgReq       ARGS((Pst *pst, AqMngmt *cfg));
EXTERN S16  AqMiLaqCfgCfm       ARGS((Pst *pst, AqMngmt *cfg));
EXTERN S16  AqMiLaqCntrlReq     ARGS((Pst *pst, AqMngmt *cntrl));
EXTERN S16  AqMiLaqCntrlCfm     ARGS((Pst *pst, AqMngmt *cntrl));
EXTERN S16  AqMiLaqStsReq       ARGS((Pst *pst, Action action,
                                      AqMngmt *sts));
EXTERN S16  AqMiLaqStsCfm       ARGS((Pst *pst, AqMngmt *sts));
EXTERN S16  AqMiLaqStaReq       ARGS((Pst *pst, AqMngmt *sta));
EXTERN S16  AqMiLaqStaCfm       ARGS((Pst *pst, AqMngmt *sta));
EXTERN S16  AqMiLaqStaInd       ARGS((Pst *pst, AqMngmt *sta));
EXTERN S16  AqMiLaqTrcInd       ARGS((Pst *pst, AqMngmt *trc,
                                      Buffer *mBuf));
//#endif /* AQ */

#ifdef SM
EXTERN S16  SmMiLaqCfgReq       ARGS((Pst *pst, AqMngmt *cfg));
EXTERN S16  SmMiLaqCfgCfm       ARGS((Pst *pst, AqMngmt *cfg));
EXTERN S16  SmMiLaqCntrlReq     ARGS((Pst *pst, AqMngmt *cntrl));
EXTERN S16  SmMiLaqCntrlCfm     ARGS((Pst *pst, AqMngmt *cntrl));
EXTERN S16  SmMiLaqStsReq       ARGS((Pst *pst, Action action,
                                      AqMngmt *sts));
EXTERN S16  SmMiLaqStsCfm       ARGS((Pst *pst, AqMngmt *sts));
EXTERN S16  SmMiLaqStaReq       ARGS((Pst *pst, AqMngmt *sta));
EXTERN S16  SmMiLaqStaInd       ARGS((Pst *pst, AqMngmt *sta));
EXTERN S16  SmMiLaqStaCfm       ARGS((Pst *pst, AqMngmt *sta));
EXTERN S16  SmMiLaqTrcInd       ARGS((Pst *pst, AqMngmt *trc,
                                      Buffer *mBuf));
#endif /* SM */


/* Packing and unpacking functions.
 */
EXTERN S16  cmPkLaqCfgReq       ARGS((Pst *pst, AqMngmt *cfg));
EXTERN S16  cmPkLaqCfgCfm       ARGS((Pst *pst, AqMngmt *cfg));
EXTERN S16  cmPkLaqCntrlReq     ARGS((Pst *pst, AqMngmt *cntrl));
EXTERN S16  cmPkLaqCntrlCfm     ARGS((Pst *pst, AqMngmt *cntrl));
EXTERN S16  cmPkLaqStsReq       ARGS((Pst *pst, Action action, AqMngmt *sts));
EXTERN S16  cmPkLaqStsCfm       ARGS((Pst *pst, AqMngmt *sts));
EXTERN S16  cmPkLaqStaReq       ARGS((Pst *pst, AqMngmt *sta));
EXTERN S16  cmPkLaqStaInd       ARGS((Pst *pst, AqMngmt *sta));
EXTERN S16  cmPkLaqTrcInd       ARGS((Pst *pst, AqMngmt *trc, Buffer *mBuf));
EXTERN S16  cmPkLaqStaCfm       ARGS((Pst *pst, AqMngmt *sta));

EXTERN S16  cmUnpkLaqCfgReq     ARGS((LaqCfgReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLaqCfgCfm     ARGS((LaqCfgCfm func, Pst *pst,
                                    Buffer *mBuf));
EXTERN S16  cmUnpkLaqCntrlReq   ARGS((LaqCntrlReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLaqCntrlCfm   ARGS((LaqCntrlCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLaqStsReq     ARGS((LaqStsReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLaqStsCfm     ARGS((LaqStsCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLaqStaReq     ARGS((LaqStaReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLaqStaInd     ARGS((LaqStaInd func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLaqStaCfm     ARGS((LaqStaCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLaqTrcInd     ARGS((LaqTrcInd func, Pst *pst,
                                      Buffer *mBuf));
/* Layer manager activation functions.
 */
EXTERN S16  smAqActvTsk         ARGS((Pst *pst, Buffer *mBuf));
EXTERN S16  smAqActvInit        ARGS((Ent ent, Inst inst,
                                      Region region, Reason reason));

#ifdef SS_MULTIPLE_PROCS
EXTERN S16 aqActvInit       ARGS((ProcId procId,
                                  Ent entity,
                                  Inst inst,
                                  Region region,
                                  Reason reason,
                                  Void **xxCb));
#else /* SS_MULTIPLE_PROCS */

EXTERN S16  aqActvInit          ARGS((Ent ent, Inst inst,
                                      Region region, Reason reason));
#endif /* SS_MULTIPLE_PROCS */
EXTERN S16  aqActvTsk           ARGS((Pst *pst, Buffer *mBuf));

#ifdef __cplusplus
}
#endif

#endif /* __LAQX__ */


/********************************************************************30**

         End of file:     lhi.x@@/main/10 - Tue Feb  1 16:04:21 2011

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat     init                  description
----------- --------- ---- -----------------------------------------------
1.1         ---       bw   1. initial release.
*********************************************************************91*/

