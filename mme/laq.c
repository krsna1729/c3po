/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     TCP UDP Convergence Layer

     Type:     C source file

     Desc:     C source code for common packing and un-packing functions for
               layer management interface(LHI) supplied by TRILLIUM.

     File:     laq.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/*
*     This software may be combined with the following TRILLIUM
*     software:
*
*     part no.                      description
*     --------    ----------------------------------------------
*
*/


/* header include files (.h) */
#include "envopt.h"        /* environment options */
#include "envdep.h"        /* environment dependent */
#include "envind.h"        /* environment independent */
#include "gen.h"           /* general layer */
#include "cm_gen.h"        /* common pack/unpack defines */
#include "cm_os.h"
#include "ssi.h"           /* system services */
#include "lhi.h"           /* layer management, HI Layer */

/* header/extern include files (.x) */
#include "gen.x"           /* general layer */
#include "cm_os.x"
#include "ssi.x"           /* system services */
//#include "lhi.x"           /* layer management, HI layer */
#include "laq.h"
#include "laq.x"
/* hi009.104 - added as HI_ZERO macro calls cmMemset which is defined in
 * cm_lib.x */
#include "cm_lib.x"            /* has the prototype of cmMemset() */


/* local defines */

/* local typedefs */

/* local externs */

/* forward references */



/* functions in other modules */

/* public variable declarations */

/* private variable declarations */

#ifdef LCLAQ



/*
*     layer management interface packing functions
*/


/*
*
*       Fun:   Pack Config Request
*
*       Desc:  This function is used to a pack config request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLaqCfgReq
(
Pst *pst,                 /* post structure */
AqMngmt *cfg              /* configuration */
)
#else
PUBLIC S16 cmPkLaqCfgReq(pst, cfg)
Pst *pst;                 /* post structure */
AqMngmt *cfg;             /* configuration */
#endif
{
   Buffer *mBuf;            /* message buffer */

   TRC3(cmPkLaqCfgReq)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch (cfg->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ065, cfg->hdr.elmId.elmnt,
                     "cmPkLaqCfgReq() Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   CMCHKPKLOG(cmPkCmStatus, &cfg->cfm, mBuf, ELAQ002, pst);
   CMCHKPKLOG(cmPkHeader, &cfg->hdr, mBuf, ELAQ003, pst);
   pst->event = (Event)EVTLAQCFGREQ;

/* hi009.104 - fill interface version number in pst */
#ifdef TDS_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LHIIFVER;
#endif /* TDS_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Config Confirm
*
*       Desc:  This function is used to a pack config confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLaqCfgCfm
(
Pst *pst,                 /* post structure */
AqMngmt *cfm              /* confirm */
)
#else
PUBLIC S16 cmPkLaqCfgCfm(pst, cfm)
Pst *pst;                 /* post structure */
AqMngmt *cfm;             /* confirm */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLaqCfgCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   CMCHKPKLOG(cmPkCmStatus, &cfm->cfm, mBuf, ELAQ068, pst);
   CMCHKPKLOG(cmPkHeader,   &cfm->hdr, mBuf, ELAQ069, pst);

   pst->event = EVTLAQCFGCFM;
   RETVALUE(SPstTsk(pst, mBuf));

} /* end of cmPkLaqCfgCfm */


/*
*
*       Fun:   Pack Control Request
*
*       Desc:  This function is used to pack the control request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLaqCntrlReq
(
Pst *pst,                 /* post structure */
AqMngmt *ctl              /* configuration */
)
#else
PUBLIC S16 cmPkLaqCntrlReq(pst, ctl)
Pst *pst;                 /* post structure */
AqMngmt *ctl;             /* configuration */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLaqCntrlReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   switch(ctl->hdr.elmId.elmnt)
   {
      case STGEN:
         break;

      case STTSAP:
         if(ctl->t.cntrl.subAction == SATRC)
         {
            CMCHKPKLOG(cmPkSpId, ctl->t.cntrl.ctlType.trcDat.sapId,
                       mBuf, ELAQ071, pst);
            CMCHKPKLOG(SPkS16, ctl->t.cntrl.ctlType.trcDat.trcLen,
                       mBuf, ELAQ072, pst);
         }
         else if(ctl->t.cntrl.subAction == SAELMNT)
         {
         }
         else
         {
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ074,  ctl->t.cntrl.subAction,
                     "cmPkLaqCntrlReq() : invalid subAction(STTSAP) ");
#endif /* ERRCLS_DEBUG */
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
         }
         break;

      case STGRTSAP:
         if(ctl->t.cntrl.subAction == SAGR_PRIORITY)
         {
            CMCHKPKLOG(cmPkPrior, ctl->t.cntrl.ctlType.priority, mBuf,
                       ELAQ075, pst);
         }
         else if(ctl->t.cntrl.subAction == SAGR_ROUTE)
         {
            CMCHKPKLOG(cmPkProcId, ctl->t.cntrl.ctlType.route, mBuf,
                       ELAQ076, pst);
         }
         else if(ctl->t.cntrl.subAction == SAGR_DSTPROCID)
         {
            CMCHKPKLOG(cmPkProcId, ctl->t.cntrl.ctlType.dstProcId, mBuf,
                       ELAQ077, pst);
         }
         else
         {
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ078, ctl->t.cntrl.subAction,
                     "cmPkLaqCntrlReq() :invalid subAction(STGRTSAP)");
#endif /* ERRCLS_DEBUG */
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
         }
         break;

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ079, ctl->hdr.elmId.elmnt,
                     "cmPkLaqCntrlReq () Failed : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   CMCHKPKLOG(SPkU8,       ctl->t.cntrl.subAction, mBuf, ELAQ080, pst);
   CMCHKPKLOG(SPkU8,       ctl->t.cntrl.action,    mBuf, ELAQ081, pst);
   CMCHKPKLOG(cmPkDateTime, &ctl->t.cntrl.dt,       mBuf, ELAQ082, pst);

   CMCHKPKLOG(cmPkCmStatus, &ctl->cfm, mBuf, ELAQ083, pst);
   CMCHKPKLOG(cmPkHeader,   &ctl->hdr, mBuf, ELAQ084, pst);
   pst->event = (Event)EVTLHICNTRLREQ;

/* hi009.104 - fill interface version number in pst */
#ifdef TDS_ROLL_UPGRADE_SUPPORT
   pst->intfVer = (CmIntfVer) LHIIFVER;
#endif /* TDS_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Control Confirm
*
*       Desc:  This function is used to pack the control confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLaqCntrlCfm
(
Pst *pst,                 /* post structure */
AqMngmt *cfm              /* confirm */
)
#else
PUBLIC S16 cmPkLaqCntrlCfm(pst, cfm)
Pst *pst;                 /* post structure */
AqMngmt *cfm;             /* confirm */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLaqCntrlCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   /* pack status */
   CMCHKPKLOG(cmPkCmStatus, &cfm->cfm, mBuf, ELAQ085, pst);

   /* pack header */
   CMCHKPKLOG(cmPkHeader, &cfm->hdr, mBuf, ELAQ086, pst);

   pst->event = EVTLHICNTRLCFM;
   RETVALUE(SPstTsk(pst, mBuf));

} /* end of cmPkLaqCntrlCfm */



/*
*
*       Fun:   Pack Statistics Request
*
*       Desc:  This function is used to pack the statistics request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLaqStsReq
(
Pst *pst,                 /* post structure */
Action action,            /* action */
AqMngmt *sts              /* statistics */
)
#else
PUBLIC S16 cmPkLaqStsReq(pst, action, sts)
Pst *pst;                 /* post structure */
Action action;            /* action */
AqMngmt *sts;             /* statistics */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLaqStsReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sts->hdr.elmId.elmnt)
   {
      case STGEN:
         break;
      case STTSAP:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ088, sts->hdr.elmId.elmnt,
                     "cmPkLaqStsReq () Failed : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }
   CMCHKPKLOG(cmPkCmStatus, &sts->cfm, mBuf, ELAQ089, pst);
   CMCHKPKLOG(cmPkHeader,   &sts->hdr, mBuf, ELAQ090, pst);
   CMCHKPKLOG(cmPkAction,   action,    mBuf, ELAQ091, pst);
   pst->event = EVTLHISTSREQ;

/* hi009.104 - fill interface version number in pst */
#ifdef TDS_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LHIIFVER;
#endif /* TDS_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLaqStsReq */

/*lhi_c_004.main_1  1. Modified statistics req to include messages
                              Tx/Rx in bytes per con per thread */
#ifdef LHI_THR_STS

/*
*
*       Fun:   Pack connection Statistics
*
*       Desc:  This function is used to pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkAqConSts
(
AqConSts *sts,              /* statistics */
Buffer  *mBuf              /* to hold the packed buffer */
)
#else
PUBLIC S16 cmPkAqConSts(sts, mBuf)
AqConSts *sts;              /* statistics */
Buffer  *mBuf;             /* to hold the packed buffer */
#endif
{
   /*lhi_c_001.main_11: Added TRC macro for cmPkAqConSts()*/
   TRC3(cmPkAqConSts)
   CMCHKPK(cmPkStsCntr, sts->numTxBytes, mBuf);
   CMCHKPK(cmPkStsCntr, sts->numRxBytes, mBuf);

   RETVALUE(ROK);
}/*cmPkAqConSts*/


/*
*
*       Fun:   Pack Con Statistics per thread
*
*       Desc:  This function is used to pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkAqThrConSts
(
AqThrConSts *sts,              /* statistics */
Pst *pst,
Buffer  *mBuf              /* to hold the packed buffer */
)
#else
PUBLIC S16 cmPkAqThrConSts(sts, pst, mBuf)
AqThrConSts *sts;              /* statistics */
Pst *pst;
Buffer  *mBuf;             /* to hold the packed buffer */
#endif
{
   /* this will hold the temporary index value for number of Threads */
   U8 iThrConIndex = 0;

   /*lhi_c_001.main_11: Added TRC macro for cmPkAqThrConSts()*/
   TRC3(cmPkAqThrConSts)
   /* if connection are present in threads, then only pack them */
   if (sts->numCons)
   {
      /* in a loop pack the thread statistics of all commands */
      for (; iThrConIndex < sts->numCons; iThrConIndex++)
      {
         /* lhi_c_001.main_12: Replaced CMCHKPK with CMCHKPKLOG macro */
         CMCHKPKLOG(cmPkAqConSts, &sts->conSts[iThrConIndex], mBuf, ELAQ639, pst);
      }

     SPutSBuf(pst->region, pst->pool, (Data*)sts->conSts,
                 (Size)(sizeof(AqConSts) * sts->numCons));
     sts->conSts = NULLP;
   } /* end of if threads are present */

   /* lhi_c_001.main_12: Replaced CMCHKPK with CMCHKPKLOG macro */
   CMCHKPKLOG(SPkU32, sts->numCons, mBuf, ELAQ640, pst);

   RETVALUE(ROK);
}/*cmPkAqThrConSts*/

#endif /*LHI_THR_STS*/

/*
*
*       Fun:   Pack Statistics Confirm
*
*       Desc:  This function is used to pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLaqStsCfm
(
Pst *pst,                 /* post structure */
AqMngmt *sts              /* statistics */
)
#else
PUBLIC S16 cmPkLaqStsCfm(pst, sts)
Pst *pst;                 /* post structure */
AqMngmt *sts;             /* statistics */
#endif

{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLaqStsCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sts->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      case STTSAP:
      {
         break;
      }

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ126, sts->hdr.elmId.elmnt,
                     "cmPkLaqStsCfm() : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   CMCHKPKLOG(cmPkDuration, &sts->t.sts.dura, mBuf, ELAQ127, pst);
   CMCHKPKLOG(cmPkDateTime, &sts->t.sts.dt, mBuf, ELAQ128, pst);

   CMCHKPKLOG(cmPkCmStatus, &sts->cfm, mBuf, ELAQ129, pst);
   CMCHKPKLOG(cmPkHeader,   &sts->hdr, mBuf, ELAQ130, pst);
   pst->event = (Event)EVTLHISTSCFM;
   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Status Request
*
*       Desc:  This function is used to pack the status request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLaqStaReq
(
Pst *pst,                 /* post structure */
AqMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 cmPkLaqStaReq(pst, sta)
Pst *pst;                 /* post structure */
AqMngmt *sta;             /* solicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLaqStaReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sta->hdr.elmId.elmnt)
   {
      case STSID:
         break;
      case STTSAP:
         break;
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ132, sta->hdr.elmId.elmnt,
                     "cmPkLaqStaReq () : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELAQ133, pst);
   CMCHKPKLOG(cmPkHeader, &sta->hdr, mBuf, ELAQ134, pst);
   pst->event = (Event)EVTLAQSTAREQ;

   /* hi009.104 - fill interface version number in pst */
#ifdef TDS_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LHIIFVER;
#endif /* TDS_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Status Confirm
*
*       Desc:  This function is used to pack the status confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLaqStaCfm
(
Pst *pst,                 /* post structure */
AqMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 cmPkLaqStaCfm(pst, sta)
Pst *pst;                 /* post structure */
AqMngmt *sta;             /* solicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLaqStaCfm)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   switch(sta->hdr.elmId.elmnt)
   {
      case STSID:
      {
         break;
      }
      case STTSAP:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ141, sta->hdr.elmId.elmnt,
                     "cmPkLaqStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   /* date */
   CMCHKPKLOG(cmPkDateTime, &sta->t.ssta.dt, mBuf, ELAQ142, pst);

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELAQ143, pst);
   CMCHKPKLOG(cmPkHeader,   &sta->hdr, mBuf, ELAQ144, pst);
   pst->event = EVTLAQSTACFM;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLaqStaCfm */


/*
*
*       Fun:   Pack Status Indication
*
*       Desc:  This function is used to pack the status indication
*              primitive to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLaqStaInd
(
Pst *pst,                 /* post structure */
AqMngmt *sta              /* unsolicited status */
)
#else
PUBLIC S16 cmPkLaqStaInd(pst, sta)
Pst *pst;                 /* post structure */
AqMngmt *sta;             /* unsolicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLaqStaInd)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   if(sta->t.usta.info.type != LHI_ALARMINFO_TYPE_NTPRSNT)
   {
      switch(sta->t.usta.info.type)
      {
         case LAQ_ALARMINFO_PAR_TYPE:
            break;

         case LAQ_ALARMINFO_MEM_ID:
            break;

         case LAQ_ALARMINFO_CON_STATE:
            break;

         case LAQ_ALARMINFO_SAP_STATE:
            break;

         default:
#if (ERRCLASS & ERRCLS_DEBUG)
            LAQLOGERROR(pst, ELAQ151, sta->t.usta.info.type,
                        "cmPkLaqStaInd () Failed");
#endif
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
      }
   }
   CMCHKPKLOG(SPkU8,   sta->t.usta.info.type, mBuf, ELAQ152, pst);
   CMCHKPKLOG(cmPkSpId,    sta->t.usta.info.spId,  mBuf, ELAQ153, pst);
   CMCHKPKLOG(cmPkCmAlarm, &sta->t.usta.alarm,     mBuf, ELAQ154, pst);

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELAQ155, pst);
   CMCHKPKLOG(cmPkHeader,   &sta->hdr, mBuf, ELAQ156, pst);
   pst->event = (Event)EVTLAQSTAIND;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLaqStaInd */



/*
*
*       Fun:   Pack Trace Indication
*
*       Desc:  This function is used to pack the trace indication
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLaqTrcInd
(
Pst *pst,                 /* post */
AqMngmt *trc,             /* trace */
Buffer  *mBuf             /* message buffer */
)
#else
PUBLIC S16 cmPkLaqTrcInd(pst, trc, mBuf)
Pst *pst;                 /* post */
AqMngmt *trc;             /* trace */
Buffer  *mBuf;            /* message buffer */
#endif
{

   TRC3(cmPkLaqTrcInd)

   if (mBuf == NULLP)
   {
      if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
      {
         RETVALUE(RFAILED);
      }
   }

   CMCHKPKLOG(SPkU16, trc->t.trc.evnt, mBuf, ELAQ158, pst);
   CMCHKPKLOG(cmPkDateTime, &trc->t.trc.dt, mBuf, ELAQ159, pst);

   CMCHKPKLOG(cmPkCmStatus, &trc->cfm, mBuf, ELAQ160, pst);
   CMCHKPKLOG(cmPkHeader,   &trc->hdr, mBuf, ELAQ161, pst);
   pst->event = EVTLAQTRCIND;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLaqTrcInd */


/*
*     layer management interface un-packing functions
*/

/*
*
*       Fun:   Un-pack Config Request
*
*       Desc:  This function is used to a un-pack config request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLaqCfgReq
(
LaqCfgReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf              /* message buffer */
)
#else
PUBLIC S16 cmUnpkLaqCfgReq(func, pst, mBuf)
LaqCfgReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;             /* message buffer */
#endif
{
   AqMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLaqCfgReq)

     /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(AqMngmt));

   /* hi009.104 - call the new MACRO to init struc to all zeros */
   LAQ_ZERO((U8 *)&mgt, sizeof(AqMngmt));

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELAQ162, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELAQ163, pst);

   switch (mgt.hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }

      case STTSAP:
      {
         break;
      }

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LAQLOGERROR(pst, ELAQ228, mgt.hdr.elmId.elmnt,
                     "cmUnpkLaqCfgReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);

   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Config Confirm
*
*       Desc:  This function is used to a un-pack config confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLaqCfgCfm
(
LaqCfgCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLaqCfgCfm(func, pst, mBuf)
LaqCfgCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   AqMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLaqCfgCfm)

   /* hi009.104 - added missing init to 0 */
	/* lhi_c_001.main_8: Modified HI_ZERO to LHI_ZERO */
   LHI_ZERO((U8*)&mgt, sizeof(AqMngmt))

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELAQ229, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELAQ230, pst);

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);

   RETVALUE(ROK);
} /* end of cmUnpkLaqCfgCfm */


/*
*
*       Fun:   Un-pack Control Request
*
*       Desc:  This function is used to un-pack the control request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLaqCntrlReq
(
LaqCntrlReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf              /* message buffer */
)
#else
PUBLIC S16 cmUnpkLaqCntrlReq(func, pst, mBuf)
LaqCntrlReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;             /* message buffer */
#endif
{
   AqMngmt  mgt;          /* configuration */

   TRC3(cmUnpkLaqCntrlReq)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(AqMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELAQ231, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELAQ232, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.cntrl.dt,  mBuf, ELAQ233, pst);
   CMCHKUNPKLOG(SUnpkU8, &mgt.t.cntrl.action,    mBuf, ELAQ234, pst);
   CMCHKUNPKLOG(SUnpkU8, &mgt.t.cntrl.subAction, mBuf, ELAQ235, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
         if(mgt.t.cntrl.subAction == SADBG)
         {
#ifdef DEBUGP
           CMCHKUNPKLOG(SUnpkU32, &(mgt.t.cntrl.ctlType.aqDbg.dbgMask),
                        mBuf, ELAQ236, pst);
#endif
         }
         break;

      case STGRTSAP:
         if(mgt.t.cntrl.subAction == SAGR_PRIORITY)
         {
            CMCHKUNPKLOG(cmUnpkPrior, &(mgt.t.cntrl.ctlType.priority), mBuf,
                         ELAQ241, pst);
         }
         else if(mgt.t.cntrl.subAction == SAGR_ROUTE)
         {
            CMCHKUNPKLOG(cmUnpkRoute, &(mgt.t.cntrl.ctlType.route), mBuf,
                         ELAQ242, pst);
         }
         else if(mgt.t.cntrl.subAction == SAGR_DSTPROCID)
         {
            CMCHKUNPKLOG(cmUnpkProcId, &(mgt.t.cntrl.ctlType.dstProcId), mBuf,
                         ELAQ243, pst);
         }
         else
         {
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ244, mgt.t.cntrl.subAction,
             "cmUnpkLaqCntrlReq () Failed : invalid subAction(STGRTSAP)");
#endif
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
         }
         break;

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ245, mgt.hdr.elmId.elmnt,
                     "cmUnpkLaqCntrlReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Control Confirm
*
*       Desc:  This function is used to un-pack the control confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLaqCntrlCfm
(
LaqCntrlCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLaqCntrlCfm(func, pst, mBuf)
LaqCntrlCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{
   AqMngmt  cfm;            /* configuration */

   TRC3(cmUnpkLaqCntrlCfm)

   CMCHKUNPKLOG(cmUnpkHeader,   &cfm.hdr, mBuf, ELAQ246, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &cfm.cfm, mBuf, ELAQ247, pst);

   (Void)SPutMsg(mBuf);
   (*func)(pst, &cfm);
   RETVALUE(ROK);
} /* end of cmUnpkLaqCntrlCfm */



/*
*
*       Fun:   Un-pack Statistics Request
*
*       Desc:  This function is used to un-pack the statistics request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLaqStsReq
(
LaqStsReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLaqStsReq(func, pst, mBuf)
LaqStsReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   AqMngmt  mgt;            /* configuration */
   /* lhi_c_001.main_11: Fix for Klockworks issue */
   Action   action = 0;         /* action type */

   TRC3(cmUnpkLaqStsReq)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(AqMngmt));
   CMCHKUNPKLOG(cmUnpkAction,   &action,  mBuf, ELAQ248, pst);
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELAQ249, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELAQ250, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
        break;

      case STTSAP:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ252, mgt.hdr.elmId.elmnt,
                     "cmUnpkLaqStsReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, action, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLaqStsReq */

/*lhi_c_004.main_1  1. modified statistics req to include messages
                              Tx/Rx in bytes per con per thread */
#ifdef LHI_THR_STS
/*
*
*       Fun:   Un Pack connection Statistics
*
*       Desc:  This function is used to un pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkAqConSts
(
AqConSts *sts,              /* statistics */
Buffer  *mBuf              /* to hold the packed buffer */
)
#else
PUBLIC S16 cmUnpkAqConSts(sts, mBuf)
AqConSts *sts;              /* statistics */
Buffer  *mBuf;             /* to hold the packed buffer */
#endif
{
   /*lhi_c_001.main_11: Added TRC macro for cmUnpkAqConSts()*/
   TRC3(cmUnpkAqConSts)
   CMCHKUNPK(cmUnpkStsCntr, &sts->numRxBytes, mBuf);

   CMCHKUNPK(cmUnpkStsCntr, &sts->numTxBytes, mBuf);
   RETVALUE(ROK);

}/*cmunPkAqConSts*/

/*
*
*       Fun:   Un Pack connection Statistics
*
*       Desc:  This function is used to un pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkAqThrConSts
(
AqThrConSts *sts,              /* statistics */
Pst *pst,
Buffer  *mBuf              /* to hold the packed buffer */
)
#else
PUBLIC S16 cmUnpkAqThrConSts(sts, pst, mBuf)
AqThrConSts *sts;              /* statistics */
Pst *pst;
Buffer  *mBuf;             /* to hold the packed buffer */
#endif
{

   /* this will hold the temporary index value for number of threads */
   S16 iConIndex = 0;

   S16 retValue = ROK;

   /*lhi_c_001.main_11: Added TRC macro for cmUnpkAqThrConSts()*/
   TRC3(cmUnpkAqThrConSts)
      /* lhi_c_001.main_12: Replaced CMCHKPK with CMCHKPKLOG macro */
   CMCHKUNPKLOG(SUnpkU32, &sts->numCons, mBuf, ELAQ644, pst);

   sts->conSts = NULLP;

   /* unpack only if Connections are present */
   if (sts->numCons)
   {
      if ( (retValue = SGetSBuf(pst->region, pst->pool, (Data** )&sts->conSts,
                               (sizeof(AqConSts) * sts->numCons))) != ROK)
      {
#if (ERRCLASS & ERRCLS_ADD_RES)
         SLogError(pst->srcEnt, pst->srcInst, pst->srcProcId,
                   __FILE__, __LINE__, (ErrCls)ERRCLS_ADD_RES,
                   (ErrVal)ELAQ253, (ErrVal)ERRZERO,
                   "cmUnpkLaqStsCfm: allocating memory for statistics confirm failed");
#endif /*  ERRCLASS & ERRCLS_ADD_RES  */
         RETVALUE(retValue);
      }

      cmMemset( (U8* )sts->conSts, 0, (sizeof(AqConSts) * sts->numCons));

      /* in a loop unpack the statistics of all commands */
      for (iConIndex = sts->numCons - 1; iConIndex >= 0; iConIndex--)
      {
         /* lhi_c_001.main_12: Replaced CMCHKPK with CMCHKPKLOG macro */
         CMCHKUNPKLOG(cmUnpkAqConSts,  &sts->conSts[iConIndex], mBuf, ELAQ645, pst);
      }
   }

   RETVALUE(ROK);
}/*cmunPkAqThrConSts*/

#endif /*LHI_THR_STS*/

/*
*
*       Fun:   Un-pack Statistics Confirm
*
*       Desc:  This function is used to un-pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLaqStsCfm
(
LaqStsCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLaqStsCfm(func, pst, mBuf)
LaqStsCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   AqMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLaqStsCfm)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(AqMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELAQ254, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELAQ255, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.sts.dt,   mBuf, ELAQ256, pst);
   CMCHKUNPKLOG(cmUnpkDuration, &mgt.t.sts.dura, mBuf, ELAQ257, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      case STTSAP:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ293, mgt.hdr.elmId.elmnt,
                     "cmUnpkLaqStsCfm () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Status Request
*
*       Desc:  This function is used to un-pack the status request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLaqStaReq
(
LaqStaReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLaqStaReq(func, pst, mBuf)
LaqStaReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{
   AqMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLaqStaReq)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(AqMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELAQ294, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELAQ295, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STSID:
         break;
      case STTSAP:
         break;
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ297, mgt.hdr.elmId.elmnt,
                     "cmUnpkLaqStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Status Confirm
*
*       Desc:  This function is used to un-pack the status confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLaqStaCfm
(
LaqStaCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLaqStaCfm(func, pst, mBuf)
LaqStaCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   AqMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLaqStaCfm)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(AqMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELAQ298, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELAQ299, pst);
   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.ssta.dt, mBuf, ELAQ300, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STSID:
      {
         break;
      }
      case STTSAP:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELAQ307, mgt.hdr.elmId.elmnt,
                     "cmUnpkLaqStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLaqStaCfm */


/*
*
*       Fun:   Un-pack Status Indication
*
*       Desc:  This function is used to un-pack the status indication
*              primitive to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLaqStaInd
(
LaqStaInd func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLaqStaInd(func, pst, mBuf)
LaqStaInd func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   AqMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLaqStaInd)

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELAQ308, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELAQ309, pst);

   CMCHKUNPKLOG(cmUnpkCmAlarm, &mgt.t.usta.alarm,      mBuf, ELAQ310, pst);
   CMCHKUNPKLOG(cmUnpkSpId,    &mgt.t.usta.info.spId,  mBuf, ELAQ311, pst);
   CMCHKUNPKLOG(SUnpkU8,       &mgt.t.usta.info.type,  mBuf, ELAQ312, pst);

   if(mgt.t.usta.info.type != LHI_ALARMINFO_TYPE_NTPRSNT)
   {
      switch(mgt.t.usta.info.type)
      {
         case LHI_ALARMINFO_SAP_STATE:
            CMCHKUNPKLOG(cmUnpkState, &mgt.t.usta.info.inf.state,
                         mBuf, ELAQ313, pst);
            break;

         case LHI_ALARMINFO_CON_STATE:
            CMCHKUNPKLOG(cmUnpkState, &mgt.t.usta.info.inf.conState,
                       mBuf, ELAQ314, pst);
            break;

         case LHI_ALARMINFO_MEM_ID:
            CMCHKUNPKLOG(cmUnpkPool, &mgt.t.usta.info.inf.mem.pool, mBuf,
                       ELAQ315, pst);
            CMCHKUNPKLOG(cmUnpkRegion, &mgt.t.usta.info.inf.mem.region, mBuf,
                       ELAQ316, pst);
            break;

         case LHI_ALARMINFO_PAR_TYPE:
            CMCHKUNPKLOG(SUnpkU8, &mgt.t.usta.info.inf.parType, mBuf,
                       ELAQ317, pst);
            break;

#ifdef HI_TLS
         case LHI_ALARMINFO_OSSL_ERR:
            CMCHKUNPKLOG(SUnpkS32, &mgt.t.usta.info.inf.osslError, mBuf,
                         ELAQ318, pst);
            break;
#endif
            /*lhi_c_001.main_11:Added new usta.info.type and protected under
             * HI_LKSCTP flag */
#ifdef HI_LKSCTP
         case LHI_ALARMINFO_ASSOC_ERR:
            CMCHKPKLOG(SPkU32, mgt.t.usta.info.assocId, mBuf,
                  ELAQ636, pst);
            CMCHKPKLOG(SPkU16, mgt.t.usta.info.error, mBuf,
                  ELAQ635, pst);
            break;
#endif

         default:
#if (ERRCLASS & ERRCLS_DEBUG)
            LHILOGERROR(pst, ELAQ319, mgt.t.usta.info.type,
                        "cmUnpkLaqStaInd () Failed");
#endif /* ERRCLS_DEBUG */
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
      }/* end of switch */
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLaqStaInd */



/*
*
*       Fun:   Un-pack Trace Indication
*
*       Desc:  This function is used to un-pack the trace indication
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLaqTrcInd
(
LaqTrcInd func,
Pst *pst,                 /* post */
Buffer  *mBuf             /* message buffer */
)
#else
PUBLIC S16 cmUnpkLaqTrcInd(func, pst, mBuf)
LaqTrcInd func;
Pst *pst;                 /* post */
Buffer  *mBuf;            /* message buffer */
#endif
{

   AqMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLaqTrcInd)

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELAQ320, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELAQ321, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.trc.dt, mBuf, ELAQ322, pst);
   CMCHKUNPKLOG(SUnpkU16, &mgt.t.trc.evnt, mBuf, ELAQ323, pst);
   /*lhi_c_002.main_1 (hi023.104) */
	/* lhi_c_001.main_8: Removed warnings and removed hi.h included
	 *                   header file */
#ifdef HI_ENB_SAP_TRC
   CMCHKUNPKLOG(SUnpkU16, (U16*)&mgt.t.trc.sap, mBuf, ELAQ324, pst);
#endif

   (*func)(pst, &mgt, mBuf);
   RETVALUE(ROK);
} /* end of cmUnpkLaqTrcInd */

#endif /* LCLHI */

/********************************************************************30**

         End of file:     lhi.c@@/main/13 - Tue Apr 26 18:11:18 2011

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat     init                  description
----------- --------- ---- -----------------------------------------------
1.1         ---       bw   1. initial release.
*********************************************************************91*/
