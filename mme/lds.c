/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     EPC DNS Convergence Layer

     Type:     C source file

     Desc:     C source code for common packing and un-packing functions for
               layer management interface(LDS).

     File:     lsy.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/*
*     This software may be combined with the following TRILLIUM
*     software:
*
*     part no.                      description
*     --------    ----------------------------------------------
*
*/


/* header include files (.h) */
#include "envopt.h"        /* environment options */
#include "envdep.h"        /* environment dependent */
#include "envind.h"        /* environment independent */
#include "gen.h"           /* general layer */
#include "cm_gen.h"        /* common pack/unpack defines */
#include "cm_os.h"
#include "ssi.h"           /* system services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */

/* header/extern include files (.x) */
#include "gen.x"           /* general layer */
#include "cm_os.x"
#include "ssi.x"           /* system services */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common linrary function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */

#include "epcdns.x"
#include "lds.h"
#include "lds.x"
#include "ds_err.h"
#include "ds.h"
#include "ds.x"

/* hi009.104 - added as HI_ZERO macro calls cmMemset which is defined in
 * cm_lib.x */
#include "cm_lib.x"            /* has the prototype of cmMemset() */


/* local defines */

/* local typedefs */

/* local externs */

/* forward references */



/* functions in other modules */

/* public variable declarations */

/* private variable declarations */

#ifdef LCLDS



/*
*     layer management interface packing functions
*/


/*
*
*       Fun:   Pack Config Request
*
*       Desc:  This function is used to a pack config request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLdsCfgReq
(
Pst *pst,                 /* post structure */
DsMngmt *cfg              /* configuration */
)
#else
PUBLIC S16 cmPkLdsCfgReq(pst, cfg)
Pst *pst;                 /* post structure */
DsMngmt *cfg;             /* configuration */
#endif
{
   Buffer *mBuf;            /* message buffer */

   TRC3(cmPkLdsCfgReq)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch (cfg->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LDSLOGERROR(pst, ELDS001, cfg->hdr.elmId.elmnt,
                     "cmPkLdsCfgReq() Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   CMCHKPKLOG(cmPkCmStatus, &cfg->cfm, mBuf, ELDS002, pst);
   CMCHKPKLOG(cmPkHeader, &cfg->hdr, mBuf, ELDS003, pst);
   pst->event = (Event)EVTLDSCFGREQ;

/* hi009.104 - fill interface version number in pst */
#ifdef TDS_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LDSIFVER;
#endif /* TDS_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Config Confirm
*
*       Desc:  This function is used to a pack config confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLdsCfgCfm
(
Pst *pst,                 /* post structure */
DsMngmt *cfm              /* confirm */
)
#else
PUBLIC S16 cmPkLdsCfgCfm(pst, cfm)
Pst *pst;                 /* post structure */
DsMngmt *cfm;             /* confirm */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLdsCfgCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   CMCHKPKLOG(cmPkCmStatus, &cfm->cfm, mBuf, ELDS004, pst);
   CMCHKPKLOG(cmPkHeader,   &cfm->hdr, mBuf, ELDS005, pst);

   pst->event = EVTLDSCFGCFM;
   RETVALUE(SPstTsk(pst, mBuf));

} /* end of cmPkLdsCfgCfm */


/*
*
*       Fun:   Pack Control Request
*
*       Desc:  This function is used to pack the control request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLdsCntrlReq
(
Pst *pst,                 /* post structure */
DsMngmt *ctl              /* configuration */
)
#else
PUBLIC S16 cmPkLdsCntrlReq(pst, ctl)
Pst *pst;                 /* post structure */
DsMngmt *ctl;             /* configuration */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLdsCntrlReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   switch(ctl->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LDSLOGERROR(pst, ELDS006, ctl->hdr.elmId.elmnt,
                     "cmPkLdsCntrlReq () Failed : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   CMCHKPKLOG(SPkU8,       ctl->t.cntrl.subAction, mBuf, ELDS007, pst);
   CMCHKPKLOG(SPkU8,       ctl->t.cntrl.action,    mBuf, ELDS008, pst);
   CMCHKPKLOG(cmPkDateTime, &ctl->t.cntrl.dt,       mBuf, ELDS009, pst);

   CMCHKPKLOG(cmPkCmStatus, &ctl->cfm, mBuf, ELDS010, pst);
   CMCHKPKLOG(cmPkHeader,   &ctl->hdr, mBuf, ELDS011, pst);
   pst->event = (Event)EVTLDSCNTRLREQ;

/* hi009.104 - fill interface version number in pst */
#ifdef TDS_ROLL_UPGRADE_SUPPORT
   pst->intfVer = (CmIntfVer) LDSIFVER;
#endif /* TDS_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Control Confirm
*
*       Desc:  This function is used to pack the control confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLdsCntrlCfm
(
Pst *pst,                 /* post structure */
DsMngmt *cfm              /* confirm */
)
#else
PUBLIC S16 cmPkLdsCntrlCfm(pst, cfm)
Pst *pst;                 /* post structure */
DsMngmt *cfm;             /* confirm */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLdsCntrlCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   /* pack status */
   CMCHKPKLOG(cmPkCmStatus, &cfm->cfm, mBuf, ELDS012, pst);

   /* pack header */
   CMCHKPKLOG(cmPkHeader, &cfm->hdr, mBuf, ELDS013, pst);

   pst->event = EVTLDSCNTRLCFM;
   RETVALUE(SPstTsk(pst, mBuf));

} /* end of cmPkLdsCntrlCfm */



/*
*
*       Fun:   Pack Statistics Request
*
*       Desc:  This function is used to pack the statistics request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLdsStsReq
(
Pst *pst,                 /* post structure */
Action action,            /* action */
DsMngmt *sts              /* statistics */
)
#else
PUBLIC S16 cmPkLdsStsReq(pst, action, sts)
Pst *pst;                 /* post structure */
Action action;            /* action */
DsMngmt *sts;             /* statistics */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLdsStsReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sts->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LDSLOGERROR(pst, ELDS014, sts->hdr.elmId.elmnt,
                     "cmPkLdsStsReq () Failed : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }
   CMCHKPKLOG(cmPkCmStatus, &sts->cfm, mBuf, ELDS015, pst);
   CMCHKPKLOG(cmPkHeader,   &sts->hdr, mBuf, ELDS016, pst);
   CMCHKPKLOG(cmPkAction,   action,    mBuf, ELDS017, pst);
   pst->event = EVTLDSSTSREQ;

/* hi009.104 - fill interface version number in pst */
#ifdef TDS_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LDSIFVER;
#endif /* TDS_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLdsStsReq */


/*
*
*       Fun:   Pack Statistics Confirm
*
*       Desc:  This function is used to pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLdsStsCfm
(
Pst *pst,                 /* post structure */
DsMngmt *sts              /* statistics */
)
#else
PUBLIC S16 cmPkLdsStsCfm(pst, sts)
Pst *pst;                 /* post structure */
DsMngmt *sts;             /* statistics */
#endif

{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLdsStsCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sts->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LDSLOGERROR(pst, ELDS018, sts->hdr.elmId.elmnt,
                     "cmPkLdsStsCfm() : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   CMCHKPKLOG(cmPkDuration, &sts->t.sts.dura, mBuf, ELDS019, pst);
   CMCHKPKLOG(cmPkDateTime, &sts->t.sts.dt, mBuf, ELDS020, pst);

   CMCHKPKLOG(cmPkCmStatus, &sts->cfm, mBuf, ELDS021, pst);
   CMCHKPKLOG(cmPkHeader,   &sts->hdr, mBuf, ELDS022, pst);
   pst->event = (Event)EVTLDSSTSCFM;
   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Status Request
*
*       Desc:  This function is used to pack the status request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLdsStaReq
(
Pst *pst,                 /* post structure */
DsMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 cmPkLdsStaReq(pst, sta)
Pst *pst;                 /* post structure */
DsMngmt *sta;             /* solicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLdsStaReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sta->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LDSLOGERROR(pst, ELDS023, sta->hdr.elmId.elmnt,
                     "cmPkLdsStaReq () : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELDS024, pst);
   CMCHKPKLOG(cmPkHeader, &sta->hdr, mBuf, ELDS025, pst);
   pst->event = (Event)EVTLDSSTAREQ;

   /* hi009.104 - fill interface version number in pst */
#ifdef TDS_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LDSIFVER;
#endif /* TDS_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Status Confirm
*
*       Desc:  This function is used to pack the status confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLdsStaCfm
(
Pst *pst,                 /* post structure */
DsMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 cmPkLdsStaCfm(pst, sta)
Pst *pst;                 /* post structure */
DsMngmt *sta;             /* solicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLdsStaCfm)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   switch(sta->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LDSLOGERROR(pst, ELDS026, sta->hdr.elmId.elmnt,
                     "cmPkLdsStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   /* date */
   CMCHKPKLOG(cmPkDateTime, &sta->t.ssta.dt, mBuf, ELDS027, pst);

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELDS028, pst);
   CMCHKPKLOG(cmPkHeader,   &sta->hdr, mBuf, ELDS029, pst);
   pst->event = EVTLDSSTACFM;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLdsStaCfm */


/*
*
*       Fun:   Pack Status Indication
*
*       Desc:  This function is used to pack the status indication
*              primitive to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLdsStaInd
(
Pst *pst,                 /* post structure */
DsMngmt *sta              /* unsolicited status */
)
#else
PUBLIC S16 cmPkLdsStaInd(pst, sta)
Pst *pst;                 /* post structure */
DsMngmt *sta;             /* unsolicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLdsStaInd)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   if(sta->t.usta.info.type != LDS_ALARMINFO_TYPE_NTPRSNT)
   {
      switch(sta->t.usta.info.type)
      {
         case STGEN:
         {
            break;
         }
         default:
#if (ERRCLASS & ERRCLS_DEBUG)
            LDSLOGERROR(pst, ELDS030, sta->t.usta.info.type,
                        "cmPkLdsStaInd () Failed");
#endif
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
      }
   }
   CMCHKPKLOG(SPkU8,   sta->t.usta.info.type, mBuf, ELDS031, pst);
   CMCHKPKLOG(cmPkSpId,    sta->t.usta.info.spId,  mBuf, ELDS032, pst);
   CMCHKPKLOG(cmPkCmAlarm, &sta->t.usta.alarm,     mBuf, ELDS033, pst);

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELDS034, pst);
   CMCHKPKLOG(cmPkHeader,   &sta->hdr, mBuf, ELDS035, pst);
   pst->event = (Event)EVTLDSSTAIND;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLdsStaInd */



/*
*
*       Fun:   Pack Trace Indication
*
*       Desc:  This function is used to pack the trace indication
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLdsTrcInd
(
Pst *pst,                 /* post */
DsMngmt *trc,             /* trace */
Buffer  *mBuf             /* message buffer */
)
#else
PUBLIC S16 cmPkLdsTrcInd(pst, trc, mBuf)
Pst *pst;                 /* post */
DsMngmt *trc;             /* trace */
Buffer  *mBuf;            /* message buffer */
#endif
{

   TRC3(cmPkLdsTrcInd)

   if (mBuf == NULLP)
   {
      if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
      {
         RETVALUE(RFAILED);
      }
   }

   CMCHKPKLOG(SPkU16, trc->t.trc.evnt, mBuf, ELDS036, pst);
   CMCHKPKLOG(cmPkDateTime, &trc->t.trc.dt, mBuf, ELDS037, pst);

   CMCHKPKLOG(cmPkCmStatus, &trc->cfm, mBuf, ELDS038, pst);
   CMCHKPKLOG(cmPkHeader,   &trc->hdr, mBuf, ELDS039, pst);
   pst->event = EVTLDSTRCIND;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLdsTrcInd */


/*
*     layer management interface un-packing functions
*/

/*
*
*       Fun:   Un-pack Config Request
*
*       Desc:  This function is used to a un-pack config request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLdsCfgReq
(
LdsCfgReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf              /* message buffer */
)
#else
PUBLIC S16 cmUnpkLdsCfgReq(func, pst, mBuf)
LdsCfgReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;             /* message buffer */
#endif
{
   DsMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLdsCfgReq)

     /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(DsMngmt));

   /* hi009.104 - call the new MACRO to init struc to all zeros */
   LDS_ZERO((U8 *)&mgt, sizeof(DsMngmt));

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELDS040, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELDS041, pst);

   switch (mgt.hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LDSLOGERROR(pst, ELDS042, mgt.hdr.elmId.elmnt,
                     "cmUnpkLdsCfgReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);

   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Config Confirm
*
*       Desc:  This function is used to a un-pack config confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLdsCfgCfm
(
LdsCfgCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLdsCfgCfm(func, pst, mBuf)
LdsCfgCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   DsMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLdsCfgCfm)

   /* hi009.104 - added missing init to 0 */
	/* lhi_c_001.main_8: Modified HI_ZERO to LDS_ZERO */
   LDS_ZERO((U8*)&mgt, sizeof(DsMngmt))

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELDS043, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELDS044, pst);

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);

   RETVALUE(ROK);
} /* end of cmUnpkLdsCfgCfm */


/*
*
*       Fun:   Un-pack Control Request
*
*       Desc:  This function is used to un-pack the control request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLdsCntrlReq
(
LdsCntrlReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf              /* message buffer */
)
#else
PUBLIC S16 cmUnpkLdsCntrlReq(func, pst, mBuf)
LdsCntrlReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;             /* message buffer */
#endif
{
   DsMngmt  mgt;          /* configuration */

   TRC3(cmUnpkLdsCntrlReq)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(DsMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELDS045, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELDS046, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.cntrl.dt,  mBuf, ELDS047, pst);
   CMCHKUNPKLOG(SUnpkU8, &mgt.t.cntrl.action,    mBuf, ELDS048, pst);
   CMCHKUNPKLOG(SUnpkU8, &mgt.t.cntrl.subAction, mBuf, ELDS049, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
         if(mgt.t.cntrl.subAction == SADBG)
         {
#ifdef DEBUGP
           CMCHKUNPKLOG(SUnpkU32, &(mgt.t.cntrl.ctlType.dsDbg.dbgMask),
                        mBuf, ELDS050, pst);
#endif
         }
         break;

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LDSLOGERROR(pst, ELDS051, mgt.hdr.elmId.elmnt,
                     "cmUnpkLdsCntrlReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Control Confirm
*
*       Desc:  This function is used to un-pack the control confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLdsCntrlCfm
(
LdsCntrlCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLdsCntrlCfm(func, pst, mBuf)
LdsCntrlCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{
   DsMngmt  cfm;            /* configuration */

   TRC3(cmUnpkLdsCntrlCfm)

   CMCHKUNPKLOG(cmUnpkHeader,   &cfm.hdr, mBuf, ELDS052, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &cfm.cfm, mBuf, ELDS053, pst);

   (Void)SPutMsg(mBuf);
   (*func)(pst, &cfm);
   RETVALUE(ROK);
} /* end of cmUnpkLdsCntrlCfm */



/*
*
*       Fun:   Un-pack Statistics Request
*
*       Desc:  This function is used to un-pack the statistics request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLdsStsReq
(
LdsStsReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLdsStsReq(func, pst, mBuf)
LdsStsReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   DsMngmt  mgt;            /* configuration */
   /* lhi_c_001.main_11: Fix for Klockworks issue */
   Action   action = 0;         /* action type */

   TRC3(cmUnpkLdsStsReq)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(DsMngmt));
   CMCHKUNPKLOG(cmUnpkAction,   &action,  mBuf, ELDS054, pst);
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELDS055, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELDS056, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LDSLOGERROR(pst, ELDS057, mgt.hdr.elmId.elmnt,
                     "cmUnpkLdsStsReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, action, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLdsStsReq */


/*
*
*       Fun:   Un-pack Statistics Confirm
*
*       Desc:  This function is used to un-pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLdsStsCfm
(
LdsStsCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLdsStsCfm(func, pst, mBuf)
LdsStsCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   DsMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLdsStsCfm)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(DsMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELDS058, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELDS059, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.sts.dt,   mBuf, ELDS060, pst);
   CMCHKUNPKLOG(cmUnpkDuration, &mgt.t.sts.dura, mBuf, ELDS061, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LDSLOGERROR(pst, ELDS062, mgt.hdr.elmId.elmnt,
                     "cmUnpkLdsStsCfm () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Status Request
*
*       Desc:  This function is used to un-pack the status request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLdsStaReq
(
LdsStaReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLdsStaReq(func, pst, mBuf)
LdsStaReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{
   DsMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLdsStaReq)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(DsMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELDS063, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELDS064, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STSID:
         break;
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LDSLOGERROR(pst, ELDS065, mgt.hdr.elmId.elmnt,
                     "cmUnpkLdsStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Status Confirm
*
*       Desc:  This function is used to un-pack the status confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLdsStaCfm
(
LdsStaCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLdsStaCfm(func, pst, mBuf)
LdsStaCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   DsMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLdsStaCfm)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(DsMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELDS066, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELDS067, pst);
   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.ssta.dt, mBuf, ELDS068, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LDSLOGERROR(pst, ELDS069, mgt.hdr.elmId.elmnt,
                     "cmUnpkLdsStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLdsStaCfm */


/*
*
*       Fun:   Un-pack Status Indication
*
*       Desc:  This function is used to un-pack the status indication
*              primitive to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLdsStaInd
(
LdsStaInd func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLdsStaInd(func, pst, mBuf)
LdsStaInd func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   DsMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLdsStaInd)

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELDS070, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELDS071, pst);

   CMCHKUNPKLOG(cmUnpkCmAlarm, &mgt.t.usta.alarm,      mBuf, ELDS072, pst);
   CMCHKUNPKLOG(cmUnpkSpId,    &mgt.t.usta.info.spId,  mBuf, ELDS073, pst);
   CMCHKUNPKLOG(SUnpkU8,       &mgt.t.usta.info.type,  mBuf, ELDS074, pst);

   if(mgt.t.usta.info.type != LDS_ALARMINFO_TYPE_NTPRSNT)
   {
      switch(mgt.t.usta.info.type)
      {
         default:
#if (ERRCLASS & ERRCLS_DEBUG)
            LDSLOGERROR(pst, ELDS075, mgt.t.usta.info.type,
                        "cmUnpkLdsStaInd () Failed");
#endif /* ERRCLS_DEBUG */
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
      }/* end of switch */
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLdsStaInd */



/*
*
*       Fun:   Un-pack Trace Indication
*
*       Desc:  This function is used to un-pack the trace indication
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lds.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLdsTrcInd
(
LdsTrcInd func,
Pst *pst,                 /* post */
Buffer  *mBuf             /* message buffer */
)
#else
PUBLIC S16 cmUnpkLdsTrcInd(func, pst, mBuf)
LdsTrcInd func;
Pst *pst;                 /* post */
Buffer  *mBuf;            /* message buffer */
#endif
{

   DsMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLdsTrcInd)

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELDS076, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELDS077, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.trc.dt, mBuf, ELDS078, pst);
   CMCHKUNPKLOG(SUnpkU16, &mgt.t.trc.evnt, mBuf, ELDS079, pst);

   (*func)(pst, &mgt, mBuf);
   RETVALUE(ROK);
} /* end of cmUnpkLdsTrcInd */

#ifdef ANSI
PUBLIC S16 cmPkLdsInitAttachSgw
(
DnsQueryData * qd
)
#else
PUBLIC S16 cmPkLdsInitAttachSgw(qd)
DnsQueryData * qd
#endif
{
   Pst pst;
   Buffer *mBuf = NULLP;

   TRC3(cmPkLdsInitAttachSgw)

   memcpy( &pst, &dsCb.uzPst, sizeof(pst) );
   pst.event = (Event) EVTLDSINITATTCHSGW;

   if (SGetMsg(pst.region, pst.pool, &mBuf) != ROK) {
#if (ERRCLASS & ERRCLS_ADD_RES)
      SLogError(pst.srcEnt, pst.srcInst, pst.srcProcId,
         __FILE__, __LINE__, (ErrCls)ERRCLS_ADD_RES,
         (ErrVal)ELDS091, (ErrVal)0, "Packing failed");
#endif
      RETVALUE(RFAILED);
   }

   if (cmPkPtr((PTR)qd, mBuf) != ROK) {
#if (ERRCLASS & ERRCLS_ADD_RES)
      SLogError(pst.srcEnt, pst.srcInst, pst.srcProcId,
         __FILE__, __LINE__, (ErrCls)ERRCLS_ADD_RES,
         (ErrVal)ELDS092, (ErrVal)0, "Packing failed");
#endif
      SPutMsg(mBuf);
      RETVALUE(RFAILED);
   }

   RETVALUE(SPstTsk(&pst,mBuf));
}

#ifdef ANSI
PUBLIC S16 cmUnpkLdsInitAttachSgw
(
LdsInitAttachSgw func,
Pst *pst,
Buffer *mBuf
)
#else
PUBLIC S16 cmUnpkLdsInitAttachSgw(func, pst, mBuf)
LdsInitAttachSgw func;
Pst *pst;
Buffer *mBuf;
#endif
{
   DnsQueryData *qd = NULLP;

   TRC3(cmUnpkLdsInitAttachSgw)

   if (cmUnpkPtr((PTR *)&qd, mBuf) != ROK) {
      SPutMsg(mBuf);
      RETVALUE(RFAILED);
   }

   SPutMsg(mBuf);
   RETVALUE((*func)(pst, qd));
}

#endif /* LCLDS */

/********************************************************************30**

         End of file:     lds.c@@/main/13 - Tue Apr 26 18:11:18 2011

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat     init                  description
----------- --------- ---- -----------------------------------------------
1.1         ---       bw   1. initial release.
*********************************************************************91*/
