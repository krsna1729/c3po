/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter T6A Interface

     Type:     C source file

     Desc:     C source code for common packing and un-packing functions for
               layer management interface(LSW).

     File:     lsw.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/*
*     This software may be combined with the following TRILLIUM
*     software:
*
*     part no.                      description
*     --------    ----------------------------------------------
*
*/


/* header include files (.h) */
#include "envopt.h"        /* environment options */
#include "envdep.h"        /* environment dependent */
#include "envind.h"        /* environment independent */
#include "gen.h"           /* general layer */
#include "cm_gen.h"        /* common pack/unpack defines */
#include "cm_os.h"
#include "ssi.h"           /* system services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */

/* header/extern include files (.x) */
#include "gen.x"           /* general layer */
#include "cm_os.x"
#include "ssi.x"           /* system services */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common linrary function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */

#include "aqfd2.x"

#include "lsw.h"
#include "lsw.x"
#include "sw_err.h"
#include "sw.h"
#include "sw.x"

#include "cm_lib.x"            /* has the prototype of cmMemset() */


/* local defines */

/* local typedefs */

/* local externs */

/* forward references */



/* functions in other modules */

/* public variable declarations */

/* private variable declarations */

#ifdef LCLSW



/*
*     layer management interface packing functions
*/


/*
*
*       Fun:   Pack Config Request
*
*       Desc:  This function is used to a pack config request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLswCfgReq
(
Pst *pst,                 /* post structure */
SwMngmt *cfg              /* configuration */
)
#else
PUBLIC S16 cmPkLswCfgReq(pst, cfg)
Pst *pst;                 /* post structure */
SwMngmt *cfg;             /* configuration */
#endif
{
   Buffer *mBuf;            /* message buffer */

   TRC3(cmPkLswCfgReq)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch (cfg->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSWLOGERROR(pst, ELSW001, cfg->hdr.elmId.elmnt,
                     "cmPkLswCfgReq() Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   CMCHKPKLOG(cmPkCmStatus, &cfg->cfm, mBuf, ELSW002, pst);
   CMCHKPKLOG(cmPkHeader, &cfg->hdr, mBuf, ELSW003, pst);
   pst->event = (Event)EVTLSWCFGREQ;

/* fill interface version number in pst */
#ifdef TSW_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LSWIFVER;
#endif /* TSW_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Config Confirm
*
*       Desc:  This function is used to a pack config confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLswCfgCfm
(
Pst *pst,                 /* post structure */
SwMngmt *cfm              /* confirm */
)
#else
PUBLIC S16 cmPkLswCfgCfm(pst, cfm)
Pst *pst;                 /* post structure */
SwMngmt *cfm;             /* confirm */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLswCfgCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   CMCHKPKLOG(cmPkCmStatus, &cfm->cfm, mBuf, ELSW004, pst);
   CMCHKPKLOG(cmPkHeader,   &cfm->hdr, mBuf, ELSW005, pst);

   pst->event = EVTLSWCFGCFM;
   RETVALUE(SPstTsk(pst, mBuf));

} /* end of cmPkLswCfgCfm */


/*
*
*       Fun:   Pack Control Request
*
*       Desc:  This function is used to pack the control request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLswCntrlReq
(
Pst *pst,                 /* post structure */
SwMngmt *ctl              /* configuration */
)
#else
PUBLIC S16 cmPkLswCntrlReq(pst, ctl)
Pst *pst;                 /* post structure */
SwMngmt *ctl;             /* configuration */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLswCntrlReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   switch(ctl->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSWLOGERROR(pst, ELSW006, ctl->hdr.elmId.elmnt,
                     "cmPkLswCntrlReq () Failed : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   CMCHKPKLOG(SPkU8,       ctl->t.cntrl.subAction, mBuf, ELSW007, pst);
   CMCHKPKLOG(SPkU8,       ctl->t.cntrl.action,    mBuf, ELSW008, pst);
   CMCHKPKLOG(cmPkDateTime, &ctl->t.cntrl.dt,       mBuf, ELSW009, pst);

   CMCHKPKLOG(cmPkCmStatus, &ctl->cfm, mBuf, ELSW010, pst);
   CMCHKPKLOG(cmPkHeader,   &ctl->hdr, mBuf, ELSW011, pst);
   pst->event = (Event)EVTLSWCNTRLREQ;

/* hi009.104 - fill interface version number in pst */
#ifdef TSW_ROLL_UPGRADE_SUPPORT
   pst->intfVer = (CmIntfVer) LSWIFVER;
#endif /* TSW_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Control Confirm
*
*       Desc:  This function is used to pack the control confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLswCntrlCfm
(
Pst *pst,                 /* post structure */
SwMngmt *cfm              /* confirm */
)
#else
PUBLIC S16 cmPkLswCntrlCfm(pst, cfm)
Pst *pst;                 /* post structure */
SwMngmt *cfm;             /* confirm */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLswCntrlCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   /* pack status */
   CMCHKPKLOG(cmPkCmStatus, &cfm->cfm, mBuf, ELSW012, pst);

   /* pack header */
   CMCHKPKLOG(cmPkHeader, &cfm->hdr, mBuf, ELSW013, pst);

   pst->event = EVTLSWCNTRLCFM;
   RETVALUE(SPstTsk(pst, mBuf));

} /* end of cmPkLswCntrlCfm */



/*
*
*       Fun:   Pack Statistics Request
*
*       Desc:  This function is used to pack the statistics request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLswStsReq
(
Pst *pst,                 /* post structure */
Action action,            /* action */
SwMngmt *sts              /* statistics */
)
#else
PUBLIC S16 cmPkLswStsReq(pst, action, sts)
Pst *pst;                 /* post structure */
Action action;            /* action */
SwMngmt *sts;             /* statistics */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLswStsReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sts->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSWLOGERROR(pst, ELSW014, sts->hdr.elmId.elmnt,
                     "cmPkLswStsReq () Failed : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }
   CMCHKPKLOG(cmPkCmStatus, &sts->cfm, mBuf, ELSW015, pst);
   CMCHKPKLOG(cmPkHeader,   &sts->hdr, mBuf, ELSW016, pst);
   CMCHKPKLOG(cmPkAction,   action,    mBuf, ELSW017, pst);
   pst->event = EVTLSWSTSREQ;

/* hi009.104 - fill interface version number in pst */
#ifdef TSW_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LSWIFVER;
#endif /* TSW_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLswStsReq */


/*
*
*       Fun:   Pack Statistics Confirm
*
*       Desc:  This function is used to pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLswStsCfm
(
Pst *pst,                 /* post structure */
SwMngmt *sts              /* statistics */
)
#else
PUBLIC S16 cmPkLswStsCfm(pst, sts)
Pst *pst;                 /* post structure */
SwMngmt *sts;             /* statistics */
#endif

{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLswStsCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sts->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSWLOGERROR(pst, ELSW018, sts->hdr.elmId.elmnt,
                     "cmPkLswStsCfm() : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   CMCHKPKLOG(cmPkDuration, &sts->t.sts.dura, mBuf, ELSW019, pst);
   CMCHKPKLOG(cmPkDateTime, &sts->t.sts.dt, mBuf, ELSW020, pst);

   CMCHKPKLOG(cmPkCmStatus, &sts->cfm, mBuf, ELSW021, pst);
   CMCHKPKLOG(cmPkHeader,   &sts->hdr, mBuf, ELSW022, pst);
   pst->event = (Event)EVTLSWSTSCFM;
   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Status Request
*
*       Desc:  This function is used to pack the status request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLswStaReq
(
Pst *pst,                 /* post structure */
SwMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 cmPkLswStaReq(pst, sta)
Pst *pst;                 /* post structure */
SwMngmt *sta;             /* solicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLswStaReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sta->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSWLOGERROR(pst, ELSW023, sta->hdr.elmId.elmnt,
                     "cmPkLswStaReq () : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELSW024, pst);
   CMCHKPKLOG(cmPkHeader, &sta->hdr, mBuf, ELSW025, pst);
   pst->event = (Event)EVTLSWSTAREQ;

   /* hi009.104 - fill interface version number in pst */
#ifdef TSW_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LSWIFVER;
#endif /* TSW_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Status Confirm
*
*       Desc:  This function is used to pack the status confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLswStaCfm
(
Pst *pst,                 /* post structure */
SwMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 cmPkLswStaCfm(pst, sta)
Pst *pst;                 /* post structure */
SwMngmt *sta;             /* solicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLswStaCfm)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   switch(sta->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSWLOGERROR(pst, ELSW026, sta->hdr.elmId.elmnt,
                     "cmPkLswStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   /* date */
   CMCHKPKLOG(cmPkDateTime, &sta->t.ssta.dt, mBuf, ELSW027, pst);

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELSW028, pst);
   CMCHKPKLOG(cmPkHeader,   &sta->hdr, mBuf, ELSW029, pst);
   pst->event = EVTLSWSTACFM;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLswStaCfm */


/*
*
*       Fun:   Pack Status Indication
*
*       Desc:  This function is used to pack the status indication
*              primitive to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLswStaInd
(
Pst *pst,                 /* post structure */
SwMngmt *sta              /* unsolicited status */
)
#else
PUBLIC S16 cmPkLswStaInd(pst, sta)
Pst *pst;                 /* post structure */
SwMngmt *sta;             /* unsolicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLswStaInd)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   if(sta->t.usta.info.type != LSW_ALARMINFO_TYPE_NTPRSNT)
   {
      switch(sta->t.usta.info.type)
      {
         case STGEN:
         {
            break;
         }
         default:
#if (ERRCLASS & ERRCLS_DEBUG)
            LSWLOGERROR(pst, ELSW030, sta->t.usta.info.type,
                        "cmPkLswStaInd () Failed");
#endif
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
      }
   }
   CMCHKPKLOG(SPkU8,   sta->t.usta.info.type, mBuf, ELSW031, pst);
   CMCHKPKLOG(cmPkSpId,    sta->t.usta.info.spId,  mBuf, ELSW032, pst);
   CMCHKPKLOG(cmPkCmAlarm, &sta->t.usta.alarm,     mBuf, ELSW033, pst);

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELSW034, pst);
   CMCHKPKLOG(cmPkHeader,   &sta->hdr, mBuf, ELSW035, pst);
   pst->event = (Event)EVTLSWSTAIND;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLswStaInd */



/*
*
*       Fun:   Pack Trace Indication
*
*       Desc:  This function is used to pack the trace indication
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLswTrcInd
(
Pst *pst,                 /* post */
SwMngmt *trc,             /* trace */
Buffer  *mBuf             /* message buffer */
)
#else
PUBLIC S16 cmPkLswTrcInd(pst, trc, mBuf)
Pst *pst;                 /* post */
SwMngmt *trc;             /* trace */
Buffer  *mBuf;            /* message buffer */
#endif
{

   TRC3(cmPkLswTrcInd)

   if (mBuf == NULLP)
   {
      if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
      {
         RETVALUE(RFAILED);
      }
   }

   CMCHKPKLOG(SPkU16, trc->t.trc.evnt, mBuf, ELSW036, pst);
   CMCHKPKLOG(cmPkDateTime, &trc->t.trc.dt, mBuf, ELSW037, pst);

   CMCHKPKLOG(cmPkCmStatus, &trc->cfm, mBuf, ELSW038, pst);
   CMCHKPKLOG(cmPkHeader,   &trc->hdr, mBuf, ELSW039, pst);
   pst->event = EVTLSWTRCIND;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLswTrcInd */


/*
*     layer management interface un-packing functions
*/

/*
*
*       Fun:   Un-pack Config Request
*
*       Desc:  This function is used to a un-pack config request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLswCfgReq
(
LswCfgReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf              /* message buffer */
)
#else
PUBLIC S16 cmUnpkLswCfgReq(func, pst, mBuf)
LswCfgReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;             /* message buffer */
#endif
{
   SwMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLswCfgReq)

   LSW_ZERO((U8 *)&mgt, sizeof(SwMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSW040, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSW041, pst);

   switch (mgt.hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSWLOGERROR(pst, ELSW042, mgt.hdr.elmId.elmnt,
                     "cmUnpkLswCfgReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);

   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Config Confirm
*
*       Desc:  This function is used to a un-pack config confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLswCfgCfm
(
LswCfgCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLswCfgCfm(func, pst, mBuf)
LswCfgCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SwMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLswCfgCfm)

   LSW_ZERO((U8*)&mgt, sizeof(SwMngmt))
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSW043, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSW044, pst);

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);

   RETVALUE(ROK);
} /* end of cmUnpkLswCfgCfm */


/*
*
*       Fun:   Un-pack Control Request
*
*       Desc:  This function is used to un-pack the control request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLswCntrlReq
(
LswCntrlReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf              /* message buffer */
)
#else
PUBLIC S16 cmUnpkLswCntrlReq(func, pst, mBuf)
LswCntrlReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;             /* message buffer */
#endif
{
   SwMngmt  mgt;          /* configuration */

   TRC3(cmUnpkLswCntrlReq)

   LSW_ZERO((U8 *)&mgt, sizeof(SwMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSW045, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSW046, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.cntrl.dt,  mBuf, ELSW047, pst);
   CMCHKUNPKLOG(SUnpkU8, &mgt.t.cntrl.action,    mBuf, ELSW048, pst);
   CMCHKUNPKLOG(SUnpkU8, &mgt.t.cntrl.subAction, mBuf, ELSW049, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
         if(mgt.t.cntrl.subAction == SADBG)
         {
#ifdef DEBUGP
           CMCHKUNPKLOG(SUnpkU32, &(mgt.t.cntrl.ctlType.swDbg.dbgMask),
                        mBuf, ELSW050, pst);
#endif
         }
         break;

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSWLOGERROR(pst, ELSW051, mgt.hdr.elmId.elmnt,
                     "cmUnpkLswCntrlReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Control Confirm
*
*       Desc:  This function is used to un-pack the control confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLswCntrlCfm
(
LswCntrlCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLswCntrlCfm(func, pst, mBuf)
LswCntrlCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{
   SwMngmt  cfm;            /* configuration */

   TRC3(cmUnpkLswCntrlCfm)

   CMCHKUNPKLOG(cmUnpkHeader,   &cfm.hdr, mBuf, ELSW052, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &cfm.cfm, mBuf, ELSW053, pst);

   (Void)SPutMsg(mBuf);
   (*func)(pst, &cfm);
   RETVALUE(ROK);
} /* end of cmUnpkLswCntrlCfm */



/*
*
*       Fun:   Un-pack Statistics Request
*
*       Desc:  This function is used to un-pack the statistics request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLswStsReq
(
LswStsReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLswStsReq(func, pst, mBuf)
LswStsReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SwMngmt  mgt;            /* configuration */
   /* lhi_c_001.main_11: Fix for Klockworks issue */
   Action   action = 0;         /* action type */

   TRC3(cmUnpkLswStsReq)

   LSW_ZERO((U8 *)&mgt, sizeof(SwMngmt));
   CMCHKUNPKLOG(cmUnpkAction,   &action,  mBuf, ELSW054, pst);
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSW055, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSW056, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSWLOGERROR(pst, ELSW057, mgt.hdr.elmId.elmnt,
                     "cmUnpkLswStsReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, action, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLswStsReq */


/*
*
*       Fun:   Un-pack Statistics Confirm
*
*       Desc:  This function is used to un-pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLswStsCfm
(
LswStsCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLswStsCfm(func, pst, mBuf)
LswStsCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SwMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLswStsCfm)

   LSW_ZERO((U8 *)&mgt, sizeof(SwMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSW058, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSW059, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.sts.dt,   mBuf, ELSW060, pst);
   CMCHKUNPKLOG(cmUnpkDuration, &mgt.t.sts.dura, mBuf, ELSW061, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSWLOGERROR(pst, ELSW062, mgt.hdr.elmId.elmnt,
                     "cmUnpkLswStsCfm () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Status Request
*
*       Desc:  This function is used to un-pack the status request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLswStaReq
(
LswStaReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLswStaReq(func, pst, mBuf)
LswStaReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{
   SwMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLswStaReq)

   LSW_ZERO((U8 *)&mgt, sizeof(SwMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSW063, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSW064, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STSID:
         break;
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSWLOGERROR(pst, ELSW065, mgt.hdr.elmId.elmnt,
                     "cmUnpkLswStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Status Confirm
*
*       Desc:  This function is used to un-pack the status confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLswStaCfm
(
LswStaCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLswStaCfm(func, pst, mBuf)
LswStaCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SwMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLswStaCfm)

   LSW_ZERO((U8 *)&mgt, sizeof(SwMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSW066, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSW067, pst);
   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.ssta.dt, mBuf, ELSW068, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSWLOGERROR(pst, ELSW069, mgt.hdr.elmId.elmnt,
                     "cmUnpkLswStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLswStaCfm */


/*
*
*       Fun:   Un-pack Status Indication
*
*       Desc:  This function is used to un-pack the status indication
*              primitive to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLswStaInd
(
LswStaInd func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLswStaInd(func, pst, mBuf)
LswStaInd func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SwMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLswStaInd)

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSW070, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSW071, pst);

   CMCHKUNPKLOG(cmUnpkCmAlarm, &mgt.t.usta.alarm,      mBuf, ELSW072, pst);
   CMCHKUNPKLOG(cmUnpkSpId,    &mgt.t.usta.info.spId,  mBuf, ELSW073, pst);
   CMCHKUNPKLOG(SUnpkU8,       &mgt.t.usta.info.type,  mBuf, ELSW074, pst);

   if(mgt.t.usta.info.type != LSW_ALARMINFO_TYPE_NTPRSNT)
   {
      switch(mgt.t.usta.info.type)
      {
         default:
#if (ERRCLASS & ERRCLS_DEBUG)
            LSWLOGERROR(pst, ELSW075, mgt.t.usta.info.type,
                        "cmUnpkLswStaInd () Failed");
#endif /* ERRCLS_DEBUG */
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
      }/* end of switch */
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLswStaInd */



/*
*
*       Fun:   Un-pack Trace Indication
*
*       Desc:  This function is used to un-pack the trace indication
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsw.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLswTrcInd
(
LswTrcInd func,
Pst *pst,                 /* post */
Buffer  *mBuf             /* message buffer */
)
#else
PUBLIC S16 cmUnpkLswTrcInd(func, pst, mBuf)
LswTrcInd func;
Pst *pst;                 /* post */
Buffer  *mBuf;            /* message buffer */
#endif
{

   SwMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLswTrcInd)

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSW076, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSW077, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.trc.dt, mBuf, ELSW078, pst);
   CMCHKUNPKLOG(SUnpkU16, &mgt.t.trc.evnt, mBuf, ELSW079, pst);

   (*func)(pst, &mgt, mBuf);
   RETVALUE(ROK);
} /* end of cmUnpkLswTrcInd */

#if 0
#ifdef ANSI
PUBLIC S16 cmPkLswInitAttachSgw
(
DnsQueryData * qd
)
#else
PUBLIC S16 cmPkLswInitAttachSgw(qd)
DnsQueryData * qd
#endif
{
   Pst pst;
   Buffer *mBuf = NULLP;

   TRC3(cmPkLswInitAttachSgw)

   memcpy( &pst, &swCb.uzPst, sizeof(pst) );
   pst.event = (Event) EVTLSWINITATTCHSGW;

   if (SGetMsg(pst.region, pst.pool, &mBuf) != ROK) {
#if (ERRCLASS & ERRCLS_ADD_RES)
      SLogError(pst.srcEnt, pst.srcInst, pst.srcProcId,
         __FILE__, __LINE__, (ErrCls)ERRCLS_ADD_RES,
         (ErrVal)ELSW091, (ErrVal)0, "Packing failed");
#endif
      RETVALUE(RFAILED);
   }

   if (cmPkPtr((PTR)qd, mBuf) != ROK) {
#if (ERRCLASS & ERRCLS_ADD_RES)
      SLogError(pst.srcEnt, pst.srcInst, pst.srcProcId,
         __FILE__, __LINE__, (ErrCls)ERRCLS_ADD_RES,
         (ErrVal)ELSW092, (ErrVal)0, "Packing failed");
#endif
      SPutMsg(mBuf);
      RETVALUE(RFAILED);
   }

printf("* sending EVTLSWINITATTCHSGW message\n");
   RETVALUE(SPstTsk(&pst,mBuf));
}

#ifdef ANSI
PUBLIC S16 cmUnpkLswInitAttachSgw
(
LswInitAttachSgw func,
Pst *pst,
Buffer *mBuf
)
#else
PUBLIC S16 cmUnpkLswInitAttachSgw(func, pst, mBuf)
LswInitAttachSgw func;
Pst *pst;
Buffer *mBuf;
#endif
{
   DnsQueryData *qd = NULLP;

   TRC3(cmUnpkLswInitAttachSgw)

   if (cmUnpkPtr((PTR *)&qd, mBuf) != ROK) {
      SPutMsg(mBuf);
      RETVALUE(RFAILED);
   }

   SPutMsg(mBuf);
   RETVALUE((*func)(pst, qd));
}
#endif

#endif /* LCLSW */

/********************************************************************30**

         End of file:     lsw.c@@/main/13 - Tue Apr 26 18:11:18 2011

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat     init                  description
----------- --------- ---- -----------------------------------------------
1.1         ---       bw   1. initial release.
*********************************************************************91*/
