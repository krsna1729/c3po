C3PO: MME Build and Run Instructions

Perform the following procedures in order.

  1. Follow the instructions located in the "Build and Installation
     Instructions for External Modules" provided in
     {installation_root}/c3po/README.txt. Make sure these steps are complete.

  2. Build the MME.
  
        NOTE: bldsec only needs to be run once unless it has changed._

        $ cd {installation_root}/c3po/mme
        $ ./bldsec
        $ ./bldmme
  
  3. Update the following files with any configuration changes you need:
  
       {installation_root}/c3po/mme/src/vbfd.conf
         Identity - the fully qualified Diameter host name (host and realm)
         Realm - the Diameter realm (everything after the 1st element)
         
       {installation_root}/c3po/mme/src/**vbsm_cfg.txt**
         VBSM_ENB_ADDR_1 - the IP address of the E-Nodeb
         VBSM_ENB_PORT_1 - the port of the E-Nodeb
         VBSM_MME_IPADDR - the IP address of the MME used for both S1AP and S11
           communication

  4. Create the certificates needed for freeDiameter using the diameter host,
     not the  and realm configured in {installation_root}/c3po/mme/src/vbfd.conf

       $ ./make_certs.sh <diameter_host> <diameter_realm>
           EXAMPLE: Assuming a Diameter identity of mme.test3gpp.net
       $ ./make_certs.sh mme test3gpp.net

  5. To run the application:

       $ cd {installation_root}/c3po/mme/src/mme
       $ sudo ./vb_acc
