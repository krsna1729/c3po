/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter T6A convergence Layer

     Type:     C include file

     Desc:     Defines and macros used by Diameter.

     File:     sw.h

     Sid:

     Prg:      bw

*********************************************************************21*/


#ifndef __SWH__
#define __SWH__

#define SWLAYERNAME     "T6A_CONVERGENCE_LAYER"

#define EVTINTGOACTV            42


/* miscellaneous defines */
#define SW_PRNTBUF_SIZE         128     /* size of print buffer */
#define SW_UNUSED               -1      /* for all unused fields */
#define SW_SRVCTYPE_MASK        4

/* Macros.
 */

/* DEBUGP */
#define SWDBGP(_msgClass, _arg)                                 \
   DBGP(&(swCb.init), SWLAYERNAME, _msgClass, _arg)

#define SW_DBG_INFO(_arg)                                       \
   DBGP(&(swCb.init), SWLAYERNAME, LSW_DBGMASK_INFO, _arg)

/* zero out a buffer */
#define SW_ZERO(_str, _len)                                     \
   cmMemset((U8 *)_str, 0, _len);

/* allocate and zero out a static buffer */
#define SW_ALLOC(_size, _datPtr)                                \
{                                                               \
   S16 _ret;                                                    \
   _ret = SGetSBuf(swCb.init.region, swCb.init.pool,            \
                   (Data **)&_datPtr, _size);                   \
   if (_ret == ROK)                                             \
      cmMemset((U8*)_datPtr, 0, _size);                         \
   else                                                         \
      _datPtr = NULLP;                                          \
}

/* free a static buffer */
#define SW_FREE(_size, _datPtr)                                 \
   SPutSBuf(swCb.init.region, swCb.init.pool,                   \
            (Data *)_datPtr, _size);

/* statistics */
#if (defined(SW_MULTI_THREADED)  &&  defined(SW_STS_LOCKS))

#define SW_INC_ERRSTS(_stat)                                    \
{                                                               \
   SW_LOCK(&swCb.errStsLock);                                   \
   _stat++;                                                     \
   SW_UNLOCK(&swCb.errStsLock);                                 \
}

#define SW_INC_TXSTS(_sap, _stat)                               \
{                                                               \
   //SW_LOCK(&_sap->txStsLock);                                   \
   //_stat++;                                                     \
   //SW_UNLOCK(&_sap->txStsLock);                                 \
}

/*hi028.201: Handled statistics properly*/
#define SW_DEC_TXSTS(_sap, _stat)                               \
{                                                               \
   //SW_LOCK(&_sap->txStsLock);                                   \
   //_stat--;                                                     \
   //SW_UNLOCK(&_sap->txStsLock);                                 \
}

#endif

/* statistics */
#if (defined(SW_MULTI_THREADED)  &&  defined(SW_STS_LOCKS))

#define SW_INC_ERRSTS(_stat)                                    \
{                                                               \
   SW_LOCK(&swCb.errStsLock);                                   \
   _stat++;                                                     \
   SW_UNLOCK(&swCb.errStsLock);                                 \
}

#define HI_INC_TXSTS(_stat)                                     \
{                                                               \
   SW_LOCK(&swCb.txStsLock);                                    \
   _stat++;                                                     \
   SW_UNLOCK(&swCb.txStsLock);                                  \
}

/*hi028.201: Handled statistics properly*/
#define HI_DEC_TXSTS(__stat)                                    \
{                                                               \
   SW_LOCK(&swCb.txStsLock);                                   \
   _stat--;                                                     \
   SW_UNLOCK(&swCb.txStsLock);                                 \
}

#define SW_ADD_TXSTS(_stat, _val)                         \
{                                                               \
   SW_LOCK(&swCb.txStsLock);                                   \
   _stat += _val;                                               \
   HI_UNLOCK(&swCb.txStsLock);                                 \
}

#define SW_ZERO_ERRSTS()                                        \
{                                                               \
   SW_LOCK(&swCb.errStsLock);                                   \
   SW_ZERO(&swCb.errSts, sizeof(SwErrSts));                     \
   SW_UNLOCK(&swCb.errStsLock);                                 \
}

#define SW_ZERO_TXSTS()                                         \
{                                                               \
   SW_LOCK(&swCb.txStsLock);                                    \
   swCb.txSts.numTxMsg = 0;                                     \
   SW_UNLOCK(&swCb.txStsLock);                                  \
}

#else  /* SW_MULTI_THREADED  &&  SW_STS_LOCKS */


#define SW_INC_ERRSTS(_stat)                                    \
   _stat++;

#define SW_INC_TXSTS(_stat)                                     \
   _stat++;

#define SW_DEC_TXSTS(_stat)                                     \
   _stat--;

#define SW_ADD_TXSTS(_stat, _val)                               \
   _stat += _val;

#define SW_ZERO_ERRSTS()                                        \
   SW_ZERO(&swCb.errSts, sizeof(SwErrSts));

#define SW_ZERO_TXSTS()                                         \
   swCb.txSts.numTxMsg = 0;

#define SW_TRC2(_arg)                                                   \
Txt __tapa_fun[PRNTSZE];                                                \
sprintf(__tapa_fun,#_arg);                                              \
{                                                                       \
   if (swCb.init.dbgMask & (LVB_DBGMASK_TRC))                        \
   {                                                                    \
      VB_PRNTTIMESTAMP(swCb.init)                                    \
      VB_PRNT_LAYER(swCb.init, VB_LYR_NAME)                                 \
      VB_PRNT_FILE_LINE(swCb.init)                                  \
      VBPRNT(swCb.init, (swCb.init.prntBuf," Entering\n"));                     \
      TRC2(_arg);                                                       \
   }                                                                    \
}


#endif /* SW_MULTI_THREADED  &&  SW_STS_LOCKS */


#endif /* __SWH__ */


/********************************************************************30**

         End of file:

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

