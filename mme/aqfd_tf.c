/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter Convergence Layer

     Type:     C source file

     Desc:     freeDiameter initialization.

     File:     aqfd_tf.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */
#include "cm_os.h"

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */
#include "cm_os.x"

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"

#include "lsx.h"
#include "lsx.x"

#include "aqfd.h"
#include "aqfd.x"

#include "sx.h"
#include "sx.x"
#include "sx_err.h"

/*
 *  Thread local Pst related variables and init routine
 */
static __thread int _pstInitialized;
static __thread Pst _pst;

PRIVATE Void _initPst ARGS((void));

PRIVATE Void _initPst()
{
   if (!_pstInitialized)
   {
      memcpy( &_pst, &aqCb.cfg.sxPst, sizeof(_pst) );
      _pstInitialized = 1;
   }
}

/*
*
*       Fun:    aqfdSendTFA
*
*       Desc:   Send MT-Forward-Short-Message-Answer to the SMS router.
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*
*       Notes:  None
*
*       File:   aqfd_tf.c
*
*      The Update-Location-Request (ULR) command, indicated by the Command-Code
*      field set to 316 and the "R" bit set in the Command Flags field, is sent
*      from MME or SGSN to HSS.
*
*      < MT-Forward-Short-Message-Answer > ::=   < Diameter Header: 8388646, PXY, 16777313 >
*        < Session-Id >
*        [ DRMP ]
*        [ Vendor-Specific-Application-Id ]
*        [ Result-Code ]
*        [ Experimental-Result ]
*        { Auth-Session-State }
*        { Origin-Host }
*        { Origin-Realm }
*       *[ Supported-Features ]
*        [ Absent-User-Diagnostic-SM ]
*        [ SM-Delivery- Failure-Cause ]
*        [ SM-RP-UI ]
*        [ Requested-Retransmission-Time ]
*        [ User-Identifier ]
*       *[ AVP ]
*        [ Failed-AVP ]
*       *[ Proxy-Info ]
*       *[ Route-Record ]
*/
#ifdef ANSI
PUBLIC S16 aqfdSendTFA
(
struct msg *tfr,
U32         rc,
U32         erc,
S32         smc,
U8         *diag,
U8          diagLen
)
#else
PUBLIC S16 aqfdSendTFA(tfr, rc, erc, smc, diag, diagLen)
struct msg *tfr;
U32 rc;
U32 erc;
S32 smc;
U8 *diag;
U8 diagLen;
#endif
{
   struct msg *tfa = tfr;

   /* construct the answer from the original request */
   AQCHECK_MSG_NEW_ANSWER_FROM_REQ(fd_g_config->cnf_dict, tfa);

   /* Add Origin-Host, Origin-Realm, Origin-State-Id AVPS at the end of the message */
   AQCHECK_MSG_ADD_ORIGIN( tfa );

   /* Auth-Session-State */
   AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Auth_Session_State, tfa, MSG_BRW_LAST_CHILD, 1 );

   if (rc != 0)
   {
      /* Result-Code */
      AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Result_Code, tfa, MSG_BRW_LAST_CHILD, rc );
   }
   else
   {
      struct avp *a;

      AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Experimental_Result, tfa, MSG_BRW_LAST_CHILD, a );
      AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Vendor_Id, a, MSG_BRW_LAST_CHILD, 10415 );
      AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Experimental_Result_Code, a, MSG_BRW_LAST_CHILD, erc );

      if (erc == 5555) /* DIAMETER_ERROR_SM_DELIVERY_FAILURE */
      {
         AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_SM_Delivery_Failure_Cause, tfa, MSG_BRW_LAST_CHILD, a );
         AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_SM_Enumerated_Delivery_Failure_Cause, a, MSG_BRW_LAST_CHILD, smc );
         AQCHECK_MSG_ADD_AVP_OSTR(aqDict.avp_SM_Diagnostic_Info, a, MSG_BRW_LAST_CHILD, diag, diagLen);
      }
   }

AQFD_DUMP_MESSAGE(tfa);

   /* send the message */
   AQCHECK_MSG_SEND( &tfa, NULL, NULL );

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfd_tfr_cb
*
*       Desc:   MT-Forward-Short-Message-Request callback
*
*       Ret:    0
*
*       Notes:  None
*
*       File:   aqfd_tf.c
*
*       < MT-Forward-Short-Message-Request > ::= < Diameter Header: 8388646, REQ, PXY, 16777313 >
*         < Session-Id >
*         [ DRMP ]
*         [ Vendor-Specific-Application-Id ]
*         { Auth-Session-State }
*         { Origin-Host }
*         { Origin-Realm }
*         { Destination-Host }
*         { Destination-Realm }
*         { User-Name }
*        *[ Supported-Features ]
*         [ SMSMI-Correlation-ID ]
*         { SC-Address }
*         { SM-RP-UI }
*         [ MME-Number-for-MT-SMS ]
*         [ SGSN-Number ]
*         [ TFR-Flags ]
*         [ SM-Delivery-Timer ]
*         [ SM-Delivery-Start-Time ]
*         [ Maximum-Retransmission-Time ]
*         [ SMS-GMSC-Address ]
*        *[ AVP ]
*        *[ Proxy-Info ]
*        *[ Route-Record ]
*/
#ifdef ANSI
PUBLIC int aqfd_tfr_cb
(
struct msg ** msg,
struct avp * pavp,
struct session * sess,
void * data,
enum disp_action * act
)
#else
PUBLIC int aqfd_tfr_cb(msg, pavp, sess, data, act)
struct msg ** msg;
struct avp * pavp;
struct session * sess;
void * data;
enum disp_action * act;
#endif
{
   S8 imsi[VB_HSS_IMSI_LEN + 1];
   VbMmeMtData *mtdata = NULL;
   struct msg   *tfr = NULL;

   tfr = *msg;
   *msg = NULL;

   /* allocate mtdata */
   if (vbMmeUtlAllocMtData(&mtdata) != ROK)
   {
      LOG_E("Unable to allocate memory for VbMmeMtData object");
      aqfdSendTFA( tfr, 5012 /* DIAMETER_UNABLE_TO_COMPLY */, 0, 0, "", 0 );
      return 0;
   }

   /* TFR */
   mtdata->tfr = tfr;

   /* IMSI */
   AQCHECK_MSG_GET_AVP_STR(tfr, aqDict.avp_User_Name, imsi, sizeof(imsi), );
   aqfdImsi2Binary(imsi, mtdata->imsi.val, &mtdata->imsi.len);

   /* RPDATA */
   mtdata->rqst.msgType = RP_MSG_TYPE_RPDATA_N2MS;
   mtdata->rqst.u.rpdata.origAddr.ext = 1; /* no extension */
   mtdata->rqst.u.rpdata.origAddr.type_of_number = 1; /* international number */
   mtdata->rqst.u.rpdata.origAddr.nbr_plan_id = 1; /* E.164 */
   AQCHECK_MSG_GET_AVP_OSTR( mtdata->tfr, aqDict.avp_SC_Address,
         mtdata->rqst.u.rpdata.origAddr.address, mtdata->rqst.u.rpdata.origAddr.len,
         sizeof(mtdata->rqst.u.rpdata.origAddr.address), );
   AQCHECK_MSG_GET_AVP_OSTR( mtdata->tfr, aqDict.avp_SM_RP_UI,
         mtdata->rqst.u.rpdata.userData.tpdu, mtdata->rqst.u.rpdata.userData.len,
         sizeof(mtdata->rqst.u.rpdata.userData.tpdu), );

   /* push ofa */
   aqQueuePush(&aqCb.tfrQueue, mtdata);

   _initPst();
   _pst.event = EVTLSXTFR;
   {
      S16 ret;
      Buffer *buf = NULL;
      if((ret = SGetMsg(aqCb.cfg.sxPst.region, aqCb.cfg.sxPst.pool, &buf)) != ROK)
         LOG_E("Error %d returned from SGetMsg() sending TFR notification", ret);
      else
      {
         if ((ret = SPstTsk(&_pst, buf)) != ROK)
         {
            LOG_E("Error %d returned from SPstTsk() sending TFR notification", ret);
            SPutMsg(buf);
         }
      }
   }

   return 0;
}

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision History:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

