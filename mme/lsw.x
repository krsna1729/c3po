/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter T6A Interface

     Type:     C header file

     Desc:     Structures, variables, typedefs and prototypes
               required at the management interface.

     File:     lsw.x

     Sid:

     Prg:      bw

*********************************************************************21*/

#ifndef __LSWX__
#define __LSWX__

#ifdef __cplusplus
extern "C" {
#endif

/* General configuration.
 */
typedef struct swGenCfg
{
   //Pst          lmPst;          /* for layer manager */
   //Pst          mmePst;

   Region       initRegion;
   Pool         initPool;
} SwGenCfg;

/* General statistics.
 */
typedef struct swGenSts
{
   StsCntr      numErrors;     /* num of errors */
} SwGenSts;

typedef struct swSta
{
   State        state;          /* DNS state */
} SwSta;

/* Unsolicited status indication.
 */
typedef struct swAlarmInfo
{
   SpId         spId;           /* sap Id */
   U8           type;           /* which member of inf is present */
   union
   {
      State     state;          /* TSAP state */
      State     conState;       /* connection state */
      Mem       mem;            /* region/pool (resource related) */
      U8        parType;        /* parameter type */
   } inf;

} SwAlarmInfo;

#ifdef DEBUGP
/* Debug control.
 */
typedef struct swDbgCntrl
{
   U32          dbgMask;        /* Debug mask */
} SwDbgCntrl;
#endif


/* Trace control.
 */
typedef struct swTrcCntrl
{
   SpId         sapId;          /* sap Id */
   S16          trcLen;         /* length to trace */

} SwTrcCntrl;



/* Configuration.
 */
typedef struct swCfg
{
   union
   {
      SwGenCfg  swGen;          /* general configuration */
   } s;

} SwCfg;


/* Statistics.
 */
typedef struct swSts
{
   DateTime     dt;             /* date and time */
   Duration     dura;           /* duration */

   union
   {
      SwGenSts  genSts;         /* general statistics */
   } s;

} SwSts;

/* Solicited status.
 */
typedef struct swSsta
{
   DateTime     dt;             /* date and time */

   union
   {
      SystemId  sysId;          /* system id */
      SwSta  swSta;             /* diameter status */
   } s;

} SwSsta;

/* Unsolicited status.
 */
typedef struct swUsta
{
   CmAlarm      alarm;          /* alarm */
   SwAlarmInfo  info;           /* alarm information */

} SwUsta;


/* Trace.
 */
typedef struct swTrc
{
   DateTime     dt;            /* date and time */
   U16          evnt;          /* event */
} SwTrc;

/* Control.
 */
typedef struct swCntrl
{
   DateTime     dt;             /* date and time */
   U8           action;         /* action */
   U8           subAction;      /* sub action */

   union
   {
      SwTrcCntrl        trcDat;         /* trace length */
      ProcId            dstProcId;      /* destination procId */
      Route             route;          /* route */
      Priority          priority;       /* priority */

#ifdef DEBUGP
      SwDbgCntrl        swDbg;          /* debug printing control */
#endif

   } ctlType;

} SwCntrl;

/* Management.
 */
typedef struct swMngmt
{
   Header       hdr;            /* header */
   CmStatus     cfm;            /* response status/confirmation */

   union
   {
      SwCfg     cfg;            /* configuration */
      SwSts     sts;            /* statistics */
      SwSsta    ssta;           /* solicited status */
      SwUsta    usta;           /* unsolicited status */
      SwTrc     trc;            /* trace */
      SwCntrl   cntrl;          /* control */

   } t;
} SwMngmt;


/* Layer management interface primitive types.
 */
typedef S16  (*LswCfgReq)       ARGS((Pst *pst, SwMngmt *cfg));
typedef S16  (*LswCfgCfm)       ARGS((Pst *pst, SwMngmt *cfg));
typedef S16  (*LswCntrlReq)     ARGS((Pst *pst, SwMngmt *cntrl));
typedef S16  (*LswCntrlCfm)     ARGS((Pst *pst, SwMngmt *cntrl));
typedef S16  (*LswStsReq)       ARGS((Pst *pst, Action action,
                                      SwMngmt *sts));
typedef S16  (*LswStsCfm)       ARGS((Pst *pst, SwMngmt *sts));
typedef S16  (*LswStaReq)       ARGS((Pst *pst, SwMngmt *sta));
typedef S16  (*LswStaInd)       ARGS((Pst *pst, SwMngmt *sta));
typedef S16  (*LswStaCfm)       ARGS((Pst *pst, SwMngmt *sta));
typedef S16  (*LswTrcInd)       ARGS((Pst *pst, SwMngmt *trc,
                                      Buffer *mBuf));

//typedef S16 (*LswInitAttachSgw) ARGS((Pst *pst, DnsQueryData *qd));

/* Layer management interface primitives.
 */
//#ifdef SW
EXTERN S16  SwMiLswCfgReq       ARGS((Pst *pst, SwMngmt *cfg));
EXTERN S16  SwMiLswCfgCfm       ARGS((Pst *pst, SwMngmt *cfg));
EXTERN S16  SwMiLswCntrlReq     ARGS((Pst *pst, SwMngmt *cntrl));
EXTERN S16  SwMiLswCntrlCfm     ARGS((Pst *pst, SwMngmt *cntrl));
EXTERN S16  SwMiLswStsReq       ARGS((Pst *pst, Action action,
                                      SwMngmt *sts));
EXTERN S16  SwMiLswStsCfm       ARGS((Pst *pst, SwMngmt *sts));
EXTERN S16  SwMiLswStaReq       ARGS((Pst *pst, SwMngmt *sta));
EXTERN S16  SwMiLswStaCfm       ARGS((Pst *pst, SwMngmt *sta));
EXTERN S16  SwMiLswStaInd       ARGS((Pst *pst, SwMngmt *sta));
EXTERN S16  SwMiLswTrcInd       ARGS((Pst *pst, SwMngmt *trc,
                                      Buffer *mBuf));
//#endif /* SW */

#ifdef SM
EXTERN S16  SmMiLswCfgReq       ARGS((Pst *pst, SwMngmt *cfg));
EXTERN S16  SmMiLswCfgCfm       ARGS((Pst *pst, SwMngmt *cfg));
EXTERN S16  SmMiLswCntrlReq     ARGS((Pst *pst, SwMngmt *cntrl));
EXTERN S16  SmMiLswCntrlCfm     ARGS((Pst *pst, SwMngmt *cntrl));
EXTERN S16  SmMiLswStsReq       ARGS((Pst *pst, Action action,
                                      SwMngmt *sts));
EXTERN S16  SmMiLswStsCfm       ARGS((Pst *pst, SwMngmt *sts));
EXTERN S16  SmMiLswStaReq       ARGS((Pst *pst, SwMngmt *sta));
EXTERN S16  SmMiLswStaInd       ARGS((Pst *pst, SwMngmt *sta));
EXTERN S16  SmMiLswStaCfm       ARGS((Pst *pst, SwMngmt *sta));
EXTERN S16  SmMiLswTrcInd       ARGS((Pst *pst, SwMngmt *trc,
                                      Buffer *mBuf));
#endif /* SM */



/* Packing and unpacking functions.
 */
EXTERN S16  cmPkLswCfgReq       ARGS((Pst *pst, SwMngmt *cfg));
EXTERN S16  cmPkLswCfgCfm       ARGS((Pst *pst, SwMngmt *cfg));
EXTERN S16  cmPkLswCntrlReq     ARGS((Pst *pst, SwMngmt *cntrl));
EXTERN S16  cmPkLswCntrlCfm     ARGS((Pst *pst, SwMngmt *cntrl));
EXTERN S16  cmPkLswStsReq       ARGS((Pst *pst, Action action, SwMngmt *sts));
EXTERN S16  cmPkLswStsCfm       ARGS((Pst *pst, SwMngmt *sts));
EXTERN S16  cmPkLswStaReq       ARGS((Pst *pst, SwMngmt *sta));
EXTERN S16  cmPkLswStaInd       ARGS((Pst *pst, SwMngmt *sta));
EXTERN S16  cmPkLswTrcInd       ARGS((Pst *pst, SwMngmt *trc, Buffer *mBuf));
EXTERN S16  cmPkLswStaCfm       ARGS((Pst *pst, SwMngmt *sta));

EXTERN S16  cmUnpkLswCfgReq     ARGS((LswCfgReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLswCfgCfm     ARGS((LswCfgCfm func, Pst *pst,
                                    Buffer *mBuf));
EXTERN S16  cmUnpkLswCntrlReq   ARGS((LswCntrlReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLswCntrlCfm   ARGS((LswCntrlCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLswStsReq     ARGS((LswStsReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLswStsCfm     ARGS((LswStsCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLswStaReq     ARGS((LswStaReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLswStaInd     ARGS((LswStaInd func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLswStaCfm     ARGS((LswStaCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLswTrcInd     ARGS((LswTrcInd func, Pst *pst,
                                      Buffer *mBuf));
/* Layer manager activation functions.
 */
#if 0
EXTERN S16  smDsActvTsk         ARGS((Pst *pst, Buffer *mBuf));
EXTERN S16  smDsActvInit        ARGS((Ent ent, Inst inst,
                                      Region region, Reason reason));
#endif

#ifdef SS_MULTIPLE_PROCS
EXTERN S16 swActvInit       ARGS((ProcId procId,
                                  Ent entity,
                                  Inst inst,
                                  Region region,
                                  Reason reason,
                                  Void **xxCb));
#else /* SS_MULTIPLE_PROCS */

EXTERN S16  swActvInit          ARGS((Ent ent, Inst inst,
                                      Region region, Reason reason));
#endif /* SS_MULTIPLE_PROCS */
EXTERN S16  swActvTsk           ARGS((Pst *pst, Buffer *mBuf));

#if 0
EXTERN S16 cmPkLdsInitAttachSgw   ARGS((DnsQueryData *qd));
EXTERN S16 cmUnpkLdsInitAttachSgw ARGS((LdsInitAttachSgw func, Pst *pst, Buffer *mBuf));
EXTERN S16 SmMiLdsInitAttachSgw   ARGS((Pst *pst, DnsQueryData *qd));
#endif

#ifdef __cplusplus
}
#endif

#endif /* __LSWX__ */


/********************************************************************30**

         End of file:     lhi.x@@/main/10 - Tue Feb  1 16:04:21 2011

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat     init                  description
----------- --------- ---- -----------------------------------------------
1.1         ---       bw   1. initial release.
*********************************************************************91*/

