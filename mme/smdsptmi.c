/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*

Layer management provides the necessary functions to control and
monitor the condition of each protocol layer.

The following functions are provided in this file:

     SmMiLdsCfgReq      Configure Request
     SmMiLdsStaReq      Status Request
     SmMiLdsStsReq      Statistics Request
     SmMiLdsCntrlReq    Control Request

It is assumed that the following functions are provided in the
stack management body files:

     SmMiLhiStaInd      Status Indication
     SmMiLhiStaCfm      Status Confirm
     SmMiLhiStsCfm      Statistics Confirm
     SmMiLhiTrcInd      Trace Indication

*/


/* header include files (.h) */

#include "envopt.h"        /* environment options */
#include "envdep.h"        /* environment dependent */
#include "envind.h"        /* environment independent */
#include "gen.h"           /* general layer */
#include "cm5.h"           /* common timer */
#include "ssi.h"           /* system services */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* common sockets */
#include "lds.h"           /* HI layer management */
#include "cm_err.h"        /* common error */
#include "smhi_err.h"      /* HI error */

/* header/extern include files (.x) */

#include "gen.x"           /* general layer */
#include "ssi.x"           /* system services */
#include "cm5.x"           /* common timer */
#include "cm_lib.x"        /* Common linrary function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* common sockets */
#include "lds.x"           /* ds layer management */

#define  MAXDSMI  2

#ifndef  LCSMDSMILDS
#define  PTSMDSMILDS
#else
#ifndef   DS
#define  PTSMDSMILDS
#endif
#endif

#ifdef PTSMDSMILDS
PRIVATE S16 PtMiLdsCfgReq    ARGS((Pst *pst, DsMngmt *cfg));
PRIVATE S16 PtMiLdsStsReq    ARGS((Pst *pst, Action action, DsMngmt *sts));
PRIVATE S16 PtMiLdsCntrlReq  ARGS((Pst *pst, DsMngmt *cntrl));
PRIVATE S16 PtMiLdsStaReq    ARGS((Pst *pst, DsMngmt *sta));
#endif


/*
the following matrices define the mapping between the primitives
called by the layer management interface of TCP UDP Convergence Layer
and the corresponding primitives in TUCL

The parameter MAXHIMI defines the maximum number of layer manager
entities on top of TUCL . There is an array of functions per primitive
invoked by TCP UDP Conbvergence Layer. Every array is MAXHIMI long
(i.e. there are as many functions as the number of service users).

The dispatching is performed by the configurable variable: selector.
The selector is configured during general configuration.

The selectors are:

   0 - loosely coupled (#define LCSMHIMILHI) 2 - Lhi (#define HI)

*/


/* Configuration request primitive */

PRIVATE LdsCfgReq SmMiLdsCfgReqMt[MAXDSMI] =
{
#ifdef LCSMDSMILDS
   cmPkLdsCfgReq,          /* 0 - loosely coupled  */
#else
   PtMiLdsCfgReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef DS
   DsMiLdsCfgReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLdsCfgReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Control request primitive */

PRIVATE LdsCntrlReq SmMiLdsCntrlReqMt[MAXDSMI] =
{
#ifdef LCSMDSMILDS
   cmPkLdsCntrlReq,          /* 0 - loosely coupled  */
#else
   PtMiLdsCntrlReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef DS
   HiMiLsyCntrlReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLdsCntrlReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Statistics request primitive */

PRIVATE LdsStsReq SmMiLdsStsReqMt[MAXDSMI] =
{
#ifdef LCSMDSMILDS
   cmPkLdsStsReq,          /* 0 - loosely coupled  */
#else
   PtMiLdsStsReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef DS
   HiMiLsyStsReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLdsStsReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Status request primitive */

PRIVATE LdsStaReq SmMiLdsStaReqMt[MAXDSMI] =
{
#ifdef LCSMDSMILDS
   cmPkLdsStaReq,          /* 0 - loosely coupled  */
#else
   PtMiLdsStaReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef DS
   HiMiLsyStaReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLdsStaReq,          /* 1 - tightly coupled, portable */
#endif
};


/*
*     layer management interface functions
*/

/*
*
*       Fun:   Configuration request
*
*       Desc:  This function is used to configure  TUCL
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smdsptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLdsCfgReq
(
Pst *spst,                /* post structure */
DsMngmt *cfg              /* configure */
)
#else
PUBLIC S16 SmMiLdsCfgReq(spst, cfg)
Pst *spst;                /* post structure */
DsMngmt *cfg;             /* configure */
#endif
{
   TRC3(SmMiLdsCfgReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLdsCfgReqMt[spst->selector])(spst, cfg);
   RETVALUE(ROK);
} /* end of SmMiLdsCfgReq */



/*
*
*       Fun:   Statistics request
*
*       Desc:  This function is used to request statistics from
*              TUCL
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smdsptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLdsStsReq
(
Pst *spst,                /* post structure */
Action action,
DsMngmt *sts              /* statistics */
)
#else
PUBLIC S16 SmMiLdsStsReq(spst, action, sts)
Pst *spst;                /* post structure */
Action action;
DsMngmt *sts;             /* statistics */
#endif
{
   TRC3(SmMiLdsStsReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLdsStsReqMt[spst->selector])(spst, action, sts);
   RETVALUE(ROK);
} /* end of SmMiLdsStsReq */


/*
*
*       Fun:   Control request
*
*       Desc:  This function is used to send control request to
*              TUCL
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smdsptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLdsCntrlReq
(
Pst *spst,                 /* post structure */
DsMngmt *cntrl            /* control */
)
#else
PUBLIC S16 SmMiLdsCntrlReq(spst, cntrl)
Pst *spst;                 /* post structure */
DsMngmt *cntrl;           /* control */
#endif
{
   TRC3(SmMiLdsCntrlReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLdsCntrlReqMt[spst->selector])(spst, cntrl);
   RETVALUE(ROK);
} /* end of SmMiLdsCntrlReq */


/*
*
*       Fun:   Status request
*
*       Desc:  This function is used to send a status request to
*              TUCL
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smdsptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLdsStaReq
(
Pst *spst,                /* post structure */
DsMngmt *sta              /* status */
)
#else
PUBLIC S16 SmMiLdsStaReq(spst, sta)
Pst *spst;                /* post structure */
DsMngmt *sta;             /* status */
#endif
{
   TRC3(SmMiLdsStaReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLdsStaReqMt[spst->selector])(spst, sta);
   RETVALUE(ROK);
} /* end of SmMiLdsStaReq */

#ifdef PTSMDSMILDS

/*
 *             Portable Functions
 */

/*
*
*       Fun:   Portable configure Request- TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smdsptmi.c
*
*/

#ifdef ANSI
PRIVATE S16 PtMiLdsCfgReq
(
Pst *spst,                  /* post structure */
DsMngmt *cfg                /* configure */
)
#else
PRIVATE S16 PtMiLdsCfgReq(spst, cfg)
Pst *spst;                  /* post structure */
DsMngmt *cfg;               /* configure */
#endif
{
  TRC3(PtMiLdsCfgReq)

  UNUSED(spst);
  UNUSED(cfg);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMHILOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
              ERRCLS_DEBUG, ESMHI002, 0, "PtMiLdsCfgReq () Failed");
#endif

  RETVALUE(ROK);
} /* end of PtMiLdsCfgReq */



/*
*
*       Fun:   Portable statistics Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smdsptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLdsStsReq
(
Pst *spst,                  /* post structure */
Action action,
DsMngmt *sts                /* statistics */
)
#else
PRIVATE S16 PtMiLdsStsReq(spst, action, sts)
Pst *spst;                  /* post structure */
Action action;
DsMngmt *sts;               /* statistics */
#endif
{
  TRC3(PtMiLdsStsReq)

  UNUSED(spst);
  UNUSED(action);
  UNUSED(sts);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMHILOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
                ERRCLS_DEBUG, ESMHI003, 0, "PtMiLdsStsReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLdsStsReq */


/*
*
*       Fun:   Portable control Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smdsptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLdsCntrlReq
(
Pst *spst,                  /* post structure */
DsMngmt *cntrl              /* control */
)
#else
PRIVATE S16 PtMiLdsCntrlReq(spst, cntrl)
Pst *spst;                  /* post structure */
DsMngmt *cntrl;             /* control */
#endif
{
  TRC3(PtMiLdsCntrlReq)

  UNUSED(spst);
  UNUSED(cntrl);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMHILOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
             ERRCLS_DEBUG, ESMHI004, 0, "PtMiLdsCntrlReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLdsCntrlReq */


/*
*
*       Fun:   Portable status Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smdsptmi.c
*
*/

#ifdef ANSI
PRIVATE S16 PtMiLdsStaReq
(
Pst *spst,                  /* post structure */
DsMngmt *sta                /* status */
)
#else
PRIVATE S16 PtMiLdsStaReq(spst, sta)
Pst *spst;                  /* post structure */
DsMngmt *sta;               /* status */
#endif
{
  TRC3(PtMiLdsStaReq);

  UNUSED(spst);
  UNUSED(sta);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMHILOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
                ERRCLS_DEBUG,ESMHI005, 0, "PtMiLdsStaReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLdsStaReq */

#endif /* PTSMDSMILDS */


/********************************************************************30**

         End of file:     smdsptmi.c@@/main/5 - Mon Mar  3 20:09:32 2008

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      asa  1. initial release.
/main/2     ---      cvp  1. changed the copyright header.
/main/3     ---      cvp  1. changed the copyright header.
/main/4      ---      kp   1. Updated for release 1.5.
/main/5      ---       hs   1. Updated for release of 2.1
*********************************************************************91*/
