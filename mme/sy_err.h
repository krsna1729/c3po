/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     S6A Convergence Layer error file.

     Type:     C include file

     Desc:     Error Hash Defines required by SY layer

     File:     sy_err.h

     Sid:

     Prg:      bw

*********************************************************************21*/

#ifndef __SYERRH__
#define __SYERRH__



#if (ERRCLASS & ERRCLS_DEBUG)
#define SYLOGERROR_DEBUGPST(procId, ent, errCode, errVal, inst, errDesc)        \
        SLogError(ent, inst, procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,     \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define SYLOGERROR_DEBUGPST(procId, ent, errCode, errVal, inst, errDesc)
#endif /* ERRCLS_DEBUG */

/* hi031.201: Fix for g++ compilation warning*/
#if (ERRCLASS & ERRCLS_DEBUG)
#define SYLOGERROR_DEBUG(errCode, errVal, inst, errDesc)        \
        SLogError(syCb.init.ent, inst, syCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define SYLOGERROR_DEBUG(errCode, errVal, inst, errDesc)
#endif

#if (ERRCLASS & ERRCLS_INT_PAR)
#define SYLOGERROR_INT_PAR(errCode, errVal, inst, errDesc)      \
        SLogError(syCb.init.ent, inst, syCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_INT_PAR,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define SYLOGERROR_INT_PAR(errCode, errVal, inst, errDesc)
#endif /* ERRCLS_INT_PAR */

#if (ERRCLASS & ERRCLS_ADD_RES)
#define SYLOGERROR_ADD_RES(errCode, errVal, inst, errDesc)      \
        SLogError(syCb.init.ent, inst, syCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_ADD_RES,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define SYLOGERROR_ADD_RES(errCode, errVal, errDesc, inst)
#endif /* ERRCLS_ADD_RES */

#define   ESYBASE     0
#define   ERRSY       (ESYBASE   +0)    /* reserved */

#define   ESY001      (ERRSY +    1)    /*   hi_accsh.c: 203 */
#define   ESY002      (ERRSY +    2)    /*   hi_accsh.c: 288 */
#define   ESY003      (ERRSY +    3)    /*   hi_accsh.c: 290 */

#define   ESY004      (ERRSY +    4)    /*   hi_ex_ms.c: 204 */
#define   ESY005      (ERRSY +    5)    /*   hi_ex_ms.c: 242 */
#define   ESY006      (ERRSY +    6)    /*   hi_ex_ms.c: 394 */
#define   ESY007      (ERRSY +    7)    /*   hi_ex_ms.c: 414 */
#define   ESY008      (ERRSY +    8)    /*   hi_ex_ms.c: 422 */

#define   ESY009      (ERRSY +    9)    /*         hi.h: 357 */
#define   ESY010      (ERRSY +   10)    /*         hi.h: 372 */
#define   ESY011      (ERRSY +   11)    /*         hi.h: 382 */

#define   ESY012      (ERRSY +   12)    /*    tl_bdy1.c: 434 */
#define   ESY013      (ERRSY +   13)    /*    tl_bdy1.c: 532 */
#define   ESY014      (ERRSY +   14)    /*    tl_bdy1.c: 653 */
#define   ESY015      (ERRSY +   15)    /*    tl_bdy1.c: 698 */
#define   ESY016      (ERRSY +   16)    /*    tl_bdy1.c: 759 */
#define   ESY017      (ERRSY +   17)    /*    tl_bdy1.c: 811 */
#define   ESY018      (ERRSY +   18)    /*    tl_bdy1.c: 837 */
#define   ESY019      (ERRSY +   19)    /*    tl_bdy1.c: 896 */
#define   ESY020      (ERRSY +   20)    /*    tl_bdy1.c:1109 */
#define   ESY021      (ERRSY +   21)    /*    tl_bdy1.c:1183 */
#define   ESY022      (ERRSY +   22)    /*    tl_bdy1.c:1257 */
#define   ESY023      (ERRSY +   23)    /*    tl_bdy1.c:1374 */
#define   ESY024      (ERRSY +   24)    /*    tl_bdy1.c:1421 */
#define   ESY025      (ERRSY +   25)    /*    tl_bdy1.c:1435 */
#define   ESY026      (ERRSY +   26)    /*    tl_bdy1.c:1454 */
#define   ESY027      (ERRSY +   27)    /*    tl_bdy1.c:1668 */
#define   ESY028      (ERRSY +   28)    /*    tl_bdy1.c:1721 */
#define   ESY029      (ERRSY +   29)    /*    tl_bdy1.c:1755 */
#define   ESY030      (ERRSY +   30)    /*    tl_bdy1.c:1775 */
#define   ESY031      (ERRSY +   31)    /*    tl_bdy1.c:1960 */
#define   ESY032      (ERRSY +   32)    /*    tl_bdy1.c:2104 */
#define   ESY033      (ERRSY +   33)    /*    tl_bdy1.c:2149 */
#define   ESY034      (ERRSY +   34)    /*    tl_bdy1.c:2204 */
#define   ESY035      (ERRSY +   35)    /*    tl_bdy1.c:2225 */
#define   ESY036      (ERRSY +   36)    /*    tl_bdy1.c:2358 */
#define   ESY037      (ERRSY +   37)    /*    tl_bdy1.c:2408 */
#define   ESY038      (ERRSY +   38)    /*    tl_bdy1.c:2421 */
#define   ESY039      (ERRSY +   39)    /*    tl_bdy1.c:2435 */
#define   ESY040      (ERRSY +   40)    /*    tl_bdy1.c:2452 */
#define   ESY041      (ERRSY +   41)    /*    tl_bdy1.c:2560 */
#define   ESY042      (ERRSY +   42)    /*    tl_bdy1.c:2581 */
#define   ESY043      (ERRSY +   43)    /*    tl_bdy1.c:2596 */
#define   ESY044      (ERRSY +   44)    /*    tl_bdy1.c:2825 */
#define   ESY045      (ERRSY +   45)    /*    tl_bdy1.c:2864 */
#define   ESY046      (ERRSY +   46)    /*    tl_bdy1.c:2916 */
#define   ESY047      (ERRSY +   47)    /*    tl_bdy1.c:3027 */
#define   ESY048      (ERRSY +   48)    /*    tl_bdy1.c:3063 */
#define   ESY049      (ERRSY +   49)    /*    tl_bdy1.c:3077 */
#define   ESY050      (ERRSY +   50)    /*    tl_bdy1.c:3284 */
#define   ESY051      (ERRSY +   51)    /*    tl_bdy1.c:3316 */
#define   ESY052      (ERRSY +   52)    /*    tl_bdy1.c:3375 */
#define   ESY053      (ERRSY +   53)    /*    tl_bdy1.c:3482 */
#define   ESY054      (ERRSY +   54)    /*    tl_bdy1.c:3938 */
#define   ESY055      (ERRSY +   55)    /*    tl_bdy1.c:4240 */
#define   ESY056      (ERRSY +   56)    /*    tl_bdy1.c:4412 */
#define   ESY057      (ERRSY +   57)    /*    tl_bdy1.c:4618 */
#define   ESY058      (ERRSY +   58)    /*    tl_bdy1.c:4748 */
#define   ESY059      (ERRSY +   59)    /*    tl_bdy1.c:4888 */
#define   ESY060      (ERRSY +   60)    /*    tl_bdy1.c:4925 */
#define   ESY061      (ERRSY +   61)    /*    tl_bdy1.c:4940 */
#define   ESY062      (ERRSY +   62)    /*    tl_bdy1.c:4952 */
#define   ESY063      (ERRSY +   63)    /*    tl_bdy1.c:5077 */

#define   ESY064      (ERRSY +   64)    /*    tl_bdy2.c: 220 */
#define   ESY065      (ERRSY +   65)    /*    tl_bdy2.c: 243 */
#define   ESY066      (ERRSY +   66)    /*    tl_bdy2.c: 297 */
#define   ESY067      (ERRSY +   67)    /*    tl_bdy2.c: 310 */
#define   ESY068      (ERRSY +   68)    /*    tl_bdy2.c:1445 */
#define   ESY069      (ERRSY +   69)    /*    tl_bdy2.c:1545 */
#define   ESY070      (ERRSY +   70)    /*    tl_bdy2.c:1552 */
#define   ESY071      (ERRSY +   71)    /*    tl_bdy2.c:1559 */
#define   ESY072      (ERRSY +   72)    /*    tl_bdy2.c:1576 */
#define   ESY073      (ERRSY +   73)    /*    tl_bdy2.c:2119 */
#define   ESY074      (ERRSY +   74)    /*    tl_bdy2.c:2126 */
#define   ESY075      (ERRSY +   75)    /*    tl_bdy2.c:2191 */
#define   ESY076      (ERRSY +   76)    /*    tl_bdy2.c:2207 */
#define   ESY077      (ERRSY +   77)    /*    tl_bdy2.c:2221 */
#define   ESY078      (ERRSY +   78)    /*    tl_bdy2.c:2230 */
#define   ESY079      (ERRSY +   79)    /*    tl_bdy2.c:2301 */
#define   ESY080      (ERRSY +   80)    /*    tl_bdy2.c:2359 */
#define   ESY081      (ERRSY +   81)    /*    tl_bdy2.c:2411 */
#define   ESY082      (ERRSY +   82)    /*    tl_bdy2.c:3174 */
#define   ESY083      (ERRSY +   83)    /*    tl_bdy2.c:3188 */
#define   ESY084      (ERRSY +   84)    /*    tl_bdy2.c:3302 */
#define   ESY085      (ERRSY +   85)    /*    tl_bdy2.c:5146 */
#define   ESY086      (ERRSY +   86)    /*    tl_bdy2.c:5186 */
#define   ESY087      (ERRSY +   87)    /*    tl_bdy2.c:5321 */

#define   ESY088      (ERRSY +   88)    /*    tl_bdy3.c:1800 */
#define   ESY089      (ERRSY +   89)    /*    tl_bdy3.c:1815 */
#define   ESY090      (ERRSY +   90)    /*    tl_bdy3.c:1851 */
#define   ESY091      (ERRSY +   91)    /*    tl_bdy3.c:2531 */
#define   ESY092      (ERRSY +   92)    /*    tl_bdy3.c:4364 */
#define   ESY093      (ERRSY +   93)    /*    tl_bdy3.c:4377 */
#define   ESY094      (ERRSY +   94)    /*    tl_bdy3.c:4391 */
#define   ESY095      (ERRSY +   95)    /*    tl_bdy3.c:4425 */
#define   ESY096      (ERRSY +   96)    /*    tl_bdy3.c:4441 */
/* hi005.201 Introducing a new error macros for conjestion timer
 *           changes */

#define   ESY097      (ERRSY +   97)    /*    tl_bdy2.c:5729 */
#define   ESY098      (ERRSY +   98)    /*    tl_bdy2.c:5735 */
#define   ESY099      (ERRSY +   99)    /*    tl_bdy2.c:5753 */
#define   ESY100      (ERRSY +   100)    /*    tl_bdy2.c:5753 */
/* hi009.201  rss   1. Added ESY101 macro */
#define   ESY101      (ERRSY +   101)    /*    tl_bdy2.c:5753 */
#endif /* __SYERRH__ */



/********************************************************************30**

         End of file:

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

