/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

/********************************************************************20**

     Name:     Diameter SGD Interface

     Type:     C source file

     Desc:     C source code for common packing and un-packing functions for
               layer management interface(LSX).

     File:     lsx.c

     Sid:

     Prg:      rakesh kumar suman

*********************************************************************21*/

/*
*     This software may be combined with the following TRILLIUM
*     software:
*
*     part no.                      description
*     --------    ----------------------------------------------
*
*/


/* header include files (.h) */
#include "envopt.h"        /* environment options */
#include "envdep.h"        /* environment dependent */
#include "envind.h"        /* environment independent */
#include "gen.h"           /* general layer */
#include "cm_gen.h"        /* common pack/unpack defines */
#include "cm_os.h"
#include "ssi.h"           /* system services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */

/* header/extern include files (.x) */
#include "gen.x"           /* general layer */
#include "cm_os.x"
#include "ssi.x"           /* system services */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common linrary function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */

#include "lsx.h"
#include "lsx.x"
#include "sx_err.h"
#include "sx.h"
#include "sx.x"

#include "cm_lib.x"            /* has the prototype of cmMemset() */


/* local defines */

/* local typedefs */

/* local externs */

/* forward references */



/* functions in other modules */

/* public variable declarations */

/* private variable declarations */

#ifdef LCLSX



/*
*     layer management interface packing functions
*/


/*
*
*       Fun:   Pack Config Request
*
*       Desc:  This function is used to a pack config request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsxCfgReq
(
Pst *pst,                 /* post structure */
SxMngmt *cfg              /* configuration */
)
#else
PUBLIC S16 cmPkLsxCfgReq(pst, cfg)
Pst *pst;                 /* post structure */
SxMngmt *cfg;             /* configuration */
#endif
{
   Buffer *mBuf;            /* message buffer */

   TRC3(cmPkLsxCfgReq)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch (cfg->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSXLOGERROR(pst, ELSX001, cfg->hdr.elmId.elmnt,
                     "cmPkLsxCfgReq() Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of sxitch */

   CMCHKPKLOG(cmPkCmStatus, &cfg->cfm, mBuf, ELSX002, pst);
   CMCHKPKLOG(cmPkHeader, &cfg->hdr, mBuf, ELSX003, pst);
   pst->event = (Event)EVTLSXCFGREQ;

/* fill interface version number in pst */
#ifdef TSX_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LSXIFVER;
#endif /* TSX_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Config Confirm
*
*       Desc:  This function is used to a pack config confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsxCfgCfm
(
Pst *pst,                 /* post structure */
SxMngmt *cfm              /* confirm */
)
#else
PUBLIC S16 cmPkLsxCfgCfm(pst, cfm)
Pst *pst;                 /* post structure */
SxMngmt *cfm;             /* confirm */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsxCfgCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   CMCHKPKLOG(cmPkCmStatus, &cfm->cfm, mBuf, ELSX004, pst);
   CMCHKPKLOG(cmPkHeader,   &cfm->hdr, mBuf, ELSX005, pst);

   pst->event = EVTLSXCFGCFM;
   RETVALUE(SPstTsk(pst, mBuf));

} /* end of cmPkLsxCfgCfm */


/*
*
*       Fun:   Pack Control Request
*
*       Desc:  This function is used to pack the control request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsxCntrlReq
(
Pst *pst,                 /* post structure */
SxMngmt *ctl              /* configuration */
)
#else
PUBLIC S16 cmPkLsxCntrlReq(pst, ctl)
Pst *pst;                 /* post structure */
SxMngmt *ctl;             /* configuration */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsxCntrlReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   switch(ctl->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSXLOGERROR(pst, ELSX006, ctl->hdr.elmId.elmnt,
                     "cmPkLsxCntrlReq () Failed : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of sxitch */

   CMCHKPKLOG(SPkU8,       ctl->t.cntrl.subAction, mBuf, ELSX007, pst);
   CMCHKPKLOG(SPkU8,       ctl->t.cntrl.action,    mBuf, ELSX008, pst);
   CMCHKPKLOG(cmPkDateTime, &ctl->t.cntrl.dt,       mBuf, ELSX009, pst);

   CMCHKPKLOG(cmPkCmStatus, &ctl->cfm, mBuf, ELSX010, pst);
   CMCHKPKLOG(cmPkHeader,   &ctl->hdr, mBuf, ELSX011, pst);
   pst->event = (Event)EVTLSXCNTRLREQ;

/* hi009.104 - fill interface version number in pst */
#ifdef TSX_ROLL_UPGRADE_SUPPORT
   pst->intfVer = (CmIntfVer) LSXIFVER;
#endif /* TSX_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Control Confirm
*
*       Desc:  This function is used to pack the control confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsxCntrlCfm
(
Pst *pst,                 /* post structure */
SxMngmt *cfm              /* confirm */
)
#else
PUBLIC S16 cmPkLsxCntrlCfm(pst, cfm)
Pst *pst;                 /* post structure */
SxMngmt *cfm;             /* confirm */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsxCntrlCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   /* pack status */
   CMCHKPKLOG(cmPkCmStatus, &cfm->cfm, mBuf, ELSX012, pst);

   /* pack header */
   CMCHKPKLOG(cmPkHeader, &cfm->hdr, mBuf, ELSX013, pst);

   pst->event = EVTLSXCNTRLCFM;
   RETVALUE(SPstTsk(pst, mBuf));

} /* end of cmPkLsxCntrlCfm */



/*
*
*       Fun:   Pack Statistics Request
*
*       Desc:  This function is used to pack the statistics request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsxStsReq
(
Pst *pst,                 /* post structure */
Action action,            /* action */
SxMngmt *sts              /* statistics */
)
#else
PUBLIC S16 cmPkLsxStsReq(pst, action, sts)
Pst *pst;                 /* post structure */
Action action;            /* action */
SxMngmt *sts;             /* statistics */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsxStsReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sts->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSXLOGERROR(pst, ELSX014, sts->hdr.elmId.elmnt,
                     "cmPkLswStsReq () Failed : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }
   CMCHKPKLOG(cmPkCmStatus, &sts->cfm, mBuf, ELSX015, pst);
   CMCHKPKLOG(cmPkHeader,   &sts->hdr, mBuf, ELSX016, pst);
   CMCHKPKLOG(cmPkAction,   action,    mBuf, ELSX017, pst);
   pst->event = EVTLSXSTSREQ;

/* hi009.104 - fill interface version number in pst */
#ifdef TSX_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LSXIFVER;
#endif /* TSX_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLsxStsReq */


/*
*
*       Fun:   Pack Statistics Confirm
*
*       Desc:  This function is used to pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsxStsCfm
(
Pst *pst,                 /* post structure */
SxMngmt *sts              /* statistics */
)
#else
PUBLIC S16 cmPkLsxStsCfm(pst, sts)
Pst *pst;                 /* post structure */
SxMngmt *sts;             /* statistics */
#endif

{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsxStsCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sts->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSXLOGERROR(pst, ELSX018, sts->hdr.elmId.elmnt,
                     "cmPkLsxStsCfm() : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   CMCHKPKLOG(cmPkDuration, &sts->t.sts.dura, mBuf, ELSX019, pst);
   CMCHKPKLOG(cmPkDateTime, &sts->t.sts.dt, mBuf, ELSX020, pst);

   CMCHKPKLOG(cmPkCmStatus, &sts->cfm, mBuf, ELSX021, pst);
   CMCHKPKLOG(cmPkHeader,   &sts->hdr, mBuf, ELSX022, pst);
   pst->event = (Event)EVTLSXSTSCFM;
   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Status Request
*
*       Desc:  This function is used to pack the status request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsxStaReq
(
Pst *pst,                 /* post structure */
SxMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 cmPkLsxStaReq(pst, sta)
Pst *pst;                 /* post structure */
SxMngmt *sta;             /* solicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsxStaReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sta->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSXLOGERROR(pst, ELSX023, sta->hdr.elmId.elmnt,
                     "cmPkLsxStaReq () : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELSX024, pst);
   CMCHKPKLOG(cmPkHeader, &sta->hdr, mBuf, ELSX025, pst);
   pst->event = (Event)EVTLSXSTAREQ;

   /* hi009.104 - fill interface version number in pst */
#ifdef TSX_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LSXIFVER;
#endif /* TSX_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Status Confirm
*
*       Desc:  This function is used to pack the status confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsxStaCfm
(
Pst *pst,                 /* post structure */
SxMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 cmPkLsxStaCfm(pst, sta)
Pst *pst;                 /* post structure */
SxMngmt *sta;             /* solicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsxStaCfm)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   switch(sta->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSXLOGERROR(pst, ELSX026, sta->hdr.elmId.elmnt,
                     "cmPkLsxStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   /* date */
   CMCHKPKLOG(cmPkDateTime, &sta->t.ssta.dt, mBuf, ELSX027, pst);

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELSX028, pst);
   CMCHKPKLOG(cmPkHeader,   &sta->hdr, mBuf, ELSX029, pst);
   pst->event = EVTLSXSTACFM;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLsxStaCfm */


/*
*
*       Fun:   Pack Status Indication
*
*       Desc:  This function is used to pack the status indication
*              primitive to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsxStaInd
(
Pst *pst,                 /* post structure */
SxMngmt *sta              /* unsolicited status */
)
#else
PUBLIC S16 cmPkLsxStaInd(pst, sta)
Pst *pst;                 /* post structure */
SxMngmt *sta;             /* unsolicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsxStaInd)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   if(sta->t.usta.info.type != LSX_ALARMINFO_TYPE_NTPRSNT)
   {
      switch(sta->t.usta.info.type)
      {
         case STGEN:
         {
            break;
         }
         default:
#if (ERRCLASS & ERRCLS_DEBUG)
            LSXLOGERROR(pst, ELSX030, sta->t.usta.info.type,
                        "cmPkLsxStaInd () Failed");
#endif
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
      }
   }
   CMCHKPKLOG(SPkU8,   sta->t.usta.info.type, mBuf, ELSX031, pst);
   CMCHKPKLOG(cmPkSpId,    sta->t.usta.info.spId,  mBuf, ELSX032, pst);
   CMCHKPKLOG(cmPkCmAlarm, &sta->t.usta.alarm,     mBuf, ELSX033, pst);

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELSX034, pst);
   CMCHKPKLOG(cmPkHeader,   &sta->hdr, mBuf, ELSX035, pst);
   pst->event = (Event)EVTLSXSTAIND;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLsxStaInd */



/*
*
*       Fun:   Pack Trace Indication
*
*       Desc:  This function is used to pack the trace indication
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsxTrcInd
(
Pst *pst,                 /* post */
SxMngmt *trc,             /* trace */
Buffer  *mBuf             /* message buffer */
)
#else
PUBLIC S16 cmPkLsxTrcInd(pst, trc, mBuf)
Pst *pst;                 /* post */
SxMngmt *trc;             /* trace */
Buffer  *mBuf;            /* message buffer */
#endif
{

   TRC3(cmPkLsxTrcInd)

   if (mBuf == NULLP)
   {
      if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
      {
         RETVALUE(RFAILED);
      }
   }

   CMCHKPKLOG(SPkU16, trc->t.trc.evnt, mBuf, ELSX036, pst);
   CMCHKPKLOG(cmPkDateTime, &trc->t.trc.dt, mBuf, ELSX037, pst);

   CMCHKPKLOG(cmPkCmStatus, &trc->cfm, mBuf, ELSX038, pst);
   CMCHKPKLOG(cmPkHeader,   &trc->hdr, mBuf, ELSX039, pst);
   pst->event = EVTLSXTRCIND;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLsxTrcInd */


/*
*     layer management interface un-packing functions
*/

/*
*
*       Fun:   Un-pack Config Request
*
*       Desc:  This function is used to a un-pack config request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsxCfgReq
(
LsxCfgReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf              /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsxCfgReq(func, pst, mBuf)
LsxCfgReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;             /* message buffer */
#endif
{
   SxMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsxCfgReq)

   LSX_ZERO((U8 *)&mgt, sizeof(SxMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSX040, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSX041, pst);

   switch (mgt.hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSXLOGERROR(pst, ELSX042, mgt.hdr.elmId.elmnt,
                     "cmUnpkLsxCfgReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of sxitch */

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);

   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Config Confirm
*
*       Desc:  This function is used to a un-pack config confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsxCfgCfm
(
LsxCfgCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsxCfgCfm(func, pst, mBuf)
LsxCfgCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SxMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsxCfgCfm)

   LSX_ZERO((U8*)&mgt, sizeof(SxMngmt))
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSX043, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSX044, pst);

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);

   RETVALUE(ROK);
} /* end of cmUnpkLsxCfgCfm */


/*
*
*       Fun:   Un-pack Control Request
*
*       Desc:  This function is used to un-pack the control request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsxCntrlReq
(
LsxCntrlReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf              /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsxCntrlReq(func, pst, mBuf)
LsxCntrlReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;             /* message buffer */
#endif
{
   SxMngmt  mgt;          /* configuration */

   TRC3(cmUnpkLsxCntrlReq)

   LSX_ZERO((U8 *)&mgt, sizeof(SxMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSX045, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSX046, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.cntrl.dt,  mBuf, ELSX047, pst);
   CMCHKUNPKLOG(SUnpkU8, &mgt.t.cntrl.action,    mBuf, ELSX048, pst);
   CMCHKUNPKLOG(SUnpkU8, &mgt.t.cntrl.subAction, mBuf, ELSX049, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
         if(mgt.t.cntrl.subAction == SADBG)
         {
#ifdef DEBUGP
           CMCHKUNPKLOG(SUnpkU32, &(mgt.t.cntrl.ctlType.sxDbg.dbgMask),
                        mBuf, ELSX050, pst);
#endif
         }
         break;

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSXLOGERROR(pst, ELSX051, mgt.hdr.elmId.elmnt,
                     "cmUnpkLsxCntrlReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of sxitch */

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Control Confirm
*
*       Desc:  This function is used to un-pack the control confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsxCntrlCfm
(
LsxCntrlCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsxCntrlCfm(func, pst, mBuf)
LsxCntrlCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{
   SxMngmt  cfm;            /* configuration */

   TRC3(cmUnpkLsxCntrlCfm)

   CMCHKUNPKLOG(cmUnpkHeader,   &cfm.hdr, mBuf, ELSX052, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &cfm.cfm, mBuf, ELSX053, pst);

   (Void)SPutMsg(mBuf);
   (*func)(pst, &cfm);
   RETVALUE(ROK);
} /* end of cmUnpkLsxCntrlCfm */



/*
*
*       Fun:   Un-pack Statistics Request
*
*       Desc:  This function is used to un-pack the statistics request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsxStsReq
(
LsxStsReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsxStsReq(func, pst, mBuf)
LsxStsReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SxMngmt  mgt;            /* configuration */
   /* lhi_c_001.main_11: Fix for Klockworks issue */
   Action   action = 0;         /* action type */

   TRC3(cmUnpkLsxStsReq)

   LSX_ZERO((U8 *)&mgt, sizeof(SxMngmt));
   CMCHKUNPKLOG(cmUnpkAction,   &action,  mBuf, ELSX054, pst);
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSX055, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSX056, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSXLOGERROR(pst, ELSX057, mgt.hdr.elmId.elmnt,
                     "cmUnpkLsxStsReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, action, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLsxStsReq */


/*
*
*       Fun:   Un-pack Statistics Confirm
*
*       Desc:  This function is used to un-pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsxStsCfm
(
LsxStsCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsxStsCfm(func, pst, mBuf)
LsxStsCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SxMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsxStsCfm)

   LSX_ZERO((U8 *)&mgt, sizeof(SxMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSX058, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSX059, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.sts.dt,   mBuf, ELSX060, pst);
   CMCHKUNPKLOG(cmUnpkDuration, &mgt.t.sts.dura, mBuf, ELSX061, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSXLOGERROR(pst, ELSX062, mgt.hdr.elmId.elmnt,
                     "cmUnpkLsxStsCfm () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Status Request
*
*       Desc:  This function is used to un-pack the status request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsxStaReq
(
LsxStaReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsxStaReq(func, pst, mBuf)
LsxStaReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{
   SxMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsxStaReq)

   LSX_ZERO((U8 *)&mgt, sizeof(SxMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSX063, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSX064, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STSID:
         break;
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSXLOGERROR(pst, ELSX065, mgt.hdr.elmId.elmnt,
                     "cmUnpkLsxStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Status Confirm
*
*       Desc:  This function is used to un-pack the status confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsxStaCfm
(
LsxStaCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsxStaCfm(func, pst, mBuf)
LsxStaCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SxMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsxStaCfm)

   LSX_ZERO((U8 *)&mgt, sizeof(SxMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSX066, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSX067, pst);
   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.ssta.dt, mBuf, ELSX068, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSXLOGERROR(pst, ELSX069, mgt.hdr.elmId.elmnt,
                     "cmUnpkLsxStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLsxStaCfm */


/*
*
*       Fun:   Un-pack Status Indication
*
*       Desc:  This function is used to un-pack the status indication
*              primitive to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsxStaInd
(
LsxStaInd func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsxStaInd(func, pst, mBuf)
LsxStaInd func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SxMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsxStaInd)

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSX070, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSX071, pst);

   CMCHKUNPKLOG(cmUnpkCmAlarm, &mgt.t.usta.alarm,      mBuf, ELSX072, pst);
   CMCHKUNPKLOG(cmUnpkSpId,    &mgt.t.usta.info.spId,  mBuf, ELSX073, pst);
   CMCHKUNPKLOG(SUnpkU8,       &mgt.t.usta.info.type,  mBuf, ELSX074, pst);

   if(mgt.t.usta.info.type != LSX_ALARMINFO_TYPE_NTPRSNT)
   {
      switch(mgt.t.usta.info.type)
      {
         default:
#if (ERRCLASS & ERRCLS_DEBUG)
            LSXLOGERROR(pst, ELSX075, mgt.t.usta.info.type,
                        "cmUnpkLsxStaInd () Failed");
#endif /* ERRCLS_DEBUG */
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
      }/* end of sxitch */
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLsxStaInd */



/*
*
*       Fun:   Un-pack Trace Indication
*
*       Desc:  This function is used to un-pack the trace indication
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lsx.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsxTrcInd
(
LsxTrcInd func,
Pst *pst,                 /* post */
Buffer  *mBuf             /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsxTrcInd(func, pst, mBuf)
LsxTrcInd func;
Pst *pst;                 /* post */
Buffer  *mBuf;            /* message buffer */
#endif
{

   SxMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsxTrcInd)

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSX076, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSX077, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.trc.dt, mBuf, ELSX078, pst);
   CMCHKUNPKLOG(SUnpkU16, &mgt.t.trc.evnt, mBuf, ELSX079, pst);

   (*func)(pst, &mgt, mBuf);
   RETVALUE(ROK);
} /* end of cmUnpkLsxTrcInd */


#endif /* LCLSX */

/********************************************************************30**

         End of file:     

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat     init                  description
----------- --------- ---- -----------------------------------------------
1.1         ---       rks   1. initial release.
*********************************************************************91*/
