/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     S6A convergence Layer

     Type:     C include file

     Desc:     Defines and macros used by Diameter.

     File:     sy.h

     Sid:

     Prg:      bw

*********************************************************************21*/


#ifndef __SYH__
#define __SYH__

#define SYLAYERNAME     "S6A_CONVERGENCE_LAYER"

#define EVTINTGOACTV            42


/* miscellaneous defines */
#define SY_PRNTBUF_SIZE         128     /* size of print buffer */
#define SY_UNUSED               -1      /* for all unused fields */
#define SY_SRVCTYPE_MASK        4

/* Macros.
 */

/* DEBUGP */
#define SYDBGP(_msgClass, _arg)                                 \
   DBGP(&(syCb.init), SYLAYERNAME, _msgClass, _arg)

#define SY_DBG_INFO(_arg)                                       \
   DBGP(&(syCb.init), SYLAYERNAME, LSY_DBGMASK_INFO, _arg)

/* zero out a buffer */
#define SY_ZERO(_str, _len)                                     \
   cmMemset((U8 *)_str, 0, _len);

/* allocate and zero out a static buffer */
#define SY_ALLOC(_size, _datPtr)                                \
{                                                               \
   S16 _ret;                                                    \
   _ret = SGetSBuf(syCb.init.region, syCb.init.pool,            \
                   (Data **)&_datPtr, _size);                   \
   if (_ret == ROK)                                             \
      cmMemset((U8*)_datPtr, 0, _size);                         \
   else                                                         \
      _datPtr = NULLP;                                          \
}

/* free a static buffer */
#define SY_FREE(_size, _datPtr)                                 \
   SPutSBuf(syCb.init.region, syCb.init.pool,                   \
            (Data *)_datPtr, _size);

/* statistics */
#if (defined(SY_MULTI_THREADED)  &&  defined(SY_STS_LOCKS))

#define SY_INC_ERRSTS(_stat)                                    \
{                                                               \
   SY_LOCK(&syCb.errStsLock);                                   \
   _stat++;                                                     \
   SY_UNLOCK(&syCb.errStsLock);                                 \
}

#define SY_INC_TXSTS(_sap, _stat)                               \
{                                                               \
   //SY_LOCK(&_sap->txStsLock);                                   \
   //_stat++;                                                     \
   //SY_UNLOCK(&_sap->txStsLock);                                 \
}

/*hi028.201: Handled statistics properly*/
#define SY_DEC_TXSTS(_sap, _stat)                               \
{                                                               \
   //SY_LOCK(&_sap->txStsLock);                                   \
   //_stat--;                                                     \
   //SY_UNLOCK(&_sap->txStsLock);                                 \
}

#endif

#ifdef HI_MULTI_THREADED

/* acquire a mutex */
#define SY_LOCK(_ptr_lock)                                      \
{                                                               \
   if (SLock(_ptr_lock) != ROK)                                 \
   {                                                            \
      SyAlarmInfo _alInfo;                                      \
      _alInfo.type = LSY_ALARMINFO_TYPE_NTPRSNT;                \
      sySendAlarm(LCM_CATEGORY_INTERNAL, LCM_EVENT_INV_EVT,     \
                  LSY_CAUSE_LOCK_ERR, &_alInfo);                \
      SYDBGP(DBGMASK_LYR, (syCb.init.prntBuf, "SLock() failed\n")); \
   }                                                            \
}

/* release a mutex */
#define SY_UNLOCK(_ptr_lock)                                    \
{                                                               \
   if (SUnlock(_ptr_lock) != ROK)                               \
   {                                                            \
      SyAlarmInfo _alInfo;                                      \
      _alInfo.type = LSY_ALARMINFO_TYPE_NTPRSNT;                \
      sySendAlarm(LCM_CATEGORY_INTERNAL, LCM_EVENT_INV_EVT,     \
                  LSY_CAUSE_UNLOCK_ERR, &_alInfo);              \
      SYDBGP(DBGMASK_LYR, (syCb.init.prntBuf, "SUnlock() failed\n")); \
   }                                                            \
}

#endif // #ifdef HI_MULTI_THREADED

/* statistics */
#if (defined(SY_MULTI_THREADED)  &&  defined(SY_STS_LOCKS))

#define SY_INC_ERRSTS(_stat)                                    \
{                                                               \
   SY_LOCK(&syCb.errStsLock);                                   \
   _stat++;                                                     \
   SY_UNLOCK(&syCb.errStsLock);                                 \
}

#define HI_INC_TXSTS(_stat)                                     \
{                                                               \
   SY_LOCK(&syCb.txStsLock);                                    \
   _stat++;                                                     \
   SY_UNLOCK(&syCb.txStsLock);                                  \
}

/*hi028.201: Handled statistics properly*/
#define HI_DEC_TXSTS(__stat)                                    \
{                                                               \
   SY_LOCK(&syCb.txStsLock);                                   \
   _stat--;                                                     \
   SY_UNLOCK(&syCb.txStsLock);                                 \
}

#define SY_ADD_TXSTS(_stat, _val)                         \
{                                                               \
   SY_LOCK(&syCb.txStsLock);                                   \
   _stat += _val;                                               \
   HI_UNLOCK(&syCb.txStsLock);                                 \
}

#if 0
#define SY_INC_TXSTSMSG(_sap, _conCb)                           \
{                                                               \
   HI_LOCK(&_sap->txStsLock);                                   \
   if ((_conCb->srvcType & 0x0F) == HI_SRVC_TLS)                \
      _sap->txSts.numTxTlsMsg++;                                \
   else if (_conCb->flag & HI_FL_TCP)                           \
      _sap->txSts.numTxTcpMsg++;                                \
   else if (_conCb->flag & HI_FL_UDP)                           \
      _sap->txSts.numTxUdpMsg++;                                \
   else                                                         \
      _sap->txSts.numTxRawMsg++;                                \
   HI_UNLOCK(&_sap->txStsLock);                                 \
}
#endif

#define SY_ZERO_ERRSTS()                                        \
{                                                               \
   SY_LOCK(&syCb.errStsLock);                                   \
   SY_ZERO(&syCb.errSts, sizeof(SyErrSts));                     \
   SY_UNLOCK(&syCb.errStsLock);                                 \
}

#define HI_ZERO_TXSTS()                                         \
{                                                               \
   SY_LOCK(&syCb.txStsLock);                                    \
   syCb.txSts.numTxMsg = 0;                                     \
   SY_UNLOCK(&syCb.txStsLock);                                  \
}

#else  /* SY_MULTI_THREADED  &&  SY_STS_LOCKS */


#define SY_INC_ERRSTS(_stat)                                    \
   _stat++;

#define SY_INC_TXSTS(_stat)                                     \
   _stat++;

#define SY_DEC_TXSTS(_stat)                                     \
   _stat--;

#define SY_ADD_TXSTS(_stat, _val)                               \
   _stat += _val;

#if 0
#define HI_INC_TXSTSMSG(_sap, _conCb)                           \
{                                                               \
   if ((_conCb->srvcType & 0x0F) == HI_SRVC_TLS)                \
      _sap->txSts.numTxTlsMsg++;                                \
   else if (_conCb->flag & HI_FL_TCP)                           \
      _sap->txSts.numTxTcpMsg++;                                \
   else if (_conCb->flag & HI_FL_UDP)                           \
      _sap->txSts.numTxUdpMsg++;                                \
   else                                                         \
      _sap->txSts.numTxRawMsg++;                                \
}
#endif

#define SY_ZERO_ERRSTS()                                        \
   SY_ZERO(&syCb.errSts, sizeof(SyErrSts));

#define SY_ZERO_TXSTS()                                         \
   syCb.txSts.numTxMsg = 0;

//#endif

#endif /* SY_MULTI_THREADED  &&  SY_STS_LOCKS */


#endif /* __SYH__ */


/********************************************************************30**

         End of file:

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

