/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter T6A Interface

     Type:     C include file

     Desc:     Diameter

     File:     lsw.h

     Sid:

     Prg:

*********************************************************************21*/

#ifndef __LSWH__
#define __LSWH__

#ifdef LSWV1            /* T6A interface version 14.1 */
#ifdef LSWIFVER
#undef LSWIFVER
#endif
#define LSWIFVER        0x0e10
#endif

/***********************************************************************
         defines for layer-specific elements
 ***********************************************************************/

#if 0
#define LDS_PATHLEN      255
#endif

/***********************************************************************
         defines for "reason" in LsyStaInd
 ***********************************************************************/
#define  LSW_REASON_OPINPROG                 (LCM_REASON_LYR_SPECIFIC + 1)

/***********************************************************************
         defines for "event" in LsyStaInd
 ***********************************************************************/
/*
#define  LSY_EVENT_???             (LCM_EVENT_LYR_SPECIFIC + 1)
*/

/***********************************************************************
         defines for "cause" in LsyStaInd
 ***********************************************************************/
/* diameter  related errors */

/*
#define  LSY_CAUSE_LOCK_ERR             (LCM_CAUSE_LYR_SPECIFIC + 1)
#define  LSY_CAUSE_UNLOCK_ERR           (LCM_CAUSE_LYR_SPECIFIC + 2)
*/

/***********************************************************************
         defines related to events across the management interface
 ***********************************************************************/

#define EVTLSWCFGREQ             1
#define EVTLSWSTSREQ             2
#define EVTLSWCNTRLREQ           3
#define EVTLSWSTAREQ             4
#define EVTLSWCFGCFM             5
#define EVTLSWSTSCFM             6
#define EVTLSWCNTRLCFM           7
#define EVTLSWSTACFM             8
#define EVTLSWSTAIND             9
#define EVTLSWTRCIND             10
#define EVTLSWINITATTCHSGW       11
#define EVTLSWRIA                12

/********************************************************************SZ**
 SW States
*********************************************************************SZ*/

#define LSW_UNINITIALIZED         1     /* uninitizlied */
#define LSW_INITIALIZED           1     /* initizlied */
#define LSW_CFG_PARSED            1     /* config file parsed */
#define LSW_STARTED               1     /* started */
#define LSW_WAITING_FOR_SHUTDOWN  1     /* shutdown issued */
#define LSW_SHUTDOWN              1     /* shutdown */


/***********************************************************************
         defines related to events in LsyTrcInd primitive
 ***********************************************************************/
/*
#define  LSY_RAW_RXED                   5
*/

/* alarmInfo.type */
#define  LSW_ALARMINFO_TYPE_NTPRSNT     0       /* alarmInfo is not present */

/* parType values in LhiStaInd */
#if 0
#define  LDS_INV_MBUF                   1       /* invalid message buffer */
#endif

/* selector values for lmPst.selector */
#define  LSW_LC                         0       /* loosely coupled LM */
#define  LSW_TC                         1       /* tightly coupled LM*/

#define LSW_DBGMASK_INFO    (DBGMASK_LYR << 3)

/* Error macro for TUCL management interface */
/* lhi_h_001.main_11: Fix for compilation warning */
#define LSWLOGERROR(pst, errCode, errVal, errDesc)            \
        SLogError(pst->srcEnt, pst->srcInst, pst->srcProcId,  \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,   \
                  (ErrCode)errCode, (ErrVal)errVal,           \
                  errDesc)

#define LSW_ZERO(_str,_len)                                   \
   cmMemset((U8 *)_str, 0, _len);

/* Error codes */
#define   ELSWBASE     0             /* reserved */
#define   ERRLSW       (ELSWBASE)

#define   ELSW001      (ERRLSW +    1)
#define   ELSW002      (ERRLSW +    2)
#define   ELSW003      (ERRLSW +    3)
#define   ELSW004      (ERRLSW +    4)
#define   ELSW005      (ERRLSW +    5)
#define   ELSW006      (ERRLSW +    6)
#define   ELSW007      (ERRLSW +    7)
#define   ELSW008      (ERRLSW +    8)
#define   ELSW009      (ERRLSW +    9)
#define   ELSW010      (ERRLSW +   10)
#define   ELSW011      (ERRLSW +   11)
#define   ELSW012      (ERRLSW +   12)
#define   ELSW013      (ERRLSW +   13)
#define   ELSW014      (ERRLSW +   14)
#define   ELSW015      (ERRLSW +   15)
#define   ELSW016      (ERRLSW +   16)
#define   ELSW017      (ERRLSW +   17)
#define   ELSW018      (ERRLSW +   18)
#define   ELSW019      (ERRLSW +   19)
#define   ELSW020      (ERRLSW +   20)
#define   ELSW021      (ERRLSW +   21)
#define   ELSW022      (ERRLSW +   22)
#define   ELSW023      (ERRLSW +   23)
#define   ELSW024      (ERRLSW +   24)
#define   ELSW025      (ERRLSW +   25)
#define   ELSW026      (ERRLSW +   26)
#define   ELSW027      (ERRLSW +   27)
#define   ELSW028      (ERRLSW +   28)
#define   ELSW029      (ERRLSW +   29)
#define   ELSW030      (ERRLSW +   30)
#define   ELSW031      (ERRLSW +   31)
#define   ELSW032      (ERRLSW +   32)
#define   ELSW033      (ERRLSW +   33)
#define   ELSW034      (ERRLSW +   34)
#define   ELSW035      (ERRLSW +   35)
#define   ELSW036      (ERRLSW +   36)
#define   ELSW037      (ERRLSW +   37)
#define   ELSW038      (ERRLSW +   38)
#define   ELSW039      (ERRLSW +   39)
#define   ELSW040      (ERRLSW +   40)
#define   ELSW041      (ERRLSW +   41)
#define   ELSW042      (ERRLSW +   42)
#define   ELSW043      (ERRLSW +   43)
#define   ELSW044      (ERRLSW +   44)
#define   ELSW045      (ERRLSW +   45)
#define   ELSW046      (ERRLSW +   46)
#define   ELSW047      (ERRLSW +   47)
#define   ELSW048      (ERRLSW +   48)
#define   ELSW049      (ERRLSW +   49)
#define   ELSW050      (ERRLSW +   50)
#define   ELSW051      (ERRLSW +   51)
#define   ELSW052      (ERRLSW +   52)
#define   ELSW053      (ERRLSW +   53)
#define   ELSW054      (ERRLSW +   54)
#define   ELSW055      (ERRLSW +   55)
#define   ELSW056      (ERRLSW +   56)
#define   ELSW057      (ERRLSW +   57)
#define   ELSW058      (ERRLSW +   58)
#define   ELSW059      (ERRLSW +   59)
#define   ELSW060      (ERRLSW +   60)
#define   ELSW061      (ERRLSW +   61)
#define   ELSW062      (ERRLSW +   62)
#define   ELSW063      (ERRLSW +   63)
#define   ELSW064      (ERRLSW +   64)
#define   ELSW065      (ERRLSW +   65)
#define   ELSW066      (ERRLSW +   66)
#define   ELSW067      (ERRLSW +   67)
#define   ELSW068      (ERRLSW +   68)
#define   ELSW069      (ERRLSW +   69)
#define   ELSW070      (ERRLSW +   70)
#define   ELSW071      (ERRLSW +   71)
#define   ELSW072      (ERRLSW +   72)
#define   ELSW073      (ERRLSW +   73)
#define   ELSW074      (ERRLSW +   74)
#define   ELSW075      (ERRLSW +   75)
#define   ELSW076      (ERRLSW +   76)
#define   ELSW077      (ERRLSW +   77)
#define   ELSW078      (ERRLSW +   78)
#define   ELSW079      (ERRLSW +   79)
#define   ELSW080      (ERRLSW +   80)
#define   ELSW081      (ERRLSW +   81)
#define   ELSW082      (ERRLSW +   82)
#define   ELSW083      (ERRLSW +   83)
#define   ELSW084      (ERRLSW +   84)
#define   ELSW085      (ERRLSW +   85)
#define   ELSW086      (ERRLSW +   86)
#define   ELSW087      (ERRLSW +   87)
#define   ELSW088      (ERRLSW +   88)
#define   ELSW089      (ERRLSW +   89)
#define   ELSW090      (ERRLSW +   90)
#define   ELSW091      (ERRLSW +   91)
#define   ELSW092      (ERRLSW +   92)

#endif /* __LSWH__ */
