/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     EPC DNS Convergence Layer

     Type:     C source file

     Desc:     Management interface.

     File:     ds_ptmi.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/*

The following functions are provided in this file:
     DsMiLdsCfgCfm      Configuration Confirm
     DsMiLdsCntrlCfm    Control Confirm
     DsMiLdsStsCfm      Statistics Confirm
     DsMiLdsStaInd      Status Indication
     DsMiLdsStaCfm      Status Confirm
     DsMiLdsTrcInd      Trace Indication

It should be noted that not all of these functions may be required
by a particular layer management service user.

It is assumed that the following functions are provided in TUCL:
     DsMiLdsCfgReq      Configuration Request
     DsMiLdsCntrlReq    Control Request
     DsMiLdsStsReq      Statistics Request
     DsMiLdsStaReq      Status Request

*/

/* header include files (.h) */

#include "envopt.h"             /* environment options */
#include "envdep.h"             /* environment dependent */
#include "envind.h"             /* environment independent */

#include "gen.h"                /* general layer */
#include "ssi.h"                /* system services interface */

/* external headers */
#ifdef HI_TLS
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/ssl.h>
#include <openssl/x509v3.h>
#endif

#include "cm_hash.h"            /* common hash list */
#include "cm_llist.h"           /* common linked list */
#include "cm5.h"                /* common timer */
#include "cm_inet.h"            /* common sockets */
#include "cm_tpt.h"             /* common transport defines */

/* header/extern include files (.x) */

#include "gen.x"                /* general layer */
#include "ssi.x"                /* system services interface */

#include "cm_hash.x"            /* common hashing */
#include "cm_llist.x"           /* common linked list */
#include "cm_lib.x"             /* common library */
#include "cm5.x"                /* common timer */
#include "cm_inet.x"            /* common sockets */
#include "cm_tpt.x"             /* common transport typedefs */

#include "lds.h"
#include "lds.x"
#include "ds.h"
#include "ds.x"

/* local defines */

#define MAXDSMI 2

#ifndef LCDSMILDS
#define PTHIMILAQ
#else
#ifndef SM
#define PTAQMILAQ
#endif
#endif


#ifdef PTDSMILDS
/* declaration of portable functions */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* PTDSMILDS */


/*
  The following matrices define the mapping between the primitives
  called by the upper interface of TUCL and the corresponding
  primitives of the TCP UDP Convergence Layer service user(s).

  The parameter MAXHIMI defines the maximum number of service
  users on top of TUCL. There is an array of functions per
  primitive invoked by TUCL. Every array is MAXHIMI long (i.e.
  there are as many functions as the number of service users).

  The dispatching is performed by the configurable variable:
  selector. The selector is configured on a per SAP basis.

  The selectors are:

   0 - loosely coupled (#define LCHIMILHI) 1 - LHI (#define SM)

*/



/* Configuration Confirm */

PRIVATE LdsCfgCfm DsMiLdsCfgCfmMt[MAXDSMI] =
{
#ifdef LCDSMILDS
   cmPkLdsCfgCfm,          /* 0 - loosely coupled  */
#else
   PtMiLdsCfgCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLdsCfgCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLdsCfgCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Control Confirm */

PRIVATE LdsCntrlCfm DsMiLdsCntrlCfmMt[MAXDSMI] =
{
#ifdef LCDSMILDS
   cmPkLdsCntrlCfm,          /* 0 - loosely coupled  */
#else
   PtMiLdsCntrlCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLdsCntrlCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLdsCntrlCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Statistics Confirm */

PRIVATE LdsStsCfm DsMiLdsStsCfmMt[MAXDSMI] =
{
#ifdef LCDSMILDS
   cmPkLdsStsCfm,          /* 0 - loosely coupled  */
#else
   PtMiLdsStsCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLdsStsCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLdsStsCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Status Indication */

PRIVATE LdsStaInd DsMiLdsStaIndMt[MAXDSMI] =
{
#ifdef LCDSMILDS
   cmPkLdsStaInd,          /* 0 - loosely coupled  */
#else
   PtMiLdsStaInd,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLdsStaInd,          /* 1 - tightly coupled, layer management */
#else
   PtMiLdsStaInd,          /* 1 - tightly coupled, portable */
#endif
};

/* Status Confirm */

PRIVATE LdsStaCfm DsMiLdsStaCfmMt[MAXDSMI] =
{
#ifdef LCDSMILDS
   cmPkLdsStaCfm,          /* 0 - loosely coupled  */
#else
   PtMiLdsStaCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLdsStaCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLdsStaCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Trace Indication */

PRIVATE LdsTrcInd DsMiLdsTrcIndMt[MAXDSMI] =
{
#ifdef LCDSMILDS
   cmPkLdsTrcInd,          /* 0 - loosely coupled  */
#else
   PtMiLdsTrcInd,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLdsTrcInd,          /* 1 - tightly coupled, layer management */
#else
   PtMiLdsTrcInd,          /* 1 - tightly coupled, portable */
#endif
};


/*
*     Layer Management Interface Functions
*/


/*
*
*       Fun:   Configuration confirm
*
*       Desc:  This function is used to send a configuration confirm
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  ds_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 DsMiLdsCfgCfm
(
Pst     *pst,            /* post structure */
DsMngmt *cfg             /* configuration */
)
#else
PUBLIC S16 DsMiLdsCfgCfm(pst, cfg)
Pst     *pst;            /* post structure */
DsMngmt *cfg;            /* configuration */
#endif
{
   TRC3(DsMiLdsCfgCfm)

   DSDBGP(DBGMASK_MI, (dsCb.init.prntBuf,
         "DsMiLdsCfgCfm(pst, cfg (0x%p))\n", (Ptr)cfg));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*DsMiLdsCfgCfmMt[pst->selector])(pst, cfg));
} /* end of DsMiLdsCfgCfm */


/*
*
*       Fun:   Control confirm
*
*       Desc:  This function is used to send a control confirm
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  ds_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 DsMiLdsCntrlCfm
(
Pst     *pst,            /* post structure */
DsMngmt *cntrl           /* control */
)
#else
PUBLIC S16 DsMiLdsCntrlCfm(pst, cntrl)
Pst     *pst;            /* post structure */
DsMngmt *cntrl;          /* control */
#endif
{
   TRC3(DsMiLdsCntrlCfm)

   DSDBGP(DBGMASK_MI, (dsCb.init.prntBuf,
         "DsMiLdsCntrlCfm(pst, cntrl (0x%p))\n", (Ptr)cntrl));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*DsMiLdsCntrlCfmMt[pst->selector])(pst, cntrl));
} /* end of DsMiLdsCntrlCfm */


/*
*
*       Fun:   Status Indication
*
*       Desc:  This function is used to indicate the status
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  ds_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 DsMiLdsStaInd
(
Pst     *pst,            /* post structure */
DsMngmt *usta             /* unsolicited status */
)
#else
PUBLIC S16 DsMiLdsStaInd(pst, usta)
Pst     *pst;            /* post structure */
DsMngmt *usta;            /* unsolicited status */
#endif
{
   TRC3(DsMiLdsStaInd)

   DSDBGP(DBGMASK_MI, (dsCb.init.prntBuf,
          "DsMiLdsStaInd(pst, usta (0x%p))\n", (Ptr)usta));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*DsMiLdsStaIndMt[pst->selector])(pst, usta));
} /* end of DsMiLdsStaInd */


/*
*
*       Fun:   Status Confirm
*
*       Desc:  This function is used to return the status
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  ds_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 DsMiLdsStaCfm
(
Pst     *pst,            /* post structure */
DsMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 DsMiLdsStaCfm(pst, sta)
Pst     *pst;            /* post structure */
DsMngmt *sta;             /* solicited status */
#endif
{
   TRC3(DsMiLdsStaCfm)

   DSDBGP(DBGMASK_MI, (dsCb.init.prntBuf,
          "DsMiLdsStaCfm(pst, sta (0x%p))\n", (Ptr)sta));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*DsMiLdsStaCfmMt[pst->selector])(pst, sta));
} /* end of DsMiLdsStaCfm */


/*
*
*       Fun:   Statistics Confirm
*
*       Desc:  This function is used to return the statistics
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  ds_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 DsMiLdsStsCfm
(
Pst     *pst,                /* post structure */
DsMngmt *sts                 /* statistics */
)
#else
PUBLIC S16 DsMiLdsStsCfm(pst, sts)
Pst     *pst;                /* post structure */
DsMngmt *sts;                /* statistics */
#endif
{
   TRC3(DsMiLdsStsCfm)

   DSDBGP(DBGMASK_MI, (dsCb.init.prntBuf,
         "DsMiLdsStsCfm(pst, sts (0x%p))\n", (Ptr)sts));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*DsMiLdsStsCfmMt[pst->selector])(pst, sts));
} /* end of DsMiLdsStsCfm */


/*
*
*       Fun:   Trace Indication
*
*       Desc:  This function is used to indicate the trace
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  ds_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 DsMiLdsTrcInd
(
Pst     *pst,            /* post structure */
DsMngmt *trc,             /* unsolicited status */
Buffer  *mBuf              /* message buffer */
)
#else
PUBLIC S16 DsMiLdsTrcInd(pst, trc, mBuf)
Pst     *pst;            /* post structure */
DsMngmt *trc;             /* unsolicited status */
Buffer  *mBuf;             /* message buffer */
#endif
{
   TRC3(DsMiLdsTrcInd)

   DSDBGP(DBGMASK_MI, (dsCb.init.prntBuf,
         "DsMiLdsTrcInd(pst, trc (0x%p))\n", (Ptr)trc));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*DsMiLdsTrcIndMt[pst->selector])(pst, trc, mBuf));
} /* end of DsMiLdsTrcInd */


/********************************************************************30**

         End of file:     ds_ptmi.c@@/main/6 - Mon Mar  3 20:09:54 2008

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision History:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

