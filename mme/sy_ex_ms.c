/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/



/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"
#include "lsy.h"
#include "lsy.x"

#include "aqfd.h"
#include "aqfd.x"

#include "sy.h"
#include "sy.x"
#include "sy_err.h"

/*
*
*       Fun:    syActvTsk
*
*       Desc:   Process received events.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   sy_ex_ms.c
*
*/
#ifdef ANSI
PUBLIC S16 syActvTsk
(
Pst             *pst,           /* post */
Buffer          *mBuf           /* message buffer */
)
#else
PUBLIC S16 syActvTsk(pst, mBuf)
Pst             *pst;           /* post */
Buffer          *mBuf;          /* message buffer */
#endif
{
   S16          ret = ROK;

   TRC3(syActvTsk);

#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId, pst->dstEnt, pst->dstInst,
                          (Void **)&syCbPtr)) != ROK)
   {
      SYLOGERROR_DEBUGPST(pst->dstProcId, pst->dstEnt, ESY004,
            (ErrVal)0, pst->dstInst,
            "syActvTsk() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
      "---------Diameter------(proc(%d),entt(%d),inst(%d),event(%d),srcEnt(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst,pst->event,pst->srcEnt));
#endif  /* SS_MULTIPLE_PROCS */

   /* check the message source */
   switch (pst->srcEnt)
   {
#ifdef LCSYMILSY
      /* stack manager primitive */
      case ENTSM:
      {
         switch (pst->event)
         {
            case EVTLSYCFGREQ:
               ret = cmUnpkLsyCfgReq(SyMiLsyCfgReq, pst, mBuf);
               break;
            case EVTLSYSTSREQ:
               ret = cmUnpkLsyStsReq(SyMiLsyStsReq, pst, mBuf);
               break;
            case EVTLSYCNTRLREQ:
               ret = cmUnpkLsyCntrlReq(SyMiLsyCntrlReq, pst, mBuf);
               break;
            case EVTLSYSTAREQ:
               ret = cmUnpkLsyStaReq(SyMiLsyStaReq, pst, mBuf);
               break;
            default:
               SYLOGERROR_INT_PAR(ESY005, pst->event, pst->dstInst,
                  "syActvTsk(): Invalid event from layer manager");
               SPutMsg(mBuf);
               ret = RFAILED;
               break;
         }
         break;
      }
#endif /* LCSYMILSY */

      case ENTAQ:
      {
         switch (pst->event)
         {
            case EVTLSYAIA:
               ret = SyMiLsyAIA(mBuf);
               break;
            case EVTLSYULA_ATTACH_REQUEST:
               ret = SyMiLsyULAAttachRequest(mBuf);
               break;
            case EVTLSYPUA:
               ret = SyMiLsyPUA(mBuf);
               break;
            case EVTLSYIDR:
               ret = SyMiLsyIDR();
               break;
            default:
               SYLOGERROR_INT_PAR(ESY006, pst->event, pst->dstInst,
                  "syActvTsk(): Invalid event from diameter");
               ret = RFAILED;
               break;
         }
         SPutMsg(mBuf);
         break;
      }

      default:
         SYLOGERROR_INT_PAR(ESY008, pst->event, pst->dstInst,
            "syActvTsk(): Invalid source entity");
         SPutMsg(mBuf);
         ret = RFAILED;
         break;
   }

#if 0
   /* primitive has invalid interface version num? */
   if (ret == RINVIFVER  &&  hiCb.init.cfgDone == TRUE)
   {
      info.spId = -1;
      info.type = LHI_ALARMINFO_TYPE_INFVER;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LCM_EVENT_INV_EVT,
                  LCM_CAUSE_DECODE_ERR, &info);
   }
#endif

   SExitTsk();

   RETVALUE(ret);
} /* end of syActvTsk */


/********************************************************************30**

         End of file:     hi_ex_ms.c@@/main/6 - Mon Mar  3 20:09:50 2008

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat     init                  description
----------- --------- ---- -----------------------------------------------
1.1         ---       asa  1. initial release.
1.1+        hi002.11  asa  1. changes for GTP.
1.1+        hi004.11  cvp  1. changes for MGCP.
1.1+        hi005.11  cvp  1. included MGCP loose coupling flag.
/main/2     ---       cvp  1. changes for Annex G.
                           2. changed the copyright header.
            /main/4   sb   1. changes for SCTP.
                      cvp  2. changes for MPLS.
/main/4+    hi003.13  cvp  1. changes for dummy layer.
/main/4+    hi006.13  bsr  1. changes for SIP layer.
/main/4     ---       cvp  1. changes for multi-threaded TUCL.
                           2. changed the copyright header.
/main/4+    hi009.104 mmh  1. passing memInfo needed to allocate memory
                              for ip hdr parameters in cmUnpkHitUDatReq.
                           2. Rolling upgrade changes, under compile flag
                              HI_RUG as per tcr0020.txt:
                           -  Added new variable for alarm info
                           -  Fill up the alarm info structure properly
                           -  Check on return value of unpacking primitive is
                              added for alarm indication to LM in case primitive
                              is received with invalid interface version number.
                           -  added missing break in case ENTSH
/main/4+    hi020.104 rs   1. Added ENTSV as one of the users.
/main/4+    hi021.104 rs   1. Warning Removed.
/main/5      ---       kp   1. Updated for release 1.5.
/main/5+    hi002.105 ss   1. SS_MULTIPLE_PROC flags added.
/main/5+    hi004.105 ss   1. Moved hiActvInit function to hi_bdy1.c for
                              code reorganization.
/main/5+    hi009.105 ss   1. Added FP as upper user.
/main/5+    hi012.105 svp   1. Added SY as upper user.
/main/5+    hi013.105 svp  1. Added two new HIT primitives.
                              HiUiHitTlsEstReq(), HiUiHitTlsEstCfm().
/main/6      ---       hs   1. Updated for release of 2.1
/main/6+    hi007.201  hsingh   1. Added SZ as upper user for TUCL
$SID$       hi013.201  pchebolu   1. Added EG as upper user for TUCL
/main/6+    hi020.201  mm    1. Update for X2AP release 1.1.
/main/6+    hi021.201  rcs   1. Update for IUH release 1.1.
/main/6+    hi028.201 ragrawal  1. Added support for sua DFTHA in
                                   function hiActvTsk().
*********************************************************************91*/

