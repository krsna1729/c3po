/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:    Trillium LTE CNE - MME Controller Module

     Type:    C Include file

     Desc:    This file contains the vb application source code

     File:    vbsm_swcfg.c

     Sid:

     Prg:     bw
*********************************************************************21*/

/* Header include files (.h) */
#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "cm_os.h"

#include "sct.h"           /* SCT interface defines           */

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
/* #include "sz_err.h"*/        /* S1AP Error */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "cm_os.x"

#include "sct.x"           /* SCT interface defines           */

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "aqfd2.x"

#include "lsw.h"
#include "lsw.x"
#include "sw_err.h"
#include "sw.h"
#include "sw.x"

#ifdef __cplusplus
EXTERN "C" {
#endif /* __cplusplus */

/* #define VB_SM_HI_CONFIGURED  (STGEN | STTSAP) */

#define VB_SM_SW_CONFIGURED (STGEN)

U8      vbSmSwCfg = 0;

/* Function prototypes */
PRIVATE Void vbMmeLswSwGenCfg ARGS ((Void));

/*
 *
 *       Fun:    vbMmeSwCfg - configure SW
 *
 *       Desc:
 *
 *       Ret:    Void
 *
 *       Notes:  None
 *
 *       File:   vbsm_swcfg.c
 *
 */

#ifdef ANSI
PUBLIC Void vbMmeSwCfg
(
Void
)
#else
PUBLIC Void vbMmeSwCfg()
#endif /* ANSI */
{
   SM_TRC2(vbMmeSwCfg);

   VBSM_DBG_INFO((VBSM_PRNTBUF,"Sending MME SwCfg...\n"));
   vbSmCb.swPst.event = EVTLSWCFGREQ;

   vbMmeLswSwGenCfg();

   RETVOID;
}
/*
 *
 *       Fun:    vbMmeLswSwGenCfg - fill in default genCfg for DS
 *
 *       Desc:
 *
 *       Ret:    Void
 *
 *       Notes:  None
 *
 *       File:   vbsm_swcfg.c
 *
 */

#ifdef ANSI
PRIVATE Void vbMmeLswSwGenCfg
(
Void
)
#else
PRIVATE Void vbMmeLswSwGenCfg()
#endif /* ANSI */
{
   SwMngmt     swMgt;
   SwGenCfg    *cfg;

   SM_TRC2(vbMmeLswSwGenCfg);

   cmMemset((U8 *)&swMgt, 0, sizeof(SwMngmt));
   vbSmDefHdr(&swMgt.hdr, ENTSW, STGEN, VBSM_SWSMSEL);

   cfg = &swMgt.t.cfg.s.swGen;

#if 0
   cfg->lmPst.srcProcId = SFndProcId();
   cfg->lmPst.dstProcId = SFndProcId();
   cfg->lmPst.srcEnt = (Ent)ENTSW;
   cfg->lmPst.dstEnt = (Ent)ENTSM;
   cfg->lmPst.srcInst = (Inst)0;
   cfg->lmPst.dstInst = (Inst)0;

   cfg->lmPst.prior = (Prior)VBSM_MSGPRIOR;
   cfg->lmPst.route = (Route)RTESPEC;
   cfg->lmPst.event = (Event)EVTNONE;
   cfg->lmPst.region = (Region)vbSmCb.init.region;
   cfg->lmPst.pool = (Pool)vbSmCb.init.pool;
   cfg->lmPst.selector = (Selector)VBSM_SWSMSEL;
#endif

   (Void)SmMiLswCfgReq(&vbSmCb.swPst, &swMgt);
   VBSM_DBG_INFO((VBSM_PRNTBUF,"Sent MME swGenCfg...\n"));
   RETVOID;
} /* end of vbMmeLswSwGenCfg() */

/*
 *      FUN:   vbMmeSwShutDwn
 *
 *      Desc:  Brings the SW to the state before configuration
 *
 *      Ret:   void
 *
 *      Notes: None
 *
 *      File:  vbsm_swcfg.c
 *
 *
 */
#ifdef ANSI
PUBLIC S16 vbMmeSwShutDwn
(
Void
)
#else
PUBLIC S16 vbMmeSwShutDwn()
#endif /* ANSI */
{
   SwMngmt              swMgt;
   S16                  ret = ROK;

   SM_TRC2(vbMmeSwShutDwn);

   cmMemset((U8 *)&swMgt, 0, sizeof(SwMngmt));
   vbSmDefHdr(&swMgt.hdr, ENTSW, STGEN, VBSM_SWSMSEL);

   swMgt.t.cntrl.action = ASHUTDOWN;
   swMgt.t.cntrl.subAction = SAELMNT;

   vbSmCb.swPst.event = EVTLSWCNTRLREQ;
   (Void)SmMiLswCntrlReq(&vbSmCb.swPst, &swMgt);
   VBSM_DBG_INFO((VBSM_PRNTBUF,"Sent MME sw Shutdown CntrlReq...\n"));

   RETVALUE(ret);
}

/*
*
*       Fun:   Configuration Confirm
*
*       Desc:  This function is used by Layer to present configuration confirm
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_swcfg.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLswCfgCfm
(
Pst     *pst,          /* post structure */
SwMngmt *cfm           /* configuration */
)
#else
PUBLIC S16 SmMiLswCfgCfm(pst, cfm)
Pst     *pst;          /* post structure */
SwMngmt *cfm;          /* configuration */
#endif
{
   SM_TRC2(SmMiLswCfgCfm);

   VBSM_DBG_INFO((VBSM_PRNTBUF,"Received MME sw CfgCfm with elmnt(%d) - status(%d)...\n",cfm->hdr.elmId.elmnt,cfm->cfm.status));
   if (cfm->cfm.status == LCM_PRIM_OK)
   {
      vbSmSwCfg |=  cfm->hdr.elmId.elmnt;
      switch (cfm->hdr.elmId.elmnt)
      {
         case STGEN:
         {
            vbSmSwCfg |=  cfm->hdr.elmId.elmnt;
            VBSM_DBG_INFO((VBSM_PRNTBUF,"Received MME sw Gen CfgCfm...\n"));
            break;
         }
         default:
            VBSM_DBG_ERROR((VBSM_PRNTBUF,"Invalid elemt(%d)...\n",cfm->hdr.elmId.elmnt));
        break;
      }

      if (vbSmSwCfg == VB_SM_SW_CONFIGURED)
      {
         VBSM_DBG_INFO((VBSM_PRNTBUF,"SmMiLswCfgCfm: VB_SM_SW_CONFIGURED\n"));
         vbMmeSendMsg(EVTVBT6ACFGDONE);
      }
      else
      {
           VBSM_DBG_INFO((VBSM_PRNTBUF,"Received MME CfgCfm Pending...\n"));
      }
   }
   else
   {
      VBSM_DBG_INFO((VBSM_PRNTBUF,"Received MME CfgCfm .NOT OK..\n"));
   }

   RETVALUE(ROK);
} /* end of SmMiLswCfgCfm */



/*
*
*       Fun:   Control Confirm
*
*       Desc:  This function is used by  Layer to present control confirm
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_swcfg.c
*
*/
#ifdef ANSI
PUBLIC S16 SmMiLswCntrlCfm
(
Pst     *pst,          /* post structure */
SwMngmt *cfm           /* control */
)
#else
PUBLIC S16 SmMiLswCntrlCfm(pst, cfm)
Pst     *pst;          /* post structure */
SwMngmt *cfm;          /* control */
#endif
{
   SM_TRC2(SmMiLswCntrlCfm)

   if (cfm->cfm.status == LCM_PRIM_OK)
   {
      VBSM_DBG_INFO((VBSM_PRNTBUF,"Received control confirmation OK from EPC DNS...\n"));
   }
   else
   {
      VBSM_DBG_INFO((VBSM_PRNTBUF,"Received control confirmation NOT OK from EPC DNS...\n"));
   }
   RETVALUE(ROK);
} /* end of SmMiLswCntrlCfm */

/*
*
*       Fun:   Status Indication
*
*       Desc:  This function is used by Layer to present  unsolicited status
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_swcfg.c
*
*/
#ifdef ANSI
PUBLIC S16 SmMiLswStaInd
(
Pst     *pst,           /* post structure */
SwMngmt *usta           /* unsolicited status */
)
#else
PUBLIC S16 SmMiLswStaInd(pst, usta)
Pst     *pst;           /* post structure */
SwMngmt *usta;          /* unsolicited status */
#endif
{
   SM_TRC2(SmMiLswStaInd);

   UNUSED(pst);
   UNUSED(usta);

   RETVALUE(ROK);
} /* end of SmMiLswStaInd */

/*
*
*       Fun:   Trace Indication
*
*       Desc:  This function is used by  Layer to present trace
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_swcfg.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLswTrcInd
(
Pst *pst,               /* post structure */
SwMngmt *trc,           /* trace */
Buffer *mBuf            /* message buffer */
)
#else
PUBLIC S16 SmMiLswTrcInd(pst, trc, mBuf)
Pst *pst;               /* post structure */
SwMngmt *trc;           /* trace */
Buffer *mBuf;           /* message buffer */
#endif
{
   SM_TRC2(SmMiLswTrcInd);

   UNUSED(pst);
   UNUSED(trc);

   RETVALUE(ROK);
} /* end of SmMiLswTrcInd */

/*
*
*       Fun:   Statistics Confirm
*
*       Desc:  This function is used by Layer to present solicited statistics
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_swcfg.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLswStsCfm
(
Pst       *pst,         /* post structure */
SwMngmt   *sts          /* confirmed statistics */
)
#else
PUBLIC S16 SmMiLswStsCfm(pst, sts)
Pst       *pst;         /* post structure */
SwMngmt   *sts;         /* confirmed statistics */
#endif
{
   SM_TRC2(SmMiLswStsCfm);

   UNUSED(pst);
   UNUSED(sts);

   RETVALUE(ROK);
} /* end of SmMiLswStsCfm */

/*
*
*       Fun:   Status Confirm
*
*       Desc:  This function is used by Layer to present solicited status
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_swcfg.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLswStaCfm
(
Pst     *pst,           /* post structure */
SwMngmt *sta             /* confirmed status */
)
#else
PUBLIC S16 SmMiLswStaCfm(pst, sta)
Pst     *pst;           /* post structure */
SwMngmt *sta;            /* confirmed status */
#endif
{
   SM_TRC2(SmMiLswStaCfm);

   UNUSED(pst);
   UNUSED(sta);

   RETVALUE(ROK);
} /* end of SmMiLswStaCfm */

#ifdef __cplusplus
}
#endif /* __cplusplus */

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************60**
        Revision history:
*********************************************************************61*/


/********************************************************************90**

     ver       pat    init                  description
------------ -------- ---- ----------------------------------------------
/main/1      ---     bw         1. Initial version.
***********************************************************************/
