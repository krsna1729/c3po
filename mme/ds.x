/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     EPC DNS  Convergence Layer

     Type:     C include file

     Desc:     Structures, variables, types and prototypes.

     File:     ds.x

     Sid:

     Prg:      bw

*********************************************************************21*/



#ifndef __DSX__
#define __DSX__

#ifdef __cplusplus
extern "C" {
#endif

/* Reception statistics.
 */
typedef struct dsRxSts
{
   StsCntr      numRxMsg;
} DsRxSts;


/* Transmission statistics.
 */
typedef struct dsTxSts
{
   StsCntr      numTxMsg;
} DsTxSts;


/* Error statistics.
 */
typedef struct dsErrSts
{
   StsCntr      s6aErr;
} DsErrSts;

/* Layer manager control requests can be pending on a SAP when
 * communication with the SY threads are required before a confirm
 * can be sent.
 */
typedef struct dsPendOp
{
   Bool         flag;           /* operation pending? */
   Header       hdr;            /* control request header */
   U8           action;         /*    "       "    action */
   Elmnt        elmnt;          /* STTSAP or STGRTSAP */
   U16          numRem;         /* for ASHUTDOWN/STGRTSAP requests */
   Pst          lmPst;          /* for ASHUTDOWN response */
} DsPendOp;

#ifdef SY_MULTI_THREADED

/* Upper interface primitives can be invoked by multiple threads.
 * Each primitive must therefore have its own Pst.
 */
typedef struct dsUiPsts
{
   Pst          uiConCfmPst;
   Pst          uiConIndPst;
   Pst          uiFlcIndPst;
   Pst          uiDatIndPst;
   Pst          uiUDatIndPst;
   Pst          uiDiscIndPst;
   Pst          uiDiscCfmPst;

} DsUiPsts;

#endif /* SY_MULTI_THREADED */

/* SY layer control block.
 */
typedef struct _dsCb
{
   TskInit      init;           /* task initialization structure */

   DsGenCfg     cfg;            /* general configuration */
   DsErrSts     errSts;         /* general error statistics */
   DsRxSts      rxSts;
   DsTxSts      txSts;

   DsPendOp     pendOp;         /* control request pending */

   Pst          uzPst;
} DsCb;

EXTERN DsCb dsCb;

/* Message to a group thread.
 */
typedef struct dsThrMsg
{
   S16          type;           /* message type */
   SpId         spId;           /* for the relevant SAP */
   UConnId      spConId;        /* for the relevant connection */
   union
   {
      Reason    reason;         /* for indications */
      Action    action;         /* for confirms */
   } disc;
} DsThrMsg;

/* management related */
EXTERN Void dsSendLmCfm         ARGS((Pst *pst, U8 cfmType,
                                      Header *hdr, U16 status,
                                      U16 reason, DsMngmt *cfm));
EXTERN S16  dsGetGenSts         ARGS((DsGenSts *genSts));
EXTERN S16  dsZeroGenSts        ARGS((Void));
EXTERN S16  dsCntrlGen          ARGS((Pst *pst, DsMngmt *cntrl, Header *hdr));
EXTERN Void dsSendAlarm         ARGS((U16 cgy, U16 evnt, U16 cause,
                                      DsAlarmInfo *info));

EXTERN S16  dsGetSid            ARGS((SystemId *sid));

/******************************************************************************/
/******************************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* __DSX__ */

/********************************************************************30**

         End of file:     ds.x@@/main/6 - Mon Mar  3 20:09:47 2008

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

