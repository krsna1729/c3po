/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:    Diameter Convergence Layer

     Type:    C source file

     Desc:    External interface to SSI.

     File:    aq_ex_ms.c

     Sid:

     Prg:     bw

*********************************************************************21*/


/* header include files (.h) */

#include "envopt.h"             /* environment options */
#include "envdep.h"             /* environment dependent */
#include "envind.h"             /* environment independent */

#include "gen.h"                /* general layer */
#include "ssi.h"                /* system services interface */

/* external headers */
#ifdef HI_TLS
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/ssl.h>
#include <openssl/x509v3.h>
#endif

#include "cm_hash.h"            /* common hash list */
#include "cm_llist.h"           /* common linked list */
#include "cm5.h"                /* common timer */
#include "cm_inet.h"            /* common sockets */
#include "cm_tpt.h"             /* common transport defines */

/* header/extern include files (.x) */

#include "gen.x"                /* general layer */
#include "ssi.x"                /* system services interface */

#include "cm_hash.x"            /* common hashing */
#include "cm_llist.x"           /* common linked list */
#include "cm_lib.x"             /* common library */
#include "cm5.x"                /* common timer */
#include "cm_inet.x"            /* common sockets */
#include "cm_tpt.x"             /* common transport typedefs */

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"
#include "aq_err.h"

/*
*
*       Fun:    aqActvTsk
*
*       Desc:   Process received events.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   aq_ex_ms.c
*
*/
#ifdef ANSI
PUBLIC S16 aqActvTsk
(
Pst             *pst,           /* post */
Buffer          *mBuf           /* message buffer */
)
#else
PUBLIC S16 aqActvTsk(pst, mBuf)
Pst             *pst;           /* post */
Buffer          *mBuf;          /* message buffer */
#endif
{
   S16          ret = ROK;

   TRC3(aqActvTsk);

#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId, pst->dstEnt, pst->dstInst,
                          (Void **)&aqCbPtr)) != ROK)
   {
      AQLOGERROR_DEBUGPST(pst->dstProcId, pst->dstEnt, EAQ004,
            (ErrVal)0, pst->dstInst,
            "aqActvTsk() failed, cannot derive aqCb");
      RETVALUE(FALSE);
   }
   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
      "---------Diameter------(proc(%d),entt(%d),inst(%d),event(%d),srcEnt(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst,pst->event,pst->srcEnt));
#endif  /* SS_MULTIPLE_PROCS */

   /* check the message source */
   switch (pst->srcEnt)
   {
//#ifdef LCHIMILHI
      /* stack manager primitive */
      case ENTSM:
      {
         switch (pst->event)
         {
            case EVTLAQCFGREQ:
printf("aqActvTsk() received EVTLAQCFGREQ\n");
               ret = cmUnpkLaqCfgReq(AqMiLaqCfgReq, pst, mBuf);
               break;
            case EVTLAQSTSREQ:
               ret = cmUnpkLaqStsReq(AqMiLaqStsReq, pst, mBuf);
               break;
            case EVTLAQCNTRLREQ:
               ret = cmUnpkLaqCntrlReq(AqMiLaqCntrlReq, pst, mBuf);
               break;
            case EVTLAQSTAREQ:
               ret = cmUnpkLaqStaReq(AqMiLaqStaReq, pst, mBuf);
               break;
            default:
               AQLOGERROR_INT_PAR(EAQ005, pst->event, pst->dstInst,
                  "aqActvTsk(): Invalid event from layer manager");
               SPutMsg(mBuf);
               ret = RFAILED;
               break;
         }
         break;
      }
//#endif /* LCHIMILHI */

#if (defined(LCHIUIHIT) || (defined(HI_LKSCTP) && defined(LCHIUISCT)))

#ifdef LCHIUIHIT
      case ENTHC:
      case ENTHR:
      case ENTGT:
      case ENTMG:
      case ENTHG:
      case ENTSB:
      case ENTLN:
#ifdef DM
      case ENTDM:
#endif
#ifdef SV
      case ENTSV:
#endif
/* hi009.105 : Added FP as upper user for TUCL */
#ifdef FP
      case ENTFP:
#endif
/* hi012.105 : Added AQ as upper user for TUCL */
#ifdef AQ
      case ENTAQ:
#endif
/* hi007.201 : Added SZ as upper user for TUCL */
#ifdef SZ
      case ENTSZ:
#endif
#endif

#if (defined(HI_LKSCTP) && defined(LCHIUISCT))
      case ENTIT:
#endif

#if (defined(LCHIUIHIT) || (defined(HI_LKSCTP) && defined(LCHIUISCT)))
#ifdef SO
      case ENTSO:
#endif
/* hi013.201 : Addtions for eGTP protocol */
#ifdef EG
      case ENTEG:
#endif
      /* hi020.201 Added the CZ Entity for Loose Couple Mode(KSCTP)*/
#ifdef CZ
    case ENTCZ:
#endif /* CZ */
      /* hi021.201 Added the HM Entity(IUH) for Loose Couple Mode(KSCTP)*/
#ifdef HM
    case ENTHM:
#endif /* HM */
#endif
      /* hi028.201: Added support for sua DFTHA */
#ifdef SU
    case ENTSU:
#endif /* SU */

#if 0
         switch (pst->event)
         {
#if (defined(HI_LKSCTP) && defined(LCHIUISCT))
             case  SCT_EVTBNDREQ:                 /* bind request */
               ret = cmUnpkSctBndReq(HiUiSctBndReq, pst, mBuf);
               break;

            case  SCT_EVTASSOCREQ:               /* association request */
               ret = cmUnpkSctAssocReq(HiUiSctAssocReq, pst, mBuf);
               break;

            case  SCT_EVTASSOCRSP:               /* association response */
               ret = cmUnpkSctAssocRsp(HiUiSctAssocRsp, pst, mBuf);
               break;

            case  SCT_EVTTERMREQ:                /* termination request */
               ret = cmUnpkSctTermReq(HiUiSctTermReq, pst, mBuf);
               break;

            case  SCT_EVTSETPRIREQ:              /* set primary DTA request */
               ret = cmUnpkSctSetPriReq(HiUiSctSetPriReq, pst, mBuf);
               break;

            case  SCT_EVTHBEATREQ:               /* heartbeat request */
               ret = cmUnpkSctHBeatReq(HiUiSctHBeatReq, pst, mBuf);
               break;

            case  SCT_EVTDATREQ:                 /* data request */
               ret = cmUnpkSctDatReq(HiUiSctDatReq, pst, mBuf);
               break;

            case  SCT_EVTSTAREQ:                 /* status request */
               ret = cmUnpkSctStaReq(HiUiSctStaReq, pst, mBuf);
               break;

            case  SCT_EVTENDPOPENREQ:            /* endpoint open request */
               ret = cmUnpkSctEndpOpenReq(HiUiSctEndpOpenReq, pst, mBuf);
               break;

            case  SCT_EVTENDPCLOSEREQ:           /* endpoint close request */
               ret = cmUnpkSctEndpCloseReq(HiUiSctEndpCloseReq, pst, mBuf);
               break;

#endif

#ifdef LCHIUIHIT
            case EVTHITBNDREQ:
               ret = cmUnpkHitBndReq(HiUiHitBndReq, pst, mBuf);
               break;

            case EVTHITUBNDREQ:
               ret = cmUnpkHitUbndReq(HiUiHitUbndReq, pst, mBuf);
               break;

            case EVTHITSRVOPENREQ:
               ret = cmUnpkHitServOpenReq(HiUiHitServOpenReq, pst, mBuf);
               break;

            case EVTHITCONREQ:
#ifdef H323_PERF
               TAKE_TIMESTAMP("L ConReq HC->HI, in HI");
#endif
               ret = cmUnpkHitConReq(HiUiHitConReq, pst, mBuf);
               break;

            case EVTHITCONRSP:
               ret = cmUnpkHitConRsp(HiUiHitConRsp, pst, mBuf);
               break;

            case EVTHITDATREQ:
#ifdef H323_PERF
               TAKE_TIMESTAMP("L DatReq HC->HI, in HI");
#endif
               ret = cmUnpkHitDatReq(HiUiHitDatReq, pst, mBuf);
               break;

            case EVTHITUDATREQ:
#ifdef H323_PERF
               TAKE_TIMESTAMP("L UDatReq HC->HI, in HI");
#endif
#ifdef IPV6_OPTS_SUPPORTED
               memInfo.region = hiCb.init.region;
               memInfo.pool = hiCb.init.pool;
               ret = cmUnpkHitUDatReq(HiUiHitUDatReq, pst, mBuf, &memInfo);
#else
               ret = cmUnpkHitUDatReq(HiUiHitUDatReq, pst, mBuf);
#endif
               break;

            case EVTHITDISCREQ:
               ret = cmUnpkHitDiscReq(HiUiHitDiscReq, pst, mBuf);
               break;

/* hi013.105 :  Added two new HIT primitives. HiUiHitTlsEstReq(), HiUiHitTlsEstCfm().*/
#ifdef HITV2
#ifdef HI_TLS
#ifdef HI_TCP_TLS
            case EVTHITTLSESTREQ:
               ret = cmUnpkHitTlsEstReq(HiUiHitTlsEstReq, pst, mBuf);
               break;
#endif /*HI_TCP_TLS */
#endif /*HI_TLS */
#endif /*HITV2 */

#endif
            default:
               AQLOGERROR_INT_PAR(EAQ006, pst->event, pst->dstInst,
                  "hiActvTsk(): Invalid event from service user");
               SPutMsg(mBuf);
               ret = RFAILED;
               break;
         }
#endif
         break;
#endif

      /* system agent is always loosely coupled */
      case ENTSH:
#if 0
         switch (pst->event)
         {
#ifdef FTHA
            case EVTSHTCNTRLREQ:
               ret = cmUnpkMiShtCntrlReq(HiMiShtCntrlReq, pst, mBuf);
               break;
#endif

            default:
               AQLOGERROR_INT_PAR(EAQ007, pst->event, pst->dstInst,
                  "hiActvTsk(): Invalid event from system agent" );
               SPutMsg(mBuf);
               break;
         }
#endif
         break;

      default:
         AQLOGERROR_INT_PAR(EAQ008, pst->event, pst->dstInst,
            "aqActvTsk(): Invalid source entity");
         SPutMsg(mBuf);
         ret = RFAILED;
         break;
   }


#ifdef HI_RUG
   /* primitive has invalid interface version num? */
   if (ret == RINVIFVER  &&  hiCb.init.cfgDone == TRUE)
   {
      info.spId = -1;
      info.type = LHI_ALARMINFO_TYPE_INFVER;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LCM_EVENT_INV_EVT,
                  LCM_CAUSE_DECODE_ERR, &info);
   }
#endif


   SExitTsk();


   RETVALUE(ret);
} /* end of hiActvTsk */


/********************************************************************30**

         End of file:     hi_ex_ms.c@@/main/6 - Mon Mar  3 20:09:50 2008

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat     init                  description
----------- --------- ---- -----------------------------------------------
1.1         ---       bw   1. initial release.
*********************************************************************91*/

