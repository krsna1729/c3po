/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:    S6A Convergence Layer

     Type:    C source file

     Desc:    Upper and Management Interface primitives.

     File:    sy.c

     Sid:

     Prg:     bw

*********************************************************************21*/

/*
Upper Layer functions interface to the HI Layer user.

The following functions are provided in this file:

Upper layer functions

    HiUiHitBndReq        Bind Request
    HiUiHitUbndReq       Unbind Request
    HiUiHitServOpenReq   Server Open Request
    HiUiHitConReq        Connect Request
    HiUiHitConRsp        Connect Response
    HiUiHitDatReq        Data Request(TCP)
    HiUiHitUDatReq       Unit Data Request(UDP)
    HiUiHitDiscReq       Disconnect Request
    HiUiHitTlsEstReq     TLS Establishment Request

Lower layer functions

    socket calls to common socket library

Layer Management functions

    SyMiLsyCfgReq        Configuration Request
    SyMiLsyStsReq        Statistics Request
    SyMiLsyCntrlReq      Control Request
    SyMiLsyStaReq        Solicited Status Request

System Services

    syActvInit           Activate task - initialize

The following function are expected from the interface providers:

Upper Interface (hi_ptui.c)
    HiUiHitConInd        Connect Indication
    HiUiHitConCfm        Connect Confirm
    HiUiHitBndCfm        Bind Confirm
    HiUiHitDatInd        Data Indication
    HiUiHitUDatInd       Unit Data Indication
    HiUiHitDiscInd       Disconnect Indication
    HiUiHitDiscCfm       Disconnect Confirm
    HiUiHitFlcInd        Flow Control Indication
    HiUiHitTlsEstCfm     Flow Control Indication

Lower Interface (hi_ptli.c)
    dummy file

Layer Management (hi_ptmi.c)
    SyMiLsyCfgCfm        Configuration Confirm
    SyMiLsyStsCfm        Statistics Confirm
    SyMiLsyCntrlCfm      Control Confirm
    SyMiLsyStaCfm        Status Confirm
    SyMiLsyTrcind        Trace Indication

System Services
    SRegTmr        Register timer activation function
    SDeregTTsk     Deregister tapa task
    SDetachTTsk    Detach tapa task

    SInitQueue     Initialize Queue
    SQueueFirst    Queue to First Place
    SQueueLast     Queue to Last Place
    SDequeueFirst  Dequeue from First Place
    SFndLenQueue   Find Length of Queue

    SPutMsg        Put Message
    SFndLenMsg     Find Length of Message

    SAddPreMsg     Add Pre Message
    SAddPstMsg     Add Post Message
    SRemPreMsg     Remove Pre Message
    SRemPstMsg     Remove Post Message

    SAddPreMsgMult Add multiple bytes to head of the Message
    SAddPstMsgMult Add multiple bytes from tail of the Message
    SRemPreMsgMult Remove multiple bytes to head of the Message
    SRemPstMsgMult Remove multiple bytes from tail of the Message

    SGetSBuf       Get Static Buffer
    SPutSBuf       Put Static Buffer
    SCatMsg        Concatenate two messages
    SSegMsg        Segments a message buffer
    SInitMsg       Initialize a message buffer pointer

    SChkRes        Check Resources
    SGetDateTime   Get Date and Time
    SGetSysTime    Get System Time

NOTE: For a complete list, please refer to the TUCL Service
      Definition Document.

*/


/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"
#include "lsy.h"
#include "lsy.x"

#include "aqfd.h"
#include "aqfd.x"

#include "sy.h"
#include "sy.x"
#include "sy_err.h"
#include "sy2.x"
#define SYSWMV 1            /* Diameter - main version */
#define SYSWMR 0            /* Diameter - main revision */
#define SYSWBV 0            /* Diameter - branch version */
#define SYSWBR 0            /* Diameter - branch revision */

#define SYSWPN "1000160"    /* Diameter - part number */

EXTERN Void SyMiLsyDestroyVbMonitoringEventConfiguration ( VbMonitoringEventConfiguration *p );

EXTERN int aqfdCreateIDA ARGS((SyIDR *idr, struct msg **ida));
EXTERN int aqfdAddMonitoringEventConfigStatus ARGS((SyMonitoringEventConfiguration *mec, struct msg *m, vendor_id_t vndid, S32 res));

PRIVATE CONSTANT SystemId sId ={
   SYSWMV,                    /* main version */
   SYSWMR,                    /* main revision */
   SYSWBV,                    /* branch version */
   SYSWBR,                    /* branch revision */
   SYSWPN,                    /* part number */
};
typedef struct sySubscriptionData SySubscriptionData;


PRIVATE S16 syCfgGen ARGS((SyGenCfg *syGen));
PRIVATE Void vbStrArr ARGS((U8 *msisdn));
PRIVATE S16 SyProcessSubscriptionData ARGS((VbMmeUeCb *ueCb, struct msg *ida, SySubscriptionData *subscription_data));
PRIVATE Void copySyCfgEvtToVbCfgEvt ARGS((SyMonitoringEventConfiguration * symoncfg, VbMonitoringEventConfiguration * vbconcfg));
PRIVATE VbMonitoringEventConfiguration *createVbMonitoringEventConfiguration ARGS((SyMonitoringEventConfiguration * symoncfg));


/* public variable declarations */

PUBLIC SyCb  syCb;         /* Diameter control block */


/* interface function to system service */

#ifdef ANSI
PRIVATE Void vbStrArr
(
   U8 *msisdn
)
#else
PRIVATE Void vbStrArr(msisdn)
U8 *msisdn;
#endif
{
   int i;
   for (i = 0; i < VB_HSS_MSISDN_LEN; i++)
      msisdn[i] = msisdn[i] -'0';
   msisdn[i] = '\0';
}

/*
*
*       Fun:    syActvInit
*
*       Desc:   Called from SSI to initialize Diameter.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef SS_MULTIPLE_PROCS
#ifdef ANSI
PUBLIC S16 syActvInit
(
ProcId procId,         /* procId */
Ent entity,            /* entity */
Inst inst,             /* instance */
Region region,         /* region */
Reason reason,         /* reason */
Void **xxCb           /* Protocol Control Block */
)
#else
PUBLIC S16 syActvInit(procId,entity, inst, region, reason, xxCb)
ProcId procId;         /* procId */
Ent entity;            /* entity */
Inst inst;             /* instance */
Region region;         /* region */
Reason reason;         /* reason */
Void **xxCb;           /* Protocol Control Block */
#endif
#else /* SS_MULTIPLE_PROCS */
#ifdef ANSI
PUBLIC S16 syActvInit
(
Ent entity,            /* entity */
Inst inst,             /* instance */
Region region,         /* region */
Reason reason          /* reason */
)
#else
PUBLIC S16 syActvInit(entity, inst, region, reason)
Ent entity;            /* entity */
Inst inst;             /* instance */
Region region;         /* region */
Reason reason;         /* reason */
#endif
#endif /* SS_MULTIPLE_PROCS */
{
   TRC2(syActvInit);

#ifdef SS_MULTIPLE_PROCS
   SYDBGP(DBGMASK_SI, (syCb.init.prntBuf,
          "syActvInit(ProcId(%d), Ent(%d), Inst(%d), Region(%d), Reason(%d))\n",
           procId, entity, inst, region, reason));
#else /* SS_MULTIPLE_PROCS */
   SYDBGP(DBGMASK_SI, (syCb.init.prntBuf,
          "syActvInit(Ent(%d), Inst(%d), Region(%d), Reason(%d))\n",
           entity, inst, region, reason));
   SY_ZERO(&syCb, sizeof(SyCb));
#endif /* SS_MULTIPLE_PROCS */

   /* initialize syCb */

   syCb.init.ent        = entity;
   syCb.init.inst       = inst;
   syCb.init.region     = region;
   syCb.init.reason     = reason;
   syCb.init.cfgDone    = FALSE;
   syCb.init.pool       = 0;
#ifdef SS_MULTIPLE_PROCS
   syCb.init.procId = procId;
#else /* SS_MULTIPLE_PROCS */
   syCb.init.procId = SFndProcId();
#endif /* SS_MULTIPLE_PROCS */
   syCb.init.acnt       = FALSE;
   syCb.init.usta       = TRUE;
   syCb.init.trc        = FALSE;

   /* hi028.201: Added dbgMask and protected under HI_DEBUG flag*/
#ifdef DEBUGP
   syCb.init.dbgMask    = 0;
#ifdef SY_DEBUG
   syCb.init.dbgMask = 0xFFFFFFFF;
#endif
#endif

   RETVALUE(ROK);
} /* end of syActvInit */

/*
*
*       Fun:    SyMiLsyCfgReq
*
*       Desc:   Configure the layer. Responds with a SyMiLsyCfgCfm
*               primitive.
*
*       Ret:    ROK     - ok
*
*       Notes:  Configuration must be performed in the following
*               sequence:
*               1) general configuration (STGEN)
*               2) transport sap configuration (STTSAP)
*               3) TLS context configuration (STCTX)
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 SyMiLsyCfgReq
(
Pst             *pst,           /* post structure */
SyMngmt         *cfg            /* configuration structure */
)
#else
PUBLIC S16 SyMiLsyCfgReq(pst, cfg)
Pst             *pst;           /* post structure */
SyMngmt         *cfg;           /* configuration structure */
#endif
{
   SyMngmt      cfmMsg;
   S16          ret;


   TRC3(SyMiLsyCfgReq);

#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&syCbPtr)) !=
      ROK)
   {
      SYLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESY012,(ErrVal)0,pst->dstInst,
            "SyMiLsyCfgReq() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
      "---------Diameter------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
          "SyMiLsyCfgReq(pst, elmnt(%d))\n",
           cfg->hdr.elmId.elmnt));

   SY_ZERO(&cfmMsg, sizeof (SyMngmt));


   /* hi032.201: Removed locking and unloacking of lmpst lock */
#ifdef HI_RUG
   /* hi028:201: Locking mechanism is used before using lmPst*/
   /* store interface version */
   if (!syCb.init.cfgDone)
      syCb.init.lmPst.intfVer = pst->intfVer;
#endif


   switch (cfg->hdr.elmId.elmnt)
   {
      case STGEN:            /* general configuration */
         ret = syCfgGen(&cfg->t.cfg.s.syGen);
         break;

      default:               /* invalid */
         ret = LCM_REASON_INVALID_ELMNT;
         break;
   }


   /* issue configuration confirm */
   sySendLmCfm(pst, TCFG, &cfg->hdr, (U16)(ret == LCM_REASON_NOT_APPL ?
               LCM_PRIM_OK : LCM_PRIM_NOK), (U16)ret, &cfmMsg);


   RETVALUE(ROK);
} /* SyMiLsyCfgReq() */


/*
*
*       Fun:    SyMiLsy:StsReq
*
*       Desc:   Get statistics information. Statistics are
*               returned by a SyMiLsyStsCfm primitive. The
*               statistics counters can be reset using the action
*               parameter:
*                  ZEROSTS      - zero statistics counters
*                  NOZEROSTS    - do not zero statistics counters
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 SyMiLsyStsReq
(
Pst             *pst,           /* post structure */
Action          action,         /* action to be done */
SyMngmt         *sts            /* statistics structure */
)
#else
PUBLIC S16 SyMiLsyStsReq(pst, action, sts)
Pst             *pst;           /* post structure */
Action          action;         /* action to be done */
SyMngmt         *sts;           /* statistics structure */
#endif
{
   SyMngmt      cfmMsg;

   TRC3(SyMiLsyStsReq);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&syCbPtr)) !=
      ROK)
   {
      SYLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt, ESY013,(ErrVal)0,pst->dstInst,
            "SyMiLsyStsReq() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
      "---------Diameter------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
          "SyMiLsyStsReq(pst, action(%d), elmnt(%d))\n",
           action, sts->hdr.elmId.elmnt));
   SYDBGP(DBGMASK_SI, (syCb.init.prntBuf,
          "SGetDateTime(DateTime(%p))\n", (Ptr)&cfmMsg.t.sts.dt));

   /* initialize the confirm structure */
   SY_ZERO(&cfmMsg, sizeof (SyMngmt));
   SGetDateTime(&cfmMsg.t.sts.dt);

  /*hi002.105 - Check if Stack is Configured */
   if(!syCb.init.cfgDone)
   {
      SYDBGP(DBGMASK_LYR, (syCb.init.prntBuf,
         "SyMiLsyStsReq(): general configuration not done\n"));
      /* hi003.105 issue a statistics confirm if genCfg not done*/
      sySendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_NOK, LCM_REASON_GENCFG_NOT_DONE,
               &cfmMsg);

      RETVALUE(ROK);
   }

   switch (sts->hdr.elmId.elmnt)
   {
      case STGEN:            /* general statistics */
         syGetGenSts(&cfmMsg.t.sts.s.genSts);
         if (action == ZEROSTS)
            syZeroGenSts();
         break;

      default:
         sySendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_NOK,
                     LCM_REASON_INVALID_ELMNT, &cfmMsg);
         RETVALUE(ROK);
   }


   /* issue a statistics confirm */
   sySendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_OK, LCM_REASON_NOT_APPL,
               &cfmMsg);


   RETVALUE(ROK);
} /* SyMiLsyStsReq() */


/*
*
*       Fun:    SyMiLsyCntrlReq
*
*       Desc:   Control the specified element: enable or diable
*               trace and alarm (unsolicited status) generation,
*               delete or disable a SAP or a group of SAPs, enable
*               or disable debug printing.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 SyMiLsyCntrlReq
(
Pst             *pst,           /* post structure */
SyMngmt         *ctl            /* pointer to control structure */
)
#else
PUBLIC S16 SyMiLsyCntrlReq(pst, ctl)
Pst             *pst;           /* post structure */
SyMngmt         *ctl;           /* pointer to control structure */
#endif
{
   Header       *hdr;
   SyMngmt      cfmMsg;
   S16          ret;

   TRC3(SyMiLsyCntrlReq);

/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&syCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESY014,(ErrVal)0,pst->dstInst,
            "SyMiLsyCntrlReq() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */
   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
          "SyMiLsyCntrlReq(pst, elmnt(%d))\n", ctl->hdr.elmId.elmnt));
   SYDBGP(DBGMASK_SI, (syCb.init.prntBuf,
          "SGetDateTime(DateTime(%p))\n", (Ptr)&ctl->t.cntrl.dt));

   hdr = &ctl->hdr;
   SY_ZERO(&cfmMsg, sizeof (SyMngmt));
   SGetDateTime(&(ctl->t.cntrl.dt));

  /*hi002.105 - Check if Stack is Configured */
   if((!syCb.init.cfgDone) && (hdr->elmId.elmnt != STGEN))
   {
      SYDBGP(DBGMASK_LYR, (syCb.init.prntBuf,
         "SyMiLsyCntrlReq(): general configuration not done\n"));
      /* issue a control confirm */
      /* hi003.105 to send cntrl cfm if gen cfg not done */
      sySendLmCfm(pst, TCNTRL, hdr, LCM_PRIM_NOK, LCM_REASON_GENCFG_NOT_DONE,
               &cfmMsg);

      RETVALUE(ROK);
   }

   switch (hdr->elmId.elmnt)
   {
      case STGEN:
         ret = syCntrlGen(pst, ctl, hdr);
         break;

      default:
         SYLOGERROR_INT_PAR(ESY015, hdr->elmId.elmnt, 0,
            "SyMiLsyCntrlReq(): bad element in control request");
         ret = LCM_REASON_INVALID_ELMNT;
         break;
   }

   if (ret == LSY_REASON_OPINPROG)
   {
      sySendLmCfm(pst, TCNTRL, hdr, LCM_PRIM_OK_NDONE,
                  LCM_REASON_NOT_APPL, &cfmMsg);
      RETVALUE(ROK);
   }

   /* issue a control confirm primitive */
   sySendLmCfm(pst, TCNTRL, hdr, (U16)(ret == LCM_REASON_NOT_APPL ?
               LCM_PRIM_OK : LCM_PRIM_NOK), (U16)ret, &cfmMsg);

   RETVALUE(ROK);
} /* SyMiLsyCntrlReq() */


/*
*
*       Fun:    SyMiLsyStaReq
*
*       Desc:   Get status information. Responds with a
*               SyMiLsyStaCfm primitive.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 SyMiLsyStaReq
(
Pst             *pst,           /* post structure */
SyMngmt         *sta            /* management structure */
)
#else
PUBLIC S16 SyMiLsyStaReq(pst, sta)
Pst             *pst;           /* post structure */
SyMngmt         *sta;           /* management structure */
#endif
{
   Header       *hdr;
   SyMngmt      cfmMsg;

   TRC3(SyMiLsyStaReq);

#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&syCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESY016,(ErrVal)0,pst->dstInst,
            "SyMiLsyStaReq() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf, "SyMiLsyStaReq(pst, elmnt(%d))\n",
           sta->hdr.elmId.elmnt));

   hdr = &(sta->hdr);
   SY_ZERO(&cfmMsg, sizeof (SyMngmt));

   /* hi002.105 - fill the date and time */
   SGetDateTime(&cfmMsg.t.ssta.dt);

   switch (hdr->elmId.elmnt)
   {
      case STSID:               /* system ID */
         syGetSid(&cfmMsg.t.ssta.s.sysId);
         break;


      default:
         SYLOGERROR_INT_PAR(ESY018, hdr->elmId.elmnt, 0,
            "SyMiLsyStaReq(): invalid element in status request");
         sySendLmCfm(pst, TSSTA, hdr, LCM_PRIM_NOK,
                     LCM_REASON_INVALID_ELMNT, &cfmMsg);
         RETVALUE(ROK);
   }




   /* issue a status confirm primitive */
   sySendLmCfm(pst, TSSTA, hdr, LCM_PRIM_OK, LCM_REASON_NOT_APPL,
               &cfmMsg);


   RETVALUE(ROK);
} /* SyMiLsyStaReq() */

/*
*
*       Fun:    SyMiLsyPUA
*
*       Desc:   Get status information. Responds with a
*               SyMiLsyStaCfm primitive.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 SyMiLsyPUA
(
Buffer *mBuf
)
#else
PUBLIC S16 SyMiLsyPUA(mBuf)
Buffer *mBuf;
#endif
{
   S16 ret;
   SyPUA *pua = NULL;
   VbMmeUeCb *ueCb = NULL;

   TRC3(SyMiLsyPUA);

   /* pop pua */
   cmUnpkPtr( &pua, mBuf);
   if (pua)
      free(pua);

   return ROK;
} /* SyMiLsyPUA() */

/*
*
*       Fun:    SyMiLsyAIA
*
*       Desc:   Get status information. Responds with a
*               SyMiLsyStaCfm primitive.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 SyMiLsyAIA
(
Buffer *mBuf
)
#else
PUBLIC S16 SyMiLsyAIA(mBuf)
Buffer *mBuf;
#endif
{
   S16 ret;
   SyAIA *aia = NULL;
   VbMmeUeCb *ueCb = NULL;

   TRC3(SyMiLsyAIA);

   /* pop aia */
   cmUnpkPtr( &aia, mBuf);
   if (!aia) /* nothing to process */
      return ROK;

   /* lookup ueCb */
   ret = vbMmeUtlFndUeCbOnImsi(aia->imsi.val, aia->imsi.len, &ueCb);

#ifdef PERFORMANCE_TIMING
   STIMER_GET_CURRENT_TP(UeAttachTimers[ueCb->UeTimerIdx].aia_time);
   UeAttachTimers[ueCb->UeTimerIdx].aia_cb_time = aia->aia_resp_time;
   UeAttachTimers[ueCb->UeTimerIdx].aia_cb_start = aia->aia_cb_start;
#endif 

   if (ret == ROK) /* found the ueCb */
   {
      SY_DBG_INFO((syCb.init.prntBuf, "ueCb found"));

      if (aia->result != 2001)
      {
      	 SY_DBG_INFO((syCb.init.prntBuf, "aia->result = %u calling vbMmeSndAttachReject()", aia->result));
         ret  = vbMmeSndAttachReject(ueCb, CM_EMM_IMSI_UNKNOWN, 0);
         goto fini;
      }

      /* fill in the authVec */
      cmMemset((Void*)&ueCb->ueCtxt.authVec, 0, sizeof(ueCb->ueCtxt.authVec));

      cmMemcpy(ueCb->ueCtxt.authVec.rndNmb, aia->vector.rand.val, aia->vector.rand.len);
      cmMemcpy(ueCb->ueCtxt.authVec.autn, aia->vector.autn.val, aia->vector.autn.len);
      cmMemcpy(ueCb->ueCtxt.authVec.xres, aia->vector.xres.val, aia->vector.xres.len);
      cmMemcpy(ueCb->ueCtxt.authVec.kasme, aia->vector.kasme.val, aia->vector.kasme.len);

      /* flag the ULA as being complete */
      VB_MME_ATTCH_CHECK_ALL( ueCb, VB_MME_ATTCH_AIA_RECEIVED );
   }
   else
   {
      SY_DBG_INFO((syCb.init.prntBuf, "ueCb not found for imsi [%s]", aia->imsi.val));
      ret  = vbMmeSndAttachReject(ueCb, CM_EMM_IMSI_UNKNOWN, 0);
   }

fini:
   /* free the aia */
   free(aia);

   RETVALUE(ret);
} /* SyMiLsyAIA() */


#ifdef ANSI
PRIVATE Void copySyCfgEvtToVbCfgEvt
(
   SyMonitoringEventConfiguration * symoncfg,
   VbMonitoringEventConfiguration * vbconcfg
)
#else
PRIVATE Void copySyCfgEvtToVbCfgEvt(symoncfg, vbconcfg)
SyMonitoringEventConfiguration * symoncfg;
VbMonitoringEventConfiguration * vbconcfg;
#endif
{
   aqfdCloneOctetString(symoncfg->charged_party, &vbconcfg->charged_party);
   vbconcfg->location_information_configuration.accuracy = symoncfg->location_information_configuration.accuracy;
   vbconcfg->location_information_configuration.monte_location_type = symoncfg->location_information_configuration.monte_location_type;
   vbconcfg->maximum_number_of_reports = symoncfg->maximum_number_of_reports;
   aqfdCloneOctetString(symoncfg->monitoring_duration, &vbconcfg->monitoring_duration);
   vbconcfg->monitoring_type = symoncfg->monitoring_type;
   aqfdCloneOctetString(symoncfg->scef_id, &vbconcfg->scef_id);
   aqfdCloneOctetString(symoncfg->scef_realm, &vbconcfg->scef_realm);
   vbconcfg->scef_reference_id = symoncfg->scef_reference_id;
   vbconcfg->ue_reachability_configuration.maximum_response_time = symoncfg->ue_reachability_configuration.maximum_response_time;
   vbconcfg->ue_reachability_configuration.reachability_type = symoncfg->ue_reachability_configuration.reachability_type;
}


#ifdef ANSI
PRIVATE VbMonitoringEventConfiguration * createVbMonitoringEventConfiguration
(
   SyMonitoringEventConfiguration * symoncfg
)
#else
PRIVATE VbMonitoringEventConfiguration * createVbMonitoringEventConfiguration(symoncfg)
SyMonitoringEventConfiguration * symoncfg;
#endif
{
   VbMonitoringEventConfiguration *ret;
   //Allocate new Sys
   ret = (VbMonitoringEventConfiguration*)malloc( sizeof(VbMonitoringEventConfiguration) );
   memset( ret, 0, sizeof(*ret) );
   copySyCfgEvtToVbCfgEvt(symoncfg, ret);
   return ret;
}

#ifdef ANSI
PRIVATE Void SyMiLsyProcessMonitoringEventCfg
(
   VbMmeUeCb *ueCb,
   SyMonitoringEventConfiguration* mec,
   struct msg *answer
)
#else
PRIVATE Void SyMiLsyProcessMonitoringEventCfg(ueCb, mec, answer)
VbMmeUeCb *ueCb;
SyMonitoringEventConfiguration *mec;
struct msg *answer;
#endif
{
   CmLListCp* mmecfg = NULL;
   U32* idDeletion = NULLP;
   CmLList *nodeIdDeletion = NULLP;
   VbMonitoringEventConfiguration* mmeCfg = NULLP;
   CmLList *nodeMme = NULLP;
   int oneTime = (mec->maximum_number_of_reports == 1 || (!mec->monitoring_duration && !mec->maximum_number_of_reports));

   // figure out which event list to use
   switch ( mec->monitoring_type )
   {
      case 0: /* LOSS_OF_CONNECTIVITY */
         mmecfg = &ueCb->ueCtxt.ueHssCtxt.monEvtLossConn;
         break;
      case 1: /* UE_REACHABILITY */
         mmecfg = &ueCb->ueCtxt.ueHssCtxt.monEvtUEReach;
         break;
      case 2: /* LOCATION_REPORTING */
         mmecfg = &ueCb->ueCtxt.ueHssCtxt.monEvtLocationRep;
         break;
      case 3: /* CHANGE_OF_IMSI_IMEI_ASSOC */
         mmecfg = &ueCb->ueCtxt.ueHssCtxt.monEvtChangeImsiImei;
         break;
      case 4: /* ROAMING_STATUS */
         mmecfg = &ueCb->ueCtxt.ueHssCtxt.monEvtRoamingSt;
         break;
      case 5:  /* COMMUNICATION_FAILURE */
         mmecfg = &ueCb->ueCtxt.ueHssCtxt.monEvtCommFail;
         break;
      case 6:  /* AVAILABILITY_AFTER_DDN_FAILURE */
         mmecfg = &ueCb->ueCtxt.ueHssCtxt.monEvtAvlAfterDdnFail;
         break;
      case 7:  /* NUMBER_OF_UES_PRESENT_IN_A_GEOGRAPHICAL_AREA */
         mmecfg = &ueCb->ueCtxt.ueHssCtxt.monEvtNbUeGeoArea;
         break;
      default:
         break;
   }

   if ( !mmecfg )
   {
      aqfdAddMonitoringEventConfigStatus( mec, answer, 0, 5012 /* DIAMETER_UNABLE_TO_COMPLY */ );
      return;
   }

   //1. Remove elements included on the idr for deletion
   for ( idDeletion = (U32*) CM_LLIST_FIRST_NODE(&mec->scef_reference_id_for_deletion, nodeIdDeletion);
         idDeletion != NULLP;
         idDeletion = (U32*) CM_LLIST_NEXT_NODE(&mec->scef_reference_id_for_deletion, nodeIdDeletion))
   {
      for ( mmeCfg = (VbMonitoringEventConfiguration* ) CM_LLIST_FIRST_NODE(mmecfg, nodeMme);
            mmeCfg != NULLP;
            mmeCfg = (VbMonitoringEventConfiguration* ) CM_LLIST_NEXT_NODE(mmecfg, nodeMme))
      {
         if(mmeCfg->scef_reference_id == *idDeletion)
            break;
      }

      if(mmecfg->crnt != NULLP) {
         CmLList * element = cmLListDelFrm(mmecfg, mmecfg->crnt);
         if (element) {
            if (element->node) {
               SyMiLsyDestroyVbMonitoringEventConfiguration( (VbMonitoringEventConfiguration*)element->node );
               free( (Void*)element->node );
            }
            free( (Void*)element );
            if ( answer )
            {
               aqfdAddMonitoringEventConfigStatus( mec, answer, 0, 2001 /* DIAMETER_SUCCESS */ );
            }
         }
         else {
            if ( answer )
            {
               aqfdAddMonitoringEventConfigStatus( mec, answer, 0, 5012 /* DIAMETER_ERROR_UNABLE_TO_COMPLY */ );
            }
         }
      }
      else {
         if ( answer )
         {
            aqfdAddMonitoringEventConfigStatus( mec, answer, 10415, 5514 /* DIAMETER_ERROR_CONFIGURATION_EVENT_NON_EXISTANT */ );
         }
      }
   }

   //2. Add/Modify the elements
   if(mec->scef_reference_id != 0) { /* there is an event to add */
      for ( mmeCfg = (VbMonitoringEventConfiguration* ) CM_LLIST_FIRST_NODE(mmecfg, nodeMme);
            mmeCfg != NULLP;
            mmeCfg = (VbMonitoringEventConfiguration* ) CM_LLIST_NEXT_NODE(mmecfg, nodeMme))
      {
         if(mmeCfg->scef_reference_id == mec->scef_reference_id)
            break; //found it
      }

      if(mmecfg->crnt != NULLP) {
         copySyCfgEvtToVbCfgEvt(mec, mmeCfg);
         if ( answer )
         {
            aqfdAddMonitoringEventConfigStatus( mec, answer, 0, 2001 /* DIAMETER_SUCCESS */ );
         }
      }
      else {
         CmLList *listElement;
         listElement = (CmLList*)malloc( sizeof(CmLList) );
         memset( listElement, 0, sizeof(CmLList) );
         VbMonitoringEventConfiguration * vbconfig = createVbMonitoringEventConfiguration(mec);
         if ( vbconfig ) {
            listElement->node = (PTR)vbconfig;
            cmLListAdd2Tail( mmecfg, listElement );
            if ( answer )
            {
               aqfdAddMonitoringEventConfigStatus( mec, answer, 0, 2001 /* DIAMETER_SUCCESS */ );
            }
         }
         else {
            if ( answer )
            {
               aqfdAddMonitoringEventConfigStatus( mec, answer, 0, 5012 /* DIAMETER_ERROR_UNABLE_TO_COMPLY */ );
            }
         }
      }
   }
}

#ifdef ANSI
PRIVATE S16 SyProcessSubscriptionData
(
VbMmeUeCb *ueCb,
struct msg *ida,
SySubscriptionData *subscription_data
)
#else
PRIVATE S16 SyProcessSubscriptionData(ueCb, ida, subscription_data)
VbMmeUeCb *ueCb;
struct msg *ida;
SySubscriptionData *subscription_data;
#endif
{
   U8 msisdn[VB_HSS_MSISDN_LEN + 1];

   /* free the ue subscriptData if it exists since we are going to reassign it */
   if ( ueCb->ueCtxt.ueHssCtxt.subscriptionData )
   {
      aqfdDestroySubscriptionData( ueCb->ueCtxt.ueHssCtxt.subscriptionData );
      free(ueCb->ueCtxt.ueHssCtxt.subscriptionData);
      ueCb->ueCtxt.ueHssCtxt.subscriptionData = NULL;
   }

   ueCb->ueCtxt.ueHssCtxt.subscriptionData = subscription_data;

   /* msisdn*/
   memset( msisdn, 0, sizeof(msisdn) );
   aqfdTBCD2Str( subscription_data->msisdn->val, subscription_data->msisdn->len, msisdn );
   vbStrArr(msisdn);
   memcpy( ueCb->ueCtxt.ueHssCtxt.msisdn, msisdn, sizeof(msisdn) - 1 );

   SyMonitoringEventConfiguration* mec = NULLP;
   CmLList *nodeIdr = NULLP;
   for ( mec = (SyMonitoringEventConfiguration* ) CM_LLIST_FIRST_NODE(&subscription_data->monitoring_event_configuration, nodeIdr);
         mec != NULLP;
         mec = (SyMonitoringEventConfiguration* ) CM_LLIST_NEXT_NODE(&subscription_data->monitoring_event_configuration, nodeIdr))
   {
      SyMiLsyProcessMonitoringEventCfg( ueCb, mec, ida );
   }

   return ROK;
}

#ifdef ANSI
PUBLIC S16 SyMiLsyIDR
(
)
#else
PUBLIC S16 SyMiLsyIDR()
#endif
{
   S16 ret;
   SyIDR *idr = NULL;
   VbMmeUeCb *ueCb = NULL;
   struct msg *ida = NULL;

   TRC3(SyMiLsyAIA);

   /* pop aia */
   idr = (SyIDR*)aqQueuePop(&aqCb.idrQueue);
   if (!idr) /* nothing to process */
      return ROK;

   /* create the IDA */
   if ( (ret = aqfdCreateIDA(idr,&ida)) != LCM_REASON_NOT_APPL )
   {
      SY_DBG_INFO((syCb.init.prntBuf, "aqfdCreateIDA() returned %d", ret ));
      ret = RFAILED;
      goto fini;
   }

   /* lookup ueCb */
   ret = vbMmeUtlFndUeCbOnImsi(idr->imsi.val, idr->imsi.len, &ueCb);

   if (ret == ROK) /* found the ueCb */
   {
      SyProcessSubscriptionData( ueCb, ida, idr->subscription_data );

      idr->subscription_data = NULL; /* set to NULL so it doesn't get deleted */

      aqfdSendIDA( ida, 0, 2001); /* DIAMETER_SUCCESS */
   }
   else
   {
      aqfdSendIDA( ida, 10415, 5001); /* DIAMETER_ERROR_USER_UNKNOWN */
   }

fini:
   /* free the idr */
   if (idr->subscription_data)
   {
      aqfdDestroySubscriptionData( idr->subscription_data );
      free( idr->subscription_data );
   }
   free(idr);
   RETVALUE(ret);
}

#ifdef ANSI
PUBLIC Void setApn
(
   VbHssPdnSubCntxt *subctxt,
   AqOctetString *ostr
)
#else
PUBLIC Void setApn(subctxt, ostr)
VbHssPdnSubCntxt *subctxt;
AqOctetString *ostr;
#endif
{
   /*
    * each segment of the apn is prefixed with a 1 byte binary length
    * the resulting value can be directly compared with values received
    * via the s1ap or can be copied for use in s11 messages
    */
   int i;
   S8 *dst = &subctxt->apn.label[1];
   S8 *plen = subctxt->apn.label;

   *plen = 0;
   subctxt->apn.len = 1;

   for (i = 0; i < ostr->len; i++)
   {
      if (ostr->val[i] == '.')
      {
         plen = dst;
         *plen = 0;
      }
      else
      {
         *dst = (S8)ostr->val[i];
         (*plen)++;
      }
      dst++;
      subctxt->apn.len++;
   }
}

/*
*
*       Fun:    SyMiLsyULAAttachRequest
*
*       Desc:   
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 SyMiLsyULAAttachRequest
(
Buffer *mBuf
)
#else
PUBLIC S16 SyMiLsyULAAttachRequest(mBuf)
Buffer *mBuf;
#endif
{
   S16 ret;
   SyULA *ula = NULL;
   VbMmeUeCb *ueCb = NULL;
   SyAPNConfiguration *papncfg = NULL;
   U32 address;
   CmLList *llnode;
   S16 idx;
#ifdef PERFORMANCE_TIMING
   stimer_t tmpTimer;
#endif

   TRC3(SyMiLsyULAAttachRequest);

   /* pop ula */
   cmUnpkPtr(&ula, mBuf);
   if (!ula) /* nothing to process */
      return ROK;

#ifdef PERFORMANCE_TIMING
   STIMER_GET_CURRENT_TP(tmpTimer);
#endif

   /* lookup ueCb */
   ret = vbMmeUtlFndUeCbOnImsi(ula->imsi.val, ula->imsi.len, &ueCb);
#ifdef PERFORMANCE_TIMING
   UeAttachTimers[ueCb->UeTimerIdx].ula_cb_time = ula->ula_ans_time;
   UeAttachTimers[ueCb->UeTimerIdx].ula_cb_start = ula->ula_cb_start;
   UeAttachTimers[ueCb->UeTimerIdx].ula_time = tmpTimer;
#endif

   if (ret == ROK) /* found the ueCb */
   {
      SY_DBG_INFO((syCb.init.prntBuf, "ueCb found)"));

      if ( !(ula->result_code == 2001 || (ula->experimental_result.present && ula->experimental_result.result_code == 2001) ) )
      {
      	 SY_DBG_INFO((syCb.init.prntBuf, "ula->result_code = %u ula->experimental_result.present = %d ula->experimental_result.result_code = %u calling vbMmeSndAttachReject()", ula->result_code, ula->experimental_result.present, ula->experimental_result.result_code));
         ret  = vbMmeSndAttachReject(ueCb, CM_EMM_IMSI_UNKNOWN, 0);
         goto fini;
      }

      SyProcessSubscriptionData( ueCb, NULL, ula->subscription_data );

      ula->subscription_data = NULL; /* set to NULL so it doesn't get deleted */

      llnode = ueCb->ueCtxt.ueHssCtxt.subscriptionData->apn_configuration_profile.apn_configuration.first;

      if ( !llnode || !llnode->node )
      {
         ret  = vbMmeSndAttachReject(ueCb, CM_EMM_IMSI_UNKNOWN, 0);
         goto fini;
      }

      papncfg = (SyAPNConfiguration*)llnode->node;

      /* set defaults */
      ueCb->ueCtxt.ueHssCtxt.isMsPsPurgd =0;
      ueCb->ueCtxt.ueHssCtxt.subUeAMBR.dl =200000000;
      ueCb->ueCtxt.ueHssCtxt.subUeAMBR.ul =100000000;
      ueCb->ueCtxt.ueHssCtxt.subUeAMBR.dlExt =22;
      ueCb->ueCtxt.ueHssCtxt.subUeAMBR.ulExt =33;
      ueCb->ueCtxt.ueHssCtxt.subUeAMBR.dlExt2 =44;
      ueCb->ueCtxt.ueHssCtxt.subUeAMBR.dlExt2 =55;

      for (idx=0; papncfg && idx < VB_HSS_MAX_PDN_SUB_CTXT; idx++)
      {
         /* ambr */
         address = *(U32*)((AqAddress*)papncfg->mip6_agent_info.mip_home_agent_address.first->node)->address;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].cntxtId = 1;
         if (papncfg->non_ip_pdn_type_indicator == 0) /* FALSE */
         {
            ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].nonIpPdn = FALSE;
            ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].addr.ip.pdnAddrLst.nAddr[0].type = CM_INET_IPV4ADDR_TYPE;
            ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].addr.ip.pdnAddrLst.nAddr[0].u.ipv4NetAddr = 0; //calling party
            ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].addr.ip.pdnAddrType = 4;
         }
         else /* TRUE */
         {
            ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].nonIpPdn = TRUE;
            if (papncfg->non_ip_data_delivery_mechanism == 1) /* SCEF-BASED-DATA-DELIVERY */
            {
               ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].nonIpDdm = VB_HSS_DDM_SCEF;
               strcpy(ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].addr.scef.scefid, (const char *)papncfg->scef_id->val);
               strcpy(ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].addr.scef.scefrealm, (const char *)papncfg->scef_realm->val);
            }
            else /* default to SGi-BASED-DATA-DELIVERY */
            {
               ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].nonIpDdm = VB_HSS_DDM_SGI;
            }
         }

         /* strcpy(ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].apn, (const char *)papncfg->service_selection->val); */
         setApn(&ueCb->ueCtxt.ueHssCtxt.subCntxt[idx], papncfg->service_selection);

         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].qos.qci = papncfg->eps_subscribed_qos_profile.qos_class_identifier;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].qos.arp.priLevel =15;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].qos.arp.preCapbFlg =8;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].qos.arp.preVlnbFlg =9;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].qos.maxBitRateUL =25400000;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].qos.maxBitRateDL = 25400000;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].qos.guaraBitRateUL =25400000;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].qos.guaraBitRateDL =25400000;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].qos.maxBitRateULExt =6;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].qos.maxBitRateDLExt =6;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].qos.guaraBitRateULExt =6;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].qos.guaraBitRateDLExt =6;

         /*9 15 8 9 25400000 25400000 25400000 25400000 6 6 6 6 am: 2 3 4 5 6 7 */
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].ambr.dl = papncfg->ambr.max_requested_bandwidth_ul;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].ambr.ul = papncfg->ambr.max_requested_bandwidth_dl;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].ambr.dlExt = 4;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].ambr.ulExt = 5;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].ambr.dlExt2 = 6;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].ambr.ulExt2 = 7;

         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].pdnGwAddr.u.ipv4NetAddr= address;
         ueCb->ueCtxt.ueHssCtxt.subCntxt[idx].allocType = 0;

         llnode = llnode->next;
         papncfg = llnode ? (SyAPNConfiguration*)llnode->node : NULL;
      }

      /* flag the ULA as being complete */
      VB_MME_ATTCH_CHECK_ALL( ueCb, VB_MME_ATTCH_ULA_RECEIVED );

      /* do not free subscrptionData since it is now associated with ueCb */
      goto fini1;
   }
   else
   {
      S8 imsi[VB_HSS_IMSI_LEN + 1];
      aqfdImsi2Str( ula->imsi.val, ula->imsi.len, imsi );
      SY_DBG_INFO((syCb.init.prntBuf, "ueCb not found for imsi [%s]", imsi));
      ret  = vbMmeSndAttachReject(ueCb, CM_EMM_IMSI_UNKNOWN, 0);
   }

fini:
   /* free the ula */
   if (ula->subscription_data)
   {
      aqfdDestroySubscriptionData( ula->subscription_data );
      free( ula->subscription_data );
   }
   free(ula);

fini1:
   RETVALUE(ret);
} /* SyMiLsyULAAttachRequest() */

#if 0


/*
*     upper interface (HIT) primitives
*/


/*
*
*       Fun:    HiUiHitBndReq
*
*       Desc:   Binds a service user to TUCL. A TSAP is assigned
*               for this bind and the identity of the service user
*               is recorded. A bind confirm primitive is then
*               issued.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitBndReq
(
Pst            *pst,            /* post structure */
SuId            suId,           /* service user id */
SpId            spId            /* service provider id */
)
#else
PUBLIC S16 HiUiHitBndReq(pst, suId, spId)
Pst            *pst;            /* post structure */
SuId            suId;           /* service user id */
SpId            spId;           /* service provider id */
#endif
{
   HiSap        *sap;
   HiAlarmInfo  alInfo;
#ifdef HI_RUG
   U16          i;
   Bool         found;
#endif


   TRC3(HiUiHitBndReq);

/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&syCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESY020,(ErrVal)0,pst->dstInst,
            "HiUiHitBndReq() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */


   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
          "HiUiHitBndReq(pst, suId(%d), spId(%d))\n",
          suId, spId));

   cmMemset((U8*)&alInfo, 0, sizeof(HiAlarmInfo));

   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_BNDREQ, alInfo, sap);


   /* If the SAP is already bound, nothing happens here and a bind
    * confirm is issued.
    */
   if (sap->state == HI_ST_BND)
   {
      HiUiHitBndCfm(&sap->uiPst, suId, CM_BND_OK);
      RETVALUE(ROK);
   }


   /* copy bind configuration parameters */
   sap->suId            = suId;
   sap->uiPst.dstProcId = pst->srcProcId;
   sap->uiPst.dstEnt    = pst->srcEnt;
   sap->uiPst.dstInst   = pst->srcInst;
   sap->uiPst.srcProcId = pst->dstProcId;
   sap->uiPst.srcInst   = pst->dstInst;

   /* duplicate Psts for multi-threaded case */
   HI_DUPLICATE_SAP_PST(sap, sap->uiPst);


#ifdef HI_RUG
   /* find interface version from stored info */
   if (!sap->remIntfValid)
   {
      found = FALSE;
      for (i = 0; i < syCb.numIntfInfo && !found; i++)
      {
         if (syCb.intfInfo[i].intf.intfId == HITIF)
         {
            switch (syCb.intfInfo[i].grpType)
            {
               case SHT_GRPTYPE_ALL:
                  if ((syCb.intfInfo[i].dstProcId == pst->srcProcId) &&
                      (syCb.intfInfo[i].dstEnt.ent == pst->srcEnt) &&
                      (syCb.intfInfo[i].dstEnt.inst == pst->srcInst))
                     found = TRUE;
                     break;

               case SHT_GRPTYPE_ENT:
                  if ((syCb.intfInfo[i].dstEnt.ent ==pst->srcEnt) &&
                      (syCb.intfInfo[i].dstEnt.inst == pst->srcInst))
                     found = TRUE;
                     break;

               default:
                  break;
            }
         }
      }

      if (!found)
      {
         SYLOGERROR_INT_PAR(ESY021, spId, pst->dstInst,
              "HiUiHitBndReq(): remIntfver not available");

         /* generate alarm to layer manager */
         alInfo.spId = spId;
         alInfo.type = LHI_ALARMINFO_PAR_TYPE;
         hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_BNDREQ,
                     LCM_CAUSE_SWVER_NAVAIL, &alInfo);

         /* send NOK to service user layer */
         HiUiHitBndCfm(&sap->uiPst, suId, CM_BND_NOK);

         RETVALUE(ROK);
      }

      sap->uiPst.intfVer = syCb.intfInfo[i-1].intf.intfVer;
      sap->remIntfValid = TRUE;
   }
#endif /* HI_RUG */


   /* SAP state is now bound and enabled */
   sap->state = HI_ST_BND;


   /* issue a bind confirm */
   HiUiHitBndCfm(&sap->uiPst, suId, CM_BND_OK);


   RETVALUE(ROK);
} /* HiUiHitBndReq() */


/*
*
*       Fun:    HiUiHitUbndReq
*
*       Desc:   Unbind a service user from TUCL. All connections
*               are closed and the TSAP is unassigned.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitUbndReq
(
Pst             *pst,           /* post structure */
SpId            spId,           /* service provider Id */
Reason          reason          /* cause for unbind operation */
)
#else
PUBLIC S16 HiUiHitUbndReq(pst, spId, reason)
Pst             *pst;           /* post structure */
SpId            spId;           /* service provider Id */
Reason          reason;         /* cause for unbind operation */
#endif
{
   S16          ret;
   U16          i;
   HiSap        *sap;
   HiThrMsg     tMsg;
   HiAlarmInfo  alInfo;


   TRC3(HiUiHitUbndReq);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&syCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESY022,(ErrVal)0,pst->dstInst,
            "HiUiHitUBndReq() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifndef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */

   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
          "HiUiHitUbndReq(pst, spId(%d), reason(%d))\n",
          spId, reason));

   cmMemset((U8*)&alInfo, 0, sizeof(HiAlarmInfo));

   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_BNDREQ, alInfo, sap);


   /* return if SAP is unbound */
   if (sap->state == HI_ST_UBND)
      RETVALUE(ROK);


   /* change SAP state to configured but not bound */
   sap->state = HI_ST_UBND;


   /* Send a disable SAP message to each group. The group
    * threads will close all connections associated with the
    * SAP.
    */
   tMsg.type = HI_THR_DISSAP;
   tMsg.spId = sap->spId;
   for (i = 0;  i < syCb.numFdGrps;  i++)
   {
      ret = hiSendThrMsg(i, &tMsg);
      if (ret != ROK)
      {
         /* we're left in an unstable state! */
         SYDBGP(DBGMASK_LYR, (syCb.init.prntBuf,
            "HiUiHitUbndReq(): send message to group thread failed\n"));
         RETVALUE(RFAILED);
      }
   }


   RETVALUE(ROK);
} /* HiUiHitUbndReq() */


/*
*
*       Fun:    HiUiHitServOpenReq
*
*       Desc:   Open a server on the address provided. TUCL
*               creates a new non-blocking socket and binds it to
*               the specified local address. Any socket options
*               specified in tPar are set. A connection control
*               block is created and assigned to an fd group. The
*               group thread issues a HiUiHitConCfm primitive on
*               success; a disconnect indication is sent in case
*               of any error.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitServOpenReq
(
Pst             *pst,           /* post stucture */
SpId            spId,           /* service provider SAP Id */
UConnId         servConId,      /* service user's connection Id */
CmTptAddr       *servTAddr,     /* transport address of the server */
CmTptParam      *tPar,          /* transport options */
CmIcmpFilter    *icmpFilter,    /* filter parameters */
U8              srvcType        /* service type */
)
#else
PUBLIC S16 HiUiHitServOpenReq(pst, spId, servConId, servTAddr, tPar,
                              icmpFilter, srvcType)
Pst             *pst;           /* post stucture */
SpId            spId;           /* service provider SAP Id */
UConnId         servConId;      /* service user's connection Id */
CmTptAddr       *servTAddr;     /* transport address of the server */
CmTptParam      *tPar;          /* transport options */
CmIcmpFilter    *icmpFilter;    /* filter parameters */
U8              srvcType;       /* service type */
#endif
{
   S16          ret;
   HiSap        *sap;
   HiConCb      *conCb;
   HiAlarmInfo  alInfo;
   U8           type;
   S16          backLog = 0;
 /* hi005.105(hi023.104) - Store the IP TOS*/
   U8             i;         /* loop counter */


   TRC3(HiUiHitServOpenReq);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&syCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESY023,(ErrVal)0,pst->dstInst,
            "HiUiHitServOpenReq() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifndef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */


   /* hi029.201: Fix for compilation warning*/
#ifndef BIT_64
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
          "HiUiHitServOpenReq(pst, spId(%d), servConId(%ld), "
          "servTAddr(%p), tPar(%p), srvcType(%d))\n",
          spId, servConId, (Ptr)servTAddr, (Ptr)tPar, srvcType));
#else
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
          "HiUiHitServOpenReq(pst, spId(%d), servConId(%d), "
          "servTAddr(%p), tPar(%p), srvcType(%d))\n",
          spId, servConId, (Ptr)servTAddr, (Ptr)tPar, srvcType));
#endif



   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_SERVOPENREQ, alInfo, sap);


   /* check SAP state */
   HI_CHK_SAPSTATE(sap, LHI_EVENT_SERVOPENREQ, alInfo);


   /* must have sufficient resources to establish a connection */
	/* hi025.201 Gaurd the check resource implementation under the
	 * flag HI_NO_CHK_RES */
#ifndef HI_NO_CHK_RES
   hiChkRes(sap);
   if (sap->resCongStrt)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_OUTOF_RES);
      RETVALUE(ROK);
   }
#endif /* HI_NO_CHK_RES */


   /* initialize alarm information */
   alInfo.spId = spId;
   alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;


#if (ERRCLASS & ERRCLS_INT_PAR)
   /* check server transport address */
   if (!HI_CHK_ADDR(servTAddr))
   {
      SYLOGERROR_INT_PAR(ESY024, 0, 0,
         "HiUiHitServOpenReq(): invalid server address");
      HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_INV_PAR_VAL);
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_TPT_ADDR;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_SERVOPENREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }

   /* for ICMP, filter must be provided */
   if (((srvcType & 0x0F) == HI_SRVC_RAW_ICMP
            &&  !HI_CHK_ICMPFILTER(icmpFilter)))
   {
      SYLOGERROR_INT_PAR(ESY025, 0, 0,
         "HiUiHitServOpenReq(): invalid filter parameter");
      HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_INV_PAR_VAL);
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_FILTER_TYPE_COMB;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_SERVOPENREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }

#endif /* ERRCLS_INT_PAR */


   /* allocate and initialize a conCb for this server */
   ret = hiAllocConCb(sap, servConId, srvcType, &type, &conCb);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_SMEM_ALLOC_ERR);
      RETVALUE(RFAILED);
   }


   /* Open and set up the server socket. ICMP is handled
    * separately, later.
    */
   if (conCb->protocol != CM_PROTOCOL_ICMP)
   {
      ret = hiCreateSock(TRUE, type, servTAddr, tPar, conCb);
      if (ret != ROK)
      {
         HI_DISCIND(sap, HI_USER_CON_ID, conCb->suConId, ret);
         HI_FREECONCB(conCb);
         RETVALUE(RFAILED);
      }

      /* need to listen for stream type connections */
      if (type == CM_INET_STREAM)
      {
         if (tPar  &&  tPar->type == CM_TPTPARAM_SOCK)
            backLog = tPar->u.sockParam.listenQSize;


         ret = HI_LISTEN(&conCb->conFd, backLog);
         if (ret != ROK)
         {
            HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_SOCK_LSTN_ERR);
            HI_CLOSE_SOCKET(&conCb->conFd);
            HI_FREECONCB(conCb);
            HI_INC_ERRSTS(syCb.errSts.sockLstnErr);
            RETVALUE(RFAILED);
         }
      }

   }


#ifdef IPV6_SUPPORTED
   /* for IPv6 addresses, set some connection flags */
   if (servTAddr->type == CM_TPTADDR_IPV6)
   {
      if (conCb->flag & HI_FL_RAW)
         conCb->flag |= HI_FL_RAW_IPV6;
#ifdef IPV6_OPTS_SUPPORTED
      if (conCb->protocol == CM_PROTOCOL_RSVP)
         conCb->ipv6OptsReq = TRUE;
#endif
   }
#endif /* IPV6_SUPPORTED */


   /* set connection state */
   conCb->state = (type == CM_INET_STREAM
                     ? HI_ST_SRV_LISTEN
                     : HI_ST_CONNECTED);


   /* Complete the connection control block: get an spConId and
    * put it in the SAP's connections hash list.
    */
   ret = hiCompleteConCb(conCb);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_CONID_NOT_AVAIL);
      alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;
      hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_SERVOPENREQ,
                  LHI_CAUSE_CONID_NOT_AVAIL, &alInfo);
      if (conCb->protocol != CM_PROTOCOL_ICMP)
         HI_CLOSE_SOCKET(&conCb->conFd);
      HI_FREECONCB(conCb);
      RETVALUE(RFAILED);
   }


   /* ICMP filters specified? */
   if (HI_CHK_ICMPFILTER(icmpFilter))
   {
      /* Process the specified filters and store them in the
       * connection.
       */
      ret = hiProcessIcmpReq(conCb, icmpFilter);
      if (ret != ROK)
      {
         HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_INTERNAL_ERR);
         HI_FREECONCB(conCb);
         RETVALUE(RFAILED);
      }
   }
   /* hi002.105 (hi023.104) - Store the IP TOS*/
   conCb->ipTos= 0xff;
   /* hi018.201: Added tPar Null check */
   for (i = 0; ((tPar != NULLP) && (i < tPar->u.sockParam.numOpts && tPar->type != CM_TPTPARAM_NOTPRSNT)); i++)
   {
           CmSockOpts *sOpts = &(tPar->u.sockParam.sockOpts[i]);
           if (sOpts->option == CM_SOCKOPT_OPT_TOS )
           {
               conCb->ipTos= (U8)sOpts->optVal.value;
               break;
           }
   }



   /* assign the connection to an fd group */
   ret = hiAssignConCb(conCb, HI_THR_ADDCON_CONCFM);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_INTERNAL_ERR);
      alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;
      hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_SERVOPENREQ,
                  LHI_CAUSE_INTPRIM_ERR, &alInfo);
      if (conCb->protocol != CM_PROTOCOL_ICMP)
         HI_CLOSE_SOCKET(&conCb->conFd);
      HI_FREECONCB(conCb);
      RETVALUE(RFAILED);
   }


   /* update number of connections stat */
   HI_INC_TXSTS(sap, sap->txSts.numCons);


   /* connect confirm is issued from the thread */


   RETVALUE(ROK);
} /* HiUiHitServOpenReq() */


/*
*
*       Fun:    HiUiHitConReq
*
*       Desc:   Open a new client connection. TUCL binds a new
*               non-blocking socket to the local address specified
*               and connects to the remote address specified. A
*               new connection control block is created and
*               assigned to an fd group. The group thread issues a
*               HiUiHitConCfm primitive on success; a
*               disconnect indication is issued to the service
*               user in case of any failure.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitConReq
(
Pst             *pst,           /* post structure */
SpId            spId,           /* service provider id */
UConnId         suConId,        /* service user's connection id */
CmTptAddr       *remAddr,       /* remote address */
CmTptAddr       *localAddr,     /* local address */
CmTptParam      *tPar,          /* transport parameters */
U8              srvcType        /* service type */
)
#else
PUBLIC S16 HiUiHitConReq(pst, spId, suConId, remAddr, localAddr,
                         tPar, srvcType)
Pst             *pst;           /* post structure */
SpId            spId;           /* service provider id */
UConnId         suConId;        /* service user's connection id */
CmTptAddr       *remAddr;       /* remote address */
CmTptAddr       *localAddr;     /* local address */
CmTptParam      *tPar;          /* transport parameters */
U8              srvcType;       /* service type */
#endif
{
   S16          ret;
   HiSap        *sap;
   HiConCb      *conCb;
   HiAlarmInfo  alInfo;
   U8           type;
   U8           srvc;


   TRC3(HiUiHitConReq);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&syCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESY027,(ErrVal)0,pst->dstInst,
            "HiUiHitConReq() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifndef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */

#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T ConReq HC->HI, in HI");
#endif


   /* hi029.201: Fix for compilation warning*/
#ifndef BIT_64
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
          "HiUiHitConReq(pst, spId(%d), suConId(%ld), remAddr(%p), "
          "localAddr(%p), tPar(%p), srvcType(%d))\n",
          spId, suConId, (Ptr)remAddr, (Ptr)localAddr, (Ptr)tPar, srvcType));
#else
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
          "HiUiHitConReq(pst, spId(%d), suConId(%d), remAddr(%p), "
          "localAddr(%p), tPar(%p), srvcType(%d))\n",
          spId, suConId, (Ptr)remAddr, (Ptr)localAddr, (Ptr)tPar, srvcType));
#endif


   /* get the predefined service type being requested */
   srvc = srvcType & 0x0F;


   /* initialize alarm information */
   alInfo.spId = spId;
   alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;


   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_CONREQ, alInfo, sap);


   /* check SAP state */
   HI_CHK_SAPSTATE(sap, LHI_EVENT_CONREQ, alInfo);


#if (ERRCLASS & ERRCLS_INT_PAR)
   /* validate the service type requested */
   if (srvc != HI_SRVC_UDP
         &&  srvc != HI_SRVC_UDP_TPKT_HDR
         &&  srvc != HI_SRVC_TCP_TPKT_HDR
         &&  srvc != HI_SRVC_TCP_NO_HDR)
   {
      SYLOGERROR_INT_PAR(ESY028, srvcType, 0,
         "HiUiHitConReq(): invalid service type");
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_INV_PAR_VAL);
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_TPT_ADDR;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_CONREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }
#endif /* ERRCLS_INT_PAR */


   /* must have sufficient resources to establish a connection */

#ifndef HI_NO_CHK_RES
   hiChkRes(sap);
   if (sap->resCongStrt)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_OUTOF_RES);
      RETVALUE(RFAILED);
   }
#endif /* HI_NO_CHK_RES */

   /* allocate and initialize a conCb for this client */
   ret = hiAllocConCb(sap, suConId, srvcType, &type, &conCb);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_SMEM_ALLOC_ERR);
      RETVALUE(RFAILED);
   }


#if (ERRCLASS & ERRCLS_INT_PAR)
   /* check remote address for TCP clients */
   if (type == CM_INET_STREAM  &&  !HI_CHK_ADDR(remAddr))
   {
      SYLOGERROR_INT_PAR(ESY029, 0, 0,
         "HiUiHitConReq(): invalid remote address");
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_INV_PAR_VAL);
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_TPT_ADDR;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_CONREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      HI_FREECONCB(conCb);
      RETVALUE(RFAILED);
   }

#endif /* ERRCLS_INT_PAR */


#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T cmInet-calls strt, in HI");
#endif


   /* open a new socket */
   ret = hiCreateSock(FALSE, type, localAddr, tPar, conCb);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, conCb->suConId, ret);
      HI_FREECONCB(conCb);
      RETVALUE(RFAILED);
   }


   /* Issue a connect for stream type connections or if a remote
    * address has been specified for datagram connections.
    */
   conCb->state = HI_ST_CONNECTED;
   if (HI_CHK_ADDR(remAddr))
   {
      ret = HI_CONNECT(&conCb->conFd, remAddr);
      if (ret == RFAILED  ||  ret == RCLOSED)
      {
         HI_DISCIND(sap, HI_USER_CON_ID, conCb->suConId,
                    (ret == RFAILED
                     ? HI_SOCK_CONN_ERR
                     : HI_CON_CLOSED_BY_PEER));
         HI_CLOSE_SOCKET(&conCb->conFd);
         HI_FREECONCB(conCb);
         HI_INC_ERRSTS(syCb.errSts.sockCnctErr);
         RETVALUE(RFAILED);
      }
      else if (ret == RINPROGRESS  ||  ret == ROKDNA
               ||  ret == RWOULDBLOCK)
         conCb->state = HI_ST_CLT_CONNECTING;

      /* this a proper UDP client? */
      if (conCb->flag & HI_FL_UDP)
         conCb->flag |= HI_FL_UDP_CLT;
   }


#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T cmInet-calls end, in HI");
#endif


   /* store remote address */
   cmMemcpy((U8 *)&conCb->peerAddr, (U8 *)remAddr, sizeof (CmTptAddr));


   /* Complete the connection control block: get an spConId and
    * put it in the SAP's connections hash list.
    */
   ret = hiCompleteConCb(conCb);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_CONID_NOT_AVAIL);
      hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_CONREQ,
                  LHI_CAUSE_CONID_NOT_AVAIL, &alInfo);
      HI_CLOSE_SOCKET(&conCb->conFd);
      HI_FREECONCB(conCb);
      RETVALUE(RFAILED);
   }




   /* assign the connection to an fd group */
   ret = hiAssignConCb(conCb, HI_THR_ADDCON_CONCFM);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_INTERNAL_ERR);
      hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_CONREQ,
                  LHI_CAUSE_INTPRIM_ERR, &alInfo);
      HI_CLOSE_SOCKET(&conCb->conFd);
      HI_FREECONCB(conCb);
      RETVALUE(RFAILED);
   }


   /* update number of connections stat */
   HI_INC_TXSTS(sap, sap->txSts.numCons);


   /* connect confirm is issued from the thread */


   RETVALUE(ROK);
} /* HiUiHitConReq() */


/*
*
*       Fun:    HiUiHitConRsp
*
*       Desc:   Accept the new client connection indicated by a
*               connection indication primitive. The specified
*               suConId is stored in the connection block and data
*               transfer can be initiated.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  If the connection control block corresponding to
*               spConId is not found, TUCL returns silently. This
*               can happen if a disconnect indication has been
*               issued for the connection and the block has been
*               released.
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitConRsp
(
Pst             *pst,           /* post structure */
SpId            spId,           /* service provider id */
UConnId         suConId,        /* service user's connection Id */
UConnId         spConId         /* service provider's connection Id */
)
#else
PUBLIC S16 HiUiHitConRsp(pst, spId, suConId, spConId)
Pst             *pst;           /* post structure */
SpId            spId;           /* service provider id */
UConnId         suConId;        /* service user's connection Id */
UConnId         spConId;        /* service provider's connection Id */
#endif
{
   S16          ret;
   HiSap        *sap;
   HiConCb      *conCb;
   HiAlarmInfo  alInfo;
   HiThrMsg     tMsg;


   TRC3(HiUiHitConRsp);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&syCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESY031,(ErrVal)0,pst->dstInst,
            "HiUiHitConRsp() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifndef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */

#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T ConRsp HC->HI, in HI");
#endif


   /* hi029.201:Fix for compilation warning*/
#ifndef BIT_64
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
          "HiUiHitConRsp(pst, spId(%d), suConId(%ld), spConId(%ld))\n",
          spId, suConId, spConId));
#else
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
          "HiUiHitConRsp(pst, spId(%d), suConId(%d), spConId(%d))\n",
          spId, suConId, spConId));
#endif


   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_CONRSP, alInfo, sap);


   /* check SAP state */
   HI_CHK_SAPSTATE(sap, LHI_EVENT_CONRSP, alInfo);


   /* initialize alarm information */
   alInfo.spId = spId;
   alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;


   /* find the connection block */
   ret = hiFindConCb(sap, spConId, &conCb);
   if (ret != ROK)
   {
      /* not found, issue a disconnect indication */
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_INV_PAR_VAL);
      RETVALUE(RFAILED);
   }


#if (ERRCLASS & ERRCLS_DEBUG)
   /* check the connection state */
   if (conCb->state != HI_ST_AW_CON_RSP)
   {
      alInfo.type = LHI_ALARMINFO_CON_STATE;
      alInfo.inf.conState = conCb->state;
      hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_CONRSP,
                  LHI_CAUSE_INV_CON_STATE, &alInfo);
      conCb->state = HI_ST_DISCONNECTING;
      hiDoneWithConCb(conCb);
      tMsg.type = HI_THR_DELCON_DISCIND;
      tMsg.spId = sap->spId;
      tMsg.spConId = conCb->spConId;
      tMsg.disc.reason = HI_INV_CON_STATE;
      hiSendThrMsg(conCb->fdGrpNum, &tMsg);
      RETVALUE(RFAILED);
   }
#endif


   /* Store the provided suConId, shift state, and tell the
    * group thread to start processing this connection.
    */
   conCb->suConId = suConId;
   conCb->state = HI_ST_CONNECTED;
   tMsg.type = HI_THR_RSPCON;
   tMsg.spId = sap->spId;
   tMsg.spConId = conCb->spConId;
   hiSendThrMsg(conCb->fdGrpNum, &tMsg);
   hiDoneWithConCb(conCb);


   /* data transfer is now possible on this connection */


   RETVALUE(ROK);
} /* HiUiHitConRsp() */


/*
*
*       Fun:    HiUiHitDatReq
*
*       Desc:   Transmit data on a stream connection. If the SAP
*               is under flow control, the primitive is dropped.
*               Otherwise, TUCL marks the message boundaries in
*               accordance with RFC 1006 if required for this
*               connection and sends the message. If the entire
*               message cannot be sent, the untransmitted portion
*               is queued. If the total untransmitted data exceeds
*               the threshold specified in the SAP configuration,
*               a flow control indication is issued and the data
*               is dropped.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  If an invalid spConId is specified, the data is
*               dropped silently. In other error cases, a
*               disconnect indication is issued.
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitDatReq
(
Pst             *pst,           /* post structure */
SpId            spId,           /* service provider Id */
UConnId         spConId,        /* service provider's connection Id */
Buffer          *mBuf           /* message buffer */
)
#else
PUBLIC S16 HiUiHitDatReq(pst, spId, spConId, mBuf)
Pst             *pst;           /* post structure */
SpId            spId;           /* service provider Id */
UConnId         spConId;        /* service provider's connection Id */
Buffer          *mBuf;          /* message buffer */
#endif
{
   S16          ret;
   HiSap        *sap;
   HiConCb      *conCb;
   MsgLen       mLen, txLen;
   QLen         qLen;
   Buffer       *qBuf;
   HiAlarmInfo  alInfo;
   HiThrMsg     tMsg;


   TRC3(HiUiHitDatReq);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&syCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESY032,(ErrVal)0,pst->dstInst,
            "HiUiHitDatReq() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifndef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */

#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T DatReq HC->HI, in HI");
#endif


   /* hi029.201:Fix for compilation warning*/
#ifndef BIT_64
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
          "HiUiHitDatReq(pst, spId(%d), spConId(%ld), mBuf(%p)))\n",
          spId, spConId, (Ptr)mBuf));
#else
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
          "HiUiHitDatReq(pst, spId(%d), spConId(%d), mBuf(%p)))\n",
          spId, spConId, (Ptr)mBuf));
#endif


   /* initialize alarm information */
   alInfo.spId = spId;
   alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;


   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_DATREQ, alInfo, sap);


   /* check SAP state */
   HI_CHK_SAPSTATE(sap, LHI_EVENT_DATREQ, alInfo);


   /* hi018.201: mLen is initialized to zero */
   mLen = 0;
   /* get length of data payload */
   ret = SFndLenMsg(mBuf, &mLen);

#if (ERRCLASS & ERRCLS_INT_PAR)
   /* validate message */
   if (ret != ROK  ||  mLen < 1)
   {
      SYLOGERROR_INT_PAR(ESY033, spId, 0,
         "HiUiHitDatReq(): invalid buffer");
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_MBUF;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_DATREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }
#endif


   /* find the connection control block */
   ret = hiFindConCb(sap, spConId, &conCb);
   if (ret != ROK)
   {
      SPutMsg(mBuf);
      HI_DISCIND(sap, HI_PROVIDER_CON_ID, spConId, HI_DATREQ_INVALID_CONID);
      RETVALUE(RFAILED);
   }


   /* must have sufficient resources to send this message */
#ifndef HI_NO_CHK_RES
   hiChkRes(sap);
   if (sap->resCongDrop  ||  conCb->flc)
   {
      /* Drop the message silently. In case of resource
       * congestion, an alarm would have already been issued. In
       * case of flow control, an indication would have already
       * been issued.
       */
      SPutMsg(mBuf);
      hiDoneWithConCb(conCb);
      RETVALUE(RFAILED);
   }
#else
   if (conCb->flc)
   {
      /* Drop the message silently. In case of resource
       * congestion, an alarm would have already been issued. In
       * case of flow control, an indication would have already
       * been issued.
       */
      SPutMsg(mBuf);
      hiDoneWithConCb(conCb);
      RETVALUE(RFAILED);
   }
#endif /* HI_NO_CHK_RES */


   /* check connection state */
   if (conCb->state != HI_ST_CONNECTED
       &&  conCb->state != HI_ST_CONNECTED_NORD)
   {
      SPutMsg(mBuf);
      alInfo.type = LHI_ALARMINFO_CON_STATE;
      alInfo.inf.conState = conCb->state;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_DATREQ,
                  LHI_CAUSE_INV_CON_STATE, &alInfo);
      hiDoneWithConCb(conCb);
      RETVALUE(RFAILED);
   }


#if (ERRCLASS & ERRCLS_INT_PAR)
   /* cannot be a UDP connection */
   if (conCb->flag & HI_FL_UDP)
   {
      SPutMsg(mBuf);
      SYLOGERROR_INT_PAR(ESY034, spId, 0,
         "HiUiHitDatReq(): invalid connection type for data request");
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_SRVC_TYPE;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_DATREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      hiDoneWithConCb(conCb);
      RETVALUE(RFAILED);
   }
#endif


   /* add in a TPKT header if required for this connection */
   if ((conCb->srvcType & 0x0F) == HI_SRVC_TCP_TPKT_HDR)
   {
      ret = hiAddTPKTHdr(mBuf, &mLen);

#if (ERRCLASS & ERRCLS_ADD_RES)
      if (ret != ROK)
      {
         SPutMsg(mBuf);
         HILOGERROR_ADD_RES(ESY035, spId, 0,
            "HiUiHitDatReq(): could not add TPKT header");
         hiSendAlarm(LCM_CATEGORY_RESOURCE, LCM_EVENT_DMEM_ALLOC_FAIL,
                     LCM_CAUSE_UNKNOWN, &alInfo);
         hiDoneWithConCb(conCb);
         RETVALUE(RFAILED);
      }
#endif
   }

   /* hi018.201 : qLen is initialized to zero */
   qLen = 0;
   /* If there is nothing in the transmit queue, send the message.
    * This could succeed partially, in which case we have to queue
    * the untransmitted portion. If there is something in the
    * transmit queue, we just queue the whole message.
    */
   HI_GET_TXQLEN(conCb, &qLen);
   if (qLen == 0)
      ret = hiTxMsg(conCb, mBuf, &txLen, &qBuf, &tMsg);
   else
   {
      ret = RWOULDBLOCK;
      qBuf = mBuf;
      txLen = 0;
   }


   /* Here, we have to enqueue the untransmitted part of the
    * message in qBuf (could be the whole message).
    */
   if (ret == RWOULDBLOCK)
   {
      ret = hiEnqueueForTx(conCb, qBuf);
      if (ret == ROK)
         /* hi006.105 : corrected the flow control check */
         hiChkFlc(conCb, 0, 0);
      else
      {
         SPutMsg(qBuf);
         alInfo.type = LHI_ALARMINFO_MEM_ID;
         alInfo.inf.mem.region = sap->uiPst.region;
         alInfo.inf.mem.pool = sap->uiPst.pool;
         hiSendAlarm(LCM_CATEGORY_RESOURCE, LCM_EVENT_DMEM_ALLOC_FAIL,
                     LCM_CAUSE_UNKNOWN, &alInfo);
         tMsg.type = HI_THR_DELCON_DISCIND;
         tMsg.spId = sap->spId;
         tMsg.spConId = conCb->spConId;
         tMsg.disc.reason = HI_OUTOF_RES;
      }
   }


   /* We have a failure here. The terminate message should have
    * been prepared, so we just send it to the group's thread.
    */
   if (ret != ROK  &&  ret != RWOULDBLOCK)
   {
      conCb->state = HI_ST_DISCONNECTING;
      hiSendThrMsg(conCb->fdGrpNum, &tMsg);
   }


   /* finished with the connection */
   hiDoneWithConCb(conCb);


#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T DatReq - End of HiUiHitDatReq, in HI");
#endif


   RETVALUE(ROK);
} /* HiUiHitDatReq() */


/*
*
*       Fun:    HiUiHitUDatReq
*
*       Desc:   Transmit data on a datagram connection.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  If an error occurs, no indication is given to the
*               service user, but an alarm is issued to the layer
*               manager.
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitUDatReq
(
Pst             *pst,           /* post structure */
SpId            spId,           /* service provider Id */
UConnId         spConId,        /* HI connection Id */
CmTptAddr       *remAddr,       /* remote address */
CmTptAddr       *srcAddr,       /* source address */
CmIpHdrParm     *hdrParm,       /* header parameters */
CmTptParam      *tPar,          /* transport parameters */
Buffer          *mBuf           /* message buffer to be sent */
)
#else
PUBLIC S16 HiUiHitUDatReq(pst, spId, spConId, remAddr, srcAddr,
                          hdrParm, tPar, mBuf)
Pst             *pst;           /* post structure */
SpId            spId;           /* service provider Id */
UConnId         spConId;        /* HI connection Id */
CmTptAddr       *remAddr;       /* remote address */
CmTptAddr       *srcAddr;       /* source address */
CmIpHdrParm     *hdrParm;       /* header parameters */
CmTptParam      *tPar;          /* transport parameters */
Buffer          *mBuf;          /* message buffer to be sent */
#endif
{
   S16          ret;
   HiSap        *sap;
   HiAlarmInfo  alInfo;
   MsgLen       mLen, txLen;
   CmInetFd     *sendFd;
   CmTptParam   lclTPar;
   HiConCb      *conCb, con;
   Bool         udpClt, resetTtl;
   HiThrMsg     tMsg;


   TRC3(HiUiHitUDatReq);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&syCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESY036,(ErrVal)0,pst->dstInst,
            "HiUiHitUDatReq() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifndef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */

#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T UDatReq HC->HI, in HI");
#endif


   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
          "HiUiHitUDatReq(pst, spId(%d), remAddr(%p), mBuf(%p))\n",
          spId, (Ptr)remAddr, (Ptr)mBuf));


   /* initialize locals */
   alInfo.spId  = spId;
   alInfo.type  = LHI_ALARMINFO_TYPE_NTPRSNT;

   conCb        = NULLP;
   udpClt       = FALSE;
   resetTtl     = FALSE;


   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_UDATREQ, alInfo, sap);


   /* check SAP state */
   HI_CHK_SAPSTATE(sap, LHI_EVENT_UDATREQ, alInfo);

   /* hi018.201 : mLen is initialized to zero */
   mLen = 0;
   /* get length of data payload */
   ret = SFndLenMsg(mBuf, &mLen);


#if (ERRCLASS & ERRCLS_INT_PAR)
   /* validate message */
   if (ret != ROK  ||  mLen < 1)
   {
      SYLOGERROR_INT_PAR(ESY037, spId, 0,
         "HiUiHitDatReq(): invalid Buffer");
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_MBUF;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_DATREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }

   /* verify the source address */
   if (!HI_CHK_ADDR(srcAddr)  &&  srcAddr->type != CM_TPTADDR_NOTPRSNT)
   {
      SPutMsg(mBuf);
      SYLOGERROR_INT_PAR(ESY038, 0, 0,
         "HiUiHitUDatReq(): invalid source address");
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_TPT_ADDR;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }

   /* check header parameters */
   if (!HI_CHK_HDRPARM(hdrParm)
       &&  hdrParm->type != CM_HDRPARM_NOTPRSNT)
   {
      SPutMsg(mBuf);
      SYLOGERROR_INT_PAR(ESY039, 0, 0,
         "HiUiHitUDatReq(): invalid protocol type in header parameters");
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_TPT_ADDR;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }

   /* check IPv4 options */
   if (hdrParm->type == CM_HDRPARM_IPV4)
   {
#ifdef IPV4_OPTS_SUPPORTED
      if (hdrParm->u.hdrParmIpv4.ipv4HdrOpt.pres
          &&  hdrParm->u.hdrParmIpv4.ipv4HdrOpt.len > CM_IPV4_OPTS_MAXLEN)
      {
         SPutMsg(mBuf);
         SYLOGERROR_INT_PAR(ESY040, hdrParm->u.hdrParmIpv4.ipv4HdrOpt.len, 0,
            "HiUiHitUDatReq(): invalid IPv4 options length");
         alInfo.type = LHI_ALARMINFO_PAR_TYPE;
         alInfo.inf.parType = LHI_INV_HDR_PARAM;
         hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
                     LCM_CAUSE_INV_PAR_VAL, &alInfo);
         RETVALUE(RFAILED);
      }
#endif
   }
#endif /* ERRCLS_INT_PAR */


   /* must have sufficient resources to send this message */
#ifndef HI_NO_CHK_RES
   hiChkRes(sap);
   if (sap->resCongDrop)
   {
      /* Drop the message silently. An alarm has already been
       * issued for resource congestion.
       */
      SPutMsg(mBuf);
      RETVALUE(RFAILED);
   }
#endif /* HI_NO_CHK_RES */


   /* If spConId is zero, we use the generic socket to send this
    * message.
    */
   sendFd = NULLP;

   if (spConId == 0)
/* hi002.105 (hi030.104) - If Generic Socket Supported */
#ifndef HI_DISABLE_GENSOCKET
   {
#ifdef IPV6_SUPPORTED
      if (remAddr->type == CM_INET_IPV6ADDR_TYPE)
         sendFd = &syCb.resv6ConFd;

      else /* assignment below */
#endif

      sendFd = &syCb.resvConFd;

      /* Copy the fd into a temporary connection block for calling
       * hiSetSockOpt().
       */
      cmMemcpy((U8 *)&con.conFd, (U8 *)sendFd, sizeof (CmInetFd));
   }
#else
   {
      if(mBuf)
         SPutMsg(mBuf);
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
               LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }
#endif /*HI_DISABLE_GENSOCKET */

   /* spConId has been specified. Find the correct connection to
    * use for this transmission.
    */
   else
   {
      ret = hiFindConCb(sap, spConId, &conCb);
      if (ret != ROK)
      {
         SPutMsg(mBuf);
         hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
                     LCM_CAUSE_INV_PAR_VAL, &alInfo);
         RETVALUE(RFAILED);
      }


      /* check connection state */
      if (conCb->state != HI_ST_CONNECTED
          &&  conCb->state != HI_ST_CONNECTED_NORD)
      {
         SPutMsg(mBuf);
         alInfo.type = LHI_ALARMINFO_CON_STATE;
         alInfo.inf.conState = conCb->state;
         hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
                     LHI_CAUSE_INV_CON_STATE, &alInfo);
         hiDoneWithConCb(conCb);
         RETVALUE(RFAILED);
      }


      /* get the fd to use */
      sendFd = &conCb->conFd;


      /* is this a true UDP client? */
      if (conCb->flag & HI_FL_UDP_CLT)
         udpClt = TRUE;


#if (ERRCLASS & ERRCLS_INT_PAR)
      /* validate header parameter type against connection type */
#ifdef IPV6_SUPPORTED
      if ((hdrParm->type == CM_HDRPARM_IPV4
           ||  hdrParm->type == CM_HDRPARM_IPV6)
          &&  conCb->flag & HI_FL_TCP)
#else
      if (hdrParm->type == CM_HDRPARM_IPV4
          &&  !(conCb->flag & HI_FL_RAW))
#endif
      {
         SPutMsg(mBuf);
         SYLOGERROR_INT_PAR(ESY041, (ErrVal)hdrParm->type, 0,
            "HiUiHitUDatReq(): invalid header parameter/protocol combination");
         alInfo.type = LHI_ALARMINFO_PAR_TYPE;
         alInfo.inf.parType = LHI_INV_TPT_ADDR;
         hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
                     LCM_CAUSE_INV_PAR_VAL, &alInfo);
         hiDoneWithConCb(conCb);
         RETVALUE(RFAILED);
      }
#endif /* ERRCLS_INT_PAR */


      /* add in a TPKT header if required for this connection */
      if ((conCb->srvcType & 0x0F) == HI_SRVC_UDP_TPKT_HDR)
      {
         ret = hiAddTPKTHdr(mBuf, &mLen);

#if (ERRCLASS & ERRCLS_ADD_RES)
         if (ret != ROK)
         {
            SPutMsg(mBuf);
            HILOGERROR_ADD_RES(ESY042, spId, 0,
               "HiUiHitUDatReq(): could not add TPKT header");
            hiSendAlarm(LCM_CATEGORY_RESOURCE, LCM_EVENT_DMEM_ALLOC_FAIL,
                        LCM_CAUSE_UNKNOWN, &alInfo);
            hiDoneWithConCb(conCb);
            RETVALUE(RFAILED);
         }
#endif
      }

      /* now, process the header parameters */
      ret = hiProcHdrParm(conCb, srcAddr, remAddr, hdrParm, mBuf, mLen);
      if (ret != ROK)
      {
         SPutMsg(mBuf);
         HILOGERROR_ADD_RES(ESY043, spId, 0,
            "HiUiHitUDatReq(): could not process header parameters");
         hiSendAlarm(LCM_CATEGORY_RESOURCE, LHI_EVENT_UDATREQ,
                     LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
         hiDoneWithConCb(conCb);
         RETVALUE(RFAILED);
      }
   }


   /* Handle multicast TTL or hop count */
   if (tPar->type == CM_TPTPARAM_SOCK
       &&  tPar->u.sockParam.numOpts > 0
       &&  HI_CHK_MCASTOPT(tPar->u.sockParam.sockOpts[0].option))
   {
      if (conCb)
      {
         if (conCb->mCastTtl != tPar->u.sockParam.sockOpts[0].optVal.value)
         {
            ret = hiSetSockOpt(conCb, tPar);
            if (ret == RNA  ||  ret == RFAILED)
            {
               SPutMsg(mBuf);
               hiSendAlarm(LCM_CATEGORY_RESOURCE, LHI_EVENT_UDATREQ,
                           LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
               hiDoneWithConCb(conCb);
               RETVALUE(RFAILED);
            }

            conCb->mCastTtl = tPar->u.sockParam.sockOpts[0].optVal.value;
         }
      }
      else
      {
         resetTtl = TRUE;
         lclTPar.u.sockParam.numOpts = 1;
         lclTPar.u.sockParam.sockOpts[0].option
            = tPar->u.sockParam.sockOpts[0].option;
         lclTPar.u.sockParam.sockOpts[0].level
            = tPar->u.sockParam.sockOpts[0].level;
         lclTPar.u.sockParam.sockOpts[0].optVal.value = 1;

         ret = hiSetSockOpt(&con, tPar);
         if (ret == RNA  ||  ret == RFAILED)
         {
            SPutMsg(mBuf);
            hiSendAlarm(LCM_CATEGORY_RESOURCE, LHI_EVENT_UDATREQ,
                        LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
            RETVALUE(RFAILED);
         }
      }
   }


#ifdef IPV6_OPTS_SUPPORTED
   /* Copy the source address to hdrParm so that it is available
    * in cmInetSendMsg() to set on the outgoing packet.
    */
   if (srcAddr->type == CM_TPTADDR_IPV6)
   {
      hdrParm->u.hdrParmIpv6.srcAddr6.type = srcAddr->type;
      cmMemcpy((U8 *)&hdrParm->u.hdrParmIpv6.srcAddr6.u.ipv6NetAddr,
               (U8 *)&srcAddr->u.ipv6TptAddr.ipv6NetAddr, 16);
   }
#endif


#ifdef H323_PERF
   TAKE_TIMESTAMP("Before UDatReq/cmInetSendMsg(), in HI");
#endif


   /* Send the message. If the UDP socket is already connected
    * then do not pass a remote address.
    */
   HI_SENDMSG(sendFd, (udpClt ? NULLP : remAddr), mBuf,
              &txLen, (udpClt ? NULLP : hdrParm), ret);


#ifdef IPV6_OPTS_SUPPORTED
   /* free IPv6 header parameters */
   /* hi001.105 - Fixed compiler warning */
   if (hdrParm->type == CM_HDRPARM_IPV6)
      CM_INET_FREE_IPV6_HDRPARM(syCb.init.region, syCb.init.pool,
         ((CmInetIpv6HdrParm*)&(hdrParm->u.hdrParmIpv6)));
#endif


#ifdef H323_PERF
   TAKE_TIMESTAMP("After UDatReq/cmInetSendMsg(), in HI");
#endif


   /* check for send errors */
   if (ret != ROK)
   {
      SPutMsg(mBuf);

      tMsg.type = HI_THR_DELCON_DISCIND;
      tMsg.spId = sap->spId;

      /* hi011.201 handling NETWORK failure sending error */
      /* no disconnect for ROUTRES , RWOULDBLOCK and RNETFAILED */
      if (ret == ROUTRES  ||  ret == RWOULDBLOCK || ret == RNETFAILED)
      {
         alInfo.type = LHI_ALARMINFO_MEM_ID;
         alInfo.inf.mem.region = syCb.init.region;
         alInfo.inf.mem.pool = syCb.init.pool;
			/* hi005.201  Free the lock in case of error */
         hiDoneWithConCb(conCb);
         hiSendAlarm(LCM_CATEGORY_RESOURCE, LCM_EVENT_DMEM_ALLOC_FAIL,
                     LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
         RETVALUE(RFAILED);
      }

      /* hi005.201 Don't close the non connected UDP in case of Sending error.
		             Insted give alarm Indication to Layer manager and return
		 *           RFAILED.
		 */
      else if (!udpClt)
		{
          HI_INC_ERRSTS(syCb.errSts.sockTxErr);
          hiDoneWithConCb(conCb);
			 hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_INET_ERR,
			             LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
			 RETVALUE(RFAILED);
      }

      /* We get RCLOSED when an ICMP message is received on a UDP
       * socket. This will only happen with a connected socket,
       * where spConId is valid. We disconnect it.
       */
      else if (ret == RCLOSED)
         tMsg.disc.reason = HI_CON_CLOSED_BY_PEER;

      /* all other errors get an alarm and are disconnected */
      else
      {
         HI_INC_ERRSTS(syCb.errSts.sockTxErr);
         hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_INET_ERR,
                     LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
         tMsg.disc.reason = HI_SOCK_SEND_ERR;
      }

      /* send the delete connection message to the group thread */
      if (conCb)
      {
		   /* hi005.201 */
         tMsg.spConId = conCb->spConId;
         conCb->state = HI_ST_DISCONNECTING;
         hiDoneWithConCb(conCb);
         hiSendThrMsg(conCb->fdGrpNum, &tMsg);
      }


      RETVALUE(RFAILED);
   }


   /* reset IP multicast TTL if necessary */
   if (resetTtl)
      hiSetSockOpt(&con, &lclTPar);


   /* update statistics and we're done with the connection */
   HI_ADD_TXSTS(sap, sap->txSts.numTxBytes, txLen);
   if (conCb)
   {
      HI_INC_TXSTSMSG(sap, conCb);
      hiDoneWithConCb(conCb);
   }


   /* trace the message if needed and release it; we're done! */
   if (sap->trc)
      hiTrcBuf(sap, LHI_UDP_TXED, mBuf);
   SPutMsg(mBuf);


#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T DatReq - End of HiUiHitUDatReq, in HC");
#endif


   RETVALUE(ROK);
} /* HiUiHitUDatReq() */


/*
*
*       Fun:    HiUiHitDiscReq
*
*       Desc:   Shut down or close the socket identified by conId.
*               TUCL responds with a disconnect confirm on
*               success. This primitive is also used to leave
*               membership of a multicast group for UDP sockets.
*               The action parameter specifies whether the socket
*               should be closed, shut down, or if this is a
*               request to leave a multicast group.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  The connection block is released only if the
*               action parameter indicates that the socket should
*               be closed. If the connection block cannot be
*               found, TUCL returns silently (a disconnect
*               indication may have already been sent).
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitDiscReq
(
Pst             *pst,           /* post structure */
SpId            spId,           /* service provider Id */
U8              choice,         /* choice of user or provider Id */
UConnId         conId,          /* connection Id */
Action          action,         /* action to be performed */
CmTptParam      *tPar           /* transport parameters */
)
#else
PUBLIC S16 HiUiHitDiscReq(pst, spId, choice, conId, action, tPar)
Pst             *pst;           /* post structure */
SpId            spId;           /* service provider Id */
U8              choice;         /* choice of user or provider Id */
UConnId         conId;          /* connection Id */
Action          action;         /* action to be performed */
CmTptParam      *tPar;          /* transport parameters */
#endif
{
   S16          ret;
   HiSap        *sap;
   HiConCb      *conCb;
   HiAlarmInfo  alInfo;
   HiThrMsg     tMsg;


   TRC3(HiUiHitDiscReq);

/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&syCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt, ESY044,(ErrVal)0,pst->dstInst,
            "HiUiHitDiscReq() failed, cannot derive syCb");
      RETVALUE(FALSE);
   }
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifdef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */

   /* hi029.201:Fix for compilation warning*/
#ifndef BIT_64
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
          "HiUiHitDiscReq(pst, spId(%d), choice(%d), conId(%ld), \
          action(%d))\n", spId, choice, conId, action));
#else
   SYDBGP(DBGMASK_UI, (syCb.init.prntBuf,
          "HiUiHitDiscReq(pst, spId(%d), choice(%d), conId(%d), \
          action(%d))\n", spId, choice, conId, action));
#endif


   /* initialize alarm information */
   alInfo.spId = spId;
   alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;


   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_DISCREQ, alInfo, sap);


   /* check SAP state */
   HI_CHK_SAPSTATE(sap, LHI_EVENT_DISCREQ, alInfo);


#if (ERRCLASS & ERRCLS_INT_PAR)
   if (action == HI_LEAVE_MCAST_GROUP)
   {
      if (tPar->type != CM_TPTPARAM_SOCK
          ||  !HI_CHK_MCASTDRPOPT(tPar->u.sockParam.sockOpts[0].option))
      {
         SYLOGERROR_INT_PAR(ESY045, 0, 0,
            "HiUiHitDiscReq(): invalid transport parameters");
         HI_DISCCFM(sap, choice, conId, action);
         alInfo.type = LHI_ALARMINFO_PAR_TYPE;
         alInfo.inf.parType = LHI_INV_TPT_PARAM;
         hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_DISCREQ,
                     LCM_CAUSE_INV_PAR_VAL, &alInfo);
         RETVALUE(RFAILED);
      }
   }
#endif


   /* find the connection */
   if (choice == HI_PROVIDER_CON_ID)
      ret = hiFindConCb(sap, conId, &conCb);
   else
      ret = hiFindConCbSuConId(sap, conId, &conCb);

   if (ret != ROK)
   {
      HI_DISCCFM(sap, choice, conId, action);
      RETVALUE(ROK);
   }


   /* check connection state */
   if (conCb->state == HI_ST_DISCONNECTING)
   {
      HI_DISCCFM(sap, choice, conId, action);

      /** hi016.105  1. Given a check for NULLP before
       *                accessing conCb to avoid FMR/W
       */
		/* hi005.201  1. If conCb is not NULL then
		 *               free the conCb lock */
      if(conCb)
         hiDoneWithConCb(conCb);

      RETVALUE(ROK);
   }




   /* We deal with multicast group leaves here; other actions are
    * handled by the group thread.
    */
   if (action == HI_LEAVE_MCAST_GROUP)
   {
      ret = hiSetSockOpt(conCb, tPar);
      if (ret != ROK)
      {
         hiSendAlarm(LCM_CATEGORY_RESOURCE, LHI_EVENT_DISCREQ,
                     LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
         hiDoneWithConCb(conCb);
         RETVALUE(RFAILED);
      }

      /* issue disconnect confirm */
      HI_DISCCFM(sap, choice, conId, action);

      /* done with the connection */
      hiDoneWithConCb(conCb);

      RETVALUE(ROK);
   }


   /* set the connection to disconnecting state */
   conCb->state = HI_ST_DISCONNECTING;

   /* prepare and send a message to the group thread */
   tMsg.type        = HI_THR_DELCON_DISCCFM;
   tMsg.spId        = sap->spId;
   tMsg.spConId     = conCb->spConId;
   tMsg.disc.action = action;
   hiSendThrMsg(conCb->fdGrpNum, &tMsg);


   /* done with the connection */
   hiDoneWithConCb(conCb);


   RETVALUE(ROK);
} /* HiUiHitDiscReq() */

#endif // #if 0

/*
*
*       Fun:    syCfgGen
*
*       Desc:   Perform general configuration of Diameter. Must be done
*               after syActvInit() is called, but before any other
*               interaction with Diameter. Reserves static memory for
*               the layer with SGetSMem(), allocates required
*               memory, sets up data structures and starts the
*               receive mechanism (threads/timer/permanent task).
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*               LSY_REASON_FD_CORE_INITIALIZE_FAIL  - failure
*               LSY_REASON_FD_CORE_PARSECONF_FAIL   - failure
*               LSY_REASON_FD_CORE_START_FAIL       - failure
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PRIVATE S16 syCfgGen
(
SyGenCfg        *syGen          /* management structure */
)
#else
PRIVATE S16 syCfgGen(syGen)
SyGenCfg        *syGen;         /* management structure */
#endif
{
   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    sySendLmCfm
*
*       Desc:   Sends configuration, control, statistics and status
*               confirms to the layer manager.
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   hi_bdy2.c
*
*/
#ifdef ANSI
PUBLIC Void sySendLmCfm
(
Pst             *pst,           /* post */
U8              cfmType,        /* confirm type */
Header          *hdr,           /* header */
U16             status,         /* confirm status */
U16             reason,         /* failure reason */
SyMngmt         *cfm            /* management structure */
)
#else
PUBLIC Void sySendLmCfm(pst, cfmType, hdr, status, reason, cfm)
Pst             *pst;           /* post */
U8              cfmType;        /* confirm type */
Header          *hdr;           /* header */
U16             status;         /* confirm status */
U16             reason;         /* failure reason */
SyMngmt         *cfm;           /* management structure */
#endif
{
   Pst          cfmPst;         /* post structure for confimation */

   TRC2(sySendLmCfm);

   SY_ZERO(&cfmPst, sizeof (Pst));

   cfm->hdr.elmId.elmnt = hdr->elmId.elmnt;
   cfm->hdr.transId     = hdr->transId;

   cfm->cfm.status = status;
   cfm->cfm.reason = reason;

   /* fill up post for confirm */
   cfmPst.srcEnt        = syCb.init.ent;
   cfmPst.srcInst       = syCb.init.inst;
   cfmPst.srcProcId     = syCb.init.procId;
   cfmPst.dstEnt        = pst->srcEnt;
   cfmPst.dstInst       = pst->srcInst;
   cfmPst.dstProcId     = pst->srcProcId;
   cfmPst.selector      = hdr->response.selector;
   cfmPst.prior         = hdr->response.prior;
   cfmPst.route         = hdr->response.route;
   cfmPst.region        = hdr->response.mem.region;
   cfmPst.pool          = hdr->response.mem.pool;

   switch (cfmType)
   {
      case TCFG:
         SyMiLsyCfgCfm(&cfmPst, cfm);
         break;

      case TSTS:
         SyMiLsyStsCfm(&cfmPst, cfm);
         break;

      case TCNTRL:
         SyMiLsyCntrlCfm(&cfmPst, cfm);
         break;

      case TSSTA:
         SyMiLsyStaCfm(&cfmPst, cfm);
         break;

      default:
         SYDBGP(DBGMASK_LYR, (syCb.init.prntBuf,
                  "sySendLmCfm(): unknown parameter cfmType\n"));
         break;
   }

   RETVOID;
} /* sySendLmCfm() */

/*
*
*       Fun:    syGetGenSts
*
*       Desc:   Get general statistics.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  Statistics counters are sampled unlocked.
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 syGetGenSts
(
SyGenSts        *genSts         /* statistics returned */
)
#else
PUBLIC S16 syGetGenSts(genSts)
SyGenSts        *genSts;        /* statistics returned */
#endif
{
   TRC2(syGetGenSts);

   SY_ZERO(genSts, sizeof (SyGenSts));

   /* get receive counters from each group */
   genSts->numErrors = syCb.errSts.s6aErr;

   RETVALUE(ROK);
} /* hsyGetGenSts() */


/*
*
*       Fun:    syZeroGenSts
*
*       Desc:   Reset general statistics.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 syZeroGenSts
(
Void
)
#else
PUBLIC S16 syZeroGenSts()
#endif
{
   TRC2(syZeroGenSts);

   /* zero the error stats */
   SY_ZERO_ERRSTS();

   RETVALUE(ROK);
} /* syZeroGenSts() */

/*
*
*       Fun:    syCntrlGen
*
*       Desc:   General control requests are used to enable/disable
*               alarms or debug printing and also to shut down
*               Diameter.
*
*       Ret:    LCM_REASON_NOT_APPL             - ok
*               LSY_REASON_OPINPROG             - ok, in progress
*               LCM_REASON_INVALID_ACTION       - failure
*               LCM_REASON_INVALID_SUBACTION    - failure
*               LSY_REASON_DIFF_OPINPROG        - failure
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC S16 syCntrlGen
(
Pst             *pst,           /* post structure */
SyMngmt         *cntrl,         /* control request */
Header          *hdr            /* header */
)
#else
PUBLIC S16 syCntrlGen(pst, cntrl, hdr)
Pst             *pst;           /* post structure */
SyMngmt         *cntrl;         /* control request */
Header          *hdr;           /* header */
#endif
{
   S16          ret;
   U8           action, subAction;
   Bool         invSubAction = FALSE;

   TRC2(syCntrlGen);

   action = cntrl->t.cntrl.action;
   subAction = cntrl->t.cntrl.subAction;

   switch (action)
   {
      case ASHUTDOWN:
         /* hi003.105 to check for gen cfg */
         if(syCb.init.cfgDone != TRUE)
            RETVALUE(LCM_REASON_NOT_APPL);
         if (syCb.pendOp.flag)
            RETVALUE(LSY_REASON_DIFF_OPINPROG);

#ifdef SY_MULTI_THREADED
         SY_LOCK(&syCb.pendLock);
#endif

         /* store the request details */
         syCb.pendOp.flag = FALSE;
         syCb.pendOp.action = action;
         cmMemcpy((U8 *)&syCb.pendOp.lmPst, (U8 *)pst, sizeof (Pst));
         cmMemcpy((U8 *)&syCb.pendOp.hdr, (U8 *)hdr, sizeof (Header));
         syCb.pendOp.numRem = 0;

#ifdef SY_MULTI_THREADED
         SY_UNLOCK(&syCb.pendLock);
#endif

         ret = LSY_REASON_OPINPROG;
         RETVALUE(ret);


      case AENA:
         if (subAction == SAUSTA)
            syCb.init.usta = TRUE;
#ifdef DEBUGP
         else if (subAction == SADBG)
            syCb.init.dbgMask |= cntrl->t.cntrl.ctlType.syDbg.dbgMask;
#endif
         else
            invSubAction = TRUE;
         break;


      case ADISIMM:
         if (subAction == SAUSTA)
            syCb.init.usta = FALSE;
#ifdef DEBUGP
         else if (subAction == SADBG)
            syCb.init.dbgMask &= ~(cntrl->t.cntrl.ctlType.syDbg.dbgMask);
#endif
         else
            invSubAction = TRUE;
         break;


      default:
         SYLOGERROR_INT_PAR(ESY073, (ErrVal)action, 0,
               "syCntrlGen(): Unknown or unsupported action");
         RETVALUE(LCM_REASON_INVALID_ACTION);
   }

   if (invSubAction)
   {
      SYLOGERROR_INT_PAR(ESY074, (ErrVal)subAction, 0,
            "syCntrlGen(): invalid sub-action specified");
      RETVALUE(LCM_REASON_INVALID_SUBACTION);
   }

   RETVALUE(LCM_REASON_NOT_APPL);
} /* syCntrlGen() */

/*
*
*       Fun:   syGetSid
*
*       Desc:  Get system id consisting of part number, main version and
*              revision and branch version and branch.
*
*       Ret:   TRUE      - ok
*
*       Notes: None
*
*       File:  sy.c
*
*/
#ifdef ANSI
PUBLIC S16 syGetSid
(
SystemId *s                 /* system id */
)
#else
PUBLIC S16 syGetSid(s)
SystemId *s;                /* system id */
#endif
{
   TRC2(syGetSid)

   s->mVer = sId.mVer;
   s->mRev = sId.mRev;
   s->bVer = sId.bVer;
   s->bRev = sId.bRev;
   s->ptNmb = sId.ptNmb;

   RETVALUE(TRUE);

} /* syGetSid */

/*
*
*       Fun:    sySendAlarm
*
*       Desc:   Send an unsolicited status indication (alarm) to
*               layer manager.
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   sy.c
*
*/
#ifdef ANSI
PUBLIC Void sySendAlarm
(
U16             cgy,            /* alarm category */
U16             evnt,           /* event */
U16             cause,          /* cause for alarm */
SyAlarmInfo     *info           /* alarm information */
)
#else
PUBLIC Void sySendAlarm(cgy, evnt, cause, info)
U16             cgy;            /* alarm category */
U16             evnt;           /* event */
U16             cause;          /* cause for alarm */
SyAlarmInfo     *info;          /* alarm information */
#endif
{
   SyMngmt      sm;             /* Management structure */


   TRC2(sySendAlarm);


   /* do nothing if unconfigured */
   if (!syCb.init.cfgDone)
   {
      SYDBGP(DBGMASK_LYR, (syCb.init.prntBuf,
               "sySendAlarm(): general configuration not done\n"));
      RETVOID;
   }


   /* send alarms only if configured to do so */
   if (syCb.init.usta)
   {
      SY_ZERO(&sm, sizeof (SyMngmt));

      sm.hdr.elmId.elmnt        = TUSTA;
      sm.hdr.elmId.elmntInst1   = SY_UNUSED;
      sm.hdr.elmId.elmntInst2   = SY_UNUSED;
      sm.hdr.elmId.elmntInst3   = SY_UNUSED;

      sm.t.usta.alarm.category  = cgy;
      sm.t.usta.alarm.event     = evnt;
      sm.t.usta.alarm.cause     = cause;

      cmMemcpy((U8 *)&sm.t.usta.info,
               (U8 *)info, sizeof (SyAlarmInfo));

      (Void)SGetDateTime(&sm.t.usta.alarm.dt);

#ifdef SY_MULTI_THREADED
   SY_LOCK(&syCb.lmPstLock);
#endif
      SyMiLsyStaInd(&(syCb.init.lmPst), &sm);
#ifdef SY_MULTI_THREADED
   SY_UNLOCK(&syCb.lmPstLock);
#endif
   }

   RETVOID;
} /* sySendAlarm() */

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/
