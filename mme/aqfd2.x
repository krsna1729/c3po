/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


#ifndef __AQFD2X__
#define __AQFD2X__

typedef S32 AqEnum;

typedef struct aqOctetString {
   U32 len;
   U8 val[0];
} AqOctetString;

typedef struct aqAddress {
   U16 type;
   U8 address[16];
} AqAddress;

typedef struct aqExperimentalResult {
   Bool present;
   U32 vendor_id;
   U32 result_code;
} AqExperimentalResult;


#endif /* __AQFD2X__ */
