/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter

     Type:     C include file

     Desc:     Diameter

     File:     lsy.h

     Sid:

     Prg:

*********************************************************************21*/

#ifndef __LSYH__
#define __LSYH__

#ifdef LSYV1            /* LSY interface version 1 */
#ifdef LSYIFVER
#undef LSYIFVER
#endif
#define LSYIFVER        0x0100
#endif

/***********************************************************************
         defines for layer-specific elements
 ***********************************************************************/

#define LSY_PATHLEN      255

/***********************************************************************
         defines for "reason" in LsyStaInd
 ***********************************************************************/
#define  LSY_REASON_OPINPROG                 (LCM_REASON_LYR_SPECIFIC + 1)
#define  LSY_REASON_DIFF_OPINPROG            (LCM_REASON_LYR_SPECIFIC + 2)

/***********************************************************************
         defines for "event" in LsyStaInd
 ***********************************************************************/
/*
#define  LSY_EVENT_???             (LCM_EVENT_LYR_SPECIFIC + 1)
*/

/***********************************************************************
         defines for "cause" in LsyStaInd
 ***********************************************************************/
/* diameter  related errors */

#define  LSY_CAUSE_LOCK_ERR             (LCM_CAUSE_LYR_SPECIFIC + 1)
#define  LSY_CAUSE_UNLOCK_ERR           (LCM_CAUSE_LYR_SPECIFIC + 2)

/***********************************************************************
         defines related to events across the management interface
 ***********************************************************************/

#define  EVTLSYCFGREQ                   1
#define  EVTLSYSTSREQ                   2
#define  EVTLSYCNTRLREQ                 3
#define  EVTLSYSTAREQ                   4
#define  EVTLSYCFGCFM                   5
#define  EVTLSYSTSCFM                   6
#define  EVTLSYCNTRLCFM                 7
#define  EVTLSYSTACFM                   8
#define  EVTLSYSTAIND                   9
#define  EVTLSYTRCIND                   10

#define EVTLSYAIA                       11
#define EVTLSYULA_ATTACH_REQUEST        12
#define EVTLSYPUA                       13
#define EVTLSYIDR                       14

/********************************************************************SZ**
 SY States
*********************************************************************SZ*/

#define LSY_UNINITIALIZED         1     /* uninitizlied */
#define LSY_INITIALIZED           1     /* initizlied */
#define LSY_CFG_PARSED            1     /* config file parsed */
#define LSY_STARTED               1     /* started */
#define LSY_WAITING_FOR_SHUTDOWN  1     /* shutdown issued */
#define LSY_SHUTDOWN              1     /* shutdown */


/***********************************************************************
         defines related to events in LsyTrcInd primitive
 ***********************************************************************/
#define  LSY_TCP_TXED                   0
#define  LSY_UDP_TXED                   1
#define  LSY_TCP_RXED                   2
#define  LSY_UDP_RXED                   3
#define  LSY_RAW_TXED                   4
#define  LSY_RAW_RXED                   5

/* alarmInfo.type */
#define  LSY_ALARMINFO_TYPE_NTPRSNT     0       /* alarmInfo is not present */
#define  LSY_ALARMINFO_SAP_STATE        1       /* SAP state */
#define  LSY_ALARMINFO_CON_STATE        2       /* connection state */
#define  LSY_ALARMINFO_MEM_ID           3       /* memory id */
#define  LSY_ALARMINFO_PAR_TYPE         4       /* parameter type */
#define  LSY_ALARMINFO_TYPE_INFVER      5       /* invalid interface version */

/* parType values in LhiStaInd */
#define  LSY_INV_MBUF                   1       /* invalid message buffer */
#define  LSY_INV_SRVC_TYPE              2       /* invalid service type */
#define  LSY_INV_TPT_ADDR               3       /* invalid transport address */
#define  LSY_INV_TPT_PARAM              4       /* invalid transport params */
#define  LSY_INV_ACTION                 5       /* invalid action */
#define  LSY_INV_FILTER_TYPE_COMB       6       /* invalid filter/type combo */

/* selector values for lmPst.selector */
#define  LSY_LC                         0       /* loosely coupled LM */
#define  LSY_TC                         1       /* tightly coupled LM*/

/* sap.state */
#define  SY_ST_UBND                     0x1     /* after SAP configuration */
#define  SY_ST_BND                      0x2     /* after binding */

/* hash defines for flag field in HiHdrinfo */
#define  LHI_LEN_INCL_HDR               0x1     /* header length includes
                                                   PDU and header */

#define LSY_DBGMASK_INFO    (DBGMASK_LYR << 3)

/* Error macro for TUCL management interface */
/* lhi_h_001.main_11: Fix for compilation warning */
#define LSYLOGERROR(pst, errCode, errVal, errDesc)            \
        SLogError(pst->srcEnt, pst->srcInst, pst->srcProcId,  \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,   \
                  (ErrCode)errCode, (ErrVal)errVal,           \
                  errDesc)

#define LSY_ZERO(_str,_len)                                   \
   cmMemset((U8 *)_str, 0, _len);

/* Error codes */
#define   ELSYBASE     0             /* reserved */
#define   ERRLSY       (ELSYBASE)

#define   ELSY001      (ERRLSY +    1)    /*        lhi.c: 192 */
#define   ELSY002      (ERRLSY +    2)    /*        lhi.c: 195 */
#define   ELSY003      (ERRLSY +    3)    /*        lhi.c: 198 */
#define   ELSY004      (ERRLSY +    4)    /*        lhi.c: 199 */
#define   ELSY005      (ERRLSY +    5)    /*        lhi.c: 200 */
#define   ELSY006      (ERRLSY +    6)    /*        lhi.c: 201 */
#define   ELSY007      (ERRLSY +    7)    /*        lhi.c: 202 */
#define   ELSY008      (ERRLSY +    8)    /*        lhi.c: 203 */
#define   ELSY009      (ERRLSY +    9)    /*        lhi.c: 204 */
#define   ELSY010      (ERRLSY +   10)    /*        lhi.c: 205 */
#define   ELSY011      (ERRLSY +   11)    /*        lhi.c: 206 */
#define   ELSY012      (ERRLSY +   12)    /*        lhi.c: 207 */
#define   ELSY013      (ERRLSY +   13)    /*        lhi.c: 209 */
#define   ELSY014      (ERRLSY +   14)    /*        lhi.c: 210 */
#define   ELSY015      (ERRLSY +   15)    /*        lhi.c: 211 */
#define   ELSY016      (ERRLSY +   16)    /*        lhi.c: 212 */
#define   ELSY017      (ERRLSY +   17)    /*        lhi.c: 214 */
#define   ELSY018      (ERRLSY +   18)    /*        lhi.c: 215 */
#define   ELSY019      (ERRLSY +   19)    /*        lhi.c: 216 */
#define   ELSY020      (ERRLSY +   20)    /*        lhi.c: 217 */
#define   ELSY021      (ERRLSY +   21)    /*        lhi.c: 218 */
#define   ELSY022      (ERRLSY +   22)    /*        lhi.c: 221 */
#define   ELSY023      (ERRLSY +   23)    /*        lhi.c: 222 */
#define   ELSY024      (ERRLSY +   24)    /*        lhi.c: 227 */
#define   ELSY025      (ERRLSY +   25)    /*        lhi.c: 234 */
#define   ELSY026      (ERRLSY +   26)    /*        lhi.c: 237 */
#define   ELSY027      (ERRLSY +   27)    /*        lhi.c: 247 */
#define   ELSY028      (ERRLSY +   28)    /*        lhi.c: 248 */
#define   ELSY029      (ERRLSY +   29)    /*        lhi.c: 253 */
#define   ELSY030      (ERRLSY +   30)    /*        lhi.c: 254 */
#define   ELSY031      (ERRLSY +   31)    /*        lhi.c: 255 */
#define   ELSY032      (ERRLSY +   32)    /*        lhi.c: 256 */
#define   ELSY033      (ERRLSY +   33)    /*        lhi.c: 259 */
#define   ELSY034      (ERRLSY +   34)    /*        lhi.c: 260 */
#define   ELSY035      (ERRLSY +   35)    /*        lhi.c: 261 */
#define   ELSY036      (ERRLSY +   36)    /*        lhi.c: 262 */
#define   ELSY037      (ERRLSY +   37)    /*        lhi.c: 264 */
#define   ELSY038      (ERRLSY +   38)    /*        lhi.c: 265 */
#define   ELSY039      (ERRLSY +   39)    /*        lhi.c: 266 */
#define   ELSY040      (ERRLSY +   40)    /*        lhi.c: 267 */
#define   ELSY041      (ERRLSY +   41)    /*        lhi.c: 268 */
#define   ELSY042      (ERRLSY +   42)    /*        lhi.c: 269 */
#define   ELSY043      (ERRLSY +   43)    /*        lhi.c: 270 */
#define   ELSY044      (ERRLSY +   44)    /*        lhi.c: 271 */
#define   ELSY045      (ERRLSY +   45)    /*        lhi.c: 273 */
#define   ELSY046      (ERRLSY +   46)    /*        lhi.c: 274 */
#define   ELSY047      (ERRLSY +   47)    /*        lhi.c: 275 */
#define   ELSY048      (ERRLSY +   48)    /*        lhi.c: 276 */
#define   ELSY049      (ERRLSY +   49)    /*        lhi.c: 277 */
#define   ELSY050      (ERRLSY +   50)    /*        lhi.c: 278 */
#define   ELSY051      (ERRLSY +   51)    /*        lhi.c: 288 */
#define   ELSY052      (ERRLSY +   52)    /*        lhi.c: 290 */
#define   ELSY053      (ERRLSY +   53)    /*        lhi.c: 292 */
#define   ELSY054      (ERRLSY +   54)    /*        lhi.c: 294 */
#define   ELSY055      (ERRLSY +   55)    /*        lhi.c: 296 */
#define   ELSY056      (ERRLSY +   56)    /*        lhi.c: 297 */
#define   ELSY057      (ERRLSY +   57)    /*        lhi.c: 298 */
#define   ELSY058      (ERRLSY +   58)    /*        lhi.c: 302 */
#define   ELSY059      (ERRLSY +   59)    /*        lhi.c: 305 */
#define   ELSY060      (ERRLSY +   60)    /*        lhi.c: 308 */
#define   ELSY061      (ERRLSY +   61)    /*        lhi.c: 309 */
#define   ELSY062      (ERRLSY +   62)    /*        lhi.c: 311 */
#define   ELSY063      (ERRLSY +   63)    /*        lhi.c: 312 */
#define   ELSY064      (ERRLSY +   64)    /*        lhi.c: 313 */
#define   ELSY065      (ERRLSY +   65)    /*        lhi.c: 320 */
#define   ELSY066      (ERRLSY +   66)    /*        lhi.c: 327 */
#define   ELSY067      (ERRLSY +   67)    /*        lhi.c: 328 */
#define   ELSY068      (ERRLSY +   68)    /*        lhi.c: 374 */
#define   ELSY069      (ERRLSY +   69)    /*        lhi.c: 375 */
#define   ELSY070      (ERRLSY +   70)    /*        lhi.c: 424 */
#define   ELSY071      (ERRLSY +   71)    /*        lhi.c: 433 */
#define   ELSY072      (ERRLSY +   72)    /*        lhi.c: 435 */
#define   ELSY073      (ERRLSY +   73)    /*        lhi.c: 440 */
#define   ELSY074      (ERRLSY +   74)    /*        lhi.c: 445 */
#define   ELSY075      (ERRLSY +   75)    /*        lhi.c: 457 */
#define   ELSY076      (ERRLSY +   76)    /*        lhi.c: 462 */
#define   ELSY077      (ERRLSY +   77)    /*        lhi.c: 467 */
#define   ELSY078      (ERRLSY +   78)    /*        lhi.c: 472 */
#define   ELSY079      (ERRLSY +   79)    /*        lhi.c: 482 */
#define   ELSY080      (ERRLSY +   80)    /*        lhi.c: 489 */
#define   ELSY081      (ERRLSY +   81)    /*        lhi.c: 490 */
#define   ELSY082      (ERRLSY +   82)    /*        lhi.c: 491 */
#define   ELSY083      (ERRLSY +   83)    /*        lhi.c: 493 */
#define   ELSY084      (ERRLSY +   84)    /*        lhi.c: 494 */
#define   ELSY085      (ERRLSY +   85)    /*        lhi.c: 541 */
#define   ELSY086      (ERRLSY +   86)    /*        lhi.c: 544 */
#define   ELSY087      (ERRLSY +   87)    /*        lhi.c: 602 */
#define   ELSY088      (ERRLSY +   88)    /*        lhi.c: 607 */
#define   ELSY089      (ERRLSY +   89)    /*        lhi.c: 613 */
#define   ELSY090      (ERRLSY +   90)    /*        lhi.c: 614 */
#define   ELSY091      (ERRLSY +   91)    /*        lhi.c: 615 */
#define   ELSY092      (ERRLSY +   92)    /*        lhi.c: 760 */
#define   ELSY093      (ERRLSY +   93)    /*        lhi.c: 761 */
#define   ELSY094      (ERRLSY +   94)    /*        lhi.c: 763 */
#define   ELSY095      (ERRLSY +   95)    /*        lhi.c: 764 */
#define   ELSY096      (ERRLSY +   96)    /*        lhi.c: 765 */
#define   ELSY097      (ERRLSY +   97)    /*        lhi.c: 766 */
#define   ELSY098      (ERRLSY +   98)    /*        lhi.c: 767 */
#define   ELSY099      (ERRLSY +   99)    /*        lhi.c: 768 */
#define   ELSY100      (ERRLSY +  100)    /*        lhi.c: 769 */
#define   ELSY101      (ERRLSY +  101)    /*        lhi.c: 770 */
#define   ELSY102      (ERRLSY +  102)    /*        lhi.c: 771 */
#define   ELSY103      (ERRLSY +  103)    /*        lhi.c: 772 */
#define   ELSY104      (ERRLSY +  104)    /*        lhi.c: 773 */
#define   ELSY105      (ERRLSY +  105)    /*        lhi.c: 774 */
#define   ELSY106      (ERRLSY +  106)    /*        lhi.c: 775 */
#define   ELSY107      (ERRLSY +  107)    /*        lhi.c: 776 */
#define   ELSY108      (ERRLSY +  108)    /*        lhi.c: 777 */
#define   ELSY109      (ERRLSY +  109)    /*        lhi.c: 778 */
#define   ELSY110      (ERRLSY +  110)    /*        lhi.c: 779 */
#define   ELSY111      (ERRLSY +  111)    /*        lhi.c: 780 */
#define   ELSY112      (ERRLSY +  112)    /*        lhi.c: 781 */
#define   ELSY113      (ERRLSY +  113)    /*        lhi.c: 782 */
#define   ELSY114      (ERRLSY +  114)    /*        lhi.c: 789 */
#define   ELSY115      (ERRLSY +  115)    /*        lhi.c: 790 */
#define   ELSY116      (ERRLSY +  116)    /*        lhi.c: 792 */
#define   ELSY117      (ERRLSY +  117)    /*        lhi.c: 793 */
#define   ELSY118      (ERRLSY +  118)    /*        lhi.c: 794 */
#define   ELSY119      (ERRLSY +  119)    /*        lhi.c: 795 */
#define   ELSY120      (ERRLSY +  120)    /*        lhi.c: 796 */
#define   ELSY121      (ERRLSY +  121)    /*        lhi.c: 797 */
#define   ELSY122      (ERRLSY +  122)    /*        lhi.c: 798 */
#define   ELSY123      (ERRLSY +  123)    /*        lhi.c: 799 */
#define   ELSY124      (ERRLSY +  124)    /*        lhi.c: 800 */
#define   ELSY125      (ERRLSY +  125)    /*        lhi.c: 801 */
#define   ELSY126      (ERRLSY +  126)    /*        lhi.c: 835 */
#define   ELSY127      (ERRLSY +  127)    /*        lhi.c: 842 */
#define   ELSY128      (ERRLSY +  128)    /*        lhi.c: 843 */
#define   ELSY129      (ERRLSY +  129)    /*        lhi.c: 845 */
#define   ELSY130      (ERRLSY +  130)    /*        lhi.c: 846 */
#define   ELSY131      (ERRLSY +  131)    /*        lhi.c: 892 */
#define   ELSY132      (ERRLSY +  132)    /*        lhi.c: 896 */
#define   ELSY133      (ERRLSY +  133)    /*        lhi.c: 903 */
#define   ELSY134      (ERRLSY +  134)    /*        lhi.c: 904 */
#define   ELSY135      (ERRLSY +  135)    /*        lhi.c: 955 */
#define   ELSY136      (ERRLSY +  136)    /*        lhi.c: 964 */
#define   ELSY137      (ERRLSY +  137)    /*        lhi.c: 965 */
#define   ELSY138      (ERRLSY +  138)    /*        lhi.c: 966 */
#define   ELSY139      (ERRLSY +  139)    /*        lhi.c: 969 */
#define   ELSY140      (ERRLSY +  140)    /*        lhi.c: 970 */
#define   ELSY141      (ERRLSY +  141)    /*        lhi.c: 975 */
#define   ELSY142      (ERRLSY +  142)    /*        lhi.c: 983 */
#define   ELSY143      (ERRLSY +  143)    /*        lhi.c: 985 */
#define   ELSY144      (ERRLSY +  144)    /*        lhi.c: 986 */
#define   ELSY145      (ERRLSY +  145)    /*        lhi.c:1033 */
#define   ELSY146      (ERRLSY +  146)    /*        lhi.c:1039 */
#define   ELSY147      (ERRLSY +  147)    /*        lhi.c:1041 */
#define   ELSY148      (ERRLSY +  148)    /*        lhi.c:1046 */
#define   ELSY149      (ERRLSY +  149)    /*        lhi.c:1051 */
#define   ELSY150      (ERRLSY +  150)    /*        lhi.c:1057 */
#define   ELSY151      (ERRLSY +  151)    /*        lhi.c:1063 */
#define   ELSY152      (ERRLSY +  152)    /*        lhi.c:1070 */
#define   ELSY153      (ERRLSY +  153)    /*        lhi.c:1071 */
#define   ELSY154      (ERRLSY +  154)    /*        lhi.c:1072 */
#define   ELSY155      (ERRLSY +  155)    /*        lhi.c:1074 */
#define   ELSY156      (ERRLSY +  156)    /*        lhi.c:1075 */
#define   ELSY157      (ERRLSY +  157)    /*        lhi.c:1124 */
#define   ELSY158      (ERRLSY +  158)    /*        lhi.c:1127 */
#define   ELSY159      (ERRLSY +  159)    /*        lhi.c:1128 */
#define   ELSY160      (ERRLSY +  160)    /*        lhi.c:1130 */
#define   ELSY161      (ERRLSY +  161)    /*        lhi.c:1131 */
#define   ELSY162      (ERRLSY +  162)    /*        lhi.c:1179 */
#define   ELSY163      (ERRLSY +  163)    /*        lhi.c:1180 */
#define   ELSY164      (ERRLSY +  164)    /*        lhi.c:1189 */
#define   ELSY165      (ERRLSY +  165)    /*        lhi.c:1194 */
#define   ELSY166      (ERRLSY +  166)    /*        lhi.c:1201 */
#define   ELSY167      (ERRLSY +  167)    /*        lhi.c:1204 */
#define   ELSY168      (ERRLSY +  168)    /*        lhi.c:1205 */
#define   ELSY169      (ERRLSY +  169)    /*        lhi.c:1208 */
#define   ELSY170      (ERRLSY +  170)    /*        lhi.c:1209 */
#define   ELSY171      (ERRLSY +  171)    /*        lhi.c:1210 */
#define   ELSY172      (ERRLSY +  172)    /*        lhi.c:1211 */
#define   ELSY173      (ERRLSY +  173)    /*        lhi.c:1212 */
#define   ELSY174      (ERRLSY +  174)    /*        lhi.c:1214 */
#define   ELSY175      (ERRLSY +  175)    /*        lhi.c:1215 */
#define   ELSY176      (ERRLSY +  176)    /*        lhi.c:1216 */
#define   ELSY177      (ERRLSY +  177)    /*        lhi.c:1217 */
#define   ELSY178      (ERRLSY +  178)    /*        lhi.c:1219 */
#define   ELSY179      (ERRLSY +  179)    /*        lhi.c:1220 */
#define   ELSY180      (ERRLSY +  180)    /*        lhi.c:1221 */
#define   ELSY181      (ERRLSY +  181)    /*        lhi.c:1222 */
#define   ELSY182      (ERRLSY +  182)    /*        lhi.c:1223 */
#define   ELSY183      (ERRLSY +  183)    /*        lhi.c:1224 */
#define   ELSY184      (ERRLSY +  184)    /*        lhi.c:1225 */
#define   ELSY185      (ERRLSY +  185)    /*        lhi.c:1226 */
#define   ELSY186      (ERRLSY +  186)    /*        lhi.c:1227 */
#define   ELSY187      (ERRLSY +  187)    /*        lhi.c:1228 */
#define   ELSY188      (ERRLSY +  188)    /*        lhi.c:1233 */
#define   ELSY189      (ERRLSY +  189)    /*        lhi.c:1236 */
#define   ELSY190      (ERRLSY +  190)    /*        lhi.c:1246 */
#define   ELSY191      (ERRLSY +  191)    /*        lhi.c:1247 */
#define   ELSY192      (ERRLSY +  192)    /*        lhi.c:1248 */
#define   ELSY193      (ERRLSY +  193)    /*        lhi.c:1249 */
#define   ELSY194      (ERRLSY +  194)    /*        lhi.c:1250 */
#define   ELSY195      (ERRLSY +  195)    /*        lhi.c:1251 */
#define   ELSY196      (ERRLSY +  196)    /*        lhi.c:1253 */
#define   ELSY197      (ERRLSY +  197)    /*        lhi.c:1254 */
#define   ELSY198      (ERRLSY +  198)    /*        lhi.c:1255 */
#define   ELSY199      (ERRLSY +  199)    /*        lhi.c:1256 */
#define   ELSY200      (ERRLSY +  200)    /*        lhi.c:1257 */
#define   ELSY201      (ERRLSY +  201)    /*        lhi.c:1258 */
#define   ELSY202      (ERRLSY +  202)    /*        lhi.c:1259 */
#define   ELSY203      (ERRLSY +  203)    /*        lhi.c:1260 */
#define   ELSY204      (ERRLSY +  204)    /*        lhi.c:1262 */
#define   ELSY205      (ERRLSY +  205)    /*        lhi.c:1263 */
#define   ELSY206      (ERRLSY +  206)    /*        lhi.c:1264 */
#define   ELSY207      (ERRLSY +  207)    /*        lhi.c:1265 */
#define   ELSY208      (ERRLSY +  208)    /*        lhi.c:1269 */
#define   ELSY209      (ERRLSY +  209)    /*        lhi.c:1271 */
#define   ELSY210      (ERRLSY +  210)    /*        lhi.c:1273 */
#define   ELSY211      (ERRLSY +  211)    /*        lhi.c:1275 */
#define   ELSY212      (ERRLSY +  212)    /*        lhi.c:1280 */
#define   ELSY213      (ERRLSY +  213)    /*        lhi.c:1281 */
#define   ELSY214      (ERRLSY +  214)    /*        lhi.c:1291 */
#define   ELSY215      (ERRLSY +  215)    /*        lhi.c:1292 */
#define   ELSY216      (ERRLSY +  216)    /*        lhi.c:1294 */
#define   ELSY217      (ERRLSY +  217)    /*        lhi.c:1295 */
#define   ELSY218      (ERRLSY +  218)    /*        lhi.c:1297 */
#define   ELSY219      (ERRLSY +  219)    /*        lhi.c:1302 */
#define   ELSY220      (ERRLSY +  220)    /*        lhi.c:1305 */
#define   ELSY221      (ERRLSY +  221)    /*        lhi.c:1307 */
#define   ELSY222      (ERRLSY +  222)    /*        lhi.c:1308 */
#define   ELSY223      (ERRLSY +  223)    /*        lhi.c:1310 */
#define   ELSY224      (ERRLSY +  224)    /*        lhi.c:1312 */
#define   ELSY225      (ERRLSY +  225)    /*        lhi.c:1314 */
#define   ELSY226      (ERRLSY +  226)    /*        lhi.c:1316 */
#define   ELSY227      (ERRLSY +  227)    /*        lhi.c:1317 */
#define   ELSY228      (ERRLSY +  228)    /*        lhi.c:1324 */
#define   ELSY229      (ERRLSY +  229)    /*        lhi.c:1375 */
#define   ELSY230      (ERRLSY +  230)    /*        lhi.c:1376 */
#define   ELSY231      (ERRLSY +  231)    /*        lhi.c:1418 */
#define   ELSY232      (ERRLSY +  232)    /*        lhi.c:1419 */
#define   ELSY233      (ERRLSY +  233)    /*        lhi.c:1421 */
#define   ELSY234      (ERRLSY +  234)    /*        lhi.c:1422 */
#define   ELSY235      (ERRLSY +  235)    /*        lhi.c:1423 */
#define   ELSY236      (ERRLSY +  236)    /*        lhi.c:1432 */
#define   ELSY237      (ERRLSY +  237)    /*        lhi.c:1441 */
#define   ELSY238      (ERRLSY +  238)    /*        lhi.c:1446 */
#define   ELSY239      (ERRLSY +  239)    /*        lhi.c:1448 */
#define   ELSY240      (ERRLSY +  240)    /*        lhi.c:1453 */
#define   ELSY241      (ERRLSY +  241)    /*        lhi.c:1465 */
#define   ELSY242      (ERRLSY +  242)    /*        lhi.c:1470 */
#define   ELSY243      (ERRLSY +  243)    /*        lhi.c:1475 */
#define   ELSY244      (ERRLSY +  244)    /*        lhi.c:1480 */
#define   ELSY245      (ERRLSY +  245)    /*        lhi.c:1490 */
#define   ELSY246      (ERRLSY +  246)    /*        lhi.c:1536 */
#define   ELSY247      (ERRLSY +  247)    /*        lhi.c:1537 */
#define   ELSY248      (ERRLSY +  248)    /*        lhi.c:1581 */
#define   ELSY249      (ERRLSY +  249)    /*        lhi.c:1582 */
#define   ELSY250      (ERRLSY +  250)    /*        lhi.c:1583 */
#define   ELSY251      (ERRLSY +  251)    /*        lhi.c:1599 */
#define   ELSY252      (ERRLSY +  252)    /*        lhi.c:1605 */
#define   ELSY253      (ERRLSY +  253)    /*        lhi.c:1703 */
#define   ELSY254      (ERRLSY +  254)    /*        lhi.c:1757 */
#define   ELSY255      (ERRLSY +  255)    /*        lhi.c:1758 */
#define   ELSY256      (ERRLSY +  256)    /*        lhi.c:1760 */
#define   ELSY257      (ERRLSY +  257)    /*        lhi.c:1761 */
#define   ELSY258      (ERRLSY +  258)    /*        lhi.c:1769 */
#define   ELSY259      (ERRLSY +  259)    /*        lhi.c:1770 */
#define   ELSY260      (ERRLSY +  260)    /*        lhi.c:1771 */
#define   ELSY261      (ERRLSY +  261)    /*        lhi.c:1772 */
#define   ELSY262      (ERRLSY +  262)    /*        lhi.c:1773 */
#define   ELSY263      (ERRLSY +  263)    /*        lhi.c:1774 */
#define   ELSY264      (ERRLSY +  264)    /*        lhi.c:1775 */
#define   ELSY265      (ERRLSY +  265)    /*        lhi.c:1776 */
#define   ELSY266      (ERRLSY +  266)    /*        lhi.c:1777 */
#define   ELSY267      (ERRLSY +  267)    /*        lhi.c:1778 */
#define   ELSY268      (ERRLSY +  268)    /*        lhi.c:1779 */
#define   ELSY269      (ERRLSY +  269)    /*        lhi.c:1780 */
#define   ELSY270      (ERRLSY +  270)    /*        lhi.c:1781 */
#define   ELSY271      (ERRLSY +  271)    /*        lhi.c:1782 */
#define   ELSY272      (ERRLSY +  272)    /*        lhi.c:1783 */
#define   ELSY273      (ERRLSY +  273)    /*        lhi.c:1784 */
#define   ELSY274      (ERRLSY +  274)    /*        lhi.c:1785 */
#define   ELSY275      (ERRLSY +  275)    /*        lhi.c:1786 */
#define   ELSY276      (ERRLSY +  276)    /*        lhi.c:1787 */
#define   ELSY277      (ERRLSY +  277)    /*        lhi.c:1788 */
#define   ELSY278      (ERRLSY +  278)    /*        lhi.c:1790 */
#define   ELSY279      (ERRLSY +  279)    /*        lhi.c:1791 */
#define   ELSY280      (ERRLSY +  280)    /*        lhi.c:1799 */
#define   ELSY281      (ERRLSY +  281)    /*        lhi.c:1800 */
#define   ELSY282      (ERRLSY +  282)    /*        lhi.c:1801 */
#define   ELSY283      (ERRLSY +  283)    /*        lhi.c:1802 */
#define   ELSY284      (ERRLSY +  284)    /*        lhi.c:1803 */
#define   ELSY285      (ERRLSY +  285)    /*        lhi.c:1804 */
#define   ELSY286      (ERRLSY +  286)    /*        lhi.c:1805 */
#define   ELSY287      (ERRLSY +  287)    /*        lhi.c:1806 */
#define   ELSY288      (ERRLSY +  288)    /*        lhi.c:1807 */
#define   ELSY289      (ERRLSY +  289)    /*        lhi.c:1808 */
#define   ELSY290      (ERRLSY +  290)    /*        lhi.c:1810 */
#define   ELSY291      (ERRLSY +  291)    /*        lhi.c:1811 */
#define   ELSY292      (ERRLSY +  292)    /*        lhi.c:1839 */
#define   ELSY293      (ERRLSY +  293)    /*        lhi.c:1859 */
#define   ELSY294      (ERRLSY +  294)    /*        lhi.c:1905 */
#define   ELSY295      (ERRLSY +  295)    /*        lhi.c:1906 */
#define   ELSY296      (ERRLSY +  296)    /*        lhi.c:1914 */
#define   ELSY297      (ERRLSY +  297)    /*        lhi.c:1918 */
#define   ELSY298      (ERRLSY +  298)    /*        lhi.c:1966 */
#define   ELSY299      (ERRLSY +  299)    /*        lhi.c:1967 */
#define   ELSY300      (ERRLSY +  300)    /*        lhi.c:1968 */
#define   ELSY301      (ERRLSY +  301)    /*        lhi.c:1976 */
#define   ELSY302      (ERRLSY +  302)    /*        lhi.c:1983 */
#define   ELSY303      (ERRLSY +  303)    /*        lhi.c:1984 */
#define   ELSY304      (ERRLSY +  304)    /*        lhi.c:1987 */
#define   ELSY305      (ERRLSY +  305)    /*        lhi.c:1988 */
#define   ELSY306      (ERRLSY +  306)    /*        lhi.c:1989 */
#define   ELSY307      (ERRLSY +  307)    /*        lhi.c:1995 */
#define   ELSY308      (ERRLSY +  308)    /*        lhi.c:2042 */
#define   ELSY309      (ERRLSY +  309)    /*        lhi.c:2043 */
#define   ELSY310      (ERRLSY +  310)    /*        lhi.c:2045 */
#define   ELSY311      (ERRLSY +  311)    /*        lhi.c:2046 */
#define   ELSY312      (ERRLSY +  312)    /*        lhi.c:2047 */
#define   ELSY313      (ERRLSY +  313)    /*        lhi.c:2055 */
#define   ELSY314      (ERRLSY +  314)    /*        lhi.c:2060 */
#define   ELSY315      (ERRLSY +  315)    /*        lhi.c:2065 */
#define   ELSY316      (ERRLSY +  316)    /*        lhi.c:2067 */
#define   ELSY317      (ERRLSY +  317)    /*        lhi.c:2072 */
#define   ELSY318      (ERRLSY +  318)    /*        lhi.c:2078 */
#define   ELSY319      (ERRLSY +  319)    /*        lhi.c:2084 */
#define   ELSY320      (ERRLSY +  320)    /*        lhi.c:2133 */
#define   ELSY321      (ERRLSY +  321)    /*        lhi.c:2134 */
#define   ELSY322      (ERRLSY +  322)    /*        lhi.c:2136 */
#define   ELSY323      (ERRLSY +  323)    /*        lhi.c:2137 */
#define   ELSY324      (ERRLSY +  324)    /*        lhi.c:2140 */

#define   ELSY325      (ERRLSY +  325)    /*        lhi.h: 305 */
#define   ELSY326      (ERRLSY +  326)    /*        lhi.h: 306 */
#define   ELSY327      (ERRLSY +  327)    /*        lhi.h: 307 */
#define   ELSY328      (ERRLSY +  328)    /*        lhi.h: 308 */
#define   ELSY329      (ERRLSY +  329)    /*        lhi.h: 309 */
#define   ELSY330      (ERRLSY +  330)    /*        lhi.h: 310 */
#define   ELSY331      (ERRLSY +  331)    /*        lhi.h: 311 */
#define   ELSY332      (ERRLSY +  332)    /*        lhi.h: 312 */
#define   ELSY333      (ERRLSY +  333)    /*        lhi.h: 313 */
#define   ELSY334      (ERRLSY +  334)    /*        lhi.h: 314 */
#define   ELSY335      (ERRLSY +  335)    /*        lhi.h: 315 */
#define   ELSY336      (ERRLSY +  336)    /*        lhi.h: 316 */
#define   ELSY337      (ERRLSY +  337)    /*        lhi.h: 317 */
#define   ELSY338      (ERRLSY +  338)    /*        lhi.h: 318 */
#define   ELSY339      (ERRLSY +  339)    /*        lhi.h: 319 */
#define   ELSY340      (ERRLSY +  340)    /*        lhi.h: 320 */
#define   ELSY341      (ERRLSY +  341)    /*        lhi.h: 321 */
#define   ELSY342      (ERRLSY +  342)    /*        lhi.h: 322 */
#define   ELSY343      (ERRLSY +  343)    /*        lhi.h: 323 */
#define   ELSY344      (ERRLSY +  344)    /*        lhi.h: 324 */
#define   ELSY345      (ERRLSY +  345)    /*        lhi.h: 325 */
#define   ELSY346      (ERRLSY +  346)    /*        lhi.h: 326 */
#define   ELSY347      (ERRLSY +  347)    /*        lhi.h: 327 */
#define   ELSY348      (ERRLSY +  348)    /*        lhi.h: 328 */
#define   ELSY349      (ERRLSY +  349)    /*        lhi.h: 329 */
#define   ELSY350      (ERRLSY +  350)    /*        lhi.h: 330 */
#define   ELSY351      (ERRLSY +  351)    /*        lhi.h: 331 */
#define   ELSY352      (ERRLSY +  352)    /*        lhi.h: 332 */
#define   ELSY353      (ERRLSY +  353)    /*        lhi.h: 333 */
#define   ELSY354      (ERRLSY +  354)    /*        lhi.h: 334 */
#define   ELSY355      (ERRLSY +  355)    /*        lhi.h: 335 */
#define   ELSY356      (ERRLSY +  356)    /*        lhi.h: 336 */
#define   ELSY357      (ERRLSY +  357)    /*        lhi.h: 337 */
#define   ELSY358      (ERRLSY +  358)    /*        lhi.h: 338 */
#define   ELSY359      (ERRLSY +  359)    /*        lhi.h: 339 */
#define   ELSY360      (ERRLSY +  360)    /*        lhi.h: 340 */
#define   ELSY361      (ERRLSY +  361)    /*        lhi.h: 341 */
#define   ELSY362      (ERRLSY +  362)    /*        lhi.h: 342 */
#define   ELSY363      (ERRLSY +  363)    /*        lhi.h: 343 */
#define   ELSY364      (ERRLSY +  364)    /*        lhi.h: 344 */
#define   ELSY365      (ERRLSY +  365)    /*        lhi.h: 345 */
#define   ELSY366      (ERRLSY +  366)    /*        lhi.h: 346 */
#define   ELSY367      (ERRLSY +  367)    /*        lhi.h: 347 */
#define   ELSY368      (ERRLSY +  368)    /*        lhi.h: 348 */
#define   ELSY369      (ERRLSY +  369)    /*        lhi.h: 349 */
#define   ELSY370      (ERRLSY +  370)    /*        lhi.h: 350 */
#define   ELSY371      (ERRLSY +  371)    /*        lhi.h: 351 */
#define   ELSY372      (ERRLSY +  372)    /*        lhi.h: 352 */
#define   ELSY373      (ERRLSY +  373)    /*        lhi.h: 353 */
#define   ELSY374      (ERRLSY +  374)    /*        lhi.h: 354 */
#define   ELSY375      (ERRLSY +  375)    /*        lhi.h: 355 */
#define   ELSY376      (ERRLSY +  376)    /*        lhi.h: 356 */
#define   ELSY377      (ERRLSY +  377)    /*        lhi.h: 357 */
#define   ELSY378      (ERRLSY +  378)    /*        lhi.h: 358 */
#define   ELSY379      (ERRLSY +  379)    /*        lhi.h: 359 */
#define   ELSY380      (ERRLSY +  380)    /*        lhi.h: 360 */
#define   ELSY381      (ERRLSY +  381)    /*        lhi.h: 361 */
#define   ELSY382      (ERRLSY +  382)    /*        lhi.h: 362 */
#define   ELSY383      (ERRLSY +  383)    /*        lhi.h: 363 */
#define   ELSY384      (ERRLSY +  384)    /*        lhi.h: 364 */
#define   ELSY385      (ERRLSY +  385)    /*        lhi.h: 365 */
#define   ELSY386      (ERRLSY +  386)    /*        lhi.h: 366 */
#define   ELSY387      (ERRLSY +  387)    /*        lhi.h: 367 */
#define   ELSY388      (ERRLSY +  388)    /*        lhi.h: 368 */
#define   ELSY389      (ERRLSY +  389)    /*        lhi.h: 369 */
#define   ELSY390      (ERRLSY +  390)    /*        lhi.h: 370 */
#define   ELSY391      (ERRLSY +  391)    /*        lhi.h: 371 */
#define   ELSY392      (ERRLSY +  392)    /*        lhi.h: 372 */
#define   ELSY393      (ERRLSY +  393)    /*        lhi.h: 373 */
#define   ELSY394      (ERRLSY +  394)    /*        lhi.h: 374 */
#define   ELSY395      (ERRLSY +  395)    /*        lhi.h: 375 */
#define   ELSY396      (ERRLSY +  396)    /*        lhi.h: 376 */
#define   ELSY397      (ERRLSY +  397)    /*        lhi.h: 377 */
#define   ELSY398      (ERRLSY +  398)    /*        lhi.h: 378 */
#define   ELSY399      (ERRLSY +  399)    /*        lhi.h: 379 */
#define   ELSY400      (ERRLSY +  400)    /*        lhi.h: 380 */
#define   ELSY401      (ERRLSY +  401)    /*        lhi.h: 381 */
#define   ELSY402      (ERRLSY +  402)    /*        lhi.h: 382 */
#define   ELSY403      (ERRLSY +  403)    /*        lhi.h: 383 */
#define   ELSY404      (ERRLSY +  404)    /*        lhi.h: 384 */
#define   ELSY405      (ERRLSY +  405)    /*        lhi.h: 385 */
#define   ELSY406      (ERRLSY +  406)    /*        lhi.h: 386 */
#define   ELSY407      (ERRLSY +  407)    /*        lhi.h: 387 */
#define   ELSY408      (ERRLSY +  408)    /*        lhi.h: 388 */
#define   ELSY409      (ERRLSY +  409)    /*        lhi.h: 389 */
#define   ELSY410      (ERRLSY +  410)    /*        lhi.h: 390 */
#define   ELSY411      (ERRLSY +  411)    /*        lhi.h: 391 */
#define   ELSY412      (ERRLSY +  412)    /*        lhi.h: 392 */
#define   ELSY413      (ERRLSY +  413)    /*        lhi.h: 393 */
#define   ELSY414      (ERRLSY +  414)    /*        lhi.h: 394 */
#define   ELSY415      (ERRLSY +  415)    /*        lhi.h: 395 */
#define   ELSY416      (ERRLSY +  416)    /*        lhi.h: 396 */
#define   ELSY417      (ERRLSY +  417)    /*        lhi.h: 397 */
#define   ELSY418      (ERRLSY +  418)    /*        lhi.h: 398 */
#define   ELSY419      (ERRLSY +  419)    /*        lhi.h: 399 */
#define   ELSY420      (ERRLSY +  420)    /*        lhi.h: 400 */
#define   ELSY421      (ERRLSY +  421)    /*        lhi.h: 401 */
#define   ELSY422      (ERRLSY +  422)    /*        lhi.h: 402 */
#define   ELSY423      (ERRLSY +  423)    /*        lhi.h: 403 */
#define   ELSY424      (ERRLSY +  424)    /*        lhi.h: 404 */
#define   ELSY425      (ERRLSY +  425)    /*        lhi.h: 405 */
#define   ELSY426      (ERRLSY +  426)    /*        lhi.h: 406 */
#define   ELSY427      (ERRLSY +  427)    /*        lhi.h: 407 */
#define   ELSY428      (ERRLSY +  428)    /*        lhi.h: 408 */
#define   ELSY429      (ERRLSY +  429)    /*        lhi.h: 409 */
#define   ELSY430      (ERRLSY +  430)    /*        lhi.h: 410 */
#define   ELSY431      (ERRLSY +  431)    /*        lhi.h: 411 */
#define   ELSY432      (ERRLSY +  432)    /*        lhi.h: 412 */
#define   ELSY433      (ERRLSY +  433)    /*        lhi.h: 413 */
#define   ELSY434      (ERRLSY +  434)    /*        lhi.h: 414 */
#define   ELSY435      (ERRLSY +  435)    /*        lhi.h: 415 */
#define   ELSY436      (ERRLSY +  436)    /*        lhi.h: 416 */
#define   ELSY437      (ERRLSY +  437)    /*        lhi.h: 417 */
#define   ELSY438      (ERRLSY +  438)    /*        lhi.h: 418 */
#define   ELSY439      (ERRLSY +  439)    /*        lhi.h: 419 */
#define   ELSY440      (ERRLSY +  440)    /*        lhi.h: 420 */
#define   ELSY441      (ERRLSY +  441)    /*        lhi.h: 421 */
#define   ELSY442      (ERRLSY +  442)    /*        lhi.h: 422 */
#define   ELSY443      (ERRLSY +  443)    /*        lhi.h: 423 */
#define   ELSY444      (ERRLSY +  444)    /*        lhi.h: 424 */
#define   ELSY445      (ERRLSY +  445)    /*        lhi.h: 425 */
#define   ELSY446      (ERRLSY +  446)    /*        lhi.h: 426 */
#define   ELSY447      (ERRLSY +  447)    /*        lhi.h: 427 */
#define   ELSY448      (ERRLSY +  448)    /*        lhi.h: 428 */
#define   ELSY449      (ERRLSY +  449)    /*        lhi.h: 429 */
#define   ELSY450      (ERRLSY +  450)    /*        lhi.h: 430 */
#define   ELSY451      (ERRLSY +  451)    /*        lhi.h: 431 */
#define   ELSY452      (ERRLSY +  452)    /*        lhi.h: 432 */
#define   ELSY453      (ERRLSY +  453)    /*        lhi.h: 433 */
#define   ELSY454      (ERRLSY +  454)    /*        lhi.h: 434 */
#define   ELSY455      (ERRLSY +  455)    /*        lhi.h: 435 */
#define   ELSY456      (ERRLSY +  456)    /*        lhi.h: 436 */
#define   ELSY457      (ERRLSY +  457)    /*        lhi.h: 437 */
#define   ELSY458      (ERRLSY +  458)    /*        lhi.h: 438 */
#define   ELSY459      (ERRLSY +  459)    /*        lhi.h: 439 */
#define   ELSY460      (ERRLSY +  460)    /*        lhi.h: 440 */
#define   ELSY461      (ERRLSY +  461)    /*        lhi.h: 441 */
#define   ELSY462      (ERRLSY +  462)    /*        lhi.h: 442 */
#define   ELSY463      (ERRLSY +  463)    /*        lhi.h: 443 */
#define   ELSY464      (ERRLSY +  464)    /*        lhi.h: 444 */
#define   ELSY465      (ERRLSY +  465)    /*        lhi.h: 445 */
#define   ELSY466      (ERRLSY +  466)    /*        lhi.h: 446 */
#define   ELSY467      (ERRLSY +  467)    /*        lhi.h: 447 */
#define   ELSY468      (ERRLSY +  468)    /*        lhi.h: 448 */
#define   ELSY469      (ERRLSY +  469)    /*        lhi.h: 449 */
#define   ELSY470      (ERRLSY +  470)    /*        lhi.h: 450 */
#define   ELSY471      (ERRLSY +  471)    /*        lhi.h: 451 */
#define   ELSY472      (ERRLSY +  472)    /*        lhi.h: 452 */
#define   ELSY473      (ERRLSY +  473)    /*        lhi.h: 453 */
#define   ELSY474      (ERRLSY +  474)    /*        lhi.h: 454 */
#define   ELSY475      (ERRLSY +  475)    /*        lhi.h: 455 */
#define   ELSY476      (ERRLSY +  476)    /*        lhi.h: 456 */
#define   ELSY477      (ERRLSY +  477)    /*        lhi.h: 457 */
#define   ELSY478      (ERRLSY +  478)    /*        lhi.h: 458 */
#define   ELSY479      (ERRLSY +  479)    /*        lhi.h: 459 */
#define   ELSY480      (ERRLSY +  480)    /*        lhi.h: 460 */
#define   ELSY481      (ERRLSY +  481)    /*        lhi.h: 461 */
#define   ELSY482      (ERRLSY +  482)    /*        lhi.h: 462 */
#define   ELSY483      (ERRLSY +  483)    /*        lhi.h: 463 */
#define   ELSY484      (ERRLSY +  484)    /*        lhi.h: 464 */
#define   ELSY485      (ERRLSY +  485)    /*        lhi.h: 465 */
#define   ELSY486      (ERRLSY +  486)    /*        lhi.h: 466 */
#define   ELSY487      (ERRLSY +  487)    /*        lhi.h: 467 */
#define   ELSY488      (ERRLSY +  488)    /*        lhi.h: 468 */
#define   ELSY489      (ERRLSY +  489)    /*        lhi.h: 469 */
#define   ELSY490      (ERRLSY +  490)    /*        lhi.h: 470 */
#define   ELSY491      (ERRLSY +  491)    /*        lhi.h: 471 */
#define   ELSY492      (ERRLSY +  492)    /*        lhi.h: 472 */
#define   ELSY493      (ERRLSY +  493)    /*        lhi.h: 473 */
#define   ELSY494      (ERRLSY +  494)    /*        lhi.h: 474 */
#define   ELSY495      (ERRLSY +  495)    /*        lhi.h: 475 */
#define   ELSY496      (ERRLSY +  496)    /*        lhi.h: 476 */
#define   ELSY497      (ERRLSY +  497)    /*        lhi.h: 477 */
#define   ELSY498      (ERRLSY +  498)    /*        lhi.h: 478 */
#define   ELSY499      (ERRLSY +  499)    /*        lhi.h: 479 */
#define   ELSY500      (ERRLSY +  500)    /*        lhi.h: 480 */
#define   ELSY501      (ERRLSY +  501)    /*        lhi.h: 481 */
#define   ELSY502      (ERRLSY +  502)    /*        lhi.h: 482 */
#define   ELSY503      (ERRLSY +  503)    /*        lhi.h: 483 */
#define   ELSY504      (ERRLSY +  504)    /*        lhi.h: 484 */
#define   ELSY505      (ERRLSY +  505)    /*        lhi.h: 485 */
#define   ELSY506      (ERRLSY +  506)    /*        lhi.h: 486 */
#define   ELSY507      (ERRLSY +  507)    /*        lhi.h: 487 */
#define   ELSY508      (ERRLSY +  508)    /*        lhi.h: 488 */
#define   ELSY509      (ERRLSY +  509)    /*        lhi.h: 489 */
#define   ELSY510      (ERRLSY +  510)    /*        lhi.h: 490 */
#define   ELSY511      (ERRLSY +  511)    /*        lhi.h: 491 */
#define   ELSY512      (ERRLSY +  512)    /*        lhi.h: 492 */
#define   ELSY513      (ERRLSY +  513)    /*        lhi.h: 493 */
#define   ELSY514      (ERRLSY +  514)    /*        lhi.h: 494 */
#define   ELSY515      (ERRLSY +  515)    /*        lhi.h: 495 */
#define   ELSY516      (ERRLSY +  516)    /*        lhi.h: 496 */
#define   ELSY517      (ERRLSY +  517)    /*        lhi.h: 497 */
#define   ELSY518      (ERRLSY +  518)    /*        lhi.h: 498 */
#define   ELSY519      (ERRLSY +  519)    /*        lhi.h: 499 */
#define   ELSY520      (ERRLSY +  520)    /*        lhi.h: 500 */
#define   ELSY521      (ERRLSY +  521)    /*        lhi.h: 501 */
#define   ELSY522      (ERRLSY +  522)    /*        lhi.h: 502 */
#define   ELSY523      (ERRLSY +  523)    /*        lhi.h: 503 */
#define   ELSY524      (ERRLSY +  524)    /*        lhi.h: 504 */
#define   ELSY525      (ERRLSY +  525)    /*        lhi.h: 505 */
#define   ELSY526      (ERRLSY +  526)    /*        lhi.h: 506 */
#define   ELSY527      (ERRLSY +  527)    /*        lhi.h: 507 */
#define   ELSY528      (ERRLSY +  528)    /*        lhi.h: 508 */
#define   ELSY529      (ERRLSY +  529)    /*        lhi.h: 509 */
#define   ELSY530      (ERRLSY +  530)    /*        lhi.h: 510 */
#define   ELSY531      (ERRLSY +  531)    /*        lhi.h: 511 */
#define   ELSY532      (ERRLSY +  532)    /*        lhi.h: 512 */
#define   ELSY533      (ERRLSY +  533)    /*        lhi.h: 513 */
#define   ELSY534      (ERRLSY +  534)    /*        lhi.h: 514 */
#define   ELSY535      (ERRLSY +  535)    /*        lhi.h: 515 */
#define   ELSY536      (ERRLSY +  536)    /*        lhi.h: 516 */
#define   ELSY537      (ERRLSY +  537)    /*        lhi.h: 517 */
#define   ELSY538      (ERRLSY +  538)    /*        lhi.h: 518 */
#define   ELSY539      (ERRLSY +  539)    /*        lhi.h: 519 */
#define   ELSY540      (ERRLSY +  540)    /*        lhi.h: 520 */
#define   ELSY541      (ERRLSY +  541)    /*        lhi.h: 521 */
#define   ELSY542      (ERRLSY +  542)    /*        lhi.h: 522 */
#define   ELSY543      (ERRLSY +  543)    /*        lhi.h: 523 */
#define   ELSY544      (ERRLSY +  544)    /*        lhi.h: 524 */
#define   ELSY545      (ERRLSY +  545)    /*        lhi.h: 525 */
#define   ELSY546      (ERRLSY +  546)    /*        lhi.h: 526 */
#define   ELSY547      (ERRLSY +  547)    /*        lhi.h: 527 */
#define   ELSY548      (ERRLSY +  548)    /*        lhi.h: 528 */
#define   ELSY549      (ERRLSY +  549)    /*        lhi.h: 529 */
#define   ELSY550      (ERRLSY +  550)    /*        lhi.h: 530 */
#define   ELSY551      (ERRLSY +  551)    /*        lhi.h: 531 */
#define   ELSY552      (ERRLSY +  552)    /*        lhi.h: 532 */
#define   ELSY553      (ERRLSY +  553)    /*        lhi.h: 533 */
#define   ELSY554      (ERRLSY +  554)    /*        lhi.h: 534 */
#define   ELSY555      (ERRLSY +  555)    /*        lhi.h: 535 */
#define   ELSY556      (ERRLSY +  556)    /*        lhi.h: 536 */
#define   ELSY557      (ERRLSY +  557)    /*        lhi.h: 537 */
#define   ELSY558      (ERRLSY +  558)    /*        lhi.h: 538 */
#define   ELSY559      (ERRLSY +  559)    /*        lhi.h: 539 */
#define   ELSY560      (ERRLSY +  560)    /*        lhi.h: 540 */
#define   ELSY561      (ERRLSY +  561)    /*        lhi.h: 541 */
#define   ELSY562      (ERRLSY +  562)    /*        lhi.h: 542 */
#define   ELSY563      (ERRLSY +  563)    /*        lhi.h: 543 */
#define   ELSY564      (ERRLSY +  564)    /*        lhi.h: 544 */
#define   ELSY565      (ERRLSY +  565)    /*        lhi.h: 545 */
#define   ELSY566      (ERRLSY +  566)    /*        lhi.h: 546 */
#define   ELSY567      (ERRLSY +  567)    /*        lhi.h: 547 */
#define   ELSY568      (ERRLSY +  568)    /*        lhi.h: 548 */
#define   ELSY569      (ERRLSY +  569)    /*        lhi.h: 549 */
#define   ELSY570      (ERRLSY +  570)    /*        lhi.h: 550 */
#define   ELSY571      (ERRLSY +  571)    /*        lhi.h: 551 */
#define   ELSY572      (ERRLSY +  572)    /*        lhi.h: 552 */
#define   ELSY573      (ERRLSY +  573)    /*        lhi.h: 553 */
#define   ELSY574      (ERRLSY +  574)    /*        lhi.h: 554 */
#define   ELSY575      (ERRLSY +  575)    /*        lhi.h: 555 */
#define   ELSY576      (ERRLSY +  576)    /*        lhi.h: 556 */
#define   ELSY577      (ERRLSY +  577)    /*        lhi.h: 557 */
#define   ELSY578      (ERRLSY +  578)    /*        lhi.h: 558 */
#define   ELSY579      (ERRLSY +  579)    /*        lhi.h: 559 */
#define   ELSY580      (ERRLSY +  580)    /*        lhi.h: 560 */
#define   ELSY581      (ERRLSY +  581)    /*        lhi.h: 561 */
#define   ELSY582      (ERRLSY +  582)    /*        lhi.h: 562 */
#define   ELSY583      (ERRLSY +  583)    /*        lhi.h: 563 */
#define   ELSY584      (ERRLSY +  584)    /*        lhi.h: 564 */
#define   ELSY585      (ERRLSY +  585)    /*        lhi.h: 565 */
#define   ELSY586      (ERRLSY +  586)    /*        lhi.h: 566 */
#define   ELSY587      (ERRLSY +  587)    /*        lhi.h: 567 */
#define   ELSY588      (ERRLSY +  588)    /*        lhi.h: 568 */
#define   ELSY589      (ERRLSY +  589)    /*        lhi.h: 569 */
#define   ELSY590      (ERRLSY +  590)    /*        lhi.h: 570 */
#define   ELSY591      (ERRLSY +  591)    /*        lhi.h: 571 */
#define   ELSY592      (ERRLSY +  592)    /*        lhi.h: 572 */
#define   ELSY593      (ERRLSY +  593)    /*        lhi.h: 573 */
#define   ELSY594      (ERRLSY +  594)    /*        lhi.h: 574 */
#define   ELSY595      (ERRLSY +  595)    /*        lhi.h: 575 */
#define   ELSY596      (ERRLSY +  596)    /*        lhi.h: 576 */
#define   ELSY597      (ERRLSY +  597)    /*        lhi.h: 577 */
#define   ELSY598      (ERRLSY +  598)    /*        lhi.h: 578 */
#define   ELSY599      (ERRLSY +  599)    /*        lhi.h: 579 */
#define   ELSY600      (ERRLSY +  600)    /*        lhi.h: 580 */
#define   ELSY601      (ERRLSY +  601)    /*        lhi.h: 581 */
#define   ELSY602      (ERRLSY +  602)    /*        lhi.h: 582 */
#define   ELSY603      (ERRLSY +  603)    /*        lhi.h: 583 */
#define   ELSY604      (ERRLSY +  604)    /*        lhi.h: 584 */
#define   ELSY605      (ERRLSY +  605)    /*        lhi.h: 585 */
#define   ELSY606      (ERRLSY +  606)    /*        lhi.h: 586 */
#define   ELSY607      (ERRLSY +  607)    /*        lhi.h: 587 */
#define   ELSY608      (ERRLSY +  608)    /*        lhi.h: 588 */
#define   ELSY609      (ERRLSY +  609)    /*        lhi.h: 589 */
#define   ELSY610      (ERRLSY +  610)    /*        lhi.h: 590 */
#define   ELSY611      (ERRLSY +  611)    /*        lhi.h: 591 */
#define   ELSY612      (ERRLSY +  612)    /*        lhi.h: 592 */
#define   ELSY613      (ERRLSY +  613)    /*        lhi.h: 593 */
#define   ELSY614      (ERRLSY +  614)    /*        lhi.h: 594 */
#define   ELSY615      (ERRLSY +  615)    /*        lhi.h: 595 */
#define   ELSY616      (ERRLSY +  616)    /*        lhi.h: 596 */
#define   ELSY617      (ERRLSY +  617)    /*        lhi.h: 597 */
#define   ELSY618      (ERRLSY +  618)    /*        lhi.h: 598 */
#define   ELSY619      (ERRLSY +  619)    /*        lhi.h: 599 */
#define   ELSY620      (ERRLSY +  620)    /*        lhi.h: 600 */
#define   ELSY621      (ERRLSY +  621)    /*        lhi.h: 601 */
#define   ELSY622      (ERRLSY +  622)    /*        lhi.h: 602 */
#define   ELSY623      (ERRLSY +  623)    /*        lhi.h: 603 */
#define   ELSY624      (ERRLSY +  624)    /*        lhi.h: 604 */
#define   ELSY625      (ERRLSY +  625)    /*        lhi.h: 605 */
#define   ELSY626      (ERRLSY +  626)    /*        lhi.h: 606 */
#define   ELSY627      (ERRLSY +  627)    /*        lhi.h: 607 */
#define   ELSY628      (ERRLSY +  628)    /*        lhi.h: 608 */
/* lhi_h_001.main_7: Handling of instream and outstream */
#define   ELSY629      (ERRLSY +  629)    /*        lhi.h: 609 */
#define   ELSY630      (ERRLSY +  630)    /*        lhi.h: 610 */
#define   ELSY631      (ERRLSY +  631)    /*        lhi.h: 611 */
#define   ELSY632      (ERRLSY +  632)    /*        lhi.h: 612 */
/* lhi_h_001.main_10: Added new error codes*/
#define   ELSY633      (ERRLSY +  633)    /*        lhi.c:1081*/
#define   ELSY634      (ERRLSY +  634)    /*        lhi.c:1083*/
#define   ELSY635      (ERRLSY +  635)    /*        lhi.c:2127*/
#define   ELSY636      (ERRLSY +  636)    /*        lhi.c:2125*/
/* lhi_h_001.main_11: Added new error codes */
#define   ELSY637      (ERRLSY +  637)    /*        lhi.c:2125*/
#define   ELSY638      (ERRLSY +  638)    /*        lhi.c:2125*/
#define   ELSY639      (ERRLSY +  639)    /*        lhi.c:2125*/
#define   ELSY640      (ERRLSY +  640)    /*        lhi.c:2125*/
#define   ELSY641      (ERRLSY +  641)    /*        lhi.c:2125*/
#define   ELSY642      (ERRLSY +  642)    /*        lhi.c:2125*/
#define   ELSY643      (ERRLSY +  643)    /*        lhi.c:2125*/
#define   ELSY644      (ERRLSY +  644)    /*        lhi.c:2125*/
#define   ELSY645      (ERRLSY +  645)    /*        lhi.c:2125*/
#define   ELSY646      (ERRLSY +  646)    /*        lhi.c:2125*/

#endif /* __LSYH__ */
