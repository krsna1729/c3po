/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter Convergence Layer

     Type:     C source file

     Desc:     freeDiameter initialization.

     File:     aqfd_init.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"
#include "lsy.h"
#include "lsy.x"

#include <freeDiameter/freeDiameter-host.h>
#include <freeDiameter/libfdcore.h>
#include <freeDiameter/libfdproto.h>

#include "aqfd.h"
#include "aqfd.x"

#include "sy.h"
#include "sy.x"
#include "sy_err.h"

AqDict aqDict;
AqQueue aqAIqueue;

/*
*
*       Fun:    aqfdInit
*
*       Desc:   Perform general configuration of freeDiameter. Must be done
*               after aqActvInit() is called, but before any other
*               interaction with Diameter.
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*               LAQ_REASON_FD_CORE_INITIALIZE_FAIL  - failure
*               LAQ_REASON_FD_CORE_PARSECONF_FAIL   - failure
*               LAQ_REASON_FD_CORE_START_FAIL       - failure
*
*       Notes:  None
*
*       File:   aqfd_init.c
*
*/
#ifdef ANSI
PUBLIC S16 aqfdInit
(
Void
)
#else
PUBLIC S16 aqfdInit(Void)
#endif
{
   /* Initialize the core freeDiameter library */
   CHECK_FCT_DO( fd_core_initialize(), return LAQ_REASON_FD_CORE_INITIALIZE_FAIL );

   /* Parse the configuration file */
   CHECK_FCT_DO( fd_core_parseconf(vbSmCb.cfgCb.hssFdCfg), return LAQ_REASON_FD_CORE_PARSECONF_FAIL );

   /* initialize the work queues */
   CHECK_FCT_DO( aqQueueInit(&aqAIqueue), return LAQ_REASON_AI_QUEUE_INIT_FAIL );

   return LCM_REASON_NOT_APPL;
}

#ifdef ANSI
PUBLIC S16 aqfdStartFd
(
Void
)
#else
PUBLIC S16 aqfdStartFd(Void)
#endif
{
   /* Start the servers */
   CHECK_FCT_DO( fd_core_start(), return LAQ_REASON_FD_CORE_START_FAIL );
   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdDictionaryInit
*
*       Desc:   Lookup freeDiameter common dictionary elements
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*               LAQ_REASON_FD_DICT_SEARCH           - failure
*
*       Notes:  None
*
*       File:   aqfd_init.c
*
*/
#ifdef ANSI
PUBLIC S16 aqfdDictionaryInit
(
Void
)
#else
PUBLIC S16 aqfdDictionaryInit(Void)
#endif
{
   vendor_id_t vnd_3gpp = 10415;

   TRC2(aqfdDictionaryInit);

   /*  initialize the dictionary object */
   AQ_ZERO(&aqDict, sizeof(aqDict));

   /* lookup 3GPP vendor */
   AQCHECK_FCT_DICT(DICT_VENDOR, VENDOR_BY_ID, &vnd_3gpp, aqDict.vnd3GPP);
   AQCHECK_FCT_DICT_GETVAL(aqDict.vnd3GPP, &aqDict.vnd3GPPdata);


   /* lookup AVP's */
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "Result-Code", aqDict.avp_Result_Code);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "Experimental-Result", aqDict.avp_Experimental_Result);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "Experimental-Result-Code", aqDict.avp_Experimental_Result_Code);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "Vendor-Specific-Application-Id", aqDict.avp_Vendor_Specific_Application_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "Auth-Application-Id", aqDict.avp_Auth_Application_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "Acct-Application-Id", aqDict.avp_Acct_Application_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "Vendor-Id", aqDict.avp_Vendor_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "Session-Id", aqDict.avp_Session_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "Auth-Session-State", aqDict.avp_Auth_Session_State);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "Origin-Host", aqDict.avp_Origin_Host);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "Origin-Realm", aqDict.avp_Origin_Realm);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "Destination-Host", aqDict.avp_Destination_Host);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "Destination-Realm", aqDict.avp_Destination_Realm);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "User-Name", aqDict.avp_User_Name);
//   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME, "", aqDict.avp_);

   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Result_Code, &aqDict.davp_Result_Code);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Experimental_Result, &aqDict.davp_Experimental_Result);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Experimental_Result_Code, &aqDict.davp_Experimental_Result_Code);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Vendor_Specific_Application_Id, &aqDict.davp_Vendor_Specific_Application_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Auth_Application_Id, &aqDict.davp_Auth_Application_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Acct_Application_Id, &aqDict.davp_Acct_Application_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Vendor_Id, &aqDict.davp_Vendor_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Session_Id, &aqDict.davp_Session_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Auth_Session_State, &aqDict.davp_Auth_Session_State);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Origin_Host, &aqDict.davp_Origin_Host);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Origin_Realm, &aqDict.davp_Origin_Realm);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Destination_Host, &aqDict.davp_Destination_Host);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Destination_Realm, &aqDict.davp_Destination_Realm);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_User_Name, &aqDict.davp_User_Name);
//   AQCHECK_FCT_DICT_GETVAL(aqDict., &aqDict.d);

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdS6aInit
*
*       Desc:   Lookup freeDiameter dictionary elements for S6A
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*               LAQ_REASON_FD_DICT_SEARCH           - failure
*
*       Notes:  None
*
*       File:   aqfd_init.c
*
*/
#ifdef ANSI
PUBLIC S16 aqfdS6aInit
(
Void
)
#else
PUBLIC S16 aqfdS6aInit(Void)
#endif
{
   /* vendor_id_t vnd_3gpp = 10415; */
   application_id_t app_s6a = 16777251;

   TRC2(aqfdS6aInit);

   /* lookup S6A application */
   AQCHECK_FCT_DICT(DICT_APPLICATION, APPLICATION_BY_ID, &app_s6a, aqDict.appS6A);
   AQCHECK_FCT_DICT_GETVAL(aqDict.appS6A, &aqDict.appS6Adata);

   /* lookup commands */
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "Authentication-Information-Request", aqDict.cmdAIR);
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "Authentication-Information-Answer", aqDict.cmdAIA);
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "Update-Location-Request", aqDict.cmdULR);
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "Update-Location-Answer", aqDict.cmdULA);
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "Purge-UE-Request", aqDict.cmdPUR);
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "Purge-UE-Answer", aqDict.cmdPUA);
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "Cancel-Location-Request", aqDict.cmdCLR);
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "Cancel-Location-Answer", aqDict.cmdCLA);
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "Insert-Subscriber-Data-Request", aqDict.cmdIDR);
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "Insert-Subscriber-Data-Answer", aqDict.cmdIDA);
//   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "", &aqDict.cmd);

   /* lookup AVP's */
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Visited-PLMN-Id", aqDict.avp_Visited_PLMN_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Requested-EUTRAN-Authentication-Info", aqDict.avp_Requested_EUTRAN_Authentication_Info);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Number-Of-Requested-Vectors", aqDict.avp_Number_Of_Requested_Vectors);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Immediate-Response-Preferred", aqDict.avp_Immediate_Response_Preferred);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Re-Synchronization-Info", aqDict.avp_Re_Synchronization_Info);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Authentication-Info", aqDict.avp_Authentication_Info);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "E-UTRAN-Vector", aqDict.avp_E_UTRAN_Vector);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Item-Number", aqDict.avp_Item_Number);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "RAND", aqDict.avp_RAND);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "XRES", aqDict.avp_XRES);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "AUTN", aqDict.avp_AUTN);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "KASME", aqDict.avp_KASME);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "RAT-Type", aqDict.avp_RAT_Type);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "ULR-Flags", aqDict.avp_ULR_Flags);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "ULA-Flags", aqDict.avp_ULA_Flags);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Subscription-Data", aqDict.avp_Subscription_Data);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Subscriber-Status", aqDict.avp_Subscriber_Status);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "MSISDN", aqDict.avp_MSISDN);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "A-MSISDN", aqDict.avp_A_MSISDN);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "STN-SR", aqDict.avp_STN_SR);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "ICS-Indicator", aqDict.avp_ICS_Indicator);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Network-Access-Mode", aqDict.avp_Network_Access_Mode);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Operator-Determined-Barring", aqDict.avp_Operator_Determined_Barring);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "HPLMN-ODB", aqDict.avp_HPLMN_ODB);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Regional-Subscription-Zone-Code", aqDict.avp_Regional_Subscription_Zone_Code);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Access-Restriction-Data", aqDict.avp_Access_Restriction_Data);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "APN-OI-Replacement", aqDict.avp_APN_OI_Replacement);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "LCS-Info", aqDict.avp_LCS_Info);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "GMLC-Number", aqDict.avp_GMLC_Number);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "LCS-PrivacyException", aqDict.avp_LCS_PrivacyException);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "MO-LR", aqDict.avp_MO_LR);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SS-Code", aqDict.avp_SS_Code);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SS-Status", aqDict.avp_SS_Status);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Notification-To-UE-User", aqDict.avp_Notification_To_UE_User);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "External-Client", aqDict.avp_External_Client);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "PLMN-Client", aqDict.avp_PLMN_Client);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Service-Type", aqDict.avp_Service_Type);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "ServiceTypeIdentity", aqDict.avp_Service_Type_Identity);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Client-Identity", aqDict.avp_Client_Identity);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "GMLC-Restriction", aqDict.avp_GMLC_Restriction);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Teleservice-List", aqDict.avp_Teleservice_List);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "TS-Code", aqDict.avp_TS_Code);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Call-Barring-Info", aqDict.avp_Call_Barring_Info);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "3GPP-Charging-Characteristics", aqDict.avp_3GPP_Charging_Characteristics);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "AMBR", aqDict.avp_AMBR);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Max-Requested-Bandwidth-UL", aqDict.avp_Max_Requested_Bandwidth_UL);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Max-Requested-Bandwidth-DL", aqDict.avp_Max_Requested_Bandwidth_DL);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "APN-Configuration-Profile", aqDict.avp_APN_Configuration_Profile);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Context-Identifier", aqDict.avp_Context_Identifier);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Additional-Context-Identifier", aqDict.avp_Additional_Context_Identifier);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "All-APN-Configurations-Included-Indicator", aqDict.avp_All_APN_Configurations_Included_Indicator);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "APN-Configuration", aqDict.avp_APN_Configuration);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Served-Party-IP-Address", aqDict.avp_Served_Party_IP_Address);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "PDN-Type", aqDict.avp_PDN_Type);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Service-Selection", aqDict.avp_Service_Selection);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "EPS-Subscribed-QoS-Profile", aqDict.avp_EPS_Subscribed_QoS_Profile);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "VPLMN-Dynamic-Address-Allowed", aqDict.avp_VPLMN_Dynamic_Address_Allowed);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "MIP6-Agent-Info", aqDict.avp_MIP6_Agent_Info);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Visited-Network-Identifier", aqDict.avp_Visited_Network_Identifier);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "PDN-GW-Allocation-Type", aqDict.avp_PDN_GW_Allocation_Type);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Specific-APN-Info", aqDict.avp_Specific_APN_Info);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SIPTO-Permission", aqDict.avp_SIPTO_Permission);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "LIPA-Permission", aqDict.avp_LIPA_Permission);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Restoration-Priority", aqDict.avp_Restoration_Priority);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SIPTO-Local-Network-Permission", aqDict.avp_SIPTO_Local_Network_Permission);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "WLAN-offloadability", aqDict.avp_WLAN_offloadability);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Non-IP-PDN-Type-Indicator", aqDict.avp_Non_IP_PDN_Type_Indicator);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Non-IP-Data-Delivery-Mechanism", aqDict.avp_Non_IP_Data_Delivery_Mechanism);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SCEF-ID", aqDict.avp_SCEF_ID);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SCEF-Realm", aqDict.avp_SCEF_Realm);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Preferred-Data-Mode", aqDict.avp_Preferred_Data_Mode);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "PDN-Connection-Continuity", aqDict.avp_PDN_Connection_Continuity);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "QoS-Class-Identifier", aqDict.avp_QoS_Class_Identifier);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Allocation-Retention-Priority", aqDict.avp_Allocation_Retention_Priority);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Priority-Level", aqDict.avp_Priority_Level);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Pre-emption-Capability", aqDict.avp_Pre_emption_Capability);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Pre-emption-Vulnerability", aqDict.avp_Pre_emption_Vulnerability);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "MIP-Home-Agent-Address", aqDict.avp_MIP_Home_Agent_Address);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "MIP-Home-Agent-Host", aqDict.avp_MIP_Home_Agent_Host);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "WLAN-offloadability-EUTRAN", aqDict.avp_WLAN_offloadability_EUTRAN);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "WLAN-offloadability-UTRAN", aqDict.avp_WLAN_offloadability_UTRAN);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "RAT-Frequency-Selection-Priority-ID", aqDict.avp_RAT_Frequency_Selection_Priority_ID);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Trace-Data", aqDict.avp_Trace_Data);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Trace-Reference", aqDict.avp_Trace_Reference);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Trace-Depth", aqDict.avp_Trace_Depth);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Trace-NE-Type-List", aqDict.avp_Trace_NE_Type_List);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Trace-Interface-List", aqDict.avp_Trace_Interface_List);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Trace-Event-List", aqDict.avp_Trace_Event_List);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "OMC-Id", aqDict.avp_OMC_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Trace-Collection-Entity", aqDict.avp_Trace_Collection_Entity);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "MDT-Configuration", aqDict.avp_MDT_Configuration);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Job-Type", aqDict.avp_Job_Type);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Area-Scope", aqDict.avp_Area_Scope);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "List-Of-Measurements", aqDict.avp_List_Of_Measurements);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Reporting-Trigger", aqDict.avp_Reporting_Trigger);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Report-Interval", aqDict.avp_Report_Interval);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Report-Amount", aqDict.avp_Report_Amount);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Event-Threshold-RSRP", aqDict.avp_Event_Threshold_RSRP);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Event-Threshold-RSRQ", aqDict.avp_Event_Threshold_RSRQ);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Logging-Interval", aqDict.avp_Logging_Interval);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Logging-Duration", aqDict.avp_Logging_Duration);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Measurement-Period-LTE", aqDict.avp_Measurement_Period_LTE);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Measurement-Period-UMTS", aqDict.avp_Measurement_Period_UMTS);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Collection-Period-RRM-LTE", aqDict.avp_Collection_Period_RRM_LTE);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Collection-Period-RRM-UMTS", aqDict.avp_Collection_Period_RRM_UMTS);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Positioning-Method", aqDict.avp_Positioning_Method);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Measurement-Quantity", aqDict.avp_Measurement_Quantity);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Event-Threshold-Event-1F", aqDict.avp_Event_Threshold_Event_1F);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Event-Threshold-Event-1I", aqDict.avp_Event_Threshold_Event_1I);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "MDT-Allowed-PLMN-Id", aqDict.avp_MDT_Allowed_PLMN_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Cell-Global-Identity", aqDict.avp_Cell_Global_Identity);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "E-UTRAN-Cell-Global-Identity", aqDict.avp_E_UTRAN_Cell_Global_Identity);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Routing-Area-Identity", aqDict.avp_Routing_Area_Identity);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Location-Area-Identity", aqDict.avp_Location_Area_Identity);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Tracking-Area-Identity", aqDict.avp_Tracking_Area_Identity);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "GPRS-Subscription-Data", aqDict.avp_GPRS_Subscription_Data);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Complete-Data-List-Included-Indicator", aqDict.avp_Complete_Data_List_Included_Indicator);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "PDP-Context", aqDict.avp_PDP_Context);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "PDP-Address", aqDict.avp_PDP_Address);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "QoS-Subscribed", aqDict.avp_QoS_Subscribed);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Ext-PDP-Type", aqDict.avp_Ext_PDP_Type);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Ext-PDP-Address", aqDict.avp_Ext_PDP_Address);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "CSG-Subscription-Data", aqDict.avp_CSG_Subscription_Data);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Roaming-Restricted-Due-To-Unsupported-Feature", aqDict.avp_Roaming_Restricted_Due_To_Unsupported_Feature);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Subscribed-Periodic-RAU-TAU-Timer", aqDict.avp_Subscribed_Periodic_RAU_TAU_Timer);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "MPS-Priority", aqDict.avp_MPS_Priority);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "VPLMN-LIPA-Allowed", aqDict.avp_VPLMN_LIPA_Allowed);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Relay-Node-Indicator", aqDict.avp_Relay_Node_Indicator);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "MDT-User-Consent", aqDict.avp_MDT_User_Consent);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Subscribed-VSRVCC", aqDict.avp_Subscribed_VSRVCC);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "ProSe-Subscription-Data", aqDict.avp_ProSe_Subscription_Data);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Subscription-Data-Flags", aqDict.avp_Subscription_Data_Flags);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Adjacent-Access-Restriction-Data", aqDict.avp_Adjacent_Access_Restriction_Data);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "DL-Buffering-Suggested-Packet-Count", aqDict.avp_DL_Buffering_Suggested_Packet_Count);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "IMSI-Group-Id", aqDict.avp_IMSI_Group_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "CSG-Id", aqDict.avp_CSG_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Expiration-Date", aqDict.avp_Expiration_Date);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "ProSe-Permission", aqDict.avp_ProSe_Permission);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Group-Service-Id", aqDict.avp_Group_Service_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Group-PLMN-Id", aqDict.avp_Group_PLMN_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Local-Group-Id", aqDict.avp_Local_Group_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "UE-Usage-Type", aqDict.avp_UE_Usage_Type);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "AESE-Communication-Pattern", aqDict.avp_AESE_Communication_Pattern);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SCEF-Reference-ID", aqDict.avp_SCEF_Reference_ID);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SCEF-Reference-ID-for-Deletion", aqDict.avp_SCEF_Reference_ID_for_Deletion);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Communication-Pattern-Set", aqDict.avp_Communication_Pattern_Set);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Periodic-Communication-Indicator", aqDict.avp_Periodic_Communication_Indicator);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Communication-Duration-Time", aqDict.avp_Communication_Duration_Time);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Periodic-Time", aqDict.avp_Periodic_Time);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Scheduled-Communication-Time", aqDict.avp_Scheduled_Communication_Time);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Stationary-Indication", aqDict.avp_Stationary_Indication);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Reference-ID-Validity-Time", aqDict.avp_Reference_ID_Validity_Time);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Day-Of-Week-Mask", aqDict.avp_Day_Of_Week_Mask);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Time-Of-Day-Start", aqDict.avp_Time_Of_Day_Start);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Time-Of-Day-End", aqDict.avp_Time_Of_Day_End);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Monitoring-Event-Configuration", aqDict.avp_Monitoring_Event_Configuration);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Monitoring-Type", aqDict.avp_Monitoring_Type);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Maximum-Number-of-Reports", aqDict.avp_Maximum_Number_of_Reports);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Monitoring-Duration", aqDict.avp_Monitoring_Duration);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Charged-Party", aqDict.avp_Charged_Party);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "UE-Reachability-Configuration", aqDict.avp_UE_Reachability_Configuration);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Location-Information-Configuration", aqDict.avp_Location_Information_Configuration);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Reachability-Type", aqDict.avp_Reachability_Type);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Maximum-Response-Time", aqDict.avp_Maximum_Response_Time);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "MONTE-Location-Type", aqDict.avp_MONTE_Location_Type);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Accuracy", aqDict.avp_Accuracy);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Emergency-Info", aqDict.avp_Emergency_Info);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "V2X-Subscription-Data", aqDict.avp_V2X_Subscription_Data);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "V2X-Permission", aqDict.avp_V2X_Permission);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "UE-PC5-AMBR", aqDict.avp_UE_PC5_AMBR);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "eDRX-Cycle-Length", aqDict.avp_eDRX_Cycle_Length);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "eDRX-Cycle-Length-Value", aqDict.avp_eDRX_Cycle_Length_Value);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "PUR-Flags", aqDict.avp_PUR_Flags);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Monitoring-Event-Config-Status", aqDict.avp_Monitoring_Event_Config_Status);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Service-Report", aqDict.avp_Service_Report);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Node-Type", aqDict.avp_Node_Type);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Service-Result", aqDict.avp_Service_Result);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Service-Result-Code", aqDict.avp_Service_Result_Code);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Monitoring-Event-Report", aqDict.avp_Monitoring_Event_Report);

   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Visited_PLMN_Id, &aqDict.davp_Visited_PLMN_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Requested_EUTRAN_Authentication_Info, &aqDict.davp_Requested_EUTRAN_Authentication_Info);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Number_Of_Requested_Vectors, &aqDict.davp_Number_Of_Requested_Vectors);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Immediate_Response_Preferred, &aqDict.davp_Immediate_Response_Preferred);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Re_Synchronization_Info, &aqDict.davp_Re_Synchronization_Info);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Authentication_Info, &aqDict.davp_Authentication_Info);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_E_UTRAN_Vector, &aqDict.davp_E_UTRAN_Vector);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Item_Number, &aqDict.davp_Item_Number);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_RAND, &aqDict.davp_RAND);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_XRES, &aqDict.davp_XRES);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_AUTN, &aqDict.davp_AUTN);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_KASME, &aqDict.davp_KASME);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_RAT_Type, &aqDict.davp_RAT_Type);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_ULR_Flags, &aqDict.davp_ULR_Flags);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_ULA_Flags, &aqDict.davp_ULA_Flags);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Subscription_Data, &aqDict.davp_Subscription_Data);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Subscriber_Status, &aqDict.davp_Subscriber_Status);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_MSISDN, &aqDict.davp_MSISDN);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_A_MSISDN, &aqDict.davp_A_MSISDN);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_STN_SR, &aqDict.davp_STN_SR);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_ICS_Indicator, &aqDict.davp_ICS_Indicator);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Network_Access_Mode, &aqDict.davp_Network_Access_Mode);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Operator_Determined_Barring, &aqDict.davp_Operator_Determined_Barring);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_HPLMN_ODB, &aqDict.davp_HPLMN_ODB);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Regional_Subscription_Zone_Code, &aqDict.davp_Regional_Subscription_Zone_Code);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Access_Restriction_Data, &aqDict.davp_Access_Restriction_Data);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_APN_OI_Replacement, &aqDict.davp_APN_OI_Replacement);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_LCS_Info, &aqDict.davp_LCS_Info);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_GMLC_Number, &aqDict.davp_GMLC_Number);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_LCS_PrivacyException, &aqDict.davp_LCS_PrivacyException);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_MO_LR, &aqDict.davp_MO_LR);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SS_Code, &aqDict.davp_SS_Code);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SS_Status, &aqDict.davp_SS_Status);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Notification_To_UE_User, &aqDict.davp_Notification_To_UE_User);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_External_Client, &aqDict.davp_External_Client);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_PLMN_Client, &aqDict.davp_PLMN_Client);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Service_Type, &aqDict.davp_Service_Type);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Service_Type_Identity, &aqDict.davp_Service_Type_Identity);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Client_Identity, &aqDict.davp_Client_Identity);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_GMLC_Restriction, &aqDict.davp_GMLC_Restriction);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Teleservice_List, &aqDict.davp_Teleservice_List);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_TS_Code, &aqDict.davp_TS_Code);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Call_Barring_Info, &aqDict.davp_Call_Barring_Info);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_3GPP_Charging_Characteristics, &aqDict.davp_3GPP_Charging_Characteristics);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_AMBR, &aqDict.davp_AMBR);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Max_Requested_Bandwidth_UL, &aqDict.davp_Max_Requested_Bandwidth_UL);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Max_Requested_Bandwidth_DL, &aqDict.davp_Max_Requested_Bandwidth_DL);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_APN_Configuration_Profile, &aqDict.davp_APN_Configuration_Profile);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Context_Identifier, &aqDict.davp_Context_Identifier);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Additional_Context_Identifier, &aqDict.davp_Additional_Context_Identifier);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_All_APN_Configurations_Included_Indicator, &aqDict.davp_All_APN_Configurations_Included_Indicator);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_APN_Configuration, &aqDict.davp_APN_Configuration);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Served_Party_IP_Address, &aqDict.davp_Served_Party_IP_Address);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_PDN_Type, &aqDict.davp_PDN_Type);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Service_Selection, &aqDict.davp_Service_Selection);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_EPS_Subscribed_QoS_Profile, &aqDict.davp_EPS_Subscribed_QoS_Profile);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_VPLMN_Dynamic_Address_Allowed, &aqDict.davp_VPLMN_Dynamic_Address_Allowed);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_MIP6_Agent_Info, &aqDict.davp_MIP6_Agent_Info);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Visited_Network_Identifier, &aqDict.davp_Visited_Network_Identifier);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_PDN_GW_Allocation_Type, &aqDict.davp_PDN_GW_Allocation_Type);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Specific_APN_Info, &aqDict.davp_Specific_APN_Info);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SIPTO_Permission, &aqDict.davp_SIPTO_Permission);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_LIPA_Permission, &aqDict.davp_LIPA_Permission);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Restoration_Priority, &aqDict.davp_Restoration_Priority);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SIPTO_Local_Network_Permission, &aqDict.davp_SIPTO_Local_Network_Permission);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_WLAN_offloadability, &aqDict.davp_WLAN_offloadability);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Non_IP_PDN_Type_Indicator, &aqDict.davp_Non_IP_PDN_Type_Indicator);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Non_IP_Data_Delivery_Mechanism, &aqDict.davp_Non_IP_Data_Delivery_Mechanism);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SCEF_ID, &aqDict.davp_SCEF_ID);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SCEF_Realm, &aqDict.davp_SCEF_Realm);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Preferred_Data_Mode, &aqDict.davp_Preferred_Data_Mode);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_PDN_Connection_Continuity, &aqDict.davp_PDN_Connection_Continuity);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_QoS_Class_Identifier, &aqDict.davp_QoS_Class_Identifier);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Allocation_Retention_Priority, &aqDict.davp_Allocation_Retention_Priority);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Priority_Level, &aqDict.davp_Priority_Level);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Pre_emption_Capability, &aqDict.davp_Pre_emption_Capability);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Pre_emption_Vulnerability, &aqDict.davp_Pre_emption_Vulnerability);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_MIP_Home_Agent_Address, &aqDict.davp_MIP_Home_Agent_Address);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_MIP_Home_Agent_Host, &aqDict.davp_MIP_Home_Agent_Host);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_WLAN_offloadability_EUTRAN, &aqDict.davp_WLAN_offloadability_EUTRAN);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_WLAN_offloadability_UTRAN, &aqDict.davp_WLAN_offloadability_UTRAN);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_RAT_Frequency_Selection_Priority_ID, &aqDict.davp_RAT_Frequency_Selection_Priority_ID);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Trace_Data, &aqDict.davp_Trace_Data);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Trace_Reference, &aqDict.davp_Trace_Reference);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Trace_Depth, &aqDict.davp_Trace_Depth);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Trace_NE_Type_List, &aqDict.davp_Trace_NE_Type_List);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Trace_Interface_List, &aqDict.davp_Trace_Interface_List);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Trace_Event_List, &aqDict.davp_Trace_Event_List);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_OMC_Id, &aqDict.davp_OMC_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Trace_Collection_Entity, &aqDict.davp_Trace_Collection_Entity);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_MDT_Configuration, &aqDict.davp_MDT_Configuration);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Job_Type, &aqDict.davp_Job_Type);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Area_Scope, &aqDict.davp_Area_Scope);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_List_Of_Measurements, &aqDict.davp_List_Of_Measurements);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Reporting_Trigger, &aqDict.davp_Reporting_Trigger);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Report_Interval, &aqDict.davp_Report_Interval);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Report_Amount, &aqDict.davp_Report_Amount);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Event_Threshold_RSRP, &aqDict.davp_Event_Threshold_RSRP);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Event_Threshold_RSRQ, &aqDict.davp_Event_Threshold_RSRQ);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Logging_Interval, &aqDict.davp_Logging_Interval);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Logging_Duration, &aqDict.davp_Logging_Duration);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Measurement_Period_LTE, &aqDict.davp_Measurement_Period_LTE);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Measurement_Period_UMTS, &aqDict.davp_Measurement_Period_UMTS);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Collection_Period_RRM_LTE, &aqDict.davp_Collection_Period_RRM_LTE);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Collection_Period_RRM_UMTS, &aqDict.davp_Collection_Period_RRM_UMTS);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Positioning_Method, &aqDict.davp_Positioning_Method);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Measurement_Quantity, &aqDict.davp_Measurement_Quantity);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Event_Threshold_Event_1F, &aqDict.davp_Event_Threshold_Event_1F);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Event_Threshold_Event_1I, &aqDict.davp_Event_Threshold_Event_1I);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_MDT_Allowed_PLMN_Id, &aqDict.davp_MDT_Allowed_PLMN_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Cell_Global_Identity, &aqDict.davp_Cell_Global_Identity);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_E_UTRAN_Cell_Global_Identity, &aqDict.davp_E_UTRAN_Cell_Global_Identity);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Routing_Area_Identity, &aqDict.davp_Routing_Area_Identity);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Location_Area_Identity, &aqDict.davp_Location_Area_Identity);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Tracking_Area_Identity, &aqDict.davp_Tracking_Area_Identity);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_GPRS_Subscription_Data, &aqDict.davp_GPRS_Subscription_Data);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Complete_Data_List_Included_Indicator, &aqDict.davp_Complete_Data_List_Included_Indicator);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_PDP_Context, &aqDict.davp_PDP_Context);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_PDP_Address, &aqDict.davp_PDP_Address);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_QoS_Subscribed, &aqDict.davp_QoS_Subscribed);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Ext_PDP_Type, &aqDict.davp_Ext_PDP_Type);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Ext_PDP_Address, &aqDict.davp_Ext_PDP_Address);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_CSG_Subscription_Data, &aqDict.davp_CSG_Subscription_Data);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Roaming_Restricted_Due_To_Unsupported_Feature, &aqDict.davp_Roaming_Restricted_Due_To_Unsupported_Feature);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Subscribed_Periodic_RAU_TAU_Timer, &aqDict.davp_Subscribed_Periodic_RAU_TAU_Timer);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_MPS_Priority, &aqDict.davp_MPS_Priority);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_VPLMN_LIPA_Allowed, &aqDict.davp_VPLMN_LIPA_Allowed);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Relay_Node_Indicator, &aqDict.davp_Relay_Node_Indicator);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_MDT_User_Consent, &aqDict.davp_MDT_User_Consent);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Subscribed_VSRVCC, &aqDict.davp_Subscribed_VSRVCC);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_ProSe_Subscription_Data, &aqDict.davp_ProSe_Subscription_Data);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Subscription_Data_Flags, &aqDict.davp_Subscription_Data_Flags);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Adjacent_Access_Restriction_Data, &aqDict.davp_Adjacent_Access_Restriction_Data);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_DL_Buffering_Suggested_Packet_Count, &aqDict.davp_DL_Buffering_Suggested_Packet_Count);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_IMSI_Group_Id, &aqDict.davp_IMSI_Group_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_CSG_Id, &aqDict.davp_CSG_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Expiration_Date, &aqDict.davp_Expiration_Date);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_ProSe_Permission, &aqDict.davp_ProSe_Permission);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Group_Service_Id, &aqDict.davp_Group_Service_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Group_PLMN_Id, &aqDict.davp_Group_PLMN_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Local_Group_Id, &aqDict.davp_Local_Group_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_UE_Usage_Type, &aqDict.davp_UE_Usage_Type);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_AESE_Communication_Pattern, &aqDict.davp_AESE_Communication_Pattern);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SCEF_Reference_ID, &aqDict.davp_SCEF_Reference_ID);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SCEF_Reference_ID_for_Deletion, &aqDict.davp_SCEF_Reference_ID_for_Deletion);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Communication_Pattern_Set, &aqDict.davp_Communication_Pattern_Set);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Periodic_Communication_Indicator, &aqDict.davp_Periodic_Communication_Indicator);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Communication_Duration_Time, &aqDict.davp_Communication_Duration_Time);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Periodic_Time, &aqDict.davp_Periodic_Time);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Scheduled_Communication_Time, &aqDict.davp_Scheduled_Communication_Time);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Stationary_Indication, &aqDict.davp_Stationary_Indication);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Reference_ID_Validity_Time, &aqDict.davp_Reference_ID_Validity_Time);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Day_Of_Week_Mask, &aqDict.davp_Day_Of_Week_Mask);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Time_Of_Day_Start, &aqDict.davp_Time_Of_Day_Start);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Time_Of_Day_End, &aqDict.davp_Time_Of_Day_End);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Monitoring_Event_Configuration, &aqDict.davp_Monitoring_Event_Configuration);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Monitoring_Type, &aqDict.davp_Monitoring_Type);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Maximum_Number_of_Reports, &aqDict.davp_Maximum_Number_of_Reports);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Monitoring_Duration, &aqDict.davp_Monitoring_Duration);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Charged_Party, &aqDict.davp_Charged_Party);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_UE_Reachability_Configuration, &aqDict.davp_UE_Reachability_Configuration);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Location_Information_Configuration, &aqDict.davp_Location_Information_Configuration);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Reachability_Type, &aqDict.davp_Reachability_Type);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Maximum_Response_Time, &aqDict.davp_Maximum_Response_Time);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_MONTE_Location_Type, &aqDict.davp_MONTE_Location_Type);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Accuracy, &aqDict.davp_Accuracy);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Emergency_Info, &aqDict.davp_Emergency_Info);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_V2X_Subscription_Data, &aqDict.davp_V2X_Subscription_Data);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_V2X_Permission, &aqDict.davp_V2X_Permission);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_UE_PC5_AMBR, &aqDict.davp_UE_PC5_AMBR);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_eDRX_Cycle_Length, &aqDict.davp_eDRX_Cycle_Length);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_eDRX_Cycle_Length_Value, &aqDict.davp_eDRX_Cycle_Length_Value);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_PUR_Flags, &aqDict.davp_PUR_Flags);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Monitoring_Event_Config_Status, &aqDict.davp_Monitoring_Event_Config_Status);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Service_Report, &aqDict.davp_Service_Report);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Node_Type, &aqDict.davp_Node_Type);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Service_Result, &aqDict.davp_Service_Result);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Service_Result_Code, &aqDict.davp_Service_Result_Code);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Monitoring_Event_Report, &aqDict.davp_Monitoring_Event_Report);
//   AQCHECK_FCT_DICT_GETVAL(aqDict., &aqDict.d);

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdT6aInit
*
*       Desc:   Lookup freeDiameter dictionary elements for T6A
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*               LAQ_REASON_FD_DICT_SEARCH           - failure
*
*       Notes:  None
*
*       File:   aqfd_init.c
*
*/
#ifdef ANSI
PUBLIC S16 aqfdT6aInit
(
Void
)
#else
PUBLIC S16 aqfdT6aInit(Void)
#endif
{

   /* vendor_id_t vnd_3gpp = 10415; */
   application_id_t app_t6a = 16777346;

   TRC2(aqfdT6aInit);

   /* lookup T6A application */
   AQCHECK_FCT_DICT(DICT_APPLICATION, APPLICATION_BY_ID, &app_t6a, aqDict.appT6A);
   AQCHECK_FCT_DICT_GETVAL(aqDict.appT6A, &aqDict.appT6Adata);

   /* lookup commands */
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "Reporting-Information-Request", aqDict.cmdRIR);
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "Reporting-Information-Answer", aqDict.cmdRIA);
//   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "", &aqDict.cmd);

   /* lookup AVP's */
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Reachability-Information", aqDict.avp_Reachability_Information);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Maximum-UE-Availability-Time", aqDict.avp_Maximum_UE_Availability_Time);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "EPS-Location-Information", aqDict.avp_EPS_Location_Information);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Event-Handling", aqDict.avp_Event_Handling);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Loss-Of-Connectivity-Reason", aqDict.avp_Loss_Of_Connectivity_Reason);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "MME-Location-Information", aqDict.avp_MME_Location_Information);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SGSN-Location-Information", aqDict.avp_SGSN_Location_Information);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Geographical-Information", aqDict.avp_Geographical_Information);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Geodetic-Information", aqDict.avp_Geodetic_Information);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Current-Location-Retrieved", aqDict.avp_Current_Location_Retrieved);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Age-Of-Location-Information", aqDict.avp_Age_Of_Location_Information);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "User-CSG-Information", aqDict.avp_User_CSG_Information);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "eNodeB-Id", aqDict.avp_eNodeB_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Extended-eNodeB-Id", aqDict.avp_Extended_eNodeB_Id);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Service-Area-Identity", aqDict.avp_Service_Area_Identity);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "CSG-Access-Mode", aqDict.avp_CSG_Access_Mode);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "CSG-Membership-Indication", aqDict.avp_CSG_Membership_Indication);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Cause-Type", aqDict.avp_Cause_Type);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "S1AP-Cause", aqDict.avp_S1AP_Cause);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "RANAP-Cause", aqDict.avp_RANAP_Cause);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "BSSGP-Cause", aqDict.avp_BSSGP_Cause);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "GMM-Cause", aqDict.avp_GMM_Cause);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SM-Cause", aqDict.avp_SM_Cause);
//   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "", aqDict.avp_);

   /* lookup AVP data */
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Reachability_Information, &aqDict.davp_Reachability_Information);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Maximum_UE_Availability_Time, &aqDict.davp_Maximum_UE_Availability_Time);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_EPS_Location_Information, &aqDict.davp_EPS_Location_Information);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Event_Handling, &aqDict.davp_Event_Handling);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Loss_Of_Connectivity_Reason, &aqDict.davp_Loss_Of_Connectivity_Reason);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_MME_Location_Information, &aqDict.davp_MME_Location_Information);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SGSN_Location_Information, &aqDict.davp_SGSN_Location_Information);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Geographical_Information, &aqDict.davp_Geographical_Information);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Geodetic_Information, &aqDict.davp_Geodetic_Information);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Current_Location_Retrieved, &aqDict.davp_Current_Location_Retrieved);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Age_Of_Location_Information, &aqDict.davp_Age_Of_Location_Information);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_User_CSG_Information, &aqDict.davp_User_CSG_Information);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_eNodeB_Id, &aqDict.davp_eNodeB_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Extended_eNodeB_Id, &aqDict.davp_Extended_eNodeB_Id);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Service_Area_Identity, &aqDict.davp_Service_Area_Identity);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_CSG_Access_Mode, &aqDict.davp_CSG_Access_Mode);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_CSG_Membership_Indication, &aqDict.davp_CSG_Membership_Indication);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Cause_Type, &aqDict.davp_Cause_Type);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_S1AP_Cause, &aqDict.davp_S1AP_Cause);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_RANAP_Cause, &aqDict.davp_RANAP_Cause);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_BSSGP_Cause, &aqDict.davp_BSSGP_Cause);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_GMM_Cause, &aqDict.davp_GMM_Cause);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SM_Cause, &aqDict.davp_SM_Cause);
//   AQCHECK_FCT_DICT_GETVAL(aqDict., &aqDict.d);

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdSGdInit
*
*       Desc:   Lookup freeDiameter dictionary elements for SGD
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*               LAQ_REASON_FD_DICT_SEARCH           - failure
*
*       Notes:  None
*
*       File:   aqfd_init.c
*
*/
#ifdef ANSI
PUBLIC S16 aqfdSGdInit
(
Void
)
#else
PUBLIC S16 aqfdSGdInit(Void)
#endif
{

   /* vendor_id_t vnd_3gpp = 10415; */
   application_id_t app_sgd = 16777313;

   TRC2(aqfdSGdInit);

   /* lookup SGD application */
   AQCHECK_FCT_DICT(DICT_APPLICATION, APPLICATION_BY_ID, &app_sgd, aqDict.appSGD);
   AQCHECK_FCT_DICT_GETVAL(aqDict.appSGD, &aqDict.appSGDdata);

   /* lookup commands */
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "MO-Forward-Short-Message-Request", aqDict.cmdOFR);
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "MO-Forward-Short-Message-Answer", aqDict.cmdOFA);
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "MT-Forward-Short-Message-Request", aqDict.cmdTFR);
   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "MT-Forward-Short-Message-Answer", aqDict.cmdTFA);
//   AQCHECK_FCT_DICT(DICT_COMMAND, CMD_BY_NAME, "", &aqDict.cmd);

   /* lookup AVP's */
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SC-Address", aqDict.avp_SC_Address);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "User-Identifier", aqDict.avp_User_Identifier);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SM-RP-UI", aqDict.avp_SM_RP_UI);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SMSMI-Correlation-ID", aqDict.avp_SMSMI_Correlation_ID);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "OFR-Flags", aqDict.avp_OFR_Flags);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SM-Delivery-Outcome", aqDict.avp_SM_Delivery_Outcome);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "Supported-Features", aqDict.avp_Supported_Features);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SM-Delivery-Failure-Cause", aqDict.avp_SM_Delivery_Failure_Cause);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SM-Enumerated-Delivery-Failure-Cause", aqDict.avp_SM_Enumerated_Delivery_Failure_Cause);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "SM-Diagnostic-Info", aqDict.avp_SM_Diagnostic_Info);
   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "External-Identifier", aqDict.avp_External_Identifier);
//   AQCHECK_FCT_DICT(DICT_AVP, AVP_BY_NAME_ALL_VENDORS, "", aqDict.avp_);

   /* lookup AVP data */
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SC_Address, &aqDict.davp_SC_Address);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_User_Identifier, &aqDict.davp_User_Identifier);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SM_RP_UI, &aqDict.davp_SM_RP_UI);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SMSMI_Correlation_ID, &aqDict.davp_SMSMI_Correlation_ID);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_OFR_Flags, &aqDict.davp_OFR_Flags);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SM_Delivery_Outcome, &aqDict.davp_SM_Delivery_Outcome);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_Supported_Features, &aqDict.davp_Supported_Features);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SM_Delivery_Failure_Cause, &aqDict.davp_SM_Delivery_Failure_Cause);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SM_Enumerated_Delivery_Failure_Cause, &aqDict.davp_SM_Enumerated_Delivery_Failure_Cause);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_SM_Diagnostic_Info, &aqDict.davp_SM_Diagnostic_Info);
   AQCHECK_FCT_DICT_GETVAL(aqDict.avp_External_Identifier, &aqDict.davp_External_Identifier);
//   AQCHECK_FCT_DICT_GETVAL(aqDict., &aqDict.d);

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdRegister
*
*       Desc:   Register Diameter applications with freeDiameter
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*               LAQ_REASON_FD_REGISTER_CALLBACK     - failure
*               LAQ_REASON_FD_REGISTER_APPLICATION  - failure
*
*       Notes:  None
*
*       File:   aqfd_init.c
*
*/
#ifdef ANSI
PUBLIC S16 aqfdRegister
(
Void
)
#else
PUBLIC S16 aqfdRegister(Void)
#endif
{
   struct disp_when data;

   /* Register the S6A dispatch callbacks */
   AQ_ZERO(&data, sizeof(data));
   data.app = aqDict.appS6A;

   AQCHECK_FCT_REGISTER_CALLBACK( aqfd_aia_cb, aqDict.cmdAIA );
   AQCHECK_FCT_REGISTER_CALLBACK( aqfd_ula_cb, aqDict.cmdULA );
   AQCHECK_FCT_REGISTER_CALLBACK( aqfd_pua_cb, aqDict.cmdPUA ); //rakesh
   AQCHECK_FCT_REGISTER_CALLBACK( aqfd_idr_cb, aqDict.cmdIDR );

   /* Register the T6A dispatch callbacks */
   AQ_ZERO(&data, sizeof(data));
   data.app = aqDict.appT6A;

   AQCHECK_FCT_REGISTER_CALLBACK( aqfd_ria_cb, aqDict.cmdRIA );

   /* Register the SGD dispatch callbacks */
   AQ_ZERO(&data, sizeof(data));
   data.app = aqDict.appSGD;

   AQCHECK_FCT_REGISTER_CALLBACK( aqfd_ofa_cb, aqDict.cmdOFA );
   AQCHECK_FCT_REGISTER_CALLBACK( aqfd_tfr_cb, aqDict.cmdTFR );

   /* Register application support */
   AQCHECK_FCT_REGISTER_APPLICATION( aqDict.appS6A, aqDict.vnd3GPP );
   AQCHECK_FCT_REGISTER_APPLICATION( aqDict.appT6A, aqDict.vnd3GPP );
   AQCHECK_FCT_REGISTER_APPLICATION( aqDict.appSGD, aqDict.vnd3GPP );

   return LCM_REASON_NOT_APPL;
}

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision History:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

