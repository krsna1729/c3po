/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:    Trillium LTE CNE - MME Controller Module

     Type:    C Include file

     Desc:    This file contains the vb application source code

     File:    vbsm_dscfg.c

     Sid:

     Prg:     bw
*********************************************************************21*/

/* Header include files (.h) */
#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "cm_os.h"

#include "sct.h"           /* SCT interface defines           */

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
/* #include "sz_err.h"*/        /* S1AP Error */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "cm_os.x"

#include "sct.x"           /* SCT interface defines           */

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

/*
#include "sy.h"
#include "sy.x"
#include "sy_err.h"
*/

#include "lds.h"
#include "lds.x"
#include "ds_err.h"
#include "ds.h"
#include "ds.x"

#ifdef __cplusplus
EXTERN "C" {
#endif /* __cplusplus */

/* #define VB_SM_HI_CONFIGURED  (STGEN | STTSAP) */

#define VB_SM_DS_CONFIGURED (STGEN)

U8      vbSmDsCfg = 0;

/* Function prototypes */
PRIVATE Void vbMmeLdsDsGenCfg ARGS ((Void));

/*
 *
 *       Fun:    vbMmeDsCfg - configure DS
 *
 *       Desc:
 *
 *       Ret:    Void
 *
 *       Notes:  None
 *
 *       File:   vbsm_dscfg.c
 *
 */

#ifdef ANSI
PUBLIC Void vbMmeDsCfg
(
Void
)
#else
PUBLIC Void vbMmeDsCfg()
#endif /* ANSI */
{
   SM_TRC2(vbMmeDsCfg);

   VBSM_DBG_INFO((VBSM_PRNTBUF,"Sending MME DsCfg...\n"));
   vbSmCb.dsPst.event = EVTLDSCFGREQ;

   vbMmeLdsDsGenCfg();

   RETVOID;
}
/*
 *
 *       Fun:    vbMmeLdsDsGenCfg - fill in default genCfg for SY
 *
 *       Desc:
 *
 *       Ret:    Void
 *
 *       Notes:  None
 *
 *       File:   vbsm_dscfg.c
 *
 */

#ifdef ANSI
PRIVATE Void vbMmeLdsDsGenCfg
(
Void
)
#else
PRIVATE Void vbMmeLdsDsGenCfg()
#endif /* ANSI */
{
   DsMngmt     dsMgt;
   DsGenCfg    *cfg;

   SM_TRC2(vbMmeLdsDsGenCfg);

   cmMemset((U8 *)&dsMgt, 0, sizeof(DsMngmt));
   vbSmDefHdr(&dsMgt.hdr, ENTDS, STGEN, VBSM_DSSMSEL);

   cfg = &dsMgt.t.cfg.s.dsGen;

#if 0
   cfg->lmPst.srcProcId = SFndProcId();
   cfg->lmPst.dstProcId = SFndProcId();
   cfg->lmPst.srcEnt = (Ent)ENTDS;
   cfg->lmPst.dstEnt = (Ent)ENTSM;
   cfg->lmPst.srcInst = (Inst)0;
   cfg->lmPst.dstInst = (Inst)0;

   cfg->lmPst.prior = (Prior)VBSM_MSGPRIOR;
   cfg->lmPst.route = (Route)RTESPEC;
   cfg->lmPst.event = (Event)EVTNONE;
   cfg->lmPst.region = (Region)vbSmCb.init.region;
   cfg->lmPst.pool = (Pool)vbSmCb.init.pool;
   cfg->lmPst.selector = (Selector)VBSM_DSSMSEL;
#endif

   (Void)SmMiLdsCfgReq(&vbSmCb.dsPst, &dsMgt);
   VBSM_DBG_INFO((VBSM_PRNTBUF,"Sent MME dsGenCfg...\n"));
   RETVOID;
} /* end of vbMmeLdsDsGenCfg() */

/*
 *      FUN:   vbMmeDsShutDwn
 *
 *      Desc:  Brings the DS to the state before configuration
 *
 *      Ret:   void
 *
 *      Notes: None
 *
 *      File:  vbsm_dscfg.c
 *
 *
 */
#ifdef ANSI
PUBLIC S16 vbMmeDsShutDwn
(
Void
)
#else
PUBLIC S16 vbMmeDsShutDwn()
#endif /* ANSI */
{
   DsMngmt              dsMgt;
   S16                  ret = ROK;

   SM_TRC2(vbMmeDsShutDwn);

   cmMemset((U8 *)&dsMgt, 0, sizeof(DsMngmt));
   vbSmDefHdr(&dsMgt.hdr, ENTDS, STGEN, VBSM_SYSMSEL);

   dsMgt.t.cntrl.action = ASHUTDOWN;
   dsMgt.t.cntrl.subAction = SAELMNT;

   vbSmCb.dsPst.event = EVTLDSCNTRLREQ;
   (Void)SmMiLdsCntrlReq(&vbSmCb.dsPst, &dsMgt);
   VBSM_DBG_INFO((VBSM_PRNTBUF,"Sent MME ds Shutdown CntrlReq...\n"));

   RETVALUE(ret);
}

/*
*
*       Fun:   Configuration Confirm
*
*       Desc:  This function is used by Layer to present configuration confirm
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_dscfg.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLdsCfgCfm
(
Pst     *pst,          /* post structure */
DsMngmt *cfm           /* configuration */
)
#else
PUBLIC S16 SmMiLdsCfgCfm(pst, cfm)
Pst     *pst;          /* post structure */
DsMngmt *cfm;          /* configuration */
#endif
{
   SM_TRC2(SmMiLdsCfgCfm);

   VBSM_DBG_INFO((VBSM_PRNTBUF,"Received MME ds CfgCfm with elmnt(%d) - status(%d)...\n",cfm->hdr.elmId.elmnt,cfm->cfm.status));
   if (cfm->cfm.status == LCM_PRIM_OK)
   {
      vbSmDsCfg |=  cfm->hdr.elmId.elmnt;
      switch (cfm->hdr.elmId.elmnt)
      {
         case STGEN:
         {
            vbSmDsCfg |=  cfm->hdr.elmId.elmnt;
            VBSM_DBG_INFO((VBSM_PRNTBUF,"Received MME ds Gen CfgCfm...\n"));
            break;
         }
         default:
            VBSM_DBG_ERROR((VBSM_PRNTBUF,"Invalid elemt(%d)...\n",cfm->hdr.elmId.elmnt));
        break;
      }

      if (vbSmDsCfg == VB_SM_DS_CONFIGURED)
      {
         VBSM_DBG_INFO((VBSM_PRNTBUF,"SmMiLdsCfgCfm: VB_SM_DS_CONFIGURED\n"));
         vbMmeSendMsg(EVTVBEPCDNSCFGDONE);
      }
      else
      {
           VBSM_DBG_INFO((VBSM_PRNTBUF,"Received MME CfgCfm Pending...\n"));
      }
   }
   else
   {
      VBSM_DBG_INFO((VBSM_PRNTBUF,"Received MME CfgCfm .NOT OK..\n"));
   }

   RETVALUE(ROK);
} /* end of SmMiLdsCfgCfm */



/*
*
*       Fun:   Control Confirm
*
*       Desc:  This function is used by  Layer to present control confirm
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_dscfg.c
*
*/
#ifdef ANSI
PUBLIC S16 SmMiLdsCntrlCfm
(
Pst     *pst,          /* post structure */
DsMngmt *cfm           /* control */
)
#else
PUBLIC S16 SmMiLdsCntrlCfm(pst, cfm)
Pst     *pst;          /* post structure */
DsMngmt *cfm;          /* control */
#endif
{
   SM_TRC2(SmMiLdsCntrlCfm)

   if (cfm->cfm.status == LCM_PRIM_OK)
   {
      VBSM_DBG_INFO((VBSM_PRNTBUF,"Received control confirmation OK from EPC DNS...\n"));
   }
   else
   {
      VBSM_DBG_INFO((VBSM_PRNTBUF,"Received control confirmation NOT OK from EPC DNS...\n"));
   }
   RETVALUE(ROK);
} /* end of SmMiLdsCntrlCfm */

/*
*
*       Fun:   Status Indication
*
*       Desc:  This function is used by Layer to present  unsolicited status
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_dscfg.c
*
*/
#ifdef ANSI
PUBLIC S16 SmMiLdsStaInd
(
Pst     *pst,           /* post structure */
DsMngmt *usta           /* unsolicited status */
)
#else
PUBLIC S16 SmMiLdsStaInd(pst, usta)
Pst     *pst;           /* post structure */
DsMngmt *usta;          /* unsolicited status */
#endif
{
   SM_TRC2(SmMiLdsStaInd);

   UNUSED(pst);
   UNUSED(usta);

   RETVALUE(ROK);
} /* end of SmMiLdsStaInd */

/*
*
*       Fun:   Trace Indication
*
*       Desc:  This function is used by  Layer to present trace
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_dscfg.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLdsTrcInd
(
Pst *pst,               /* post structure */
DsMngmt *trc,           /* trace */
Buffer *mBuf            /* message buffer */
)
#else
PUBLIC S16 SmMiLdsTrcInd(pst, trc, mBuf)
Pst *pst;               /* post structure */
DsMngmt *trc;           /* trace */
Buffer *mBuf;           /* message buffer */
#endif
{
   SM_TRC2(SmMiLdsTrcInd);

   UNUSED(pst);
   UNUSED(trc);

   RETVALUE(ROK);
} /* end of SmMiLdsTrcInd */

/*
*
*       Fun:   Statistics Confirm
*
*       Desc:  This function is used by Layer to present solicited statistics
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_dscfg.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLdsStsCfm
(
Pst       *pst,         /* post structure */
DsMngmt   *sts          /* confirmed statistics */
)
#else
PUBLIC S16 SmMiLdsStsCfm(pst, sts)
Pst       *pst;         /* post structure */
DsMngmt   *sts;         /* confirmed statistics */
#endif
{
   SM_TRC2(SmMiLdsStsCfm);

   UNUSED(pst);
   UNUSED(sts);

   RETVALUE(ROK);
} /* end of SmMiLdsStsCfm */

/*
*
*       Fun:   Status Confirm
*
*       Desc:  This function is used by Layer to present solicited status
*              information to Layer Management.
*
*       Ret:   None
*
*       Notes: None
*
*       File:  vbsm_dscfg.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLdsStaCfm
(
Pst     *pst,           /* post structure */
DsMngmt *sta             /* confirmed status */
)
#else
PUBLIC S16 SmMiLdsStaCfm(pst, sta)
Pst     *pst;           /* post structure */
DsMngmt *sta;            /* confirmed status */
#endif
{
   SM_TRC2(SmMiLdsStaCfm);

   UNUSED(pst);
   UNUSED(sta);

   RETVALUE(ROK);
} /* end of SmMiLdsStaCfm */

#ifdef __cplusplus
}
#endif /* __cplusplus */

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************60**
        Revision history:
*********************************************************************61*/


/********************************************************************90**

     ver       pat    init                  description
------------ -------- ---- ----------------------------------------------
/main/1      ---     bw         1. Initial version.
***********************************************************************/
