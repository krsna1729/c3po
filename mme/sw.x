/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

/********************************************************************20**

     Name:     Diameter T6A Interface Convergence Layer

     Type:     C include file

     Desc:     Structures, variables, types and prototypes.

     File:     sw.x

     Sid:

     Prg:      bw

*********************************************************************21*/

#ifndef __SWX__
#define __SWX__

#ifdef __cplusplus
extern "C" {
#endif

typedef struct swRIAInfo
{
   S8 imsi[15 + 1];
   S8 scef_id[128];
   U32 scef_reference_id;
   U32 monitoring_type;
   U32 result_code;
   AqExperimentalResult exp_result;
} SwRIAInfo;

/* Reception statistics.
 */
typedef struct swRxSts
{
   StsCntr      numRxMsg;
} SwRxSts;


/* Transmission statistics.
 */
typedef struct swTxSts
{
   StsCntr      numTxMsg;
} SwTxSts;


/* Error statistics.
 */
typedef struct swErrSts
{
   StsCntr      t6aErr;
} SwErrSts;

/* Layer manager control requests can be pending on a SAP when
 * communication with the SW threads are required before a confirm
 * can be sent.
 */
typedef struct swPendOp
{
   Bool         flag;           /* operation pending? */
   Header       hdr;            /* control request header */
   U8           action;         /*    "       "    action */
   Elmnt        elmnt;          /* STTSAP or STGRTSAP */
   U16          numRem;         /* for ASHUTDOWN/STGRTSAP requests */
   Pst          lmPst;          /* for ASHUTDOWN response */
} SwPendOp;

#ifdef SW_MULTI_THREADED

/* Upper interface primitives can be invoked by multiple threads.
 * Each primitive must therefore have its own Pst.
 */
typedef struct swUiPsts
{
   Pst          uiConCfmPst;
   Pst          uiConIndPst;
   Pst          uiFlcIndPst;
   Pst          uiDatIndPst;
   Pst          uiUDatIndPst;
   Pst          uiDiscIndPst;
   Pst          uiDiscCfmPst;

} SwUiPsts;

#endif /* SW_MULTI_THREADED */

/* SW layer control block.
 */
typedef struct _swCb
{
   TskInit      init;           /* task initialization structure */

   SwGenCfg     cfg;            /* general configuration */
   SwErrSts     errSts;         /* general error statistics */
   SwRxSts      rxSts;
   SwTxSts      txSts;

   SwPendOp     pendOp;         /* control request pending */

   Pst          uzPst;
} SwCb;

EXTERN SwCb swCb;

/* Message to a group thread.
 */
typedef struct swThrMsg
{
   S16          type;           /* message type */
   SpId         spId;           /* for the relevant SAP */
   UConnId      spConId;        /* for the relevant connection */
   union
   {
      Reason    reason;         /* for indications */
      Action    action;         /* for confirms */
   } disc;
} SwThrMsg;

/* management related */
EXTERN Void swSendLmCfm         ARGS((Pst *pst, U8 cfmType,
                                      Header *hdr, U16 status,
                                      U16 reason, SwMngmt *cfm));
EXTERN S16  swGetGenSts         ARGS((SwGenSts *genSts));
EXTERN S16  swZeroGenSts        ARGS((Void));
EXTERN S16  swCntrlGen          ARGS((Pst *pst, SwMngmt *cntrl, Header *hdr));
EXTERN Void swSendAlarm         ARGS((U16 cgy, U16 evnt, U16 cause,
                                      SwAlarmInfo *info));

EXTERN S16  swGetSid            ARGS((SystemId *sid));

/******************************************************************************/
/******************************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* __SWX__ */

/********************************************************************30**

         End of file:     sw.x@@/main/6 - Mon Mar  3 20:09:47 2008

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

