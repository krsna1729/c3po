/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter Convergence Layer

     Type:     C source file

     Desc:     freeDiameter initialization.

     File:     aqfd_of.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */
#include "cm_os.h"

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */
#include "cm_os.x"

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"

#include "lsx.h"
#include "lsx.x"
#include "lsy.h"
#include "lsy.x"

#include "aqfd.h"
#include "aqfd.x"

#include "sx.h"
#include "sx.x"
#include "sx_err.h"

#include "sy.h"
#include "sy.x"
#include "sy_err.h"

PRIVATE Void _aqfd_ofa_cb ARGS((void * data, struct msg ** msg));

/*
 *  Thread local Pst related variables and init routine
 */
static __thread int _pstInitialized;
static __thread Pst _pst;

PRIVATE Void _initPst ARGS((void));

PRIVATE Void _initPst()
{
   if (!_pstInitialized)
   {
      memcpy( &_pst, &aqCb.cfg.sxPst, sizeof(_pst) );
      _pstInitialized = 1;
   }
}

/*
*
*       Fun:    aqfdSendOFR
*
*       Desc:   Send MO-Forward-Short-Message-Request to the SMS router.
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*
*       Notes:  None
*
*       File:   aqfd_of.c
*
*      The Update-Location-Request (ULR) command, indicated by the Command-Code
*      field set to 316 and the "R" bit set in the Command Flags field, is sent
*      from MME or SGSN to HSS.
*
*
*      < MO-Forward-Short-Message-Request > ::= < Diameter Header: 8388645, REQ, PXY, 16777313 >
*        < Session-Id >
*        [ DRMP ]
*        [ Vendor-Specific-Application-Id ]
*        { Auth-Session-State }
*        { Origin-Host }
*        { Origin-Realm }
*        [ Destination-Host ]
*        { Destination-Realm }
*        { SC-Address }
*        [ OFR-Flags ]
*       *[ Supported-Features ]
*        { User-Identifier }
*        { SM-RP-UI }
*        [ SMSMI-Correlation-ID ]
*        [ SM-Delivery-Outcome ]
*       *[ AVP ]
*       *[ Proxy-Info ]
*       *[ Route-Record ]
*/
#ifdef ANSI
PUBLIC S16 aqfdSendOFR
(
VbMmeUeCb *ueCb,
VbMmeMoData *modata
)
#else
PUBLIC S16 aqfdSendOFR(ueCb, modata)
VbMmeUeCb *ueCb;
VbMmeMoData *modata;
#endif
{
   struct avp *avp;
   struct msg *msg;
   S8 imsi[VB_HSS_IMSI_LEN + 1];
   U32 ulr_flags = 0;

   aqfdImsi2Str(ueCb->ueCtxt.ueImsi, ueCb->ueCtxt.ueImsiLen, imsi);

   /* construct the message */
   AQCHECK_MSG_NEW( aqDict.cmdOFR, msg );

   /* Session-Id */
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Session_Id, msg, MSG_BRW_LAST_CHILD, modata->sessId, modata->sessIdLen);

   /* Vendor-Specific-Application-Id */
   AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Vendor_Specific_Application_Id, msg, MSG_BRW_LAST_CHILD, avp );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Vendor_Id, avp, MSG_BRW_LAST_CHILD, aqDict.vnd3GPPdata.vendor_id );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Auth_Application_Id, avp, MSG_BRW_LAST_CHILD, aqDict.appSGDdata.application_id );

   /* Auth-Session-State */
   AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Auth_Session_State, msg, MSG_BRW_LAST_CHILD, 1 );

   /* Origin-Host & Origin-Realm */
   AQCHECK_MSG_ADD_ORIGIN( msg );

   /* Destination-Host & Destination-Realm */
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_Destination_Host, msg, MSG_BRW_LAST_CHILD, vbSmCb.cfgCb.smsRouterHost );
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_Destination_Realm, msg, MSG_BRW_LAST_CHILD, vbSmCb.cfgCb.smsRouterRealm );

   /* SC-Address */
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_SC_Address, msg, MSG_BRW_LAST_CHILD, modata->rqst.u.rpdata.destAddr.address, modata->rqst.u.rpdata.destAddr.len );

   /* User-Identifier */
   AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_User_Identifier, msg, MSG_BRW_LAST_CHILD, avp );
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_User_Name, avp, MSG_BRW_LAST_CHILD, imsi );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_MSISDN, avp, MSG_BRW_LAST_CHILD,
         ueCb->ueCtxt.ueHssCtxt.subscriptionData->msisdn->val,
         ueCb->ueCtxt.ueHssCtxt.subscriptionData->msisdn->len);

   /* SM-RP-UI */
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_SM_RP_UI, msg, MSG_BRW_LAST_CHILD,
         modata->rqst.u.rpdata.userData.tpdu, modata->rqst.u.rpdata.userData.len );

   /* send the message */
   AQFD_DUMP_MESSAGE(msg);  
   AQCHECK_MSG_SEND( &msg, _aqfd_ofa_cb, (Void*)modata );

   return ROK;
}

/*
*
*       Fun:    aqfd_ofa_cb
*
*       Desc:   MO-Forward-Short-Message-Answer callback
*
*       Ret:    0
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       < MO-Forward-Short-Message-Answer > ::=   < Diameter Header: 8388645, PXY, 16777313 >
*          < Session-Id >
*          [ DRMP ]
*          [ Vendor-Specific-Application-Id ]
*          [ Result-Code ]
*          [ Experimental-Result ]
*          { Auth-Session-State }
*          { Origin-Host }
*          { Origin-Realm }
*         *[ Supported-Features ]
*          [ SM-Delivery-Failure-Cause ]
*          [ SM-RP-UI ]
*          [ External-Identifier ]
*         *[ AVP ]
*          [ Failed-AVP ]
*         *[ Proxy-Info ]
*         *[ Route-Record ]
*/
#ifdef ANSI
PUBLIC int aqfd_ofa_cb
(
struct msg ** msg,
struct avp * pavp,
struct session * sess,
void * data,
enum disp_action * act
)
#else
PUBLIC int aqfd_ofa_cb(msg, pavp, sess, data, act)
struct msg ** msg;
struct avp * pavp;
struct session * sess;
void * data;
enum disp_action * act;
#endif
{
   fd_msg_free( *msg );
   *msg = NULL;
   return 0;
}

#ifdef ANSI
PRIVATE Void _aqfd_ofa_cb
(
void * data,
struct msg ** msg
)
#else
PRIVATE Void _aqfd_ofa_cb(data, msg)
void * data;
struct msg ** msg;
#endif
{
   S16 ret = ROK;
   U32 rc;
   struct msg   *ans = NULL;
   struct avp   *avp = NULL;
   VbMmeMoData  *modata = (VbMmeMoData*)data;

   ans = *msg;

AQFD_DUMP_MESSAGE(ans);

   /* get Result-Code */
   AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_Result_Code, avp, );
   if (avp)
   {
      /* Result-Code found */
      AQCHECK_AVP_GET_U32(aqDict.avp_Result_Code, avp, rc, );
      if (!rc)
      {
         LOG_E("Error Result-Code AVP invalid value = %u", rc);
         modata->resp.msgType = RP_MSG_TYPE_RPERROR_N2MS;
         modata->resp.rpMsgRef = modata->rqst.rpMsgRef;
         modata->resp.u.rperror.cause.value[modata->resp.u.rperror.cause.len++] =
               CM_EMM_RPERROR_CAUSE_PROTOCOL_ERROR;
         goto fini1;
      }
   }
   else
   {
      /* Result-Code not found, check for Experimental-Result */
      AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_Experimental_Result, avp, );
      if (!avp)
      {
         LOG_E("Error Experimental-Result AVP not found");
         modata->resp.msgType = RP_MSG_TYPE_RPERROR_N2MS;
         modata->resp.rpMsgRef = modata->rqst.rpMsgRef;
         modata->resp.u.rperror.cause.value[modata->resp.u.rperror.cause.len++] =
               CM_EMM_RPERROR_CAUSE_PROTOCOL_ERROR;
         goto fini1;
      }

      AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_Experimental_Result, avp, );
      if (avp)
      {
         struct avp *ercavp = NULL;
         AQCHECK_MSG_FIND_AVP(avp, aqDict.avp_Experimental_Result_Code, ercavp, );
         if (ercavp)
         {
            AQCHECK_AVP_GET_U32(ercavp, aqDict.avp_Experimental_Result_Code, rc, );
         }
         else
         {
            LOG_E("Error Experimental-Result-Code AVP not found");
            modata->resp.msgType = RP_MSG_TYPE_RPERROR_N2MS;
            modata->resp.rpMsgRef = modata->rqst.rpMsgRef;
            modata->resp.u.rperror.cause.value[modata->resp.u.rperror.cause.len++] =
                  CM_EMM_RPERROR_CAUSE_PROTOCOL_ERROR;
            goto fini1;
         }
      }
   }

   switch (rc)
   {
      case 2001: /* DIAMETER_SUCCESS */
      {
         modata->resp.msgType = RP_MSG_TYPE_RPACK_N2MS;
         modata->resp.rpMsgRef = modata->rqst.rpMsgRef;
         break;
      }
      case 5552: /* DIAMETER_ERROR_FACILITY_NOT_SUPPORTED */
      {
         modata->resp.msgType = RP_MSG_TYPE_RPERROR_N2MS;
         modata->resp.rpMsgRef = modata->rqst.rpMsgRef;
         modata->resp.u.rperror.cause.value[modata->resp.u.rperror.cause.len++] =
               CM_EMM_RPERROR_CAUSE_REQUESTED_FACILITY_NOT_IMPLEMENTED;
         break;
      }
      case 5555: /* DIAMETER_ERROR_SM_DELIVERY_FAILURE */
      {
         struct avp *dfc = NULL;

         modata->resp.msgType = RP_MSG_TYPE_RPERROR_N2MS;
         modata->resp.rpMsgRef = modata->rqst.rpMsgRef;

         AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_SM_Delivery_Failure_Cause, dfc, );
         if (dfc)
         {
            struct avp *edfc = NULL;
            AQCHECK_MSG_FIND_AVP(dfc, aqDict.avp_SM_Enumerated_Delivery_Failure_Cause, edfc, );
            if (edfc)
            {
               S32 fc = 0;
               AQCHECK_AVP_GET_S32(edfc, aqDict.avp_SM_Enumerated_Delivery_Failure_Cause, fc, );
               switch (fc)
               {
                  switch (fc)
                  {
                     case 3: /* UNKNOWN_SERVICE_CENTRE */
                     {
                        modata->resp.u.rperror.cause.value[modata->resp.u.rperror.cause.len++] =
                              CM_EMM_RPERROR_CAUSE_REQUESTED_FACILITY_NOT_IMPLEMENTED;
                        break;
                     }
                     case 4: /* SC-CONGESTION */
                     {
                        modata->resp.u.rperror.cause.value[modata->resp.u.rperror.cause.len++] =
                              CM_EMM_RPERROR_CAUSE_CONGESTION;
                        break;
                     }
                     case 5: /* INVALID_SME-ADDRESS */
                     {
                        modata->resp.u.rperror.cause.value[modata->resp.u.rperror.cause.len++] =
                              CM_EMM_RPERROR_CAUSE_UNIDENTIFIED_SUBSCRIBER;
                        break;
                     }
                     case 6: /* USER_NOT_SC-USER */
                     {
                        modata->resp.u.rperror.cause.value[modata->resp.u.rperror.cause.len++] =
                              CM_EMM_RPERROR_CAUSE_UNKNOWN_SUBSCRIBER;
                        break;
                     }
                     case 0: /* MEMORY_CAPACITY_EXCEEDED */
                     case 1: /* EQUIPMENT_PROTOCOL_ERROR */
                     case 2: /* EQUIPMENT_NOT_SM-EQUIPPED */
                     default:
                     {
                        LOG_E("Error unexpected SM-Enumerated-Delivery-Failure-Cause value %d", fc);
                        modata->resp.u.rperror.cause.value[modata->resp.u.rperror.cause.len++] =
                              CM_EMM_RPERROR_CAUSE_PROTOCOL_ERROR;
                        break;
                     }
                  }
               }
            }
            else
            {
               LOG_E("Error SM-Enumerated-Delivery-Failure-Cause AVP not found");
               modata->resp.u.rperror.cause.value[modata->resp.u.rperror.cause.len++] =
                     CM_EMM_RPERROR_CAUSE_PROTOCOL_ERROR;
            }
         }
         else
         {
            LOG_E("Error SM-Delivery-Failure-Cause AVP not found");
            modata->resp.u.rperror.cause.value[modata->resp.u.rperror.cause.len++] =
                  CM_EMM_RPERROR_CAUSE_PROTOCOL_ERROR;
         }
         break;
      }
   }

fini1:
   /* push ofa */
   aqQueuePush(&aqCb.ofaQueue, modata);

   _initPst();
   _pst.event = EVTLSXOFA;
   {
      Buffer *buf = NULL;
      if(SGetMsg(aqCb.cfg.sxPst.region, aqCb.cfg.sxPst.pool, &buf) != ROK)
         LOG_E("Error %d returned from SGetMsg() sending OFA notification", ret);
      else
      {
         if ((ret = SPstTsk(&_pst, buf)) != ROK)
         {
            LOG_E("Error %d returned from SPstTsk() sending OFA notification", ret);
            SPutMsg(buf);
         }
      }
   }

fini2:
   return ret;
}

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision History:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

