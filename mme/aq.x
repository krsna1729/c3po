/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter  Convergence Layer

     Type:     C include file

     Desc:     Structures, variables, types and prototypes.

     File:     aq.x

     Sid:

     Prg:      bw

*********************************************************************21*/



#ifndef __AQX__
#define __AQX__


#ifdef __cplusplus
extern "C" {
#endif


typedef struct aqQueueNode {
   struct aqQueueNode *pnext;
   struct aqQueueNode *pprev;
   Void *pdata;
} AqQueueNode;

typedef struct aqQueue {
   AqQueueNode * queueHead; // pop from the head
   AqQueueNode * queueTail; // push to the tail
   pthread_mutex_t queueMutex;
} AqQueue;

EXTERN int aqQueueInit ARGS((AqQueue *pqueue));
EXTERN Void aqQueuePush ARGS((AqQueue *pqueue, void *pdata));
EXTERN Void *aqQueuePop ARGS((AqQueue *pqueue));

/* Reception statistics.
 */
typedef struct aqRxSts
{
   StsCntr      numRxMsg;
} AqRxSts;


/* Transmission statistics.
 */
typedef struct aqTxSts
{
   StsCntr      numTxMsg;
} AqTxSts;


/* Error statistics.
 */
typedef struct aqErrSts
{
   StsCntr      diameterErr;
} AqErrSts;

/* Layer manager control requests can be pending on a SAP when
 * communication with the AQ threads are required before a confirm
 * can be sent.
 */
typedef struct aqPendOp
{
   Bool         flag;           /* operation pending? */
   Header       hdr;            /* control request header */
   U8           action;         /*    "       "    action */
   Elmnt        elmnt;          /* STTSAP or STGRTSAP */
   U16          numRem;         /* for ASHUTDOWN/STGRTSAP requests */
   Pst          lmPst;          /* for ASHUTDOWN response */
} AqPendOp;

#ifdef AQ_MULTI_THREADED

/* Upper interface primitives can be invoked by multiple threads.
 * Each primitive must therefore have its own Pst.
 */
typedef struct aqUiPsts
{
   Pst          uiConCfmPst;
   Pst          uiConIndPst;
   Pst          uiFlcIndPst;
   Pst          uiDatIndPst;
   Pst          uiUDatIndPst;
   Pst          uiDiscIndPst;
   Pst          uiDiscCfmPst;

} AqUiPsts;

#endif /* AQ_MULTI_THREADED */

/* AQ layer control block.
 */
typedef struct _aqCb
{
   TskInit      init;           /* task initialization structure */

   AqGenCfg     cfg;            /* general configuration */
   AqErrSts     errSts;         /* general error statistics */
   AqRxSts      rxSts;
   AqTxSts      txSts;

   AqPendOp     pendOp;         /* control request pending */

#ifdef AQ_MULTI_THREADED
   SLockId      pendLock;       /* for pendOp */
   SLockId      lmPstLock;       /* for lmPst */
#endif

   AqQueue     aiaQueue;
   AqQueue     ulaQueue;
   AqQueue     puaQueue;
   AqQueue     clrQueue;
   AqQueue     idrQueue;
   AqQueue     ofaQueue;
   AqQueue     tfrQueue;
   AqQueue     riaQueue;
} AqCb;

EXTERN AqCb aqCb;

/* Message to a group thread.
 */
typedef struct aqThrMsg
{
   S16          type;           /* message type */
   SpId         spId;           /* for the relevant SAP */
   UConnId      spConId;        /* for the relevant connection */
   union
   {
      Reason    reason;         /* for indications */
      Action    action;         /* for confirms */
   } disc;
} AqThrMsg;

/* management related */
EXTERN Void aqSendLmCfm         ARGS((Pst *pst, U8 cfmType,
                                      Header *hdr, U16 status,
                                      U16 reason, AqMngmt *cfm));
EXTERN S16  aqGetGenSts         ARGS((AqGenSts *genSts));
EXTERN S16  aqZeroGenSts        ARGS((Void));
EXTERN S16  aqCntrlGen          ARGS((Pst *pst, AqMngmt *cntrl, Header *hdr));
EXTERN Void aqSendAlarm         ARGS((U16 cgy, U16 evnt, U16 cause,
                                      AqAlarmInfo *info));

EXTERN S16  aqGetSid            ARGS((SystemId *sid));

/*
EXTERN S16  hiCfgGen            ARGS((HiGenCfg *hiGen));
EXTERN S16  hiCfgSap            ARGS((HiSapCfg *cfg));
#ifdef HI_TLS
EXTERN S16  hiCfgCtx            ARGS((HiCtxCfg *cfg));
#endif
EXTERN S16  hiShutdown          ARGS((Void));
EXTERN S16  hiCntrlSap          ARGS((Pst *pst, HiMngmt *cntrl, Header *hdr));
EXTERN S16  hiCntrlSapGrp       ARGS((Pst *pst, HiMngmt *cntrl, Header *hdr));
EXTERN S16  hiGetSapSts         ARGS((HiSapSts *sapSts, HiSap *sap));
EXTERN S16  hiZeroSapSts        ARGS((HiSap *sap));
EXTERN Void hiTrcBuf            ARGS((HiSap *sap, U16 evnt, Buffer *mBuf));
*/


struct avp;
typedef struct sySubscriptionData SySubscriptionData;
EXTERN S16 aqfdParseSubscriptionData		ARGS((struct avp * sub_data_avp, SySubscriptionData *sub_data));

#ifdef __cplusplus
}
#endif

#endif /* __AQX__ */

/********************************************************************30**

         End of file:     hi.x@@/main/6 - Mon Mar  3 20:09:47 2008

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

