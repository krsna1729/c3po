/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     S6A  Convergence Layer

     Type:     C include file

     Desc:     Structures, variables, types and prototypes.

     File:     sy2.x

     Sid:

     Prg:      bw

*********************************************************************21*/



#ifndef __SY2X__
#define __SY2X__

#include "aqfd2.x"
#ifdef PERFORMANCE_TIMING
#include "timer.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef struct syExternalClient {
   AqOctetString *client_identity;
   AqEnum gmlc_restriction;
   AqEnum notification_to_ue_user;
} SyExternalClient;

typedef struct syServiceType {
   U32 servicetypeidentity;
   AqEnum gmlc_restriction;
   AqEnum notification_to_ue_user;
} SyServiceType;

typedef struct syLCSPrivacyException {
   AqOctetString *ss_code;
   AqOctetString *ss_status;
   AqEnum notification_to_ue_user;
   CmLListCp external_client;                    /* SyExternalClient* */
   CmLListCp plmn_client;                        /* AqEnum* */
   CmLListCp service_type;                       /* SyServiceType* */
} SyLCSPrivacyException;

typedef struct syMOLR {
   AqOctetString *ss_code;
   AqOctetString *ss_status;
} SyMOLR;

typedef struct syLCSInfo
{
   CmLListCp gmlc_number;                        /* AqOctetString* */
   CmLListCp lcs_privacyexception;               /* SyLCSPrivacyException* */
   CmLListCp mo_lr;                              /* SyMOLR* */
} SyLCSInfo;

typedef struct syTeleserviceList {
   CmLListCp ts_code;                            /* AqOctetString* */
} SyTeleserviceList;

typedef struct syCallBarringInfo {
   AqOctetString *ss_code;
   AqOctetString *ss_status;
} SyCallBarringInfo;

typedef struct syAMBR {
   U32 max_requested_bandwidth_ul;
   U32 max_requested_bandwidth_dl;
} SyAMBR;

typedef struct syAllocationRetentionPriority {
   U32 priority_level;
   AqEnum pre_emption_capability;
   AqEnum pre_emption_vulnerability;
} SyAllocationRetentionPriority;

typedef struct syEPSSubscribedQoSProfile {
   AqEnum qos_class_identifier;
   SyAllocationRetentionPriority allocation_retention_priority;
} SyEPSSubscribedQoSProfile;

typedef struct syMIPHomeAgentHost {
   AqOctetString *destination_host;
   AqOctetString *destination_realm;
} SyMIPHomeAgentHost;

typedef struct syMIP6AgentInfo {
   CmLListCp mip_home_agent_address;             /* AqAddress* */
   SyMIPHomeAgentHost mip_home_agent_host;
} SyMIP6AgentInfo;

typedef struct sySpecificAPNInfo {
   AqOctetString *service_selection;
   SyMIP6AgentInfo mip6_agent_info;
   AqOctetString *visited_network_identifier;
} SySpecificAPNInfo;

typedef struct syWLANoffloadability {
   U32 wlan_offloadability_eutran;
   U32 wlan_offloadability_utran;
} SyWLANoffloadability;

typedef struct syAPNConfiguration {
   U32 context_identifier;
   CmLListCp served_party_ip_address;            /* AqAddress* */
   AqEnum pdn_type;
   AqOctetString *service_selection;
   SyEPSSubscribedQoSProfile eps_subscribed_qos_profile;
   AqEnum vplmn_dynamic_address_allowed;
   SyMIP6AgentInfo mip6_agent_info;
   AqOctetString *visited_network_identifier;
   AqEnum pdn_gw_allocation_type;
   AqOctetString *tgpp_charging_characteristics;
   SyAMBR ambr;
   CmLListCp specific_apn_info;                  /* SySpecificAPNInfo* */
   AqOctetString *apn_oi_replacement;
   AqEnum sipto_permission;
   AqEnum lipa_permission;
   U32 restoration_priority;
   U32 sipto_local_network_permission;
   SyWLANoffloadability wlan_offloadability;
   AqEnum non_ip_pdn_type_indicator;
   U32 non_ip_data_delivery_mechanism;
   AqOctetString *scef_id;
   AqOctetString *scef_realm;
   U32 preferred_data_mode;
   U32 pdn_connection_continuity;
} SyAPNConfiguration;

typedef struct syAPNConfigurationProfile {
   U32 context_identifier;
   U32 additional_context_identifier;
   AqEnum all_apn_configurations_included_indicator;
   CmLListCp apn_configuration;                  /* SyAPNConfiguration* */
} SyAPNConfigurationProfile;

typedef struct syAreaScope {
   CmLListCp cell_global_identity;               /* AqOctetString* */
   CmLListCp e_utran_cell_global_identity;       /* AqOctetString* */
   CmLListCp routing_area_identity;              /* AqOctetString* */
   CmLListCp location_area_identity;             /* AqOctetString* */
   CmLListCp tracking_area_identity;             /* AqOctetString* */
} SyAreaScope;

typedef struct syMDTConfiguration {
   AqEnum job_type;
   SyAreaScope area_scope;
   U32 list_of_measurements;
   U32 reporting_trigger;
   AqEnum report_interval;
   AqEnum report_amount;
   U32 event_threshold_rsrp;
   U32 event_threshold_rsrq;
   AqEnum logging_interval;
   AqEnum logging_duration;
   AqEnum measurement_period_lte;
   AqEnum measurement_period_umts;
   AqEnum collection_period_rrm_lte;
   AqEnum collection_period_rrm_umts;
   AqOctetString *positioning_method;
   AqOctetString *measurement_quantity;
   S32 event_threshold_event_1f;
   S32 event_threshold_event_1i;
   CmLListCp mdt_allowed_plmn_id;                /* AqOctetString* */
} SyMDTConfiguration;

typedef struct syTraceData {
   AqOctetString *trace_reference;
   U32 trace_depth;
   AqOctetString *trace_ne_type_list;
   AqOctetString *trace_interface_list;
   AqOctetString *trace_event_list;
   AqOctetString *omc_id;
   AqAddress *trace_collection_entity;
   SyMDTConfiguration mdt_configuration;
} SyTraceData;

typedef struct syPDPContext {
   U32 context_identifier;
   AqOctetString *pdp_type;
   AqAddress *pdp_address;
   AqOctetString *qos_subscribed;
   AqEnum vplmn_dynamic_address_allowed;
   AqOctetString *service_selection;
   AqOctetString *tgpp_charging_characteristics;
   AqOctetString *ext_pdp_type;
   AqAddress *ext_pdp_address;
   SyAMBR ambr;
   AqOctetString *apn_oi_replacement;
   U32 sipto_permission;
   U32 lipa_permission;
   U32 restoration_priority;
   U32 sipto_local_network_permission;
   U32 non_ip_data_delivery_mechanism;
   AqOctetString *scef_id;
} SyPDPContext;

typedef struct syGPRSSubscriptionData {
   AqEnum complete_data_list_included_indicator;
   CmLListCp pdp_context;                        /* SyPDPContext* */
} SyGPRSSubscriptionData;

typedef struct syCSGSubscriptionData {
   U32 csg_id;
   AqOctetString *expiration_date;
   CmLListCp service_selection;                  /* AqOctetString* */
   AqOctetString *visited_plmn_id;
} SyCSGSubscriptionData;

typedef struct syProSeSubscriptionData {
   U32 prose_permission;
} SyProSeSubscriptionData;

typedef struct syAdjacentAccessRestrictionData {
   AqOctetString *visited_plmn_id;
   U32 access_restriction_data;
} SyAdjacentAccessRestrictionData;

typedef struct syIMSIGroupId {
   U32 group_service_id;
   AqOctetString *group_plmn_id;
   AqOctetString *local_group_id;
} SyIMSIGroupId;

typedef struct syScheduledCommunicationTime {
   U32 day_of_week_mask;
   U32 time_of_day_start;
   U32 time_of_day_end;
} SyScheduledCommunicationTime;

typedef struct syCommunicationPatternSet {
   U32 periodic_communication_indicator;
   U32 communication_duration_time;
   U32 periodic_time;
   CmLListCp scheduled_communication_time;       /* SyScheduledCommunicationTime* */
   U32 stationary_indication;
   AqOctetString *reference_id_validity_time;
} SyCommunicationPatternSet;

typedef struct syAESECommunicationPattern {
   U32 scef_reference_id;
   AqOctetString *scef_id;
   CmLListCp scef_reference_id_for_deletion;     /* U32* */
   CmLListCp communication_pattern_set;          /* SyCommunicationPatternSet* */
} SyAESECommunicationPattern;

typedef struct syUEReachabilityConfiguration {
   U32 reachability_type;
   U32 maximum_response_time;
} SyUEReachabilityConfiguration;

typedef struct syLocationInformationConfiguration {
   U32 monte_location_type;
   U32 accuracy;
} SyLocationInformationConfiguration;

typedef struct syMonitoringEventConfiguration {
   U32 scef_reference_id;
   AqOctetString *scef_id;
   U32 monitoring_type;
   CmLListCp scef_reference_id_for_deletion;     /* U32* */
   U32 maximum_number_of_reports;
   AqOctetString *monitoring_duration;
   AqOctetString *charged_party;
   SyUEReachabilityConfiguration ue_reachability_configuration;
   SyLocationInformationConfiguration location_information_configuration;
   AqOctetString *scef_realm;
} SyMonitoringEventConfiguration;

typedef struct syEmergencyInfo {
   SyMIP6AgentInfo mip6_agent_info;
} SyEmergencyInfo;

typedef struct syV2XSubscriptionData {
   U32 v2x_permission;
   U32 ue_pc5_ambr;
} SyV2XSubscriptionData;

typedef struct syeDRXCycleLength {
   AqEnum rat_type;
   AqOctetString *edrx_cycle_length_value;
} SyeDRXCycleLength;

typedef struct sySubscriptionData {
   AqEnum subscriber_status;
   AqOctetString *msisdn;
   AqOctetString *a_msisdn;
   AqOctetString *stn_sr;
   AqEnum ics_indicator;
   AqEnum network_access_mode;
   U32 operator_determined_barring;
   U32 hplmn_odb;
   CmLListCp regional_subscription_zone_code;    /* AqOctetString* */
   U32 access_restriction_data;
   AqOctetString *apn_oi_replacement;
   SyLCSInfo lcs_info;
   SyTeleserviceList teleservice_list;
   CmLListCp call_barring_info;                  /* SyCallBarringInfo* */
   AqOctetString *tgpp_charging_characteristics;
   SyAMBR ambr;
   SyAPNConfigurationProfile apn_configuration_profile;
   U32 rat_frequency_selection_priority_id;
   SyTraceData trace_data;
   SyGPRSSubscriptionData gprs_subscription_data;
   CmLListCp csg_subscription_data;              /* SyCSGSubscriptionData */
   AqEnum roaming_restricted_due_to_unsupported_feature;
   U32 subscribed_periodic_rau_tau_timer;
   U32 mps_priority;
   AqEnum vplmn_lipa_allowed;
   AqEnum relay_node_indicator;
   AqEnum mdt_user_consent;
   AqEnum subscribed_vsrvcc;
   SyProSeSubscriptionData prose_subscription_data;
   U32 subscription_data_flags;
   CmLListCp adjacent_access_restriction_data;   /* SyAdjacentAccessRestrictionData* */
   S32 dl_buffering_suggested_packet_count;
   CmLListCp imsi_group_id;                      /* SyIMSIGroupId* */
   U32 ue_usage_type;
   CmLListCp aese_communication_pattern;         /* SyAESECommunicationPattern* */
   CmLListCp monitoring_event_configuration;     /* SyMonitoringEventConfiguration* */
   SyEmergencyInfo emergency_info;
   SyV2XSubscriptionData v2x_subscription_data;
   CmLListCp edrx_cycle_length;                  /* SyeDRXCycleLength* */
} SySubscriptionData;

typedef struct syULA {
   VbHssImsi imsi;
   U32 result_code;
   AqExperimentalResult experimental_result;
   U32 ula_flags;
   SySubscriptionData *subscription_data;
#ifdef PERFORMANCE_TIMING
   stimer_t ula_ans_time;
   stimer_t ula_cb_start;
#endif
} SyULA;

typedef struct syIDR { 
  VbHssImsi imsi;
  SySubscriptionData *subscription_data;
  struct msg *msg;
} SyIDR;

/*  PUR structures
typedef struct syUserCSGInformation {
   U32 csg_id;
   AqEnum csg_access_mode;
   AqEnum csg_membership_indication;
} SyUserCSGInformation;

typedef struct syMMELocationInformation {
   AqOctetString *e_utran_cell_global_identity;
   AqOctetString *tracking_area_identity;
   AqOctetString *geographical_information;
   AqOctetString *geodetic_information;
   AqEnum current_location_retrieved;
   U32 age_of_location_information;
   SyUserCSGInformation user_csg_information;
} SyMMELocationInformation;

typedef struct sySGSNLocationInformation {
   AqOctetString *cell_global_identity;
   AqOctetString *location_area_identity;
   AqOctetString *service_area_identity;
   AqOctetString *routing_area_identity;
   AqOctetString *geographical_information;
   AqOctetString *geodetic_information;
   AqEnum current_location_retrieved;
   U32 age_of_location_information;
   SyUserCSGInformation user_csg_information;
} SySGSNLocationInformation;

typedef struct syEPSLocationInformation {
   SyMMELocationInformation mme_location_information;
   SySGSNLocationInformation sgsn_location_information;
} SyEPSLocationInformation;
*/

/******************************************************************************/
/******************************************************************************/

typedef struct syRand {
   U8 len;
   U8 val[VB_RAND_SIZE];
} SyRand;

typedef struct syXres {
   U8 len;
   U8 val[VB_RES_SIZE];
} SyXres;

typedef struct syAutn {
   U8 len;
   U8 val[VB_AUTN_SIZE];
} SyAutn;

typedef struct syKasme {
   U8 len;
   U8 val[VB_KASME_SIZE];
} SyKasme;

typedef struct syVector {
   SyRand   rand;
   SyXres   xres;
   SyAutn   autn;
   SyKasme  kasme;
} SyVector;

typedef struct syAIA {
   VbHssImsi   imsi;
   U32         result;
   SyVector    vector;
#ifdef PERFORMANCE_TIMING
   stimer_t    aia_resp_time;
   stimer_t    aia_cb_start;
#endif
} SyAIA;

typedef struct syPUA {
   VbHssImsi   imsi;
   U32         result;
} SyPUA;

/******************************************************************************/
/******************************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* __SYX__ */

/********************************************************************30**

         End of file:     hi.x@@/main/6 - Mon Mar  3 20:09:47 2008

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

