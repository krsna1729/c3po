/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef __AQFD_RI_H__
#define __AQFD_RI_H__

/* Monitoring-Type values (U32) */
#define MT_LOSS_OF_CONNECTIVITY     0
#define MT_UE_REACHABILITY          1
#define MT_LOCATION_REPORTING       2
#define MT_COMMUNICATION_FAILURE    5
#define MT_AVBL_AFTER_DDN_FAILURE   6

/* Loss-Of-Connectivity-Reason values (U32) */
#define LOCR_UE_DETACHED_MME                 0
#define LOCR_UE_DETACHED_SGSN                1
#define LOCR_MAX_DETECTION_TIME_EXPIRED_MME  2
#define LOCR_MAX_DETECTION_TIME_EXPIRED_SGSN 3
#define LOCR_UE_PURGED_MME                   4
#define LOCR_UE_PURGED_SGSN                  5

/* Reachability-Information values (U32) */
#define RI_REACHABLE_FOR_SMS     0
#define RI_REACHABLE_FOR_DATA    1

/* Cause-Type values (U32) */
#define CT_RADIO_NETWORK_LAYER   0
#define CT_TRANSPORT_LAYER       1
#define CT_NAS                   2
#define CT_PROTOCOL              3
#define CT_MISCELLANEOUS         4

#endif /* __AQFD_RI_H__ */
