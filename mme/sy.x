/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     S6A  Convergence Layer

     Type:     C include file

     Desc:     Structures, variables, types and prototypes.

     File:     sy.x

     Sid:

     Prg:      bw

*********************************************************************21*/



#ifndef __SYX__
#define __SYX__

#include "sy2.x"

#ifdef __cplusplus
extern "C" {
#endif


/* Reception statistics.
 */
typedef struct syRxSts
{
   StsCntr      numRxMsg;
} SyRxSts;


/* Transmission statistics.
 */
typedef struct syTxSts
{
   StsCntr      numTxMsg;
} SyTxSts;


/* Error statistics.
 */
typedef struct syErrSts
{
   StsCntr      s6aErr;
} SyErrSts;

/* Layer manager control requests can be pending on a SAP when
 * communication with the SY threads are required before a confirm
 * can be sent.
 */
typedef struct syPendOp
{
   Bool         flag;           /* operation pending? */
   Header       hdr;            /* control request header */
   U8           action;         /*    "       "    action */
   Elmnt        elmnt;          /* STTSAP or STGRTSAP */
   U16          numRem;         /* for ASHUTDOWN/STGRTSAP requests */
   Pst          lmPst;          /* for ASHUTDOWN response */
} SyPendOp;

#ifdef SY_MULTI_THREADED

/* Upper interface primitives can be invoked by multiple threads.
 * Each primitive must therefore have its own Pst.
 */
typedef struct syUiPsts
{
   Pst          uiConCfmPst;
   Pst          uiConIndPst;
   Pst          uiFlcIndPst;
   Pst          uiDatIndPst;
   Pst          uiUDatIndPst;
   Pst          uiDiscIndPst;
   Pst          uiDiscCfmPst;

} SyUiPsts;

#endif /* SY_MULTI_THREADED */

/* SY layer control block.
 */
typedef struct _syCb
{
   TskInit      init;           /* task initialization structure */

   SyGenCfg     cfg;            /* general configuration */
   SyErrSts     errSts;         /* general error statistics */
   SyRxSts      rxSts;
   SyTxSts      txSts;

   SyPendOp     pendOp;         /* control request pending */
} SyCb;

EXTERN SyCb syCb;

/* Message to a group thread.
 */
typedef struct syThrMsg
{
   S16          type;           /* message type */
   SpId         spId;           /* for the relevant SAP */
   UConnId      spConId;        /* for the relevant connection */
   union
   {
      Reason    reason;         /* for indications */
      Action    action;         /* for confirms */
   } disc;
} SyThrMsg;

/* management related */
EXTERN Void sySendLmCfm         ARGS((Pst *pst, U8 cfmType,
                                      Header *hdr, U16 status,
                                      U16 reason, SyMngmt *cfm));
EXTERN S16  syGetGenSts         ARGS((SyGenSts *genSts));
EXTERN S16  syZeroGenSts        ARGS((Void));
EXTERN S16  syCntrlGen          ARGS((Pst *pst, SyMngmt *cntrl, Header *hdr));
EXTERN Void sySendAlarm         ARGS((U16 cgy, U16 evnt, U16 cause,
                                      SyAlarmInfo *info));

EXTERN S16  syGetSid            ARGS((SystemId *sid));

/*
EXTERN S16  hiCfgGen            ARGS((HiGenCfg *hiGen));
EXTERN S16  hiCfgSap            ARGS((HiSapCfg *cfg));
#ifdef HI_TLS
EXTERN S16  hiCfgCtx            ARGS((HiCtxCfg *cfg));
#endif
EXTERN S16  hiShutdown          ARGS((Void));
EXTERN S16  hiCntrlSap          ARGS((Pst *pst, HiMngmt *cntrl, Header *hdr));
EXTERN S16  hiCntrlSapGrp       ARGS((Pst *pst, HiMngmt *cntrl, Header *hdr));
EXTERN S16  hiGetSapSts         ARGS((HiSapSts *sapSts, HiSap *sap));
EXTERN S16  hiZeroSapSts        ARGS((HiSap *sap));
EXTERN Void hiTrcBuf            ARGS((HiSap *sap, U16 evnt, Buffer *mBuf));
*/

/******************************************************************************/
/******************************************************************************/

EXTERN Void aqfdDestroySubscriptionData ARGS(( SySubscriptionData *sd ));

#ifdef __cplusplus
}
#endif

#endif /* __SYX__ */

/********************************************************************30**

         End of file:     hi.x@@/main/6 - Mon Mar  3 20:09:47 2008

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

