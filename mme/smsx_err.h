/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

/********************************************************************20**
  
     Name:     TCP UDP Convergence Layer Sample Stack Manager
  
     Type:     C include file
  
     Desc:     Error file 
              
     File:     smsx_err.h
  
     Sid:      

     Prg:      rks
  
*********************************************************************21*/

#ifndef __SMSXERRH_
#define __SMSXERRH_

/* error codes */
#define SMSXLOGERROR(ent, inst, procId, errCls, errCode, errVal, errDesc) \
        SLogError(ent, inst, procId, __FILE__, __LINE__,                  \
                  (ErrCls)errCls, (ErrCode)errCode, (ErrVal)errVal,       \
                  (Txt*)errDesc)

/* Error codes for TUCL sample layer manager */
#define   ERRSMSXBASE   0
#define   ERRSMSX       ERRSMSXBASE

#define   ESMSX001      (ERRSMSX +    1)    /*   smsxexms.c: 202 */

#define   ESMSX002      (ERRSMSX +    2)    /*   smsxptmi.c: 418 */
#define   ESMSX003      (ERRSMSX +    3)    /*   smsxptmi.c: 461 */
#define   ESMSX004      (ERRSMSX +    4)    /*   smsxptmi.c: 499 */
#define   ESMSX005      (ERRSMSX +    5)    /*   smsxptmi.c: 538 */

#endif /* __SMSXERRH_*/


/********************************************************************30**
 
         End of file:     

*********************************************************************31*/
 
/********************************************************************40**
 
        Notes:
 
*********************************************************************41*/
 
/********************************************************************50**
 
*********************************************************************51*/
 
/********************************************************************60**
 
        Revision history:
 
*********************************************************************61*/
/********************************************************************70**
  
  version    initials                   description
-----------  ---------  ------------------------------------------------
 
*********************************************************************71*/
 
/********************************************************************80**
 
*********************************************************************81*/
/********************************************************************90**
 
    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      rks   1. initial release.
*********************************************************************91*/
