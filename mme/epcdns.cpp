/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "envopt.h"        /* Environment options             */
#include "envdep.h"        /* Environment dependent options   */
#include "envind.h"        /* Environment independent options */
#include "gen.h"           /* General layer                   */
#include "ssi.h"           /* System service interface        */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "lvb.h"
#include "vbsm.h"

#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common linrary function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */

#include "lvb.x"
#include "vbsm.x"
#include "lds.x"

#include "epcdns.x"

#include "epc.h"
#include "cdnscache.h"

#include <memory.h>

extern "C" {
   void sgwCallback( CachedDNS::Query *query, bool cacheHit, void *data );
}

void sgwCallback( CachedDNS::Query *query, bool cacheHit, void *data )
{
   if ( !data )
      return;

   DnsQueryData *qd = (DnsQueryData*)data; // qd will be released when the event is processed

   // populate the ip address (if one exists) and set the success flag
   for ( CachedDNS::ResourceRecordList::const_iterator itnaptr = query->getAnswers().begin();
         itnaptr != query->getAnswers().end();
         ++itnaptr )
   {
      if ( (*itnaptr)->getType() == ns_t_naptr )
      {
         CachedDNS::RRecordNAPTR *naptr = (CachedDNS::RRecordNAPTR*)*itnaptr;
         for ( CachedDNS::ResourceRecordList::const_iterator itadd = query->getAdditional().begin();
               itadd != query->getAdditional().end();
               ++itadd )
         {
            if ( (*itadd)->getType() == ns_t_a  && (*itadd)->getName() == naptr->getReplacement() )
            {
               CachedDNS::RRecordA *a = (CachedDNS::RRecordA*)*itadd;
               qd->addr.type = CM_INET_IPV4ADDR_TYPE;
               qd->addr.u.ipv4TptAddr.port = vbSmCb.cfgCb.egDfltPort;
               qd->addr.u.ipv4TptAddr.address = ntohl(a->getAddress().s_addr);
               qd->success = TRUE;
               break;
            }
         }
         
         if ( qd->success )
            break;
      }
   }

   // pack and post the message
   if (cmPkLdsInitAttachSgw(qd) != ROK)
      free(qd);
}

void dnsQuerySgwTac( const U8 *plmnid, U16 tac, const U8 *imsi, U8 imsilen )
{
   char lb[3];
   char hb[3];

   sprintf( lb, "%02X", tac & 0x00ff );
   sprintf( hb, "%02X", (tac & 0xff00) >> 8 );

   std::string fqdn = EPC::Utility::tai_fqdn( lb, hb, plmnid );

   DnsQueryData *qd = (DnsQueryData*)malloc( sizeof(DnsQueryData) );
   memset( qd, 0, sizeof(*qd) );
   memcpy( qd->imsi, imsi, imsilen );
   qd->imsilen = imsilen;
   
   if ( vbSmCb.cfgCb.disableEpcDns )
   {
      if ( cmPkLdsInitAttachSgw(qd) != ROK )
         free( qd );
   }
   else
   {
      CachedDNS::Cache::getInstance().query( ns_t_naptr, fqdn, sgwCallback, qd );
   }
}

