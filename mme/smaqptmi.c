/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*

Layer management provides the necessary functions to control and
monitor the condition of each protocol layer.

The following functions are provided in this file:

     SmMiLaqCfgReq      Configure Request
     SmMiLaqStaReq      Status Request
     SmMiLaqStsReq      Statistics Request
     SmMiLaqCntrlReq    Control Request

It is assumed that the following functions are provided in the
stack management body files:

     SmMiLhiStaInd      Status Indication
     SmMiLhiStaCfm      Status Confirm
     SmMiLhiStsCfm      Statistics Confirm
     SmMiLhiTrcInd      Trace Indication

*/


/* header include files (.h) */

#include "envopt.h"        /* environment options */
#include "envdep.h"        /* environment dependent */
#include "envind.h"        /* environment independent */
#include "gen.h"           /* general layer */
#include "cm5.h"           /* common timer */
#include "ssi.h"           /* system services */
#include "cm_inet.h"       /* common sockets */
#include "laq.h"           /* HI layer management */
#include "cm_err.h"        /* common error */
#include "smhi_err.h"      /* HI error */

/* header/extern include files (.x) */

#include "gen.x"           /* general layer */
#include "ssi.x"           /* system services */
#include "cm5.x"           /* common timer */
#include "cm_inet.x"       /* common sockets */
#include "laq.x"           /* hi layer management */

#define  MAXAQMI  2

#ifndef  LCSMAQMILAQ
#define  PTSMAQMILAQ
#else
#ifndef   AQ
#define  PTSMAQMILAQ
#endif
#endif

#ifdef PTSMAQMILAQ
PRIVATE S16 PtMiLaqCfgReq    ARGS((Pst *pst, AqMngmt *cfg));
PRIVATE S16 PtMiLaqStsReq    ARGS((Pst *pst, Action action, AqMngmt *sts));
PRIVATE S16 PtMiLaqCntrlReq  ARGS((Pst *pst, AqMngmt *cntrl));
PRIVATE S16 PtMiLaqStaReq    ARGS((Pst *pst, AqMngmt *sta));
#endif


/*
the following matrices define the mapping between the primitives
called by the layer management interface of TCP UDP Convergence Layer
and the corresponding primitives in TUCL

The parameter MAXHIMI defines the maximum number of layer manager
entities on top of TUCL . There is an array of functions per primitive
invoked by TCP UDP Conbvergence Layer. Every array is MAXHIMI long
(i.e. there are as many functions as the number of service users).

The dispatching is performed by the configurable variable: selector.
The selector is configured during general configuration.

The selectors are:

   0 - loosely coupled (#define LCSMHIMILHI) 2 - Lhi (#define HI)

*/


/* Configuration request primitive */

PRIVATE LaqCfgReq SmMiLaqCfgReqMt[MAXAQMI] =
{
#ifdef LCSMAQMILAQ
   cmPkLaqCfgReq,          /* 0 - loosely coupled  */
#else
   PtMiLaqCfgReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef AQ
   AqMiLaqCfgReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLaqCfgReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Control request primitive */

PRIVATE LaqCntrlReq SmMiLaqCntrlReqMt[MAXAQMI] =
{
#ifdef LCSMAQMILAQ
   cmPkLaqCntrlReq,          /* 0 - loosely coupled  */
#else
   PtMiLaqCntrlReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef AQ
   HiMiLaqCntrlReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLaqCntrlReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Statistics request primitive */

PRIVATE LaqStsReq SmMiLaqStsReqMt[MAXAQMI] =
{
#ifdef LCSMAQMILAQ
   cmPkLaqStsReq,          /* 0 - loosely coupled  */
#else
   PtMiLaqStsReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef AQ
   HiMiLaqStsReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLaqStsReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Status request primitive */

PRIVATE LaqStaReq SmMiLaqStaReqMt[MAXAQMI] =
{
#ifdef LCSMAQMILAQ
   cmPkLaqStaReq,          /* 0 - loosely coupled  */
#else
   PtMiLaqStaReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef AQ
   HiMiLaqStaReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLaqStaReq,          /* 1 - tightly coupled, portable */
#endif
};


/*
*     layer management interface functions
*/

/*
*
*       Fun:   Configuration request
*
*       Desc:  This function is used to configure  TUCL
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smaqptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLaqCfgReq
(
Pst *spst,                /* post structure */
AqMngmt *cfg              /* configure */
)
#else
PUBLIC S16 SmMiLaqCfgReq(spst, cfg)
Pst *spst;                /* post structure */
AqMngmt *cfg;             /* configure */
#endif
{
   TRC3(SmMiLaqCfgReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLaqCfgReqMt[spst->selector])(spst, cfg);
   RETVALUE(ROK);
} /* end of SmMiLaqCfgReq */



/*
*
*       Fun:   Statistics request
*
*       Desc:  This function is used to request statistics from
*              TUCL
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smaqptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLaqStsReq
(
Pst *spst,                /* post structure */
Action action,
AqMngmt *sts              /* statistics */
)
#else
PUBLIC S16 SmMiLaqStsReq(spst, action, sts)
Pst *spst;                /* post structure */
Action action;
AqMngmt *sts;             /* statistics */
#endif
{
   TRC3(SmMiLaqStsReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLaqStsReqMt[spst->selector])(spst, action, sts);
   RETVALUE(ROK);
} /* end of SmMiLaqStsReq */


/*
*
*       Fun:   Control request
*
*       Desc:  This function is used to send control request to
*              TUCL
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smaqptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLaqCntrlReq
(
Pst *spst,                 /* post structure */
AqMngmt *cntrl            /* control */
)
#else
PUBLIC S16 SmMiLaqCntrlReq(spst, cntrl)
Pst *spst;                 /* post structure */
AqMngmt *cntrl;           /* control */
#endif
{
   TRC3(SmMiLaqCntrlReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLaqCntrlReqMt[spst->selector])(spst, cntrl);
   RETVALUE(ROK);
} /* end of SmMiLaqCntrlReq */


/*
*
*       Fun:   Status request
*
*       Desc:  This function is used to send a status request to
*              TUCL
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smaqptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLaqStaReq
(
Pst *spst,                /* post structure */
AqMngmt *sta              /* status */
)
#else
PUBLIC S16 SmMiLaqStaReq(spst, sta)
Pst *spst;                /* post structure */
AqMngmt *sta;             /* status */
#endif
{
   TRC3(SmMiLaqStaReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLaqStaReqMt[spst->selector])(spst, sta);
   RETVALUE(ROK);
} /* end of SmMiLaqStaReq */

#ifdef PTSMAQMILAQ

/*
 *             Portable Functions
 */

/*
*
*       Fun:   Portable configure Request- TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smaqptmi.c
*
*/

#ifdef ANSI
PRIVATE S16 PtMiLaqCfgReq
(
Pst *spst,                  /* post structure */
AqMngmt *cfg                /* configure */
)
#else
PRIVATE S16 PtMiLaqCfgReq(spst, cfg)
Pst *spst;                  /* post structure */
AqMngmt *cfg;               /* configure */
#endif
{
  TRC3(PtMiLaqCfgReq)

  UNUSED(spst);
  UNUSED(cfg);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMHILOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
              ERRCLS_DEBUG, ESMHI002, 0, "PtMiLaqCfgReq () Failed");
#endif

  RETVALUE(ROK);
} /* end of PtMiLaqCfgReq */



/*
*
*       Fun:   Portable statistics Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smaqptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLaqStsReq
(
Pst *spst,                  /* post structure */
Action action,
AqMngmt *sts                /* statistics */
)
#else
PRIVATE S16 PtMiLaqStsReq(spst, action, sts)
Pst *spst;                  /* post structure */
Action action;
AqMngmt *sts;               /* statistics */
#endif
{
  TRC3(PtMiLaqStsReq)

  UNUSED(spst);
  UNUSED(action);
  UNUSED(sts);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMHILOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
                ERRCLS_DEBUG, ESMHI003, 0, "PtMiLaqStsReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLaqStsReq */


/*
*
*       Fun:   Portable control Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smaqptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLaqCntrlReq
(
Pst *spst,                  /* post structure */
AqMngmt *cntrl              /* control */
)
#else
PRIVATE S16 PtMiLaqCntrlReq(spst, cntrl)
Pst *spst;                  /* post structure */
AqMngmt *cntrl;             /* control */
#endif
{
  TRC3(PtMiLaqCntrlReq)

  UNUSED(spst);
  UNUSED(cntrl);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMHILOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
             ERRCLS_DEBUG, ESMHI004, 0, "PtMiLaqCntrlReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLaqCntrlReq */


/*
*
*       Fun:   Portable status Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smaqptmi.c
*
*/

#ifdef ANSI
PRIVATE S16 PtMiLaqStaReq
(
Pst *spst,                  /* post structure */
AqMngmt *sta                /* status */
)
#else
PRIVATE S16 PtMiLaqStaReq(spst, sta)
Pst *spst;                  /* post structure */
AqMngmt *sta;               /* status */
#endif
{
  TRC3(PtMiLaqStaReq);

  UNUSED(spst);
  UNUSED(sta);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMHILOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
                ERRCLS_DEBUG,ESMHI005, 0, "PtMiLaqStaReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLaqStaReq */

#endif /* PTSMHIMILHI */


/********************************************************************30**

         End of file:     smaqptmi.c@@/main/5 - Mon Mar  3 20:09:32 2008

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      asa  1. initial release.
/main/2     ---      cvp  1. changed the copyright header.
/main/3     ---      cvp  1. changed the copyright header.
/main/4      ---      kp   1. Updated for release 1.5.
/main/5      ---       hs   1. Updated for release of 2.1
*********************************************************************91*/
