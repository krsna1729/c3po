/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:    Diameter T6A Convergence Layer

     Type:    C source file

     Desc:    Upper and Management Interface primitives.

     File:    sw.c

     Sid:

     Prg:     bw

*********************************************************************21*/


/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"

#include "aqfd.h"
#include "aqfd.x"

/*
#include "sy.h"
#include "sy.x"
#include "sy_err.h"
#include "sy2.x"
*/

#include "lsw.h"
#include "lsw.x"
#include "sw_err.h"
#include "sw.h"
#include "sw.x"

#define SWSWMV 1            /* Diameter T6a - main version */
#define SWSWMR 0            /* Diameter T6a - main revision */
#define SWSWBV 0            /* Diameter T6a - branch version */
#define SWSWBR 0            /* Diameter T6a - branch revision */

#define SWSWPN "1000161"    /* Diameter T6a - part number */

PRIVATE CONSTANT SystemId sId ={
   SWSWMV,                    /* main version */
   SWSWMR,                    /* main revision */
   SWSWBV,                    /* branch version */
   SWSWBR,                    /* branch revision */
   SWSWPN,                    /* part number */
};
PRIVATE S16 swCfgGen ARGS((SwGenCfg *swGen));


/* public variable declarations */

PUBLIC SwCb  swCb;         /* Epc DNS control block */


/* interface function to system service */


/*
*
*       Fun:    swActvInit
*
*       Desc:   Called from SSI to initialize Diameter.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   sw.c
*
*/
#ifdef SS_MULTIPLE_PROCS
#ifdef ANSI
PUBLIC S16 swActvInit
(
ProcId procId,         /* procId */
Ent entity,            /* entity */
Inst inst,             /* instance */
Region region,         /* region */
Reason reason,         /* reason */
Void **xxCb           /* Protocol Control Block */
)
#else
PUBLIC S16 swActvInit(procId,entity, inst, region, reason, xxCb)
ProcId procId;         /* procId */
Ent entity;            /* entity */
Inst inst;             /* instance */
Region region;         /* region */
Reason reason;         /* reason */
Void **xxCb;           /* Protocol Control Block */
#endif
#else /* SS_MULTIPLE_PROCS */
#ifdef ANSI
PUBLIC S16 swActvInit
(
Ent entity,            /* entity */
Inst inst,             /* instance */
Region region,         /* region */
Reason reason          /* reason */
)
#else
PUBLIC S16 swActvInit(entity, inst, region, reason)
Ent entity;            /* entity */
Inst inst;             /* instance */
Region region;         /* region */
Reason reason;         /* reason */
#endif
#endif /* SS_MULTIPLE_PROCS */
{
   TRC2(swActvInit);

#ifdef SS_MULTIPLE_PROCS
   SWDBGP(DBGMASK_SI, (swCb.init.prntBuf,
          "swActvInit(ProcId(%d), Ent(%d), Inst(%d), Region(%d), Reason(%d))\n",
           procId, entity, inst, region, reason));
#else /* SS_MULTIPLE_PROCS */
   SWDBGP(DBGMASK_SI, (swCb.init.prntBuf,
          "swActvInit(Ent(%d), Inst(%d), Region(%d), Reason(%d))\n",
           entity, inst, region, reason));
   SW_ZERO(&swCb, sizeof(SwCb));
#endif /* SS_MULTIPLE_PROCS */

   /* initialize swCb */

   swCb.init.ent        = entity;
   swCb.init.inst       = inst;
   swCb.init.region     = region;
   swCb.init.reason     = reason;
   swCb.init.cfgDone    = FALSE;
   swCb.init.pool       = 0;
#ifdef SS_MULTIPLE_PROCS
   swCb.init.procId = procId;
#else /* SS_MULTIPLE_PROCS */
   swCb.init.procId = SFndProcId();
#endif /* SS_MULTIPLE_PROCS */
   swCb.init.acnt       = FALSE;
   swCb.init.usta       = TRUE;
   swCb.init.trc        = FALSE;

#ifdef DEBUGP
   swCb.init.dbgMask    = 0;
#ifdef SW_DEBUG
   swCb.init.dbgMask = 0xFFFFFFFF;
#endif
#endif

   /* Used at SW---->UZ */
   swCb.uzPst.dstProcId = SFndProcId();
   swCb.uzPst.srcProcId = SFndProcId();
   swCb.uzPst.dstEnt    = (Ent)ENTUZ;
   swCb.uzPst.dstInst   = (Inst)0;
   swCb.uzPst.srcEnt    = (Ent)ENTSW;
   swCb.uzPst.srcInst   = (Inst)0;
   swCb.uzPst.prior     = (Prior)VBSM_MSGPRIOR;
   swCb.uzPst.route     = (Route)RTESPEC;
   swCb.uzPst.event     = (Event)EVTNONE;
   swCb.uzPst.region    = (Region)vbSmCb.init.region;
   swCb.uzPst.pool      = (Pool)vbSmCb.init.region;
   swCb.uzPst.selector  = (Selector)VBSM_SWSMSEL;

   RETVALUE(ROK);
} /* end of swActvInit */

/*
*
*       Fun:    SwMiLswCfgReq
*
*       Desc:   Configure the layer. Responds with a SwMiLswCfgCfm
*               primitive.
*
*       Ret:    ROK     - ok
*
*       Notes:  
*
*       File:   sw.c
*
*/
#ifdef ANSI
PUBLIC S16 SwMiLswCfgReq
(
Pst             *pst,           /* post structure */
SwMngmt         *cfg            /* configuration structure */
)
#else
PUBLIC S16 SwMiLswCfgReq(pst, cfg)
Pst             *pst;           /* post structure */
SwMngmt         *cfg;           /* configuration structure */
#endif
{
   SwMngmt      cfmMsg;
   S16          ret;


   TRC3(SwMiLswCfgReq);

#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&swCbPtr)) !=
      ROK)
   {
      SWLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESW004,(ErrVal)0,pst->dstInst,
            "SwMiLswCfgReq() failed, cannot derive swCb");
      RETVALUE(FALSE);
   }
   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf,
      "---------EPC DNS------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf,
          "SwMiLswCfgReq(pst, elmnt(%d))\n",
           cfg->hdr.elmId.elmnt));

   SW_ZERO(&cfmMsg, sizeof (SwMngmt));

   if (!swCb.init.cfgDone)
      swCb.init.lmPst.intfVer = pst->intfVer;

   switch (cfg->hdr.elmId.elmnt)
   {
      case STGEN:            /* general configuration */
         ret = swCfgGen(&cfg->t.cfg.s.swGen);
         break;

      default:               /* invalid */
         ret = LCM_REASON_INVALID_ELMNT;
         break;
   }

   /* issue configuration confirm */
   swSendLmCfm(pst, TCFG, &cfg->hdr, (U16)(ret == LCM_REASON_NOT_APPL ?
               LCM_PRIM_OK : LCM_PRIM_NOK), (U16)ret, &cfmMsg);

   RETVALUE(ROK);
} /* SwMiLswCfgReq() */


/*
*
*       Fun:    SwMiLsw:StsReq
*
*       Desc:   Get statistics information. Statistics are
*               returned by a SwMiLswStsCfm primitive. The
*               statistics counters can be reset using the action
*               parameter:
*                  ZEROSTS      - zero statistics counters
*                  NOZEROSTS    - do not zero statistics counters
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sw.c
*
*/
#ifdef ANSI
PUBLIC S16 SwMiLswStsReq
(
Pst             *pst,           /* post structure */
Action          action,         /* action to be done */
SwMngmt         *sts            /* statistics structure */
)
#else
PUBLIC S16 SwMiLswStsReq(pst, action, sts)
Pst             *pst;           /* post structure */
Action          action;         /* action to be done */
SwMngmt         *sts;           /* statistics structure */
#endif
{
   SwMngmt      cfmMsg;

   TRC3(SwMiLswStsReq);
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&swCbPtr)) !=
      ROK)
   {
      SWLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt, ESW005,(ErrVal)0,pst->dstInst,
            "SwMiLswStsReq() failed, cannot derive swCb");
      RETVALUE(FALSE);
   }
   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf,
      "---------EPC DNS------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf,
          "SwMiLswStsReq(pst, action(%d), elmnt(%d))\n",
           action, sts->hdr.elmId.elmnt));
   SWDBGP(DBGMASK_SI, (swCb.init.prntBuf,
          "SGetDateTime(DateTime(%p))\n", (Ptr)&cfmMsg.t.sts.dt));

   /* initialize the confirm structure */
   SW_ZERO(&cfmMsg, sizeof (SwMngmt));
   SGetDateTime(&cfmMsg.t.sts.dt);

   if(!swCb.init.cfgDone)
   {
      SWDBGP(DBGMASK_LYR, (swCb.init.prntBuf,
         "SwMiLswStsReq(): general configuration not done\n"));
      swSendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_NOK, LCM_REASON_GENCFG_NOT_DONE,
               &cfmMsg);

      RETVALUE(ROK);
   }

   switch (sts->hdr.elmId.elmnt)
   {
      case STGEN:            /* general statistics */
         swGetGenSts(&cfmMsg.t.sts.s.genSts);
         if (action == ZEROSTS)
            swZeroGenSts();
         break;

      default:
         swSendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_NOK,
                     LCM_REASON_INVALID_ELMNT, &cfmMsg);
         RETVALUE(ROK);
   }

   /* issue a statistics confirm */
   swSendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_OK, LCM_REASON_NOT_APPL,
               &cfmMsg);

   RETVALUE(ROK);
} /* SwMiLswStsReq() */

/*
*       Fun:    SwMiLsxRIA
*
*       Desc:   Processes an RIA message received from the AQ layer.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sx.c
*
*/
#ifdef ANSI
PUBLIC S16 SwMiLsxRIA
(
)
#else
PUBLIC S16 SwMiLsxRIA()
#endif
{
   S16 ret;
   SwRIAInfo *riinfo = NULL;
   VbMmeUeCb *ueCb = NULL;

   TRC3(SxMiLsxOFA);

   /* pop modata */
   riinfo = (SwRIAInfo*)aqQueuePop(&aqCb.riaQueue);
   if (!riinfo) /* nothing to process */
      return ROK;

   /* lookup ueCb */
   ret = vbMmeUtlFndUeCbOnImsi(riinfo->imsi, strlen(riinfo->imsi), &ueCb);

   if (ret == ROK) /* found the ueCb */
   {
      SW_DBG_INFO((swCb.init.prntBuf, "ueCb found processing RIA"));

      /* remove the monitoring event entry */
      vbMmeUtlDelMonitoringEventConfiguration(ueCb, riinfo->monitoring_type, riinfo->scef_reference_id, riinfo->scef_id);
   }
   else
   {
      SW_DBG_INFO((swCb.init.prntBuf, "ueCb not found for imsi [%s]", riinfo->imsi));
   }

   free(riinfo);
} /* SwMiLsxRIA() */

/*
*
*       Fun:    SwMiLswCntrlReq
*
*       Desc:   Control the specified element: enable or diable
*               trace and alarm (unsolicited status) generation,
*               delete or disable a SAP or a group of SAPs, enable
*               or disable debug printing.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sw.c
*
*/
#ifdef ANSI
PUBLIC S16 SwMiLswCntrlReq
(
Pst             *pst,           /* post structure */
SwMngmt         *ctl            /* pointer to control structure */
)
#else
PUBLIC S16 SwMiLswCntrlReq(pst, ctl)
Pst             *pst;           /* post structure */
SwMngmt         *ctl;           /* pointer to control structure */
#endif
{
   Header       *hdr;
   SwMngmt      cfmMsg;
   S16          ret;

   TRC3(SwMiLswCntrlReq);

/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&swCbPtr)) !=
      ROK)
   {
      SWLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESW006,(ErrVal)0,pst->dstInst,
            "SwMiLswCntrlReq() failed, cannot derive swCb");
      RETVALUE(FALSE);
   }
   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */
   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf,
          "SwMiLswCntrlReq(pst, elmnt(%d))\n", ctl->hdr.elmId.elmnt));
   SWDBGP(DBGMASK_SI, (swCb.init.prntBuf,
          "SGetDateTime(DateTime(%p))\n", (Ptr)&ctl->t.cntrl.dt));

   hdr = &ctl->hdr;
   SW_ZERO(&cfmMsg, sizeof (SwMngmt));
   SGetDateTime(&(ctl->t.cntrl.dt));

  /*hi002.105 - Check if Stack is Configured */
   if((!swCb.init.cfgDone) && (hdr->elmId.elmnt != STGEN))
   {
      SWDBGP(DBGMASK_LYR, (swCb.init.prntBuf,
         "SwMiLswCntrlReq(): general configuration not done\n"));
      /* issue a control confirm */
      /* hi003.105 to send cntrl cfm if gen cfg not done */
      swSendLmCfm(pst, TCNTRL, hdr, LCM_PRIM_NOK, LCM_REASON_GENCFG_NOT_DONE,
               &cfmMsg);

      RETVALUE(ROK);
   }

   switch (hdr->elmId.elmnt)
   {
      case STGEN:
         ret = swCntrlGen(pst, ctl, hdr);
         break;

      default:
         SWLOGERROR_INT_PAR(ESW007, hdr->elmId.elmnt, 0,
            "SwMiLswCntrlReq(): bad element in control request");
         ret = LCM_REASON_INVALID_ELMNT;
         break;
   }

   if (ret == LSW_REASON_OPINPROG)
   {
      swSendLmCfm(pst, TCNTRL, hdr, LCM_PRIM_OK_NDONE,
                  LCM_REASON_NOT_APPL, &cfmMsg);
      RETVALUE(ROK);
   }

   /* issue a control confirm primitive */
   swSendLmCfm(pst, TCNTRL, hdr, (U16)(ret == LCM_REASON_NOT_APPL ?
               LCM_PRIM_OK : LCM_PRIM_NOK), (U16)ret, &cfmMsg);

   RETVALUE(ROK);
} /* SwMiLswCntrlReq() */


/*
*
*       Fun:    SwMiLswStaReq
*
*       Desc:   Get status information. Responds with a
*               SwMiLswStaCfm primitive.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   sw.c
*
*/
#ifdef ANSI
PUBLIC S16 SwMiLswStaReq
(
Pst             *pst,           /* post structure */
SwMngmt         *sta            /* management structure */
)
#else
PUBLIC S16 SwMiLswStaReq(pst, sta)
Pst             *pst;           /* post structure */
SwMngmt         *sta;           /* management structure */
#endif
{
   Header       *hdr;
   SwMngmt      cfmMsg;

   TRC3(SwMiLswStaReq);

#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&swCbPtr)) !=
      ROK)
   {
      SWLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,ESW009,(ErrVal)0,pst->dstInst,
            "SwMiLswStaReq() failed, cannot derive swCb");
      RETVALUE(FALSE);
   }
   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf,
      "---------EPC DNS------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf, "SwMiLswStaReq(pst, elmnt(%d))\n",
           sta->hdr.elmId.elmnt));

   hdr = &(sta->hdr);
   SW_ZERO(&cfmMsg, sizeof (SwMngmt));

   /* hi002.105 - fill the date and time */
   SGetDateTime(&cfmMsg.t.ssta.dt);

   switch (hdr->elmId.elmnt)
   {
      case STSID:               /* system ID */
         swGetSid(&cfmMsg.t.ssta.s.sysId);
         break;

      default:
         SWLOGERROR_INT_PAR(ESW010, hdr->elmId.elmnt, 0,
            "SwMiLswStaReq(): invalid element in status request");
         swSendLmCfm(pst, TSSTA, hdr, LCM_PRIM_NOK,
                     LCM_REASON_INVALID_ELMNT, &cfmMsg);
         RETVALUE(ROK);
   }

   /* issue a status confirm primitive */
   swSendLmCfm(pst, TSSTA, hdr, LCM_PRIM_OK, LCM_REASON_NOT_APPL,
               &cfmMsg);

   RETVALUE(ROK);
} /* SwMiLswStaReq() */

/*
*
*       Fun:    swCfgGen
*
*       Desc:   Perform general configuration of Diameter. Must be done
*               after swActvInit() is called, but before any other
*               interaction with Diameter. Reserves static memory for
*               the layer with SGetSMem(), allocates required
*               memory, sets up data structures and starts the
*               receive mechanism (threads/timer/permanent task).
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*
*       Notes:  None
*
*       File:   sw.c
*
*/
#ifdef ANSI
PUBLIC S16 swCfgGen
(
SwGenCfg        *swGen          /* management structure */
)
#else
PUBLIC S16 swCfgGen(swGen)
SwGenCfg        *swGen;         /* management structure */
#endif
{
   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    swSendLmCfm
*
*       Desc:   Sends configuration, control, statistics and status
*               confirms to the layer manager.
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   sw.c
*
*/
#ifdef ANSI
PUBLIC Void swSendLmCfm
(
Pst             *pst,           /* post */
U8              cfmType,        /* confirm type */
Header          *hdr,           /* header */
U16             status,         /* confirm status */
U16             reason,         /* failure reason */
SwMngmt         *cfm            /* management structure */
)
#else
PUBLIC Void swSendLmCfm(pst, cfmType, hdr, status, reason, cfm)
Pst             *pst;           /* post */
U8              cfmType;        /* confirm type */
Header          *hdr;           /* header */
U16             status;         /* confirm status */
U16             reason;         /* failure reason */
SwMngmt         *cfm;           /* management structure */
#endif
{
   Pst          cfmPst;         /* post structure for confimation */

   TRC2(swSendLmCfm);

   SW_ZERO(&cfmPst, sizeof (Pst));

   cfm->hdr.elmId.elmnt = hdr->elmId.elmnt;
   cfm->hdr.transId     = hdr->transId;

   cfm->cfm.status = status;
   cfm->cfm.reason = reason;

   /* fill up post for confirm */
   cfmPst.srcEnt        = swCb.init.ent;
   cfmPst.srcInst       = swCb.init.inst;
   cfmPst.srcProcId     = swCb.init.procId;
   cfmPst.dstEnt        = pst->srcEnt;
   cfmPst.dstInst       = pst->srcInst;
   cfmPst.dstProcId     = pst->srcProcId;
   cfmPst.selector      = hdr->response.selector;
   cfmPst.prior         = hdr->response.prior;
   cfmPst.route         = hdr->response.route;
   cfmPst.region        = hdr->response.mem.region;
   cfmPst.pool          = hdr->response.mem.pool;

   switch (cfmType)
   {
      case TCFG:
         SwMiLswCfgCfm(&cfmPst, cfm);
         break;

      case TSTS:
         SwMiLswStsCfm(&cfmPst, cfm);
         break;

      case TCNTRL:
         SwMiLswCntrlCfm(&cfmPst, cfm);
         break;

      case TSSTA:
         SwMiLswStaCfm(&cfmPst, cfm);
         break;

      default:
         SWDBGP(DBGMASK_LYR, (swCb.init.prntBuf,
                  "swSendLmCfm(): unknown parameter cfmType\n"));
         break;
   }

   RETVOID;
} /* swSendLmCfm() */

/*
*
*       Fun:    swGetGenSts
*
*       Desc:   Get general statistics.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  Statistics counters are sampled unlocked.
*
*       File:   sw.c
*
*/
#ifdef ANSI
PUBLIC S16 swGetGenSts
(
SwGenSts        *genSts         /* statistics returned */
)
#else
PUBLIC S16 swGetGenSts(genSts)
SwGenSts        *genSts;        /* statistics returned */
#endif
{
   TRC2(swGetGenSts);

   SW_ZERO(genSts, sizeof (SwGenSts));

   /* get receive counters from each group */
   genSts->numErrors = 0;

   RETVALUE(ROK);
} /* swGetGenSts() */


/*
*
*       Fun:    swZeroGenSts
*
*       Desc:   Reset general statistics.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   sw.c
*
*/
#ifdef ANSI
PUBLIC S16 swZeroGenSts
(
Void
)
#else
PUBLIC S16 swZeroGenSts()
#endif
{
   TRC2(swZeroGenSts);

   /* zero the error stats */
   SW_ZERO_ERRSTS();

   RETVALUE(ROK);
} /* swZeroGenSts() */

/*
*
*       Fun:    swCntrlGen
*
*       Desc:   General control requests are used to enable/disable
*               alarms or debug printing and also to shut down
*               Diameter.
*
*       Ret:    LCM_REASON_NOT_APPL             - ok
*               LSW_REASON_OPINPROG             - ok, in progress
*               LCM_REASON_INVALID_ACTION       - failure
*               LCM_REASON_INVALID_SUBACTION    - failure
*               LSW_REASON_DIFF_OPINPROG        - failure
*
*       Notes:  None
*
*       File:   sw.c
*
*/
#ifdef ANSI
PUBLIC S16 swCntrlGen
(
Pst             *pst,           /* post structure */
SwMngmt         *cntrl,         /* control request */
Header          *hdr            /* header */
)
#else
PUBLIC S16 swCntrlGen(pst, cntrl, hdr)
Pst             *pst;           /* post structure */
SwMngmt         *cntrl;         /* control request */
Header          *hdr;           /* header */
#endif
{
   S16          ret;
   U8           action, subAction;
   Bool         invSubAction = FALSE;

   TRC2(swCntrlGen);

   action = cntrl->t.cntrl.action;
   subAction = cntrl->t.cntrl.subAction;

   switch (action)
   {
      case ASHUTDOWN:
         ret = ROK;
         RETVALUE(LCM_REASON_NOT_APPL);


      case AENA:
         if (subAction == SAUSTA)
            swCb.init.usta = TRUE;
#ifdef DEBUGP
         else if (subAction == SADBG)
            swCb.init.dbgMask |= cntrl->t.cntrl.ctlType.swDbg.dbgMask;
#endif
         else
            invSubAction = TRUE;
         break;


      case ADISIMM:
         if (subAction == SAUSTA)
            swCb.init.usta = FALSE;
#ifdef DEBUGP
         else if (subAction == SADBG)
            swCb.init.dbgMask &= ~(cntrl->t.cntrl.ctlType.swDbg.dbgMask);
#endif
         else
            invSubAction = TRUE;
         break;


      default:
         SWLOGERROR_INT_PAR(ESW011, (ErrVal)action, 0,
               "swCntrlGen(): Unknown or unsupported action");
         RETVALUE(LCM_REASON_INVALID_ACTION);
   }

   if (invSubAction)
   {
      SWLOGERROR_INT_PAR(ESW012, (ErrVal)subAction, 0,
            "swCntrlGen(): invalid sub-action specified");
      RETVALUE(LCM_REASON_INVALID_SUBACTION);
   }

   RETVALUE(LCM_REASON_NOT_APPL);
} /* swCntrlGen() */

/*
*
*       Fun:   swGetSid
*
*       Desc:  Get system id consisting of part number, main version and
*              revision and branch version and branch.
*
*       Ret:   TRUE      - ok
*
*       Notes: None
*
*       File:  sw.c
*
*/
#ifdef ANSI
PUBLIC S16 swGetSid
(
SystemId *s                 /* system id */
)
#else
PUBLIC S16 swGetSid(s)
SystemId *s;                /* system id */
#endif
{
   TRC2(swGetSid)

   s->mVer = sId.mVer;
   s->mRev = sId.mRev;
   s->bVer = sId.bVer;
   s->bRev = sId.bRev;
   s->ptNmb = sId.ptNmb;

   RETVALUE(TRUE);

} /* swGetSid */

/*
	*
	*       Fun:    swSendAlarm
*
*       Desc:   Send an unsolicited status indication (alarm) to
*               layer manager.
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   sw.c
*
*/
#ifdef ANSI
PUBLIC Void swSendAlarm
(
U16             cgy,            /* alarm category */
U16             evnt,           /* event */
U16             cause,          /* cause for alarm */
SwAlarmInfo     *info           /* alarm information */
)
#else
PUBLIC Void swSendAlarm(cgy, evnt, cause, info)
U16             cgy;            /* alarm category */
U16             evnt;           /* event */
U16             cause;          /* cause for alarm */
SwAlarmInfo     *info;          /* alarm information */
#endif
{
   SwMngmt      sm;             /* Management structure */

   TRC2(swSendAlarm);

   /* do nothing if unconfigured */
   if (!swCb.init.cfgDone)
   {
      SWDBGP(DBGMASK_LYR, (swCb.init.prntBuf,
               "swSendAlarm(): general configuration not done\n"));
      RETVOID;
   }

   /* send alarms only if configured to do so */
   if (swCb.init.usta)
   {
      SW_ZERO(&sm, sizeof (SwMngmt));

      sm.hdr.elmId.elmnt        = TUSTA;
      sm.hdr.elmId.elmntInst1   = SW_UNUSED;
      sm.hdr.elmId.elmntInst2   = SW_UNUSED;
      sm.hdr.elmId.elmntInst3   = SW_UNUSED;

      sm.t.usta.alarm.category  = cgy;
      sm.t.usta.alarm.event     = evnt;
      sm.t.usta.alarm.cause     = cause;

      cmMemcpy((U8 *)&sm.t.usta.info,
               (U8 *)info, sizeof (SwAlarmInfo));

      (Void)SGetDateTime(&sm.t.usta.alarm.dt);

#ifdef SW_MULTI_THREADED
   SW_LOCK(&swCb.lmPstLock);
#endif
      SwMiLswStaInd(&(swCb.init.lmPst), &sm);
#ifdef SW_MULTI_THREADED
   SW_UNLOCK(&swCb.lmPstLock);
#endif
   }

   RETVOID;
} /* swSendAlarm() */

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/
