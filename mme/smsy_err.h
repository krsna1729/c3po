/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**
  
     Name:     Diameter S6A Convergence Layer Sample Stack Manager
  
     Type:     C include file
  
     Desc:     Error file 
              
     File:     smsy_err.h
  
     Sid:      smsy_err.h@@/main/5 - Mon Mar  3 20:09:28 2008

     Prg:      asa
  
*********************************************************************21*/

#ifndef __SMSYERRH_
#define __SMSYERRH_

/* error codes */
#define SMSYLOGERROR(ent, inst, procId, errCls, errCode, errVal, errDesc) \
        SLogError(ent, inst, procId, __FILE__, __LINE__,                  \
                  (ErrCls)errCls, (ErrCode)errCode, (ErrVal)errVal,       \
                  (Txt*)errDesc)

/* Error codes for TUCL sample layer manager */
#define   ERRSMSYBASE   0
#define   ERRSMSY       ERRSMSYBASE

#define   ESMSY001      (ERRSMSY +    1)    /*   smsyexms.c: 202 */

#define   ESMSY002      (ERRSMSY +    2)    /*   smsyptmi.c: 418 */
#define   ESMSY003      (ERRSMSY +    3)    /*   smsyptmi.c: 461 */
#define   ESMSY004      (ERRSMSY +    4)    /*   smsyptmi.c: 499 */
#define   ESMSY005      (ERRSMSY +    5)    /*   smsyptmi.c: 538 */

#endif /* __SMSYERRH_*/


/********************************************************************30**
 
         End of file:     smsy_err.h@@/main/5 - Mon Mar  3 20:09:28 2008

*********************************************************************31*/
 
/********************************************************************40**
 
        Notes:
 
*********************************************************************41*/
 
/********************************************************************50**
 
*********************************************************************51*/
 
/********************************************************************60**
 
        Revision history:
 
*********************************************************************61*/
/********************************************************************70**
  
  version    initials                   description
-----------  ---------  ------------------------------------------------
 
*********************************************************************71*/
 
/********************************************************************80**
 
*********************************************************************81*/
/********************************************************************90**
 
    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/
