/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:    Diameter Convergence Layer (TUCL)

     Type:    C source file

     Desc:    Upper and Management Interface primitives.

     File:    aq.c

     Sid:

     Prg:     bw

*********************************************************************21*/

/*
Upper Layer functions interface to the HI Layer user.

The following functions are provided in this file:

Upper layer functions

    HiUiHitBndReq        Bind Request
    HiUiHitUbndReq       Unbind Request
    HiUiHitServOpenReq   Server Open Request
    HiUiHitConReq        Connect Request
    HiUiHitConRsp        Connect Response
    HiUiHitDatReq        Data Request(TCP)
    HiUiHitUDatReq       Unit Data Request(UDP)
    HiUiHitDiscReq       Disconnect Request
    HiUiHitTlsEstReq     TLS Establishment Request

Lower layer functions

    socket calls to common socket library

Layer Management functions

    AqMiLaqCfgReq        Configuration Request
    AqMiLaqStsReq        Statistics Request
    AqMiLaqCntrlReq      Control Request
    AqMiLaqStaReq        Solicited Status Request

System Services

    aqActvInit           Activate task - initialize

The following function are expected from the interface providers:

Upper Interface (hi_ptui.c)
    HiUiHitConInd        Connect Indication
    HiUiHitConCfm        Connect Confirm
    HiUiHitBndCfm        Bind Confirm
    HiUiHitDatInd        Data Indication
    HiUiHitUDatInd       Unit Data Indication
    HiUiHitDiscInd       Disconnect Indication
    HiUiHitDiscCfm       Disconnect Confirm
    HiUiHitFlcInd        Flow Control Indication
    HiUiHitTlsEstCfm     Flow Control Indication

Lower Interface (hi_ptli.c)
    dummy file

Layer Management (hi_ptmi.c)
    AqMiLaqCfgCfm        Configuration Confirm
    AqMiLaqStsCfm        Statistics Confirm
    AqMiLaqCntrlCfm      Control Confirm
    AqMiLaqStaCfm        Status Confirm
    AqMiLaqTrcind        Trace Indication

System Services
    SRegTmr        Register timer activation function
    SDeregTTsk     Deregister tapa task
    SDetachTTsk    Detach tapa task

    SInitQueue     Initialize Queue
    SQueueFirst    Queue to First Place
    SQueueLast     Queue to Last Place
    SDequeueFirst  Dequeue from First Place
    SFndLenQueue   Find Length of Queue

    SPutMsg        Put Message
    SFndLenMsg     Find Length of Message

    SAddPreMsg     Add Pre Message
    SAddPstMsg     Add Post Message
    SRemPreMsg     Remove Pre Message
    SRemPstMsg     Remove Post Message

    SAddPreMsgMult Add multiple bytes to head of the Message
    SAddPstMsgMult Add multiple bytes from tail of the Message
    SRemPreMsgMult Remove multiple bytes to head of the Message
    SRemPstMsgMult Remove multiple bytes from tail of the Message

    SGetSBuf       Get Static Buffer
    SPutSBuf       Put Static Buffer
    SCatMsg        Concatenate two messages
    SSegMsg        Segments a message buffer
    SInitMsg       Initialize a message buffer pointer

    SChkRes        Check Resources
    SGetDateTime   Get Date and Time
    SGetSysTime    Get System Time

NOTE: For a complete list, please refer to the TUCL Service
      Definition Document.

*/


/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"
#include "aq_err.h"
#include "lsy.h"
#include "lsy.x"

#include <freeDiameter/freeDiameter-host.h>
#include <freeDiameter/libfdcore.h>
#include <freeDiameter/libfdproto.h>

#include "aqfd.h"
#include "aqfd.x"

#include "sy.h"
#include "sy.x"
#include "sy_err.h"

#define AQSWMV 1            /* Diameter - main version */
#define AQSWMR 0            /* Diameter - main revision */
#define AQSWBV 0            /* Diameter - branch version */
#define AQSWBR 0            /* Diameter - branch revision */

#define AQSWPN "1000159"    /* Diameter - part number */

PRIVATE CONSTANT SystemId sId ={
   AQSWMV,                    /* main version */
   AQSWMR,                    /* main revision */
   AQSWBV,                    /* branch version */
   AQSWBR,                    /* branch revision */
   AQSWPN,                    /* part number */
};

PRIVATE S16 aqCfgGen ARGS((AqGenCfg *aqGen));


/* public variable declarations */

PUBLIC AqCb  aqCb;         /* Diameter control block */


/* interface function to system service */


/*
*
*       Fun:    aqActvInit
*
*       Desc:   Called from SSI to initialize Diameter.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef SS_MULTIPLE_PROCS
#ifdef ANSI
PUBLIC S16 aqActvInit
(
ProcId procId,         /* procId */
Ent entity,            /* entity */
Inst inst,             /* instance */
Region region,         /* region */
Reason reason,         /* reason */
Void **xxCb           /* Protocol Control Block */
)
#else
PUBLIC S16 aqActvInit(procId,entity, inst, region, reason, xxCb)
ProcId procId;         /* procId */
Ent entity;            /* entity */
Inst inst;             /* instance */
Region region;         /* region */
Reason reason;         /* reason */
Void **xxCb;           /* Protocol Control Block */
#endif
#else /* SS_MULTIPLE_PROCS */
#ifdef ANSI
PUBLIC S16 aqActvInit
(
Ent entity,            /* entity */
Inst inst,             /* instance */
Region region,         /* region */
Reason reason          /* reason */
)
#else
PUBLIC S16 aqActvInit(entity, inst, region, reason)
Ent entity;            /* entity */
Inst inst;             /* instance */
Region region;         /* region */
Reason reason;         /* reason */
#endif
#endif /* SS_MULTIPLE_PROCS */
{
   TRC2(aqActvInit);

#ifdef SS_MULTIPLE_PROCS
   AQDBGP(DBGMASK_SI, (aqCb.init.prntBuf,
          "aqActvInit(ProcId(%d), Ent(%d), Inst(%d), Region(%d), Reason(%d))\n",
           procId, entity, inst, region, reason));
#else /* SS_MULTIPLE_PROCS */
   AQDBGP(DBGMASK_SI, (aqCb.init.prntBuf,
          "aqActvInit(Ent(%d), Inst(%d), Region(%d), Reason(%d))\n",
           entity, inst, region, reason));
   AQ_ZERO(&aqCb, sizeof(AqCb));
#endif /* SS_MULTIPLE_PROCS */

   /* initialize aqCb */

   aqCb.init.ent        = entity;
   aqCb.init.inst       = inst;
   aqCb.init.region     = region;
   aqCb.init.reason     = reason;
   aqCb.init.cfgDone    = FALSE;
   aqCb.init.pool       = 0;
#ifdef SS_MULTIPLE_PROCS
   aqCb.init.procId = procId;
#else /* SS_MULTIPLE_PROCS */
   aqCb.init.procId = SFndProcId();
#endif /* SS_MULTIPLE_PROCS */
   aqCb.init.acnt       = FALSE;
   aqCb.init.usta       = TRUE;
   aqCb.init.trc        = FALSE;

   /* hi028.201: Added dbgMask and protected under HI_DEBUG flag*/
#ifdef DEBUGP
   aqCb.init.dbgMask    = 0;
#ifdef HI_DEBUG
   aqCb.init.dbgMask = 0xFFFFFFFF;
#endif
#endif

   aqQueueInit( &aqCb.aiaQueue );
   aqQueueInit( &aqCb.ulaQueue );
   aqQueueInit( &aqCb.puaQueue );
   aqQueueInit( &aqCb.clrQueue );
   aqQueueInit( &aqCb.idrQueue );
   aqQueueInit( &aqCb.ofaQueue );
   aqQueueInit( &aqCb.tfrQueue );
   aqQueueInit( &aqCb.riaQueue );

   RETVALUE(ROK);
} /* end of aqActvInit */

/*
*
*       Fun:    AqMiLaqCfgReq
*
*       Desc:   Configure the layer. Responds with a AqMiLaqCfgCfm
*               primitive.
*
*       Ret:    ROK     - ok
*
*       Notes:  Configuration must be performed in the following
*               sequence:
*               1) general configuration (STGEN)
*               2) transport sap configuration (STTSAP)
*               3) TLS context configuration (STCTX)
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 AqMiLaqCfgReq
(
Pst             *pst,           /* post structure */
AqMngmt         *cfg            /* configuration structure */
)
#else
PUBLIC S16 AqMiLaqCfgReq(pst, cfg)
Pst             *pst;           /* post structure */
AqMngmt         *cfg;           /* configuration structure */
#endif
{
   AqMngmt      cfmMsg;
   S16          ret;


   TRC3(AqMiLaqCfgReq);

#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&aqCbPtr)) !=
      ROK)
   {
      AQLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,EAQ012,(ErrVal)0,pst->dstInst,
            "AqMiLaqCfgReq() failed, cannot derive aqCb");
      RETVALUE(FALSE);
   }
   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
      "---------Diameter------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
          "AqMiLaqCfgReq(pst, elmnt(%d))\n",
           cfg->hdr.elmId.elmnt));

   AQ_ZERO(&cfmMsg, sizeof (AqMngmt));


   /* hi032.201: Removed locking and unloacking of lmpst lock */
#ifdef HI_RUG
   /* hi028:201: Locking mechanism is used before using lmPst*/
   /* store interface version */
   if (!aqCb.init.cfgDone)
      aqCb.init.lmPst.intfVer = pst->intfVer;
#endif


   switch (cfg->hdr.elmId.elmnt)
   {
      case STGEN:            /* general configuration */
         ret = aqCfgGen(&cfg->t.cfg.s.aqGen);
         break;

      default:               /* invalid */
         ret = LCM_REASON_INVALID_ELMNT;
         break;
   }


   /* issue configuration confirm */
   aqSendLmCfm(pst, TCFG, &cfg->hdr, (U16)(ret == LCM_REASON_NOT_APPL ?
               LCM_PRIM_OK : LCM_PRIM_NOK), (U16)ret, &cfmMsg);


   RETVALUE(ROK);
} /* AqMiLaqCfgReq() */


/*
*
*       Fun:    AqMiLaq:StsReq
*
*       Desc:   Get statistics information. Statistics are
*               returned by a AqMiLaqStsCfm primitive. The
*               statistics counters can be reset using the action
*               parameter:
*                  ZEROSTS      - zero statistics counters
*                  NOZEROSTS    - do not zero statistics counters
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 AqMiLaqStsReq
(
Pst             *pst,           /* post structure */
Action          action,         /* action to be done */
AqMngmt         *sts            /* statistics structure */
)
#else
PUBLIC S16 AqMiLaqStsReq(pst, action, sts)
Pst             *pst;           /* post structure */
Action          action;         /* action to be done */
AqMngmt         *sts;           /* statistics structure */
#endif
{
   AqMngmt      cfmMsg;

   TRC3(AqMiLaqStsReq);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&aqCbPtr)) !=
      ROK)
   {
      AQLOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt, EAQ013,(ErrVal)0,pst->dstInst,
            "AqMiLaqStsReq() failed, cannot derive aqCb");
      RETVALUE(FALSE);
   }
   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
      "---------Diameter------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
          "AqMiLaqStsReq(pst, action(%d), elmnt(%d))\n",
           action, sts->hdr.elmId.elmnt));
   AQDBGP(DBGMASK_SI, (aqCb.init.prntBuf,
          "SGetDateTime(DateTime(%p))\n", (Ptr)&cfmMsg.t.sts.dt));

   /* initialize the confirm structure */
   AQ_ZERO(&cfmMsg, sizeof (AqMngmt));
   SGetDateTime(&cfmMsg.t.sts.dt);

  /*hi002.105 - Check if Stack is Configured */
   if(!aqCb.init.cfgDone)
   {
      AQDBGP(DBGMASK_LYR, (aqCb.init.prntBuf,
         "AqMiLaqStsReq(): general configuration not done\n"));
      /* hi003.105 issue a statistics confirm if genCfg not done*/
      aqSendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_NOK, LCM_REASON_GENCFG_NOT_DONE,
               &cfmMsg);

      RETVALUE(ROK);
   }

   switch (sts->hdr.elmId.elmnt)
   {
      case STGEN:            /* general statistics */
         aqGetGenSts(&cfmMsg.t.sts.s.genSts);
         if (action == ZEROSTS)
            aqZeroGenSts();
         break;

      default:
         aqSendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_NOK,
                     LCM_REASON_INVALID_ELMNT, &cfmMsg);
         RETVALUE(ROK);
   }


   /* issue a statistics confirm */
   aqSendLmCfm(pst, TSTS, &sts->hdr, LCM_PRIM_OK, LCM_REASON_NOT_APPL,
               &cfmMsg);


   RETVALUE(ROK);
} /* AqMiLaqStsReq() */


/*
*
*       Fun:    AqMiLaqCntrlReq
*
*       Desc:   Control the specified element: enable or diable
*               trace and alarm (unsolicited status) generation,
*               delete or disable a SAP or a group of SAPs, enable
*               or disable debug printing.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 AqMiLaqCntrlReq
(
Pst             *pst,           /* post structure */
AqMngmt         *ctl            /* pointer to control structure */
)
#else
PUBLIC S16 AqMiLaqCntrlReq(pst, ctl)
Pst             *pst;           /* post structure */
AqMngmt         *ctl;           /* pointer to control structure */
#endif
{
   Header       *hdr;
   AqMngmt      cfmMsg;
   S16          ret;

   TRC3(AqMiLaqCntrlReq);

/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&aqCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,EAQ014,(ErrVal)0,pst->dstInst,
            "AqMiLaqCntrlReq() failed, cannot derive aqCb");
      RETVALUE(FALSE);
   }
   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */
   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
          "AqMiLaqCntrlReq(pst, elmnt(%d))\n", ctl->hdr.elmId.elmnt));
   AQDBGP(DBGMASK_SI, (aqCb.init.prntBuf,
          "SGetDateTime(DateTime(%p))\n", (Ptr)&ctl->t.cntrl.dt));

   hdr = &ctl->hdr;
   AQ_ZERO(&cfmMsg, sizeof (AqMngmt));
   SGetDateTime(&(ctl->t.cntrl.dt));

  /*hi002.105 - Check if Stack is Configured */
   if((!aqCb.init.cfgDone) && (hdr->elmId.elmnt != STGEN))
   {
      AQDBGP(DBGMASK_LYR, (aqCb.init.prntBuf,
         "AqMiLaqCntrlReq(): general configuration not done\n"));
      /* issue a control confirm */
      /* hi003.105 to send cntrl cfm if gen cfg not done */
      aqSendLmCfm(pst, TCNTRL, hdr, LCM_PRIM_NOK, LCM_REASON_GENCFG_NOT_DONE,
               &cfmMsg);

      RETVALUE(ROK);
   }

   switch (hdr->elmId.elmnt)
   {
      case STGEN:
         ret = aqCntrlGen(pst, ctl, hdr);
         break;

      default:
         AQLOGERROR_INT_PAR(EAQ015, hdr->elmId.elmnt, 0,
            "AqMiLaqCntrlReq(): bad element in control request");
         ret = LCM_REASON_INVALID_ELMNT;
         break;
   }

   if (ret == LAQ_REASON_OPINPROG)
   {
      aqSendLmCfm(pst, TCNTRL, hdr, LCM_PRIM_OK_NDONE,
                  LCM_REASON_NOT_APPL, &cfmMsg);
      RETVALUE(ROK);
   }

   /* issue a control confirm primitive */
   aqSendLmCfm(pst, TCNTRL, hdr, (U16)(ret == LCM_REASON_NOT_APPL ?
               LCM_PRIM_OK : LCM_PRIM_NOK), (U16)ret, &cfmMsg);

   RETVALUE(ROK);
} /* AqMiLaqCntrlReq() */


/*
*
*       Fun:    AqMiLaqStaReq
*
*       Desc:   Get status information. Responds with a
*               AqMiLaqStaCfm primitive.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 AqMiLaqStaReq
(
Pst             *pst,           /* post structure */
AqMngmt         *sta            /* management structure */
)
#else
PUBLIC S16 AqMiLaqStaReq(pst, sta)
Pst             *pst;           /* post structure */
AqMngmt         *sta;           /* management structure */
#endif
{
   Header       *hdr;
   AqMngmt      cfmMsg;

   TRC3(AqMiLaqStaReq);

#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&aqCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,EAQ016,(ErrVal)0,pst->dstInst,
            "AqMiLaqStaReq() failed, cannot derive aqCb");
      RETVALUE(FALSE);
   }
   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf, "AqMiLaqStaReq(pst, elmnt(%d))\n",
           sta->hdr.elmId.elmnt));

   hdr = &(sta->hdr);
   AQ_ZERO(&cfmMsg, sizeof (AqMngmt));

   /* hi002.105 - fill the date and time */
   SGetDateTime(&cfmMsg.t.ssta.dt);

   switch (hdr->elmId.elmnt)
   {
      case STSID:               /* system ID */
         aqGetSid(&cfmMsg.t.ssta.s.sysId);
         break;


      default:
         AQLOGERROR_INT_PAR(EAQ018, hdr->elmId.elmnt, 0,
            "AqMiLaqStaReq(): invalid element in status request");
         aqSendLmCfm(pst, TSSTA, hdr, LCM_PRIM_NOK,
                     LCM_REASON_INVALID_ELMNT, &cfmMsg);
         RETVALUE(ROK);
   }




   /* issue a status confirm primitive */
   aqSendLmCfm(pst, TSSTA, hdr, LCM_PRIM_OK, LCM_REASON_NOT_APPL,
               &cfmMsg);


   RETVALUE(ROK);
} /* AqMiLaqStaReq() */

#if 0


/*
*     upper interface (HIT) primitives
*/


/*
*
*       Fun:    HiUiHitBndReq
*
*       Desc:   Binds a service user to TUCL. A TSAP is assigned
*               for this bind and the identity of the service user
*               is recorded. A bind confirm primitive is then
*               issued.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitBndReq
(
Pst            *pst,            /* post structure */
SuId            suId,           /* service user id */
SpId            spId            /* service provider id */
)
#else
PUBLIC S16 HiUiHitBndReq(pst, suId, spId)
Pst            *pst;            /* post structure */
SuId            suId;           /* service user id */
SpId            spId;           /* service provider id */
#endif
{
   HiSap        *sap;
   HiAlarmInfo  alInfo;
#ifdef HI_RUG
   U16          i;
   Bool         found;
#endif


   TRC3(HiUiHitBndReq);

/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&aqCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,EAQ020,(ErrVal)0,pst->dstInst,
            "HiUiHitBndReq() failed, cannot derive aqCb");
      RETVALUE(FALSE);
   }
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */


   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
          "HiUiHitBndReq(pst, suId(%d), spId(%d))\n",
          suId, spId));

   cmMemset((U8*)&alInfo, 0, sizeof(HiAlarmInfo));

   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_BNDREQ, alInfo, sap);


   /* If the SAP is already bound, nothing happens here and a bind
    * confirm is issued.
    */
   if (sap->state == HI_ST_BND)
   {
      HiUiHitBndCfm(&sap->uiPst, suId, CM_BND_OK);
      RETVALUE(ROK);
   }


   /* copy bind configuration parameters */
   sap->suId            = suId;
   sap->uiPst.dstProcId = pst->srcProcId;
   sap->uiPst.dstEnt    = pst->srcEnt;
   sap->uiPst.dstInst   = pst->srcInst;
   sap->uiPst.srcProcId = pst->dstProcId;
   sap->uiPst.srcInst   = pst->dstInst;

   /* duplicate Psts for multi-threaded case */
   HI_DUPLICATE_SAP_PST(sap, sap->uiPst);


#ifdef HI_RUG
   /* find interface version from stored info */
   if (!sap->remIntfValid)
   {
      found = FALSE;
      for (i = 0; i < aqCb.numIntfInfo && !found; i++)
      {
         if (aqCb.intfInfo[i].intf.intfId == HITIF)
         {
            switch (aqCb.intfInfo[i].grpType)
            {
               case SHT_GRPTYPE_ALL:
                  if ((aqCb.intfInfo[i].dstProcId == pst->srcProcId) &&
                      (aqCb.intfInfo[i].dstEnt.ent == pst->srcEnt) &&
                      (aqCb.intfInfo[i].dstEnt.inst == pst->srcInst))
                     found = TRUE;
                     break;

               case SHT_GRPTYPE_ENT:
                  if ((aqCb.intfInfo[i].dstEnt.ent ==pst->srcEnt) &&
                      (aqCb.intfInfo[i].dstEnt.inst == pst->srcInst))
                     found = TRUE;
                     break;

               default:
                  break;
            }
         }
      }

      if (!found)
      {
         AQLOGERROR_INT_PAR(EAQ021, spId, pst->dstInst,
              "HiUiHitBndReq(): remIntfver not available");

         /* generate alarm to layer manager */
         alInfo.spId = spId;
         alInfo.type = LHI_ALARMINFO_PAR_TYPE;
         hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_BNDREQ,
                     LCM_CAUSE_SWVER_NAVAIL, &alInfo);

         /* send NOK to service user layer */
         HiUiHitBndCfm(&sap->uiPst, suId, CM_BND_NOK);

         RETVALUE(ROK);
      }

      sap->uiPst.intfVer = aqCb.intfInfo[i-1].intf.intfVer;
      sap->remIntfValid = TRUE;
   }
#endif /* HI_RUG */


   /* SAP state is now bound and enabled */
   sap->state = HI_ST_BND;


   /* issue a bind confirm */
   HiUiHitBndCfm(&sap->uiPst, suId, CM_BND_OK);


   RETVALUE(ROK);
} /* HiUiHitBndReq() */


/*
*
*       Fun:    HiUiHitUbndReq
*
*       Desc:   Unbind a service user from TUCL. All connections
*               are closed and the TSAP is unassigned.
*
*       Ret:    ROK     - ok
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitUbndReq
(
Pst             *pst,           /* post structure */
SpId            spId,           /* service provider Id */
Reason          reason          /* cause for unbind operation */
)
#else
PUBLIC S16 HiUiHitUbndReq(pst, spId, reason)
Pst             *pst;           /* post structure */
SpId            spId;           /* service provider Id */
Reason          reason;         /* cause for unbind operation */
#endif
{
   S16          ret;
   U16          i;
   HiSap        *sap;
   HiThrMsg     tMsg;
   HiAlarmInfo  alInfo;


   TRC3(HiUiHitUbndReq);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&aqCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,EAQ022,(ErrVal)0,pst->dstInst,
            "HiUiHitUBndReq() failed, cannot derive aqCb");
      RETVALUE(FALSE);
   }
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifndef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */

   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
          "HiUiHitUbndReq(pst, spId(%d), reason(%d))\n",
          spId, reason));

   cmMemset((U8*)&alInfo, 0, sizeof(HiAlarmInfo));

   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_BNDREQ, alInfo, sap);


   /* return if SAP is unbound */
   if (sap->state == HI_ST_UBND)
      RETVALUE(ROK);


   /* change SAP state to configured but not bound */
   sap->state = HI_ST_UBND;


   /* Send a disable SAP message to each group. The group
    * threads will close all connections associated with the
    * SAP.
    */
   tMsg.type = HI_THR_DISSAP;
   tMsg.spId = sap->spId;
   for (i = 0;  i < aqCb.numFdGrps;  i++)
   {
      ret = hiSendThrMsg(i, &tMsg);
      if (ret != ROK)
      {
         /* we're left in an unstable state! */
         AQDBGP(DBGMASK_LYR, (aqCb.init.prntBuf,
            "HiUiHitUbndReq(): send message to group thread failed\n"));
         RETVALUE(RFAILED);
      }
   }


   RETVALUE(ROK);
} /* HiUiHitUbndReq() */


/*
*
*       Fun:    HiUiHitServOpenReq
*
*       Desc:   Open a server on the address provided. TUCL
*               creates a new non-blocking socket and binds it to
*               the specified local address. Any socket options
*               specified in tPar are set. A connection control
*               block is created and assigned to an fd group. The
*               group thread issues a HiUiHitConCfm primitive on
*               success; a disconnect indication is sent in case
*               of any error.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitServOpenReq
(
Pst             *pst,           /* post stucture */
SpId            spId,           /* service provider SAP Id */
UConnId         servConId,      /* service user's connection Id */
CmTptAddr       *servTAddr,     /* transport address of the server */
CmTptParam      *tPar,          /* transport options */
CmIcmpFilter    *icmpFilter,    /* filter parameters */
U8              srvcType        /* service type */
)
#else
PUBLIC S16 HiUiHitServOpenReq(pst, spId, servConId, servTAddr, tPar,
                              icmpFilter, srvcType)
Pst             *pst;           /* post stucture */
SpId            spId;           /* service provider SAP Id */
UConnId         servConId;      /* service user's connection Id */
CmTptAddr       *servTAddr;     /* transport address of the server */
CmTptParam      *tPar;          /* transport options */
CmIcmpFilter    *icmpFilter;    /* filter parameters */
U8              srvcType;       /* service type */
#endif
{
   S16          ret;
   HiSap        *sap;
   HiConCb      *conCb;
   HiAlarmInfo  alInfo;
   U8           type;
   S16          backLog = 0;
 /* hi005.105(hi023.104) - Store the IP TOS*/
   U8             i;         /* loop counter */


   TRC3(HiUiHitServOpenReq);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&aqCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,EAQ023,(ErrVal)0,pst->dstInst,
            "HiUiHitServOpenReq() failed, cannot derive aqCb");
      RETVALUE(FALSE);
   }
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifndef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */


   /* hi029.201: Fix for compilation warning*/
#ifndef BIT_64
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
          "HiUiHitServOpenReq(pst, spId(%d), servConId(%ld), "
          "servTAddr(%p), tPar(%p), srvcType(%d))\n",
          spId, servConId, (Ptr)servTAddr, (Ptr)tPar, srvcType));
#else
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
          "HiUiHitServOpenReq(pst, spId(%d), servConId(%d), "
          "servTAddr(%p), tPar(%p), srvcType(%d))\n",
          spId, servConId, (Ptr)servTAddr, (Ptr)tPar, srvcType));
#endif



   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_SERVOPENREQ, alInfo, sap);


   /* check SAP state */
   HI_CHK_SAPSTATE(sap, LHI_EVENT_SERVOPENREQ, alInfo);


   /* must have sufficient resources to establish a connection */
	/* hi025.201 Gaurd the check resource implementation under the
	 * flag HI_NO_CHK_RES */
#ifndef HI_NO_CHK_RES
   hiChkRes(sap);
   if (sap->resCongStrt)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_OUTOF_RES);
      RETVALUE(ROK);
   }
#endif /* HI_NO_CHK_RES */


   /* initialize alarm information */
   alInfo.spId = spId;
   alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;


#if (ERRCLASS & ERRCLS_INT_PAR)
   /* check server transport address */
   if (!HI_CHK_ADDR(servTAddr))
   {
      AQLOGERROR_INT_PAR(EAQ024, 0, 0,
         "HiUiHitServOpenReq(): invalid server address");
      HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_INV_PAR_VAL);
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_TPT_ADDR;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_SERVOPENREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }

   /* for ICMP, filter must be provided */
   if (((srvcType & 0x0F) == HI_SRVC_RAW_ICMP
            &&  !HI_CHK_ICMPFILTER(icmpFilter)))
   {
      AQLOGERROR_INT_PAR(EAQ025, 0, 0,
         "HiUiHitServOpenReq(): invalid filter parameter");
      HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_INV_PAR_VAL);
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_FILTER_TYPE_COMB;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_SERVOPENREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }

#endif /* ERRCLS_INT_PAR */


   /* allocate and initialize a conCb for this server */
   ret = hiAllocConCb(sap, servConId, srvcType, &type, &conCb);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_SMEM_ALLOC_ERR);
      RETVALUE(RFAILED);
   }


   /* Open and set up the server socket. ICMP is handled
    * separately, later.
    */
   if (conCb->protocol != CM_PROTOCOL_ICMP)
   {
      ret = hiCreateSock(TRUE, type, servTAddr, tPar, conCb);
      if (ret != ROK)
      {
         HI_DISCIND(sap, HI_USER_CON_ID, conCb->suConId, ret);
         HI_FREECONCB(conCb);
         RETVALUE(RFAILED);
      }

      /* need to listen for stream type connections */
      if (type == CM_INET_STREAM)
      {
         if (tPar  &&  tPar->type == CM_TPTPARAM_SOCK)
            backLog = tPar->u.sockParam.listenQSize;


         ret = HI_LISTEN(&conCb->conFd, backLog);
         if (ret != ROK)
         {
            HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_SOCK_LSTN_ERR);
            HI_CLOSE_SOCKET(&conCb->conFd);
            HI_FREECONCB(conCb);
            HI_INC_ERRSTS(aqCb.errSts.sockLstnErr);
            RETVALUE(RFAILED);
         }
      }

   }


#ifdef IPV6_SUPPORTED
   /* for IPv6 addresses, set some connection flags */
   if (servTAddr->type == CM_TPTADDR_IPV6)
   {
      if (conCb->flag & HI_FL_RAW)
         conCb->flag |= HI_FL_RAW_IPV6;
#ifdef IPV6_OPTS_SUPPORTED
      if (conCb->protocol == CM_PROTOCOL_RSVP)
         conCb->ipv6OptsReq = TRUE;
#endif
   }
#endif /* IPV6_SUPPORTED */


   /* set connection state */
   conCb->state = (type == CM_INET_STREAM
                     ? HI_ST_SRV_LISTEN
                     : HI_ST_CONNECTED);


   /* Complete the connection control block: get an spConId and
    * put it in the SAP's connections hash list.
    */
   ret = hiCompleteConCb(conCb);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_CONID_NOT_AVAIL);
      alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;
      hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_SERVOPENREQ,
                  LHI_CAUSE_CONID_NOT_AVAIL, &alInfo);
      if (conCb->protocol != CM_PROTOCOL_ICMP)
         HI_CLOSE_SOCKET(&conCb->conFd);
      HI_FREECONCB(conCb);
      RETVALUE(RFAILED);
   }


   /* ICMP filters specified? */
   if (HI_CHK_ICMPFILTER(icmpFilter))
   {
      /* Process the specified filters and store them in the
       * connection.
       */
      ret = hiProcessIcmpReq(conCb, icmpFilter);
      if (ret != ROK)
      {
         HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_INTERNAL_ERR);
         HI_FREECONCB(conCb);
         RETVALUE(RFAILED);
      }
   }
   /* hi002.105 (hi023.104) - Store the IP TOS*/
   conCb->ipTos= 0xff;
   /* hi018.201: Added tPar Null check */
   for (i = 0; ((tPar != NULLP) && (i < tPar->u.sockParam.numOpts && tPar->type != CM_TPTPARAM_NOTPRSNT)); i++)
   {
           CmSockOpts *sOpts = &(tPar->u.sockParam.sockOpts[i]);
           if (sOpts->option == CM_SOCKOPT_OPT_TOS )
           {
               conCb->ipTos= (U8)sOpts->optVal.value;
               break;
           }
   }



   /* assign the connection to an fd group */
   ret = hiAssignConCb(conCb, HI_THR_ADDCON_CONCFM);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, servConId, HI_INTERNAL_ERR);
      alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;
      hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_SERVOPENREQ,
                  LHI_CAUSE_INTPRIM_ERR, &alInfo);
      if (conCb->protocol != CM_PROTOCOL_ICMP)
         HI_CLOSE_SOCKET(&conCb->conFd);
      HI_FREECONCB(conCb);
      RETVALUE(RFAILED);
   }


   /* update number of connections stat */
   HI_INC_TXSTS(sap, sap->txSts.numCons);


   /* connect confirm is issued from the thread */


   RETVALUE(ROK);
} /* HiUiHitServOpenReq() */


/*
*
*       Fun:    HiUiHitConReq
*
*       Desc:   Open a new client connection. TUCL binds a new
*               non-blocking socket to the local address specified
*               and connects to the remote address specified. A
*               new connection control block is created and
*               assigned to an fd group. The group thread issues a
*               HiUiHitConCfm primitive on success; a
*               disconnect indication is issued to the service
*               user in case of any failure.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitConReq
(
Pst             *pst,           /* post structure */
SpId            spId,           /* service provider id */
UConnId         suConId,        /* service user's connection id */
CmTptAddr       *remAddr,       /* remote address */
CmTptAddr       *localAddr,     /* local address */
CmTptParam      *tPar,          /* transport parameters */
U8              srvcType        /* service type */
)
#else
PUBLIC S16 HiUiHitConReq(pst, spId, suConId, remAddr, localAddr,
                         tPar, srvcType)
Pst             *pst;           /* post structure */
SpId            spId;           /* service provider id */
UConnId         suConId;        /* service user's connection id */
CmTptAddr       *remAddr;       /* remote address */
CmTptAddr       *localAddr;     /* local address */
CmTptParam      *tPar;          /* transport parameters */
U8              srvcType;       /* service type */
#endif
{
   S16          ret;
   HiSap        *sap;
   HiConCb      *conCb;
   HiAlarmInfo  alInfo;
   U8           type;
   U8           srvc;


   TRC3(HiUiHitConReq);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&aqCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,EAQ027,(ErrVal)0,pst->dstInst,
            "HiUiHitConReq() failed, cannot derive aqCb");
      RETVALUE(FALSE);
   }
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifndef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */

#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T ConReq HC->HI, in HI");
#endif


   /* hi029.201: Fix for compilation warning*/
#ifndef BIT_64
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
          "HiUiHitConReq(pst, spId(%d), suConId(%ld), remAddr(%p), "
          "localAddr(%p), tPar(%p), srvcType(%d))\n",
          spId, suConId, (Ptr)remAddr, (Ptr)localAddr, (Ptr)tPar, srvcType));
#else
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
          "HiUiHitConReq(pst, spId(%d), suConId(%d), remAddr(%p), "
          "localAddr(%p), tPar(%p), srvcType(%d))\n",
          spId, suConId, (Ptr)remAddr, (Ptr)localAddr, (Ptr)tPar, srvcType));
#endif


   /* get the predefined service type being requested */
   srvc = srvcType & 0x0F;


   /* initialize alarm information */
   alInfo.spId = spId;
   alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;


   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_CONREQ, alInfo, sap);


   /* check SAP state */
   HI_CHK_SAPSTATE(sap, LHI_EVENT_CONREQ, alInfo);


#if (ERRCLASS & ERRCLS_INT_PAR)
   /* validate the service type requested */
   if (srvc != HI_SRVC_UDP
         &&  srvc != HI_SRVC_UDP_TPKT_HDR
         &&  srvc != HI_SRVC_TCP_TPKT_HDR
         &&  srvc != HI_SRVC_TCP_NO_HDR)
   {
      AQLOGERROR_INT_PAR(EAQ028, srvcType, 0,
         "HiUiHitConReq(): invalid service type");
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_INV_PAR_VAL);
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_TPT_ADDR;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_CONREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }
#endif /* ERRCLS_INT_PAR */


   /* must have sufficient resources to establish a connection */

#ifndef HI_NO_CHK_RES
   hiChkRes(sap);
   if (sap->resCongStrt)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_OUTOF_RES);
      RETVALUE(RFAILED);
   }
#endif /* HI_NO_CHK_RES */

   /* allocate and initialize a conCb for this client */
   ret = hiAllocConCb(sap, suConId, srvcType, &type, &conCb);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_SMEM_ALLOC_ERR);
      RETVALUE(RFAILED);
   }


#if (ERRCLASS & ERRCLS_INT_PAR)
   /* check remote address for TCP clients */
   if (type == CM_INET_STREAM  &&  !HI_CHK_ADDR(remAddr))
   {
      AQLOGERROR_INT_PAR(EAQ029, 0, 0,
         "HiUiHitConReq(): invalid remote address");
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_INV_PAR_VAL);
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_TPT_ADDR;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_CONREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      HI_FREECONCB(conCb);
      RETVALUE(RFAILED);
   }

#endif /* ERRCLS_INT_PAR */


#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T cmInet-calls strt, in HI");
#endif


   /* open a new socket */
   ret = hiCreateSock(FALSE, type, localAddr, tPar, conCb);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, conCb->suConId, ret);
      HI_FREECONCB(conCb);
      RETVALUE(RFAILED);
   }


   /* Issue a connect for stream type connections or if a remote
    * address has been specified for datagram connections.
    */
   conCb->state = HI_ST_CONNECTED;
   if (HI_CHK_ADDR(remAddr))
   {
      ret = HI_CONNECT(&conCb->conFd, remAddr);
      if (ret == RFAILED  ||  ret == RCLOSED)
      {
         HI_DISCIND(sap, HI_USER_CON_ID, conCb->suConId,
                    (ret == RFAILED
                     ? HI_SOCK_CONN_ERR
                     : HI_CON_CLOSED_BY_PEER));
         HI_CLOSE_SOCKET(&conCb->conFd);
         HI_FREECONCB(conCb);
         HI_INC_ERRSTS(aqCb.errSts.sockCnctErr);
         RETVALUE(RFAILED);
      }
      else if (ret == RINPROGRESS  ||  ret == ROKDNA
               ||  ret == RWOULDBLOCK)
         conCb->state = HI_ST_CLT_CONNECTING;

      /* this a proper UDP client? */
      if (conCb->flag & HI_FL_UDP)
         conCb->flag |= HI_FL_UDP_CLT;
   }


#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T cmInet-calls end, in HI");
#endif


   /* store remote address */
   cmMemcpy((U8 *)&conCb->peerAddr, (U8 *)remAddr, sizeof (CmTptAddr));


   /* Complete the connection control block: get an spConId and
    * put it in the SAP's connections hash list.
    */
   ret = hiCompleteConCb(conCb);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_CONID_NOT_AVAIL);
      hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_CONREQ,
                  LHI_CAUSE_CONID_NOT_AVAIL, &alInfo);
      HI_CLOSE_SOCKET(&conCb->conFd);
      HI_FREECONCB(conCb);
      RETVALUE(RFAILED);
   }




   /* assign the connection to an fd group */
   ret = hiAssignConCb(conCb, HI_THR_ADDCON_CONCFM);
   if (ret != ROK)
   {
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_INTERNAL_ERR);
      hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_CONREQ,
                  LHI_CAUSE_INTPRIM_ERR, &alInfo);
      HI_CLOSE_SOCKET(&conCb->conFd);
      HI_FREECONCB(conCb);
      RETVALUE(RFAILED);
   }


   /* update number of connections stat */
   HI_INC_TXSTS(sap, sap->txSts.numCons);


   /* connect confirm is issued from the thread */


   RETVALUE(ROK);
} /* HiUiHitConReq() */


/*
*
*       Fun:    HiUiHitConRsp
*
*       Desc:   Accept the new client connection indicated by a
*               connection indication primitive. The specified
*               suConId is stored in the connection block and data
*               transfer can be initiated.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  If the connection control block corresponding to
*               spConId is not found, TUCL returns silently. This
*               can happen if a disconnect indication has been
*               issued for the connection and the block has been
*               released.
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitConRsp
(
Pst             *pst,           /* post structure */
SpId            spId,           /* service provider id */
UConnId         suConId,        /* service user's connection Id */
UConnId         spConId         /* service provider's connection Id */
)
#else
PUBLIC S16 HiUiHitConRsp(pst, spId, suConId, spConId)
Pst             *pst;           /* post structure */
SpId            spId;           /* service provider id */
UConnId         suConId;        /* service user's connection Id */
UConnId         spConId;        /* service provider's connection Id */
#endif
{
   S16          ret;
   HiSap        *sap;
   HiConCb      *conCb;
   HiAlarmInfo  alInfo;
   HiThrMsg     tMsg;


   TRC3(HiUiHitConRsp);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&aqCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,EAQ031,(ErrVal)0,pst->dstInst,
            "HiUiHitConRsp() failed, cannot derive aqCb");
      RETVALUE(FALSE);
   }
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifndef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */

#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T ConRsp HC->HI, in HI");
#endif


   /* hi029.201:Fix for compilation warning*/
#ifndef BIT_64
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
          "HiUiHitConRsp(pst, spId(%d), suConId(%ld), spConId(%ld))\n",
          spId, suConId, spConId));
#else
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
          "HiUiHitConRsp(pst, spId(%d), suConId(%d), spConId(%d))\n",
          spId, suConId, spConId));
#endif


   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_CONRSP, alInfo, sap);


   /* check SAP state */
   HI_CHK_SAPSTATE(sap, LHI_EVENT_CONRSP, alInfo);


   /* initialize alarm information */
   alInfo.spId = spId;
   alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;


   /* find the connection block */
   ret = hiFindConCb(sap, spConId, &conCb);
   if (ret != ROK)
   {
      /* not found, issue a disconnect indication */
      HI_DISCIND(sap, HI_USER_CON_ID, suConId, HI_INV_PAR_VAL);
      RETVALUE(RFAILED);
   }


#if (ERRCLASS & ERRCLS_DEBUG)
   /* check the connection state */
   if (conCb->state != HI_ST_AW_CON_RSP)
   {
      alInfo.type = LHI_ALARMINFO_CON_STATE;
      alInfo.inf.conState = conCb->state;
      hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_CONRSP,
                  LHI_CAUSE_INV_CON_STATE, &alInfo);
      conCb->state = HI_ST_DISCONNECTING;
      hiDoneWithConCb(conCb);
      tMsg.type = HI_THR_DELCON_DISCIND;
      tMsg.spId = sap->spId;
      tMsg.spConId = conCb->spConId;
      tMsg.disc.reason = HI_INV_CON_STATE;
      hiSendThrMsg(conCb->fdGrpNum, &tMsg);
      RETVALUE(RFAILED);
   }
#endif


   /* Store the provided suConId, shift state, and tell the
    * group thread to start processing this connection.
    */
   conCb->suConId = suConId;
   conCb->state = HI_ST_CONNECTED;
   tMsg.type = HI_THR_RSPCON;
   tMsg.spId = sap->spId;
   tMsg.spConId = conCb->spConId;
   hiSendThrMsg(conCb->fdGrpNum, &tMsg);
   hiDoneWithConCb(conCb);


   /* data transfer is now possible on this connection */


   RETVALUE(ROK);
} /* HiUiHitConRsp() */


/*
*
*       Fun:    HiUiHitDatReq
*
*       Desc:   Transmit data on a stream connection. If the SAP
*               is under flow control, the primitive is dropped.
*               Otherwise, TUCL marks the message boundaries in
*               accordance with RFC 1006 if required for this
*               connection and sends the message. If the entire
*               message cannot be sent, the untransmitted portion
*               is queued. If the total untransmitted data exceeds
*               the threshold specified in the SAP configuration,
*               a flow control indication is issued and the data
*               is dropped.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  If an invalid spConId is specified, the data is
*               dropped silently. In other error cases, a
*               disconnect indication is issued.
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitDatReq
(
Pst             *pst,           /* post structure */
SpId            spId,           /* service provider Id */
UConnId         spConId,        /* service provider's connection Id */
Buffer          *mBuf           /* message buffer */
)
#else
PUBLIC S16 HiUiHitDatReq(pst, spId, spConId, mBuf)
Pst             *pst;           /* post structure */
SpId            spId;           /* service provider Id */
UConnId         spConId;        /* service provider's connection Id */
Buffer          *mBuf;          /* message buffer */
#endif
{
   S16          ret;
   HiSap        *sap;
   HiConCb      *conCb;
   MsgLen       mLen, txLen;
   QLen         qLen;
   Buffer       *qBuf;
   HiAlarmInfo  alInfo;
   HiThrMsg     tMsg;


   TRC3(HiUiHitDatReq);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&aqCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,EAQ032,(ErrVal)0,pst->dstInst,
            "HiUiHitDatReq() failed, cannot derive aqCb");
      RETVALUE(FALSE);
   }
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifndef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */

#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T DatReq HC->HI, in HI");
#endif


   /* hi029.201:Fix for compilation warning*/
#ifndef BIT_64
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
          "HiUiHitDatReq(pst, spId(%d), spConId(%ld), mBuf(%p)))\n",
          spId, spConId, (Ptr)mBuf));
#else
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
          "HiUiHitDatReq(pst, spId(%d), spConId(%d), mBuf(%p)))\n",
          spId, spConId, (Ptr)mBuf));
#endif


   /* initialize alarm information */
   alInfo.spId = spId;
   alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;


   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_DATREQ, alInfo, sap);


   /* check SAP state */
   HI_CHK_SAPSTATE(sap, LHI_EVENT_DATREQ, alInfo);


   /* hi018.201: mLen is initialized to zero */
   mLen = 0;
   /* get length of data payload */
   ret = SFndLenMsg(mBuf, &mLen);

#if (ERRCLASS & ERRCLS_INT_PAR)
   /* validate message */
   if (ret != ROK  ||  mLen < 1)
   {
      AQLOGERROR_INT_PAR(EAQ033, spId, 0,
         "HiUiHitDatReq(): invalid buffer");
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_MBUF;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_DATREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }
#endif


   /* find the connection control block */
   ret = hiFindConCb(sap, spConId, &conCb);
   if (ret != ROK)
   {
      SPutMsg(mBuf);
      HI_DISCIND(sap, HI_PROVIDER_CON_ID, spConId, HI_DATREQ_INVALID_CONID);
      RETVALUE(RFAILED);
   }


   /* must have sufficient resources to send this message */
#ifndef HI_NO_CHK_RES
   hiChkRes(sap);
   if (sap->resCongDrop  ||  conCb->flc)
   {
      /* Drop the message silently. In case of resource
       * congestion, an alarm would have already been issued. In
       * case of flow control, an indication would have already
       * been issued.
       */
      SPutMsg(mBuf);
      hiDoneWithConCb(conCb);
      RETVALUE(RFAILED);
   }
#else
   if (conCb->flc)
   {
      /* Drop the message silently. In case of resource
       * congestion, an alarm would have already been issued. In
       * case of flow control, an indication would have already
       * been issued.
       */
      SPutMsg(mBuf);
      hiDoneWithConCb(conCb);
      RETVALUE(RFAILED);
   }
#endif /* HI_NO_CHK_RES */


   /* check connection state */
   if (conCb->state != HI_ST_CONNECTED
       &&  conCb->state != HI_ST_CONNECTED_NORD)
   {
      SPutMsg(mBuf);
      alInfo.type = LHI_ALARMINFO_CON_STATE;
      alInfo.inf.conState = conCb->state;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_DATREQ,
                  LHI_CAUSE_INV_CON_STATE, &alInfo);
      hiDoneWithConCb(conCb);
      RETVALUE(RFAILED);
   }


#if (ERRCLASS & ERRCLS_INT_PAR)
   /* cannot be a UDP connection */
   if (conCb->flag & HI_FL_UDP)
   {
      SPutMsg(mBuf);
      AQLOGERROR_INT_PAR(EAQ034, spId, 0,
         "HiUiHitDatReq(): invalid connection type for data request");
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_SRVC_TYPE;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_DATREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      hiDoneWithConCb(conCb);
      RETVALUE(RFAILED);
   }
#endif


   /* add in a TPKT header if required for this connection */
   if ((conCb->srvcType & 0x0F) == HI_SRVC_TCP_TPKT_HDR)
   {
      ret = hiAddTPKTHdr(mBuf, &mLen);

#if (ERRCLASS & ERRCLS_ADD_RES)
      if (ret != ROK)
      {
         SPutMsg(mBuf);
         HILOGERROR_ADD_RES(EAQ035, spId, 0,
            "HiUiHitDatReq(): could not add TPKT header");
         hiSendAlarm(LCM_CATEGORY_RESOURCE, LCM_EVENT_DMEM_ALLOC_FAIL,
                     LCM_CAUSE_UNKNOWN, &alInfo);
         hiDoneWithConCb(conCb);
         RETVALUE(RFAILED);
      }
#endif
   }

   /* hi018.201 : qLen is initialized to zero */
   qLen = 0;
   /* If there is nothing in the transmit queue, send the message.
    * This could succeed partially, in which case we have to queue
    * the untransmitted portion. If there is something in the
    * transmit queue, we just queue the whole message.
    */
   HI_GET_TXQLEN(conCb, &qLen);
   if (qLen == 0)
      ret = hiTxMsg(conCb, mBuf, &txLen, &qBuf, &tMsg);
   else
   {
      ret = RWOULDBLOCK;
      qBuf = mBuf;
      txLen = 0;
   }


   /* Here, we have to enqueue the untransmitted part of the
    * message in qBuf (could be the whole message).
    */
   if (ret == RWOULDBLOCK)
   {
      ret = hiEnqueueForTx(conCb, qBuf);
      if (ret == ROK)
         /* hi006.105 : corrected the flow control check */
         hiChkFlc(conCb, 0, 0);
      else
      {
         SPutMsg(qBuf);
         alInfo.type = LHI_ALARMINFO_MEM_ID;
         alInfo.inf.mem.region = sap->uiPst.region;
         alInfo.inf.mem.pool = sap->uiPst.pool;
         hiSendAlarm(LCM_CATEGORY_RESOURCE, LCM_EVENT_DMEM_ALLOC_FAIL,
                     LCM_CAUSE_UNKNOWN, &alInfo);
         tMsg.type = HI_THR_DELCON_DISCIND;
         tMsg.spId = sap->spId;
         tMsg.spConId = conCb->spConId;
         tMsg.disc.reason = HI_OUTOF_RES;
      }
   }


   /* We have a failure here. The terminate message should have
    * been prepared, so we just send it to the group's thread.
    */
   if (ret != ROK  &&  ret != RWOULDBLOCK)
   {
      conCb->state = HI_ST_DISCONNECTING;
      hiSendThrMsg(conCb->fdGrpNum, &tMsg);
   }


   /* finished with the connection */
   hiDoneWithConCb(conCb);


#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T DatReq - End of HiUiHitDatReq, in HI");
#endif


   RETVALUE(ROK);
} /* HiUiHitDatReq() */


/*
*
*       Fun:    HiUiHitUDatReq
*
*       Desc:   Transmit data on a datagram connection.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  If an error occurs, no indication is given to the
*               service user, but an alarm is issued to the layer
*               manager.
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitUDatReq
(
Pst             *pst,           /* post structure */
SpId            spId,           /* service provider Id */
UConnId         spConId,        /* HI connection Id */
CmTptAddr       *remAddr,       /* remote address */
CmTptAddr       *srcAddr,       /* source address */
CmIpHdrParm     *hdrParm,       /* header parameters */
CmTptParam      *tPar,          /* transport parameters */
Buffer          *mBuf           /* message buffer to be sent */
)
#else
PUBLIC S16 HiUiHitUDatReq(pst, spId, spConId, remAddr, srcAddr,
                          hdrParm, tPar, mBuf)
Pst             *pst;           /* post structure */
SpId            spId;           /* service provider Id */
UConnId         spConId;        /* HI connection Id */
CmTptAddr       *remAddr;       /* remote address */
CmTptAddr       *srcAddr;       /* source address */
CmIpHdrParm     *hdrParm;       /* header parameters */
CmTptParam      *tPar;          /* transport parameters */
Buffer          *mBuf;          /* message buffer to be sent */
#endif
{
   S16          ret;
   HiSap        *sap;
   HiAlarmInfo  alInfo;
   MsgLen       mLen, txLen;
   CmInetFd     *sendFd;
   CmTptParam   lclTPar;
   HiConCb      *conCb, con;
   Bool         udpClt, resetTtl;
   HiThrMsg     tMsg;


   TRC3(HiUiHitUDatReq);
/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&aqCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt,EAQ036,(ErrVal)0,pst->dstInst,
            "HiUiHitUDatReq() failed, cannot derive aqCb");
      RETVALUE(FALSE);
   }
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifndef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */

#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T UDatReq HC->HI, in HI");
#endif


   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
          "HiUiHitUDatReq(pst, spId(%d), remAddr(%p), mBuf(%p))\n",
          spId, (Ptr)remAddr, (Ptr)mBuf));


   /* initialize locals */
   alInfo.spId  = spId;
   alInfo.type  = LHI_ALARMINFO_TYPE_NTPRSNT;

   conCb        = NULLP;
   udpClt       = FALSE;
   resetTtl     = FALSE;


   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_UDATREQ, alInfo, sap);


   /* check SAP state */
   HI_CHK_SAPSTATE(sap, LHI_EVENT_UDATREQ, alInfo);

   /* hi018.201 : mLen is initialized to zero */
   mLen = 0;
   /* get length of data payload */
   ret = SFndLenMsg(mBuf, &mLen);


#if (ERRCLASS & ERRCLS_INT_PAR)
   /* validate message */
   if (ret != ROK  ||  mLen < 1)
   {
      AQLOGERROR_INT_PAR(EAQ037, spId, 0,
         "HiUiHitDatReq(): invalid Buffer");
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_MBUF;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_DATREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }

   /* verify the source address */
   if (!HI_CHK_ADDR(srcAddr)  &&  srcAddr->type != CM_TPTADDR_NOTPRSNT)
   {
      SPutMsg(mBuf);
      AQLOGERROR_INT_PAR(EAQ038, 0, 0,
         "HiUiHitUDatReq(): invalid source address");
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_TPT_ADDR;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }

   /* check header parameters */
   if (!HI_CHK_HDRPARM(hdrParm)
       &&  hdrParm->type != CM_HDRPARM_NOTPRSNT)
   {
      SPutMsg(mBuf);
      AQLOGERROR_INT_PAR(EAQ039, 0, 0,
         "HiUiHitUDatReq(): invalid protocol type in header parameters");
      alInfo.type = LHI_ALARMINFO_PAR_TYPE;
      alInfo.inf.parType = LHI_INV_TPT_ADDR;
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
                  LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }

   /* check IPv4 options */
   if (hdrParm->type == CM_HDRPARM_IPV4)
   {
#ifdef IPV4_OPTS_SUPPORTED
      if (hdrParm->u.hdrParmIpv4.ipv4HdrOpt.pres
          &&  hdrParm->u.hdrParmIpv4.ipv4HdrOpt.len > CM_IPV4_OPTS_MAXLEN)
      {
         SPutMsg(mBuf);
         AQLOGERROR_INT_PAR(EAQ040, hdrParm->u.hdrParmIpv4.ipv4HdrOpt.len, 0,
            "HiUiHitUDatReq(): invalid IPv4 options length");
         alInfo.type = LHI_ALARMINFO_PAR_TYPE;
         alInfo.inf.parType = LHI_INV_HDR_PARAM;
         hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
                     LCM_CAUSE_INV_PAR_VAL, &alInfo);
         RETVALUE(RFAILED);
      }
#endif
   }
#endif /* ERRCLS_INT_PAR */


   /* must have sufficient resources to send this message */
#ifndef HI_NO_CHK_RES
   hiChkRes(sap);
   if (sap->resCongDrop)
   {
      /* Drop the message silently. An alarm has already been
       * issued for resource congestion.
       */
      SPutMsg(mBuf);
      RETVALUE(RFAILED);
   }
#endif /* HI_NO_CHK_RES */


   /* If spConId is zero, we use the generic socket to send this
    * message.
    */
   sendFd = NULLP;

   if (spConId == 0)
/* hi002.105 (hi030.104) - If Generic Socket Supported */
#ifndef HI_DISABLE_GENSOCKET
   {
#ifdef IPV6_SUPPORTED
      if (remAddr->type == CM_INET_IPV6ADDR_TYPE)
         sendFd = &aqCb.resv6ConFd;

      else /* assignment below */
#endif

      sendFd = &aqCb.resvConFd;

      /* Copy the fd into a temporary connection block for calling
       * hiSetSockOpt().
       */
      cmMemcpy((U8 *)&con.conFd, (U8 *)sendFd, sizeof (CmInetFd));
   }
#else
   {
      if(mBuf)
         SPutMsg(mBuf);
      hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
               LCM_CAUSE_INV_PAR_VAL, &alInfo);
      RETVALUE(RFAILED);
   }
#endif /*HI_DISABLE_GENSOCKET */

   /* spConId has been specified. Find the correct connection to
    * use for this transmission.
    */
   else
   {
      ret = hiFindConCb(sap, spConId, &conCb);
      if (ret != ROK)
      {
         SPutMsg(mBuf);
         hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
                     LCM_CAUSE_INV_PAR_VAL, &alInfo);
         RETVALUE(RFAILED);
      }


      /* check connection state */
      if (conCb->state != HI_ST_CONNECTED
          &&  conCb->state != HI_ST_CONNECTED_NORD)
      {
         SPutMsg(mBuf);
         alInfo.type = LHI_ALARMINFO_CON_STATE;
         alInfo.inf.conState = conCb->state;
         hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
                     LHI_CAUSE_INV_CON_STATE, &alInfo);
         hiDoneWithConCb(conCb);
         RETVALUE(RFAILED);
      }


      /* get the fd to use */
      sendFd = &conCb->conFd;


      /* is this a true UDP client? */
      if (conCb->flag & HI_FL_UDP_CLT)
         udpClt = TRUE;


#if (ERRCLASS & ERRCLS_INT_PAR)
      /* validate header parameter type against connection type */
#ifdef IPV6_SUPPORTED
      if ((hdrParm->type == CM_HDRPARM_IPV4
           ||  hdrParm->type == CM_HDRPARM_IPV6)
          &&  conCb->flag & HI_FL_TCP)
#else
      if (hdrParm->type == CM_HDRPARM_IPV4
          &&  !(conCb->flag & HI_FL_RAW))
#endif
      {
         SPutMsg(mBuf);
         AQLOGERROR_INT_PAR(EAQ041, (ErrVal)hdrParm->type, 0,
            "HiUiHitUDatReq(): invalid header parameter/protocol combination");
         alInfo.type = LHI_ALARMINFO_PAR_TYPE;
         alInfo.inf.parType = LHI_INV_TPT_ADDR;
         hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_UDATREQ,
                     LCM_CAUSE_INV_PAR_VAL, &alInfo);
         hiDoneWithConCb(conCb);
         RETVALUE(RFAILED);
      }
#endif /* ERRCLS_INT_PAR */


      /* add in a TPKT header if required for this connection */
      if ((conCb->srvcType & 0x0F) == HI_SRVC_UDP_TPKT_HDR)
      {
         ret = hiAddTPKTHdr(mBuf, &mLen);

#if (ERRCLASS & ERRCLS_ADD_RES)
         if (ret != ROK)
         {
            SPutMsg(mBuf);
            HILOGERROR_ADD_RES(EAQ042, spId, 0,
               "HiUiHitUDatReq(): could not add TPKT header");
            hiSendAlarm(LCM_CATEGORY_RESOURCE, LCM_EVENT_DMEM_ALLOC_FAIL,
                        LCM_CAUSE_UNKNOWN, &alInfo);
            hiDoneWithConCb(conCb);
            RETVALUE(RFAILED);
         }
#endif
      }

      /* now, process the header parameters */
      ret = hiProcHdrParm(conCb, srcAddr, remAddr, hdrParm, mBuf, mLen);
      if (ret != ROK)
      {
         SPutMsg(mBuf);
         HILOGERROR_ADD_RES(EAQ043, spId, 0,
            "HiUiHitUDatReq(): could not process header parameters");
         hiSendAlarm(LCM_CATEGORY_RESOURCE, LHI_EVENT_UDATREQ,
                     LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
         hiDoneWithConCb(conCb);
         RETVALUE(RFAILED);
      }
   }


   /* Handle multicast TTL or hop count */
   if (tPar->type == CM_TPTPARAM_SOCK
       &&  tPar->u.sockParam.numOpts > 0
       &&  HI_CHK_MCASTOPT(tPar->u.sockParam.sockOpts[0].option))
   {
      if (conCb)
      {
         if (conCb->mCastTtl != tPar->u.sockParam.sockOpts[0].optVal.value)
         {
            ret = hiSetSockOpt(conCb, tPar);
            if (ret == RNA  ||  ret == RFAILED)
            {
               SPutMsg(mBuf);
               hiSendAlarm(LCM_CATEGORY_RESOURCE, LHI_EVENT_UDATREQ,
                           LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
               hiDoneWithConCb(conCb);
               RETVALUE(RFAILED);
            }

            conCb->mCastTtl = tPar->u.sockParam.sockOpts[0].optVal.value;
         }
      }
      else
      {
         resetTtl = TRUE;
         lclTPar.u.sockParam.numOpts = 1;
         lclTPar.u.sockParam.sockOpts[0].option
            = tPar->u.sockParam.sockOpts[0].option;
         lclTPar.u.sockParam.sockOpts[0].level
            = tPar->u.sockParam.sockOpts[0].level;
         lclTPar.u.sockParam.sockOpts[0].optVal.value = 1;

         ret = hiSetSockOpt(&con, tPar);
         if (ret == RNA  ||  ret == RFAILED)
         {
            SPutMsg(mBuf);
            hiSendAlarm(LCM_CATEGORY_RESOURCE, LHI_EVENT_UDATREQ,
                        LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
            RETVALUE(RFAILED);
         }
      }
   }


#ifdef IPV6_OPTS_SUPPORTED
   /* Copy the source address to hdrParm so that it is available
    * in cmInetSendMsg() to set on the outgoing packet.
    */
   if (srcAddr->type == CM_TPTADDR_IPV6)
   {
      hdrParm->u.hdrParmIpv6.srcAddr6.type = srcAddr->type;
      cmMemcpy((U8 *)&hdrParm->u.hdrParmIpv6.srcAddr6.u.ipv6NetAddr,
               (U8 *)&srcAddr->u.ipv6TptAddr.ipv6NetAddr, 16);
   }
#endif


#ifdef H323_PERF
   TAKE_TIMESTAMP("Before UDatReq/cmInetSendMsg(), in HI");
#endif


   /* Send the message. If the UDP socket is already connected
    * then do not pass a remote address.
    */
   HI_SENDMSG(sendFd, (udpClt ? NULLP : remAddr), mBuf,
              &txLen, (udpClt ? NULLP : hdrParm), ret);


#ifdef IPV6_OPTS_SUPPORTED
   /* free IPv6 header parameters */
   /* hi001.105 - Fixed compiler warning */
   if (hdrParm->type == CM_HDRPARM_IPV6)
      CM_INET_FREE_IPV6_HDRPARM(aqCb.init.region, aqCb.init.pool,
         ((CmInetIpv6HdrParm*)&(hdrParm->u.hdrParmIpv6)));
#endif


#ifdef H323_PERF
   TAKE_TIMESTAMP("After UDatReq/cmInetSendMsg(), in HI");
#endif


   /* check for send errors */
   if (ret != ROK)
   {
      SPutMsg(mBuf);

      tMsg.type = HI_THR_DELCON_DISCIND;
      tMsg.spId = sap->spId;

      /* hi011.201 handling NETWORK failure sending error */
      /* no disconnect for ROUTRES , RWOULDBLOCK and RNETFAILED */
      if (ret == ROUTRES  ||  ret == RWOULDBLOCK || ret == RNETFAILED)
      {
         alInfo.type = LHI_ALARMINFO_MEM_ID;
         alInfo.inf.mem.region = aqCb.init.region;
         alInfo.inf.mem.pool = aqCb.init.pool;
			/* hi005.201  Free the lock in case of error */
         hiDoneWithConCb(conCb);
         hiSendAlarm(LCM_CATEGORY_RESOURCE, LCM_EVENT_DMEM_ALLOC_FAIL,
                     LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
         RETVALUE(RFAILED);
      }

      /* hi005.201 Don't close the non connected UDP in case of Sending error.
		             Insted give alarm Indication to Layer manager and return
		 *           RFAILED.
		 */
      else if (!udpClt)
		{
          HI_INC_ERRSTS(aqCb.errSts.sockTxErr);
          hiDoneWithConCb(conCb);
			 hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_INET_ERR,
			             LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
			 RETVALUE(RFAILED);
      }

      /* We get RCLOSED when an ICMP message is received on a UDP
       * socket. This will only happen with a connected socket,
       * where spConId is valid. We disconnect it.
       */
      else if (ret == RCLOSED)
         tMsg.disc.reason = HI_CON_CLOSED_BY_PEER;

      /* all other errors get an alarm and are disconnected */
      else
      {
         HI_INC_ERRSTS(aqCb.errSts.sockTxErr);
         hiSendAlarm(LCM_CATEGORY_INTERNAL, LHI_EVENT_INET_ERR,
                     LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
         tMsg.disc.reason = HI_SOCK_SEND_ERR;
      }

      /* send the delete connection message to the group thread */
      if (conCb)
      {
		   /* hi005.201 */
         tMsg.spConId = conCb->spConId;
         conCb->state = HI_ST_DISCONNECTING;
         hiDoneWithConCb(conCb);
         hiSendThrMsg(conCb->fdGrpNum, &tMsg);
      }


      RETVALUE(RFAILED);
   }


   /* reset IP multicast TTL if necessary */
   if (resetTtl)
      hiSetSockOpt(&con, &lclTPar);


   /* update statistics and we're done with the connection */
   HI_ADD_TXSTS(sap, sap->txSts.numTxBytes, txLen);
   if (conCb)
   {
      HI_INC_TXSTSMSG(sap, conCb);
      hiDoneWithConCb(conCb);
   }


   /* trace the message if needed and release it; we're done! */
   if (sap->trc)
      hiTrcBuf(sap, LHI_UDP_TXED, mBuf);
   SPutMsg(mBuf);


#ifdef H323_PERF
   TAKE_TIMESTAMP("L/T DatReq - End of HiUiHitUDatReq, in HC");
#endif


   RETVALUE(ROK);
} /* HiUiHitUDatReq() */


/*
*
*       Fun:    HiUiHitDiscReq
*
*       Desc:   Shut down or close the socket identified by conId.
*               TUCL responds with a disconnect confirm on
*               success. This primitive is also used to leave
*               membership of a multicast group for UDP sockets.
*               The action parameter specifies whether the socket
*               should be closed, shut down, or if this is a
*               request to leave a multicast group.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  The connection block is released only if the
*               action parameter indicates that the socket should
*               be closed. If the connection block cannot be
*               found, TUCL returns silently (a disconnect
*               indication may have already been sent).
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 HiUiHitDiscReq
(
Pst             *pst,           /* post structure */
SpId            spId,           /* service provider Id */
U8              choice,         /* choice of user or provider Id */
UConnId         conId,          /* connection Id */
Action          action,         /* action to be performed */
CmTptParam      *tPar           /* transport parameters */
)
#else
PUBLIC S16 HiUiHitDiscReq(pst, spId, choice, conId, action, tPar)
Pst             *pst;           /* post structure */
SpId            spId;           /* service provider Id */
U8              choice;         /* choice of user or provider Id */
UConnId         conId;          /* connection Id */
Action          action;         /* action to be performed */
CmTptParam      *tPar;          /* transport parameters */
#endif
{
   S16          ret;
   HiSap        *sap;
   HiConCb      *conCb;
   HiAlarmInfo  alInfo;
   HiThrMsg     tMsg;


   TRC3(HiUiHitDiscReq);

/* hi002.105 (hi025.104) */
#ifdef SS_MULTIPLE_PROCS
   if((SGetXxCb(pst->dstProcId,pst->dstEnt,pst->dstInst,(Void **)&aqCbPtr)) !=
      ROK)
   {
      HILOGERROR_DEBUGPST(pst->dstProcId,pst->dstEnt, EAQ044,(ErrVal)0,pst->dstInst,
            "HiUiHitDiscReq() failed, cannot derive aqCb");
      RETVALUE(FALSE);
   }
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
      "---------TUCL------(proc(%d),entt(%d),inst(%d))--------\n",
      pst->dstProcId,pst->dstEnt,pst->dstInst));
#endif  /* SS_MULTIPLE_PROCS */

#ifndef SS_MULTIPLE_PROCS
#ifdef DEBUGP
   UNUSED(pst);
#endif
#endif  /* SS_MULTIPLE_PROCS */

   /* hi029.201:Fix for compilation warning*/
#ifndef BIT_64
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
          "HiUiHitDiscReq(pst, spId(%d), choice(%d), conId(%ld), \
          action(%d))\n", spId, choice, conId, action));
#else
   AQDBGP(DBGMASK_UI, (aqCb.init.prntBuf,
          "HiUiHitDiscReq(pst, spId(%d), choice(%d), conId(%d), \
          action(%d))\n", spId, choice, conId, action));
#endif


   /* initialize alarm information */
   alInfo.spId = spId;
   alInfo.type = LHI_ALARMINFO_TYPE_NTPRSNT;


   /* get the SAP */
   HI_CHK_SPID(spId, LHI_EVENT_DISCREQ, alInfo, sap);


   /* check SAP state */
   HI_CHK_SAPSTATE(sap, LHI_EVENT_DISCREQ, alInfo);


#if (ERRCLASS & ERRCLS_INT_PAR)
   if (action == HI_LEAVE_MCAST_GROUP)
   {
      if (tPar->type != CM_TPTPARAM_SOCK
          ||  !HI_CHK_MCASTDRPOPT(tPar->u.sockParam.sockOpts[0].option))
      {
         AQLOGERROR_INT_PAR(EAQ045, 0, 0,
            "HiUiHitDiscReq(): invalid transport parameters");
         HI_DISCCFM(sap, choice, conId, action);
         alInfo.type = LHI_ALARMINFO_PAR_TYPE;
         alInfo.inf.parType = LHI_INV_TPT_PARAM;
         hiSendAlarm(LCM_CATEGORY_INTERFACE, LHI_EVENT_DISCREQ,
                     LCM_CAUSE_INV_PAR_VAL, &alInfo);
         RETVALUE(RFAILED);
      }
   }
#endif


   /* find the connection */
   if (choice == HI_PROVIDER_CON_ID)
      ret = hiFindConCb(sap, conId, &conCb);
   else
      ret = hiFindConCbSuConId(sap, conId, &conCb);

   if (ret != ROK)
   {
      HI_DISCCFM(sap, choice, conId, action);
      RETVALUE(ROK);
   }


   /* check connection state */
   if (conCb->state == HI_ST_DISCONNECTING)
   {
      HI_DISCCFM(sap, choice, conId, action);

      /** hi016.105  1. Given a check for NULLP before
       *                accessing conCb to avoid FMR/W
       */
		/* hi005.201  1. If conCb is not NULL then
		 *               free the conCb lock */
      if(conCb)
         hiDoneWithConCb(conCb);

      RETVALUE(ROK);
   }




   /* We deal with multicast group leaves here; other actions are
    * handled by the group thread.
    */
   if (action == HI_LEAVE_MCAST_GROUP)
   {
      ret = hiSetSockOpt(conCb, tPar);
      if (ret != ROK)
      {
         hiSendAlarm(LCM_CATEGORY_RESOURCE, LHI_EVENT_DISCREQ,
                     LHI_CAUSE_SOCK_SEND_ERR, &alInfo);
         hiDoneWithConCb(conCb);
         RETVALUE(RFAILED);
      }

      /* issue disconnect confirm */
      HI_DISCCFM(sap, choice, conId, action);

      /* done with the connection */
      hiDoneWithConCb(conCb);

      RETVALUE(ROK);
   }


   /* set the connection to disconnecting state */
   conCb->state = HI_ST_DISCONNECTING;

   /* prepare and send a message to the group thread */
   tMsg.type        = HI_THR_DELCON_DISCCFM;
   tMsg.spId        = sap->spId;
   tMsg.spConId     = conCb->spConId;
   tMsg.disc.action = action;
   hiSendThrMsg(conCb->fdGrpNum, &tMsg);


   /* done with the connection */
   hiDoneWithConCb(conCb);


   RETVALUE(ROK);
} /* HiUiHitDiscReq() */

#endif // #if 0

/*
*
*       Fun:    aqCfgGen
*
*       Desc:   Perform general configuration of Diameter. Must be done
*               after aqActvInit() is called, but before any other
*               interaction with Diameter. Reserves static memory for
*               the layer with SGetSMem(), allocates required
*               memory, sets up data structures and starts the
*               receive mechanism (threads/timer/permanent task).
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*               LAQ_REASON_FD_CORE_INITIALIZE_FAIL  - failure
*               LAQ_REASON_FD_CORE_PARSECONF_FAIL   - failure
*               LAQ_REASON_FD_CORE_START_FAIL       - failure
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 aqCfgGen
(
AqGenCfg        *aqGen          /* management structure */
)
#else
PUBLIC S16 aqCfgGen(aqGen)
AqGenCfg        *aqGen;         /* management structure */
#endif
{
   int ret = ROK;

   ret = aqfdInit();
   if (ret == LCM_REASON_NOT_APPL)
      ret = aqfdDictionaryInit();
   if (ret == LCM_REASON_NOT_APPL)
      ret = aqfdS6aInit();
   if (ret == LCM_REASON_NOT_APPL)
      ret = aqfdT6aInit();
   if (ret == LCM_REASON_NOT_APPL)
      ret = aqfdSGdInit();
   if (ret == LCM_REASON_NOT_APPL)
      ret = aqfdRegister();
   if(ret == LCM_REASON_NOT_APPL)
      ret = aqfdStartFd();

   return ret;
}

/*
*
*       Fun:    aqSendLmCfm
*
*       Desc:   Sends configuration, control, statistics and status
*               confirms to the layer manager.
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   hi_bdy2.c
*
*/
#ifdef ANSI
PUBLIC Void aqSendLmCfm
(
Pst             *pst,           /* post */
U8              cfmType,        /* confirm type */
Header          *hdr,           /* header */
U16             status,         /* confirm status */
U16             reason,         /* failure reason */
AqMngmt         *cfm            /* management structure */
)
#else
PUBLIC Void aqSendLmCfm(pst, cfmType, hdr, status, reason, cfm)
Pst             *pst;           /* post */
U8              cfmType;        /* confirm type */
Header          *hdr;           /* header */
U16             status;         /* confirm status */
U16             reason;         /* failure reason */
AqMngmt         *cfm;           /* management structure */
#endif
{
   Pst          cfmPst;         /* post structure for confimation */

   TRC2(aqSendLmCfm);

   AQ_ZERO(&cfmPst, sizeof (Pst));

   cfm->hdr.elmId.elmnt = hdr->elmId.elmnt;
   cfm->hdr.transId     = hdr->transId;

   cfm->cfm.status = status;
   cfm->cfm.reason = reason;

   /* fill up post for confirm */
   cfmPst.srcEnt        = aqCb.init.ent;
   cfmPst.srcInst       = aqCb.init.inst;
   cfmPst.srcProcId     = aqCb.init.procId;
   cfmPst.dstEnt        = pst->srcEnt;
   cfmPst.dstInst       = pst->srcInst;
   cfmPst.dstProcId     = pst->srcProcId;
   cfmPst.selector      = hdr->response.selector;
   cfmPst.prior         = hdr->response.prior;
   cfmPst.route         = hdr->response.route;
   cfmPst.region        = hdr->response.mem.region;
   cfmPst.pool          = hdr->response.mem.pool;

   switch (cfmType)
   {
      case TCFG:
         AqMiLaqCfgCfm(&cfmPst, cfm);
         break;

      case TSTS:
         AqMiLaqStsCfm(&cfmPst, cfm);
         break;

      case TCNTRL:
         AqMiLaqCntrlCfm(&cfmPst, cfm);
         break;

      case TSSTA:
         AqMiLaqStaCfm(&cfmPst, cfm);
         break;

      default:
         AQDBGP(DBGMASK_LYR, (aqCb.init.prntBuf,
                  "aqSendLmCfm(): unknown parameter cfmType\n"));
         break;
   }

   RETVOID;
} /* aqSendLmCfm() */

/*
*
*       Fun:    aqGetGenSts
*
*       Desc:   Get general statistics.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  Statistics counters are sampled unlocked.
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 aqGetGenSts
(
AqGenSts        *genSts         /* statistics returned */
)
#else
PUBLIC S16 aqGetGenSts(genSts)
AqGenSts        *genSts;        /* statistics returned */
#endif
{
   TRC2(aqGetGenSts);

   AQ_ZERO(genSts, sizeof (AqGenSts));

   /* get receive counters from each group */
   genSts->numErrors = aqCb.errSts.diameterErr;
   genSts->numAqReqMsg = aqCb.rxSts.numRxMsg;
   genSts->numAqAnsMsg = aqCb.txSts.numTxMsg;

   RETVALUE(ROK);
} /* haqGetGenSts() */


/*
*
*       Fun:    aqZeroGenSts
*
*       Desc:   Reset general statistics.
*
*       Ret:    ROK     - ok
*               RFAILED - failed
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 aqZeroGenSts
(
Void
)
#else
PUBLIC S16 aqZeroGenSts()
#endif
{
   TRC2(aqZeroGenSts);

   /* zero the error stats */
   AQ_ZERO_ERRSTS();

   RETVALUE(ROK);
} /* aqZeroGenSts() */

/*
*
*       Fun:    aqCntrlGen
*
*       Desc:   General control requests are used to enable/disable
*               alarms or debug printing and also to shut down
*               Diameter.
*
*       Ret:    LCM_REASON_NOT_APPL             - ok
*               LAQ_REASON_OPINPROG             - ok, in progress
*               LCM_REASON_INVALID_ACTION       - failure
*               LCM_REASON_INVALID_SUBACTION    - failure
*               LAQ_REASON_DIFF_OPINPROG        - failure
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC S16 aqCntrlGen
(
Pst             *pst,           /* post structure */
AqMngmt         *cntrl,         /* control request */
Header          *hdr            /* header */
)
#else
PUBLIC S16 aqCntrlGen(pst, cntrl, hdr)
Pst             *pst;           /* post structure */
AqMngmt         *cntrl;         /* control request */
Header          *hdr;           /* header */
#endif
{
   S16          ret;
   U8           action, subAction;
   Bool         invSubAction = FALSE;

   TRC2(aqCntrlGen);

   action = cntrl->t.cntrl.action;
   subAction = cntrl->t.cntrl.subAction;

   switch (action)
   {
      case ASHUTDOWN:
         /* hi003.105 to check for gen cfg */
         if(aqCb.init.cfgDone != TRUE)
            RETVALUE(LCM_REASON_NOT_APPL);
         if (aqCb.pendOp.flag)
            RETVALUE(LAQ_REASON_DIFF_OPINPROG);

#ifdef AQ_MULTI_THREADED
         AQ_LOCK(&aqCb.pendLock);
#endif

         /* store the request details */
         aqCb.pendOp.flag = TRUE;
         aqCb.pendOp.action = action;
         cmMemcpy((U8 *)&aqCb.pendOp.lmPst, (U8 *)pst, sizeof (Pst));
         cmMemcpy((U8 *)&aqCb.pendOp.hdr, (U8 *)hdr, sizeof (Header));
         aqCb.pendOp.numRem = 1;

         /* Shutdown freeDiameter, need to come up with a better way
          * than calling fd_core_wait_shutdown_complete() since it
          * blocks the current thread.
          */

         /* initiate freeDiameter shutdown */
         fd_core_shutdown();

         /* wait for completion of freeDiameter shutdown */
         fd_core_wait_shutdown_complete();

         /* this will need to be removed when non-blocking shutdown
          * is implemented.
          */
         aqCb.pendOp.flag = FALSE;

#ifdef AQ_MULTI_THREADED
         AQ_UNLOCK(&aqCb.pendLock);
#endif

         ret = LAQ_REASON_OPINPROG;
         RETVALUE(ret);


      case AENA:
         if (subAction == SAUSTA)
            aqCb.init.usta = TRUE;
#ifdef DEBUGP
         else if (subAction == SADBG)
            aqCb.init.dbgMask |= cntrl->t.cntrl.ctlType.aqDbg.dbgMask;
#endif
         else
            invSubAction = TRUE;
         break;


      case ADISIMM:
         if (subAction == SAUSTA)
            aqCb.init.usta = FALSE;
#ifdef DEBUGP
         else if (subAction == SADBG)
            aqCb.init.dbgMask &= ~(cntrl->t.cntrl.ctlType.aqDbg.dbgMask);
#endif
         else
            invSubAction = TRUE;
         break;


      default:
         AQLOGERROR_INT_PAR(EAQ073, (ErrVal)action, 0,
               "aqCntrlGen(): Unknown or unsupported action");
         RETVALUE(LCM_REASON_INVALID_ACTION);
   }

   if (invSubAction)
   {
      AQLOGERROR_INT_PAR(EAQ074, (ErrVal)subAction, 0,
            "aqCntrlGen(): invalid sub-action specified");
      RETVALUE(LCM_REASON_INVALID_SUBACTION);
   }

   RETVALUE(LCM_REASON_NOT_APPL);
} /* aqCntrlGen() */

/*
*
*       Fun:   aqGetSid
*
*       Desc:  Get system id consisting of part number, main version and
*              revision and branch version and branch.
*
*       Ret:   TRUE      - ok
*
*       Notes: None
*
*       File:  aq.c
*
*/
#ifdef ANSI
PUBLIC S16 aqGetSid
(
SystemId *s                 /* system id */
)
#else
PUBLIC S16 aqGetSid(s)
SystemId *s;                /* system id */
#endif
{
   TRC2(aqGetSid)

   s->mVer = sId.mVer;
   s->mRev = sId.mRev;
   s->bVer = sId.bVer;
   s->bRev = sId.bRev;
   s->ptNmb = sId.ptNmb;

   RETVALUE(TRUE);

} /* aqGetSid */

/*
*
*       Fun:    aqSendAlarm
*
*       Desc:   Send an unsolicited status indication (alarm) to
*               layer manager.
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC Void aqSendAlarm
(
U16             cgy,            /* alarm category */
U16             evnt,           /* event */
U16             cause,          /* cause for alarm */
AqAlarmInfo     *info           /* alarm information */
)
#else
PUBLIC Void aqSendAlarm(cgy, evnt, cause, info)
U16             cgy;            /* alarm category */
U16             evnt;           /* event */
U16             cause;          /* cause for alarm */
AqAlarmInfo     *info;          /* alarm information */
#endif
{
   AqMngmt      sm;             /* Management structure */


   TRC2(aqSendAlarm);


   /* do nothing if unconfigured */
   if (!aqCb.init.cfgDone)
   {
      AQDBGP(DBGMASK_LYR, (aqCb.init.prntBuf,
               "aqSendAlarm(): general configuration not done\n"));
      RETVOID;
   }


   /* send alarms only if configured to do so */
   if (aqCb.init.usta)
   {
      AQ_ZERO(&sm, sizeof (AqMngmt));

      sm.hdr.elmId.elmnt        = TUSTA;
      sm.hdr.elmId.elmntInst1   = AQ_UNUSED;
      sm.hdr.elmId.elmntInst2   = AQ_UNUSED;
      sm.hdr.elmId.elmntInst3   = AQ_UNUSED;

      sm.t.usta.alarm.category  = cgy;
      sm.t.usta.alarm.event     = evnt;
      sm.t.usta.alarm.cause     = cause;

      cmMemcpy((U8 *)&sm.t.usta.info,
               (U8 *)info, sizeof (AqAlarmInfo));

      (Void)SGetDateTime(&sm.t.usta.alarm.dt);

#ifdef AQ_MULTI_THREADED
   AQ_LOCK(&aqCb.lmPstLock);
#endif
      AqMiLaqStaInd(&(aqCb.init.lmPst), &sm);
#ifdef AQ_MULTI_THREADED
   AQ_UNLOCK(&aqCb.lmPstLock);
#endif
   }

   RETVOID;
} /* aqSendAlarm() */

/*
*
*       Fun:    aqQueueInit
*
*       Desc:   Initialize a queue.
*
*       Ret:    int
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC int aqQueueInit
(
AqQueue *pqueue
)
#else
PUBLIC int aqQueueInit(pqueue)
AqQueue *pqueue;
#endif
{
   pqueue->queueHead = NULL;
   pqueue->queueTail = NULL;
   return pthread_mutex_init( &pqueue->queueMutex, NULL );
}

/*
*
*       Fun:    aqQueuePush
*
*       Desc:   Puish the node to the head.
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC Void aqQueuePush
(
AqQueue *pqueue,
void *pdata
)
#else
PUBLIC Void aqQueuePush(pqueue, pdata)
AqQueue *pqueue;
void *pdata;
#endif
{
   AqQueueNode *pnode = (AqQueueNode*)malloc(sizeof(AqQueueNode));
   pnode->pdata = pdata;

   pthread_mutex_lock(&pqueue->queueMutex);

   pnode->pprev = NULL;
   pnode->pnext = pqueue->queueHead;

   if (pqueue->queueHead)
      pqueue->queueHead->pprev = pnode;
   pqueue->queueHead = pnode;

   if (!pqueue->queueTail)
      pqueue->queueTail = pnode;

   pthread_mutex_unlock(&pqueue->queueMutex);
}

/*
*
*       Fun:    aqQueuePop
*
*       Desc:   Pop the next node from the tail.
*
*       Ret:    Void *
*
*       Notes:  None
*
*       File:   aq.c
*
*/
#ifdef ANSI
PUBLIC Void *aqQueuePop
(
AqQueue *pqueue
)
#else
PUBLIC Void *aqQueuePop(pqueue)
AqQueue *pqueue;
#endif
{
   Void *pdata = NULL;

   pthread_mutex_lock(&pqueue->queueMutex);

   if (pqueue->queueTail)
   {
      AqQueueNode *pnode = pqueue->queueTail;

      if (pqueue->queueHead == pqueue->queueTail)
         pqueue->queueHead = NULL;

      pqueue->queueTail = pnode->pprev;

      pdata = pnode->pdata;

      free(pnode);
   }

   pthread_mutex_unlock(&pqueue->queueMutex);

   return pdata;
}

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/
