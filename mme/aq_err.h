/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**
 
     Name:     TCP UDP Convergence Layer error file.

     Type:     C include file

     Desc:     Error Hash Defines required by HI layer

     File:     aq_err.h

     Sid:      

     Prg:      bw

*********************************************************************21*/
  
#ifndef __AQERRH__
#define __AQERRH__



#if (ERRCLASS & ERRCLS_DEBUG)
#define AQLOGERROR_DEBUGPST(procId, ent, errCode, errVal, inst, errDesc)        \
        SLogError(ent, inst, procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,     \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define AQLOGERROR_DEBUGPST(procId, ent, errCode, errVal, inst, errDesc)
#endif /* ERRCLS_DEBUG */

/* hi031.201: Fix for g++ compilation warning*/
#if (ERRCLASS & ERRCLS_DEBUG)
#define AQLOGERROR_DEBUG(errCode, errVal, inst, errDesc)        \
        SLogError(aqCb.init.ent, inst, aqCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define AQLOGERROR_DEBUG(errCode, errVal, inst, errDesc)
#endif

#if (ERRCLASS & ERRCLS_INT_PAR)
#define AQLOGERROR_INT_PAR(errCode, errVal, inst, errDesc)      \
        SLogError(aqCb.init.ent, inst, aqCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_INT_PAR,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define AQLOGERROR_INT_PAR(errCode, errVal, inst, errDesc)
#endif /* ERRCLS_INT_PAR */

#if (ERRCLASS & ERRCLS_ADD_RES)
#define AQLOGERROR_ADD_RES(errCode, errVal, inst, errDesc)      \
        SLogError(aqCb.init.ent, inst, aqCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_ADD_RES,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define AQLOGERROR_ADD_RES(errCode, errVal, errDesc, inst)
#endif /* ERRCLS_ADD_RES */

#define   EAQBASE     0
#define   ERRAQ       (EAQBASE   +0)    /* reserved */
 
#define   EAQ001      (ERRAQ +    1)    /*   hi_accsh.c: 203 */
#define   EAQ002      (ERRAQ +    2)    /*   hi_accsh.c: 288 */
#define   EAQ003      (ERRAQ +    3)    /*   hi_accsh.c: 290 */

#define   EAQ004      (ERRAQ +    4)    /*   hi_ex_ms.c: 204 */
#define   EAQ005      (ERRAQ +    5)    /*   hi_ex_ms.c: 242 */
#define   EAQ006      (ERRAQ +    6)    /*   hi_ex_ms.c: 394 */
#define   EAQ007      (ERRAQ +    7)    /*   hi_ex_ms.c: 414 */
#define   EAQ008      (ERRAQ +    8)    /*   hi_ex_ms.c: 422 */

#define   EAQ009      (ERRAQ +    9)    /*         hi.h: 357 */
#define   EAQ010      (ERRAQ +   10)    /*         hi.h: 372 */
#define   EAQ011      (ERRAQ +   11)    /*         hi.h: 382 */

#define   EAQ012      (ERRAQ +   12)    /*    tl_bdy1.c: 434 */
#define   EAQ013      (ERRAQ +   13)    /*    tl_bdy1.c: 532 */
#define   EAQ014      (ERRAQ +   14)    /*    tl_bdy1.c: 653 */
#define   EAQ015      (ERRAQ +   15)    /*    tl_bdy1.c: 698 */
#define   EAQ016      (ERRAQ +   16)    /*    tl_bdy1.c: 759 */
#define   EAQ017      (ERRAQ +   17)    /*    tl_bdy1.c: 811 */
#define   EAQ018      (ERRAQ +   18)    /*    tl_bdy1.c: 837 */
#define   EAQ019      (ERRAQ +   19)    /*    tl_bdy1.c: 896 */
#define   EAQ020      (ERRAQ +   20)    /*    tl_bdy1.c:1109 */
#define   EAQ021      (ERRAQ +   21)    /*    tl_bdy1.c:1183 */
#define   EAQ022      (ERRAQ +   22)    /*    tl_bdy1.c:1257 */
#define   EAQ023      (ERRAQ +   23)    /*    tl_bdy1.c:1374 */
#define   EAQ024      (ERRAQ +   24)    /*    tl_bdy1.c:1421 */
#define   EAQ025      (ERRAQ +   25)    /*    tl_bdy1.c:1435 */
#define   EAQ026      (ERRAQ +   26)    /*    tl_bdy1.c:1454 */
#define   EAQ027      (ERRAQ +   27)    /*    tl_bdy1.c:1668 */
#define   EAQ028      (ERRAQ +   28)    /*    tl_bdy1.c:1721 */
#define   EAQ029      (ERRAQ +   29)    /*    tl_bdy1.c:1755 */
#define   EAQ030      (ERRAQ +   30)    /*    tl_bdy1.c:1775 */
#define   EAQ031      (ERRAQ +   31)    /*    tl_bdy1.c:1960 */
#define   EAQ032      (ERRAQ +   32)    /*    tl_bdy1.c:2104 */
#define   EAQ033      (ERRAQ +   33)    /*    tl_bdy1.c:2149 */
#define   EAQ034      (ERRAQ +   34)    /*    tl_bdy1.c:2204 */
#define   EAQ035      (ERRAQ +   35)    /*    tl_bdy1.c:2225 */
#define   EAQ036      (ERRAQ +   36)    /*    tl_bdy1.c:2358 */
#define   EAQ037      (ERRAQ +   37)    /*    tl_bdy1.c:2408 */
#define   EAQ038      (ERRAQ +   38)    /*    tl_bdy1.c:2421 */
#define   EAQ039      (ERRAQ +   39)    /*    tl_bdy1.c:2435 */
#define   EAQ040      (ERRAQ +   40)    /*    tl_bdy1.c:2452 */
#define   EAQ041      (ERRAQ +   41)    /*    tl_bdy1.c:2560 */
#define   EAQ042      (ERRAQ +   42)    /*    tl_bdy1.c:2581 */
#define   EAQ043      (ERRAQ +   43)    /*    tl_bdy1.c:2596 */
#define   EAQ044      (ERRAQ +   44)    /*    tl_bdy1.c:2825 */
#define   EAQ045      (ERRAQ +   45)    /*    tl_bdy1.c:2864 */
#define   EAQ046      (ERRAQ +   46)    /*    tl_bdy1.c:2916 */
#define   EAQ047      (ERRAQ +   47)    /*    tl_bdy1.c:3027 */
#define   EAQ048      (ERRAQ +   48)    /*    tl_bdy1.c:3063 */
#define   EAQ049      (ERRAQ +   49)    /*    tl_bdy1.c:3077 */
#define   EAQ050      (ERRAQ +   50)    /*    tl_bdy1.c:3284 */
#define   EAQ051      (ERRAQ +   51)    /*    tl_bdy1.c:3316 */
#define   EAQ052      (ERRAQ +   52)    /*    tl_bdy1.c:3375 */
#define   EAQ053      (ERRAQ +   53)    /*    tl_bdy1.c:3482 */
#define   EAQ054      (ERRAQ +   54)    /*    tl_bdy1.c:3938 */
#define   EAQ055      (ERRAQ +   55)    /*    tl_bdy1.c:4240 */
#define   EAQ056      (ERRAQ +   56)    /*    tl_bdy1.c:4412 */
#define   EAQ057      (ERRAQ +   57)    /*    tl_bdy1.c:4618 */
#define   EAQ058      (ERRAQ +   58)    /*    tl_bdy1.c:4748 */
#define   EAQ059      (ERRAQ +   59)    /*    tl_bdy1.c:4888 */
#define   EAQ060      (ERRAQ +   60)    /*    tl_bdy1.c:4925 */
#define   EAQ061      (ERRAQ +   61)    /*    tl_bdy1.c:4940 */
#define   EAQ062      (ERRAQ +   62)    /*    tl_bdy1.c:4952 */
#define   EAQ063      (ERRAQ +   63)    /*    tl_bdy1.c:5077 */

#define   EAQ064      (ERRAQ +   64)    /*    tl_bdy2.c: 220 */
#define   EAQ065      (ERRAQ +   65)    /*    tl_bdy2.c: 243 */
#define   EAQ066      (ERRAQ +   66)    /*    tl_bdy2.c: 297 */
#define   EAQ067      (ERRAQ +   67)    /*    tl_bdy2.c: 310 */
#define   EAQ068      (ERRAQ +   68)    /*    tl_bdy2.c:1445 */
#define   EAQ069      (ERRAQ +   69)    /*    tl_bdy2.c:1545 */
#define   EAQ070      (ERRAQ +   70)    /*    tl_bdy2.c:1552 */
#define   EAQ071      (ERRAQ +   71)    /*    tl_bdy2.c:1559 */
#define   EAQ072      (ERRAQ +   72)    /*    tl_bdy2.c:1576 */
#define   EAQ073      (ERRAQ +   73)    /*    tl_bdy2.c:2119 */
#define   EAQ074      (ERRAQ +   74)    /*    tl_bdy2.c:2126 */
#define   EAQ075      (ERRAQ +   75)    /*    tl_bdy2.c:2191 */
#define   EAQ076      (ERRAQ +   76)    /*    tl_bdy2.c:2207 */
#define   EAQ077      (ERRAQ +   77)    /*    tl_bdy2.c:2221 */
#define   EAQ078      (ERRAQ +   78)    /*    tl_bdy2.c:2230 */
#define   EAQ079      (ERRAQ +   79)    /*    tl_bdy2.c:2301 */
#define   EAQ080      (ERRAQ +   80)    /*    tl_bdy2.c:2359 */
#define   EAQ081      (ERRAQ +   81)    /*    tl_bdy2.c:2411 */
#define   EAQ082      (ERRAQ +   82)    /*    tl_bdy2.c:3174 */
#define   EAQ083      (ERRAQ +   83)    /*    tl_bdy2.c:3188 */
#define   EAQ084      (ERRAQ +   84)    /*    tl_bdy2.c:3302 */
#define   EAQ085      (ERRAQ +   85)    /*    tl_bdy2.c:5146 */
#define   EAQ086      (ERRAQ +   86)    /*    tl_bdy2.c:5186 */
#define   EAQ087      (ERRAQ +   87)    /*    tl_bdy2.c:5321 */

#define   EAQ088      (ERRAQ +   88)    /*    tl_bdy3.c:1800 */
#define   EAQ089      (ERRAQ +   89)    /*    tl_bdy3.c:1815 */
#define   EAQ090      (ERRAQ +   90)    /*    tl_bdy3.c:1851 */
#define   EAQ091      (ERRAQ +   91)    /*    tl_bdy3.c:2531 */
#define   EAQ092      (ERRAQ +   92)    /*    tl_bdy3.c:4364 */
#define   EAQ093      (ERRAQ +   93)    /*    tl_bdy3.c:4377 */
#define   EAQ094      (ERRAQ +   94)    /*    tl_bdy3.c:4391 */
#define   EAQ095      (ERRAQ +   95)    /*    tl_bdy3.c:4425 */
#define   EAQ096      (ERRAQ +   96)    /*    tl_bdy3.c:4441 */
/* hi005.201 Introducing a new error macros for conjestion timer 
 *           changes */

#define   EAQ097      (ERRAQ +   97)    /*    tl_bdy2.c:5729 */
#define   EAQ098      (ERRAQ +   98)    /*    tl_bdy2.c:5735 */
#define   EAQ099      (ERRAQ +   99)    /*    tl_bdy2.c:5753 */
#define   EAQ100      (ERRAQ +   100)    /*    tl_bdy2.c:5753 */
/* hi009.201  rss   1. Added EAQ101 macro */
#define   EAQ101      (ERRAQ +   101)    /*    tl_bdy2.c:5753 */
#endif /* __AQERRH__ */
 


/********************************************************************30**
 
         End of file:     hi_err.h@@/main/6 - Mon Mar  3 20:09:49 2008

*********************************************************************31*/
 

/********************************************************************40**
 
        Notes:
 
*********************************************************************41*/
 
/********************************************************************50**
 
*********************************************************************51*/
 

/********************************************************************60**
 
        Revision history:
 
*********************************************************************61*/
/********************************************************************70**
  
  version    initials                   description
-----------  ---------  ------------------------------------------------
 
*********************************************************************71*/
 
/********************************************************************80**
 
*********************************************************************81*/
/********************************************************************90**
 
    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

