/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     EPC DNS Convergence Layer error file.

     Type:     C include file

     Desc:     Error Hash Defines required by DS layer

     File:     ds_err.h

     Sid:

     Prg:      bw

*********************************************************************21*/

#ifndef __DSERRH__
#define __DSERRH__



#if (ERRCLASS & ERRCLS_DEBUG)
#define DSLOGERROR_DEBUGPST(procId, ent, errCode, errVal, inst, errDesc)        \
        SLogError(ent, inst, procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,     \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define DSLOGERROR_DEBUGPST(procId, ent, errCode, errVal, inst, errDesc)
#endif /* ERRCLS_DEBUG */

/* hi031.201: Fix for g++ compilation warning*/
#if (ERRCLASS & ERRCLS_DEBUG)
#define DSLOGERROR_DEBUG(errCode, errVal, inst, errDesc)        \
        SLogError(dsCb.init.ent, inst, dsCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define DSLOGERROR_DEBUG(errCode, errVal, inst, errDesc)
#endif

#if (ERRCLASS & ERRCLS_INT_PAR)
#define DSLOGERROR_INT_PAR(errCode, errVal, inst, errDesc)      \
        SLogError(dsCb.init.ent, inst, dsCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_INT_PAR,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define DSLOGERROR_INT_PAR(errCode, errVal, inst, errDesc)
#endif /* ERRCLS_INT_PAR */

#if (ERRCLASS & ERRCLS_ADD_RES)
#define DSLOGERROR_ADD_RES(errCode, errVal, inst, errDesc)      \
        SLogError(dsCb.init.ent, inst, dsCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_ADD_RES,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define DSLOGERROR_ADD_RES(errCode, errVal, errDesc, inst)
#endif /* ERRCLS_ADD_RES */

/* Error codes */
#define   EDSBASE     0             /* reserved */
#define   ERRDS       (EDSBASE)

#define   EDS001      (ERRDS +    1)
#define   EDS002      (ERRDS +    2)
#define   EDS003      (ERRDS +    3)
#define   EDS004      (ERRDS +    4)
#define   EDS005      (ERRDS +    5)
#define   EDS006      (ERRDS +    6)
#define   EDS007      (ERRDS +    7)
#define   EDS008      (ERRDS +    8)
#define   EDS009      (ERRDS +    9)
#define   EDS010      (ERRDS +   10)
#define   EDS011      (ERRDS +   11)
#define   EDS012      (ERRDS +   12)

#endif /* __DSERRH__ */



/********************************************************************30**

         End of file:

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

