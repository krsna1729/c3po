/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


#ifndef __AQFDH__
#define __AQFDH__

/*NOLOG*/
#ifdef NOLOG
#define AQFD_DUMP_MESSAGE(a)
#else
#define AQFD_DUMP_MESSAGE(a) \
if (vbMmeCb.init.dbgMask & LVB_DBGMASK_INFO) \
{ \
   char * buf = NULL; \
   size_t len = 0; \
   printf("%s\n", fd_msg_dump_treeview(&buf, &len, NULL, a, fd_g_config->cnf_dict, 0, 1)); \
   free(buf); \
}
#endif

#define AQCHECK_FCT(a, b) \
   CHECK_FCT_DO(a, return b)

#define AQCHECK_FCT_2(a) \
{ \
   S16 __ret__ = a; \
   if (__ret__ != LCM_REASON_NOT_APPL) \
      return __ret__; \
}

#define AQCHECK_FCT_DICT_GETVAL(a, b) \
   CHECK_FCT_DO(fd_dict_getval(a,b), return LAQ_REASON_FD_DICT_GETVAL)

#define AQCHECK_FCT_DICT(a, b, c, d) \
   CHECK_FCT_DO(fd_dict_search(fd_g_config->cnf_dict, a, b, (void*)c, &d, ENOENT), return LAQ_REASON_FD_DICT_SEARCH)

#define AQCHECK_FCT_REGISTER_CALLBACK(a, b) \
   data.command = b; \
   CHECK_FCT_DO(fd_disp_register(a, DISP_HOW_CC, &data, NULL, NULL), return LAQ_REASON_FD_REGISTER_CALLBACK)

#define AQCHECK_FCT_REGISTER_APPLICATION(a, b) \
   CHECK_FCT_DO(fd_disp_app_support(a, b, 1, 0), return LAQ_REASON_FD_REGISTER_APPLICATION)

#define AQCHECK_MSG_SEND(a, b, c) \
   CHECK_FCT_DO(fd_msg_send(a,b,c), return LAQ_REASON_FD_MSG_SEND_FAIL)

#define AQCHECK_MSG_ADD_ORIGIN(a) \
   CHECK_FCT_DO(fd_msg_add_origin(a, 0), return LAQ_REASON_FD_MSG_ADD_ORIGIN_FAIL)

#define AQCHECK_MSG_NEW(a, b) \
   CHECK_FCT_DO(fd_msg_new(a, MSGFL_ALLOC_ETEID, &b), return LAQ_REASON_FD_MSG_NEW_FAIL)

#define AQCHECK_MSG_NEW_APPL(a, b, c) \
   CHECK_FCT_DO(fd_msg_new_appl(a, b, MSGFL_ALLOC_ETEID, &c), return LAQ_REASON_FD_MSG_NEW_FAIL)

#define AQCHECK_MSG_NEW_ANSWER_FROM_REQ(a, b) \
   CHECK_FCT_DO(fd_msg_new_answer_from_req(a, &b, 0), return LAQ_REASON_FD_MSG_NEW_FAIL)

#define AQCHECK_MSG_ADD_AVP_GROUPED_2(a, b, c, d) \
{ \
   struct avp * ___avp___; \
   CHECK_FCT_DO(fd_msg_avp_new(a, 0, &___avp___), return LAQ_REASON_FD_AVP_NEW_FAIL); \
   CHECK_FCT_DO(fd_msg_avp_add(b, c, ___avp___), return LAQ_REASON_FD_AVP_ADD_FAIL); \
   d = ___avp___; \
}

#define AQCHECK_MSG_ADD_AVP_GROUPED(a, b, c) \
{ \
   struct avp * __avp__; \
   AQCHECK_MSG_ADD_AVP_GROUPED_2(a, b, c, __avp__); \
   (void)__avp__; \
}

#define AQCHECK_MSG_ADD_AVP(a, b, c, d, e) \
{ \
   struct avp * ___avp___; \
   CHECK_FCT_DO(fd_msg_avp_new(a, 0, &___avp___), return LAQ_REASON_FD_AVP_NEW_FAIL); \
   CHECK_FCT_DO(fd_msg_avp_setvalue(___avp___, d), return LAQ_REASON_FD_AVP_SETVALUE_FAIL); \
   CHECK_FCT_DO(fd_msg_avp_add(b, c, ___avp___), return LAQ_REASON_FD_AVP_ADD_FAIL); \
   e = ___avp___; \
}

#define AQCHECK_MSG_ADD_AVP_OSTR_2(a, b, c, d, e, f) \
{ \
   union avp_value __val__; \
   __val__.os.data = (unsigned char *)d; \
   __val__.os.len = e; \
   AQCHECK_MSG_ADD_AVP(a, b, c, &__val__, f); \
}

#define AQCHECK_MSG_ADD_AVP_OSTR(a, b, c, d, e) \
{ \
   struct avp * __avp__; \
   AQCHECK_MSG_ADD_AVP_OSTR_2(a, b, c, d, e, __avp__); \
   (void)__avp__;\
}

#define AQCHECK_MSG_ADD_AVP_STR_2(a, b, c, d, e) \
{ \
   union avp_value val; \
   val.os.data = (unsigned char *)d; \
   val.os.len = osStrlen((const S8 *)d); \
   AQCHECK_MSG_ADD_AVP(a, b, c, &val, e); \
}

#define AQCHECK_MSG_ADD_AVP_STR(a, b, c, d) \
{ \
   struct avp * __avp__; \
   AQCHECK_MSG_ADD_AVP_STR_2(a, b, c, d, __avp__); \
   (void)__avp__;\
}

#define AQCHECK_MSG_ADD_AVP_S32_2(a, b, c, d, e) \
{ \
   union avp_value val; \
   val.i32 = d; \
   AQCHECK_MSG_ADD_AVP(a, b, c, &val, e); \
}

#define AQCHECK_MSG_ADD_AVP_S32(a, b, c, d) \
{ \
   struct avp * __avp__; \
   AQCHECK_MSG_ADD_AVP_S32_2(a, b, c, d, __avp__); \
   (void)__avp__; \
}

#define AQCHECK_MSG_ADD_AVP_U32_2(a, b, c, d, e) \
{ \
   union avp_value val; \
   val.u32 = d; \
   AQCHECK_MSG_ADD_AVP(a, b, c, &val, e); \
}

#define AQCHECK_MSG_ADD_AVP_U32(a, b, c, d) \
{ \
   struct avp * __avp__; \
   AQCHECK_MSG_ADD_AVP_U32_2(a, b, c, d, __avp__); \
   (void)__avp__; \
}

#define AQCHECK_MSG_FIND_AVP(a, b, c, __fallback__) \
{ \
   if (fd_msg_search_avp(a, b, &c) != 0) \
   { \
      __fallback__; \
   } \
}

#define AQCHECK_AVP_GET_HDR(a, b, c, __fallback__) \
{ \
   if (fd_msg_avp_hdr(b,&c) != 0) { \
      struct dict_avp_data * __deval__ = NULL; \
      fd_dict_getval(a, &__deval__); \
      LOG_E("fd_msg_avp_hdr(): unable to retrieve avp header for [%s]", __deval__ ? __deval__->avp_name : ""); \
      __fallback__; \
   } \
}

#define AQCHECK_AVP_GET_S32(a, b, c, __fallback__) \
{ \
   struct avp_hdr *__hdr__= NULL; \
   AQCHECK_AVP_GET_HDR(a, b, __hdr__, __fallback__); \
   c = __hdr__->avp_value->i32; \
}

#define AQCHECK_AVP_GET_U32(a, b, c, __fallback__) \
{ \
   struct avp_hdr *__hdr__= NULL; \
   AQCHECK_AVP_GET_HDR(a, b, __hdr__, __fallback__); \
   c = __hdr__->avp_value->u32; \
}

#define AQCHECK_MSG_GET_AVPHDR(a, b, c, __fallback__) \
{ \
   struct avp * ___avp___; \
   __ret__ = fd_msg_search_avp(a,b,&___avp___); \
   if (__ret__ == 0) { \
      __ret__ = fd_msg_avp_hdr(___avp___, &c); \
      if (__ret__ != 0) { \
         struct dict_avp_data * __deval__ = NULL; \
         fd_dict_getval(b, &__deval__); \
         LOG_E("fd_msg_avp_hdr(): unable to retrieve avp header for [%s]", __deval__ ? __deval__->avp_name : ""); \
         { __fallback__; } \
      } \
   } else { \
      struct dict_avp_data * __deval__ = NULL; \
      fd_dict_getval(b, &__deval__); \
      LOG_E("fd_msg_search_avp(): unable to find avp [%s]", __deval__ ? __deval__->avp_name : ""); \
      { __fallback__; } \
   } \
}

/* avp_hdr, length, data, max_length, dict_info */
#define AQCHECK_MSG_GET_AVP_OSTR2(a, b, c) \
{ \
   b = a->avp_value->os.len; \
   c = malloc( b ); \
   memcpy( c, a->avp_value->os.data, b ); \
}

#define AQCHECK_MSG_GET_AVP_OSTR(a, b, c, d, e, __fallback__) \
{ \
   int __ret__; \
   struct avp_hdr * __hdr__; \
   AQCHECK_MSG_GET_AVPHDR(a, b, __hdr__, __fallback__); \
   if (__hdr__->avp_value->os.len <= e) { \
      d = __hdr__->avp_value->os.len; \
      memcpy(c, __hdr__->avp_value->os.data, d); \
   } else { \
      struct dict_avp_data * __deval__ = NULL; \
      fd_dict_getval(b, &__deval__); \
      LOG_E("avp length of %u for [%s] exceeds the maximum length of %u", \
         (U32)__hdr__->avp_value->os.len, __deval__ ? __deval__->avp_name : "", (U32)e); \
      __fallback__; \
   } \
}

#define AQCHECK_MSG_GET_AVP_STR(a, b, c, d, __fallback__) \
{ \
   int __ret__; \
   struct avp_hdr * __hdr__; \
   AQCHECK_MSG_GET_AVPHDR(a, b, __hdr__, __fallback__); \
   if (__hdr__->avp_value->os.len < d) { \
      memcpy(c, __hdr__->avp_value->os.data, __hdr__->avp_value->os.len); \
      c[__hdr__->avp_value->os.len] = '\0'; \
   } else { \
      struct dict_avp_data * __deval__ = NULL; \
      fd_dict_getval(b, &__deval__); \
      LOG_E("avp length of %u for [%s] exceeds the maximum length of %u", \
         (U32)__hdr__->avp_value->os.len, __deval__ ? __deval__->avp_name : "", (U32)d); \
      __fallback__; \
   } \
}

#define AQCHECK_MSG_GET_AVP_U32(a, b, c, __fallback__) \
{ \
   int __ret__; \
   struct avp_hdr * __hdr__; \
   AQCHECK_MSG_GET_AVPHDR(a, b, __hdr__, __fallback__); \
   c = __hdr__->avp_value->u32; \
}

#define AQ_DUP_OSTR(a, b) \
{ \
   b = aqfdDupOctetString( a ); \
}

#define AQ_DUP_OSTR2(a, b) \
{ \
   b = (PTR)aqfdDupOctetString( a ); \
}

#define AQ_DESTROY_OCTETSTRING(a) \
{ \
   if (a) { \
      free(a); \
      a = NULL; \
   } \
}

#define AQ_ALLOC_CMLLIST(a) \
   CmLList *a; \
   a = (CmLList*)malloc( sizeof(CmLList) ); \
   memset( a, 0, sizeof(CmLList) ); \

#define AQ_DUP_OSTR_ADD_LLIST(a, b) \
{ \
   AQ_ALLOC_CMLLIST( pln ); \
   AQ_DUP_OSTR2( a, pln->node ); \
   cmLListAdd2Tail( &b, pln ); \
}

#define AQ_DUP_U32_ADD_LLIST(a, b) \
{ \
   U32 *__val__; \
   AQ_ALLOC_CMLLIST( pln ); \
   __val__ = (U32*)malloc( sizeof(U32) ); \
   *__val__ = a->u32; \
   pln->node = (PTR)__val__; \
   cmLListAdd2Tail( &b, pln ); \
}

#define AQ_DUP_ENUM(a, b) \
{ \
   AqEnum *__enum__ = (AqEnum*)malloc( sizeof(AqEnum) ); \
   *__enum__ = a; \
   b = (PTR)__enum__; \
}

#if defined(__BYTE_ORDER__) && (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)
#define AQ_NETWORK_TO16(a) ((a[0] << 8) | a[1])
#else
#define AQ_NETWORK_TO16(a) ((a[1] << 8) | a[0])
#endif

#define AQ_ALLOC_AQADDRESS(a) \
   AqAddress *__addr__ = (AqAddress*)malloc( sizeof(AqAddress) ); \
   memset( __addr__, 0, sizeof(AqAddress) ); \
   __addr__->type = AQ_NETWORK_TO16( a->os.data ); \
   memcpy( __addr__->address, &a->os.data[2], a->os.len - 2 ); \

#define AQ_ALLOC_AQADDRESS1(a, b) \
{ \
   AQ_ALLOC_AQADDRESS(a); \
   b = __addr__; \
}

#define AQ_ALLOC_AQADDRESS2(a, b) \
{ \
   AQ_ALLOC_AQADDRESS(a); \
   b = (PTR)__addr__; \
}

#define AQ_DESTROY_ADDRESS(a) \
{ \
   if (a) { \
      free(a); \
      a = NULL; \
   } \
}

/* AQCHECK_PARSE_DIRECT( function_name, struct avp *, pointer to structure to populate ) */
#define AQCHECK_PARSE_DIRECT(a,b,c) \
{ \
   S16 __ret__ = a(b,c); \
   if (__ret__ != LCM_REASON_NOT_APPL) \
      return __ret__; \
}

/* AQCHECK_PARSE_LLIST( function_name, struct avp *, CmLListCp * ) */
#define AQCHECK_PARSE_LLIST(a,b,c) \
{ \
   S16 __ret__; \
   AQ_ALLOC_CMLLIST( __pln__ ); \
   __ret__ = a( b, &__pln__->node ); \
   if (__ret__ != LCM_REASON_NOT_APPL) \
      return __ret__; \
   cmLListAdd2Tail( c, __pln__ ); \
}

#define AQ_DESTROY_LLIST1(a) \
{ \
   while ( a.first ) { \
      CmLList *n = cmLListDelFrm( &a, a.first ); \
      if (n) { \
         if (n->node) \
            free( (Void*)n->node ); \
         free( (Void*)n ); \
      } \
   } \
}

//#define AQ_DESTROY_LLIST2( sd->call_barring_info, aqfdDestroyCallBarringInfo, SyCallBarringInfo );
//AQ_DESTROY_LLIST2( sd->call_barring_info, aqfdDestroyCallBarringInfo, SyCallBarringInfo );
#define AQ_DESTROY_LLIST2(a,b,c) { \
   while ( a.first ) { \
      CmLList *n = cmLListDelFrm( &a, a.first ); \
      if (n) { \
         if (n->node) { \
            b( (c*)n->node ); \
            free( (Void*)n->node ); \
         } \
         free( (Void*)n ); \
      } \
   } \
}

#define AQFD_E164_LENGTH (15)
#define AQFD_APN_MAX_LENGTH (100)
#define UE_PURGED_MME      (1 <<0)
#define UE_PURGED_SGSN     (1 <<1)
   
#define S6A_ULR_FLAGS_SINGLE_REGSISTRATION_INDICATION	(1 << 0)
#define S6A_ULR_FLAGS_S6AS6D_INDICATOR                  (1 << 1)
#define S6A_ULR_FLAGS_SKIP_SUBSCRIBER_DATA              (1 << 2)
#define S6A_ULR_FLAGS_GPRS_SUBSCRIPTION_DATA_INDICATOR  (1 << 3)
#define S6A_ULR_FLAGS_NODE_TYPE_INDICATOR               (1 << 4)
#define S6A_ULR_FLAGS_INITIAL_ATTACH_INDICATOR          (1 << 5)
#define S6A_ULR_FLAGS_PS_LCS_NOT_SUPPORTED_BY_UE        (1 << 6)
#define S6A_ULR_FLAGS_SMS_ONLY_INDICATION               (1 << 7)

#define S6A_RAT_TYPE_WLAN           (0) /* This value shall be used to indicate that the RAT is WLAN. */
#define S6A_RAT_TYPE_VIRTUAL        (1) /* This value shall be used to indicate that the RAT is unknown. */
#define S6A_RAT_TYPE_UTRAN          (1000) /* This value shall be used to indicate that the RAT is UTRAN. */
#define S6A_RAT_TYPE_GERAN          (1001) /* This value shall be used to indicate that the RAT is GERAN. */
#define S6A_RAT_TYPE_GAN            (1002) /* This value shall be used to indicate that the RAT is GAN. */
#define S6A_RAT_TYPE_HSPA_EVOLUTION (1003) /* This value shall be used to indicate that the RAT is HSPA Evolution. */
#define S6A_RAT_TYPE_EUTRAN         (1004) /* This value shall be used to indicate that the RAT is EUTRAN (WB-EUTRAN). */
#define S6A_RAT_TYPE_EUTRAN_NB_IOT  (1005) /* This value shall be used to indicate that the RAT is NB-IoT. */
#define S6A_RAT_TYPE_CDMA2000_1X    (2000) /* This value shall be used to indicate that the RAT is CDMA2000 1X. */
#define S6A_RAT_TYPE_HRPD           (2001) /* This value shall be used to indicate that the RAT is HRPD. */
#define S6A_RAT_TYPE_UMB            (2002) /* This value shall be used to indicate that the RAT is UMB. */
#define S6A_RAT_TYPE_EHRPD          (2003) /* This value shall be used to indicate that the RAT is eHRPD. */

#define S6A_SUBSCRIPTION_DATA_FLAGS_PS_AND_SMS_ONLY_SERVICE_PROVISION_INDICATION (1 << 0)
#define S6A_SUBSCRIPTION_DATA_FLAGS_SMS_IN_SGSN_ALLOWED_INDICATION               (1 << 1)
#define S6A_SUBSCRIPTION_DATA_FLAGS_USER_PLANE_INTEGRITY_PROTECTION              (1 << 2)
#define S6A_SUBSCRIPTION_DATA_FLAGS_PDN_CONNECTION_RESTRICTED                    (1 << 3)

#define S6A_SUBSCRIBER_STATUS_SERVICE_GRANTED             (0)
#define S6A_SUBSCRIBER_STATUS_OPERATOR_DETERMINED_BARRING (1)

#define AQFD_LOW_NIBBLE(b) (b & 0x0f)
#define AQFD_HIGH_NIBBLE(b) (AQFD_LOW_NIBBLE(b >> 4))

#define AQFD_CHAR2TBCD(c) \
( \
   c >= '0' && c <= '9' ? c - '0' : \
   c == '*' ? 10 : \
   c == '#' ? 11 : \
   c == 'a' ? 12 : \
   c == 'b' ? 13 : \
   c == 'c' ? 14 : 15 \
)


#endif /* __AQFDH__ */
