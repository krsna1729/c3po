/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     EPC DNS

     Type:     C header file

     Desc:     Structures, variables, typedefs and prototypes
               required at the management interface.

     File:     lds.x

     Sid:

     Prg:      bw

*********************************************************************21*/

#ifndef __LDSX__
#define __LDSX__

#ifdef __cplusplus
extern "C" {
#endif

/* General configuration.
 */
typedef struct dsGenCfg
{
   //Pst          lmPst;          /* for layer manager */
   //Pst          mmePst;

   Region       initRegion;
   Pool         initPool;
} DsGenCfg;

/* General statistics.
 */
typedef struct dsGenSts
{
   StsCntr      numErrors;     /* num of errors */
} DsGenSts;

typedef struct dsSta
{
   State        state;          /* DNS state */
} DsSta;

/* Unsolicited status indication.
 */
typedef struct dsAlarmInfo
{
   SpId         spId;           /* sap Id */
   U8           type;           /* which member of inf is present */
   union
   {
      State     state;          /* TSAP state */
      State     conState;       /* connection state */
      Mem       mem;            /* region/pool (resource related) */
      U8        parType;        /* parameter type */
   } inf;

} DsAlarmInfo;

#ifdef DEBUGP
/* Debug control.
 */
typedef struct dsDbgCntrl
{
   U32          dbgMask;        /* Debug mask */
} DsDbgCntrl;
#endif


/* Trace control.
 */
typedef struct dsTrcCntrl
{
   SpId         sapId;          /* sap Id */
   S16          trcLen;         /* length to trace */

} DsTrcCntrl;



/* Configuration.
 */
typedef struct dsCfg
{
   union
   {
      DsGenCfg  dsGen;          /* general configuration */
   } s;

} DsCfg;


/* Statistics.
 */
typedef struct dsSts
{
   DateTime     dt;             /* date and time */
   Duration     dura;           /* duration */

   union
   {
      DsGenSts  genSts;         /* general statistics */
   } s;

} DsSts;

/* Solicited status.
 */
typedef struct dsSsta
{
   DateTime     dt;             /* date and time */

   union
   {
      SystemId  sysId;          /* system id */
      DsSta  dsSta;             /* diameter status */
   } s;

} DsSsta;

/* Unsolicited status.
 */
typedef struct dsUsta
{
   CmAlarm      alarm;          /* alarm */
   DsAlarmInfo  info;           /* alarm information */

} DsUsta;


/* Trace.
 */
typedef struct dsTrc
{
   DateTime     dt;            /* date and time */
   U16          evnt;          /* event */
} DsTrc;

/* Control.
 */
typedef struct dsCntrl
{
   DateTime     dt;             /* date and time */
   U8           action;         /* action */
   U8           subAction;      /* sub action */

   union
   {
      DsTrcCntrl        trcDat;         /* trace length */
      ProcId            dstProcId;      /* destination procId */
      Route             route;          /* route */
      Priority          priority;       /* priority */

#ifdef DEBUGP
      DsDbgCntrl        dsDbg;          /* debug printing control */
#endif

   } ctlType;

} DsCntrl;

/* Management.
 */
typedef struct dsMngmt
{
   Header       hdr;            /* header */
   CmStatus     cfm;            /* response status/confirmation */

   union
   {
      DsCfg     cfg;            /* configuration */
      DsSts     sts;            /* statistics */
      DsSsta    ssta;           /* solicited status */
      DsUsta    usta;           /* unsolicited status */
      DsTrc     trc;            /* trace */
      DsCntrl   cntrl;          /* control */

   } t;
} DsMngmt;

typedef struct dnsQueryData
{
   U8 imsi[16];
   U8 imsilen;
   CmTptAddr addr;
   Bool success;
} DnsQueryData;



/* Layer management interface primitive types.
 */
typedef S16  (*LdsCfgReq)       ARGS((Pst *pst, DsMngmt *cfg));
typedef S16  (*LdsCfgCfm)       ARGS((Pst *pst, DsMngmt *cfg));
typedef S16  (*LdsCntrlReq)     ARGS((Pst *pst, DsMngmt *cntrl));
typedef S16  (*LdsCntrlCfm)     ARGS((Pst *pst, DsMngmt *cntrl));
typedef S16  (*LdsStsReq)       ARGS((Pst *pst, Action action,
                                      DsMngmt *sts));
typedef S16  (*LdsStsCfm)       ARGS((Pst *pst, DsMngmt *sts));
typedef S16  (*LdsStaReq)       ARGS((Pst *pst, DsMngmt *sta));
typedef S16  (*LdsStaInd)       ARGS((Pst *pst, DsMngmt *sta));
typedef S16  (*LdsStaCfm)       ARGS((Pst *pst, DsMngmt *sta));
typedef S16  (*LdsTrcInd)       ARGS((Pst *pst, DsMngmt *trc,
                                      Buffer *mBuf));

typedef S16 (*LdsInitAttachSgw) ARGS((Pst *pst, DnsQueryData *qd));

/* Layer management interface primitives.
 */
//#ifdef SY
EXTERN S16  DsMiLdsCfgReq       ARGS((Pst *pst, DsMngmt *cfg));
EXTERN S16  DsMiLdsCfgCfm       ARGS((Pst *pst, DsMngmt *cfg));
EXTERN S16  DsMiLdsCntrlReq     ARGS((Pst *pst, DsMngmt *cntrl));
EXTERN S16  DsMiLdsCntrlCfm     ARGS((Pst *pst, DsMngmt *cntrl));
EXTERN S16  DsMiLdsStsReq       ARGS((Pst *pst, Action action,
                                      DsMngmt *sts));
EXTERN S16  DsMiLdsStsCfm       ARGS((Pst *pst, DsMngmt *sts));
EXTERN S16  DsMiLdsStaReq       ARGS((Pst *pst, DsMngmt *sta));
EXTERN S16  DsMiLdsStaCfm       ARGS((Pst *pst, DsMngmt *sta));
EXTERN S16  DsMiLdsStaInd       ARGS((Pst *pst, DsMngmt *sta));
EXTERN S16  DsMiLdsTrcInd       ARGS((Pst *pst, DsMngmt *trc,
                                      Buffer *mBuf));

//#endif /* SY */

#ifdef SM
EXTERN S16  SmMiLdsCfgReq       ARGS((Pst *pst, DsMngmt *cfg));
EXTERN S16  SmMiLdsCfgCfm       ARGS((Pst *pst, DsMngmt *cfg));
EXTERN S16  SmMiLdsCntrlReq     ARGS((Pst *pst, DsMngmt *cntrl));
EXTERN S16  SmMiLdsCntrlCfm     ARGS((Pst *pst, DsMngmt *cntrl));
EXTERN S16  SmMiLdsStsReq       ARGS((Pst *pst, Action action,
                                      DsMngmt *sts));
EXTERN S16  SmMiLdsStsCfm       ARGS((Pst *pst, DsMngmt *sts));
EXTERN S16  SmMiLdsStaReq       ARGS((Pst *pst, DsMngmt *sta));
EXTERN S16  SmMiLdsStaInd       ARGS((Pst *pst, DsMngmt *sta));
EXTERN S16  SmMiLdsStaCfm       ARGS((Pst *pst, DsMngmt *sta));
EXTERN S16  SmMiLdsTrcInd       ARGS((Pst *pst, DsMngmt *trc,
                                      Buffer *mBuf));
#endif /* SM */


/* Packing and unpacking functions.
 */
EXTERN S16  cmPkLdsCfgReq       ARGS((Pst *pst, DsMngmt *cfg));
EXTERN S16  cmPkLdsCfgCfm       ARGS((Pst *pst, DsMngmt *cfg));
EXTERN S16  cmPkLdsCntrlReq     ARGS((Pst *pst, DsMngmt *cntrl));
EXTERN S16  cmPkLdsCntrlCfm     ARGS((Pst *pst, DsMngmt *cntrl));
EXTERN S16  cmPkLdsStsReq       ARGS((Pst *pst, Action action, DsMngmt *sts));
EXTERN S16  cmPkLdsStsCfm       ARGS((Pst *pst, DsMngmt *sts));
EXTERN S16  cmPkLdsStaReq       ARGS((Pst *pst, DsMngmt *sta));
EXTERN S16  cmPkLdsStaInd       ARGS((Pst *pst, DsMngmt *sta));
EXTERN S16  cmPkLdsTrcInd       ARGS((Pst *pst, DsMngmt *trc, Buffer *mBuf));
EXTERN S16  cmPkLdsStaCfm       ARGS((Pst *pst, DsMngmt *sta));

EXTERN S16  cmUnpkLdsCfgReq     ARGS((LdsCfgReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLdsCfgCfm     ARGS((LdsCfgCfm func, Pst *pst,
                                    Buffer *mBuf));
EXTERN S16  cmUnpkLdsCntrlReq   ARGS((LdsCntrlReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLdsCntrlCfm   ARGS((LdsCntrlCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLdsStsReq     ARGS((LdsStsReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLdsStsCfm     ARGS((LdsStsCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLdsStaReq     ARGS((LdsStaReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLdsStaInd     ARGS((LdsStaInd func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLdsStaCfm     ARGS((LdsStaCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLdsTrcInd     ARGS((LdsTrcInd func, Pst *pst,
                                      Buffer *mBuf));
/* Layer manager activation functions.
 */
#if 0
EXTERN S16  smDsActvTsk         ARGS((Pst *pst, Buffer *mBuf));
EXTERN S16  smDsActvInit        ARGS((Ent ent, Inst inst,
                                      Region region, Reason reason));
#endif

#ifdef SS_MULTIPLE_PROCS
EXTERN S16 dsActvInit       ARGS((ProcId procId,
                                  Ent entity,
                                  Inst inst,
                                  Region region,
                                  Reason reason,
                                  Void **xxCb));
#else /* SS_MULTIPLE_PROCS */

EXTERN S16  dsActvInit          ARGS((Ent ent, Inst inst,
                                      Region region, Reason reason));
#endif /* SS_MULTIPLE_PROCS */
EXTERN S16  dsActvTsk           ARGS((Pst *pst, Buffer *mBuf));

EXTERN S16 cmPkLdsInitAttachSgw   ARGS((DnsQueryData *qd));
EXTERN S16 cmUnpkLdsInitAttachSgw ARGS((LdsInitAttachSgw func, Pst *pst, Buffer *mBuf));
EXTERN S16 SmMiLdsInitAttachSgw   ARGS((Pst *pst, DnsQueryData *qd));

#ifdef __cplusplus
}
#endif

#endif /* __LDSX__ */


/********************************************************************30**

         End of file:     lhi.x@@/main/10 - Tue Feb  1 16:04:21 2011

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat     init                  description
----------- --------- ---- -----------------------------------------------
1.1         ---       bw   1. initial release.
*********************************************************************91*/

