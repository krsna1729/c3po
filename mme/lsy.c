/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     S6A Convergence Layer

     Type:     C source file

     Desc:     C source code for common packing and un-packing functions for
               layer management interface(LSY).

     File:     lsy.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/*
*     This software may be combined with the following TRILLIUM
*     software:
*
*     part no.                      description
*     --------    ----------------------------------------------
*
*/


/* header include files (.h) */
#include "envopt.h"        /* environment options */
#include "envdep.h"        /* environment dependent */
#include "envind.h"        /* environment independent */
#include "gen.h"           /* general layer */
#include "cm_gen.h"        /* common pack/unpack defines */
#include "cm_os.h"
#include "ssi.h"           /* system services */
#include "lhi.h"           /* layer management, HI Layer */

/* header/extern include files (.x) */
#include "gen.x"           /* general layer */
#include "cm_os.x"
#include "ssi.x"           /* system services */
//#include "lhi.x"           /* layer management, HI layer */
#include "lsy.h"
#include "lsy.x"
/* hi009.104 - added as HI_ZERO macro calls cmMemset which is defined in
 * cm_lib.x */
#include "cm_lib.x"            /* has the prototype of cmMemset() */


/* local defines */

/* local typedefs */

/* local externs */

/* forward references */



/* functions in other modules */

/* public variable declarations */

/* private variable declarations */

#ifdef LCLSY



/*
*     layer management interface packing functions
*/


/*
*
*       Fun:   Pack Config Request
*
*       Desc:  This function is used to a pack config request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsyCfgReq
(
Pst *pst,                 /* post structure */
SyMngmt *cfg              /* configuration */
)
#else
PUBLIC S16 cmPkLsyCfgReq(pst, cfg)
Pst *pst;                 /* post structure */
SyMngmt *cfg;             /* configuration */
#endif
{
   Buffer *mBuf;            /* message buffer */

   TRC3(cmPkLsyCfgReq)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch (cfg->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY065, cfg->hdr.elmId.elmnt,
                     "cmPkLsyCfgReq() Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   CMCHKPKLOG(cmPkCmStatus, &cfg->cfm, mBuf, ELSY002, pst);
   CMCHKPKLOG(cmPkHeader, &cfg->hdr, mBuf, ELSY003, pst);
   pst->event = (Event)EVTLSYCFGREQ;

/* hi009.104 - fill interface version number in pst */
#ifdef TDS_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LHIIFVER;
#endif /* TDS_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Config Confirm
*
*       Desc:  This function is used to a pack config confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsyCfgCfm
(
Pst *pst,                 /* post structure */
SyMngmt *cfm              /* confirm */
)
#else
PUBLIC S16 cmPkLsyCfgCfm(pst, cfm)
Pst *pst;                 /* post structure */
SyMngmt *cfm;             /* confirm */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsyCfgCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   CMCHKPKLOG(cmPkCmStatus, &cfm->cfm, mBuf, ELSY068, pst);
   CMCHKPKLOG(cmPkHeader,   &cfm->hdr, mBuf, ELSY069, pst);

   pst->event = EVTLSYCFGCFM;
   RETVALUE(SPstTsk(pst, mBuf));

} /* end of cmPkLsyCfgCfm */


/*
*
*       Fun:   Pack Control Request
*
*       Desc:  This function is used to pack the control request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsyCntrlReq
(
Pst *pst,                 /* post structure */
SyMngmt *ctl              /* configuration */
)
#else
PUBLIC S16 cmPkLsyCntrlReq(pst, ctl)
Pst *pst;                 /* post structure */
SyMngmt *ctl;             /* configuration */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsyCntrlReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   switch(ctl->hdr.elmId.elmnt)
   {
      case STGEN:
         break;

      case STTSAP:
         if(ctl->t.cntrl.subAction == SATRC)
         {
            CMCHKPKLOG(cmPkSpId, ctl->t.cntrl.ctlType.trcDat.sapId,
                       mBuf, ELSY071, pst);
            CMCHKPKLOG(SPkS16, ctl->t.cntrl.ctlType.trcDat.trcLen,
                       mBuf, ELSY072, pst);
         }
         else if(ctl->t.cntrl.subAction == SAELMNT)
         {
         }
         else
         {
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY074,  ctl->t.cntrl.subAction,
                     "cmPkLsyCntrlReq() : invalid subAction(STTSAP) ");
#endif /* ERRCLS_DEBUG */
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
         }
         break;

      case STGRTSAP:
         if(ctl->t.cntrl.subAction == SAGR_PRIORITY)
         {
            CMCHKPKLOG(cmPkPrior, ctl->t.cntrl.ctlType.priority, mBuf,
                       ELSY075, pst);
         }
         else if(ctl->t.cntrl.subAction == SAGR_ROUTE)
         {
            CMCHKPKLOG(cmPkProcId, ctl->t.cntrl.ctlType.route, mBuf,
                       ELSY076, pst);
         }
         else if(ctl->t.cntrl.subAction == SAGR_DSTPROCID)
         {
            CMCHKPKLOG(cmPkProcId, ctl->t.cntrl.ctlType.dstProcId, mBuf,
                       ELSY077, pst);
         }
         else
         {
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY078, ctl->t.cntrl.subAction,
                     "cmPkLsyCntrlReq() :invalid subAction(STGRTSAP)");
#endif /* ERRCLS_DEBUG */
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
         }
         break;

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY079, ctl->hdr.elmId.elmnt,
                     "cmPkLsyCntrlReq () Failed : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   CMCHKPKLOG(SPkU8,       ctl->t.cntrl.subAction, mBuf, ELSY080, pst);
   CMCHKPKLOG(SPkU8,       ctl->t.cntrl.action,    mBuf, ELSY081, pst);
   CMCHKPKLOG(cmPkDateTime, &ctl->t.cntrl.dt,       mBuf, ELSY082, pst);

   CMCHKPKLOG(cmPkCmStatus, &ctl->cfm, mBuf, ELSY083, pst);
   CMCHKPKLOG(cmPkHeader,   &ctl->hdr, mBuf, ELSY084, pst);
   pst->event = (Event)EVTLHICNTRLREQ;

/* hi009.104 - fill interface version number in pst */
#ifdef TDS_ROLL_UPGRADE_SUPPORT
   pst->intfVer = (CmIntfVer) LHIIFVER;
#endif /* TDS_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Control Confirm
*
*       Desc:  This function is used to pack the control confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsyCntrlCfm
(
Pst *pst,                 /* post structure */
SyMngmt *cfm              /* confirm */
)
#else
PUBLIC S16 cmPkLsyCntrlCfm(pst, cfm)
Pst *pst;                 /* post structure */
SyMngmt *cfm;             /* confirm */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsyCntrlCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   /* pack status */
   CMCHKPKLOG(cmPkCmStatus, &cfm->cfm, mBuf, ELSY085, pst);

   /* pack header */
   CMCHKPKLOG(cmPkHeader, &cfm->hdr, mBuf, ELSY086, pst);

   pst->event = EVTLHICNTRLCFM;
   RETVALUE(SPstTsk(pst, mBuf));

} /* end of cmPkLsyCntrlCfm */



/*
*
*       Fun:   Pack Statistics Request
*
*       Desc:  This function is used to pack the statistics request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsyStsReq
(
Pst *pst,                 /* post structure */
Action action,            /* action */
SyMngmt *sts              /* statistics */
)
#else
PUBLIC S16 cmPkLsyStsReq(pst, action, sts)
Pst *pst;                 /* post structure */
Action action;            /* action */
SyMngmt *sts;             /* statistics */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsyStsReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sts->hdr.elmId.elmnt)
   {
      case STGEN:
         break;
      case STTSAP:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY088, sts->hdr.elmId.elmnt,
                     "cmPkLsyStsReq () Failed : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }
   CMCHKPKLOG(cmPkCmStatus, &sts->cfm, mBuf, ELSY089, pst);
   CMCHKPKLOG(cmPkHeader,   &sts->hdr, mBuf, ELSY090, pst);
   CMCHKPKLOG(cmPkAction,   action,    mBuf, ELSY091, pst);
   pst->event = EVTLHISTSREQ;

/* hi009.104 - fill interface version number in pst */
#ifdef TDS_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LHIIFVER;
#endif /* TDS_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLsyStsReq */

/*lhi_c_004.main_1  1. Modified statistics req to include messages
                              Tx/Rx in bytes per con per thread */
#ifdef LHI_THR_STS

/*
*
*       Fun:   Pack connection Statistics
*
*       Desc:  This function is used to pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkSyConSts
(
SyConSts *sts,              /* statistics */
Buffer  *mBuf              /* to hold the packed buffer */
)
#else
PUBLIC S16 cmPkSyConSts(sts, mBuf)
SyConSts *sts;              /* statistics */
Buffer  *mBuf;             /* to hold the packed buffer */
#endif
{
   /*lhi_c_001.main_11: Added TRC macro for cmPkSyConSts()*/
   TRC3(cmPkSyConSts)
   CMCHKPK(cmPkStsCntr, sts->numTxBytes, mBuf);
   CMCHKPK(cmPkStsCntr, sts->numRxBytes, mBuf);

   RETVALUE(ROK);
}/*cmPkSyConSts*/


/*
*
*       Fun:   Pack Con Statistics per thread
*
*       Desc:  This function is used to pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkSyThrConSts
(
SyThrConSts *sts,              /* statistics */
Pst *pst,
Buffer  *mBuf              /* to hold the packed buffer */
)
#else
PUBLIC S16 cmPkSyThrConSts(sts, pst, mBuf)
SyThrConSts *sts;              /* statistics */
Pst *pst;
Buffer  *mBuf;             /* to hold the packed buffer */
#endif
{
   /* this will hold the temporary index value for number of Threads */
   U8 iThrConIndex = 0;

   /*lhi_c_001.main_11: Added TRC macro for cmPkSyThrConSts()*/
   TRC3(cmPkSyThrConSts)
   /* if connection are present in threads, then only pack them */
   if (sts->numCons)
   {
      /* in a loop pack the thread statistics of all commands */
      for (; iThrConIndex < sts->numCons; iThrConIndex++)
      {
         /* lhi_c_001.main_12: Replaced CMCHKPK with CMCHKPKLOG macro */
         CMCHKPKLOG(cmPkSyConSts, &sts->conSts[iThrConIndex], mBuf, ELSY639, pst);
      }

     SPutSBuf(pst->region, pst->pool, (Data*)sts->conSts,
                 (Size)(sizeof(SyConSts) * sts->numCons));
     sts->conSts = NULLP;
   } /* end of if threads are present */

   /* lhi_c_001.main_12: Replaced CMCHKPK with CMCHKPKLOG macro */
   CMCHKPKLOG(SPkU32, sts->numCons, mBuf, ELSY640, pst);

   RETVALUE(ROK);
}/*cmPkSyThrConSts*/

#endif /*LHI_THR_STS*/

/*
*
*       Fun:   Pack Statistics Confirm
*
*       Desc:  This function is used to pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsyStsCfm
(
Pst *pst,                 /* post structure */
SyMngmt *sts              /* statistics */
)
#else
PUBLIC S16 cmPkLsyStsCfm(pst, sts)
Pst *pst;                 /* post structure */
SyMngmt *sts;             /* statistics */
#endif

{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsyStsCfm)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sts->hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      case STTSAP:
      {
         break;
      }

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY126, sts->hdr.elmId.elmnt,
                     "cmPkLsyStsCfm() : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   CMCHKPKLOG(cmPkDuration, &sts->t.sts.dura, mBuf, ELSY127, pst);
   CMCHKPKLOG(cmPkDateTime, &sts->t.sts.dt, mBuf, ELSY128, pst);

   CMCHKPKLOG(cmPkCmStatus, &sts->cfm, mBuf, ELSY129, pst);
   CMCHKPKLOG(cmPkHeader,   &sts->hdr, mBuf, ELSY130, pst);
   pst->event = (Event)EVTLHISTSCFM;
   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Status Request
*
*       Desc:  This function is used to pack the status request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsyStaReq
(
Pst *pst,                 /* post structure */
SyMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 cmPkLsyStaReq(pst, sta)
Pst *pst;                 /* post structure */
SyMngmt *sta;             /* solicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsyStaReq)
   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   switch(sta->hdr.elmId.elmnt)
   {
      case STSID:
         break;
      case STTSAP:
         break;
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY132, sta->hdr.elmId.elmnt,
                     "cmPkLsyStaReq () : invalid elmnt");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELSY133, pst);
   CMCHKPKLOG(cmPkHeader, &sta->hdr, mBuf, ELSY134, pst);
   pst->event = (Event)EVTLSYSTAREQ;

   /* hi009.104 - fill interface version number in pst */
#ifdef TDS_ROLL_UPGRADE_SUPPORT
      pst->intfVer = (CmIntfVer) LHIIFVER;
#endif /* TDS_ROLL_UPGRADE_SUPPORT */

   RETVALUE(SPstTsk(pst, mBuf));
}


/*
*
*       Fun:   Pack Status Confirm
*
*       Desc:  This function is used to pack the status confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsyStaCfm
(
Pst *pst,                 /* post structure */
SyMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 cmPkLsyStaCfm(pst, sta)
Pst *pst;                 /* post structure */
SyMngmt *sta;             /* solicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsyStaCfm)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }
   switch(sta->hdr.elmId.elmnt)
   {
      case STSID:
      {
         break;
      }
      case STTSAP:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY141, sta->hdr.elmId.elmnt,
                     "cmPkLsyStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   /* date */
   CMCHKPKLOG(cmPkDateTime, &sta->t.ssta.dt, mBuf, ELSY142, pst);

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELSY143, pst);
   CMCHKPKLOG(cmPkHeader,   &sta->hdr, mBuf, ELSY144, pst);
   pst->event = EVTLSYSTACFM;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLsyStaCfm */


/*
*
*       Fun:   Pack Status Indication
*
*       Desc:  This function is used to pack the status indication
*              primitive to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsyStaInd
(
Pst *pst,                 /* post structure */
SyMngmt *sta              /* unsolicited status */
)
#else
PUBLIC S16 cmPkLsyStaInd(pst, sta)
Pst *pst;                 /* post structure */
SyMngmt *sta;             /* unsolicited status */
#endif
{
   Buffer *mBuf;          /* message buffer */

   TRC3(cmPkLsyStaInd)

   if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
   {
      RETVALUE(RFAILED);
   }

   if(sta->t.usta.info.type != LHI_ALARMINFO_TYPE_NTPRSNT)
   {
      switch(sta->t.usta.info.type)
      {
         case LSY_ALARMINFO_PAR_TYPE:
            break;

         case LSY_ALARMINFO_MEM_ID:
            break;

         case LSY_ALARMINFO_CON_STATE:
            break;

         case LSY_ALARMINFO_SAP_STATE:
            break;

         default:
#if (ERRCLASS & ERRCLS_DEBUG)
            LSYLOGERROR(pst, ELSY151, sta->t.usta.info.type,
                        "cmPkLsyStaInd () Failed");
#endif
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
      }
   }
   CMCHKPKLOG(SPkU8,   sta->t.usta.info.type, mBuf, ELSY152, pst);
   CMCHKPKLOG(cmPkSpId,    sta->t.usta.info.spId,  mBuf, ELSY153, pst);
   CMCHKPKLOG(cmPkCmAlarm, &sta->t.usta.alarm,     mBuf, ELSY154, pst);

   CMCHKPKLOG(cmPkCmStatus, &sta->cfm, mBuf, ELSY155, pst);
   CMCHKPKLOG(cmPkHeader,   &sta->hdr, mBuf, ELSY156, pst);
   pst->event = (Event)EVTLSYSTAIND;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLsyStaInd */



/*
*
*       Fun:   Pack Trace Indication
*
*       Desc:  This function is used to pack the trace indication
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmPkLsyTrcInd
(
Pst *pst,                 /* post */
SyMngmt *trc,             /* trace */
Buffer  *mBuf             /* message buffer */
)
#else
PUBLIC S16 cmPkLsyTrcInd(pst, trc, mBuf)
Pst *pst;                 /* post */
SyMngmt *trc;             /* trace */
Buffer  *mBuf;            /* message buffer */
#endif
{

   TRC3(cmPkLsyTrcInd)

   if (mBuf == NULLP)
   {
      if(SGetMsg(pst->region, pst->pool, &mBuf) != ROK)
      {
         RETVALUE(RFAILED);
      }
   }

   CMCHKPKLOG(SPkU16, trc->t.trc.evnt, mBuf, ELSY158, pst);
   CMCHKPKLOG(cmPkDateTime, &trc->t.trc.dt, mBuf, ELSY159, pst);

   CMCHKPKLOG(cmPkCmStatus, &trc->cfm, mBuf, ELSY160, pst);
   CMCHKPKLOG(cmPkHeader,   &trc->hdr, mBuf, ELSY161, pst);
   pst->event = EVTLSYTRCIND;
   RETVALUE(SPstTsk(pst, mBuf));
} /* end of cmPkLsyTrcInd */


/*
*     layer management interface un-packing functions
*/

/*
*
*       Fun:   Un-pack Config Request
*
*       Desc:  This function is used to a un-pack config request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsyCfgReq
(
LsyCfgReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf              /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsyCfgReq(func, pst, mBuf)
LsyCfgReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;             /* message buffer */
#endif
{
   SyMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsyCfgReq)

     /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(SyMngmt));

   /* hi009.104 - call the new MACRO to init struc to all zeros */
   LSY_ZERO((U8 *)&mgt, sizeof(SyMngmt));

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSY162, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSY163, pst);

   switch (mgt.hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }

      case STTSAP:
      {
         break;
      }

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LSYLOGERROR(pst, ELSY228, mgt.hdr.elmId.elmnt,
                     "cmUnpkLsyCfgReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);

   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Config Confirm
*
*       Desc:  This function is used to a un-pack config confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsyCfgCfm
(
LsyCfgCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsyCfgCfm(func, pst, mBuf)
LsyCfgCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SyMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsyCfgCfm)

   /* hi009.104 - added missing init to 0 */
	/* lhi_c_001.main_8: Modified HI_ZERO to LHI_ZERO */
   LHI_ZERO((U8*)&mgt, sizeof(SyMngmt))

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSY229, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSY230, pst);

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);

   RETVALUE(ROK);
} /* end of cmUnpkLsyCfgCfm */


/*
*
*       Fun:   Un-pack Control Request
*
*       Desc:  This function is used to un-pack the control request
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsyCntrlReq
(
LsyCntrlReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf              /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsyCntrlReq(func, pst, mBuf)
LsyCntrlReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;             /* message buffer */
#endif
{
   SyMngmt  mgt;          /* configuration */

   TRC3(cmUnpkLsyCntrlReq)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(SyMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSY231, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSY232, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.cntrl.dt,  mBuf, ELSY233, pst);
   CMCHKUNPKLOG(SUnpkU8, &mgt.t.cntrl.action,    mBuf, ELSY234, pst);
   CMCHKUNPKLOG(SUnpkU8, &mgt.t.cntrl.subAction, mBuf, ELSY235, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
         if(mgt.t.cntrl.subAction == SADBG)
         {
#ifdef DEBUGP
           CMCHKUNPKLOG(SUnpkU32, &(mgt.t.cntrl.ctlType.syDbg.dbgMask),
                        mBuf, ELSY236, pst);
#endif
         }
         break;

      case STGRTSAP:
         if(mgt.t.cntrl.subAction == SAGR_PRIORITY)
         {
            CMCHKUNPKLOG(cmUnpkPrior, &(mgt.t.cntrl.ctlType.priority), mBuf,
                         ELSY241, pst);
         }
         else if(mgt.t.cntrl.subAction == SAGR_ROUTE)
         {
            CMCHKUNPKLOG(cmUnpkRoute, &(mgt.t.cntrl.ctlType.route), mBuf,
                         ELSY242, pst);
         }
         else if(mgt.t.cntrl.subAction == SAGR_DSTPROCID)
         {
            CMCHKUNPKLOG(cmUnpkProcId, &(mgt.t.cntrl.ctlType.dstProcId), mBuf,
                         ELSY243, pst);
         }
         else
         {
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY244, mgt.t.cntrl.subAction,
             "cmUnpkLsyCntrlReq () Failed : invalid subAction(STGRTSAP)");
#endif
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
         }
         break;

      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY245, mgt.hdr.elmId.elmnt,
                     "cmUnpkLsyCntrlReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }/* end of switch */

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Control Confirm
*
*       Desc:  This function is used to un-pack the control confirm
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsyCntrlCfm
(
LsyCntrlCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsyCntrlCfm(func, pst, mBuf)
LsyCntrlCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{
   SyMngmt  cfm;            /* configuration */

   TRC3(cmUnpkLsyCntrlCfm)

   CMCHKUNPKLOG(cmUnpkHeader,   &cfm.hdr, mBuf, ELSY246, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &cfm.cfm, mBuf, ELSY247, pst);

   (Void)SPutMsg(mBuf);
   (*func)(pst, &cfm);
   RETVALUE(ROK);
} /* end of cmUnpkLsyCntrlCfm */



/*
*
*       Fun:   Un-pack Statistics Request
*
*       Desc:  This function is used to un-pack the statistics request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsyStsReq
(
LsyStsReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsyStsReq(func, pst, mBuf)
LsyStsReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SyMngmt  mgt;            /* configuration */
   /* lhi_c_001.main_11: Fix for Klockworks issue */
   Action   action = 0;         /* action type */

   TRC3(cmUnpkLsyStsReq)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(SyMngmt));
   CMCHKUNPKLOG(cmUnpkAction,   &action,  mBuf, ELSY248, pst);
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSY249, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSY250, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
        break;

      case STTSAP:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY252, mgt.hdr.elmId.elmnt,
                     "cmUnpkLsyStsReq () Failed");
#endif
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, action, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLsyStsReq */

/*lhi_c_004.main_1  1. modified statistics req to include messages
                              Tx/Rx in bytes per con per thread */
#ifdef LHI_THR_STS
/*
*
*       Fun:   Un Pack connection Statistics
*
*       Desc:  This function is used to un pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkSyConSts
(
SyConSts *sts,              /* statistics */
Buffer  *mBuf              /* to hold the packed buffer */
)
#else
PUBLIC S16 cmUnpkSyConSts(sts, mBuf)
SyConSts *sts;              /* statistics */
Buffer  *mBuf;             /* to hold the packed buffer */
#endif
{
   /*lhi_c_001.main_11: Added TRC macro for cmUnpkSyConSts()*/
   TRC3(cmUnpkSyConSts)
   CMCHKUNPK(cmUnpkStsCntr, &sts->numRxBytes, mBuf);

   CMCHKUNPK(cmUnpkStsCntr, &sts->numTxBytes, mBuf);
   RETVALUE(ROK);

}/*cmunPkSyConSts*/

/*
*
*       Fun:   Un Pack connection Statistics
*
*       Desc:  This function is used to un pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkSyThrConSts
(
SyThrConSts *sts,              /* statistics */
Pst *pst,
Buffer  *mBuf              /* to hold the packed buffer */
)
#else
PUBLIC S16 cmUnpkSyThrConSts(sts, pst, mBuf)
SyThrConSts *sts;              /* statistics */
Pst *pst;
Buffer  *mBuf;             /* to hold the packed buffer */
#endif
{

   /* this will hold the temporary index value for number of threads */
   S16 iConIndex = 0;

   S16 retValue = ROK;

   /*lhi_c_001.main_11: Added TRC macro for cmUnpkSyThrConSts()*/
   TRC3(cmUnpkSyThrConSts)
      /* lhi_c_001.main_12: Replaced CMCHKPK with CMCHKPKLOG macro */
   CMCHKUNPKLOG(SUnpkU32, &sts->numCons, mBuf, ELSY644, pst);

   sts->conSts = NULLP;

   /* unpack only if Connections are present */
   if (sts->numCons)
   {
      if ( (retValue = SGetSBuf(pst->region, pst->pool, (Data** )&sts->conSts,
                               (sizeof(SyConSts) * sts->numCons))) != ROK)
      {
#if (ERRCLASS & ERRCLS_ADD_RES)
         SLogError(pst->srcEnt, pst->srcInst, pst->srcProcId,
                   __FILE__, __LINE__, (ErrCls)ERRCLS_ADD_RES,
                   (ErrVal)ELSY253, (ErrVal)ERRZERO,
                   "cmUnpkLsyStsCfm: allocating memory for statistics confirm failed");
#endif /*  ERRCLASS & ERRCLS_ADD_RES  */
         RETVALUE(retValue);
      }

      cmMemset( (U8* )sts->conSts, 0, (sizeof(SyConSts) * sts->numCons));

      /* in a loop unpack the statistics of all commands */
      for (iConIndex = sts->numCons - 1; iConIndex >= 0; iConIndex--)
      {
         /* lhi_c_001.main_12: Replaced CMCHKPK with CMCHKPKLOG macro */
         CMCHKUNPKLOG(cmUnpkSyConSts,  &sts->conSts[iConIndex], mBuf, ELSY645, pst);
      }
   }

   RETVALUE(ROK);
}/*cmunPkSyThrConSts*/

#endif /*LHI_THR_STS*/

/*
*
*       Fun:   Un-pack Statistics Confirm
*
*       Desc:  This function is used to un-pack the statistics confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsyStsCfm
(
LsyStsCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsyStsCfm(func, pst, mBuf)
LsyStsCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SyMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsyStsCfm)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(SyMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSY254, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSY255, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.sts.dt,   mBuf, ELSY256, pst);
   CMCHKUNPKLOG(cmUnpkDuration, &mgt.t.sts.dura, mBuf, ELSY257, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STGEN:
      {
         break;
      }
      case STTSAP:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY293, mgt.hdr.elmId.elmnt,
                     "cmUnpkLsyStsCfm () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Status Request
*
*       Desc:  This function is used to un-pack the status request
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsyStaReq
(
LsyStaReq func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsyStaReq(func, pst, mBuf)
LsyStaReq func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{
   SyMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsyStaReq)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(SyMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSY294, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSY295, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STSID:
         break;
      case STTSAP:
         break;
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY297, mgt.hdr.elmId.elmnt,
                     "cmUnpkLsyStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
}


/*
*
*       Fun:   Un-pack Status Confirm
*
*       Desc:  This function is used to un-pack the status confirm
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsyStaCfm
(
LsyStaCfm func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsyStaCfm(func, pst, mBuf)
LsyStaCfm func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SyMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsyStaCfm)

      /* lhi_c_001.main_11: Fix for Klockworks issue */
      cmMemset((U8 *)&mgt, 0, sizeof(SyMngmt));
   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSY298, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSY299, pst);
   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.ssta.dt, mBuf, ELSY300, pst);

   switch(mgt.hdr.elmId.elmnt)
   {
      case STSID:
      {
         break;
      }
      case STTSAP:
      {
         break;
      }
      default:
#if (ERRCLASS & ERRCLS_DEBUG)
         LHILOGERROR(pst, ELSY307, mgt.hdr.elmId.elmnt,
                     "cmUnpkLsyStaReq () Failed");
#endif /* ERRCLS_DEBUG */
         (Void)SPutMsg(mBuf);
         RETVALUE(RFAILED);
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLsyStaCfm */


/*
*
*       Fun:   Un-pack Status Indication
*
*       Desc:  This function is used to un-pack the status indication
*              primitive to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsyStaInd
(
LsyStaInd func,
Pst *pst,                 /* post structure */
Buffer *mBuf               /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsyStaInd(func, pst, mBuf)
LsyStaInd func;
Pst *pst;                 /* post structure */
Buffer *mBuf;              /* message buffer */
#endif
{

   SyMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsyStaInd)

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSY308, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSY309, pst);

   CMCHKUNPKLOG(cmUnpkCmAlarm, &mgt.t.usta.alarm,      mBuf, ELSY310, pst);
   CMCHKUNPKLOG(cmUnpkSpId,    &mgt.t.usta.info.spId,  mBuf, ELSY311, pst);
   CMCHKUNPKLOG(SUnpkU8,       &mgt.t.usta.info.type,  mBuf, ELSY312, pst);

   if(mgt.t.usta.info.type != LHI_ALARMINFO_TYPE_NTPRSNT)
   {
      switch(mgt.t.usta.info.type)
      {
         case LHI_ALARMINFO_SAP_STATE:
            CMCHKUNPKLOG(cmUnpkState, &mgt.t.usta.info.inf.state,
                         mBuf, ELSY313, pst);
            break;

         case LHI_ALARMINFO_CON_STATE:
            CMCHKUNPKLOG(cmUnpkState, &mgt.t.usta.info.inf.conState,
                       mBuf, ELSY314, pst);
            break;

         case LHI_ALARMINFO_MEM_ID:
            CMCHKUNPKLOG(cmUnpkPool, &mgt.t.usta.info.inf.mem.pool, mBuf,
                       ELSY315, pst);
            CMCHKUNPKLOG(cmUnpkRegion, &mgt.t.usta.info.inf.mem.region, mBuf,
                       ELSY316, pst);
            break;

         case LHI_ALARMINFO_PAR_TYPE:
            CMCHKUNPKLOG(SUnpkU8, &mgt.t.usta.info.inf.parType, mBuf,
                       ELSY317, pst);
            break;

#ifdef HI_TLS
         case LHI_ALARMINFO_OSSL_ERR:
            CMCHKUNPKLOG(SUnpkS32, &mgt.t.usta.info.inf.osslError, mBuf,
                         ELSY318, pst);
            break;
#endif
            /*lhi_c_001.main_11:Added new usta.info.type and protected under
             * HI_LKSCTP flag */
#ifdef HI_LKSCTP
         case LHI_ALARMINFO_ASSOC_ERR:
            CMCHKPKLOG(SPkU32, mgt.t.usta.info.assocId, mBuf,
                  ELSY636, pst);
            CMCHKPKLOG(SPkU16, mgt.t.usta.info.error, mBuf,
                  ELSY635, pst);
            break;
#endif

         default:
#if (ERRCLASS & ERRCLS_DEBUG)
            LHILOGERROR(pst, ELSY319, mgt.t.usta.info.type,
                        "cmUnpkLsyStaInd () Failed");
#endif /* ERRCLS_DEBUG */
            (Void)SPutMsg(mBuf);
            RETVALUE(RFAILED);
      }/* end of switch */
   }

   (Void)SPutMsg(mBuf);
   (*func)(pst, &mgt);
   RETVALUE(ROK);
} /* end of cmUnpkLsyStaInd */



/*
*
*       Fun:   Un-pack Trace Indication
*
*       Desc:  This function is used to un-pack the trace indication
*              primitive to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  lhi.c
*
*/

#ifdef ANSI
PUBLIC S16 cmUnpkLsyTrcInd
(
LsyTrcInd func,
Pst *pst,                 /* post */
Buffer  *mBuf             /* message buffer */
)
#else
PUBLIC S16 cmUnpkLsyTrcInd(func, pst, mBuf)
LsyTrcInd func;
Pst *pst;                 /* post */
Buffer  *mBuf;            /* message buffer */
#endif
{

   SyMngmt  mgt;            /* configuration */

   TRC3(cmUnpkLsyTrcInd)

   CMCHKUNPKLOG(cmUnpkHeader,   &mgt.hdr, mBuf, ELSY320, pst);
   CMCHKUNPKLOG(cmUnpkCmStatus, &mgt.cfm, mBuf, ELSY321, pst);

   CMCHKUNPKLOG(cmUnpkDateTime, &mgt.t.trc.dt, mBuf, ELSY322, pst);
   CMCHKUNPKLOG(SUnpkU16, &mgt.t.trc.evnt, mBuf, ELSY323, pst);
   /*lhi_c_002.main_1 (hi023.104) */
	/* lhi_c_001.main_8: Removed warnings and removed hi.h included
	 *                   header file */
#ifdef HI_ENB_SAP_TRC
   CMCHKUNPKLOG(SUnpkU16, (U16*)&mgt.t.trc.sap, mBuf, ELSY324, pst);
#endif

   (*func)(pst, &mgt, mBuf);
   RETVALUE(ROK);
} /* end of cmUnpkLsyTrcInd */

#endif /* LCLSY */

/********************************************************************30**

         End of file:     lhi.c@@/main/13 - Tue Apr 26 18:11:18 2011

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat     init                  description
----------- --------- ---- -----------------------------------------------
1.1         ---       bw   1. initial release.
*********************************************************************91*/
