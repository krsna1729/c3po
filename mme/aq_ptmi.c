/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*

The following functions are provided in this file:
     AqMiLaqCfgCfm      Configuration Confirm
     AqMiLaqCntrlCfm    Control Confirm
     AqMiLaqStsCfm      Statistics Confirm
     AqMiLaqStaInd      Status Indication
     AqMiLaqStaCfm      Status Confirm
     AqMiLaqTrcInd      Trace Indication

It should be noted that not all of these functions may be required
by a particular layer management service user.

It is assumed that the following functions are provided in TUCL:
     AqMiLaqCfgReq      Configuration Request
     AqMiLaqCntrlReq    Control Request
     AqMiLaqStsReq      Statistics Request
     AqMiLaqStaReq      Status Request

*/

/* header include files (.h) */

#include "envopt.h"             /* environment options */
#include "envdep.h"             /* environment dependent */
#include "envind.h"             /* environment independent */

#include "gen.h"                /* general layer */
#include "ssi.h"                /* system services interface */

/* external headers */
#ifdef HI_TLS
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/ssl.h>
#include <openssl/x509v3.h>
#endif

#include "cm_hash.h"            /* common hash list */
#include "cm_llist.h"           /* common linked list */
#include "cm5.h"                /* common timer */
#include "cm_inet.h"            /* common sockets */
#include "cm_tpt.h"             /* common transport defines */

/* header/extern include files (.x) */

#include "gen.x"                /* general layer */
#include "ssi.x"                /* system services interface */

#include "cm_hash.x"            /* common hashing */
#include "cm_llist.x"           /* common linked list */
#include "cm_lib.x"             /* common library */
#include "cm5.x"                /* common timer */
#include "cm_inet.x"            /* common sockets */
#include "cm_tpt.x"             /* common transport typedefs */

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"

/* local defines */

#define MAXAQMI 2

#ifndef LCAQMILAQ
#define PTHIMILAQ
#else
#ifndef SM
#define PTAQMILAQ
#endif
#endif



#ifdef PTAQMILAQ
/* declaration of portable functions */

#ifdef __cplusplus
extern "C" {
#endif

PRIVATE S16 PtMiLhiCfgCfm     ARGS((Pst *pst, AqMngmt  *cfg));
PRIVATE S16 PtMiLhiCntrlCfm   ARGS((Pst *pst, AqMngmt *cntrl));
PRIVATE S16 PtMiLhiStaCfm     ARGS((Pst *pst, AqMngmt *sta));
PRIVATE S16 PtMiLhiStaInd     ARGS((Pst *pst, AqMngmt *usta));
PRIVATE S16 PtMiLhiStsCfm     ARGS((Pst *pst, AqMngmt *sts));
PRIVATE S16 PtMiLhiTrcInd     ARGS((Pst *pst, AqMngmt *trc, Buffer *mBuf));

#ifdef __cplusplus
}
#endif

#endif /* PTHIMILHI */

#ifdef FTHA
#ifndef SH
EXTERN S16 PtHiMiShtCntrlCfm ARGS((Pst *pst, ShtCntrlCfmEvnt *cfmInfo));
#endif
#endif


/*
  The following matrices define the mapping between the primitives
  called by the upper interface of TUCL and the corresponding
  primitives of the TCP UDP Convergence Layer service user(s).

  The parameter MAXHIMI defines the maximum number of service
  users on top of TUCL. There is an array of functions per
  primitive invoked by TUCL. Every array is MAXHIMI long (i.e.
  there are as many functions as the number of service users).

  The dispatching is performed by the configurable variable:
  selector. The selector is configured on a per SAP basis.

  The selectors are:

   0 - loosely coupled (#define LCHIMILHI) 1 - LHI (#define SM)

*/



/* Configuration Confirm */

PRIVATE LaqCfgCfm AqMiLaqCfgCfmMt[MAXAQMI] =
{
#ifdef LCAQMILAQ
   cmPkLaqCfgCfm,          /* 0 - loosely coupled  */
#else
   PtMiLaqCfgCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLaqCfgCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLaqCfgCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Control Confirm */

PRIVATE LaqCntrlCfm AqMiLaqCntrlCfmMt[MAXAQMI] =
{
#ifdef LCAQMILAQ
   cmPkLaqCntrlCfm,          /* 0 - loosely coupled  */
#else
   PtMiLaqCntrlCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLaqCntrlCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLaqCntrlCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Statistics Confirm */

PRIVATE LaqStsCfm AqMiLaqStsCfmMt[MAXAQMI] =
{
#ifdef LCAQMILAQ
   cmPkLaqStsCfm,          /* 0 - loosely coupled  */
#else
   PtMiLaqStsCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLaqStsCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLaqStsCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Status Indication */

PRIVATE LaqStaInd AqMiLaqStaIndMt[MAXAQMI] =
{
#ifdef LCAQMILAQ
   cmPkLaqStaInd,          /* 0 - loosely coupled  */
#else
   PtMiLaqStaInd,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLaqStaInd,          /* 1 - tightly coupled, layer management */
#else
   PtMiLaqStaInd,          /* 1 - tightly coupled, portable */
#endif
};

/* Status Confirm */

PRIVATE LaqStaCfm AqMiLaqStaCfmMt[MAXAQMI] =
{
#ifdef LCAQMILAQ
   cmPkLaqStaCfm,          /* 0 - loosely coupled  */
#else
   PtMiLaqStaCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLaqStaCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLaqStaCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Trace Indication */

PRIVATE LaqTrcInd AqMiLaqTrcIndMt[MAXAQMI] =
{
#ifdef LCAQMILAQ
   cmPkLaqTrcInd,          /* 0 - loosely coupled  */
#else
   PtMiLaqTrcInd,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLaqTrcInd,          /* 1 - tightly coupled, layer management */
#else
   PtMiLaqTrcInd,          /* 1 - tightly coupled, portable */
#endif
};

#ifdef FTHA
/* System Agent Control Confirm */
PRIVATE ShtCntrlCfm HiMiShtCntrlCfmMt[MAXHIMI] =
{
   cmPkMiShtCntrlCfm,      /* 0 - loosely coupled */
#ifdef SH
   ShMiShtCntrlCfm,        /* 1 - tightly coupled system agent */
#else
   PtHiMiShtCntrlCfm,      /* 1 - tightly coupled, portable */
#endif
};


/*
*
*       Fun:   System Agent Control Confirm
*
*       Desc:  This function is used to send the system agent control confirm
*              primitive
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  hi_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 HiMiShtCntrlCfm
(
Pst *pst,                /* post structure */
ShtCntrlCfmEvnt *cfmInfo     /* system agent control confirm event */
)
#else
PUBLIC S16 HiMiShtCntrlCfm(pst, cfmInfo)
Pst *pst;                /* post structure */
ShtCntrlCfmEvnt *cfmInfo;    /* system agent control confirm event */
#endif
{
   TRC3(HiMiShtCntrlCfm)

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*HiMiShtCntrlCfmMt[pst->selector])(pst, cfmInfo));
} /* end of HiMiShtCntrlCfm */

#ifndef SH

/*
*
*       Fun:   Portable system agent control Confirm
*
*       Desc:  This function is used to send the system agent control confirm
*              primitive
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  hi_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 PtHiMiShtCntrlCfm
(
Pst *pst,                /* post structure */
ShtCntrlCfmEvnt *cfmInfo     /* system agent control confirm event */
)
#else
PUBLIC S16 PtHiMiShtCntrlCfm(pst, cfmInfo)
Pst *pst;                /* post structure */
ShtCntrlCfmEvnt *cfmInfo;    /* system agent control confirm event */
#endif
{
   TRC3(PtHiMiShtCntrlCfm)

   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
         "AqMiLaqCfgCfm(pst, cfg))\n"));

   RETVALUE(ROK);
} /* end of PtHiMiShtCntrlCfm */
#endif /* SH */
#endif /* FTHA */


/*
*     Layer Management Interface Functions
*/


/*
*
*       Fun:   Configuration confirm
*
*       Desc:  This function is used to send a configuration confirm
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  aq_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 AqMiLaqCfgCfm
(
Pst     *pst,            /* post structure */
AqMngmt *cfg             /* configuration */
)
#else
PUBLIC S16 AqMiLaqCfgCfm(pst, cfg)
Pst     *pst;            /* post structure */
AqMngmt *cfg;            /* configuration */
#endif
{
   TRC3(AqMiLaqCfgCfm)

   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
         "AqMiLaqCfgCfm(pst, cfg (0x%p))\n", (Ptr)cfg));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*AqMiLaqCfgCfmMt[pst->selector])(pst, cfg));
} /* end of AqMiLaqCfgCfm */


/*
*
*       Fun:   Control confirm
*
*       Desc:  This function is used to send a control confirm
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  aq_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 AqMiLaqCntrlCfm
(
Pst     *pst,            /* post structure */
AqMngmt *cntrl           /* control */
)
#else
PUBLIC S16 AqMiLaqCntrlCfm(pst, cntrl)
Pst     *pst;            /* post structure */
AqMngmt *cntrl;          /* control */
#endif
{
   TRC3(AqMiLaqCntrlCfm)

   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
         "AqMiLaqCntrlCfm(pst, cntrl (0x%p))\n", (Ptr)cntrl));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*AqMiLaqCntrlCfmMt[pst->selector])(pst, cntrl));
} /* end of AqMiLaqCntrlCfm */


/*
*
*       Fun:   Status Indication
*
*       Desc:  This function is used to indicate the status
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  aq_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 AqMiLaqStaInd
(
Pst     *pst,            /* post structure */
AqMngmt *usta             /* unsolicited status */
)
#else
PUBLIC S16 AqMiLaqStaInd(pst, usta)
Pst     *pst;            /* post structure */
AqMngmt *usta;            /* unsolicited status */
#endif
{
   TRC3(AqMiLaqStaInd)

   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
          "AqMiLaqStaInd(pst, usta (0x%p))\n", (Ptr)usta));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*AqMiLaqStaIndMt[pst->selector])(pst, usta));
} /* end of AqMiLaqStaInd */


/*
*
*       Fun:   Status Confirm
*
*       Desc:  This function is used to return the status
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  aq_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 AqMiLaqStaCfm
(
Pst     *pst,            /* post structure */
AqMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 AqMiLaqStaCfm(pst, sta)
Pst     *pst;            /* post structure */
AqMngmt *sta;             /* solicited status */
#endif
{
   TRC3(AqMiLaqStaCfm)

   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
          "AqMiLaqStaCfm(pst, sta (0x%p))\n", (Ptr)sta));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*AqMiLaqStaCfmMt[pst->selector])(pst, sta));
} /* end of AqMiLaqStaCfm */


/*
*
*       Fun:   Statistics Confirm
*
*       Desc:  This function is used to return the statistics
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  aq_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 AqMiLaqStsCfm
(
Pst     *pst,                /* post structure */
AqMngmt *sts                 /* statistics */
)
#else
PUBLIC S16 AqMiLaqStsCfm(pst, sts)
Pst     *pst;                /* post structure */
AqMngmt *sts;                /* statistics */
#endif
{
   TRC3(AqMiLaqStsCfm)

   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
         "AqMiLaqStsCfm(pst, sts (0x%p))\n", (Ptr)sts));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*AqMiLaqStsCfmMt[pst->selector])(pst, sts));
} /* end of AqMiLaqStsCfm */


/*
*
*       Fun:   Trace Indication
*
*       Desc:  This function is used to indicate the trace
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  hi_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 AqMiLaqTrcInd
(
Pst     *pst,            /* post structure */
AqMngmt *trc,             /* unsolicited status */
Buffer  *mBuf              /* message buffer */
)
#else
PUBLIC S16 AqMiLaqTrcInd(pst, trc, mBuf)
Pst     *pst;            /* post structure */
AqMngmt *trc;             /* unsolicited status */
Buffer  *mBuf;             /* message buffer */
#endif
{
   TRC3(AqMiLaqTrcInd)

   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
         "AqMiLaqTrcInd(pst, trc (0x%p))\n", (Ptr)trc));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*AqMiLaqTrcIndMt[pst->selector])(pst, trc, mBuf));
} /* end of AqMiLaqTrcInd */

#ifdef PTHIMILHI

/*
*
*       Fun:   Portable Configuration confirm
*
*       Desc:
*
*       Ret:   None
*
*       Notes: None
*
*       File:  hi_ptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLhiCfgCfm
(
Pst     *pst,              /* post structure */
AqMngmt *cfg               /* configuration */
)
#else
PRIVATE S16 PtMiLhiCfgCfm(pst, cfg)
Pst     *pst;              /* post structure */
AqMngmt *cfg;              /* configuration */
#endif
{
   TRC3(PtMiLhiCfgCfm)

   UNUSED(pst);
   UNUSED(cfg);

#if (ERRCLASS & ERRCLS_DEBUG)
   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
         "PtMiLhiCfgCfm () Failed\n"));
#endif

   RETVALUE(ROK);
} /* end of PtMiLhiCfgCfm */


/*
*
*       Fun:   Portable Control confirm
*
*       Desc:
*
*       Ret:   None
*
*       Notes: None
*
*       File:  hi_ptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLhiCntrlCfm
(
Pst     *pst,              /* post structure */
AqMngmt *cntrl             /* control */
)
#else
PRIVATE S16 PtMiLhiCntrlCfm(pst, cntrl)
Pst     *pst;              /* post structure */
AqMngmt *cntrl;            /* control */
#endif
{
   TRC3(PtMiLhiCntrlCfm)

   UNUSED(pst);
   UNUSED(cntrl);

#if (ERRCLASS & ERRCLS_DEBUG)
   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
         "PtMiLhiCntrlCfm () Failed\n"));
#endif

   RETVALUE(ROK);
} /* end of PtMiLhiCntrlCfm */

/*
*
*       Fun:   Portable Status Confirm
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  hi_ptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLhiStaCfm
(
Pst     *pst,              /* post structure */
AqMngmt *sta                /* solicited status */
)
#else
PRIVATE S16 PtMiLhiStaCfm(pst, sta)
Pst     *pst;              /* post structure */
AqMngmt *sta;               /* solicited status */
#endif
{
   TRC3(PtMiLhiStaCfm)

   UNUSED(pst);
   UNUSED(sta);

#if (ERRCLASS & ERRCLS_DEBUG)
   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
         "PtMiLhiStaCfm () Failed\n"));
#endif

   RETVALUE(ROK);
} /* end of PtMiLhiStaCfm */


/*
*
*       Fun:   Portable Status Indication
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  hi_ptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLhiStaInd
(
Pst     *pst,               /* post structure */
AqMngmt *usta                /* unsolicited status */
)
#else
PRIVATE S16 PtMiLhiStaInd(pst, usta)
Pst     *pst;               /* post structure */
AqMngmt *usta;               /* unsolicited status */
#endif
{
   TRC3(PtMiLhiStaInd)

   UNUSED(pst);
   UNUSED(usta);

#if (ERRCLASS & ERRCLS_DEBUG)
   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
         "PtMiLhiStaInd () Failed\n"));
#endif

   RETVALUE(ROK);
} /* end of PtMiLhiStaInd */


/*
*
*       Fun:   Portable Statistics Confirm
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  hi_ptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLhiStsCfm
(
Pst     *pst,              /* post structure */
AqMngmt *sts               /* statistics */
)
#else
PRIVATE S16 PtMiLhiStsCfm(pst, sts)
Pst     *pst;              /* post structure */
AqMngmt *sts;              /* statistics */
#endif
{
   TRC3(PtMiLhiStsCfm)

   UNUSED(pst);
   UNUSED(sts);

#if (ERRCLASS & ERRCLS_DEBUG)
   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
        "PtMiLhiStsCfm () Failed\n"));
#endif

  RETVALUE(ROK);
} /* end of PtMiLhiStsCfm */


/*
*
*       Fun:   Portable Trace Indication
*
*       Desc:
*
*       Ret:   None
*
*       Notes: None
*
*       File:  hi_ptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLhiTrcInd
(
Pst     *pst,              /* post structure */
AqMngmt *trc,              /* trace */
Buffer  *mBuf              /* message buffer */
)
#else
PRIVATE S16 PtMiLhiTrcInd(pst, trc, mBuf)
Pst     *pst;              /* post structure */
AqMngmt *trc;               /* trace */
Buffer  *mBuf;             /* message buffer */
#endif
{
   TRC3(PtMiLhiTrcInd)

   UNUSED(pst);
   UNUSED(trc);

#if (ERRCLASS & ERRCLS_DEBUG)
   AQDBGP(DBGMASK_MI, (aqCb.init.prntBuf,
         "PtMiLhiTrcInd () Failed\n"));
#endif

   RETVALUE(ROK);
} /* end of PtMiLhiTrcInd */

#endif /* PTHIMILHI */


/********************************************************************30**

         End of file:     hi_ptmi.c@@/main/6 - Mon Mar  3 20:09:54 2008

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision History:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      asa  1. initial release.
/main/2     ---      cvp  1. changed the copyright header.
            /main/4  cvp  1. changes for sht interface.
/main/4     ---      cvp  1. changed the copyright header.
/main/5      ---      kp   1. Updated for release 1.5.
/main/6      ---       hs   1. Updated for release of 2.1
*********************************************************************91*/

