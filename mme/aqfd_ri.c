/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter Convergence Layer

     Type:     C source file

     Desc:     freeDiameter initialization.

     File:     aqfd_ri.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */
#include "cm_os.h"

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */
#include "cm_os.x"

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"

#include "lsy.h"
#include "lsy.x"

#include "lsw.h"
#include "lsw.x"

#include "aqfd.h"
#include "aqfd.x"
#include "aqfd_ri.h"

#include "sy.h"
#include "sy.x"
#include "sy_err.h"

#include "sw.h"
#include "sw.x"
#include "sw_err.h"

PRIVATE Void _aqfd_ria_cb ARGS((void * data, struct msg ** msg));
PRIVATE S16 createRIAInfo ARGS((VbMmeUeCb *ueCb, VbMonitoringEventConfiguration *mevnt, SwRIAInfo **riinfo));

/*
 *  Thread local Pst related variables and init routine
 */
static __thread int _pstInitialized;
static __thread Pst _pst;

PRIVATE Void _initPst ARGS((void));

PRIVATE Void _initPst()
{
   if (!_pstInitialized)
   {
      memcpy( &_pst, &aqCb.cfg.sxPst, sizeof(_pst) );
      _pstInitialized = 1;
   }
}

#ifdef ANSI
PRIVATE S16 createRIAInfo
(
VbMmeUeCb *ueCb,
VbMonitoringEventConfiguration *mevnt,
SwRIAInfo **riinfo
)
#else
PRIVATE S16 createRIAInfo(ueCb, mevnt, riinfo)
VbMmeUeCb *ueCb;
VbMonitoringEventConfiguration *mevnt;
SwRIAInfo **riinfo;
#endif
{
   /* create RI info object */
   *riinfo = (SwRIAInfo*)malloc( sizeof(**riinfo) );
   if (!*riinfo)
   {
      LOG_E("%s:%d - createRIAInfo() - unable to allocate SwRIAInfo object", __FILE__, __LINE__);
      return LCM_REASON_MEM_NOAVAIL;
   }
   memset( *riinfo, 0, sizeof(**riinfo) );

   aqfdImsi2Str(ueCb->ueCtxt.ueImsi, ueCb->ueCtxt.ueImsiLen, (*riinfo)->imsi);
   memcpy( (*riinfo)->scef_id, mevnt->scef_id->val, mevnt->scef_id->len );
   (*riinfo)->scef_reference_id = mevnt->scef_reference_id;
   (*riinfo)->monitoring_type = mevnt->monitoring_type;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdSendRIR_*
*
*       Desc:   Send Reporting-Information-Request to the SCEF.
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*
*       Notes:  None
*
*       File:   aqfd_ri.c
*
*      The Reporting-Information-Request (RIR) command is sent
*      from MME to the SCEF.
*
*      < Reporting-Information-Request > ::=  < Diameter Header: 8388719, PXY, 16777346 >
*        < Session-Id >
*        [ DRMP ]
*        { Auth-Session-State }
*        { Origin-Host }
*        { Origin-Realm }
*        [ Destination-Host ]
*        { Destination-Realm }
*        [ OC-Supported-Features ]
*       *[ Supported-Features ]
*        [ User-Identifier ]
*       *[ Monitoring-Event-Report ]
*       *[ Proxy-Info ]
*       *[ Route-Record ]
*       *[AVP]
*
*      Monitoring-Event-Report ::= <AVP header: 3123 10415>
*        { SCEF-Reference-ID }
*        [ SCEF-ID ]
*        [ Monitoring-Type ]
*        [ Reachability-Information ]
*        [ EPS-Location-Information ]
*        [ Communication-Failure-Information ]
*       *[ Number-Of-UE-Per-Location-Report ]
*        [ Loss-Of-Connectivity-Reason ]
*       *[AVP]
*/
#ifdef ANSI
PUBLIC S16 aqfdSendRIR_UeLossOfConnectivity
(
VbMmeUeCb *ueCb,
VbMonitoringEventConfiguration *mevnt,
U32 reason
)
#else
PUBLIC S16 aqfdSendRIR_UeLossOfConnectivity(ueCb, mevnt, reason)
VbMmeUeCb *ueCb;
VbMonitoringEventConfiguration *mevnt;
U32 reason;
#endif
{
   S16 ret;
   struct avp *avp;
   struct msg *rir;
   SwRIAInfo *riinfo = NULL;
   U8 sessId[128];
   U8 sessIdLen = sizeof(sessId);

   /* create RI info object */
   if ((ret = createRIAInfo(ueCb, mevnt, &riinfo)) != LCM_REASON_NOT_APPL)
      RETVALUE(ret);

   /* construct the message */
   AQCHECK_MSG_NEW_APPL( aqDict.cmdRIR, aqDict.appT6A, rir );

   /* Session-Id */
   AQCHECK_FCT_2( aqfdCreateSessionId2(sessId, &sessIdLen) );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Session_Id, rir, MSG_BRW_LAST_CHILD, sessId, sessIdLen);

   /* Auth-Session-State */
   AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Auth_Session_State, rir, MSG_BRW_LAST_CHILD, 1 );

   /* Origin-Host & Origin-Realm */
   AQCHECK_MSG_ADD_ORIGIN( rir );

   /* Destination-Host */
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Destination_Host, rir, MSG_BRW_LAST_CHILD, mevnt->scef_id->val, mevnt->scef_id->len );

   /* Destination-Realm */
   if (mevnt->scef_realm)
   {
      AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Destination_Realm, rir, MSG_BRW_LAST_CHILD, mevnt->scef_realm->val, mevnt->scef_realm->len );
   }
   else
   {
      uint32_t ofs;
      uint32_t realmOfs;
      S8 realm[128];

      if (!mevnt->scef_id)
      {
         LOG_E("%s:%d - aqfdSendRIR_CommunicationFailure() - neither the scef_realm or the scef_id are defined for scef_reference_id %u", __FILE__, __LINE__, mevnt->scef_reference_id);
         return RFAILED;
      }

      for (realmOfs=0; realmOfs<mevnt->scef_id->len && mevnt->scef_id->val[realmOfs] != '.'; realmOfs++);

      realmOfs++; // skip over the '.'
      if (realmOfs >= mevnt->scef_id->len)
      {
         LOG_E("%s:%d - aqfdSendRIR_CommunicationFailure() - The scef_id [%*s] does not contain a realm for scef_reference_id %u", __FILE__, __LINE__,
               mevnt->scef_id->len, mevnt->scef_id->val, mevnt->scef_reference_id);
         return RFAILED;
      }

      for (ofs=0; realmOfs<mevnt->scef_id->len; ofs++,realmOfs++)
         realm[ofs] = (S8)mevnt->scef_id->val[realmOfs];
      realm[ofs] = '\0';

      AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_Destination_Realm, rir, MSG_BRW_LAST_CHILD, realm );
   }

   /* User-Identifier */
   AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_User_Identifier, rir, MSG_BRW_LAST_CHILD, avp );
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_User_Name, avp, MSG_BRW_LAST_CHILD, riinfo->imsi );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_MSISDN, avp, MSG_BRW_LAST_CHILD,
         ueCb->ueCtxt.ueHssCtxt.subscriptionData->msisdn->val,
         ueCb->ueCtxt.ueHssCtxt.subscriptionData->msisdn->len);

   /* Monitoring-Event-Report */
   AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Monitoring_Event_Report, rir, MSG_BRW_LAST_CHILD, avp );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_SCEF_Reference_ID, avp, MSG_BRW_LAST_CHILD, mevnt->scef_reference_id );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_SCEF_ID, avp, MSG_BRW_LAST_CHILD, mevnt->scef_id->val, mevnt->scef_id->len );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Monitoring_Type, avp, MSG_BRW_LAST_CHILD, MT_LOSS_OF_CONNECTIVITY );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Loss_Of_Connectivity_Reason, avp, MSG_BRW_LAST_CHILD, reason );

   /* send the message */
   AQFD_DUMP_MESSAGE(rir);
   AQCHECK_MSG_SEND( &rir, _aqfd_ria_cb, (Void*)riinfo );

   return ROK;
}

#ifdef ANSI
PUBLIC S16 aqfdSendRIR_UeReachability
(
VbMmeUeCb *ueCb,
VbMonitoringEventConfiguration *mevnt
)
#else
PUBLIC S16 aqfdSendRIR_UeReachability(ueCb, mevnt)
VbMmeUeCb *ueCb;
VbMonitoringEventConfiguration *mevnt;
#endif
{
   S16 ret;
   struct avp *avp;
   struct msg *rir;
   SwRIAInfo *riinfo = NULL;
   U8 sessId[128];
   U8 sessIdLen = sizeof(sessId);

   /* create RI info object */
   if ((ret = createRIAInfo(ueCb, mevnt, &riinfo)) != LCM_REASON_NOT_APPL)
      RETVALUE(ret);

   /* construct the message */
   AQCHECK_MSG_NEW_APPL( aqDict.cmdRIR, aqDict.appT6A, rir );

   /* Session-Id */
   AQCHECK_FCT_2( aqfdCreateSessionId2(sessId, &sessIdLen) );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Session_Id, rir, MSG_BRW_LAST_CHILD, sessId, sessIdLen);

   /* Auth-Session-State */
   AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Auth_Session_State, rir, MSG_BRW_LAST_CHILD, 1 );

   /* Origin-Host & Origin-Realm */
   AQCHECK_MSG_ADD_ORIGIN( rir );

   /* Destination-Host */
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Destination_Host, rir, MSG_BRW_LAST_CHILD, mevnt->scef_id->val, mevnt->scef_id->len );

   /* Destination-Realm */
   if (mevnt->scef_realm)
   {
      AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Destination_Realm, rir, MSG_BRW_LAST_CHILD, mevnt->scef_realm->val, mevnt->scef_realm->len );
   }
   else
   {
      uint32_t ofs;
      uint32_t realmOfs;
      S8 realm[128];

      if (!mevnt->scef_id)
      {
         LOG_E("%s:%d - aqfdSendRIR_CommunicationFailure() - neither the scef_realm or the scef_id are defined for scef_reference_id %u", __FILE__, __LINE__, mevnt->scef_reference_id);
         return RFAILED;
      }

      for (realmOfs=0; realmOfs<mevnt->scef_id->len && mevnt->scef_id->val[realmOfs] != '.'; realmOfs++);

      realmOfs++; // skip over the '.'
      if (realmOfs >= mevnt->scef_id->len)
      {
         LOG_E("%s:%d - aqfdSendRIR_CommunicationFailure() - The scef_id [%*s] does not contain a realm for scef_reference_id %u", __FILE__, __LINE__,
               mevnt->scef_id->len, mevnt->scef_id->val, mevnt->scef_reference_id);
         return RFAILED;
      }

      for (ofs=0; realmOfs<mevnt->scef_id->len; ofs++,realmOfs++)
         realm[ofs] = (S8)mevnt->scef_id->val[realmOfs];
      realm[ofs] = '\0';

      AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_Destination_Realm, rir, MSG_BRW_LAST_CHILD, realm );
   }

   /* User-Identifier */
   AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_User_Identifier, rir, MSG_BRW_LAST_CHILD, avp );
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_User_Name, avp, MSG_BRW_LAST_CHILD, riinfo->imsi );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_MSISDN, avp, MSG_BRW_LAST_CHILD,
         ueCb->ueCtxt.ueHssCtxt.subscriptionData->msisdn->val,
         ueCb->ueCtxt.ueHssCtxt.subscriptionData->msisdn->len);

   /* Monitoring-Event-Report */
   AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Monitoring_Event_Report, rir, MSG_BRW_LAST_CHILD, avp );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_SCEF_Reference_ID, avp, MSG_BRW_LAST_CHILD, mevnt->scef_reference_id );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_SCEF_ID, avp, MSG_BRW_LAST_CHILD, mevnt->scef_id->val, mevnt->scef_id->len );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Monitoring_Type, avp, MSG_BRW_LAST_CHILD, MT_UE_REACHABILITY );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Reachability_Information, avp, MSG_BRW_LAST_CHILD, RI_REACHABLE_FOR_DATA );

   /* send the message */
   AQFD_DUMP_MESSAGE(rir);
   AQCHECK_MSG_SEND( &rir, _aqfd_ria_cb, (Void*)riinfo );

   return ROK;
}

#ifdef ANSI
PUBLIC S16 aqfdSendRIR_CommunicationFailureS1AP
(
VbMmeUeCb *ueCb,
VbMonitoringEventConfiguration *mevnt,
U32 causeType,
U32 s1apCause
)
#else
PUBLIC S16 aqfdSendRIR_CommunicationFailureS1AP(ueCb, mevnt, causeType, s1apCause)
VbMmeUeCb *ueCb;
VbMonitoringEventConfiguration *mevnt;
U32 causeType;
U32 s1apCause;
#endif
{
   S16 ret;
   struct avp *avp;
   struct msg *rir;
   SwRIAInfo *riinfo = NULL;
   U8 sessId[128];
   U8 sessIdLen = sizeof(sessId);

   /* create RI info object */
   if ((ret = createRIAInfo(ueCb, mevnt, &riinfo)) != LCM_REASON_NOT_APPL)
      RETVALUE(ret);

   /* construct the message */
   AQCHECK_MSG_NEW_APPL( aqDict.cmdRIR, aqDict.appT6A, rir );

   /* Session-Id */
   AQCHECK_FCT_2( aqfdCreateSessionId2(sessId, &sessIdLen) );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Session_Id, rir, MSG_BRW_LAST_CHILD, sessId, sessIdLen);

   /* Auth-Session-State */
   AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Auth_Session_State, rir, MSG_BRW_LAST_CHILD, 1 );

   /* Origin-Host & Origin-Realm */
   AQCHECK_MSG_ADD_ORIGIN( rir );

   /* Destination-Host */
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Destination_Host, rir, MSG_BRW_LAST_CHILD, mevnt->scef_id->val, mevnt->scef_id->len );

   /* Destination-Realm */
   if (mevnt->scef_realm)
   {
      AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Destination_Realm, rir, MSG_BRW_LAST_CHILD, mevnt->scef_realm->val, mevnt->scef_realm->len );
   }
   else
   {
      uint32_t ofs;
      uint32_t realmOfs;
      S8 realm[128];

      if (!mevnt->scef_id)
      {
         LOG_E("%s:%d - aqfdSendRIR_CommunicationFailure() - neither the scef_realm or the scef_id are defined for scef_reference_id %u", __FILE__, __LINE__, mevnt->scef_reference_id);
         return RFAILED;
      }

      for (realmOfs=0; realmOfs<mevnt->scef_id->len && mevnt->scef_id->val[realmOfs] != '.'; realmOfs++);

      realmOfs++; // skip over the '.'
      if (realmOfs >= mevnt->scef_id->len)
      {
         LOG_E("%s:%d - aqfdSendRIR_CommunicationFailure() - The scef_id [%*s] does not contain a realm for scef_reference_id %u", __FILE__, __LINE__,
               mevnt->scef_id->len, mevnt->scef_id->val, mevnt->scef_reference_id);
         return RFAILED;
      }

      for (ofs=0; realmOfs<mevnt->scef_id->len; ofs++,realmOfs++)
         realm[ofs] = (S8)mevnt->scef_id->val[realmOfs];
      realm[ofs] = '\0';

      AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_Destination_Realm, rir, MSG_BRW_LAST_CHILD, realm );
   }

   /* User-Identifier */
   AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_User_Identifier, rir, MSG_BRW_LAST_CHILD, avp );
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_User_Name, avp, MSG_BRW_LAST_CHILD, riinfo->imsi );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_MSISDN, avp, MSG_BRW_LAST_CHILD,
         ueCb->ueCtxt.ueHssCtxt.subscriptionData->msisdn->val,
         ueCb->ueCtxt.ueHssCtxt.subscriptionData->msisdn->len);

   /* Monitoring-Event-Report */
   AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Monitoring_Event_Report, rir, MSG_BRW_LAST_CHILD, avp );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_SCEF_Reference_ID, avp, MSG_BRW_LAST_CHILD, mevnt->scef_reference_id );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_SCEF_ID, avp, MSG_BRW_LAST_CHILD, mevnt->scef_id->val, mevnt->scef_id->len );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Monitoring_Type, avp, MSG_BRW_LAST_CHILD, MT_COMMUNICATION_FAILURE );

   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Cause_Type, avp, MSG_BRW_LAST_CHILD, causeType );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_S1AP_Cause, avp, MSG_BRW_LAST_CHILD, s1apCause );

   /* send the message */
   AQFD_DUMP_MESSAGE(rir);
   AQCHECK_MSG_SEND( &rir, _aqfd_ria_cb, (Void*)riinfo );

   return ROK;
}

#ifdef ANSI
PUBLIC S16 aqfdSendRIR_AvailabilityAfterDDNFailure
(
VbMmeUeCb *ueCb,
VbMonitoringEventConfiguration *mevnt
)
#else
PUBLIC S16 aqfdSendRIR_AvailabilityAfterDDNFailure(ueCb, mevnt)
VbMmeUeCb *ueCb;
VbMonitoringEventConfiguration *mevnt;
#endif
{
   S16 ret;
   struct avp *avp;
   struct msg *rir;
   SwRIAInfo *riinfo = NULL;
   U8 sessId[128];
   U8 sessIdLen = sizeof(sessId);

   /* create RI info object */
   if ((ret = createRIAInfo(ueCb, mevnt, &riinfo)) != LCM_REASON_NOT_APPL)
      RETVALUE(ret);

   /* construct the message */
   AQCHECK_MSG_NEW_APPL( aqDict.cmdRIR, aqDict.appT6A, rir );

   /* Session-Id */
   AQCHECK_FCT_2( aqfdCreateSessionId2(sessId, &sessIdLen) );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Session_Id, rir, MSG_BRW_LAST_CHILD, sessId, sessIdLen);

   /* Auth-Session-State */
   AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Auth_Session_State, rir, MSG_BRW_LAST_CHILD, 1 );

   /* Origin-Host & Origin-Realm */
   AQCHECK_MSG_ADD_ORIGIN( rir );

   /* Destination-Host */
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Destination_Host, rir, MSG_BRW_LAST_CHILD, mevnt->scef_id->val, mevnt->scef_id->len );

   /* Destination-Realm */
   if (mevnt->scef_realm)
   {
      AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Destination_Realm, rir, MSG_BRW_LAST_CHILD, mevnt->scef_realm->val, mevnt->scef_realm->len );
   }
   else
   {
      uint32_t ofs;
      uint32_t realmOfs;
      S8 realm[128];

      if (!mevnt->scef_id)
      {
         LOG_E("%s:%d - aqfdSendRIR_CommunicationFailure() - neither the scef_realm or the scef_id are defined for scef_reference_id %u", __FILE__, __LINE__, mevnt->scef_reference_id);
         return RFAILED;
      }

      for (realmOfs=0; realmOfs<mevnt->scef_id->len && mevnt->scef_id->val[realmOfs] != '.'; realmOfs++);

      realmOfs++; // skip over the '.'
      if (realmOfs >= mevnt->scef_id->len)
      {
         LOG_E("%s:%d - aqfdSendRIR_CommunicationFailure() - The scef_id [%*s] does not contain a realm for scef_reference_id %u", __FILE__, __LINE__,
               mevnt->scef_id->len, mevnt->scef_id->val, mevnt->scef_reference_id);
         return RFAILED;
      }

      for (ofs=0; realmOfs<mevnt->scef_id->len; ofs++,realmOfs++)
         realm[ofs] = (S8)mevnt->scef_id->val[realmOfs];
      realm[ofs] = '\0';

      AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_Destination_Realm, rir, MSG_BRW_LAST_CHILD, realm );
   }

   /* User-Identifier */
   AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_User_Identifier, rir, MSG_BRW_LAST_CHILD, avp );
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_User_Name, avp, MSG_BRW_LAST_CHILD, riinfo->imsi );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_MSISDN, avp, MSG_BRW_LAST_CHILD,
         ueCb->ueCtxt.ueHssCtxt.subscriptionData->msisdn->val,
         ueCb->ueCtxt.ueHssCtxt.subscriptionData->msisdn->len);

   /* Monitoring-Event-Report */
   AQCHECK_MSG_ADD_AVP_GROUPED_2( aqDict.avp_Monitoring_Event_Report, rir, MSG_BRW_LAST_CHILD, avp );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_SCEF_Reference_ID, avp, MSG_BRW_LAST_CHILD, mevnt->scef_reference_id );
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_SCEF_ID, avp, MSG_BRW_LAST_CHILD, mevnt->scef_id->val, mevnt->scef_id->len );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_Monitoring_Type, avp, MSG_BRW_LAST_CHILD, MT_AVBL_AFTER_DDN_FAILURE );

   /* send the message */
   AQFD_DUMP_MESSAGE(rir);
   /* AQCHECK_MSG_SEND( &rir, _aqfd_ria_cb, (Void*)riinfo ); */
   AQCHECK_MSG_SEND( &rir, NULL, NULL );

   return ROK;
}

/*
*
*       Fun:    aqfd_ria_cb
*
*       Desc:   Reporting-Information-Answer callback
*
*       Ret:    0
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       < Reporting-Information-Answer > ::=   < Diameter Header: 8388719, PXY, 16777346 >
*         < Session-Id >
*         [ DRMP ]
*         [ Result-Code ]
*         [ Experimental-Result ]
*         { Auth-Session-State }
*         { Origin-Host }
*         { Origin-Realm }
*         [ OC-Supported-Features ]
*         [ OC-OLR ]
*        *[ Load ]
*        *[ Supported-Features ]
*         [ Failed-AVP ]
*        *[ Proxy-Info ]
*        *[ Route-Record ]
*        *[AVP]
*/
#ifdef ANSI
PUBLIC int aqfd_ria_cb
(
struct msg ** msg,
struct avp * pavp,
struct session * sess,
void * data,
enum disp_action * act
)
#else
PUBLIC int aqfd_ria_cb(msg, pavp, sess, data, act)
struct msg ** msg;
struct avp * pavp;
struct session * sess;
void * data;
enum disp_action * act;
#endif
{
   fd_msg_free( *msg );
   *msg = NULL;
   return 0;
}

#ifdef ANSI
PRIVATE Void _aqfd_ria_cb
(
void * data,
struct msg ** msg
)
#else
PRIVATE Void _aqfd_ria_cb(data, msg)
void * data;
struct msg ** msg;
#endif
{
   S16 ret = ROK;
   U32 rc;
   struct msg   *ans = NULL;
   struct msg   *qry = NULL;
   struct avp   *avp = NULL;
   SwRIAInfo *riinfo = (SwRIAInfo*)data;

   ans = *msg;

AQFD_DUMP_MESSAGE(ans);

   /* get Result-Code */
   AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_Result_Code, avp, );
   if (avp)
   {
      /* Result-Code found */
      AQCHECK_AVP_GET_U32(aqDict.avp_Result_Code, avp, riinfo->result_code, );
      if (!riinfo->result_code)
      {
         LOG_E("%s:%d - _aqfd_ria_cb() - Error Result-Code AVP invalid value = %u", __FILE__, __LINE__, rc);
         ret = RFAILED;
         goto fini1;
      }
   }
   else
   {
      /* Result-Code not found, check for Experimental-Result */
      AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_Experimental_Result, avp, );
      if (!avp)
      {
         LOG_E("%s:%d - _aqfd_ria_cb() - Error Experimental-Result AVP not found", __FILE__, __LINE__ );
         ret = LAQ_REASON_FD_EXPERIMENTAL_RESULT;
         goto fini1;
      }

      AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_Experimental_Result, avp, );
      if (avp)
      {
         if ((ret = aqfdParseExperimentalResult(avp, &riinfo->exp_result)) != LCM_REASON_NOT_APPL)
         {
            LOG_E("%s:%d - _aqfd_ria_cb() - Error %d parsing Experimental-Result", __FILE__, __LINE__, ret);
            goto fini1;
         }
      }
   }

   if (riinfo->exp_result.present || riinfo->exp_result.result_code == 5515) /* DIAMETER_ERROR_ SCEF_REFERENCE_ID_UNKNOWN */
   {
      /* push ria */
      aqQueuePush(&aqCb.riaQueue, riinfo);

      _initPst();
      _pst.event = EVTLSWRIA;
      {
         Buffer *buf = NULL;
         if(SGetMsg(aqCb.cfg.swPst.region, aqCb.cfg.swPst.pool, &buf) != ROK)
            LOG_E("Error %d returned from SGetMsg() sending RIA notification", ret);
         else
         {
            if ((ret = SPstTsk(&_pst, buf)) == ROK)
            {
               riinfo = NULL; // prevent from being released
            }
            else
            {
               LOG_E("Error %d returned from SPstTsk() sending RIA notification", ret);
               SPutMsg(buf);
            }
         }
      }
   }

fini1:
   if (riinfo)
      free(riinfo);
}

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision History:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

