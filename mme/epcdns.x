/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


#ifndef __EPCDNS_X
#define __EPCDNS_X

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void dnsQuerySgwTac( const U8 *plmnid, U16 tac, const U8 *imsi, U8 imsilen );


#ifdef __cplusplus
}
#endif

#endif /* __EPCDNS_X */
