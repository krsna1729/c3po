/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Stack Manager - portable Diameter

     Type:     C source file

     Desc:     Code for S6A Convergence Layer  layer management
               primitives.

     File:     smsyptmi.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/*

Layer management provides the necessary functions to control and
monitor the condition of each protocol layer.

The following functions are provided in this file:

     SmMiLsyCfgReq      Configure Request
     SmMiLsyStaReq      Status Request
     SmMiLsyStsReq      Statistics Request
     SmMiLsyCntrlReq    Control Request

It is assumed that the following functions are provided in the
stack management body files:

     SmMiLsyStaInd      Status Indication
     SmMiLsyStaCfm      Status Confirm
     SmMiLsyStsCfm      Statistics Confirm
     SmMiLsyTrcInd      Trace Indication

*/


/* header include files (.h) */

#include "envopt.h"        /* environment options */
#include "envdep.h"        /* environment dependent */
#include "envind.h"        /* environment independent */
#include "gen.h"           /* general layer */
#include "cm5.h"           /* common timer */
#include "ssi.h"           /* system services */
#include "cm_inet.h"       /* common sockets */
#include "lsy.h"           /* SY layer management */
#include "cm_err.h"        /* common error */
#include "smsy_err.h"      /* SY error */

/* header/extern include files (.x) */

#include "gen.x"           /* general layer */
#include "ssi.x"           /* system services */
#include "cm5.x"           /* common timer */
#include "cm_inet.x"       /* common sockets */
#include "lsy.x"           /* sy layer management */

#define  MAXSYMI  2

#ifndef  LCSMSYMILSY
#define  PTSMSYMILSY
#else
#ifndef   SY
#define  PTSMSYMILSY
#endif
#endif

#ifdef PTSMSYMILSY
PRIVATE S16 PtMiLsyCfgReq    ARGS((Pst *pst, SyMngmt *cfg));
PRIVATE S16 PtMiLsyStsReq    ARGS((Pst *pst, Action action, SyMngmt *sts));
PRIVATE S16 PtMiLsyCntrlReq  ARGS((Pst *pst, SyMngmt *cntrl));
PRIVATE S16 PtMiLsyStaReq    ARGS((Pst *pst, SyMngmt *sta));
#endif


/*
the following matrices define the mapping between the primitives
called by the layer management interface of TCP UDP Convergence Layer
and the corresponding primitives in TUCL

The parameter MAXSYMI defines the maximum number of layer manager
entities on top of TUCL . There is an array of functions per primitive
invoked by TCP UDP Conbvergence Layer. Every array is MAXSYMI long
(i.e. there are as many functions as the number of service users).

The dispatching is performed by the configurable variable: selector.
The selector is configured during general configuration.

The selectors are:

   0 - loosely coupled (#define LCSMSYMILSY) 2 - Lsy (#define SY)

*/


/* Configuration request primitive */

PRIVATE LsyCfgReq SmMiLsyCfgReqMt[MAXSYMI] =
{
#ifdef LCSMSYMILSY
   cmPkLsyCfgReq,          /* 0 - loosely coupled  */
#else
   PtMiLsyCfgReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef SY
   SyMiLsyCfgReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsyCfgReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Control request primitive */

PRIVATE LsyCntrlReq SmMiLsyCntrlReqMt[MAXSYMI] =
{
#ifdef LCSMSYMILSY
   cmPkLsyCntrlReq,          /* 0 - loosely coupled  */
#else
   PtMiLsyCntrlReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef SY
   SyMiLsyCntrlReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsyCntrlReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Statistics request primitive */

PRIVATE LsyStsReq SmMiLsyStsReqMt[MAXSYMI] =
{
#ifdef LCSMSYMILSY
   cmPkLsyStsReq,          /* 0 - loosely coupled  */
#else
   PtMiLsyStsReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef SY
   SyMiLsyStsReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsyStsReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Status request primitive */

PRIVATE LsyStaReq SmMiLsyStaReqMt[MAXSYMI] =
{
#ifdef LCSMSYMILSY
   cmPkLsyStaReq,          /* 0 - loosely coupled  */
#else
   PtMiLsyStaReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef SY
   SyMiLsyStaReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsyStaReq,          /* 1 - tightly coupled, portable */
#endif
};


/*
*     layer management interface functions
*/

/*
*
*       Fun:   Configuration request
*
*       Desc:  This function is used to configure  TUCL
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsyptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLsyCfgReq
(
Pst *spst,                /* post structure */
SyMngmt *cfg              /* configure */
)
#else
PUBLIC S16 SmMiLsyCfgReq(spst, cfg)
Pst *spst;                /* post structure */
SyMngmt *cfg;             /* configure */
#endif
{
   TRC3(SmMiLsyCfgReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLsyCfgReqMt[spst->selector])(spst, cfg);
   RETVALUE(ROK);
} /* end of SmMiLsyCfgReq */



/*
*
*       Fun:   Statistics request
*
*       Desc:  This function is used to request statistics from
*              TUCL
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsyptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLsyStsReq
(
Pst *spst,                /* post structure */
Action action,
SyMngmt *sts              /* statistics */
)
#else
PUBLIC S16 SmMiLsyStsReq(spst, action, sts)
Pst *spst;                /* post structure */
Action action;
SyMngmt *sts;             /* statistics */
#endif
{
   TRC3(SmMiLsyStsReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLsyStsReqMt[spst->selector])(spst, action, sts);
   RETVALUE(ROK);
} /* end of SmMiLsyStsReq */


/*
*
*       Fun:   Control request
*
*       Desc:  This function is used to send control request to
*              TUCL
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsyptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLsyCntrlReq
(
Pst *spst,                 /* post structure */
SyMngmt *cntrl            /* control */
)
#else
PUBLIC S16 SmMiLsyCntrlReq(spst, cntrl)
Pst *spst;                 /* post structure */
SyMngmt *cntrl;           /* control */
#endif
{
   TRC3(SmMiLsyCntrlReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLsyCntrlReqMt[spst->selector])(spst, cntrl);
   RETVALUE(ROK);
} /* end of SmMiLsyCntrlReq */


/*
*
*       Fun:   Status request
*
*       Desc:  This function is used to send a status request to
*              TUCL
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsyptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLsyStaReq
(
Pst *spst,                /* post structure */
SyMngmt *sta              /* status */
)
#else
PUBLIC S16 SmMiLsyStaReq(spst, sta)
Pst *spst;                /* post structure */
SyMngmt *sta;             /* status */
#endif
{
   TRC3(SmMiLsyStaReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLsyStaReqMt[spst->selector])(spst, sta);
   RETVALUE(ROK);
} /* end of SmMiLsyStaReq */

#ifdef PTSMSYMILSY

/*
 *             Portable Functions
 */

/*
*
*       Fun:   Portable configure Request- TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsyptmi.c
*
*/

#ifdef ANSI
PRIVATE S16 PtMiLsyCfgReq
(
Pst *spst,                  /* post structure */
SyMngmt *cfg                /* configure */
)
#else
PRIVATE S16 PtMiLsyCfgReq(spst, cfg)
Pst *spst;                  /* post structure */
SyMngmt *cfg;               /* configure */
#endif
{
  TRC3(PtMiLsyCfgReq)

  UNUSED(spst);
  UNUSED(cfg);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMSYLOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
              ERRCLS_DEBUG, ESMSY002, 0, "PtMiLsyCfgReq () Failed");
#endif

  RETVALUE(ROK);
} /* end of PtMiLsyCfgReq */



/*
*
*       Fun:   Portable statistics Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsyptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLsyStsReq
(
Pst *spst,                  /* post structure */
Action action,
SyMngmt *sts                /* statistics */
)
#else
PRIVATE S16 PtMiLsyStsReq(spst, action, sts)
Pst *spst;                  /* post structure */
Action action;
SyMngmt *sts;               /* statistics */
#endif
{
  TRC3(PtMiLsyStsReq)

  UNUSED(spst);
  UNUSED(action);
  UNUSED(sts);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMSYLOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
                ERRCLS_DEBUG, ESMSY003, 0, "PtMiLsyStsReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLsyStsReq */


/*
*
*       Fun:   Portable control Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsyptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLsyCntrlReq
(
Pst *spst,                  /* post structure */
SyMngmt *cntrl              /* control */
)
#else
PRIVATE S16 PtMiLsyCntrlReq(spst, cntrl)
Pst *spst;                  /* post structure */
SyMngmt *cntrl;             /* control */
#endif
{
  TRC3(PtMiLsyCntrlReq)

  UNUSED(spst);
  UNUSED(cntrl);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMSYLOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
             ERRCLS_DEBUG, ESMSY004, 0, "PtMiLsyCntrlReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLsyCntrlReq */


/*
*
*       Fun:   Portable status Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsyptmi.c
*
*/

#ifdef ANSI
PRIVATE S16 PtMiLsyStaReq
(
Pst *spst,                  /* post structure */
SyMngmt *sta                /* status */
)
#else
PRIVATE S16 PtMiLsyStaReq(spst, sta)
Pst *spst;                  /* post structure */
SyMngmt *sta;               /* status */
#endif
{
  TRC3(PtMiLsyStaReq);

  UNUSED(spst);
  UNUSED(sta);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMSYLOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
                ERRCLS_DEBUG,ESMSY005, 0, "PtMiLsyStaReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLsyStaReq */

#endif /* PTSMSYMILSY */


/********************************************************************30**

         End of file:     smsyptmi.c@@/main/5 - Mon Mar  3 20:09:32 2008

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      asa  1. initial release.
*********************************************************************91*/
