/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


#ifndef __AQFDX__
#define __AQFDX__

#include <freeDiameter/freeDiameter-host.h>
#include <freeDiameter/libfdproto.h>
#include <freeDiameter/libfdcore.h>

#include "aqfd2.x"

typedef struct aqDict {
   struct dict_object * vnd3GPP;
   struct dict_object * appS6A;
   struct dict_object * appT6A;
   struct dict_object * appSGD;
   
   struct dict_vendor_data vnd3GPPdata;
   struct dict_application_data appS6Adata;
   struct dict_application_data appT6Adata;
   struct dict_application_data appSGDdata;

   /* S6A commands */
   struct dict_object * cmdAIR;
   struct dict_object * cmdAIA;
   struct dict_object * cmdULR;
   struct dict_object * cmdULA;
   struct dict_object * cmdPUR;
   struct dict_object * cmdPUA;
   struct dict_object * cmdCLR;
   struct dict_object * cmdCLA;
   struct dict_object * cmdIDR;
   struct dict_object * cmdIDA;
   
   /* T6A commands */
   struct dict_object * cmdRIR;
   struct dict_object * cmdRIA;
   
   /* SGD commands */
   struct dict_object * cmdOFR;
   struct dict_object * cmdOFA;
   struct dict_object * cmdTFR;
   struct dict_object * cmdTFA;

   struct dict_object * avp_Result_Code;
   struct dict_object * avp_Experimental_Result;
   struct dict_object * avp_Experimental_Result_Code;
   struct dict_object * avp_Vendor_Specific_Application_Id;
   struct dict_object * avp_Auth_Application_Id;
   struct dict_object * avp_Acct_Application_Id;
   struct dict_object * avp_Vendor_Id;
   struct dict_object * avp_Session_Id;
   struct dict_object * avp_Auth_Session_State;
   struct dict_object * avp_Origin_Host;
   struct dict_object * avp_Origin_Realm;
   struct dict_object * avp_Destination_Host;
   struct dict_object * avp_Destination_Realm;
   struct dict_object * avp_User_Name;
   struct dict_object * avp_Visited_PLMN_Id;
   struct dict_object * avp_Requested_EUTRAN_Authentication_Info;
   struct dict_object * avp_Number_Of_Requested_Vectors;
   struct dict_object * avp_Immediate_Response_Preferred;
   struct dict_object * avp_Re_Synchronization_Info;
   struct dict_object * avp_Authentication_Info;
   struct dict_object * avp_E_UTRAN_Vector;
   struct dict_object * avp_Item_Number;
   struct dict_object * avp_RAND;
   struct dict_object * avp_XRES;
   struct dict_object * avp_AUTN;
   struct dict_object * avp_KASME;
   struct dict_object * avp_RAT_Type;
   struct dict_object * avp_ULR_Flags;
   struct dict_object * avp_ULA_Flags;
   struct dict_object * avp_Subscription_Data;
   struct dict_object * avp_Subscriber_Status;
   struct dict_object * avp_MSISDN;
   struct dict_object * avp_A_MSISDN;
   struct dict_object * avp_STN_SR;
   struct dict_object * avp_ICS_Indicator;
   struct dict_object * avp_Network_Access_Mode;
   struct dict_object * avp_Operator_Determined_Barring;
   struct dict_object * avp_HPLMN_ODB;
   struct dict_object * avp_Regional_Subscription_Zone_Code;
   struct dict_object * avp_Access_Restriction_Data;
   struct dict_object * avp_APN_OI_Replacement;
   struct dict_object * avp_LCS_Info;
   struct dict_object * avp_GMLC_Number;
   struct dict_object * avp_LCS_PrivacyException;
   struct dict_object * avp_MO_LR;
   struct dict_object * avp_SS_Code;
   struct dict_object * avp_SS_Status;
   struct dict_object * avp_Notification_To_UE_User;
   struct dict_object * avp_External_Client;
   struct dict_object * avp_PLMN_Client;
   struct dict_object * avp_Service_Type;
   struct dict_object * avp_Service_Type_Identity;
   struct dict_object * avp_Client_Identity;
   struct dict_object * avp_GMLC_Restriction;
   struct dict_object * avp_Teleservice_List;
   struct dict_object * avp_TS_Code;
   struct dict_object * avp_Call_Barring_Info;
   struct dict_object * avp_3GPP_Charging_Characteristics;
   struct dict_object * avp_AMBR;
   struct dict_object * avp_Max_Requested_Bandwidth_UL;
   struct dict_object * avp_Max_Requested_Bandwidth_DL;
   struct dict_object * avp_APN_Configuration_Profile;
   struct dict_object * avp_Context_Identifier;
   struct dict_object * avp_Additional_Context_Identifier;
   struct dict_object * avp_All_APN_Configurations_Included_Indicator;
   struct dict_object * avp_APN_Configuration;
   struct dict_object * avp_Served_Party_IP_Address;
   struct dict_object * avp_PDN_Type;
   struct dict_object * avp_Service_Selection;
   struct dict_object * avp_EPS_Subscribed_QoS_Profile;
   struct dict_object * avp_VPLMN_Dynamic_Address_Allowed;
   struct dict_object * avp_MIP6_Agent_Info;
   struct dict_object * avp_Visited_Network_Identifier;
   struct dict_object * avp_PDN_GW_Allocation_Type;
   struct dict_object * avp_Specific_APN_Info;
   struct dict_object * avp_SIPTO_Permission;
   struct dict_object * avp_LIPA_Permission;
   struct dict_object * avp_Restoration_Priority;
   struct dict_object * avp_SIPTO_Local_Network_Permission;
   struct dict_object * avp_WLAN_offloadability;
   struct dict_object * avp_Non_IP_PDN_Type_Indicator;
   struct dict_object * avp_Non_IP_Data_Delivery_Mechanism;
   struct dict_object * avp_SCEF_ID;
   struct dict_object * avp_SCEF_Realm;
   struct dict_object * avp_Preferred_Data_Mode;
   struct dict_object * avp_PDN_Connection_Continuity;
   struct dict_object * avp_QoS_Class_Identifier;
   struct dict_object * avp_Allocation_Retention_Priority;
   struct dict_object * avp_Priority_Level;
   struct dict_object * avp_Pre_emption_Capability;
   struct dict_object * avp_Pre_emption_Vulnerability;
   struct dict_object * avp_MIP_Home_Agent_Address;
   struct dict_object * avp_MIP_Home_Agent_Host;
   struct dict_object * avp_WLAN_offloadability_EUTRAN;
   struct dict_object * avp_WLAN_offloadability_UTRAN;
   struct dict_object * avp_RAT_Frequency_Selection_Priority_ID;
   struct dict_object * avp_Trace_Data;
   struct dict_object * avp_Trace_Reference;
   struct dict_object * avp_Trace_Depth;
   struct dict_object * avp_Trace_NE_Type_List;
   struct dict_object * avp_Trace_Interface_List;
   struct dict_object * avp_Trace_Event_List;
   struct dict_object * avp_OMC_Id;
   struct dict_object * avp_Trace_Collection_Entity;
   struct dict_object * avp_MDT_Configuration;
   struct dict_object * avp_Job_Type;
   struct dict_object * avp_Area_Scope;
   struct dict_object * avp_List_Of_Measurements;
   struct dict_object * avp_Reporting_Trigger;
   struct dict_object * avp_Report_Interval;
   struct dict_object * avp_Report_Amount;
   struct dict_object * avp_Event_Threshold_RSRP;
   struct dict_object * avp_Event_Threshold_RSRQ;
   struct dict_object * avp_Logging_Interval;
   struct dict_object * avp_Logging_Duration;
   struct dict_object * avp_Measurement_Period_LTE;
   struct dict_object * avp_Measurement_Period_UMTS;
   struct dict_object * avp_Collection_Period_RRM_LTE;
   struct dict_object * avp_Collection_Period_RRM_UMTS;
   struct dict_object * avp_Positioning_Method;
   struct dict_object * avp_Measurement_Quantity;
   struct dict_object * avp_Event_Threshold_Event_1F;
   struct dict_object * avp_Event_Threshold_Event_1I;
   struct dict_object * avp_MDT_Allowed_PLMN_Id;
   struct dict_object * avp_Cell_Global_Identity;
   struct dict_object * avp_E_UTRAN_Cell_Global_Identity;
   struct dict_object * avp_Routing_Area_Identity;
   struct dict_object * avp_Location_Area_Identity;
   struct dict_object * avp_Tracking_Area_Identity;
   struct dict_object * avp_GPRS_Subscription_Data;
   struct dict_object * avp_Complete_Data_List_Included_Indicator;
   struct dict_object * avp_PDP_Context;
   struct dict_object * avp_PDP_Address;
   struct dict_object * avp_QoS_Subscribed;
   struct dict_object * avp_Ext_PDP_Type;
   struct dict_object * avp_Ext_PDP_Address;
   struct dict_object * avp_CSG_Subscription_Data;
   struct dict_object * avp_Roaming_Restricted_Due_To_Unsupported_Feature;
   struct dict_object * avp_Subscribed_Periodic_RAU_TAU_Timer;
   struct dict_object * avp_MPS_Priority;
   struct dict_object * avp_VPLMN_LIPA_Allowed;
   struct dict_object * avp_Relay_Node_Indicator;
   struct dict_object * avp_MDT_User_Consent;
   struct dict_object * avp_Subscribed_VSRVCC;
   struct dict_object * avp_ProSe_Subscription_Data;
   struct dict_object * avp_Subscription_Data_Flags;
   struct dict_object * avp_Adjacent_Access_Restriction_Data;
   struct dict_object * avp_DL_Buffering_Suggested_Packet_Count;
   struct dict_object * avp_IMSI_Group_Id;
   struct dict_object * avp_CSG_Id;
   struct dict_object * avp_Expiration_Date;
   struct dict_object * avp_ProSe_Permission;
   struct dict_object * avp_Group_Service_Id;
   struct dict_object * avp_Group_PLMN_Id;
   struct dict_object * avp_Local_Group_Id;
   struct dict_object * avp_UE_Usage_Type;
   struct dict_object * avp_AESE_Communication_Pattern;
   struct dict_object * avp_SCEF_Reference_ID;
   struct dict_object * avp_SCEF_Reference_ID_for_Deletion;
   struct dict_object * avp_Communication_Pattern_Set;
   struct dict_object * avp_Periodic_Communication_Indicator;
   struct dict_object * avp_Communication_Duration_Time;
   struct dict_object * avp_Periodic_Time;
   struct dict_object * avp_Scheduled_Communication_Time;
   struct dict_object * avp_Stationary_Indication;
   struct dict_object * avp_Reference_ID_Validity_Time;
   struct dict_object * avp_Day_Of_Week_Mask;
   struct dict_object * avp_Time_Of_Day_Start;
   struct dict_object * avp_Time_Of_Day_End;
   struct dict_object * avp_Monitoring_Event_Configuration;
   struct dict_object * avp_Monitoring_Type;
   struct dict_object * avp_Maximum_Number_of_Reports;
   struct dict_object * avp_Monitoring_Duration;
   struct dict_object * avp_Charged_Party;
   struct dict_object * avp_UE_Reachability_Configuration;
   struct dict_object * avp_Location_Information_Configuration;
   struct dict_object * avp_Reachability_Type;
   struct dict_object * avp_Maximum_Response_Time;
   struct dict_object * avp_MONTE_Location_Type;
   struct dict_object * avp_Accuracy;
   struct dict_object * avp_Emergency_Info;
   struct dict_object * avp_V2X_Subscription_Data;
   struct dict_object * avp_V2X_Permission;
   struct dict_object * avp_UE_PC5_AMBR;
   struct dict_object * avp_eDRX_Cycle_Length;
   struct dict_object * avp_eDRX_Cycle_Length_Value;
   struct dict_object * avp_PUR_Flags;
   struct dict_object * avp_Monitoring_Event_Config_Status;
   struct dict_object * avp_Service_Report;
   struct dict_object * avp_Node_Type;
   struct dict_object * avp_Service_Result;
   struct dict_object * avp_Service_Result_Code;
   struct dict_object * avp_Monitoring_Event_Report;

   /* T6A AVP's */
   struct dict_object * avp_Reachability_Information;
   struct dict_object * avp_Maximum_UE_Availability_Time;
   struct dict_object * avp_EPS_Location_Information;
   struct dict_object * avp_Event_Handling;
   struct dict_object * avp_Loss_Of_Connectivity_Reason;
   struct dict_object * avp_MME_Location_Information;
   struct dict_object * avp_SGSN_Location_Information;
   struct dict_object * avp_Geographical_Information;
   struct dict_object * avp_Geodetic_Information;
   struct dict_object * avp_Current_Location_Retrieved;
   struct dict_object * avp_Age_Of_Location_Information;
   struct dict_object * avp_User_CSG_Information;
   struct dict_object * avp_eNodeB_Id;
   struct dict_object * avp_Extended_eNodeB_Id;
   struct dict_object * avp_Service_Area_Identity;
   struct dict_object * avp_CSG_Access_Mode;
   struct dict_object * avp_CSG_Membership_Indication;
   struct dict_object * avp_Cause_Type;
   struct dict_object * avp_S1AP_Cause;
   struct dict_object * avp_RANAP_Cause;
   struct dict_object * avp_BSSGP_Cause;
   struct dict_object * avp_GMM_Cause;
   struct dict_object * avp_SM_Cause;
   
   /* SGD AVP's */
   struct dict_object * avp_SC_Address;
   struct dict_object * avp_User_Identifier;
   struct dict_object * avp_SM_RP_UI;
   struct dict_object * avp_SMSMI_Correlation_ID;
   struct dict_object * avp_OFR_Flags;
   struct dict_object * avp_SM_Delivery_Outcome;
   struct dict_object * avp_Supported_Features;
   struct dict_object * avp_SM_Delivery_Failure_Cause;
   struct dict_object * avp_SM_Enumerated_Delivery_Failure_Cause;
   struct dict_object * avp_SM_Diagnostic_Info;
   struct dict_object * avp_External_Identifier;
 
   /* S6A AVP Data */
   struct dict_avp_data davp_Result_Code;
   struct dict_avp_data davp_Experimental_Result;
   struct dict_avp_data davp_Experimental_Result_Code;
   struct dict_avp_data davp_Vendor_Specific_Application_Id;
   struct dict_avp_data davp_Auth_Application_Id;
   struct dict_avp_data davp_Acct_Application_Id;
   struct dict_avp_data davp_Vendor_Id;
   struct dict_avp_data davp_Session_Id;
   struct dict_avp_data davp_Auth_Session_State;
   struct dict_avp_data davp_Origin_Host;
   struct dict_avp_data davp_Origin_Realm;
   struct dict_avp_data davp_Destination_Host;
   struct dict_avp_data davp_Destination_Realm;
   struct dict_avp_data davp_User_Name;
   struct dict_avp_data davp_Visited_PLMN_Id;
   struct dict_avp_data davp_Requested_EUTRAN_Authentication_Info;
   struct dict_avp_data davp_Number_Of_Requested_Vectors;
   struct dict_avp_data davp_Immediate_Response_Preferred;
   struct dict_avp_data davp_Re_Synchronization_Info;
   struct dict_avp_data davp_Authentication_Info;
   struct dict_avp_data davp_E_UTRAN_Vector;
   struct dict_avp_data davp_Item_Number;
   struct dict_avp_data davp_RAND;
   struct dict_avp_data davp_XRES;
   struct dict_avp_data davp_AUTN;
   struct dict_avp_data davp_KASME;
   struct dict_avp_data davp_RAT_Type;
   struct dict_avp_data davp_ULR_Flags;
   struct dict_avp_data davp_ULA_Flags;
   struct dict_avp_data davp_Subscription_Data;
   struct dict_avp_data davp_Subscriber_Status;
   struct dict_avp_data davp_MSISDN;
   struct dict_avp_data davp_A_MSISDN;
   struct dict_avp_data davp_STN_SR;
   struct dict_avp_data davp_ICS_Indicator;
   struct dict_avp_data davp_Network_Access_Mode;
   struct dict_avp_data davp_Operator_Determined_Barring;
   struct dict_avp_data davp_HPLMN_ODB;
   struct dict_avp_data davp_Regional_Subscription_Zone_Code;
   struct dict_avp_data davp_Access_Restriction_Data;
   struct dict_avp_data davp_APN_OI_Replacement;
   struct dict_avp_data davp_LCS_Info;
   struct dict_avp_data davp_GMLC_Number;
   struct dict_avp_data davp_LCS_PrivacyException;
   struct dict_avp_data davp_MO_LR;
   struct dict_avp_data davp_SS_Code;
   struct dict_avp_data davp_SS_Status;
   struct dict_avp_data davp_Notification_To_UE_User;
   struct dict_avp_data davp_External_Client;
   struct dict_avp_data davp_PLMN_Client;
   struct dict_avp_data davp_Service_Type;
   struct dict_avp_data davp_Service_Type_Identity;
   struct dict_avp_data davp_Client_Identity;
   struct dict_avp_data davp_GMLC_Restriction;
   struct dict_avp_data davp_Teleservice_List;
   struct dict_avp_data davp_TS_Code;
   struct dict_avp_data davp_Call_Barring_Info;
   struct dict_avp_data davp_3GPP_Charging_Characteristics;
   struct dict_avp_data davp_AMBR;
   struct dict_avp_data davp_Max_Requested_Bandwidth_UL;
   struct dict_avp_data davp_Max_Requested_Bandwidth_DL;
   struct dict_avp_data davp_APN_Configuration_Profile;
   struct dict_avp_data davp_Context_Identifier;
   struct dict_avp_data davp_Additional_Context_Identifier;
   struct dict_avp_data davp_All_APN_Configurations_Included_Indicator;
   struct dict_avp_data davp_APN_Configuration;
   struct dict_avp_data davp_Served_Party_IP_Address;
   struct dict_avp_data davp_PDN_Type;
   struct dict_avp_data davp_Service_Selection;
   struct dict_avp_data davp_EPS_Subscribed_QoS_Profile;
   struct dict_avp_data davp_VPLMN_Dynamic_Address_Allowed;
   struct dict_avp_data davp_MIP6_Agent_Info;
   struct dict_avp_data davp_Visited_Network_Identifier;
   struct dict_avp_data davp_PDN_GW_Allocation_Type;
   struct dict_avp_data davp_Specific_APN_Info;
   struct dict_avp_data davp_SIPTO_Permission;
   struct dict_avp_data davp_LIPA_Permission;
   struct dict_avp_data davp_Restoration_Priority;
   struct dict_avp_data davp_SIPTO_Local_Network_Permission;
   struct dict_avp_data davp_WLAN_offloadability;
   struct dict_avp_data davp_Non_IP_PDN_Type_Indicator;
   struct dict_avp_data davp_Non_IP_Data_Delivery_Mechanism;
   struct dict_avp_data davp_SCEF_ID;
   struct dict_avp_data davp_SCEF_Realm;
   struct dict_avp_data davp_Preferred_Data_Mode;
   struct dict_avp_data davp_PDN_Connection_Continuity;
   struct dict_avp_data davp_QoS_Class_Identifier;
   struct dict_avp_data davp_Allocation_Retention_Priority;
   struct dict_avp_data davp_Priority_Level;
   struct dict_avp_data davp_Pre_emption_Capability;
   struct dict_avp_data davp_Pre_emption_Vulnerability;
   struct dict_avp_data davp_MIP_Home_Agent_Address;
   struct dict_avp_data davp_MIP_Home_Agent_Host;
   struct dict_avp_data davp_WLAN_offloadability_EUTRAN;
   struct dict_avp_data davp_WLAN_offloadability_UTRAN;
   struct dict_avp_data davp_RAT_Frequency_Selection_Priority_ID;
   struct dict_avp_data davp_Trace_Data;
   struct dict_avp_data davp_Trace_Reference;
   struct dict_avp_data davp_Trace_Depth;
   struct dict_avp_data davp_Trace_NE_Type_List;
   struct dict_avp_data davp_Trace_Interface_List;
   struct dict_avp_data davp_Trace_Event_List;
   struct dict_avp_data davp_OMC_Id;
   struct dict_avp_data davp_Trace_Collection_Entity;
   struct dict_avp_data davp_MDT_Configuration;
   struct dict_avp_data davp_Job_Type;
   struct dict_avp_data davp_Area_Scope;
   struct dict_avp_data davp_List_Of_Measurements;
   struct dict_avp_data davp_Reporting_Trigger;
   struct dict_avp_data davp_Report_Interval;
   struct dict_avp_data davp_Report_Amount;
   struct dict_avp_data davp_Event_Threshold_RSRP;
   struct dict_avp_data davp_Event_Threshold_RSRQ;
   struct dict_avp_data davp_Logging_Interval;
   struct dict_avp_data davp_Logging_Duration;
   struct dict_avp_data davp_Measurement_Period_LTE;
   struct dict_avp_data davp_Measurement_Period_UMTS;
   struct dict_avp_data davp_Collection_Period_RRM_LTE;
   struct dict_avp_data davp_Collection_Period_RRM_UMTS;
   struct dict_avp_data davp_Positioning_Method;
   struct dict_avp_data davp_Measurement_Quantity;
   struct dict_avp_data davp_Event_Threshold_Event_1F;
   struct dict_avp_data davp_Event_Threshold_Event_1I;
   struct dict_avp_data davp_MDT_Allowed_PLMN_Id;
   struct dict_avp_data davp_Cell_Global_Identity;
   struct dict_avp_data davp_E_UTRAN_Cell_Global_Identity;
   struct dict_avp_data davp_Routing_Area_Identity;
   struct dict_avp_data davp_Location_Area_Identity;
   struct dict_avp_data davp_Tracking_Area_Identity;
   struct dict_avp_data davp_GPRS_Subscription_Data;
   struct dict_avp_data davp_Complete_Data_List_Included_Indicator;
   struct dict_avp_data davp_PDP_Context;
   struct dict_avp_data davp_PDP_Type;
   struct dict_avp_data davp_PDP_Address;
   struct dict_avp_data davp_QoS_Subscribed;
   struct dict_avp_data davp_Ext_PDP_Type;
   struct dict_avp_data davp_Ext_PDP_Address;
   struct dict_avp_data davp_CSG_Subscription_Data;
   struct dict_avp_data davp_Roaming_Restricted_Due_To_Unsupported_Feature;
   struct dict_avp_data davp_Subscribed_Periodic_RAU_TAU_Timer;
   struct dict_avp_data davp_MPS_Priority;
   struct dict_avp_data davp_VPLMN_LIPA_Allowed;
   struct dict_avp_data davp_Relay_Node_Indicator;
   struct dict_avp_data davp_MDT_User_Consent;
   struct dict_avp_data davp_Subscribed_VSRVCC;
   struct dict_avp_data davp_ProSe_Subscription_Data;
   struct dict_avp_data davp_Subscription_Data_Flags;
   struct dict_avp_data davp_Adjacent_Access_Restriction_Data;
   struct dict_avp_data davp_DL_Buffering_Suggested_Packet_Count;
   struct dict_avp_data davp_IMSI_Group_Id;
   struct dict_avp_data davp_CSG_Id;
   struct dict_avp_data davp_Expiration_Date;
   struct dict_avp_data davp_ProSe_Permission;
   struct dict_avp_data davp_Group_Service_Id;
   struct dict_avp_data davp_Group_PLMN_Id;
   struct dict_avp_data davp_Local_Group_Id;
   struct dict_avp_data davp_UE_Usage_Type;
   struct dict_avp_data davp_AESE_Communication_Pattern;
   struct dict_avp_data davp_SCEF_Reference_ID;
   struct dict_avp_data davp_SCEF_Reference_ID_for_Deletion;
   struct dict_avp_data davp_Communication_Pattern_Set;
   struct dict_avp_data davp_Periodic_Communication_Indicator;
   struct dict_avp_data davp_Communication_Duration_Time;
   struct dict_avp_data davp_Periodic_Time;
   struct dict_avp_data davp_Scheduled_Communication_Time;
   struct dict_avp_data davp_Stationary_Indication;
   struct dict_avp_data davp_Reference_ID_Validity_Time;
   struct dict_avp_data davp_Day_Of_Week_Mask;
   struct dict_avp_data davp_Time_Of_Day_Start;
   struct dict_avp_data davp_Time_Of_Day_End;
   struct dict_avp_data davp_Monitoring_Event_Configuration;
   struct dict_avp_data davp_Monitoring_Type;
   struct dict_avp_data davp_Maximum_Number_of_Reports;
   struct dict_avp_data davp_Monitoring_Duration;
   struct dict_avp_data davp_Charged_Party;
   struct dict_avp_data davp_UE_Reachability_Configuration;
   struct dict_avp_data davp_Location_Information_Configuration;
   struct dict_avp_data davp_Reachability_Type;
   struct dict_avp_data davp_Maximum_Response_Time;
   struct dict_avp_data davp_MONTE_Location_Type;
   struct dict_avp_data davp_Accuracy;
   struct dict_avp_data davp_Emergency_Info;
   struct dict_avp_data davp_V2X_Subscription_Data;
   struct dict_avp_data davp_V2X_Permission;
   struct dict_avp_data davp_UE_PC5_AMBR;
   struct dict_avp_data davp_eDRX_Cycle_Length;
   struct dict_avp_data davp_eDRX_Cycle_Length_Value;
   struct dict_avp_data davp_PUR_Flags;
   struct dict_avp_data davp_Monitoring_Event_Config_Status;
   struct dict_avp_data davp_Service_Report;
   struct dict_avp_data davp_Node_Type;
   struct dict_avp_data davp_Service_Result;
   struct dict_avp_data davp_Service_Result_Code;
   struct dict_avp_data davp_Monitoring_Event_Report;
 
   /* T6A AVP Data */
   struct dict_avp_data davp_Reachability_Information;
   struct dict_avp_data davp_Maximum_UE_Availability_Time;
   struct dict_avp_data davp_EPS_Location_Information;
   struct dict_avp_data davp_Event_Handling;
   struct dict_avp_data davp_Loss_Of_Connectivity_Reason;
   struct dict_avp_data davp_MME_Location_Information;
   struct dict_avp_data davp_SGSN_Location_Information;
   struct dict_avp_data davp_Geographical_Information;
   struct dict_avp_data davp_Geodetic_Information;
   struct dict_avp_data davp_Current_Location_Retrieved;
   struct dict_avp_data davp_Age_Of_Location_Information;
   struct dict_avp_data davp_User_CSG_Information;
   struct dict_avp_data davp_eNodeB_Id;
   struct dict_avp_data davp_Extended_eNodeB_Id;
   struct dict_avp_data davp_Service_Area_Identity;
   struct dict_avp_data davp_CSG_Access_Mode;
   struct dict_avp_data davp_CSG_Membership_Indication;
   struct dict_avp_data davp_Cause_Type;
   struct dict_avp_data davp_S1AP_Cause;
   struct dict_avp_data davp_RANAP_Cause;
   struct dict_avp_data davp_BSSGP_Cause;
   struct dict_avp_data davp_GMM_Cause;
   struct dict_avp_data davp_SM_Cause;
   
   /* SGD AVP Data */
   struct dict_avp_data davp_SC_Address;
   struct dict_avp_data davp_User_Identifier;
   struct dict_avp_data davp_SM_RP_UI;
   struct dict_avp_data davp_SMSMI_Correlation_ID;
   struct dict_avp_data davp_OFR_Flags;
   struct dict_avp_data davp_SM_Delivery_Outcome;
   struct dict_avp_data davp_Supported_Features;
   struct dict_avp_data davp_SM_Delivery_Failure_Cause;
   struct dict_avp_data davp_SM_Enumerated_Delivery_Failure_Cause;
   struct dict_avp_data davp_SM_Diagnostic_Info;
   struct dict_avp_data davp_External_Identifier;
 } AqDict;

EXTERN AqDict aqDict;

EXTERN S16  aqfdInit       		ARGS((Void));
EXTERN S16  aqfdDictionaryInit	ARGS((Void));
EXTERN S16  aqfdS6aInit    		ARGS((Void));
EXTERN S16  aqfdT6aInit    		ARGS((Void));
EXTERN S16  aqfdSGdInit    		ARGS((Void));
EXTERN S16  aqfdRegister   		ARGS((Void));
EXTERN S16  aqfdStartFd    		ARGS((Void));

/* S6A callbacks */
EXTERN int aqfd_aia_cb ARGS((struct msg ** msg, struct avp * avp, struct session * sess, void * data, enum disp_action * act));
EXTERN int aqfd_ula_cb ARGS((struct msg ** msg, struct avp * avp, struct session * sess, void * data, enum disp_action * act));
EXTERN int aqfd_pua_cb ARGS((struct msg ** msg, struct avp * avp, struct session * sess, void * data, enum disp_action * act));
EXTERN int aqfd_cla_cb ARGS((struct msg ** msg, struct avp * avp, struct session * sess, void * data, enum disp_action * act));
EXTERN int aqfd_idr_cb ARGS((struct msg ** msg, struct avp * avp, struct session * sess, void * data, enum disp_action * act));

/* T6A callbacks */
EXTERN int aqfd_ria_cb ARGS((struct msg ** msg, struct avp * avp, struct session * sess, void * data, enum disp_action * act));

/* SGD callbacks */
EXTERN int aqfd_ofa_cb ARGS((struct msg ** msg, struct avp * avp, struct session * sess, void * data, enum disp_action * act));
EXTERN int aqfd_tfr_cb ARGS((struct msg ** msg, struct avp * avp, struct session * sess, void * data, enum disp_action * act));

/* S6A requests */
EXTERN S16 aqfdSendAIR ARGS((VbMmeUeCb *ueCb, U8 *autn));
EXTERN S16 aqfdSendULR ARGS((VbMmeUeCb *ueCb, Bool skip_sub_data, Bool initial_attach, U8 event));
EXTERN S16 aqfdSendPUR ARGS((VbMmeUeCb *ueCb, Bool skip_ue_purged_sgsn));

/* S6A answers */
EXTERN int aqfdSendIDA ARGS((struct msg * ida, vendor_id_t vndid, S32 res));

/* T6A requests */
EXTERN S16 aqfdSendRIR_UeLossOfConnectivity ARGS((VbMmeUeCb *ueCb, VbMonitoringEventConfiguration *mevnt, U32 reason));
EXTERN S16 aqfdSendRIR_UeReachability ARGS((VbMmeUeCb *ueCb, VbMonitoringEventConfiguration *mevnt));
EXTERN S16 aqfdSendRIR_CommunicationFailureS1AP ARGS((VbMmeUeCb *ueCb, VbMonitoringEventConfiguration *mevnt, U32 causeType, U32 s1apCause));
EXTERN S16 aqfdSendRIR_AvailabilityAfterDDNFailure ARGS((VbMmeUeCb *ueCb, VbMonitoringEventConfiguration *mevnt));

/* T6A answers */

/* SGD requests */
EXTERN S16 aqfdSendOFR ARGS((VbMmeUeCb *ueCb, VbMmeMoData *modata));

/* SGD answers */
EXTERN S16 aqfdSendTFA ARGS((struct msg *tfr, U32 rc, U32 erc, S32 smc, U8 *diag, U8 diagLen));

/* Utilities */
EXTERN S16 aqfdCreateSessionId ARGS((VbMmeUeCb *ueCb));
EXTERN S16 aqfdCreateSessionId2 ARGS((U8 *sessId, U8 *sessIdLen));
EXTERN Void aqfdImsi2Str ARGS((U8 *imsi, U8 imsiLen, S8 *pImsi));
EXTERN Void aqfdImsi2Binary ARGS((S8 *pImsi, U8 *imsi, U8 *imsiLen));

EXTERN U32 aqfdStr2TBCD ARGS((U8 *src, U8 *dst));
EXTERN U32 aqfdTBCD2Str ARGS((U8 *src, U32 srcLen, U8 *dst));

PUBLIC AqOctetString *aqfdDupOctetString ARGS((union avp_value *v));
PUBLIC void aqfdCloneOctetString ARGS((AqOctetString *org, AqOctetString **tgt));
EXTERN S16 aqfdParseExperimentalResult ARGS((struct avp *avp, AqExperimentalResult *er));


#endif /* __AQFDX__ */
