/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter T6A Convergence Layer

     Type:     C source file

     Desc:     Management interface.

     File:     sw_ptmi.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/*

The following functions are provided in this file:
     SwMiLswCfgCfm      Configuration Confirm
     SwMiLswCntrlCfm    Control Confirm
     SwMiLswStsCfm      Statistics Confirm
     SwMiLswStaInd      Status Indication
     SwMiLswStaCfm      Status Confirm
     SwMiLswTrcInd      Trace Indication

It should be noted that not all of these functions may be required
by a particular layer management service user.

It is assumed that the following functions are provided in TUCL:
     SwMiLswCfgReq      Configuration Request
     SwMiLswCntrlReq    Control Request
     SwMiLswStsReq      Statistics Request
     SwMiLswStaReq      Status Request

*/

/* header include files (.h) */

#include "envopt.h"             /* environment options */
#include "envdep.h"             /* environment dependent */
#include "envind.h"             /* environment independent */

#include "gen.h"                /* general layer */
#include "ssi.h"                /* system services interface */

/* external headers */
#ifdef HI_TLS
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/ssl.h>
#include <openssl/x509v3.h>
#endif

#include "cm_hash.h"            /* common hash list */
#include "cm_llist.h"           /* common linked list */
#include "cm5.h"                /* common timer */
#include "cm_inet.h"            /* common sockets */
#include "cm_tpt.h"             /* common transport defines */

/* header/extern include files (.x) */

#include "gen.x"                /* general layer */
#include "ssi.x"                /* system services interface */

#include "cm_hash.x"            /* common hashing */
#include "cm_llist.x"           /* common linked list */
#include "cm_lib.x"             /* common library */
#include "cm5.x"                /* common timer */
#include "cm_inet.x"            /* common sockets */
#include "cm_tpt.x"             /* common transport typedefs */

#include "aqfd2.x"

#include "lsw.h"
#include "lsw.x"
#include "sw.h"
#include "sw.x"

/* local defines */

#define MAXSWMI 2

#ifndef LCSWMILSW
#define PTHIMILAQ
#else
#ifndef SM
#define PTAQMILAQ
#endif
#endif


#ifdef PTSWMILSW
/* declaration of portable functions */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* PTSWMILSW */


/*
  The following matrices define the mapping between the primitives
  called by the upper interface of TUCL and the corresponding
  primitives of the TCP UDP Convergence Layer service user(s).

  The parameter MAXHIMI defines the maximum number of service
  users on top of TUCL. There is an array of functions per
  primitive invoked by TUCL. Every array is MAXHIMI long (i.e.
  there are as many functions as the number of service users).

  The dispatching is performed by the configurable variable:
  selector. The selector is configured on a per SAP basis.

  The selectors are:

   0 - loosely coupled (#define LCHIMILHI) 1 - LHI (#define SM)

*/



/* Configuration Confirm */

PRIVATE LswCfgCfm SwMiLswCfgCfmMt[MAXSWMI] =
{
#ifdef LCSWMILSW
   cmPkLswCfgCfm,          /* 0 - loosely coupled  */
#else
   PtMiLswCfgCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLswCfgCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLswCfgCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Control Confirm */

PRIVATE LswCntrlCfm SwMiLswCntrlCfmMt[MAXSWMI] =
{
#ifdef LCSWMILSW
   cmPkLswCntrlCfm,          /* 0 - loosely coupled  */
#else
   PtMiLswCntrlCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLswCntrlCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLswCntrlCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Statistics Confirm */

PRIVATE LswStsCfm SwMiLswStsCfmMt[MAXSWMI] =
{
#ifdef LCSWMILSW
   cmPkLswStsCfm,          /* 0 - loosely coupled  */
#else
   PtMiLswStsCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLswStsCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLswStsCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Status Indication */

PRIVATE LswStaInd SwMiLswStaIndMt[MAXSWMI] =
{
#ifdef LCSWMILSW
   cmPkLswStaInd,          /* 0 - loosely coupled  */
#else
   PtMiLswStaInd,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLswStaInd,          /* 1 - tightly coupled, layer management */
#else
   PtMiLswStaInd,          /* 1 - tightly coupled, portable */
#endif
};

/* Status Confirm */

PRIVATE LswStaCfm SwMiLswStaCfmMt[MAXSWMI] =
{
#ifdef LCSWMILSW
   cmPkLswStaCfm,          /* 0 - loosely coupled  */
#else
   PtMiLswStaCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLswStaCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLswStaCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Trace Indication */

PRIVATE LswTrcInd SwMiLswTrcIndMt[MAXSWMI] =
{
#ifdef LCSWMILSW
   cmPkLswTrcInd,          /* 0 - loosely coupled  */
#else
   PtMiLswTrcInd,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLswTrcInd,          /* 1 - tightly coupled, layer management */
#else
   PtMiLswTrcInd,          /* 1 - tightly coupled, portable */
#endif
};


/*
*     Layer Management Interface Functions
*/


/*
*
*       Fun:   Configuration confirm
*
*       Desc:  This function is used to send a configuration confirm
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sw_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SwMiLswCfgCfm
(
Pst     *pst,            /* post structure */
SwMngmt *cfg             /* configuration */
)
#else
PUBLIC S16 SwMiLswCfgCfm(pst, cfg)
Pst     *pst;            /* post structure */
SwMngmt *cfg;            /* configuration */
#endif
{
   TRC3(SwMiLswCfgCfm)

   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf,
         "SwMiLswCfgCfm(pst, cfg (0x%p))\n", (Ptr)cfg));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SwMiLswCfgCfmMt[pst->selector])(pst, cfg));
} /* end of SwMiLswCfgCfm */


/*
*
*       Fun:   Control confirm
*
*       Desc:  This function is used to send a control confirm
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sw_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SwMiLswCntrlCfm
(
Pst     *pst,            /* post structure */
SwMngmt *cntrl           /* control */
)
#else
PUBLIC S16 SwMiLswCntrlCfm(pst, cntrl)
Pst     *pst;            /* post structure */
SwMngmt *cntrl;          /* control */
#endif
{
   TRC3(SwMiLswCntrlCfm)

   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf,
         "SwMiLswCntrlCfm(pst, cntrl (0x%p))\n", (Ptr)cntrl));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SwMiLswCntrlCfmMt[pst->selector])(pst, cntrl));
} /* end of SwMiLswCntrlCfm */


/*
*
*       Fun:   Status Indication
*
*       Desc:  This function is used to indicate the status
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sw_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SwMiLswStaInd
(
Pst     *pst,            /* post structure */
SwMngmt *usta             /* unsolicited status */
)
#else
PUBLIC S16 SwMiLswStaInd(pst, usta)
Pst     *pst;            /* post structure */
SwMngmt *usta;            /* unsolicited status */
#endif
{
   TRC3(SwMiLswStaInd)

   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf,
          "SwMiLswStaInd(pst, usta (0x%p))\n", (Ptr)usta));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SwMiLswStaIndMt[pst->selector])(pst, usta));
} /* end of SwMiLswStaInd */


/*
*
*       Fun:   Status Confirm
*
*       Desc:  This function is used to return the status
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sw_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SwMiLswStaCfm
(
Pst     *pst,            /* post structure */
SwMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 SwMiLswStaCfm(pst, sta)
Pst     *pst;            /* post structure */
SwMngmt *sta;             /* solicited status */
#endif
{
   TRC3(SwMiLswStaCfm)

   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf,
          "SwMiLswStaCfm(pst, sta (0x%p))\n", (Ptr)sta));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SwMiLswStaCfmMt[pst->selector])(pst, sta));
} /* end of SwMiLswStaCfm */


/*
*
*       Fun:   Statistics Confirm
*
*       Desc:  This function is used to return the statistics
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sw_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SwMiLswStsCfm
(
Pst     *pst,                /* post structure */
SwMngmt *sts                 /* statistics */
)
#else
PUBLIC S16 SwMiLswStsCfm(pst, sts)
Pst     *pst;                /* post structure */
SwMngmt *sts;                /* statistics */
#endif
{
   TRC3(SwMiLswStsCfm)

   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf,
         "SwMiLswStsCfm(pst, sts (0x%p))\n", (Ptr)sts));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SwMiLswStsCfmMt[pst->selector])(pst, sts));
} /* end of SwMiLswStsCfm */


/*
*
*       Fun:   Trace Indication
*
*       Desc:  This function is used to indicate the trace
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sw_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SwMiLswTrcInd
(
Pst     *pst,            /* post structure */
SwMngmt *trc,             /* unsolicited status */
Buffer  *mBuf              /* message buffer */
)
#else
PUBLIC S16 SwMiLswTrcInd(pst, trc, mBuf)
Pst     *pst;            /* post structure */
SwMngmt *trc;             /* unsolicited status */
Buffer  *mBuf;             /* message buffer */
#endif
{
   TRC3(SwMiLswTrcInd)

   SWDBGP(DBGMASK_MI, (swCb.init.prntBuf,
         "SwMiLswTrcInd(pst, trc (0x%p))\n", (Ptr)trc));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SwMiLswTrcIndMt[pst->selector])(pst, trc, mBuf));
} /* end of SwMiLswTrcInd */


/********************************************************************30**

         End of file:     sw_ptmi.c@@/main/6 - Mon Mar  3 20:09:54 2008

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision History:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

