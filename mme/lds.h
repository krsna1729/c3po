/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter

     Type:     C include file

     Desc:     Diameter

     File:     lsy.h

     Sid:

     Prg:

*********************************************************************21*/

#ifndef __LDSH__
#define __LDSH__

#ifdef LDSV1            /* LDS interface version 1 */
#ifdef LDSIFVER
#undef LDSIFVER
#endif
#define LDSIFVER        0x0100
#endif

/***********************************************************************
         defines for layer-specific elements
 ***********************************************************************/

#define LDS_PATHLEN      255

/***********************************************************************
         defines for "reason" in LsyStaInd
 ***********************************************************************/
/*
#define  LDS_REASON_OPINPROG                 (LCM_REASON_LYR_SPECIFIC + 1)
*/

/***********************************************************************
         defines for "event" in LsyStaInd
 ***********************************************************************/
/*
#define  LSY_EVENT_???             (LCM_EVENT_LYR_SPECIFIC + 1)
*/

/***********************************************************************
         defines for "cause" in LsyStaInd
 ***********************************************************************/
/* diameter  related errors */

/*
#define  LSY_CAUSE_LOCK_ERR             (LCM_CAUSE_LYR_SPECIFIC + 1)
#define  LSY_CAUSE_UNLOCK_ERR           (LCM_CAUSE_LYR_SPECIFIC + 2)
*/

/***********************************************************************
         defines related to events across the management interface
 ***********************************************************************/

#define  EVTLDSCFGREQ                   1
#define  EVTLDSSTSREQ                   2
#define  EVTLDSCNTRLREQ                 3
#define  EVTLDSSTAREQ                   4
#define  EVTLDSCFGCFM                   5
#define  EVTLDSSTSCFM                   6
#define  EVTLDSCNTRLCFM                 7
#define  EVTLDSSTACFM                   8
#define  EVTLDSSTAIND                   9
#define  EVTLDSTRCIND                   10
#define  EVTLDSINITATTCHSGW             11

/********************************************************************SZ**
 DS States
*********************************************************************SZ*/

#define LDS_UNINITIALIZED         1     /* uninitizlied */
#define LDS_INITIALIZED           1     /* initizlied */
#define LDS_CFG_PARSED            1     /* config file parsed */
#define LDS_STARTED               1     /* started */
#define LDS_WAITING_FOR_SHUTDOWN  1     /* shutdown issued */
#define LDS_SHUTDOWN              1     /* shutdown */


/***********************************************************************
         defines related to events in LsyTrcInd primitive
 ***********************************************************************/
/*
#define  LSY_RAW_RXED                   5
*/

/* alarmInfo.type */
#define  LDS_ALARMINFO_TYPE_NTPRSNT     0       /* alarmInfo is not present */

/* parType values in LhiStaInd */
#if 0
#define  LDS_INV_MBUF                   1       /* invalid message buffer */
#endif

/* selector values for lmPst.selector */
#define  LDS_LC                         0       /* loosely coupled LM */
#define  LDS_TC                         1       /* tightly coupled LM*/

#define LDS_DBGMASK_INFO    (DBGMASK_LYR << 3)

/* Error macro for TUCL management interface */
/* lhi_h_001.main_11: Fix for compilation warning */
#define LDSLOGERROR(pst, errCode, errVal, errDesc)            \
        SLogError(pst->srcEnt, pst->srcInst, pst->srcProcId,  \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,   \
                  (ErrCode)errCode, (ErrVal)errVal,           \
                  errDesc)

#define LDS_ZERO(_str,_len)                                   \
   cmMemset((U8 *)_str, 0, _len);

/* Error codes */
#define   ELDSBASE     0             /* reserved */
#define   ERRLDS       (ELDSBASE)

#define   ELDS001      (ERRLDS +    1)
#define   ELDS002      (ERRLDS +    2)
#define   ELDS003      (ERRLDS +    3)
#define   ELDS004      (ERRLDS +    4)
#define   ELDS005      (ERRLDS +    5)
#define   ELDS006      (ERRLDS +    6)
#define   ELDS007      (ERRLDS +    7)
#define   ELDS008      (ERRLDS +    8)
#define   ELDS009      (ERRLDS +    9)
#define   ELDS010      (ERRLDS +   10)
#define   ELDS011      (ERRLDS +   11)
#define   ELDS012      (ERRLDS +   12)
#define   ELDS013      (ERRLDS +   13)
#define   ELDS014      (ERRLDS +   14)
#define   ELDS015      (ERRLDS +   15)
#define   ELDS016      (ERRLDS +   16)
#define   ELDS017      (ERRLDS +   17)
#define   ELDS018      (ERRLDS +   18)
#define   ELDS019      (ERRLDS +   19)
#define   ELDS020      (ERRLDS +   20)
#define   ELDS021      (ERRLDS +   21)
#define   ELDS022      (ERRLDS +   22)
#define   ELDS023      (ERRLDS +   23)
#define   ELDS024      (ERRLDS +   24)
#define   ELDS025      (ERRLDS +   25)
#define   ELDS026      (ERRLDS +   26)
#define   ELDS027      (ERRLDS +   27)
#define   ELDS028      (ERRLDS +   28)
#define   ELDS029      (ERRLDS +   29)
#define   ELDS030      (ERRLDS +   30)
#define   ELDS031      (ERRLDS +   31)
#define   ELDS032      (ERRLDS +   32)
#define   ELDS033      (ERRLDS +   33)
#define   ELDS034      (ERRLDS +   34)
#define   ELDS035      (ERRLDS +   35)
#define   ELDS036      (ERRLDS +   36)
#define   ELDS037      (ERRLDS +   37)
#define   ELDS038      (ERRLDS +   38)
#define   ELDS039      (ERRLDS +   39)
#define   ELDS040      (ERRLDS +   40)
#define   ELDS041      (ERRLDS +   41)
#define   ELDS042      (ERRLDS +   42)
#define   ELDS043      (ERRLDS +   43)
#define   ELDS044      (ERRLDS +   44)
#define   ELDS045      (ERRLDS +   45)
#define   ELDS046      (ERRLDS +   46)
#define   ELDS047      (ERRLDS +   47)
#define   ELDS048      (ERRLDS +   48)
#define   ELDS049      (ERRLDS +   49)
#define   ELDS050      (ERRLDS +   50)
#define   ELDS051      (ERRLDS +   51)
#define   ELDS052      (ERRLDS +   52)
#define   ELDS053      (ERRLDS +   53)
#define   ELDS054      (ERRLDS +   54)
#define   ELDS055      (ERRLDS +   55)
#define   ELDS056      (ERRLDS +   56)
#define   ELDS057      (ERRLDS +   57)
#define   ELDS058      (ERRLDS +   58)
#define   ELDS059      (ERRLDS +   59)
#define   ELDS060      (ERRLDS +   60)
#define   ELDS061      (ERRLDS +   61)
#define   ELDS062      (ERRLDS +   62)
#define   ELDS063      (ERRLDS +   63)
#define   ELDS064      (ERRLDS +   64)
#define   ELDS065      (ERRLDS +   65)
#define   ELDS066      (ERRLDS +   66)
#define   ELDS067      (ERRLDS +   67)
#define   ELDS068      (ERRLDS +   68)
#define   ELDS069      (ERRLDS +   69)
#define   ELDS070      (ERRLDS +   70)
#define   ELDS071      (ERRLDS +   71)
#define   ELDS072      (ERRLDS +   72)
#define   ELDS073      (ERRLDS +   73)
#define   ELDS074      (ERRLDS +   74)
#define   ELDS075      (ERRLDS +   75)
#define   ELDS076      (ERRLDS +   76)
#define   ELDS077      (ERRLDS +   77)
#define   ELDS078      (ERRLDS +   78)
#define   ELDS079      (ERRLDS +   79)
#define   ELDS080      (ERRLDS +   80)
#define   ELDS081      (ERRLDS +   81)
#define   ELDS082      (ERRLDS +   82)
#define   ELDS083      (ERRLDS +   83)
#define   ELDS084      (ERRLDS +   84)
#define   ELDS085      (ERRLDS +   85)
#define   ELDS086      (ERRLDS +   86)
#define   ELDS087      (ERRLDS +   87)
#define   ELDS088      (ERRLDS +   88)
#define   ELDS089      (ERRLDS +   89)
#define   ELDS090      (ERRLDS +   90)
#define   ELDS091      (ERRLDS +   91)
#define   ELDS092      (ERRLDS +   92)

#endif /* __LDSH__ */
