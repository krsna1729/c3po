/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

/********************************************************************20**

     Name:     Diameter SGD Interface

     Type:     C include file

     Desc:     Diameter

     File:     lsx.h

     Sid:

     Prg:      rakesh kumar suman

*********************************************************************21*/

#ifndef __LSXH__
#define __LSXH__

#ifdef LSXV1            /* T6A interface version 14.1 */
#ifdef LSXIFVER
#undef LSXIFVER
#endif
#define LSXIFVER        0x0e10
#endif

/***********************************************************************
         defines for layer-specific elements
 ***********************************************************************/

#if 0
#define LDS_PATHLEN      255
#endif

/***********************************************************************
         defines for "reason" in LsyStaInd
 ***********************************************************************/
#define  LSX_REASON_OPINPROG                 (LCM_REASON_LYR_SPECIFIC + 1)

/***********************************************************************
         defines for "event" in LsyStaInd
 ***********************************************************************/
/*
#define  LSY_EVENT_???             (LCM_EVENT_LYR_SPECIFIC + 1)
*/

/***********************************************************************
         defines for "cause" in LsyStaInd
 ***********************************************************************/
/* diameter  related errors */

/*
#define  LSY_CAUSE_LOCK_ERR             (LCM_CAUSE_LYR_SPECIFIC + 1)
#define  LSY_CAUSE_UNLOCK_ERR           (LCM_CAUSE_LYR_SPECIFIC + 2)
*/

/***********************************************************************
         defines related to events across the management interface
 ***********************************************************************/

#define  EVTLSXCFGREQ                   1
#define  EVTLSXSTSREQ                   2
#define  EVTLSXCNTRLREQ                 3
#define  EVTLSXSTAREQ                   4
#define  EVTLSXCFGCFM                   5
#define  EVTLSXSTSCFM                   6
#define  EVTLSXCNTRLCFM                 7
#define  EVTLSXSTACFM                   8
#define  EVTLSXSTAIND                   9
#define  EVTLSXTRCIND                   10
#define  EVTLSXINITATTCHSGW             11

#define  EVTLSXOFA                      12
#define  EVTLSXTFR                      13

/********************************************************************SZ**
 SX States
*********************************************************************SZ*/

#define LSX_UNINITIALIZED         1     /* uninitizlied */
#define LSX_INITIALIZED           1     /* initizlied */
#define LSX_CFG_PARSED            1     /* config file parsed */
#define LSX_STARTED               1     /* started */
#define LSX_WAITING_FOR_SHUTDOWN  1     /* shutdown issued */
#define LSX_SHUTDOWN              1     /* shutdown */


/***********************************************************************
         defines related to events in LsyTrcInd primitive
 ***********************************************************************/
/*
#define  LSY_RAW_RXED                   5
*/

/* alarmInfo.type */
#define  LSX_ALARMINFO_TYPE_NTPRSNT     0       /* alarmInfo is not present */

/* parType values in LhiStaInd */
#if 0
#define  LDS_INV_MBUF                   1       /* invalid message buffer */
#endif

/* selector values for lmPst.selector */
#define  LSX_LC                         0       /* loosely coupled LM */
#define  LSX_TC                         1       /* tightly coupled LM*/

#define LSX_DBGMASK_INFO    (DBGMASK_LYR << 3)

/* Error macro for TUCL management interface */
/* lhi_h_001.main_11: Fix for compilation warning */
#define LSXLOGERROR(pst, errCode, errVal, errDesc)            \
        SLogError(pst->srcEnt, pst->srcInst, pst->srcProcId,  \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,   \
                  (ErrCode)errCode, (ErrVal)errVal,           \
                  errDesc)

#define LSX_ZERO(_str,_len)                                   \
   cmMemset((U8 *)_str, 0, _len);

/* Error codes */
#define   ELSXBASE     0             /* reserved */
#define   ERRLSX       (ELSXBASE)

#define   ELSX001      (ERRLSX +    1)
#define   ELSX002      (ERRLSX +    2)
#define   ELSX003      (ERRLSX +    3)
#define   ELSX004      (ERRLSX +    4)
#define   ELSX005      (ERRLSX +    5)
#define   ELSX006      (ERRLSX +    6)
#define   ELSX007      (ERRLSX +    7)
#define   ELSX008      (ERRLSX +    8)
#define   ELSX009      (ERRLSX +    9)
#define   ELSX010      (ERRLSX +   10)
#define   ELSX011      (ERRLSX +   11)
#define   ELSX012      (ERRLSX +   12)
#define   ELSX013      (ERRLSX +   13)
#define   ELSX014      (ERRLSX +   14)
#define   ELSX015      (ERRLSX +   15)
#define   ELSX016      (ERRLSX +   16)
#define   ELSX017      (ERRLSX +   17)
#define   ELSX018      (ERRLSX +   18)
#define   ELSX019      (ERRLSX +   19)
#define   ELSX020      (ERRLSX +   20)
#define   ELSX021      (ERRLSX +   21)
#define   ELSX022      (ERRLSX +   22)
#define   ELSX023      (ERRLSX +   23)
#define   ELSX024      (ERRLSX +   24)
#define   ELSX025      (ERRLSX +   25)
#define   ELSX026      (ERRLSX +   26)
#define   ELSX027      (ERRLSX +   27)
#define   ELSX028      (ERRLSX +   28)
#define   ELSX029      (ERRLSX +   29)
#define   ELSX030      (ERRLSX +   30)
#define   ELSX031      (ERRLSX +   31)
#define   ELSX032      (ERRLSX +   32)
#define   ELSX033      (ERRLSX +   33)
#define   ELSX034      (ERRLSX +   34)
#define   ELSX035      (ERRLSX +   35)
#define   ELSX036      (ERRLSX +   36)
#define   ELSX037      (ERRLSX +   37)
#define   ELSX038      (ERRLSX +   38)
#define   ELSX039      (ERRLSX +   39)
#define   ELSX040      (ERRLSX +   40)
#define   ELSX041      (ERRLSX +   41)
#define   ELSX042      (ERRLSX +   42)
#define   ELSX043      (ERRLSX +   43)
#define   ELSX044      (ERRLSX +   44)
#define   ELSX045      (ERRLSX +   45)
#define   ELSX046      (ERRLSX +   46)
#define   ELSX047      (ERRLSX +   47)
#define   ELSX048      (ERRLSX +   48)
#define   ELSX049      (ERRLSX +   49)
#define   ELSX050      (ERRLSX +   50)
#define   ELSX051      (ERRLSX +   51)
#define   ELSX052      (ERRLSX +   52)
#define   ELSX053      (ERRLSX +   53)
#define   ELSX054      (ERRLSX +   54)
#define   ELSX055      (ERRLSX +   55)
#define   ELSX056      (ERRLSX +   56)
#define   ELSX057      (ERRLSX +   57)
#define   ELSX058      (ERRLSX +   58)
#define   ELSX059      (ERRLSX +   59)
#define   ELSX060      (ERRLSX +   60)
#define   ELSX061      (ERRLSX +   61)
#define   ELSX062      (ERRLSX +   62)
#define   ELSX063      (ERRLSX +   63)
#define   ELSX064      (ERRLSX +   64)
#define   ELSX065      (ERRLSX +   65)
#define   ELSX066      (ERRLSX +   66)
#define   ELSX067      (ERRLSX +   67)
#define   ELSX068      (ERRLSX +   68)
#define   ELSX069      (ERRLSX +   69)
#define   ELSX070      (ERRLSX +   70)
#define   ELSX071      (ERRLSX +   71)
#define   ELSX072      (ERRLSX +   72)
#define   ELSX073      (ERRLSX +   73)
#define   ELSX074      (ERRLSX +   74)
#define   ELSX075      (ERRLSX +   75)
#define   ELSX076      (ERRLSX +   76)
#define   ELSX077      (ERRLSX +   77)
#define   ELSX078      (ERRLSX +   78)
#define   ELSX079      (ERRLSX +   79)
#define   ELSX080      (ERRLSX +   80)
#define   ELSX081      (ERRLSX +   81)
#define   ELSX082      (ERRLSX +   82)
#define   ELSX083      (ERRLSX +   83)
#define   ELSX084      (ERRLSX +   84)
#define   ELSX085      (ERRLSX +   85)
#define   ELSX086      (ERRLSX +   86)
#define   ELSX087      (ERRLSX +   87)
#define   ELSX088      (ERRLSX +   88)
#define   ELSX089      (ERRLSX +   89)
#define   ELSX090      (ERRLSX +   90)
#define   ELSX091      (ERRLSX +   91)
#define   ELSX092      (ERRLSX +   92)

#endif /* __LSXH__ */
