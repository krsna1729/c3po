/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter Convergence Layer

     Type:     C source file

     Desc:     freeDiameter initialization.

     File:     aqfd_utl.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"
#include "aq_err.h"
#include "lsy.h"
#include "lsy.x"

#include "aqfd.h"
#include "aqfd.x"

#include "sy.h"
#include "sy.x"
#include "sy_err.h"

/*
*
*       Fun:    aqfdCreateSessionId
*
*       Desc:   Create the session id and save it.
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*
*       Notes:  None
*
*       File:   aqfd_utl.c
*
*/
#ifdef ANSI
PUBLIC S16 aqfdCreateSessionId
(
VbMmeUeCb *ueCb
)
#else
PUBLIC S16 aqfdCreateSessionId(ueCb)
VbMmeUeCb *ueCb;
#endif
{
   struct session *sess = NULL;
   U8 *sid;
   size_t sidlen;

   /* clear the Session-Id if it exists */
   if (ueCb->ueCtxt.ueHssCtxt.sessIdLen > 0)
   {
      int isnew = 0;

      /* must be an existing session id, so try to free it */
      AQCHECK_FCT(fd_sess_fromsid(ueCb->ueCtxt.ueHssCtxt.sessId, ueCb->ueCtxt.ueHssCtxt.sessIdLen, &sess, &isnew), LAQ_REASON_FD_SESS_FROMSID_FAIL);

      if (isnew == 0) /* need to destroy the session */
      {
         AQCHECK_FCT(fd_sess_destroy(&sess), LAQ_REASON_FD_SESS_DESTROY_FAIL);
         sess = NULL;
      }
   }

   /* create session if not previously created by fd_sess_fromsid() */
   if (sess == NULL)
      AQCHECK_FCT(fd_sess_new(&sess, fd_g_config->cnf_diamid, fd_g_config->cnf_diamid_len, NULL, 0), LAQ_REASON_FD_SESS_NEW_FAIL);

   AQCHECK_FCT(fd_sess_getsid(sess, &sid, &sidlen), LAQ_REASON_FD_SESS_GETSID_FAIL);

   ueCb->ueCtxt.ueHssCtxt.sessIdLen = (U8)sidlen;
   cmMemcpy(ueCb->ueCtxt.ueHssCtxt.sessId, sid, ueCb->ueCtxt.ueHssCtxt.sessIdLen);
   ueCb->ueCtxt.ueHssCtxt.sessId[ueCb->ueCtxt.ueHssCtxt.sessIdLen] = '\0';

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdCreateSessionId2
*
*       Desc:   Create the session id and save it.
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*
*       Notes:  None
*
*       File:   aqfd_utl.c
*
*/
#ifdef ANSI
PUBLIC S16 aqfdCreateSessionId2
(
U8 *sessId,
U8 *sessIdLen
)
#else
PUBLIC S16 aqfdCreateSessionId2(sessId, sessIdLen)
U8 *sessId;
U8 *sessIdLen;
#endif
{
   struct session *sess = NULL;
   U8 *sid;
   size_t sidlen;

   /* create session if not previously created by fd_sess_fromsid() */
   AQCHECK_FCT(fd_sess_new(&sess, fd_g_config->cnf_diamid, fd_g_config->cnf_diamid_len, NULL, 0), LAQ_REASON_FD_SESS_NEW_FAIL);

   AQCHECK_FCT(fd_sess_getsid(sess, &sid, &sidlen), LAQ_REASON_FD_SESS_GETSID_FAIL);

   /* save the newly created session id */
   *sessIdLen = (U8)sidlen;
   cmMemcpy(sessId, sid, *sessIdLen);
   sessId[*sessIdLen] = '\0';

   /* destroy the freeDiameter session object (now that we have the string) */
   AQCHECK_FCT(fd_sess_destroy(&sess), RFAILED);

   return ROK;
}

/*
*
*       Fun:    aqfdImsi2Str
*
*       Desc:   
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*
*       Notes:  None
*
*       File:   aqfd_utl.c
*
*/
#ifdef ANSI
PUBLIC Void aqfdImsi2Str
(
U8 *imsi,
U8 imsiLen,
S8 *pImsi
)
#else
PUBLIC Void aqfdImsi2Str(imsi, imsiLen, pImsi)
U8 *imsi;
U8 imsiLen;
S8 *pImsi;
#endif
{
   int i;
   for (i = 0; i < imsiLen; i++)
      pImsi[i] = '0' + imsi[i];
   pImsi[i] = '\0';
}

/*
*
*       Fun:    aqfdImsi2Binary
*
*       Desc:   
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*
*       Notes:  None
*
*       File:   aqfd_utl.c
*
*/
#ifdef ANSI
PUBLIC Void aqfdImsi2Binary
(
S8 *pImsi,
U8 *imsi,
U8 *imsiLen
)
#else
PUBLIC Void aqfdImsi2Binary(pImsi, imsi, imsiLen)
S8 *pImsi;
U8 *imsi;
U8 *imsiLen;
#endif
{
   for (*imsiLen = 0; pImsi[*imsiLen]; (*imsiLen)++)
      imsi[*imsiLen] = pImsi[*imsiLen] - '0';
}

/*
*
*       Fun:    aqfdStr2TBCD
*
*       Desc:   
*
*       Ret:    Length of resulting TBCD string
*
*       Notes:  None
*
*       File:   aqfd_utl.c
*
*       TBCD-STRING ::= OCTET STRING
*           -- This type (Telephony Binary Coded Decimal String) is used to
*           -- represent several digits from 0 through 9, *, #, a, b, c, two
*           -- digits per octet, each digit encoded 0000 to 1001 (0 to 9),
*           -- 1010 (*), 1011 (#), 1100 (a), 1101 (b) or 1110 (c); 1111 used
*           -- as filler when there is an odd number of digits.
*
*/

#ifdef ANSI
PUBLIC U32 aqfdStr2TBCD
(
U8 *src,
U8 *dst
)
#else
PUBLIC U32 aqfdStr2TBCD(src, dst)
U8 *src,
U8 *dst
#endif
{
   U32 dstLen;

   for (dstLen = 0; src[dstLen << 1]; dstLen++)
   {
      dst[dstLen] =
         (AQFD_CHAR2TBCD(src[dstLen << 1]) << 4) |
         AQFD_CHAR2TBCD(src[(dstLen << 1) + 1]);
      if (!src[(dstLen << 1) + 1])
         break;
   }

   return dstLen;
}

/*
*
*       Fun:    aqfdTBCD2Str
*
*       Desc:   
*
*       Ret:    Length of resulting string
*
*       Notes:  None
*
*       File:   aqfd_utl.c
*
*       TBCD-STRING ::= OCTET STRING
*           -- This type (Telephony Binary Coded Decimal String) is used to
*           -- represent several digits from 0 through 9, *, #, a, b, c, two
*           -- digits per octet, each digit encoded 0000 to 1001 (0 to 9),
*           -- 1010 (*), 1011 (#), 1100 (a), 1101 (b) or 1110 (c); 1111 used
*           -- as filler when there is an odd number of digits.
*
*/

#ifdef ANSI
PUBLIC U32 aqfdTBCD2Str
(
U8 *src,
U32 srcLen,
U8 *dst
)
#else
PUBLIC U32 aqfdTBCD2Str(src, srcLen, dst)
U8 *src,
U32 srcLen,
U8 *dst
#endif
{
   static const U8 *__TBCD__ = (U8*)"0123456789*#abc";
   U32 srcIdx;
   U32 dstLen = 0;

   for (srcIdx = 0; srcIdx < srcLen; srcIdx++)
   {
      dst[dstLen] = __TBCD__[AQFD_HIGH_NIBBLE(src[srcIdx])];
      if (dst[dstLen])
      {
         dst[++dstLen] = __TBCD__[AQFD_LOW_NIBBLE(src[srcIdx])];
         if (dst[dstLen])
            dstLen++;
      }
   }

   dst[dstLen] = 0;

   return dstLen;
}

/*
*
*       Fun:    aqfdParseExperimentalResult
*
*       Desc:   Create the session id and save it.
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*
*       Notes:  None
*
*       File:   aqfd_utl.c
*
*/
#ifdef ANSI
PUBLIC S16 aqfdParseExperimentalResult
(
struct avp *avp,
AqExperimentalResult *er
)
#else
PUBLIC S16 aqfdParseExperimentalResult(avp, er)
struct avp *avp;
AqExperimentalResult *er;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   AQCHECK_FCT(fd_msg_avp_hdr(avp, &hdr), LAQ_REASON_FD_MSG_AVP_HDR);
   if (hdr->avp_code != aqDict.davp_Experimental_Result.avp_code)
      return LAQ_REASON_FD_EXPERIMENTAL_RESULT;

   AQCHECK_FCT(fd_msg_browse(avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if (hdr->avp_code == aqDict.davp_Experimental_Result_Code.avp_code)
      {
         er->result_code = hdr->avp_value->u32;
         er->present = TRUE;
      }
      else if (hdr->avp_code == aqDict.davp_Vendor_Id.avp_code)
         er->vendor_id = hdr->avp_value->u32;

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdDupOctetString
*
*       Desc:
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_utl.c
*
*/
#ifdef ANSI
PUBLIC AqOctetString *aqfdDupOctetString 
(
union avp_value *v
)
#else
PUBLIC AqOctetString *aqfdDupOctetString(v)
union avp_value *v;
#endif
{
   AqOctetString *p = (AqOctetString*)malloc( sizeof(AqOctetString) + v->os.len  + 1 );

   p->len = v->os.len;
   memcpy( p->val, v->os.data, p->len );
   p->val[ p->len] = '\0';

   return p;
}

#ifdef ANSI
PUBLIC void aqfdCloneOctetString
(
AqOctetString *org,
AqOctetString **tgt
)
#else
PUBLIC void aqfdCloneOctetString(org, tgt)
AqOctetString *org;
AqOctetString **tgt;
#endif
{
   if(*tgt){
      free(*tgt);
      *tgt = NULL;
   }
   if(org == NULLP){
      return;
   }
   *tgt = (AqOctetString*)malloc( sizeof(AqOctetString) + org->len + 1);
   (*tgt)->len = org->len;
   memcpy( (*tgt)->val, org->val, (*tgt)->len );
   (*tgt)->val[ (*tgt)->len] = '\0';
}
