/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

/********************************************************************20**

     Name:     Diameter SGD Interface Convergence Layer error file.

     Type:     C include file

     Desc:     Error Hash Defines required by SX layer

     File:     sx_err.h

     Sid:

     Prg:      rakesh kumar suman

*********************************************************************21*/

#ifndef __SXERRH__
#define __SXERRH__



#if (ERRCLASS & ERRCLS_DEBUG)
#define SXLOGERROR_DEBUGPST(procId, ent, errCode, errVal, inst, errDesc)        \
        SLogError(ent, inst, procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,     \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define SXLOGERROR_DEBUGPST(procId, ent, errCode, errVal, inst, errDesc)
#endif /* ERRCLS_DEBUG */

/* hi031.201: Fix for g++ compilation warning*/
#if (ERRCLASS & ERRCLS_DEBUG)
#define SXLOGERROR_DEBUG(errCode, errVal, inst, errDesc)        \
        SLogError(sxCb.init.ent, inst, sxCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define SXLOGERROR_DEBUG(errCode, errVal, inst, errDesc)
#endif

#if (ERRCLASS & ERRCLS_INT_PAR)
#define SXLOGERROR_INT_PAR(errCode, errVal, inst, errDesc)      \
        SLogError(sxCb.init.ent, inst, sxCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_INT_PAR,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define SXLOGERROR_INT_PAR(errCode, errVal, inst, errDesc)
#endif /* ERRCLS_INT_PAR */

#if (ERRCLASS & ERRCLS_ADD_RES)
#define SXLOGERROR_ADD_RES(errCode, errVal, inst, errDesc)      \
        SLogError(sxCb.init.ent, inst, sxCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_ADD_RES,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define SXLOGERROR_ADD_RES(errCode, errVal, errDesc, inst)
#endif /* ERRCLS_ADD_RES */

/* Error codes */
#define   ESXBASE     0             /* reserved */
#define   ERRSX       (ESXBASE)

#define   ESX001      (ERRSX +    1)
#define   ESX002      (ERRSX +    2)
#define   ESX003      (ERRSX +    3)
#define   ESX004      (ERRSX +    4)
#define   ESX005      (ERRSX +    5)
#define   ESX006      (ERRSX +    6)
#define   ESX007      (ERRSX +    7)
#define   ESX008      (ERRSX +    8)
#define   ESX009      (ERRSX +    9)
#define   ESX010      (ERRSX +   10)
#define   ESX011      (ERRSX +   11)
#define   ESX012      (ERRSX +   12)

#endif /* __SXERRH__ */



/********************************************************************30**

         End of file:

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      rks   1. initial release.
*********************************************************************91*/

