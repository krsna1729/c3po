/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

/********************************************************************20**

     Name:     Stack Manager - portable managment SGD Diameter interface 

     Type:     C source file

     Desc:     Code for SGD Convergence Layer  layer management
               user primitive used in loosely coupled
               systems

     File:     smsxptmi.c

     Sid:

     Prg:      rakesh kumar suman

*********************************************************************21*/

/*

Layer management provides the necessary functions to control and
monitor the condition of each protocol layer.

The following functions are provided in this file:

     SmMiLsxCfgReq      Configure Request
     SmMiLsxStaReq      Status Request
     SmMiLsxStsReq      Statistics Request
     SmMiLsxCntrlReq    Control Request

It is assumed that the following functions are provided in the
stack management body files:

     SmMiLsxStaInd      Status Indication
     SmMiLsxStaCfm      Status Confirm
     SmMiLsxStsCfm      Statistics Confirm
     SmMiLsxTrcInd      Trace Indication

*/


/* header include files (.h) */

#include "envopt.h"        /* environment options */
#include "envdep.h"        /* environment dependent */
#include "envind.h"        /* environment independent */
#include "gen.h"           /* general layer */
#include "cm5.h"           /* common timer */
#include "ssi.h"           /* system services */
#include "cm_inet.h"       /* common sockets */
#include "lsx.h"           /* SX layer management */
#include "cm_err.h"        /* common error */
#include "smsx_err.h"      /* SX error */

/* header/extern include files (.x) */

#include "gen.x"           /* general layer */
#include "ssi.x"           /* system services */
#include "cm5.x"           /* common timer */
#include "cm_inet.x"       /* common sockets */
#include "lsx.x"           /* sx layer management */

#define  MAXSXMI  2

#ifndef  LCSMSXMILSX
#define  PTSMSXMILSX
#else
#ifndef   SX
#define  PTSMSXMILSX
#endif
#endif

#ifdef PTSMSXMILSX
PRIVATE S16 PtMiLsxCfgReq    ARGS((Pst *pst, SxMngmt *cfg));
PRIVATE S16 PtMiLsxStsReq    ARGS((Pst *pst, Action action, SxMngmt *sts));
PRIVATE S16 PtMiLsxCntrlReq  ARGS((Pst *pst, SxMngmt *cntrl));
PRIVATE S16 PtMiLsxStaReq    ARGS((Pst *pst, SxMngmt *sta));
#endif


/*
the following matrices define the mapping between the primitives
called by the layer management interface of TCP UDP Convergence Layer
and the corresponding primitives in TUCL

The parameter MAXSXMI defines the maximum number of layer manager
entities on top of TUCL . There is an array of functions per primitive
invoked by TCP UDP Conbvergence Layer. Every array is MAXSXMI long
(i.e. there are as many functions as the number of service users).

The dispatching is performed by the configurable variable: selector.
The selector is configured during general configuration.

The selectors are:

   0 - loosely coupled (#define LCSMSXMILSX) 2 - Lsx (#define SX)

*/


/* Configuration request primitive */

PRIVATE LsxCfgReq SmMiLsxCfgReqMt[MAXSXMI] =
{
#ifdef LCSMSXMILSX
   cmPkLsxCfgReq,          /* 0 - loosely coupled  */
#else
   PtMiLsxCfgReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef SX
   SxMiLsxCfgReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsxCfgReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Control request primitive */

PRIVATE LsxCntrlReq SmMiLsxCntrlReqMt[MAXSXMI] =
{
#ifdef LCSMSXMILSX
   cmPkLsxCntrlReq,          /* 0 - loosely coupled  */
#else
   PtMiLsxCntrlReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef SX
   SxMiLsxCntrlReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsxCntrlReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Statistics request primitive */

PRIVATE LsxStsReq SmMiLsxStsReqMt[MAXSXMI] =
{
#ifdef LCSMSXMILSX
   cmPkLsxStsReq,          /* 0 - loosely coupled  */
#else
   PtMiLsxStsReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef SX
   SxMiLsxStsReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsxStsReq,          /* 1 - tightly coupled, portable */
#endif
};


/* Status request primitive */

PRIVATE LsxStaReq SmMiLsxStaReqMt[MAXSXMI] =
{
#ifdef LCSMSXMILSX
   cmPkLsxStaReq,          /* 0 - loosely coupled  */
#else
   PtMiLsxStaReq,          /* 0 - tightly coupled, portable */
#endif
#ifdef SX
   SxMiLsxStaReq,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsxStaReq,          /* 1 - tightly coupled, portable */
#endif
};


/*
*     layer management interface functions
*/

/*
*
*       Fun:   Configuration request
*
*       Desc:  This function is used to configure  TUCL
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsxptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLsxCfgReq
(
Pst *spst,                /* post structure */
SxMngmt *cfg              /* configure */
)
#else
PUBLIC S16 SmMiLsxCfgReq(spst, cfg)
Pst *spst;                /* post structure */
SxMngmt *cfg;             /* configure */
#endif
{
   TRC3(SmMiLsxCfgReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLsxCfgReqMt[spst->selector])(spst, cfg);
   RETVALUE(ROK);
} /* end of SmMiLsxCfgReq */



/*
*
*       Fun:   Statistics request
*
*       Desc:  This function is used to request statistics from
*              TUCL
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsxptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLsxStsReq
(
Pst *spst,                /* post structure */
Action action,
SxMngmt *sts              /* statistics */
)
#else
PUBLIC S16 SmMiLsxStsReq(spst, action, sts)
Pst *spst;                /* post structure */
Action action;
SxMngmt *sts;             /* statistics */
#endif
{
   TRC3(SmMiLsxStsReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLsxStsReqMt[spst->selector])(spst, action, sts);
   RETVALUE(ROK);
} /* end of SmMiLsxStsReq */


/*
*
*       Fun:   Control request
*
*       Desc:  This function is used to send control request to
*              TUCL
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsxptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLsxCntrlReq
(
Pst *spst,                 /* post structure */
SxMngmt *cntrl            /* control */
)
#else
PUBLIC S16 SmMiLsxCntrlReq(spst, cntrl)
Pst *spst;                 /* post structure */
SxMngmt *cntrl;           /* control */
#endif
{
   TRC3(SmMiLsxCntrlReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLsxCntrlReqMt[spst->selector])(spst, cntrl);
   RETVALUE(ROK);
} /* end of SmMiLsxCntrlReq */


/*
*
*       Fun:   Status request
*
*       Desc:  This function is used to send a status request to
*              TUCL
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsxptmi.c
*
*/

#ifdef ANSI
PUBLIC S16 SmMiLsxStaReq
(
Pst *spst,                /* post structure */
SxMngmt *sta              /* status */
)
#else
PUBLIC S16 SmMiLsxStaReq(spst, sta)
Pst *spst;                /* post structure */
SxMngmt *sta;             /* status */
#endif
{
   TRC3(SmMiLsxStaReq)
   /* jump to specific primitive depending on configured selector */
   (*SmMiLsxStaReqMt[spst->selector])(spst, sta);
   RETVALUE(ROK);
} /* end of SmMiLsxStaReq */

#ifdef PTSMSXMILSX

/*
 *             Portable Functions
 */

/*
*
*       Fun:   Portable configure Request- TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsxptmi.c
*
*/

#ifdef ANSI
PRIVATE S16 PtMiLsxCfgReq
(
Pst *spst,                  /* post structure */
SxMngmt *cfg                /* configure */
)
#else
PRIVATE S16 PtMiLsxCfgReq(spst, cfg)
Pst *spst;                  /* post structure */
SxMngmt *cfg;               /* configure */
#endif
{
  TRC3(PtMiLsxCfgReq)

  UNUSED(spst);
  UNUSED(cfg);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMSXLOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
              ERRCLS_DEBUG, ESMSX002, 0, "PtMiLsxCfgReq () Failed");
#endif

  RETVALUE(ROK);
} /* end of PtMiLsxCfgReq */



/*
*
*       Fun:   Portable statistics Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsxptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLsxStsReq
(
Pst *spst,                  /* post structure */
Action action,
SxMngmt *sts                /* statistics */
)
#else
PRIVATE S16 PtMiLsxStsReq(spst, action, sts)
Pst *spst;                  /* post structure */
Action action;
SxMngmt *sts;               /* statistics */
#endif
{
  TRC3(PtMiLsxStsReq)

  UNUSED(spst);
  UNUSED(action);
  UNUSED(sts);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMSXLOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
                ERRCLS_DEBUG, ESMSX003, 0, "PtMiLsxStsReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLsxStsReq */


/*
*
*       Fun:   Portable control Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsxptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLsxCntrlReq
(
Pst *spst,                  /* post structure */
SxMngmt *cntrl              /* control */
)
#else
PRIVATE S16 PtMiLsxCntrlReq(spst, cntrl)
Pst *spst;                  /* post structure */
SxMngmt *cntrl;             /* control */
#endif
{
  TRC3(PtMiLsxCntrlReq)

  UNUSED(spst);
  UNUSED(cntrl);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMSXLOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
             ERRCLS_DEBUG, ESMSX004, 0, "PtMiLsxCntrlReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLsxCntrlReq */


/*
*
*       Fun:   Portable status Request TUCL
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  smsxptmi.c
*
*/

#ifdef ANSI
PRIVATE S16 PtMiLsxStaReq
(
Pst *spst,                  /* post structure */
SxMngmt *sta                /* status */
)
#else
PRIVATE S16 PtMiLsxStaReq(spst, sta)
Pst *spst;                  /* post structure */
SxMngmt *sta;               /* status */
#endif
{
  TRC3(PtMiLsxStaReq);

  UNUSED(spst);
  UNUSED(sta);

#if (ERRCLASS & ERRCLS_DEBUG)
   SMSXLOGERROR(spst->srcEnt, spst->srcInst, spst->srcProcId, \
                ERRCLS_DEBUG,ESMSX005, 0, "PtMiLsxStaReq () Failed");
#endif
  RETVALUE(ROK);
} /* end of PtMiLsxStaReq */

#endif /* PTSMSXMILSX */


/********************************************************************30**

         End of file:     

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      rks  1. initial release.
*********************************************************************91*/
