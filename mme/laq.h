/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter

     Type:     C include file

     Desc:     Diameter

     File:     laq.h

     Sid:

     Prg:

*********************************************************************21*/

#ifndef __LAQH__
#define __LAQH__

#ifdef LAQV1            /* LAQ interface version 1 */
#ifdef LAQIFVER
#undef LAQIFVER
#endif
#define LAQIFVER        0x0100
#endif

/***********************************************************************
         defines for layer-specific elements
 ***********************************************************************/

#define LAQ_PATHLEN      255

/***********************************************************************
         defines for "reason" in LaqStaInd
 ***********************************************************************/
#define LAQ_REASON_FD_CORE_INITIALIZE_FAIL   (LCM_REASON_LYR_SPECIFIC + 1)
#define LAQ_REASON_FD_CORE_PARSECONF_FAIL    (LCM_REASON_LYR_SPECIFIC + 2)
#define LAQ_REASON_FD_CORE_START_FAIL        (LCM_REASON_LYR_SPECIFIC + 3)
#define LAQ_REASON_OPINPROG                  (LCM_REASON_LYR_SPECIFIC + 4)
#define LAQ_REASON_DIFF_OPINPROG             (LCM_REASON_LYR_SPECIFIC + 5)
#define LAQ_REASON_FD_DICT_SEARCH            (LCM_REASON_LYR_SPECIFIC + 6)
#define LAQ_REASON_FD_REGISTER_CALLBACK      (LCM_REASON_LYR_SPECIFIC + 7)
#define LAQ_REASON_FD_REGISTER_APPLICATION   (LCM_REASON_LYR_SPECIFIC + 8)
#define LAQ_REASON_FD_SESS_FROMSID_FAIL      (LCM_REASON_LYR_SPECIFIC + 9)
#define LAQ_REASON_FD_SESS_DESTROY_FAIL      (LCM_REASON_LYR_SPECIFIC + 10)
#define LAQ_REASON_FD_SESS_NEW_FAIL          (LCM_REASON_LYR_SPECIFIC + 11)
#define LAQ_REASON_FD_SESS_GETSID_FAIL       (LCM_REASON_LYR_SPECIFIC + 12)
#define LAQ_REASON_FD_MSG_NEW_FAIL           (LCM_REASON_LYR_SPECIFIC + 13)
#define LAQ_REASON_FD_AVP_NEW_FAIL           (LCM_REASON_LYR_SPECIFIC + 14)
#define LAQ_REASON_FD_AVP_SETVALUE_FAIL      (LCM_REASON_LYR_SPECIFIC + 15)
#define LAQ_REASON_FD_AVP_ADD_FAIL           (LCM_REASON_LYR_SPECIFIC + 16)
#define LAQ_REASON_FD_MSG_ADD_ORIGIN_FAIL    (LCM_REASON_LYR_SPECIFIC + 17)
#define LAQ_REASON_FD_MSG_SEND_FAIL          (LCM_REASON_LYR_SPECIFIC + 18)
#define LAQ_REASON_AI_QUEUE_INIT_FAIL        (LCM_REASON_LYR_SPECIFIC + 19)
#define LAQ_REASON_FD_DICT_GETVAL            (LCM_REASON_LYR_SPECIFIC + 20)
#define LAQ_REASON_FD_MSG_AVP_HDR            (LCM_REASON_LYR_SPECIFIC + 21)
#define LAQ_REASON_FD_EXPERIMENTAL_RESULT    (LCM_REASON_LYR_SPECIFIC + 22)
#define LAQ_REASON_FD_E_UTRAN_VECTOR         (LCM_REASON_LYR_SPECIFIC + 23)
#define LAQ_REASON_FD_RAND_INVALID_LENGTH    (LCM_REASON_LYR_SPECIFIC + 24)
#define LAQ_REASON_FD_XRES_INVALID_LENGTH    (LCM_REASON_LYR_SPECIFIC + 25)
#define LAQ_REASON_FD_AUTN_INVALID_LENGTH    (LCM_REASON_LYR_SPECIFIC + 26)
#define LAQ_REASON_FD_KASME_INVALID_LENGTH   (LCM_REASON_LYR_SPECIFIC + 27)
#define LAQ_REASON_FD_MISSING_E_UTRAN_VECTOR (LCM_REASON_LYR_SPECIFIC + 28)
#define LAQ_REASON_FD_MSG_BROWSE             (LCM_REASON_LYR_SPECIFIC + 29)
#define LAQ_REASON_PARSE_MOLR                             (LCM_REASON_LYR_SPECIFIC + 30)
#define LAQ_REASON_PARSE_LCS_PRIVACY_EXCEPTION            (LCM_REASON_LYR_SPECIFIC + 31)
#define LAQ_REASON_PARSE_EXTERNAL_CLIENT                  (LCM_REASON_LYR_SPECIFIC + 32)
#define LAQ_REASON_PARSE_TELESERVICE_LIST                 (LCM_REASON_LYR_SPECIFIC + 33)
#define LAQ_REASON_PARSE_SERVICE_TYPE                     (LCM_REASON_LYR_SPECIFIC + 34)
#define LAQ_REASON_PARSE_CALL_BARRING_INFO                (LCM_REASON_LYR_SPECIFIC + 35)
#define LAQ_REASON_PARSE_APN_CONFIGURATION                (LCM_REASON_LYR_SPECIFIC + 36)
#define LAQ_REASON_PARSE_PDP_CONTEXT                      (LCM_REASON_LYR_SPECIFIC + 37)
#define LAQ_REASON_PARSE_CSG_SUBSCRIPTION_DATA            (LCM_REASON_LYR_SPECIFIC + 38)
#define LAQ_REASON_PARSE_ADJACENT_ACCESS_RESTRICTION_DATA (LCM_REASON_LYR_SPECIFIC + 39)
#define LAQ_REASON_PARSE_IMSI_GROUP_ID                    (LCM_REASON_LYR_SPECIFIC + 40)
#define LAQ_REASON_PARSE_AESE_COMMUNICATION_PATTERN       (LCM_REASON_LYR_SPECIFIC + 41)
#define LAQ_REASON_PARSE_COMMUNICATION_PATTERN_SET        (LCM_REASON_LYR_SPECIFIC + 42)
#define LAQ_REASON_PARSE_SCHEDULED_COMMUNICATION_TIME     (LCM_REASON_LYR_SPECIFIC + 43)
#define LAQ_REASON_PARSE_MONITORING_EVENT_CONFIGURATION   (LCM_REASON_LYR_SPECIFIC + 44)
#define LAQ_REASON_PARSE_SPECIFIC_APN_INFO                (LCM_REASON_LYR_SPECIFIC + 45)
#define LAQ_REASON_PARSE_LCS_INFO                         (LCM_REASON_LYR_SPECIFIC + 46)

/***********************************************************************
         defines for "event" in LaqStaInd
 ***********************************************************************/
/*
#define  LAQ_EVENT_???             (LCM_EVENT_LYR_SPECIFIC + 1)
*/

/***********************************************************************
         defines for "cause" in LaqStaInd
 ***********************************************************************/
/* diameter  related errors */

#define  LAQ_CAUSE_LOCK_ERR             (LCM_CAUSE_LYR_SPECIFIC + 1)
#define  LAQ_CAUSE_UNLOCK_ERR           (LCM_CAUSE_LYR_SPECIFIC + 2)

/***********************************************************************
         defines related to events across the management interface
 ***********************************************************************/

#define  EVTLAQCFGREQ                   1
#define  EVTLAQSTSREQ                   2
#define  EVTLAQCNTRLREQ                 3
#define  EVTLAQSTAREQ                   4
#define  EVTLAQCFGCFM                   5
#define  EVTLAQSTSCFM                   6
#define  EVTLAQCNTRLCFM                 7
#define  EVTLAQSTACFM                   8
#define  EVTLAQSTAIND                   9
#define  EVTLAQTRCIND                   10

/********************************************************************SZ**
 AQ States
*********************************************************************SZ*/

#define LAQ_UNINITIALIZED         1     /* uninitizlied */
#define LAQ_INITIALIZED           1     /* initizlied */
#define LAQ_CFG_PARSED            1     /* config file parsed */
#define LAQ_STARTED               1     /* started */
#define LAQ_WAITING_FOR_SHUTDOWN  1     /* shutdown issued */
#define LAQ_SHUTDOWN              1     /* shutdown */


/***********************************************************************
         defines related to events in LaqTrcInd primitive
 ***********************************************************************/
#define  LAQ_TCP_TXED                   0
#define  LAQ_UDP_TXED                   1
#define  LAQ_TCP_RXED                   2
#define  LAQ_UDP_RXED                   3
#define  LAQ_RAW_TXED                   4
#define  LAQ_RAW_RXED                   5

/* alarmInfo.type */
#define  LAQ_ALARMINFO_TYPE_NTPRSNT     0       /* alarmInfo is not present */
#define  LAQ_ALARMINFO_SAP_STATE        1       /* SAP state */
#define  LAQ_ALARMINFO_CON_STATE        2       /* connection state */
#define  LAQ_ALARMINFO_MEM_ID           3       /* memory id */
#define  LAQ_ALARMINFO_PAR_TYPE         4       /* parameter type */
#define  LAQ_ALARMINFO_TYPE_INFVER      5       /* invalid interface version */

/* parType values in LhiStaInd */
#define  LAQ_INV_MBUF                   1       /* invalid message buffer */
#define  LAQ_INV_SRVC_TYPE              2       /* invalid service type */
#define  LAQ_INV_TPT_ADDR               3       /* invalid transport address */
#define  LAQ_INV_TPT_PARAM              4       /* invalid transport params */
#define  LAQ_INV_ACTION                 5       /* invalid action */
#define  LAQ_INV_FILTER_TYPE_COMB       6       /* invalid filter/type combo */

/* selector values for lmPst.selector */
#define  LAQ_LC                         0       /* loosely coupled LM */
#define  LAQ_TC                         1       /* tightly coupled LM*/

/* sap.state */
#define  AQ_ST_UBND                     0x1     /* after SAP configuration */
#define  AQ_ST_BND                      0x2     /* after binding */

/* hash defines for flag field in HiHdrinfo */
#define  LHI_LEN_INCL_HDR               0x1     /* header length includes
                                                   PDU and header */


/* Error macro for TUCL management interface */
/* lhi_h_001.main_11: Fix for compilation warning */
#define LAQLOGERROR(pst, errCode, errVal, errDesc)            \
        SLogError(pst->srcEnt, pst->srcInst, pst->srcProcId,  \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,   \
                  (ErrCode)errCode, (ErrVal)errVal,           \
                  errDesc)

#define LAQ_ZERO(_str,_len)                                   \
   cmMemset((U8 *)_str, 0, _len);

/* Error codes */
#define   ELAQBASE     0             /* reserved */
#define   ERRLAQ       (ELAQBASE)

#define   ELAQ001      (ERRLAQ +    1)    /*        lhi.c: 192 */
#define   ELAQ002      (ERRLAQ +    2)    /*        lhi.c: 195 */
#define   ELAQ003      (ERRLAQ +    3)    /*        lhi.c: 198 */
#define   ELAQ004      (ERRLAQ +    4)    /*        lhi.c: 199 */
#define   ELAQ005      (ERRLAQ +    5)    /*        lhi.c: 200 */
#define   ELAQ006      (ERRLAQ +    6)    /*        lhi.c: 201 */
#define   ELAQ007      (ERRLAQ +    7)    /*        lhi.c: 202 */
#define   ELAQ008      (ERRLAQ +    8)    /*        lhi.c: 203 */
#define   ELAQ009      (ERRLAQ +    9)    /*        lhi.c: 204 */
#define   ELAQ010      (ERRLAQ +   10)    /*        lhi.c: 205 */
#define   ELAQ011      (ERRLAQ +   11)    /*        lhi.c: 206 */
#define   ELAQ012      (ERRLAQ +   12)    /*        lhi.c: 207 */
#define   ELAQ013      (ERRLAQ +   13)    /*        lhi.c: 209 */
#define   ELAQ014      (ERRLAQ +   14)    /*        lhi.c: 210 */
#define   ELAQ015      (ERRLAQ +   15)    /*        lhi.c: 211 */
#define   ELAQ016      (ERRLAQ +   16)    /*        lhi.c: 212 */
#define   ELAQ017      (ERRLAQ +   17)    /*        lhi.c: 214 */
#define   ELAQ018      (ERRLAQ +   18)    /*        lhi.c: 215 */
#define   ELAQ019      (ERRLAQ +   19)    /*        lhi.c: 216 */
#define   ELAQ020      (ERRLAQ +   20)    /*        lhi.c: 217 */
#define   ELAQ021      (ERRLAQ +   21)    /*        lhi.c: 218 */
#define   ELAQ022      (ERRLAQ +   22)    /*        lhi.c: 221 */
#define   ELAQ023      (ERRLAQ +   23)    /*        lhi.c: 222 */
#define   ELAQ024      (ERRLAQ +   24)    /*        lhi.c: 227 */
#define   ELAQ025      (ERRLAQ +   25)    /*        lhi.c: 234 */
#define   ELAQ026      (ERRLAQ +   26)    /*        lhi.c: 237 */
#define   ELAQ027      (ERRLAQ +   27)    /*        lhi.c: 247 */
#define   ELAQ028      (ERRLAQ +   28)    /*        lhi.c: 248 */
#define   ELAQ029      (ERRLAQ +   29)    /*        lhi.c: 253 */
#define   ELAQ030      (ERRLAQ +   30)    /*        lhi.c: 254 */
#define   ELAQ031      (ERRLAQ +   31)    /*        lhi.c: 255 */
#define   ELAQ032      (ERRLAQ +   32)    /*        lhi.c: 256 */
#define   ELAQ033      (ERRLAQ +   33)    /*        lhi.c: 259 */
#define   ELAQ034      (ERRLAQ +   34)    /*        lhi.c: 260 */
#define   ELAQ035      (ERRLAQ +   35)    /*        lhi.c: 261 */
#define   ELAQ036      (ERRLAQ +   36)    /*        lhi.c: 262 */
#define   ELAQ037      (ERRLAQ +   37)    /*        lhi.c: 264 */
#define   ELAQ038      (ERRLAQ +   38)    /*        lhi.c: 265 */
#define   ELAQ039      (ERRLAQ +   39)    /*        lhi.c: 266 */
#define   ELAQ040      (ERRLAQ +   40)    /*        lhi.c: 267 */
#define   ELAQ041      (ERRLAQ +   41)    /*        lhi.c: 268 */
#define   ELAQ042      (ERRLAQ +   42)    /*        lhi.c: 269 */
#define   ELAQ043      (ERRLAQ +   43)    /*        lhi.c: 270 */
#define   ELAQ044      (ERRLAQ +   44)    /*        lhi.c: 271 */
#define   ELAQ045      (ERRLAQ +   45)    /*        lhi.c: 273 */
#define   ELAQ046      (ERRLAQ +   46)    /*        lhi.c: 274 */
#define   ELAQ047      (ERRLAQ +   47)    /*        lhi.c: 275 */
#define   ELAQ048      (ERRLAQ +   48)    /*        lhi.c: 276 */
#define   ELAQ049      (ERRLAQ +   49)    /*        lhi.c: 277 */
#define   ELAQ050      (ERRLAQ +   50)    /*        lhi.c: 278 */
#define   ELAQ051      (ERRLAQ +   51)    /*        lhi.c: 288 */
#define   ELAQ052      (ERRLAQ +   52)    /*        lhi.c: 290 */
#define   ELAQ053      (ERRLAQ +   53)    /*        lhi.c: 292 */
#define   ELAQ054      (ERRLAQ +   54)    /*        lhi.c: 294 */
#define   ELAQ055      (ERRLAQ +   55)    /*        lhi.c: 296 */
#define   ELAQ056      (ERRLAQ +   56)    /*        lhi.c: 297 */
#define   ELAQ057      (ERRLAQ +   57)    /*        lhi.c: 298 */
#define   ELAQ058      (ERRLAQ +   58)    /*        lhi.c: 302 */
#define   ELAQ059      (ERRLAQ +   59)    /*        lhi.c: 305 */
#define   ELAQ060      (ERRLAQ +   60)    /*        lhi.c: 308 */
#define   ELAQ061      (ERRLAQ +   61)    /*        lhi.c: 309 */
#define   ELAQ062      (ERRLAQ +   62)    /*        lhi.c: 311 */
#define   ELAQ063      (ERRLAQ +   63)    /*        lhi.c: 312 */
#define   ELAQ064      (ERRLAQ +   64)    /*        lhi.c: 313 */
#define   ELAQ065      (ERRLAQ +   65)    /*        lhi.c: 320 */
#define   ELAQ066      (ERRLAQ +   66)    /*        lhi.c: 327 */
#define   ELAQ067      (ERRLAQ +   67)    /*        lhi.c: 328 */
#define   ELAQ068      (ERRLAQ +   68)    /*        lhi.c: 374 */
#define   ELAQ069      (ERRLAQ +   69)    /*        lhi.c: 375 */
#define   ELAQ070      (ERRLAQ +   70)    /*        lhi.c: 424 */
#define   ELAQ071      (ERRLAQ +   71)    /*        lhi.c: 433 */
#define   ELAQ072      (ERRLAQ +   72)    /*        lhi.c: 435 */
#define   ELAQ073      (ERRLAQ +   73)    /*        lhi.c: 440 */
#define   ELAQ074      (ERRLAQ +   74)    /*        lhi.c: 445 */
#define   ELAQ075      (ERRLAQ +   75)    /*        lhi.c: 457 */
#define   ELAQ076      (ERRLAQ +   76)    /*        lhi.c: 462 */
#define   ELAQ077      (ERRLAQ +   77)    /*        lhi.c: 467 */
#define   ELAQ078      (ERRLAQ +   78)    /*        lhi.c: 472 */
#define   ELAQ079      (ERRLAQ +   79)    /*        lhi.c: 482 */
#define   ELAQ080      (ERRLAQ +   80)    /*        lhi.c: 489 */
#define   ELAQ081      (ERRLAQ +   81)    /*        lhi.c: 490 */
#define   ELAQ082      (ERRLAQ +   82)    /*        lhi.c: 491 */
#define   ELAQ083      (ERRLAQ +   83)    /*        lhi.c: 493 */
#define   ELAQ084      (ERRLAQ +   84)    /*        lhi.c: 494 */
#define   ELAQ085      (ERRLAQ +   85)    /*        lhi.c: 541 */
#define   ELAQ086      (ERRLAQ +   86)    /*        lhi.c: 544 */
#define   ELAQ087      (ERRLAQ +   87)    /*        lhi.c: 602 */
#define   ELAQ088      (ERRLAQ +   88)    /*        lhi.c: 607 */
#define   ELAQ089      (ERRLAQ +   89)    /*        lhi.c: 613 */
#define   ELAQ090      (ERRLAQ +   90)    /*        lhi.c: 614 */
#define   ELAQ091      (ERRLAQ +   91)    /*        lhi.c: 615 */
#define   ELAQ092      (ERRLAQ +   92)    /*        lhi.c: 760 */
#define   ELAQ093      (ERRLAQ +   93)    /*        lhi.c: 761 */
#define   ELAQ094      (ERRLAQ +   94)    /*        lhi.c: 763 */
#define   ELAQ095      (ERRLAQ +   95)    /*        lhi.c: 764 */
#define   ELAQ096      (ERRLAQ +   96)    /*        lhi.c: 765 */
#define   ELAQ097      (ERRLAQ +   97)    /*        lhi.c: 766 */
#define   ELAQ098      (ERRLAQ +   98)    /*        lhi.c: 767 */
#define   ELAQ099      (ERRLAQ +   99)    /*        lhi.c: 768 */
#define   ELAQ100      (ERRLAQ +  100)    /*        lhi.c: 769 */
#define   ELAQ101      (ERRLAQ +  101)    /*        lhi.c: 770 */
#define   ELAQ102      (ERRLAQ +  102)    /*        lhi.c: 771 */
#define   ELAQ103      (ERRLAQ +  103)    /*        lhi.c: 772 */
#define   ELAQ104      (ERRLAQ +  104)    /*        lhi.c: 773 */
#define   ELAQ105      (ERRLAQ +  105)    /*        lhi.c: 774 */
#define   ELAQ106      (ERRLAQ +  106)    /*        lhi.c: 775 */
#define   ELAQ107      (ERRLAQ +  107)    /*        lhi.c: 776 */
#define   ELAQ108      (ERRLAQ +  108)    /*        lhi.c: 777 */
#define   ELAQ109      (ERRLAQ +  109)    /*        lhi.c: 778 */
#define   ELAQ110      (ERRLAQ +  110)    /*        lhi.c: 779 */
#define   ELAQ111      (ERRLAQ +  111)    /*        lhi.c: 780 */
#define   ELAQ112      (ERRLAQ +  112)    /*        lhi.c: 781 */
#define   ELAQ113      (ERRLAQ +  113)    /*        lhi.c: 782 */
#define   ELAQ114      (ERRLAQ +  114)    /*        lhi.c: 789 */
#define   ELAQ115      (ERRLAQ +  115)    /*        lhi.c: 790 */
#define   ELAQ116      (ERRLAQ +  116)    /*        lhi.c: 792 */
#define   ELAQ117      (ERRLAQ +  117)    /*        lhi.c: 793 */
#define   ELAQ118      (ERRLAQ +  118)    /*        lhi.c: 794 */
#define   ELAQ119      (ERRLAQ +  119)    /*        lhi.c: 795 */
#define   ELAQ120      (ERRLAQ +  120)    /*        lhi.c: 796 */
#define   ELAQ121      (ERRLAQ +  121)    /*        lhi.c: 797 */
#define   ELAQ122      (ERRLAQ +  122)    /*        lhi.c: 798 */
#define   ELAQ123      (ERRLAQ +  123)    /*        lhi.c: 799 */
#define   ELAQ124      (ERRLAQ +  124)    /*        lhi.c: 800 */
#define   ELAQ125      (ERRLAQ +  125)    /*        lhi.c: 801 */
#define   ELAQ126      (ERRLAQ +  126)    /*        lhi.c: 835 */
#define   ELAQ127      (ERRLAQ +  127)    /*        lhi.c: 842 */
#define   ELAQ128      (ERRLAQ +  128)    /*        lhi.c: 843 */
#define   ELAQ129      (ERRLAQ +  129)    /*        lhi.c: 845 */
#define   ELAQ130      (ERRLAQ +  130)    /*        lhi.c: 846 */
#define   ELAQ131      (ERRLAQ +  131)    /*        lhi.c: 892 */
#define   ELAQ132      (ERRLAQ +  132)    /*        lhi.c: 896 */
#define   ELAQ133      (ERRLAQ +  133)    /*        lhi.c: 903 */
#define   ELAQ134      (ERRLAQ +  134)    /*        lhi.c: 904 */
#define   ELAQ135      (ERRLAQ +  135)    /*        lhi.c: 955 */
#define   ELAQ136      (ERRLAQ +  136)    /*        lhi.c: 964 */
#define   ELAQ137      (ERRLAQ +  137)    /*        lhi.c: 965 */
#define   ELAQ138      (ERRLAQ +  138)    /*        lhi.c: 966 */
#define   ELAQ139      (ERRLAQ +  139)    /*        lhi.c: 969 */
#define   ELAQ140      (ERRLAQ +  140)    /*        lhi.c: 970 */
#define   ELAQ141      (ERRLAQ +  141)    /*        lhi.c: 975 */
#define   ELAQ142      (ERRLAQ +  142)    /*        lhi.c: 983 */
#define   ELAQ143      (ERRLAQ +  143)    /*        lhi.c: 985 */
#define   ELAQ144      (ERRLAQ +  144)    /*        lhi.c: 986 */
#define   ELAQ145      (ERRLAQ +  145)    /*        lhi.c:1033 */
#define   ELAQ146      (ERRLAQ +  146)    /*        lhi.c:1039 */
#define   ELAQ147      (ERRLAQ +  147)    /*        lhi.c:1041 */
#define   ELAQ148      (ERRLAQ +  148)    /*        lhi.c:1046 */
#define   ELAQ149      (ERRLAQ +  149)    /*        lhi.c:1051 */
#define   ELAQ150      (ERRLAQ +  150)    /*        lhi.c:1057 */
#define   ELAQ151      (ERRLAQ +  151)    /*        lhi.c:1063 */
#define   ELAQ152      (ERRLAQ +  152)    /*        lhi.c:1070 */
#define   ELAQ153      (ERRLAQ +  153)    /*        lhi.c:1071 */
#define   ELAQ154      (ERRLAQ +  154)    /*        lhi.c:1072 */
#define   ELAQ155      (ERRLAQ +  155)    /*        lhi.c:1074 */
#define   ELAQ156      (ERRLAQ +  156)    /*        lhi.c:1075 */
#define   ELAQ157      (ERRLAQ +  157)    /*        lhi.c:1124 */
#define   ELAQ158      (ERRLAQ +  158)    /*        lhi.c:1127 */
#define   ELAQ159      (ERRLAQ +  159)    /*        lhi.c:1128 */
#define   ELAQ160      (ERRLAQ +  160)    /*        lhi.c:1130 */
#define   ELAQ161      (ERRLAQ +  161)    /*        lhi.c:1131 */
#define   ELAQ162      (ERRLAQ +  162)    /*        lhi.c:1179 */
#define   ELAQ163      (ERRLAQ +  163)    /*        lhi.c:1180 */
#define   ELAQ164      (ERRLAQ +  164)    /*        lhi.c:1189 */
#define   ELAQ165      (ERRLAQ +  165)    /*        lhi.c:1194 */
#define   ELAQ166      (ERRLAQ +  166)    /*        lhi.c:1201 */
#define   ELAQ167      (ERRLAQ +  167)    /*        lhi.c:1204 */
#define   ELAQ168      (ERRLAQ +  168)    /*        lhi.c:1205 */
#define   ELAQ169      (ERRLAQ +  169)    /*        lhi.c:1208 */
#define   ELAQ170      (ERRLAQ +  170)    /*        lhi.c:1209 */
#define   ELAQ171      (ERRLAQ +  171)    /*        lhi.c:1210 */
#define   ELAQ172      (ERRLAQ +  172)    /*        lhi.c:1211 */
#define   ELAQ173      (ERRLAQ +  173)    /*        lhi.c:1212 */
#define   ELAQ174      (ERRLAQ +  174)    /*        lhi.c:1214 */
#define   ELAQ175      (ERRLAQ +  175)    /*        lhi.c:1215 */
#define   ELAQ176      (ERRLAQ +  176)    /*        lhi.c:1216 */
#define   ELAQ177      (ERRLAQ +  177)    /*        lhi.c:1217 */
#define   ELAQ178      (ERRLAQ +  178)    /*        lhi.c:1219 */
#define   ELAQ179      (ERRLAQ +  179)    /*        lhi.c:1220 */
#define   ELAQ180      (ERRLAQ +  180)    /*        lhi.c:1221 */
#define   ELAQ181      (ERRLAQ +  181)    /*        lhi.c:1222 */
#define   ELAQ182      (ERRLAQ +  182)    /*        lhi.c:1223 */
#define   ELAQ183      (ERRLAQ +  183)    /*        lhi.c:1224 */
#define   ELAQ184      (ERRLAQ +  184)    /*        lhi.c:1225 */
#define   ELAQ185      (ERRLAQ +  185)    /*        lhi.c:1226 */
#define   ELAQ186      (ERRLAQ +  186)    /*        lhi.c:1227 */
#define   ELAQ187      (ERRLAQ +  187)    /*        lhi.c:1228 */
#define   ELAQ188      (ERRLAQ +  188)    /*        lhi.c:1233 */
#define   ELAQ189      (ERRLAQ +  189)    /*        lhi.c:1236 */
#define   ELAQ190      (ERRLAQ +  190)    /*        lhi.c:1246 */
#define   ELAQ191      (ERRLAQ +  191)    /*        lhi.c:1247 */
#define   ELAQ192      (ERRLAQ +  192)    /*        lhi.c:1248 */
#define   ELAQ193      (ERRLAQ +  193)    /*        lhi.c:1249 */
#define   ELAQ194      (ERRLAQ +  194)    /*        lhi.c:1250 */
#define   ELAQ195      (ERRLAQ +  195)    /*        lhi.c:1251 */
#define   ELAQ196      (ERRLAQ +  196)    /*        lhi.c:1253 */
#define   ELAQ197      (ERRLAQ +  197)    /*        lhi.c:1254 */
#define   ELAQ198      (ERRLAQ +  198)    /*        lhi.c:1255 */
#define   ELAQ199      (ERRLAQ +  199)    /*        lhi.c:1256 */
#define   ELAQ200      (ERRLAQ +  200)    /*        lhi.c:1257 */
#define   ELAQ201      (ERRLAQ +  201)    /*        lhi.c:1258 */
#define   ELAQ202      (ERRLAQ +  202)    /*        lhi.c:1259 */
#define   ELAQ203      (ERRLAQ +  203)    /*        lhi.c:1260 */
#define   ELAQ204      (ERRLAQ +  204)    /*        lhi.c:1262 */
#define   ELAQ205      (ERRLAQ +  205)    /*        lhi.c:1263 */
#define   ELAQ206      (ERRLAQ +  206)    /*        lhi.c:1264 */
#define   ELAQ207      (ERRLAQ +  207)    /*        lhi.c:1265 */
#define   ELAQ208      (ERRLAQ +  208)    /*        lhi.c:1269 */
#define   ELAQ209      (ERRLAQ +  209)    /*        lhi.c:1271 */
#define   ELAQ210      (ERRLAQ +  210)    /*        lhi.c:1273 */
#define   ELAQ211      (ERRLAQ +  211)    /*        lhi.c:1275 */
#define   ELAQ212      (ERRLAQ +  212)    /*        lhi.c:1280 */
#define   ELAQ213      (ERRLAQ +  213)    /*        lhi.c:1281 */
#define   ELAQ214      (ERRLAQ +  214)    /*        lhi.c:1291 */
#define   ELAQ215      (ERRLAQ +  215)    /*        lhi.c:1292 */
#define   ELAQ216      (ERRLAQ +  216)    /*        lhi.c:1294 */
#define   ELAQ217      (ERRLAQ +  217)    /*        lhi.c:1295 */
#define   ELAQ218      (ERRLAQ +  218)    /*        lhi.c:1297 */
#define   ELAQ219      (ERRLAQ +  219)    /*        lhi.c:1302 */
#define   ELAQ220      (ERRLAQ +  220)    /*        lhi.c:1305 */
#define   ELAQ221      (ERRLAQ +  221)    /*        lhi.c:1307 */
#define   ELAQ222      (ERRLAQ +  222)    /*        lhi.c:1308 */
#define   ELAQ223      (ERRLAQ +  223)    /*        lhi.c:1310 */
#define   ELAQ224      (ERRLAQ +  224)    /*        lhi.c:1312 */
#define   ELAQ225      (ERRLAQ +  225)    /*        lhi.c:1314 */
#define   ELAQ226      (ERRLAQ +  226)    /*        lhi.c:1316 */
#define   ELAQ227      (ERRLAQ +  227)    /*        lhi.c:1317 */
#define   ELAQ228      (ERRLAQ +  228)    /*        lhi.c:1324 */
#define   ELAQ229      (ERRLAQ +  229)    /*        lhi.c:1375 */
#define   ELAQ230      (ERRLAQ +  230)    /*        lhi.c:1376 */
#define   ELAQ231      (ERRLAQ +  231)    /*        lhi.c:1418 */
#define   ELAQ232      (ERRLAQ +  232)    /*        lhi.c:1419 */
#define   ELAQ233      (ERRLAQ +  233)    /*        lhi.c:1421 */
#define   ELAQ234      (ERRLAQ +  234)    /*        lhi.c:1422 */
#define   ELAQ235      (ERRLAQ +  235)    /*        lhi.c:1423 */
#define   ELAQ236      (ERRLAQ +  236)    /*        lhi.c:1432 */
#define   ELAQ237      (ERRLAQ +  237)    /*        lhi.c:1441 */
#define   ELAQ238      (ERRLAQ +  238)    /*        lhi.c:1446 */
#define   ELAQ239      (ERRLAQ +  239)    /*        lhi.c:1448 */
#define   ELAQ240      (ERRLAQ +  240)    /*        lhi.c:1453 */
#define   ELAQ241      (ERRLAQ +  241)    /*        lhi.c:1465 */
#define   ELAQ242      (ERRLAQ +  242)    /*        lhi.c:1470 */
#define   ELAQ243      (ERRLAQ +  243)    /*        lhi.c:1475 */
#define   ELAQ244      (ERRLAQ +  244)    /*        lhi.c:1480 */
#define   ELAQ245      (ERRLAQ +  245)    /*        lhi.c:1490 */
#define   ELAQ246      (ERRLAQ +  246)    /*        lhi.c:1536 */
#define   ELAQ247      (ERRLAQ +  247)    /*        lhi.c:1537 */
#define   ELAQ248      (ERRLAQ +  248)    /*        lhi.c:1581 */
#define   ELAQ249      (ERRLAQ +  249)    /*        lhi.c:1582 */
#define   ELAQ250      (ERRLAQ +  250)    /*        lhi.c:1583 */
#define   ELAQ251      (ERRLAQ +  251)    /*        lhi.c:1599 */
#define   ELAQ252      (ERRLAQ +  252)    /*        lhi.c:1605 */
#define   ELAQ253      (ERRLAQ +  253)    /*        lhi.c:1703 */
#define   ELAQ254      (ERRLAQ +  254)    /*        lhi.c:1757 */
#define   ELAQ255      (ERRLAQ +  255)    /*        lhi.c:1758 */
#define   ELAQ256      (ERRLAQ +  256)    /*        lhi.c:1760 */
#define   ELAQ257      (ERRLAQ +  257)    /*        lhi.c:1761 */
#define   ELAQ258      (ERRLAQ +  258)    /*        lhi.c:1769 */
#define   ELAQ259      (ERRLAQ +  259)    /*        lhi.c:1770 */
#define   ELAQ260      (ERRLAQ +  260)    /*        lhi.c:1771 */
#define   ELAQ261      (ERRLAQ +  261)    /*        lhi.c:1772 */
#define   ELAQ262      (ERRLAQ +  262)    /*        lhi.c:1773 */
#define   ELAQ263      (ERRLAQ +  263)    /*        lhi.c:1774 */
#define   ELAQ264      (ERRLAQ +  264)    /*        lhi.c:1775 */
#define   ELAQ265      (ERRLAQ +  265)    /*        lhi.c:1776 */
#define   ELAQ266      (ERRLAQ +  266)    /*        lhi.c:1777 */
#define   ELAQ267      (ERRLAQ +  267)    /*        lhi.c:1778 */
#define   ELAQ268      (ERRLAQ +  268)    /*        lhi.c:1779 */
#define   ELAQ269      (ERRLAQ +  269)    /*        lhi.c:1780 */
#define   ELAQ270      (ERRLAQ +  270)    /*        lhi.c:1781 */
#define   ELAQ271      (ERRLAQ +  271)    /*        lhi.c:1782 */
#define   ELAQ272      (ERRLAQ +  272)    /*        lhi.c:1783 */
#define   ELAQ273      (ERRLAQ +  273)    /*        lhi.c:1784 */
#define   ELAQ274      (ERRLAQ +  274)    /*        lhi.c:1785 */
#define   ELAQ275      (ERRLAQ +  275)    /*        lhi.c:1786 */
#define   ELAQ276      (ERRLAQ +  276)    /*        lhi.c:1787 */
#define   ELAQ277      (ERRLAQ +  277)    /*        lhi.c:1788 */
#define   ELAQ278      (ERRLAQ +  278)    /*        lhi.c:1790 */
#define   ELAQ279      (ERRLAQ +  279)    /*        lhi.c:1791 */
#define   ELAQ280      (ERRLAQ +  280)    /*        lhi.c:1799 */
#define   ELAQ281      (ERRLAQ +  281)    /*        lhi.c:1800 */
#define   ELAQ282      (ERRLAQ +  282)    /*        lhi.c:1801 */
#define   ELAQ283      (ERRLAQ +  283)    /*        lhi.c:1802 */
#define   ELAQ284      (ERRLAQ +  284)    /*        lhi.c:1803 */
#define   ELAQ285      (ERRLAQ +  285)    /*        lhi.c:1804 */
#define   ELAQ286      (ERRLAQ +  286)    /*        lhi.c:1805 */
#define   ELAQ287      (ERRLAQ +  287)    /*        lhi.c:1806 */
#define   ELAQ288      (ERRLAQ +  288)    /*        lhi.c:1807 */
#define   ELAQ289      (ERRLAQ +  289)    /*        lhi.c:1808 */
#define   ELAQ290      (ERRLAQ +  290)    /*        lhi.c:1810 */
#define   ELAQ291      (ERRLAQ +  291)    /*        lhi.c:1811 */
#define   ELAQ292      (ERRLAQ +  292)    /*        lhi.c:1839 */
#define   ELAQ293      (ERRLAQ +  293)    /*        lhi.c:1859 */
#define   ELAQ294      (ERRLAQ +  294)    /*        lhi.c:1905 */
#define   ELAQ295      (ERRLAQ +  295)    /*        lhi.c:1906 */
#define   ELAQ296      (ERRLAQ +  296)    /*        lhi.c:1914 */
#define   ELAQ297      (ERRLAQ +  297)    /*        lhi.c:1918 */
#define   ELAQ298      (ERRLAQ +  298)    /*        lhi.c:1966 */
#define   ELAQ299      (ERRLAQ +  299)    /*        lhi.c:1967 */
#define   ELAQ300      (ERRLAQ +  300)    /*        lhi.c:1968 */
#define   ELAQ301      (ERRLAQ +  301)    /*        lhi.c:1976 */
#define   ELAQ302      (ERRLAQ +  302)    /*        lhi.c:1983 */
#define   ELAQ303      (ERRLAQ +  303)    /*        lhi.c:1984 */
#define   ELAQ304      (ERRLAQ +  304)    /*        lhi.c:1987 */
#define   ELAQ305      (ERRLAQ +  305)    /*        lhi.c:1988 */
#define   ELAQ306      (ERRLAQ +  306)    /*        lhi.c:1989 */
#define   ELAQ307      (ERRLAQ +  307)    /*        lhi.c:1995 */
#define   ELAQ308      (ERRLAQ +  308)    /*        lhi.c:2042 */
#define   ELAQ309      (ERRLAQ +  309)    /*        lhi.c:2043 */
#define   ELAQ310      (ERRLAQ +  310)    /*        lhi.c:2045 */
#define   ELAQ311      (ERRLAQ +  311)    /*        lhi.c:2046 */
#define   ELAQ312      (ERRLAQ +  312)    /*        lhi.c:2047 */
#define   ELAQ313      (ERRLAQ +  313)    /*        lhi.c:2055 */
#define   ELAQ314      (ERRLAQ +  314)    /*        lhi.c:2060 */
#define   ELAQ315      (ERRLAQ +  315)    /*        lhi.c:2065 */
#define   ELAQ316      (ERRLAQ +  316)    /*        lhi.c:2067 */
#define   ELAQ317      (ERRLAQ +  317)    /*        lhi.c:2072 */
#define   ELAQ318      (ERRLAQ +  318)    /*        lhi.c:2078 */
#define   ELAQ319      (ERRLAQ +  319)    /*        lhi.c:2084 */
#define   ELAQ320      (ERRLAQ +  320)    /*        lhi.c:2133 */
#define   ELAQ321      (ERRLAQ +  321)    /*        lhi.c:2134 */
#define   ELAQ322      (ERRLAQ +  322)    /*        lhi.c:2136 */
#define   ELAQ323      (ERRLAQ +  323)    /*        lhi.c:2137 */
#define   ELAQ324      (ERRLAQ +  324)    /*        lhi.c:2140 */

#define   ELAQ325      (ERRLAQ +  325)    /*        lhi.h: 305 */
#define   ELAQ326      (ERRLAQ +  326)    /*        lhi.h: 306 */
#define   ELAQ327      (ERRLAQ +  327)    /*        lhi.h: 307 */
#define   ELAQ328      (ERRLAQ +  328)    /*        lhi.h: 308 */
#define   ELAQ329      (ERRLAQ +  329)    /*        lhi.h: 309 */
#define   ELAQ330      (ERRLAQ +  330)    /*        lhi.h: 310 */
#define   ELAQ331      (ERRLAQ +  331)    /*        lhi.h: 311 */
#define   ELAQ332      (ERRLAQ +  332)    /*        lhi.h: 312 */
#define   ELAQ333      (ERRLAQ +  333)    /*        lhi.h: 313 */
#define   ELAQ334      (ERRLAQ +  334)    /*        lhi.h: 314 */
#define   ELAQ335      (ERRLAQ +  335)    /*        lhi.h: 315 */
#define   ELAQ336      (ERRLAQ +  336)    /*        lhi.h: 316 */
#define   ELAQ337      (ERRLAQ +  337)    /*        lhi.h: 317 */
#define   ELAQ338      (ERRLAQ +  338)    /*        lhi.h: 318 */
#define   ELAQ339      (ERRLAQ +  339)    /*        lhi.h: 319 */
#define   ELAQ340      (ERRLAQ +  340)    /*        lhi.h: 320 */
#define   ELAQ341      (ERRLAQ +  341)    /*        lhi.h: 321 */
#define   ELAQ342      (ERRLAQ +  342)    /*        lhi.h: 322 */
#define   ELAQ343      (ERRLAQ +  343)    /*        lhi.h: 323 */
#define   ELAQ344      (ERRLAQ +  344)    /*        lhi.h: 324 */
#define   ELAQ345      (ERRLAQ +  345)    /*        lhi.h: 325 */
#define   ELAQ346      (ERRLAQ +  346)    /*        lhi.h: 326 */
#define   ELAQ347      (ERRLAQ +  347)    /*        lhi.h: 327 */
#define   ELAQ348      (ERRLAQ +  348)    /*        lhi.h: 328 */
#define   ELAQ349      (ERRLAQ +  349)    /*        lhi.h: 329 */
#define   ELAQ350      (ERRLAQ +  350)    /*        lhi.h: 330 */
#define   ELAQ351      (ERRLAQ +  351)    /*        lhi.h: 331 */
#define   ELAQ352      (ERRLAQ +  352)    /*        lhi.h: 332 */
#define   ELAQ353      (ERRLAQ +  353)    /*        lhi.h: 333 */
#define   ELAQ354      (ERRLAQ +  354)    /*        lhi.h: 334 */
#define   ELAQ355      (ERRLAQ +  355)    /*        lhi.h: 335 */
#define   ELAQ356      (ERRLAQ +  356)    /*        lhi.h: 336 */
#define   ELAQ357      (ERRLAQ +  357)    /*        lhi.h: 337 */
#define   ELAQ358      (ERRLAQ +  358)    /*        lhi.h: 338 */
#define   ELAQ359      (ERRLAQ +  359)    /*        lhi.h: 339 */
#define   ELAQ360      (ERRLAQ +  360)    /*        lhi.h: 340 */
#define   ELAQ361      (ERRLAQ +  361)    /*        lhi.h: 341 */
#define   ELAQ362      (ERRLAQ +  362)    /*        lhi.h: 342 */
#define   ELAQ363      (ERRLAQ +  363)    /*        lhi.h: 343 */
#define   ELAQ364      (ERRLAQ +  364)    /*        lhi.h: 344 */
#define   ELAQ365      (ERRLAQ +  365)    /*        lhi.h: 345 */
#define   ELAQ366      (ERRLAQ +  366)    /*        lhi.h: 346 */
#define   ELAQ367      (ERRLAQ +  367)    /*        lhi.h: 347 */
#define   ELAQ368      (ERRLAQ +  368)    /*        lhi.h: 348 */
#define   ELAQ369      (ERRLAQ +  369)    /*        lhi.h: 349 */
#define   ELAQ370      (ERRLAQ +  370)    /*        lhi.h: 350 */
#define   ELAQ371      (ERRLAQ +  371)    /*        lhi.h: 351 */
#define   ELAQ372      (ERRLAQ +  372)    /*        lhi.h: 352 */
#define   ELAQ373      (ERRLAQ +  373)    /*        lhi.h: 353 */
#define   ELAQ374      (ERRLAQ +  374)    /*        lhi.h: 354 */
#define   ELAQ375      (ERRLAQ +  375)    /*        lhi.h: 355 */
#define   ELAQ376      (ERRLAQ +  376)    /*        lhi.h: 356 */
#define   ELAQ377      (ERRLAQ +  377)    /*        lhi.h: 357 */
#define   ELAQ378      (ERRLAQ +  378)    /*        lhi.h: 358 */
#define   ELAQ379      (ERRLAQ +  379)    /*        lhi.h: 359 */
#define   ELAQ380      (ERRLAQ +  380)    /*        lhi.h: 360 */
#define   ELAQ381      (ERRLAQ +  381)    /*        lhi.h: 361 */
#define   ELAQ382      (ERRLAQ +  382)    /*        lhi.h: 362 */
#define   ELAQ383      (ERRLAQ +  383)    /*        lhi.h: 363 */
#define   ELAQ384      (ERRLAQ +  384)    /*        lhi.h: 364 */
#define   ELAQ385      (ERRLAQ +  385)    /*        lhi.h: 365 */
#define   ELAQ386      (ERRLAQ +  386)    /*        lhi.h: 366 */
#define   ELAQ387      (ERRLAQ +  387)    /*        lhi.h: 367 */
#define   ELAQ388      (ERRLAQ +  388)    /*        lhi.h: 368 */
#define   ELAQ389      (ERRLAQ +  389)    /*        lhi.h: 369 */
#define   ELAQ390      (ERRLAQ +  390)    /*        lhi.h: 370 */
#define   ELAQ391      (ERRLAQ +  391)    /*        lhi.h: 371 */
#define   ELAQ392      (ERRLAQ +  392)    /*        lhi.h: 372 */
#define   ELAQ393      (ERRLAQ +  393)    /*        lhi.h: 373 */
#define   ELAQ394      (ERRLAQ +  394)    /*        lhi.h: 374 */
#define   ELAQ395      (ERRLAQ +  395)    /*        lhi.h: 375 */
#define   ELAQ396      (ERRLAQ +  396)    /*        lhi.h: 376 */
#define   ELAQ397      (ERRLAQ +  397)    /*        lhi.h: 377 */
#define   ELAQ398      (ERRLAQ +  398)    /*        lhi.h: 378 */
#define   ELAQ399      (ERRLAQ +  399)    /*        lhi.h: 379 */
#define   ELAQ400      (ERRLAQ +  400)    /*        lhi.h: 380 */
#define   ELAQ401      (ERRLAQ +  401)    /*        lhi.h: 381 */
#define   ELAQ402      (ERRLAQ +  402)    /*        lhi.h: 382 */
#define   ELAQ403      (ERRLAQ +  403)    /*        lhi.h: 383 */
#define   ELAQ404      (ERRLAQ +  404)    /*        lhi.h: 384 */
#define   ELAQ405      (ERRLAQ +  405)    /*        lhi.h: 385 */
#define   ELAQ406      (ERRLAQ +  406)    /*        lhi.h: 386 */
#define   ELAQ407      (ERRLAQ +  407)    /*        lhi.h: 387 */
#define   ELAQ408      (ERRLAQ +  408)    /*        lhi.h: 388 */
#define   ELAQ409      (ERRLAQ +  409)    /*        lhi.h: 389 */
#define   ELAQ410      (ERRLAQ +  410)    /*        lhi.h: 390 */
#define   ELAQ411      (ERRLAQ +  411)    /*        lhi.h: 391 */
#define   ELAQ412      (ERRLAQ +  412)    /*        lhi.h: 392 */
#define   ELAQ413      (ERRLAQ +  413)    /*        lhi.h: 393 */
#define   ELAQ414      (ERRLAQ +  414)    /*        lhi.h: 394 */
#define   ELAQ415      (ERRLAQ +  415)    /*        lhi.h: 395 */
#define   ELAQ416      (ERRLAQ +  416)    /*        lhi.h: 396 */
#define   ELAQ417      (ERRLAQ +  417)    /*        lhi.h: 397 */
#define   ELAQ418      (ERRLAQ +  418)    /*        lhi.h: 398 */
#define   ELAQ419      (ERRLAQ +  419)    /*        lhi.h: 399 */
#define   ELAQ420      (ERRLAQ +  420)    /*        lhi.h: 400 */
#define   ELAQ421      (ERRLAQ +  421)    /*        lhi.h: 401 */
#define   ELAQ422      (ERRLAQ +  422)    /*        lhi.h: 402 */
#define   ELAQ423      (ERRLAQ +  423)    /*        lhi.h: 403 */
#define   ELAQ424      (ERRLAQ +  424)    /*        lhi.h: 404 */
#define   ELAQ425      (ERRLAQ +  425)    /*        lhi.h: 405 */
#define   ELAQ426      (ERRLAQ +  426)    /*        lhi.h: 406 */
#define   ELAQ427      (ERRLAQ +  427)    /*        lhi.h: 407 */
#define   ELAQ428      (ERRLAQ +  428)    /*        lhi.h: 408 */
#define   ELAQ429      (ERRLAQ +  429)    /*        lhi.h: 409 */
#define   ELAQ430      (ERRLAQ +  430)    /*        lhi.h: 410 */
#define   ELAQ431      (ERRLAQ +  431)    /*        lhi.h: 411 */
#define   ELAQ432      (ERRLAQ +  432)    /*        lhi.h: 412 */
#define   ELAQ433      (ERRLAQ +  433)    /*        lhi.h: 413 */
#define   ELAQ434      (ERRLAQ +  434)    /*        lhi.h: 414 */
#define   ELAQ435      (ERRLAQ +  435)    /*        lhi.h: 415 */
#define   ELAQ436      (ERRLAQ +  436)    /*        lhi.h: 416 */
#define   ELAQ437      (ERRLAQ +  437)    /*        lhi.h: 417 */
#define   ELAQ438      (ERRLAQ +  438)    /*        lhi.h: 418 */
#define   ELAQ439      (ERRLAQ +  439)    /*        lhi.h: 419 */
#define   ELAQ440      (ERRLAQ +  440)    /*        lhi.h: 420 */
#define   ELAQ441      (ERRLAQ +  441)    /*        lhi.h: 421 */
#define   ELAQ442      (ERRLAQ +  442)    /*        lhi.h: 422 */
#define   ELAQ443      (ERRLAQ +  443)    /*        lhi.h: 423 */
#define   ELAQ444      (ERRLAQ +  444)    /*        lhi.h: 424 */
#define   ELAQ445      (ERRLAQ +  445)    /*        lhi.h: 425 */
#define   ELAQ446      (ERRLAQ +  446)    /*        lhi.h: 426 */
#define   ELAQ447      (ERRLAQ +  447)    /*        lhi.h: 427 */
#define   ELAQ448      (ERRLAQ +  448)    /*        lhi.h: 428 */
#define   ELAQ449      (ERRLAQ +  449)    /*        lhi.h: 429 */
#define   ELAQ450      (ERRLAQ +  450)    /*        lhi.h: 430 */
#define   ELAQ451      (ERRLAQ +  451)    /*        lhi.h: 431 */
#define   ELAQ452      (ERRLAQ +  452)    /*        lhi.h: 432 */
#define   ELAQ453      (ERRLAQ +  453)    /*        lhi.h: 433 */
#define   ELAQ454      (ERRLAQ +  454)    /*        lhi.h: 434 */
#define   ELAQ455      (ERRLAQ +  455)    /*        lhi.h: 435 */
#define   ELAQ456      (ERRLAQ +  456)    /*        lhi.h: 436 */
#define   ELAQ457      (ERRLAQ +  457)    /*        lhi.h: 437 */
#define   ELAQ458      (ERRLAQ +  458)    /*        lhi.h: 438 */
#define   ELAQ459      (ERRLAQ +  459)    /*        lhi.h: 439 */
#define   ELAQ460      (ERRLAQ +  460)    /*        lhi.h: 440 */
#define   ELAQ461      (ERRLAQ +  461)    /*        lhi.h: 441 */
#define   ELAQ462      (ERRLAQ +  462)    /*        lhi.h: 442 */
#define   ELAQ463      (ERRLAQ +  463)    /*        lhi.h: 443 */
#define   ELAQ464      (ERRLAQ +  464)    /*        lhi.h: 444 */
#define   ELAQ465      (ERRLAQ +  465)    /*        lhi.h: 445 */
#define   ELAQ466      (ERRLAQ +  466)    /*        lhi.h: 446 */
#define   ELAQ467      (ERRLAQ +  467)    /*        lhi.h: 447 */
#define   ELAQ468      (ERRLAQ +  468)    /*        lhi.h: 448 */
#define   ELAQ469      (ERRLAQ +  469)    /*        lhi.h: 449 */
#define   ELAQ470      (ERRLAQ +  470)    /*        lhi.h: 450 */
#define   ELAQ471      (ERRLAQ +  471)    /*        lhi.h: 451 */
#define   ELAQ472      (ERRLAQ +  472)    /*        lhi.h: 452 */
#define   ELAQ473      (ERRLAQ +  473)    /*        lhi.h: 453 */
#define   ELAQ474      (ERRLAQ +  474)    /*        lhi.h: 454 */
#define   ELAQ475      (ERRLAQ +  475)    /*        lhi.h: 455 */
#define   ELAQ476      (ERRLAQ +  476)    /*        lhi.h: 456 */
#define   ELAQ477      (ERRLAQ +  477)    /*        lhi.h: 457 */
#define   ELAQ478      (ERRLAQ +  478)    /*        lhi.h: 458 */
#define   ELAQ479      (ERRLAQ +  479)    /*        lhi.h: 459 */
#define   ELAQ480      (ERRLAQ +  480)    /*        lhi.h: 460 */
#define   ELAQ481      (ERRLAQ +  481)    /*        lhi.h: 461 */
#define   ELAQ482      (ERRLAQ +  482)    /*        lhi.h: 462 */
#define   ELAQ483      (ERRLAQ +  483)    /*        lhi.h: 463 */
#define   ELAQ484      (ERRLAQ +  484)    /*        lhi.h: 464 */
#define   ELAQ485      (ERRLAQ +  485)    /*        lhi.h: 465 */
#define   ELAQ486      (ERRLAQ +  486)    /*        lhi.h: 466 */
#define   ELAQ487      (ERRLAQ +  487)    /*        lhi.h: 467 */
#define   ELAQ488      (ERRLAQ +  488)    /*        lhi.h: 468 */
#define   ELAQ489      (ERRLAQ +  489)    /*        lhi.h: 469 */
#define   ELAQ490      (ERRLAQ +  490)    /*        lhi.h: 470 */
#define   ELAQ491      (ERRLAQ +  491)    /*        lhi.h: 471 */
#define   ELAQ492      (ERRLAQ +  492)    /*        lhi.h: 472 */
#define   ELAQ493      (ERRLAQ +  493)    /*        lhi.h: 473 */
#define   ELAQ494      (ERRLAQ +  494)    /*        lhi.h: 474 */
#define   ELAQ495      (ERRLAQ +  495)    /*        lhi.h: 475 */
#define   ELAQ496      (ERRLAQ +  496)    /*        lhi.h: 476 */
#define   ELAQ497      (ERRLAQ +  497)    /*        lhi.h: 477 */
#define   ELAQ498      (ERRLAQ +  498)    /*        lhi.h: 478 */
#define   ELAQ499      (ERRLAQ +  499)    /*        lhi.h: 479 */
#define   ELAQ500      (ERRLAQ +  500)    /*        lhi.h: 480 */
#define   ELAQ501      (ERRLAQ +  501)    /*        lhi.h: 481 */
#define   ELAQ502      (ERRLAQ +  502)    /*        lhi.h: 482 */
#define   ELAQ503      (ERRLAQ +  503)    /*        lhi.h: 483 */
#define   ELAQ504      (ERRLAQ +  504)    /*        lhi.h: 484 */
#define   ELAQ505      (ERRLAQ +  505)    /*        lhi.h: 485 */
#define   ELAQ506      (ERRLAQ +  506)    /*        lhi.h: 486 */
#define   ELAQ507      (ERRLAQ +  507)    /*        lhi.h: 487 */
#define   ELAQ508      (ERRLAQ +  508)    /*        lhi.h: 488 */
#define   ELAQ509      (ERRLAQ +  509)    /*        lhi.h: 489 */
#define   ELAQ510      (ERRLAQ +  510)    /*        lhi.h: 490 */
#define   ELAQ511      (ERRLAQ +  511)    /*        lhi.h: 491 */
#define   ELAQ512      (ERRLAQ +  512)    /*        lhi.h: 492 */
#define   ELAQ513      (ERRLAQ +  513)    /*        lhi.h: 493 */
#define   ELAQ514      (ERRLAQ +  514)    /*        lhi.h: 494 */
#define   ELAQ515      (ERRLAQ +  515)    /*        lhi.h: 495 */
#define   ELAQ516      (ERRLAQ +  516)    /*        lhi.h: 496 */
#define   ELAQ517      (ERRLAQ +  517)    /*        lhi.h: 497 */
#define   ELAQ518      (ERRLAQ +  518)    /*        lhi.h: 498 */
#define   ELAQ519      (ERRLAQ +  519)    /*        lhi.h: 499 */
#define   ELAQ520      (ERRLAQ +  520)    /*        lhi.h: 500 */
#define   ELAQ521      (ERRLAQ +  521)    /*        lhi.h: 501 */
#define   ELAQ522      (ERRLAQ +  522)    /*        lhi.h: 502 */
#define   ELAQ523      (ERRLAQ +  523)    /*        lhi.h: 503 */
#define   ELAQ524      (ERRLAQ +  524)    /*        lhi.h: 504 */
#define   ELAQ525      (ERRLAQ +  525)    /*        lhi.h: 505 */
#define   ELAQ526      (ERRLAQ +  526)    /*        lhi.h: 506 */
#define   ELAQ527      (ERRLAQ +  527)    /*        lhi.h: 507 */
#define   ELAQ528      (ERRLAQ +  528)    /*        lhi.h: 508 */
#define   ELAQ529      (ERRLAQ +  529)    /*        lhi.h: 509 */
#define   ELAQ530      (ERRLAQ +  530)    /*        lhi.h: 510 */
#define   ELAQ531      (ERRLAQ +  531)    /*        lhi.h: 511 */
#define   ELAQ532      (ERRLAQ +  532)    /*        lhi.h: 512 */
#define   ELAQ533      (ERRLAQ +  533)    /*        lhi.h: 513 */
#define   ELAQ534      (ERRLAQ +  534)    /*        lhi.h: 514 */
#define   ELAQ535      (ERRLAQ +  535)    /*        lhi.h: 515 */
#define   ELAQ536      (ERRLAQ +  536)    /*        lhi.h: 516 */
#define   ELAQ537      (ERRLAQ +  537)    /*        lhi.h: 517 */
#define   ELAQ538      (ERRLAQ +  538)    /*        lhi.h: 518 */
#define   ELAQ539      (ERRLAQ +  539)    /*        lhi.h: 519 */
#define   ELAQ540      (ERRLAQ +  540)    /*        lhi.h: 520 */
#define   ELAQ541      (ERRLAQ +  541)    /*        lhi.h: 521 */
#define   ELAQ542      (ERRLAQ +  542)    /*        lhi.h: 522 */
#define   ELAQ543      (ERRLAQ +  543)    /*        lhi.h: 523 */
#define   ELAQ544      (ERRLAQ +  544)    /*        lhi.h: 524 */
#define   ELAQ545      (ERRLAQ +  545)    /*        lhi.h: 525 */
#define   ELAQ546      (ERRLAQ +  546)    /*        lhi.h: 526 */
#define   ELAQ547      (ERRLAQ +  547)    /*        lhi.h: 527 */
#define   ELAQ548      (ERRLAQ +  548)    /*        lhi.h: 528 */
#define   ELAQ549      (ERRLAQ +  549)    /*        lhi.h: 529 */
#define   ELAQ550      (ERRLAQ +  550)    /*        lhi.h: 530 */
#define   ELAQ551      (ERRLAQ +  551)    /*        lhi.h: 531 */
#define   ELAQ552      (ERRLAQ +  552)    /*        lhi.h: 532 */
#define   ELAQ553      (ERRLAQ +  553)    /*        lhi.h: 533 */
#define   ELAQ554      (ERRLAQ +  554)    /*        lhi.h: 534 */
#define   ELAQ555      (ERRLAQ +  555)    /*        lhi.h: 535 */
#define   ELAQ556      (ERRLAQ +  556)    /*        lhi.h: 536 */
#define   ELAQ557      (ERRLAQ +  557)    /*        lhi.h: 537 */
#define   ELAQ558      (ERRLAQ +  558)    /*        lhi.h: 538 */
#define   ELAQ559      (ERRLAQ +  559)    /*        lhi.h: 539 */
#define   ELAQ560      (ERRLAQ +  560)    /*        lhi.h: 540 */
#define   ELAQ561      (ERRLAQ +  561)    /*        lhi.h: 541 */
#define   ELAQ562      (ERRLAQ +  562)    /*        lhi.h: 542 */
#define   ELAQ563      (ERRLAQ +  563)    /*        lhi.h: 543 */
#define   ELAQ564      (ERRLAQ +  564)    /*        lhi.h: 544 */
#define   ELAQ565      (ERRLAQ +  565)    /*        lhi.h: 545 */
#define   ELAQ566      (ERRLAQ +  566)    /*        lhi.h: 546 */
#define   ELAQ567      (ERRLAQ +  567)    /*        lhi.h: 547 */
#define   ELAQ568      (ERRLAQ +  568)    /*        lhi.h: 548 */
#define   ELAQ569      (ERRLAQ +  569)    /*        lhi.h: 549 */
#define   ELAQ570      (ERRLAQ +  570)    /*        lhi.h: 550 */
#define   ELAQ571      (ERRLAQ +  571)    /*        lhi.h: 551 */
#define   ELAQ572      (ERRLAQ +  572)    /*        lhi.h: 552 */
#define   ELAQ573      (ERRLAQ +  573)    /*        lhi.h: 553 */
#define   ELAQ574      (ERRLAQ +  574)    /*        lhi.h: 554 */
#define   ELAQ575      (ERRLAQ +  575)    /*        lhi.h: 555 */
#define   ELAQ576      (ERRLAQ +  576)    /*        lhi.h: 556 */
#define   ELAQ577      (ERRLAQ +  577)    /*        lhi.h: 557 */
#define   ELAQ578      (ERRLAQ +  578)    /*        lhi.h: 558 */
#define   ELAQ579      (ERRLAQ +  579)    /*        lhi.h: 559 */
#define   ELAQ580      (ERRLAQ +  580)    /*        lhi.h: 560 */
#define   ELAQ581      (ERRLAQ +  581)    /*        lhi.h: 561 */
#define   ELAQ582      (ERRLAQ +  582)    /*        lhi.h: 562 */
#define   ELAQ583      (ERRLAQ +  583)    /*        lhi.h: 563 */
#define   ELAQ584      (ERRLAQ +  584)    /*        lhi.h: 564 */
#define   ELAQ585      (ERRLAQ +  585)    /*        lhi.h: 565 */
#define   ELAQ586      (ERRLAQ +  586)    /*        lhi.h: 566 */
#define   ELAQ587      (ERRLAQ +  587)    /*        lhi.h: 567 */
#define   ELAQ588      (ERRLAQ +  588)    /*        lhi.h: 568 */
#define   ELAQ589      (ERRLAQ +  589)    /*        lhi.h: 569 */
#define   ELAQ590      (ERRLAQ +  590)    /*        lhi.h: 570 */
#define   ELAQ591      (ERRLAQ +  591)    /*        lhi.h: 571 */
#define   ELAQ592      (ERRLAQ +  592)    /*        lhi.h: 572 */
#define   ELAQ593      (ERRLAQ +  593)    /*        lhi.h: 573 */
#define   ELAQ594      (ERRLAQ +  594)    /*        lhi.h: 574 */
#define   ELAQ595      (ERRLAQ +  595)    /*        lhi.h: 575 */
#define   ELAQ596      (ERRLAQ +  596)    /*        lhi.h: 576 */
#define   ELAQ597      (ERRLAQ +  597)    /*        lhi.h: 577 */
#define   ELAQ598      (ERRLAQ +  598)    /*        lhi.h: 578 */
#define   ELAQ599      (ERRLAQ +  599)    /*        lhi.h: 579 */
#define   ELAQ600      (ERRLAQ +  600)    /*        lhi.h: 580 */
#define   ELAQ601      (ERRLAQ +  601)    /*        lhi.h: 581 */
#define   ELAQ602      (ERRLAQ +  602)    /*        lhi.h: 582 */
#define   ELAQ603      (ERRLAQ +  603)    /*        lhi.h: 583 */
#define   ELAQ604      (ERRLAQ +  604)    /*        lhi.h: 584 */
#define   ELAQ605      (ERRLAQ +  605)    /*        lhi.h: 585 */
#define   ELAQ606      (ERRLAQ +  606)    /*        lhi.h: 586 */
#define   ELAQ607      (ERRLAQ +  607)    /*        lhi.h: 587 */
#define   ELAQ608      (ERRLAQ +  608)    /*        lhi.h: 588 */
#define   ELAQ609      (ERRLAQ +  609)    /*        lhi.h: 589 */
#define   ELAQ610      (ERRLAQ +  610)    /*        lhi.h: 590 */
#define   ELAQ611      (ERRLAQ +  611)    /*        lhi.h: 591 */
#define   ELAQ612      (ERRLAQ +  612)    /*        lhi.h: 592 */
#define   ELAQ613      (ERRLAQ +  613)    /*        lhi.h: 593 */
#define   ELAQ614      (ERRLAQ +  614)    /*        lhi.h: 594 */
#define   ELAQ615      (ERRLAQ +  615)    /*        lhi.h: 595 */
#define   ELAQ616      (ERRLAQ +  616)    /*        lhi.h: 596 */
#define   ELAQ617      (ERRLAQ +  617)    /*        lhi.h: 597 */
#define   ELAQ618      (ERRLAQ +  618)    /*        lhi.h: 598 */
#define   ELAQ619      (ERRLAQ +  619)    /*        lhi.h: 599 */
#define   ELAQ620      (ERRLAQ +  620)    /*        lhi.h: 600 */
#define   ELAQ621      (ERRLAQ +  621)    /*        lhi.h: 601 */
#define   ELAQ622      (ERRLAQ +  622)    /*        lhi.h: 602 */
#define   ELAQ623      (ERRLAQ +  623)    /*        lhi.h: 603 */
#define   ELAQ624      (ERRLAQ +  624)    /*        lhi.h: 604 */
#define   ELAQ625      (ERRLAQ +  625)    /*        lhi.h: 605 */
#define   ELAQ626      (ERRLAQ +  626)    /*        lhi.h: 606 */
#define   ELAQ627      (ERRLAQ +  627)    /*        lhi.h: 607 */
#define   ELAQ628      (ERRLAQ +  628)    /*        lhi.h: 608 */
/* lhi_h_001.main_7: Handling of instream and outstream */
#define   ELAQ629      (ERRLAQ +  629)    /*        lhi.h: 609 */
#define   ELAQ630      (ERRLAQ +  630)    /*        lhi.h: 610 */
#define   ELAQ631      (ERRLAQ +  631)    /*        lhi.h: 611 */
#define   ELAQ632      (ERRLAQ +  632)    /*        lhi.h: 612 */
/* lhi_h_001.main_10: Added new error codes*/
#define   ELAQ633      (ERRLAQ +  633)    /*        lhi.c:1081*/
#define   ELAQ634      (ERRLAQ +  634)    /*        lhi.c:1083*/
#define   ELAQ635      (ERRLAQ +  635)    /*        lhi.c:2127*/
#define   ELAQ636      (ERRLAQ +  636)    /*        lhi.c:2125*/
/* lhi_h_001.main_11: Added new error codes */
#define   ELAQ637      (ERRLAQ +  637)    /*        lhi.c:2125*/
#define   ELAQ638      (ERRLAQ +  638)    /*        lhi.c:2125*/
#define   ELAQ639      (ERRLAQ +  639)    /*        lhi.c:2125*/
#define   ELAQ640      (ERRLAQ +  640)    /*        lhi.c:2125*/
#define   ELAQ641      (ERRLAQ +  641)    /*        lhi.c:2125*/
#define   ELAQ642      (ERRLAQ +  642)    /*        lhi.c:2125*/
#define   ELAQ643      (ERRLAQ +  643)    /*        lhi.c:2125*/
#define   ELAQ644      (ERRLAQ +  644)    /*        lhi.c:2125*/
#define   ELAQ645      (ERRLAQ +  645)    /*        lhi.c:2125*/
#define   ELAQ646      (ERRLAQ +  646)    /*        lhi.c:2125*/

#endif /* __LAQH__ */
