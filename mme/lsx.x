/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter SGD Interface

     Type:     C header file

     Desc:     Structures, variables, typedefs and prototypes
               required at the management interface.

     File:     lsx.x

     Sid:

     Prg:      rakesh kumar suman

*********************************************************************21*/

#ifndef __LSXX__
#define __LSXX__

#ifdef __cplusplus
extern "C" {
#endif

/* General configuration.
 */
typedef struct sxGenCfg
{
   //Pst          lmPst;          /* for layer manager */
   //Pst          mmePst;

   Region       initRegion;
   Pool         initPool;
} SxGenCfg;

/* General statistics.
 */
typedef struct sxGenSts
{
   StsCntr      numErrors;     /* num of errors */
} SxGenSts;

typedef struct sxSta
{
   State        state;          /* DNS state */
} SxSta;

/* Unsolicited status indication.
 */
typedef struct sxAlarmInfo
{
   SpId         spId;           /* sap Id */
   U8           type;           /* which member of inf is present */
   union
   {
      State     state;          /* TSAP state */
      State     conState;       /* connection state */
      Mem       mem;            /* region/pool (resource related) */
      U8        parType;        /* parameter type */
   } inf;

} SxAlarmInfo;

#ifdef DEBUGP
/* Debug control.
 */
typedef struct sxDbgCntrl
{
   U32          dbgMask;        /* Debug mask */
} SxDbgCntrl;
#endif


/* Trace control.
 */
typedef struct sxTrcCntrl
{
   SpId         sapId;          /* sap Id */
   S16          trcLen;         /* length to trace */

} SxTrcCntrl;



/* Configuration.
 */
typedef struct sxCfg
{
   union
   {
      SxGenCfg  sxGen;          /* general configuration */
   } s;

} SxCfg;


/* Statistics.
 */
typedef struct sxSts
{
   DateTime     dt;             /* date and time */
   Duration     dura;           /* duration */

   union
   {
      SxGenSts  genSts;         /* general statistics */
   } s;

} SxSts;

/* Solicited status.
 */
typedef struct sxSsta
{
   DateTime     dt;             /* date and time */

   union
   {
      SystemId  sysId;          /* system id */
      SxSta  sxSta;             /* diameter status */
   } s;

} SxSsta;

/* Unsolicited status.
 */
typedef struct sxUsta
{
   CmAlarm      alarm;          /* alarm */
   SxAlarmInfo  info;           /* alarm information */

} SxUsta;


/* Trace.
 */
typedef struct sxTrc
{
   DateTime     dt;            /* date and time */
   U16          evnt;          /* event */
} SxTrc;

/* Control.
 */
typedef struct sxCntrl
{
   DateTime     dt;             /* date and time */
   U8           action;         /* action */
   U8           subAction;      /* sub action */

   union
   {
      SxTrcCntrl        trcDat;         /* trace length */
      ProcId            dstProcId;      /* destination procId */
      Route             route;          /* route */
      Priority          priority;       /* priority */

#ifdef DEBUGP
      SxDbgCntrl        sxDbg;          /* debug printing control */
#endif

   } ctlType;

} SxCntrl;

/* Management.
 */
typedef struct sxMngmt
{
   Header       hdr;            /* header */
   CmStatus     cfm;            /* response status/confirmation */

   union
   {
      SxCfg     cfg;            /* configuration */
      SxSts     sts;            /* statistics */
      SxSsta    ssta;           /* solicited status */
      SxUsta    usta;           /* unsolicited status */
      SxTrc     trc;            /* trace */
      SxCntrl   cntrl;          /* control */

   } t;
} SxMngmt;


/* Layer management interface primitive types.
 */
typedef S16  (*LsxCfgReq)       ARGS((Pst *pst, SxMngmt *cfg));
typedef S16  (*LsxCfgCfm)       ARGS((Pst *pst, SxMngmt *cfg));
typedef S16  (*LsxCntrlReq)     ARGS((Pst *pst, SxMngmt *cntrl));
typedef S16  (*LsxCntrlCfm)     ARGS((Pst *pst, SxMngmt *cntrl));
typedef S16  (*LsxStsReq)       ARGS((Pst *pst, Action action,
                                      SxMngmt *sts));
typedef S16  (*LsxStsCfm)       ARGS((Pst *pst, SxMngmt *sts));
typedef S16  (*LsxStaReq)       ARGS((Pst *pst, SxMngmt *sta));
typedef S16  (*LsxStaInd)       ARGS((Pst *pst, SxMngmt *sta));
typedef S16  (*LsxStaCfm)       ARGS((Pst *pst, SxMngmt *sta));
typedef S16  (*LsxTrcInd)       ARGS((Pst *pst, SxMngmt *trc,
                                      Buffer *mBuf));

//typedef S16 (*LsxInitAttachSgw) ARGS((Pst *pst, DnsQueryData *qd));

/* Layer management interface primitives.
 */
//#ifdef SX
EXTERN S16  SxMiLsxCfgReq       ARGS((Pst *pst, SxMngmt *cfg));
EXTERN S16  SxMiLsxCfgCfm       ARGS((Pst *pst, SxMngmt *cfg));
EXTERN S16  SxMiLsxCntrlReq     ARGS((Pst *pst, SxMngmt *cntrl));
EXTERN S16  SxMiLsxCntrlCfm     ARGS((Pst *pst, SxMngmt *cntrl));
EXTERN S16  SxMiLsxStsReq       ARGS((Pst *pst, Action action,
                                      SxMngmt *sts));
EXTERN S16  SxMiLsxStsCfm       ARGS((Pst *pst, SxMngmt *sts));
EXTERN S16  SxMiLsxStaReq       ARGS((Pst *pst, SxMngmt *sta));
EXTERN S16  SxMiLsxStaCfm       ARGS((Pst *pst, SxMngmt *sta));
EXTERN S16  SxMiLsxStaInd       ARGS((Pst *pst, SxMngmt *sta));
EXTERN S16  SxMiLsxTrcInd       ARGS((Pst *pst, SxMngmt *trc,
                                      Buffer *mBuf));
//#endif /* SX */

#ifdef SM
EXTERN S16  SmMiLsxCfgReq       ARGS((Pst *pst, SxMngmt *cfg));
EXTERN S16  SmMiLsxCfgCfm       ARGS((Pst *pst, SxMngmt *cfg));
EXTERN S16  SmMiLsxCntrlReq     ARGS((Pst *pst, SxMngmt *cntrl));
EXTERN S16  SmMiLsxCntrlCfm     ARGS((Pst *pst, SxMngmt *cntrl));
EXTERN S16  SmMiLsxStsReq       ARGS((Pst *pst, Action action,
                                      SxMngmt *sts));
EXTERN S16  SmMiLsxStsCfm       ARGS((Pst *pst, SxMngmt *sts));
EXTERN S16  SmMiLsxStaReq       ARGS((Pst *pst, SxMngmt *sta));
EXTERN S16  SmMiLsxStaInd       ARGS((Pst *pst, SxMngmt *sta));
EXTERN S16  SmMiLsxStaCfm       ARGS((Pst *pst, SxMngmt *sta));
EXTERN S16  SmMiLsxTrcInd       ARGS((Pst *pst, SxMngmt *trc,
                                      Buffer *mBuf));
#endif /* SM */



/* Packing and unpacking functions.
 */
EXTERN S16  cmPkLsxCfgReq       ARGS((Pst *pst, SxMngmt *cfg));
EXTERN S16  cmPkLsxCfgCfm       ARGS((Pst *pst, SxMngmt *cfg));
EXTERN S16  cmPkLsxCntrlReq     ARGS((Pst *pst, SxMngmt *cntrl));
EXTERN S16  cmPkLsxCntrlCfm     ARGS((Pst *pst, SxMngmt *cntrl));
EXTERN S16  cmPkLsxStsReq       ARGS((Pst *pst, Action action, SxMngmt *sts));
EXTERN S16  cmPkLsxStsCfm       ARGS((Pst *pst, SxMngmt *sts));
EXTERN S16  cmPkLsxStaReq       ARGS((Pst *pst, SxMngmt *sta));
EXTERN S16  cmPkLsxStaInd       ARGS((Pst *pst, SxMngmt *sta));
EXTERN S16  cmPkLsxTrcInd       ARGS((Pst *pst, SxMngmt *trc, Buffer *mBuf));
EXTERN S16  cmPkLsxStaCfm       ARGS((Pst *pst, SxMngmt *sta));

EXTERN S16  cmUnpkLsxCfgReq     ARGS((LsxCfgReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsxCfgCfm     ARGS((LsxCfgCfm func, Pst *pst,
                                    Buffer *mBuf));
EXTERN S16  cmUnpkLsxCntrlReq   ARGS((LsxCntrlReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsxCntrlCfm   ARGS((LsxCntrlCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsxStsReq     ARGS((LsxStsReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsxStsCfm     ARGS((LsxStsCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsxStaReq     ARGS((LsxStaReq func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsxStaInd     ARGS((LsxStaInd func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsxStaCfm     ARGS((LsxStaCfm func, Pst *pst,
                                      Buffer *mBuf));
EXTERN S16  cmUnpkLsxTrcInd     ARGS((LsxTrcInd func, Pst *pst,
                                      Buffer *mBuf));
/* Layer manager activation functions.
 */

#ifdef SS_MULTIPLE_PROCS
EXTERN S16 sxActvInit       ARGS((ProcId procId,
                                  Ent entity,
                                  Inst inst,
                                  Region region,
                                  Reason reason,
                                  Void **xxCb));
#else /* SS_MULTIPLE_PROCS */

EXTERN S16  sxActvInit          ARGS((Ent ent, Inst inst,
                                      Region region, Reason reason));
#endif /* SS_MULTIPLE_PROCS */
EXTERN S16  sxActvTsk           ARGS((Pst *pst, Buffer *mBuf));


#ifdef __cplusplus
}
#endif

#endif /* __LSXX__ */


/********************************************************************30**

         End of file:     

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************90**

    ver       pat     init                  description
----------- --------- ---- -----------------------------------------------
1.1         ---       rks   1. initial release.
*********************************************************************91*/

