/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

/********************************************************************20**

     Name:     Diameter SGD Convergence Layer

     Type:     C source file

     Desc:     Management interface.

     File:     sx_ptmi.c

     Sid:

     Prg:      rakesh kumar suman

*********************************************************************21*/

/*

The following functions are provided in this file:
     SxMiLsxCfgCfm      Configuration Confirm
     SxMiLsxCntrlCfm    Control Confirm
     SxMiLsxStsCfm      Statistics Confirm
     SxMiLsxStaInd      Status Indication
     SxMiLsxStaCfm      Status Confirm
     SxMiLsxTrcInd      Trace Indication

It should be noted that not all of these functions may be required
by a particular layer management service user.

It is assumed that the following functions are provided in TUCL:
     SxMiLsxCfgReq      Configuration Request
     SxMiLsxCntrlReq    Control Request
     SxMiLsxStsReq      Statistics Request
     SxMiLsxStaReq      Status Request

*/

/* header include files (.h) */

#include "envopt.h"             /* environment options */
#include "envdep.h"             /* environment dependent */
#include "envind.h"             /* environment independent */

#include "gen.h"                /* general layer */
#include "ssi.h"                /* system services interface */

/* external headers */
#ifdef HI_TLS
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/ssl.h>
#include <openssl/x509v3.h>
#endif

#include "cm_hash.h"            /* common hash list */
#include "cm_llist.h"           /* common linked list */
#include "cm5.h"                /* common timer */
#include "cm_inet.h"            /* common sockets */
#include "cm_tpt.h"             /* common transport defines */

/* header/extern include files (.x) */

#include "gen.x"                /* general layer */
#include "ssi.x"                /* system services interface */

#include "cm_hash.x"            /* common hashing */
#include "cm_llist.x"           /* common linked list */
#include "cm_lib.x"             /* common library */
#include "cm5.x"                /* common timer */
#include "cm_inet.x"            /* common sockets */
#include "cm_tpt.x"             /* common transport typedefs */

#include "lsx.h"
#include "lsx.x"
#include "sx.h"
#include "sx.x"

/* local defines */

#define MAXSXMI 2

#ifndef LCSXMILSX
#define PTHIMILAQ
#else
#ifndef SM
#define PTAQMILAQ
#endif
#endif


#ifdef PTSXMILSX
/* declaration of portable functions */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* PTSXMILSX */


/*
  The following matrices define the mapping between the primitives
  called by the upper interface of TUCL and the corresponding
  primitives of the TCP UDP Convergence Layer service user(s).

  The parameter MAXHIMI defines the maximum number of service
  users on top of TUCL. There is an array of functions per
  primitive invoked by TUCL. Every array is MAXHIMI long (i.e.
  there are as many functions as the number of service users).

  The dispatching is performed by the configurable variable:
  selector. The selector is configured on a per SAP basis.

  The selectors are:

   0 - loosely coupled (#define LCHIMILHI) 1 - LHI (#define SM)

*/



/* Configuration Confirm */

PRIVATE LsxCfgCfm SxMiLsxCfgCfmMt[MAXSXMI] =
{
#ifdef LCSXMILSX
   cmPkLsxCfgCfm,          /* 0 - loosely coupled  */
#else
   PtMiLsxCfgCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLsxCfgCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsxCfgCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Control Confirm */

PRIVATE LsxCntrlCfm SxMiLsxCntrlCfmMt[MAXSXMI] =
{
#ifdef LCSXMILSX
   cmPkLsxCntrlCfm,          /* 0 - loosely coupled  */
#else
   PtMiLsxCntrlCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLsxCntrlCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsxCntrlCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Statistics Confirm */

PRIVATE LsxStsCfm SxMiLsxStsCfmMt[MAXSXMI] =
{
#ifdef LCSXMILSX
   cmPkLsxStsCfm,          /* 0 - loosely coupled  */
#else
   PtMiLsxStsCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLsxStsCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsxStsCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Status Indication */

PRIVATE LsxStaInd SxMiLsxStaIndMt[MAXSXMI] =
{
#ifdef LCSXMILSX
   cmPkLsxStaInd,          /* 0 - loosely coupled  */
#else
   PtMiLsxStaInd,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLsxStaInd,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsxStaInd,          /* 1 - tightly coupled, portable */
#endif
};

/* Status Confirm */

PRIVATE LsxStaCfm SxMiLsxStaCfmMt[MAXSXMI] =
{
#ifdef LCSXMILSX
   cmPkLsxStaCfm,          /* 0 - loosely coupled  */
#else
   PtMiLsxStaCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLsxStaCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsxStaCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Trace Indication */

PRIVATE LsxTrcInd SxMiLsxTrcIndMt[MAXSXMI] =
{
#ifdef LCSXMILSX
   cmPkLsxTrcInd,          /* 0 - loosely coupled  */
#else
   PtMiLsxTrcInd,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLsxTrcInd,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsxTrcInd,          /* 1 - tightly coupled, portable */
#endif
};


/*
*     Layer Management Interface Functions
*/


/*
*
*       Fun:   Configuration confirm
*
*       Desc:  This function is used to send a configuration confirm
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sx_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SxMiLsxCfgCfm
(
Pst     *pst,            /* post structure */
SxMngmt *cfg             /* configuration */
)
#else
PUBLIC S16 SxMiLsxCfgCfm(pst, cfg)
Pst     *pst;            /* post structure */
SxMngmt *cfg;            /* configuration */
#endif
{
   TRC3(SxMiLsxCfgCfm)

   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf,
         "SxMiLsxCfgCfm(pst, cfg (0x%p))\n", (Ptr)cfg));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SxMiLsxCfgCfmMt[pst->selector])(pst, cfg));
} /* end of SxMiLsxCfgCfm */


/*
*
*       Fun:   Control confirm
*
*       Desc:  This function is used to send a control confirm
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sx_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SxMiLsxCntrlCfm
(
Pst     *pst,            /* post structure */
SxMngmt *cntrl           /* control */
)
#else
PUBLIC S16 SxMiLsxCntrlCfm(pst, cntrl)
Pst     *pst;            /* post structure */
SxMngmt *cntrl;          /* control */
#endif
{
   TRC3(SxMiLsxCntrlCfm)

   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf,
         "SxMiLsxCntrlCfm(pst, cntrl (0x%p))\n", (Ptr)cntrl));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SxMiLsxCntrlCfmMt[pst->selector])(pst, cntrl));
} /* end of SxMiLsxCntrlCfm */


/*
*
*       Fun:   Status Indication
*
*       Desc:  This function is used to indicate the status
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sx_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SxMiLsxStaInd
(
Pst     *pst,            /* post structure */
SxMngmt *usta             /* unsolicited status */
)
#else
PUBLIC S16 SxMiLsxStaInd(pst, usta)
Pst     *pst;            /* post structure */
SxMngmt *usta;            /* unsolicited status */
#endif
{
   TRC3(SxMiLsxStaInd)

   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf,
          "SxMiLsxStaInd(pst, usta (0x%p))\n", (Ptr)usta));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SxMiLsxStaIndMt[pst->selector])(pst, usta));
} /* end of SxMiLsxStaInd */


/*
*
*       Fun:   Status Confirm
*
*       Desc:  This function is used to return the status
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sx_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SxMiLsxStaCfm
(
Pst     *pst,            /* post structure */
SxMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 SxMiLsxStaCfm(pst, sta)
Pst     *pst;            /* post structure */
SxMngmt *sta;             /* solicited status */
#endif
{
   TRC3(SxMiLsxStaCfm)

   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf,
          "SxMiLsxStaCfm(pst, sta (0x%p))\n", (Ptr)sta));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SxMiLsxStaCfmMt[pst->selector])(pst, sta));
} /* end of SxMiLsxStaCfm */


/*
*
*       Fun:   Statistics Confirm
*
*       Desc:  This function is used to return the statistics
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sx_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SxMiLsxStsCfm
(
Pst     *pst,                /* post structure */
SxMngmt *sts                 /* statistics */
)
#else
PUBLIC S16 SxMiLsxStsCfm(pst, sts)
Pst     *pst;                /* post structure */
SxMngmt *sts;                /* statistics */
#endif
{
   TRC3(SxMiLsxStsCfm)

   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf,
         "SxMiLsxStsCfm(pst, sts (0x%p))\n", (Ptr)sts));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SxMiLsxStsCfmMt[pst->selector])(pst, sts));
} /* end of SxMiLsxStsCfm */


/*
*
*       Fun:   Trace Indication
*
*       Desc:  This function is used to indicate the trace
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sx_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SxMiLsxTrcInd
(
Pst     *pst,            /* post structure */
SxMngmt *trc,             /* unsolicited status */
Buffer  *mBuf              /* message buffer */
)
#else
PUBLIC S16 SxMiLsxTrcInd(pst, trc, mBuf)
Pst     *pst;            /* post structure */
SxMngmt *trc;             /* unsolicited status */
Buffer  *mBuf;             /* message buffer */
#endif
{
   TRC3(SxMiLsxTrcInd)

   SXDBGP(DBGMASK_MI, (sxCb.init.prntBuf,
         "SxMiLsxTrcInd(pst, trc (0x%p))\n", (Ptr)trc));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SxMiLsxTrcIndMt[pst->selector])(pst, trc, mBuf));
} /* end of SxMiLsxTrcInd */


/********************************************************************30**

         End of file:     

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision History:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      rks   1. initial release.
*********************************************************************91*/

