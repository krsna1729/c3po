/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter Convergence Layer

     Type:     C source file

     Desc:     freeDiameter initialization.

     File:     aqfd_ul.c

     Sid:

     Prg:      bw

*********************************************************************21*/

/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */
#include "cm_os.h"

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */
#include "cm_os.x"

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"

#include "lsy.h"
#include "lsy.x"

#include "aqfd.h"
#include "aqfd.x"

#include "sy.h"
#include "sy.x"
#include "sy_err.h"

PRIVATE Void _aqfd_ula_cb ARGS((void * data, struct msg ** msg));

PRIVATE S16 aqfdParseLCSInfo ARGS((struct avp * lcs_info_avp, SyLCSInfo *lcs_info));
PRIVATE S16 aqfdParseMOLR ARGS((struct avp * mo_lr_avp, PTR *node));
PRIVATE S16 aqfdParseLCSPrivacyException ARGS((struct avp * lcs_pe_avp, PTR *node));
PRIVATE S16 aqfdParseExternalClient ARGS((struct avp * ec_avp, PTR *node));
PRIVATE S16 aqfdParseServiceType ARGS((struct avp * st_avp, PTR *node));
PRIVATE S16 aqfdParseTeleserviceList ARGS((struct avp * tslist_avp, SyTeleserviceList *tslist));
PRIVATE S16 aqfdParseCallBarringInfo ARGS((struct avp * cbi_avp, PTR *node));
PRIVATE S16 aqfdParseAMBR ARGS((struct avp * ambr_avp, SyAMBR *ambr));
PRIVATE S16 aqfdParseAPNConfigurationProfile ARGS((struct avp * apncp_avp, SyAPNConfigurationProfile *apncp));
PRIVATE S16 aqfdParseAPNConfiguration ARGS((struct avp * apn_cfg_avp, PTR *node));
PRIVATE S16 aqfdParseEPSSubscribedQoSProfile ARGS((struct avp * eps_avp, SyEPSSubscribedQoSProfile *eps));
PRIVATE S16 aqfdParseAllocationRetentionPriority ARGS((struct avp * arp_avp, SyAllocationRetentionPriority *arp));
PRIVATE S16 aqfdParseMIP6AgentInfo ARGS((struct avp * mai_avp, SyMIP6AgentInfo *mai));
PRIVATE S16 aqfdParseMIPHomeAgentHost ARGS((struct avp * mhah_avp, SyMIPHomeAgentHost *mhah));
PRIVATE S16 aqfdParseSpecificAPNInfo ARGS((struct avp * sai_avp, PTR *node));
PRIVATE S16 aqfdParseWLANoffloadability ARGS((struct avp * wlan_avp, SyWLANoffloadability *wlan));
PRIVATE S16 aqfdParseTraceData ARGS((struct avp * td_avp, SyTraceData *td));
PRIVATE S16 aqfdParseMDTConfiguration ARGS((struct avp * mdtc_avp, SyMDTConfiguration *mdtc));
PRIVATE S16 aqfdParseAreaScope ARGS((struct avp * as_avp, SyAreaScope *as));
PRIVATE S16 aqfdParseGPRSSubscriptionData ARGS((struct avp * gprs_avp, SyGPRSSubscriptionData *gprs));
PRIVATE S16 aqfdParsePDPContext ARGS((struct avp * pc_avp, PTR *node));
PRIVATE S16 aqfdParseCSGSubscriptionData ARGS((struct avp * csgsd_avp, PTR *node));
PRIVATE S16 aqfdParseProSeSubscriptionData ARGS((struct avp * pssd_avp, SyProSeSubscriptionData *pssd));
PRIVATE S16 aqfdParseAdjacentAccessRestrictionData ARGS((struct avp * aard_avp, PTR *node));
PRIVATE S16 aqfdParseIMSIGroupId ARGS((struct avp * imsigi_avp, PTR *node));
PRIVATE S16 aqfdParseAESECommunicationPattern ARGS((struct avp * aesecp_avp, PTR *node));
PRIVATE S16 aqfdParseCommunicationPatternSet ARGS((struct avp * cps_avp, PTR *node));
PRIVATE S16 aqfdParseScheduledCommunicationTime ARGS((struct avp * sct_avp, PTR *node));
PRIVATE S16 aqfdParseMonitoringEventConfiguration ARGS((struct avp * mec_avp, PTR *node));
PRIVATE S16 aqfdParseUEReachabilityConfiguration ARGS((struct avp * uerc_avp, SyUEReachabilityConfiguration *uerc));
PRIVATE S16 aqfdParseLocationInformationConfiguration ARGS((struct avp * lic_avp, SyLocationInformationConfiguration *lic));
PRIVATE S16 aqfdParseEmergencyInfo ARGS((struct avp * ei_avp, SyEmergencyInfo *ei));
PRIVATE S16 aqfdParseV2XSubscriptionData ARGS((struct avp * v2xsd_avp, SyV2XSubscriptionData *v2xsd));
PRIVATE S16 aqfdParseeDRXCycleLength ARGS((struct avp * cl_avp, PTR *node));

PRIVATE Void aqfdDestroyTeleserviceList ARGS(( SyTeleserviceList *p ));
PRIVATE Void aqfdDestroyLCSInfo ARGS(( SyLCSInfo *p ));
PRIVATE Void aqfdDestroyLCSPrivacyException ARGS(( SyLCSPrivacyException *p ));
PRIVATE Void aqfdDestroyExternalClient ARGS(( SyExternalClient *p ));
PRIVATE Void aqfdDestroyServiceType ARGS(( SyServiceType *p ));
PRIVATE Void aqfdDestroyMOLR ARGS(( SyMOLR *p ));
PRIVATE Void aqfdDestroyCallBarringInfo ARGS(( SyCallBarringInfo *p ));
PRIVATE Void aqfdDestroyAMBR ARGS(( SyAMBR *p ));
PRIVATE Void aqfdDestroyAPNConfigurationProfile ARGS(( SyAPNConfigurationProfile *p ));
PRIVATE Void aqfdDestroyAPNConfiguration ARGS(( SyAPNConfiguration *p ));
PRIVATE Void aqfdDestroyEPSSubscribedQoSProfile ARGS(( SyEPSSubscribedQoSProfile *p ));
PRIVATE Void aqfdDestroyMIP6AgentInfo ARGS(( SyMIP6AgentInfo *p ));
PRIVATE Void aqfdDestroyAMBR ARGS(( SyAMBR *p ));
PRIVATE Void aqfdDestroyWLANoffloadability ARGS(( SyWLANoffloadability *p ));
PRIVATE Void aqfdDestroyAllocationRetentionPriority ARGS(( SyAllocationRetentionPriority *p ));
PRIVATE Void aqfdDestroyMIPHomeAgentHost ARGS(( SyMIPHomeAgentHost *p ));
PRIVATE Void aqfdDestroySpecificAPNInfo ARGS(( SySpecificAPNInfo *p ));
PRIVATE Void aqfdDestroyTraceData ARGS(( SyTraceData *p ));
PRIVATE Void aqfdDestroyMDTConfiguration ARGS(( SyMDTConfiguration *p ));
PRIVATE Void aqfdDestroyAreaScope ARGS(( SyAreaScope *p ));
PRIVATE Void aqfdDestroyGPRSSubscriptionData ARGS(( SyGPRSSubscriptionData *p ));
PRIVATE Void aqfdDestroyPDPContext ARGS(( SyPDPContext *p ));
PRIVATE Void aqfdDestroyCSGSubscriptionData ARGS(( SyCSGSubscriptionData *p ));
PRIVATE Void aqfdDestroyProSeSubscriptionData ARGS(( SyProSeSubscriptionData *p ));
PRIVATE Void aqfdDestroyAdjacentAccessRestrictionData ARGS(( SyAdjacentAccessRestrictionData *p ));
PRIVATE Void aqfdDestroyIMSIGroupId ARGS(( SyIMSIGroupId *p ));
PRIVATE Void aqfdDestroyAESECommunicationPattern ARGS(( SyAESECommunicationPattern *p ));
PRIVATE Void aqfdDestroyCommunicationPatternSet ARGS(( SyCommunicationPatternSet *p ));
PRIVATE Void aqfdDestroyScheduledCommunicationTime ARGS(( SyScheduledCommunicationTime *p ));
PRIVATE Void aqfdDestroyMonitoringEventConfiguration ARGS(( SyMonitoringEventConfiguration *p ));
PRIVATE Void aqfdDestroyUEReachabilityConfiguration ARGS(( SyUEReachabilityConfiguration *p ));
PRIVATE Void aqfdDestroyLocationInformationConfiguration ARGS(( SyLocationInformationConfiguration *p ));
PRIVATE Void aqfdDestroyEmergencyInfo ARGS(( SyEmergencyInfo *p ));
PRIVATE Void aqfdDestroyV2XSubscriptionData ARGS(( SyV2XSubscriptionData *p ));
PRIVATE Void aqfdDestroyeDRXCycleLength ARGS(( SyeDRXCycleLength *p ));

/*
 *  Thread local Pst related variables and init routine
 */
static __thread int _pstInitialized;
static __thread Pst _pst;

PRIVATE Void _initPst ARGS((void));

PRIVATE Void _initPst()
{
   if (!_pstInitialized)
   {
      memcpy( &_pst, &aqCb.cfg.syPst, sizeof(_pst) ); 
      _pstInitialized = 1;
   }
}

/*
*
*       Fun:    aqfdSendULR
*
*       Desc:   Perform general configuration of freeDiameter. Must be done
*               after aqActvInit() is called, but before any other
*               interaction with Diameter.
*
*       Ret:    LCM_REASON_NOT_APPL                 - ok
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*      The Update-Location-Request (ULR) command, indicated by the Command-Code
*      field set to 316 and the "R" bit set in the Command Flags field, is sent
*      from MME or SGSN to HSS.
*
*      < Update-Location-Request> ::=	< Diameter Header: 316, REQ, PXY, 16777251 >
*        < Session-Id >
*        [ DRMP ]
*        [ Vendor-Specific-Application-Id ]
*        { Auth-Session-State }
*        { Origin-Host }
*        { Origin-Realm }
*        [ Destination-Host ]
*        { Destination-Realm }
*        { User-Name }
*        [ OC-Supported-Features ]
*       *[ Supported-Features ]
*        [ Terminal-Information ]
*        { RAT-Type }
*        { ULR-Flags }
*        [ UE-SRVCC-Capability ]
*        { Visited-PLMN-Id }
*        [ SGSN-Number ] 
*        [ Homogeneous-Support-of-IMS-Voice-Over-PS-Sessions ] 
*        [ GMLC-Address ]
*       *[ Active-APN ] 
*        [ Equivalent-PLMN-List ]
*        [ MME-Number-for-MT-SMS ]
*        [ SMS-Register-Request ]
*        [ SGs-MME-Identity ]
*        [ Coupled-Node-Diameter-ID ]
*        [ Adjacent-PLMNs ]
*        [ Supported-Services ]
*       *[ AVP ]
*       *[ Proxy-Info ]
*       *[ Route-Record ]
*/
#ifdef ANSI
PUBLIC S16 aqfdSendULR
(
VbMmeUeCb *ueCb,
Bool skip_sub_data,
Bool initial_attach,
U8 event
)
#else
PUBLIC S16 aqfdSendULR(ueCb, skip_sub_data, initial_attach, event)
VbMmeUeCb *ueCb;
Bool skip_sub_data;
Bool initial_attach;
U8 event;
#endif
{
   struct msg *msg;
   S8 imsi[VB_HSS_IMSI_LEN + 1];
   U32 ulr_flags = 0;

#ifdef PERFORMANCE_TIMING
   STIMER_GET_CURRENT_TP(UeAttachTimers[ueCb->UeTimerIdx].ulr_time);
#endif

   aqfdImsi2Str(ueCb->ueCtxt.ueImsi, ueCb->ueCtxt.ueImsiLen, imsi);

   /* create new session id, if needed */
   if (!*ueCb->ueCtxt.ueHssCtxt.sessId)
      AQCHECK_FCT_2( aqfdCreateSessionId(ueCb) );

   /* construct the message */
   AQCHECK_MSG_NEW( aqDict.cmdULR, msg );

   /* Session-Id */
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_Session_Id, msg, MSG_BRW_LAST_CHILD, ueCb->ueCtxt.ueHssCtxt.sessId );

   /* Auth-Session-State */
   AQCHECK_MSG_ADD_AVP_S32( aqDict.avp_Auth_Session_State, msg, MSG_BRW_LAST_CHILD, 1 );

   /* Origin-Host & Origin-Realm */
   AQCHECK_MSG_ADD_ORIGIN( msg );

   /* Destination-Host & Destination-Realm */
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_Destination_Host, msg, MSG_BRW_LAST_CHILD, vbSmCb.cfgCb.hssHost );
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_Destination_Realm, msg, MSG_BRW_LAST_CHILD, vbSmCb.cfgCb.hssRealm );

   /* User-Name (IMSI) */
   AQCHECK_MSG_ADD_AVP_STR( aqDict.avp_User_Name, msg, MSG_BRW_LAST_CHILD, imsi );

   /* RAT-Type */
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_RAT_Type, msg, MSG_BRW_LAST_CHILD, S6A_RAT_TYPE_EUTRAN );

   /* ULR-Flags */
   ulr_flags = 
      S6A_ULR_FLAGS_S6AS6D_INDICATOR |
      ( skip_sub_data ? S6A_ULR_FLAGS_SKIP_SUBSCRIBER_DATA : 0 ) |
      ( initial_attach ? S6A_ULR_FLAGS_INITIAL_ATTACH_INDICATOR : 0 );
   AQCHECK_MSG_ADD_AVP_U32( aqDict.avp_ULR_Flags, msg, MSG_BRW_LAST_CHILD, ulr_flags );

   /* Visited-PLMN-Id */
   AQCHECK_MSG_ADD_AVP_OSTR( aqDict.avp_Visited_PLMN_Id, msg, MSG_BRW_LAST_CHILD, ueCb->ueCtxt.tai.plmnId.plmnId, 3 );

   AQFD_DUMP_MESSAGE(msg);

   /* send the message */
   AQCHECK_MSG_SEND( &msg, _aqfd_ula_cb, (Void*)event );
 
   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfd_ula_cb
*
*       Desc:   Update-Location-Answer callback
*
*       Ret:    0
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       < Update-Location-Answer> ::=	< Diameter Header: 316, PXY, 16777251 >
*          < Session-Id >
*          [ DRMP ]
*          [ Vendor-Specific-Application-Id ]
*          [ Result-Code ]
*          [ Experimental-Result ] 
*          [ Error-Diagnostic ] 
*          { Auth-Session-State }
*          { Origin-Host }
*          { Origin-Realm }
*          [ OC-Supported-Features ]
*          [ OC-OLR ]
*         *[ Load ]
*         *[ Supported-Features ]
*          [ ULA-Flags ]
*          [ Subscription-Data ]
*         *[ Reset-ID ]
*         *[ AVP ]
*          [ Failed-AVP ]
*         *[ Proxy-Info ]
*         *[ Route-Record ]
*/
#ifdef ANSI
PUBLIC int aqfd_ula_cb
(
struct msg ** msg,
struct avp * pavp,
struct session * sess,
void * data,
enum disp_action * act
)
#else
PUBLIC int aqfd_ula_cb(msg, pavp, sess, data, act)
struct msg ** msg;
struct avp * pavp;
struct session * sess;
void * data;
enum disp_action * act;
#endif
{
   fd_msg_free( *msg );
   *msg = NULL;
   return 0;
}

#ifdef ANSI
PRIVATE Void _aqfd_ula_cb
(
void * data,
struct msg ** msg
)
#else
PRIVATE Void _aqfd_ula_cb(data, msg)
void * data;
struct msg ** msg;
#endif
{
   S16 ret;
   S8 imsi[VB_HSS_IMSI_LEN + 1];
   struct msg   *ans = NULL;
   struct msg   *qry = NULL;
   struct avp   *avp = NULL;
   SyULA     	*ula = NULL;
#ifdef PERFORMANCE_TIMING
   stimer_t     tmp_time;
   stimer_t     tmp_cb_start;
#endif

   ans = *msg;

#ifdef PERFORMANCE_TIMING
   STIMER_GET_CURRENT_TP(tmp_cb_start);
#endif
   AQFD_DUMP_MESSAGE(ans);

   /* retrieve the original query associated with the answer */
   CHECK_FCT (fd_msg_answ_getq (ans, &qry));

   /* allocate the ula message */
   ula = (SyULA*)malloc( sizeof(*ula) );
   memset( (void*)ula, 0, sizeof(*ula) );
   ula->subscription_data = (SySubscriptionData*)malloc( sizeof(SySubscriptionData) );
   memset( (void*)ula->subscription_data, 0, sizeof(*ula->subscription_data) );

   /* retrieve the IMSI from the original request */
   AQCHECK_MSG_GET_AVP_STR(qry, aqDict.avp_User_Name, imsi, sizeof(imsi), goto err);

#ifdef PERFORMANCE_TIMING
   ula->ula_cb_start = tmp_cb_start;
#endif
   aqfdImsi2Binary(imsi, ula->imsi.val, &ula->imsi.len);

   /* get Result-Code */
   AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_Result_Code, avp, );
   if (avp)
   {
      /* Result-Code found */
      AQCHECK_AVP_GET_U32(aqDict.avp_Result_Code, avp, ula->result_code, );
      if (!ula->result_code)
      {
         ula->result_code = ER_DIAMETER_INVALID_AVP_VALUE;
         goto fini1;
      }
   }
   else
   {
      /* Result-Code not found, check for Experimental-Result */
      AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_Experimental_Result, avp, );
      if (!avp)
      {
         ula->result_code = ER_DIAMETER_MISSING_AVP;
         goto fini1;
      }

      if (aqfdParseExperimentalResult(avp, &ula->experimental_result) != LCM_REASON_NOT_APPL)
      {
         ula->result_code = ER_DIAMETER_INVALID_AVP_VALUE;
         goto fini1;
      }

      if (!ula->experimental_result.present)
      {
         ula->result_code = ER_DIAMETER_MISSING_AVP;
         goto fini1;
      }
   }

   if (ula->result_code == ER_DIAMETER_SUCCESS || ula->experimental_result.result_code == ER_DIAMETER_SUCCESS)
   {
      /* get ULA-Flags */
      AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_ULA_Flags, avp, );
      if ( avp )
      {
         AQCHECK_AVP_GET_U32( aqDict.avp_ULA_Flags, avp, ula->ula_flags, );
      }

      /* get Subscription-Data */
      AQCHECK_MSG_FIND_AVP(ans, aqDict.avp_Subscription_Data, avp, );
      if ( avp )
      {
         if ( aqfdParseSubscriptionData(avp, ula->subscription_data) != LCM_REASON_NOT_APPL)
         {
            ula->result_code = ER_DIAMETER_UNABLE_TO_COMPLY;
            goto err;
         }
      }
   }

   goto fini1;

err:
   free(ula);
   goto fini2;

fini1:
   _initPst();
   _pst.event = (Event)data;
   {
      Buffer *buf = NULL;
      if(SGetMsg(aqCb.cfg.syPst.region, aqCb.cfg.syPst.pool, &buf) != ROK)
         LOG_E("Error %d returned from SGetMsg() sending ULA notification", ret);
      else
      {
         cmPkPtr((PTR)ula, buf);
#ifdef PERFORMANCE_TIMING
         STIMER_GET_CURRENT_TP(tmp_time);
         ula->ula_ans_time = tmp_time;
#endif
         if ((ret = SPstTsk(&_pst, buf)) != ROK)
         {
            LOG_E("Error %d returned from SPstTsk() sending ULA notification", ret);
            SPutMsg(buf);
         }
      }
   }

fini2:
   return;
}

/*
*
*       Fun:    aqfdParseSubscriptionData
*
*       Desc:   Parse the Subscription-Data AVP
*
*       Ret:    0
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Subscription-Data ::= <AVP header: 1400 10415>
*          [ Subscriber-Status ]
*          [ MSISDN ] 
*          [ A-MSISDN ]
*          [ STN-SR ] 
*          [ ICS-Indicator ]
*          [ Network-Access-Mode ]
*          [ Operator-Determined-Barring ]
*          [ HPLMN-ODB ]
*       *10[ Regional-Subscription-Zone-Code ]
*          [ Access-Restriction-Data ]
*          [ APN-OI-Replacement ]
*          [ LCS-Info ]
*          [ Teleservice-List ]
*         *[ Call-Barring-Info ]
*          [ 3GPP-Charging-Characteristics ] 
*          [ AMBR ]
*          [ APN-Configuration-Profile ]
*          [ RAT-Frequency-Selection-Priority-ID ]
*          [ Trace-Data]
*          [ GPRS-Subscription-Data ]
*         *[ CSG-Subscription-Data ] 
*          [ Roaming-Restricted-Due-To-Unsupported-Feature ] 
*          [ Subscribed-Periodic-RAU-TAU-Timer ]
*          [ MPS-Priority ]
*          [ VPLMN-LIPA-Allowed ]
*          [ Relay-Node-Indicator ]
*          [ MDT-User-Consent ]
*          [ Subscribed-VSRVCC ]
*          [ ProSe-Subscription-Data ]
*          [ Subscription-Data-Flags ]
*         *[ Adjacent-Access-Restriction-Data ]
*          [ DL-Buffering-Suggested-Packet-Count ]
*         *[ IMSI-Group-Id ] 
*          [ UE-Usage-Type ]
*         *[ AESE-Communication-Pattern ]
*         *[ Monitoring-Event-Configuration ]
*          [ Emergency-Info ]
*          [ V2X-Subscription-Data ]
*         *[ eDRX-Cycle-Length ]
*         *[ AVP ]
*/

#define IS_AVP(a) (aqDict.a.avp_code == hdr->avp_code)
#ifdef ANSI
PUBLIC S16 aqfdParseSubscriptionData
(
struct avp * sub_data_avp,
SySubscriptionData *sub_data
)
#else
PUBLIC S16 aqfdParseSubscriptionData(sub_data_avp, sub_data)
struct avp * sub_data_avp;
SySubscriptionData *sub_data;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the Subscription-Data child AVP's */
   AQCHECK_FCT(fd_msg_browse(sub_data_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Subscriber_Status ) )
      {
         sub_data->subscriber_status = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_MSISDN ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, sub_data->msisdn );
      }
      else if ( IS_AVP( davp_A_MSISDN ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, sub_data->a_msisdn );
      }
      else if ( IS_AVP( davp_STN_SR ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, sub_data->stn_sr );
      }
      else if ( IS_AVP( davp_ICS_Indicator ) )
      {
         sub_data->ics_indicator = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Network_Access_Mode ) )
      {
         sub_data->network_access_mode = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Operator_Determined_Barring ) )
      {
         sub_data->operator_determined_barring = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_HPLMN_ODB ) )
      {
         sub_data->hplmn_odb = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Regional_Subscription_Zone_Code ) )
      {
         AQ_DUP_OSTR_ADD_LLIST( hdr->avp_value, sub_data->regional_subscription_zone_code );
      }
      else if ( IS_AVP( davp_Access_Restriction_Data ) )
      {
         sub_data->access_restriction_data = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_APN_OI_Replacement ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, sub_data->apn_oi_replacement );
      }
      else if ( IS_AVP( davp_LCS_Info ) )
      {
         aqfdParseLCSInfo( child_avp, &sub_data->lcs_info );
      }
      else if ( IS_AVP( davp_Teleservice_List ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseTeleserviceList, child_avp, &sub_data->teleservice_list );
         /* aqfdParseTeleserviceList( child_avp, &sub_data->teleservice_list ); */
      }
      else if ( IS_AVP( davp_Call_Barring_Info ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseCallBarringInfo, child_avp, &sub_data->call_barring_info );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseCallBarringInfo( child_avp );
         //cmLListAdd2Tail( &sub_data->call_barring_info, pln );
      }
      else if ( IS_AVP( davp_3GPP_Charging_Characteristics ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, sub_data->tgpp_charging_characteristics );
      }
      else if ( IS_AVP( davp_AMBR ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseAMBR, child_avp, &sub_data->ambr );
         //aqfdParseAMBR( child_avp, &sub_data->ambr );
      }
      else if ( IS_AVP( davp_APN_Configuration_Profile ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseAPNConfigurationProfile, child_avp, &sub_data->apn_configuration_profile );
         //aqfdParseAPNConfigurationProfile( child_avp, &sub_data->apn_configuration_profile );
      }
      else if ( IS_AVP( davp_RAT_Frequency_Selection_Priority_ID ) )
      {
         sub_data->rat_frequency_selection_priority_id = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Trace_Data ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseTraceData, child_avp, &sub_data->trace_data );
         //aqfdParseTraceData( child_avp, &sub_data->trace_data );
      }
      else if ( IS_AVP( davp_GPRS_Subscription_Data ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseGPRSSubscriptionData, child_avp, &sub_data->gprs_subscription_data );
         //aqfdParseGPRSSubscriptionData( child_avp, &sdn_data->gprs_subscription_data );
      }
      else if ( IS_AVP( davp_CSG_Subscription_Data ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseCSGSubscriptionData, child_avp, &sub_data->csg_subscription_data );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseCSGSubscriptionData( child_avp );
         //cmLListAdd2Tail( &sub_data->csg_subscription_data, pln );
      }
      else if ( IS_AVP( davp_Roaming_Restricted_Due_To_Unsupported_Feature ) )
      {
         sub_data->roaming_restricted_due_to_unsupported_feature = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Subscribed_Periodic_RAU_TAU_Timer ) )
      {
         sub_data->subscribed_periodic_rau_tau_timer = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_MPS_Priority ) )
      {
         sub_data->mps_priority = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_VPLMN_LIPA_Allowed ) )
      {
         sub_data->vplmn_lipa_allowed = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Relay_Node_Indicator ) )
      {
         sub_data->relay_node_indicator = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_MDT_User_Consent ) )
      {
         sub_data->mdt_user_consent = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Subscribed_VSRVCC ) )
      {
         sub_data->subscribed_vsrvcc = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_ProSe_Subscription_Data ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseProSeSubscriptionData, child_avp, &sub_data->prose_subscription_data );
         //aqfdParseProSeSubscriptionData( child_avp, &sub_data->prose_subscription_data;
      }
      else if ( IS_AVP( davp_Subscription_Data_Flags ) )
      {
         sub_data->subscription_data_flags = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Adjacent_Access_Restriction_Data ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseAdjacentAccessRestrictionData, child_avp, &sub_data->adjacent_access_restriction_data );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseAdjacentAccessRestrictionData( child_avp );
         //cmLListAdd2Tail( &sub_data->adjacent_access_restriction_data, pln );
      }
      else if ( IS_AVP( davp_DL_Buffering_Suggested_Packet_Count ) )
      {
         sub_data->dl_buffering_suggested_packet_count = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_IMSI_Group_Id ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseIMSIGroupId, child_avp, &sub_data->imsi_group_id );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseIMSIGroupId( child_avp );
         //cmLListAdd2Tail( &sub_data->imsi_group_id, pln );
      }
      else if ( IS_AVP( davp_UE_Usage_Type ) )
      {
         sub_data->ue_usage_type = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_AESE_Communication_Pattern ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseAESECommunicationPattern, child_avp, &sub_data->aese_communication_pattern );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseAESECommunicationPattern( child_avp );
         //cmLListAdd2Tail( &sub_data->aese_communication_pattern, pln );
      }
      else if ( IS_AVP( davp_Monitoring_Event_Configuration ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseMonitoringEventConfiguration, child_avp, &sub_data->monitoring_event_configuration );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseMonitoringEventConfiguration( child_avp );
         //cmLListAdd2Tail( &sub_data->monitoring_event_configuration, pln );
      }
      else if ( IS_AVP( davp_Emergency_Info ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseEmergencyInfo, child_avp, &sub_data->emergency_info );
         //aqfdParseEmergencyInfo( child_avp, &sub_data->emergency_info );
      }
      else if ( IS_AVP( davp_V2X_Subscription_Data ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseV2XSubscriptionData, child_avp, &sub_data->v2x_subscription_data );
         //aqfdParseV2XSubscriptionData( child_avp, &sub_data->v2x_subscription_data );
      }
      else if ( IS_AVP( davp_eDRX_Cycle_Length ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseeDRXCycleLength, child_avp, &sub_data->edrx_cycle_length );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseeDRXCycleLength( child_avp );
         //cmLListAdd2Tail( &sub_data->edrx_cycle_length, pln );
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseLCSInfo
*
*       Desc:   LCS-Info AVP
*
*       Ret:    0
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       LCS-Info ::= <AVP header: 1473 10415>
*         *[ GMLC-Number]
*         *[ LCS-PrivacyException ]
*         *[ MO-LR ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseLCSInfo
(
struct avp * lcs_info_avp,
SyLCSInfo *lcs_info
)
#else
PRIVATE S16 aqfdParseLCSInfo(lcs_info_avp, lcs_info)
struct avp * lcs_info_avp;
SyLCSInfo *lcs_info;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the LCS-Info child AVP's */
   AQCHECK_FCT( fd_msg_browse(lcs_info_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_LCS_INFO );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_GMLC_Number ) )
      {
         AQ_DUP_OSTR_ADD_LLIST( hdr->avp_value, lcs_info->gmlc_number );
      }
      else if ( IS_AVP( davp_LCS_PrivacyException ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseLCSPrivacyException, child_avp, &lcs_info->lcs_privacyexception );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseLCSPrivacyException( child_avp );
         //cmLListAdd2Tail( &lcs_info->lcs_privacyexception, pln );
      }
      else if ( IS_AVP( davp_MO_LR ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseMOLR, child_avp, &lcs_info->mo_lr );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseMOLR( child_avp );
         //cmLListAdd2Tail( &lcs_info->mo_lr, pln );
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_LCS_INFO );
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseMOLR
*
*       Desc:   MO-LR AVP
*
*       Ret:    0
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       MO-LR ::= <AVP header: 1485 10415>
*          { SS-Code }
*          { SS-Status }
*         *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseMOLR
(
struct avp * mo_lr_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseMOLR(mo_lr_avp, node)
struct avp * mo_lr_avp;
PTR *node;
#endif
{
   SyMOLR *mo_lr;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyMOLR */
   mo_lr = (SyMOLR*)malloc( sizeof(SyMOLR) );
   memset( mo_lr, 0, sizeof(*mo_lr) );

   /* iterate through the MO-LR child AVP's */
   AQCHECK_FCT(fd_msg_browse(mo_lr_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_MOLR );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_SS_Code ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, mo_lr->ss_code );
      }
      else if ( IS_AVP( davp_SS_Status ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, mo_lr->ss_status );
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_MOLR );
   }
   
   *node = (PTR)mo_lr;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseLCSPrivacyException
*
*       Desc:   Parse LCS-PrivacyException AVP
*
*       Ret:    0
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       LCS-PrivacyException ::= <AVP header: 1475 10415>
*          { SS-Code }
*          { SS-Status }
*          [ Notification-To-UE-User ]
*         *[ External-Client ]
*         *[ PLMN-Client ]
*         *[ Service-Type ]
*         *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseLCSPrivacyException
(
struct avp * lcs_pe_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseLCSPrivacyException(lcs_pe_avp, node)
struct avp * lcs_pe_avp;
PTR *node;
#endif
{
   SyLCSPrivacyException *lcs_pe;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyLCSPrivacyException */
   lcs_pe = (SyLCSPrivacyException*)malloc( sizeof(SyLCSPrivacyException) );
   memset( lcs_pe, 0, sizeof(*lcs_pe) );

   /* iterate through the LCS-Info child AVP's */
   AQCHECK_FCT( fd_msg_browse(lcs_pe_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_LCS_PRIVACY_EXCEPTION );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_SS_Code ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, lcs_pe->ss_code );
      }
      else if ( IS_AVP( davp_SS_Status ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, lcs_pe->ss_status );
      }
      else if ( IS_AVP( davp_Notification_To_UE_User ) )
      {
         lcs_pe->notification_to_ue_user = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_External_Client ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseExternalClient, child_avp, &lcs_pe->external_client );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseExternalClient( child_avp );
         //cmLListAdd2Tail( &lcs_pe->external_client, pln );
      }
      else if ( IS_AVP( davp_PLMN_Client ) )
      {
         AQ_ALLOC_CMLLIST( pln );
         AQ_DUP_ENUM( hdr->avp_value->i32, pln->node );
         cmLListAdd2Tail( &lcs_pe->plmn_client, pln );
      }
      else if ( IS_AVP( davp_Service_Type ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseServiceType, child_avp, &lcs_pe->service_type );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseServiceType( child_avp );
         //cmLListAdd2Tail( &lcs_pe->service_type, pln );
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_LCS_PRIVACY_EXCEPTION );
   }
   
   *node = (PTR)lcs_pe;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseExternalClient
*
*       Desc:   Parse External-Client AVP
*
*       Ret:    0
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       External-Client ::= <AVP header: 1479 10415>
*          { Client-Identity }
*          [ GMLC-Restriction ]
*          [ Notification-To-UE-User ]
*         *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseExternalClient
(
struct avp * ec_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseExternalClient(ec_avp, node)
struct avp * ec_avp;
PTR *node;
#endif
{
   SyExternalClient *ec;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyExternalClient */
   ec = (SyExternalClient*)malloc( sizeof(SyExternalClient) );
   memset( ec, 0, sizeof(*ec) );

   /* iterate through the External-Client child AVP's */
   AQCHECK_FCT( fd_msg_browse(ec_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_EXTERNAL_CLIENT );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Client_Identity ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, ec->client_identity );
      }
      else if ( IS_AVP( davp_GMLC_Restriction ) )
      {
         ec->gmlc_restriction = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Notification_To_UE_User ) )
      {
         ec->notification_to_ue_user = hdr->avp_value->i32;
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_EXTERNAL_CLIENT );
   }
   
   *node = (PTR)ec;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseServiceType
*
*       Desc:   Parse External-Client AVP
*
*       Ret:    0
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Service-Type ::= <AVP header: 1483 10415>
*          { ServiceTypeIdentity }
*          [ GMLC-Restriction ]
*          [ Notification-To-UE-User ]
*         *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseServiceType
(
struct avp * st_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseServiceType(st_avp, node)
struct avp * st_avp;
PTR *node;
#endif
{
   SyServiceType *st;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyServiceType */
   st = (SyServiceType*)malloc( sizeof(SyServiceType) );
   memset( st, 0, sizeof(*st) );

   /* iterate through the Service-Type child AVP's */
   AQCHECK_FCT( fd_msg_browse(st_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_SERVICE_TYPE );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Service_Type_Identity ) )
      {
         st->servicetypeidentity = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_GMLC_Restriction ) )
      {
         st->gmlc_restriction = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Notification_To_UE_User ) )
      {
         st->notification_to_ue_user = hdr->avp_value->i32;
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_SERVICE_TYPE );
   }
   
   *node = (PTR)st;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseTeleserviceList
*
*       Desc:   Parse Teleservice-List AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Teleservice-List ::= <AVP header: 1486 10415>
*         1 * { TS-Code }* [ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseTeleserviceList
(
struct avp * tslist_avp,
SyTeleserviceList *tslist
)
#else
PRIVATE S16 aqfdParseTeleserviceList(tslist_avp, tslist)
struct avp * tslist_avp;
SyTeleserviceList *tslist;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the Teleservice-List child AVP's */
   AQCHECK_FCT( fd_msg_browse(tslist_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_TELESERVICE_LIST );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Teleservice_List ) )
      {
         AQ_DUP_OSTR_ADD_LLIST( hdr->avp_value, tslist->ts_code );
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_TELESERVICE_LIST );
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseCallBarringInfo
*
*       Desc:   Call-Barring-Info AVP
*
*       Ret:    
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Call-Barring-Info ::= <AVP header: 1488 10415>
*          { SS-Code }
*          { SS-Status }
*         *[ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseCallBarringInfo
(
struct avp * cbi_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseCallBarringInfo(mo_lr_avp, node)
struct avp * cbi_avp;
PTR *node;
#endif
{
   SyCallBarringInfo *cbi;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyCallBarringInfo */
   cbi = (SyCallBarringInfo*)malloc( sizeof(SyCallBarringInfo) );
   memset( cbi, 0, sizeof(*cbi) );

   /* iterate through the Call-Barring-Info child AVP's */
   AQCHECK_FCT( fd_msg_browse(cbi_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_CALL_BARRING_INFO );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_SS_Code ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, cbi->ss_code );
      }
      else if ( IS_AVP( davp_SS_Status ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, cbi->ss_status );
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_CALL_BARRING_INFO );
   }
   
   *node = (PTR)cbi;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseAMBR
*
*       Desc:   Parse AMBR AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       AMBR ::= <AVP header: 1435 10415>
*          { Max-Requested-Bandwidth-UL }
*          { Max-Requested-Bandwidth-DL }
*         *[AVP] 
*/
#ifdef ANSI
PRIVATE S16 aqfdParseAMBR
(
struct avp * ambr_avp,
SyAMBR *ambr
)
#else
PRIVATE S16 aqfdParseAMBR(ambr_avp, ambr)
struct avp * tslist_avp;
SyAMBR *ambr;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the AMBR child AVP's */
   AQCHECK_FCT(fd_msg_browse(ambr_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Max_Requested_Bandwidth_UL ) )
      {
         ambr->max_requested_bandwidth_ul = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Max_Requested_Bandwidth_DL ) )
      {
         ambr->max_requested_bandwidth_dl = hdr->avp_value->u32;
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseAPNConfigurationProfile
*
*       Desc:   Parse APN-Configuration-Profile AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       APN-Configuration-Profile ::= <AVP header: 1429 10415>
*          { Context-Identifier }
*          [ Additional-Context-Identifier ]
*          { All-APN-Configurations-Included-Indicator }
*        1*{APN-Configuration}
*         *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseAPNConfigurationProfile
(
struct avp * apncp_avp,
SyAPNConfigurationProfile *apncp
)
#else
PRIVATE S16 aqfdParseAPNConfigurationProfile(apncp_avp, apncp)
struct avp * apncp_avp;
SyAPNConfigurationProfile *apncp;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the AMBR child AVP's */
   AQCHECK_FCT(fd_msg_browse(apncp_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Context_Identifier ) )
      {
         apncp->context_identifier = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Additional_Context_Identifier ) )
      {
         apncp->additional_context_identifier = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_All_APN_Configurations_Included_Indicator ) )
      {
         apncp->all_apn_configurations_included_indicator = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_APN_Configuration ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseAPNConfiguration, child_avp, &apncp->apn_configuration );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseAPNConfiguration( child_avp );
         //cmLListAdd2Tail( &apncp->apn_configuration, pln );
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseAPNConfiguration
*
*       Desc:   Parse APN-Configuration AVP
*
*       Ret:    
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       APN-Configuration ::= <AVP header: 1430 10415>
*          { Context-Identifier } 
*      * 2 [ Served-Party-IP-Address ]
*          { PDN-Type }
*          { Service-Selection}
*          [ EPS-Subscribed-QoS-Profile ]
*          [ VPLMN-Dynamic-Address-Allowed ]
*          [ MIP6-Agent-Info ] 
*          [ Visited-Network-Identifier ]
*          [ PDN-GW-Allocation-Type ]
*          [ 3GPP-Charging-Characteristics ]
*          [ AMBR ]
*         *[ Specific-APN-Info ] 
*          [ APN-OI-Replacement ] 
*          [ SIPTO-Permission ] 
*          [ LIPA-Permission ]
*          [ Restoration-Priority ]
*          [ SIPTO-Local-Network-Permission ]
*          [ WLAN-offloadability ] 
*          [ Non-IP-PDN-Type-Indicator ]
*          [ Non-IP-Data-Delivery-Mechanism ]
*          [ SCEF-ID ]
*          [ SCEF-Realm ]
*          [ Preferred-Data-Mode ]
*          [ PDN-Connection-Continuity ]
*         *[ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseAPNConfiguration
(
struct avp * apn_cfg_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseAPNConfiguration(mo_lr_avp, node)
struct avp * apn_cfg_avp;
PTR *node;
#endif
{
   SyAPNConfiguration *apn_cfg;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyAPNConfiguration */
   apn_cfg = (SyAPNConfiguration*)malloc( sizeof(SyAPNConfiguration) );
   memset( apn_cfg, 0, sizeof(*apn_cfg) );

   /* iterate through the Call-Barring-Info child AVP's */
   AQCHECK_FCT( fd_msg_browse(apn_cfg_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_APN_CONFIGURATION );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Context_Identifier ) )
      {
         apn_cfg->context_identifier = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Served_Party_IP_Address ) )
      {
         AQ_ALLOC_CMLLIST( pln );
         AQ_ALLOC_AQADDRESS2( hdr->avp_value, pln->node );
         cmLListAdd2Tail( &apn_cfg->served_party_ip_address, pln );
      }
      else if ( IS_AVP( davp_PDN_Type ) )
      {
         apn_cfg->pdn_type = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Service_Selection ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, apn_cfg->service_selection );
      }
      else if ( IS_AVP( davp_EPS_Subscribed_QoS_Profile ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseEPSSubscribedQoSProfile, child_avp, &apn_cfg->eps_subscribed_qos_profile );
         //aqfdParseEPSSubscribedQoSProfile( child_avp, &apn_cfg->eps_subscribed_qos_profile );
      }
      else if ( IS_AVP( davp_VPLMN_Dynamic_Address_Allowed ) )
      {
         apn_cfg->vplmn_dynamic_address_allowed = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_MIP6_Agent_Info ) ) {
         AQCHECK_PARSE_DIRECT( aqfdParseMIP6AgentInfo, child_avp, &apn_cfg->mip6_agent_info );
         //aqfdParseMIP6AgentInfo( child_avp, &apn_cfg->mip6_agent_info );
      }
      else if ( IS_AVP( davp_Visited_Network_Identifier ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, apn_cfg->visited_network_identifier );
      }
      else if ( IS_AVP( davp_PDN_GW_Allocation_Type ) )
      {
         apn_cfg->pdn_gw_allocation_type = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_3GPP_Charging_Characteristics ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, apn_cfg->tgpp_charging_characteristics );
      }
      else if ( IS_AVP( davp_AMBR ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseAMBR, child_avp, &apn_cfg->ambr );
         //aqfdParseAMBR( child_avp, &apn_cfg->ambr );
      }
      else if ( IS_AVP( davp_Specific_APN_Info ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseSpecificAPNInfo, child_avp, &apn_cfg->specific_apn_info );
         //aqfdParseSpecificAPNInfo( child_avp, &apn_cfg->specific_apn_info );
      }
      else if ( IS_AVP( davp_APN_OI_Replacement ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, apn_cfg->apn_oi_replacement );
      }
      else if ( IS_AVP( davp_SIPTO_Permission ) )
      {
         apn_cfg->sipto_permission = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_LIPA_Permission ) )
      {
         apn_cfg->lipa_permission = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Restoration_Priority ) )
      {
         apn_cfg->restoration_priority = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_SIPTO_Local_Network_Permission ) )
      {
         apn_cfg->sipto_local_network_permission = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_WLAN_offloadability ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseWLANoffloadability, child_avp, &apn_cfg->wlan_offloadability );
         //aqfdParseWLANoffloadability( child_avp, &apn_cfg->wlan_offloadability );
      }
      else if ( IS_AVP( davp_Non_IP_PDN_Type_Indicator ) )
      {
         apn_cfg->non_ip_pdn_type_indicator = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Non_IP_Data_Delivery_Mechanism ) )
      {
         apn_cfg->non_ip_data_delivery_mechanism = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_SCEF_ID ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, apn_cfg->scef_id );
      }
      else if ( IS_AVP( davp_SCEF_Realm ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, apn_cfg->scef_realm );
      }
      else if ( IS_AVP( davp_Preferred_Data_Mode ) )
      {
         apn_cfg->preferred_data_mode = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_PDN_Connection_Continuity ) )
      {
         apn_cfg->pdn_connection_continuity = hdr->avp_value->u32;
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_APN_CONFIGURATION );
   }
   
   *node = (PTR)apn_cfg;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseEPSSubscribedQoSProfile
*
*       Desc:   Parse EPS-Subscribed-QoS-Profile AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       EPS-Subscribed-QoS-Profile ::= <AVP header: 1431 10415>
*          { QoS-Class-Identifier }
*          { Allocation-Retention-Priority }
*         *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseEPSSubscribedQoSProfile
(
struct avp * eps_avp,
SyEPSSubscribedQoSProfile *eps
)
#else
PRIVATE S16 aqfdParseEPSSubscribedQoSProfile(eps_avp, eps)
struct avp * eps_avp;
SyEPSSubscribedQoSProfile *eps;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the EPS-Subscribed-QoS-Profile child AVP's */
   AQCHECK_FCT(fd_msg_browse(eps_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_QoS_Class_Identifier ) )
      {
         eps->qos_class_identifier = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Allocation_Retention_Priority ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseAllocationRetentionPriority, child_avp, &eps->allocation_retention_priority );
         //aqfdParseAllocationRetentionPriority( child_avp, eps->allocation_retention_priority );
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseAllocationRetentionPriority
*
*       Desc:   Parse EPS-Subscribed-QoS-Profile AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Allocation-Retention-Priority ::= <AVP header: 1034 10415>
*          { Priority-Level }
*          [ Pre-emption-Capability ]
*          [ Pre-emption-Vulnerability ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseAllocationRetentionPriority
(
struct avp * arp_avp,
SyAllocationRetentionPriority *arp
)
#else
PRIVATE S16 aqfdParseAllocationRetentionPriority(eps_avp, eps)
struct avp * arp_avp;
SyAllocationRetentionPriority *arp;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the Allocation-Retention-Priority child AVP's */
   AQCHECK_FCT(fd_msg_browse(arp_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Priority_Level ) )
      {
         arp->priority_level = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Pre_emption_Capability ) )
      {
         arp->pre_emption_capability = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Pre_emption_Vulnerability ) )
      {
         arp->pre_emption_vulnerability = hdr->avp_value->i32;
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseMIP6AgentInfo
*
*       Desc:   Parse MIP6-Agent-Info AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       MIP6-Agent-Info ::=	 < AVP Header: 486 >
*        *2[ MIP-Home-Agent-Address ]
*          [ MIP-Home-Agent-Host ]
*          [ MIP6-Home-Link-Prefix ]   **** NOT USED IN S6A ****
*         *[ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseMIP6AgentInfo
(
struct avp * mai_avp,
SyMIP6AgentInfo *mai
)
#else
PRIVATE S16 aqfdParseMIP6AgentInfo(mai_avp, mai)
struct avp * mai_avp;
SyMIP6AgentInfo *mai;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the Allocation-Retention-Priority child AVP's */
   AQCHECK_FCT(fd_msg_browse(mai_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_MIP_Home_Agent_Address ) )
      {
         AQ_ALLOC_CMLLIST( pln );
         AQ_ALLOC_AQADDRESS2( hdr->avp_value, pln->node );
         cmLListAdd2Tail( &mai->mip_home_agent_address, pln );
      }
      else if ( IS_AVP( davp_MIP_Home_Agent_Host ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseMIPHomeAgentHost, child_avp, &mai->mip_home_agent_host );
         //aqfdParseMIPHomeAgentHost( child_avp, &mai->mip_home_agent_host );
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseMIPHomeAgentHost
*
*       Desc:   Parse MIP-Home-Agent-Host AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       MIP-Home-Agent-Host ::= < AVP Header: 348 >
*          { Destination-Realm }
*          { Destination-Host }
*        * [ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseMIPHomeAgentHost
(
struct avp * mhah_avp,
SyMIPHomeAgentHost *mhah
)
#else
PRIVATE S16 aqfdParseMIPHomeAgentHost(mai_avp, mhah)
struct avp * mhah_avp;
SyMIPHomeAgentHost *mhah;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the MIP-Home-Agent-Host child AVP's */
   AQCHECK_FCT(fd_msg_browse(mhah_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Destination_Realm ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, mhah->destination_realm );
      }
      else if ( IS_AVP( davp_Destination_Host ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, mhah->destination_host );
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseSpecificAPNInfo
*
*       Desc:   Parse Specific-APN-Info AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Specific-APN-Info ::= <AVP header: 1472 10415>
*          { Service-Selection }
*          { MIP6-Agent-Info }
*          [ Visited-Network-Identifier ]
*         *[ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseSpecificAPNInfo
(
struct avp * sai_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseSpecificAPNInfo(sai_avp, node)
struct avp * sai_avp;
PTR *node;
#endif
{
   SySpecificAPNInfo *sai;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SySpecificAPNInfo */
   sai = (SySpecificAPNInfo*)malloc( sizeof(SySpecificAPNInfo) );
   memset( sai, 0, sizeof(*sai) );

   /* iterate through the Specific-APN-Info child AVP's */
   AQCHECK_FCT( fd_msg_browse(sai_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_SPECIFIC_APN_INFO );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Service_Selection ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, sai->service_selection );
      }
      else if ( IS_AVP( davp_MIP6_Agent_Info ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseMIP6AgentInfo, child_avp, &sai->mip6_agent_info );
         //aqfdParseMIP6AgentInfo( child_avp, &sai->mip6_agent_info );
      }
      else if ( IS_AVP( davp_Visited_Network_Identifier ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, sai->visited_network_identifier );
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_SPECIFIC_APN_INFO );
   }

   *node = (PTR)sai;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseWLANoffloadability
*
*       Desc:   Parse Specific-APN-Info AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       WLAN-offloadability ::= <AVP header: 1667>
*          [ WLAN-offloadability-EUTRAN ]
*          [ WLAN-offloadability-UTRAN ]
*         *[ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseWLANoffloadability
(
struct avp * wlan_avp,
SyWLANoffloadability *wlan
)
#else
PRIVATE S16 aqfdParseWLANoffloadability(wlan_avp, wlan)
struct avp * wlan_avp;
SyWLANoffloadability *wlan;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the Specific-APN-Info child AVP's */
   AQCHECK_FCT(fd_msg_browse(wlan_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_WLAN_offloadability_EUTRAN ) )
      {
         wlan->wlan_offloadability_eutran = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_WLAN_offloadability_UTRAN ) )
      {
         wlan->wlan_offloadability_utran = hdr->avp_value->u32;
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseTraceData
*
*       Desc:   Parse Trace-Data AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Trace-Data ::= <AVP header: 1458 10415>
*          {Trace-Reference}
*          {Trace-Depth}
*          {Trace-NE-Type-List}
*          [Trace-Interface-List]
*          {Trace-Event-List}
*          [OMC-Id]
*          {Trace-Collection-Entity}
*          [MDT-Configuration]
*         *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseTraceData
(
struct avp * td_avp,
SyTraceData *td
)
#else
PRIVATE S16 aqfdParseTraceData(td_avp, td)
struct avp * td_avp;
SyTraceData *td;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the Trace-Data child AVP's */
   AQCHECK_FCT(fd_msg_browse(td_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Trace_Reference ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, td->trace_reference );
      }
      else if ( IS_AVP( davp_Trace_Depth ) )
      {
         td->trace_depth = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Trace_NE_Type_List ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, td->trace_ne_type_list);
      }
      else if ( IS_AVP( davp_Trace_Interface_List ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, td->trace_interface_list);
      }
      else if ( IS_AVP( davp_Trace_Event_List ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, td->trace_event_list);
      }
      else if ( IS_AVP( davp_OMC_Id ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, td->omc_id);
      }
      else if ( IS_AVP( davp_Trace_Collection_Entity ) )
      {
         AQ_ALLOC_AQADDRESS1( hdr->avp_value, td->trace_collection_entity );
      }
      else if ( IS_AVP( davp_MDT_Configuration ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseMDTConfiguration, child_avp, &td->mdt_configuration );
         //aqfdParseMDTConfiguration( child_avp, &td->mdt_configuration );
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseMDTConfiguration
*
*       Desc:   Parse MDT-Configuration AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       MDT-Configuration ::= <AVP header: 1622 10415>
*          { Job-Type } 
*          [ Area-Scope ] 
*          [ List-Of-Measurements ] 
*          [ Reporting-Trigger ]
*          [ Report-Interval ] 
*          [ Report-Amount ]
*          [ Event-Threshold-RSRP ]
*          [ Event-Threshold-RSRQ ]
*          [ Logging-Interval ]
*          [ Logging-Duration ]
*          [ Measurement-Period-LTE ]
*          [ Measurement-Period-UMTS ]
*          [ Collection-Period-RRM-LTE ]
*          [ Collection-Period-RRM-UMTS ]
*          [ Positioning-Method ]
*          [ Measurement-Quantity] 
*          [ Event-Threshold-Event-1F ]
*          [ Event-Threshold-Event-1I ]
*         *[ MDT-Allowed-PLMN-Id ]
*         *[ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseMDTConfiguration
(
struct avp * mdtc_avp,
SyMDTConfiguration *mdtc
)
#else
PRIVATE S16 aqfdParseMDTConfiguration(mdtc_avp, mdtc)
struct avp * mdtc_avp;
SyMDTConfiguration *mdtc;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the Trace-Data child AVP's */
   AQCHECK_FCT(fd_msg_browse(mdtc_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Job_Type ) )
      {
         mdtc->job_type = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Area_Scope ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseAreaScope, child_avp, &mdtc->area_scope );
         //aqfdParseAreaScope( child_avp, &mdtc->area_scope );
      }
      else if ( IS_AVP( davp_List_Of_Measurements ) )
      {
         mdtc->list_of_measurements = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Reporting_Trigger ) )
      {
         mdtc->reporting_trigger = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Report_Interval ) )
      {
         mdtc->report_interval = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Report_Amount ) )
      {
         mdtc->report_amount = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Event_Threshold_RSRP ) )
      {
         mdtc->event_threshold_rsrp = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Event_Threshold_RSRQ ) )
      {
         mdtc->event_threshold_rsrq = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Logging_Interval ) )
      {
         mdtc->logging_interval = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Logging_Duration ) )
      {
         mdtc->logging_duration = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Measurement_Period_LTE ) )
      {
         mdtc->measurement_period_lte = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Measurement_Period_UMTS ) )
      {
         mdtc->measurement_period_umts = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Collection_Period_RRM_LTE ) )
      {
         mdtc->collection_period_rrm_lte = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Collection_Period_RRM_UMTS ) )
      {
         mdtc->collection_period_rrm_umts = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Positioning_Method ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, mdtc->positioning_method );
      }
      else if ( IS_AVP( davp_Measurement_Quantity ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, mdtc->measurement_quantity );
      }
      else if ( IS_AVP( davp_Event_Threshold_Event_1F ) )
      {
         mdtc->event_threshold_event_1f = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Event_Threshold_Event_1I ) )
      {
         mdtc->event_threshold_event_1i = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_MDT_Allowed_PLMN_Id ) )
      {
         AQ_DUP_OSTR_ADD_LLIST( hdr->avp_value, mdtc->mdt_allowed_plmn_id );
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseAreaScope
*
*       Desc:   Parse Area-Scope AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Area-Scope ::= <AVP header: 1623 10415>
*         *[ Cell-Global-Identity ]
*         *[ E-UTRAN-Cell-Global-Identity ]
*         *[ Routing-Area-Identity ]
*         *[ Location-Area-Identity ]
*         *[ Tracking-Area-Identity ]
*         *[ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseAreaScope
(
struct avp * as_avp,
SyAreaScope *as
)
#else
PRIVATE S16 aqfdParseAreaScope(as_avp, as)
struct avp * as_avp;
SyAreaScope *as;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the Trace-Data child AVP's */
   AQCHECK_FCT(fd_msg_browse(as_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Cell_Global_Identity ) )
      {
         AQ_DUP_OSTR_ADD_LLIST( hdr->avp_value, as->cell_global_identity );
      }
      else if ( IS_AVP( davp_E_UTRAN_Cell_Global_Identity ) )
      {
         AQ_DUP_OSTR_ADD_LLIST( hdr->avp_value, as->e_utran_cell_global_identity );
      }
      else if ( IS_AVP( davp_Routing_Area_Identity ) )
      {
         AQ_DUP_OSTR_ADD_LLIST( hdr->avp_value, as->routing_area_identity );
      }
      else if ( IS_AVP( davp_Location_Area_Identity ) )
      {
         AQ_DUP_OSTR_ADD_LLIST( hdr->avp_value, as->location_area_identity );
      }
      else if ( IS_AVP( davp_Tracking_Area_Identity ) )
      {
         AQ_DUP_OSTR_ADD_LLIST( hdr->avp_value, as->tracking_area_identity );
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseGPRSSubscriptionData
*
*       Desc:   Parse GPRS-Subscription-Data AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       GPRS-Subscription-Data ::= <AVP header: 1467 10415>
*          { Complete-Data-List-Included-Indicator }
*      1*50{ PDP-Context }
*         *[ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseGPRSSubscriptionData
(
struct avp * gprs_avp,
SyGPRSSubscriptionData *gprs
)
#else
PRIVATE S16 aqfdParseGPRSSubscriptionData(gprs_avp, gprs)
struct avp * gprs_avp;
SyGPRSSubscriptionData *gprs;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the GPRS-Subscription-Data child AVP's */
   AQCHECK_FCT(fd_msg_browse(gprs_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Complete_Data_List_Included_Indicator ) )
      {
         gprs->complete_data_list_included_indicator = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_PDP_Context ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParsePDPContext, child_avp, &gprs->pdp_context );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParsePDPContext( child_avp );
         //cmLListAdd2Tail( &gprs->pdp_context, pln );
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParsePDPContext
*
*       Desc:   Parse PDP-Context AVP
*
*       Ret:    
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       PDP-Context ::= <AVP header: 1469 10415>
*          { Context-Identifier } 
*          { PDP-Type }
*          [ PDP-Address ]
*          { QoS-Subscribed }
*          [ VPLMN-Dynamic-Address-Allowed ]
*          { Service-Selection }
*          [ 3GPP-Charging-Characteristics ] 
*          [ Ext-PDP-Type ]
*          [ Ext-PDP-Address ]
*          [ AMBR ] 
*          [ APN-OI-Replacement ]
*          [ SIPTO-Permission ] 
*          [ LIPA-Permission ]
*          [ Restoration-Priority ]
*          [ SIPTO-Local-Network-Permission ]
*          [ Non-IP-Data-Delivery-Mechanism ]
*          [ SCEF-ID ]
*          *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParsePDPContext
(
struct avp * pc_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParsePDPContext(pc_avp, node)
struct avp * pc_avp;
PTR *node;
#endif
{
   SyPDPContext *pc;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyPDPContext */
   pc = (SyPDPContext*)malloc( sizeof(SyPDPContext) );
   memset( pc, 0, sizeof(*pc) );

   /* iterate through the PDP-Context child AVP's */
   AQCHECK_FCT( fd_msg_browse(pc_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_PDP_CONTEXT );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Context_Identifier ) )
      {
         pc->context_identifier = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_PDP_Type ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, pc->pdp_type );
      }
      else if ( IS_AVP( davp_PDP_Address ) )
      {
         AQ_ALLOC_AQADDRESS1( hdr->avp_value, pc->pdp_address );
      }
      else if ( IS_AVP( davp_QoS_Subscribed ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, pc->qos_subscribed );
      }
      else if ( IS_AVP( davp_VPLMN_Dynamic_Address_Allowed ) )
      {
         pc->vplmn_dynamic_address_allowed = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_Service_Selection ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, pc->service_selection );
      }
      else if ( IS_AVP( davp_3GPP_Charging_Characteristics ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, pc->tgpp_charging_characteristics );
      }
      else if ( IS_AVP( davp_Ext_PDP_Type ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, pc->ext_pdp_type );
      }
      else if ( IS_AVP( davp_Ext_PDP_Address ) )
      {
         AQ_ALLOC_AQADDRESS1( hdr->avp_value, pc->ext_pdp_address );
      }
      else if ( IS_AVP( davp_AMBR ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseAMBR, child_avp, &pc->ambr );
         //aqfdParseAMBR( child_avp, &pc->ambr );
      }
      else if ( IS_AVP( davp_APN_OI_Replacement ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, pc->apn_oi_replacement );
      }
      else if ( IS_AVP( davp_SIPTO_Permission ) )
      {
         pc->sipto_permission = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_LIPA_Permission ) )
      {
         pc->lipa_permission = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Restoration_Priority ) )
      {
         pc->restoration_priority = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_SIPTO_Local_Network_Permission ) )
      {
         pc->sipto_local_network_permission = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Non_IP_Data_Delivery_Mechanism ) )
      {
         pc->non_ip_data_delivery_mechanism = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_SCEF_ID ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, pc->scef_id );
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_PDP_CONTEXT );
   }
   
   *node = (PTR)pc;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseCSGSubscriptionData
*
*       Desc:   Parse PDP-Context AVP
*
*       Ret:    
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       CSG-Subscription-Data ::= <AVP header: 1436 10415>
*          { CSG-Id }
*          [ Expiration-Date ] 
*         *[ Service-Selection ]
*          [ Visited-PLMN-Id ]
*         *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseCSGSubscriptionData
(
struct avp * csgsd_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseCSGSubscriptionData(pc_avp, node)
struct avp * csgsd_avp;
PTR *node;
#endif
{
   SyCSGSubscriptionData *csgsd;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyCSGSubscriptionData */
   csgsd = (SyCSGSubscriptionData*)malloc( sizeof(SyCSGSubscriptionData) );
   memset( csgsd, 0, sizeof(*csgsd) );

   /* iterate through the CSG-Subscription-Data child AVP's */
   AQCHECK_FCT( fd_msg_browse(csgsd_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_CSG_SUBSCRIPTION_DATA );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_CSG_Id ) )
      {
         csgsd->csg_id = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Expiration_Date ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, csgsd->expiration_date );
      }
      else if ( IS_AVP( davp_Service_Selection ) )
      {
         AQ_DUP_OSTR_ADD_LLIST( hdr->avp_value, csgsd->service_selection );
      }
      else if ( IS_AVP( davp_Visited_PLMN_Id ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, csgsd->visited_plmn_id );
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_CSG_SUBSCRIPTION_DATA );
   }
   
   *node = (PTR)csgsd;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseProSeSubscriptionData
*
*       Desc:   Parse ProSe-Subscription-Data AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       ProSe-Subscription-Data ::= <AVP header: xxx 10415>
*          { ProSe-Permission }
*         *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseProSeSubscriptionData
(
struct avp * pssd_avp,
SyProSeSubscriptionData *pssd
)
#else
PRIVATE S16 aqfdParseProSeSubscriptionData(pssd_avp, pssd)
struct avp * pssd_avp;
SyProSeSubscriptionData *pssd;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the ProSe-Subscription-Data child AVP's */
   AQCHECK_FCT(fd_msg_browse(pssd_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_ProSe_Permission ) )
      {
         pssd->prose_permission = hdr->avp_value->u32;
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseAdjacentAccessRestrictionData
*
*       Desc:   Parse Adjacent-Access-Restriction-Data AVP
*
*       Ret:    
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Adjacent-Access-Restriction-Data ::= <AVP header: 1673 10415>
*          { Visited-PLMN-Id }
*          { Access-Restriction-Data }
*          *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseAdjacentAccessRestrictionData
(
struct avp * aard_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseAdjacentAccessRestrictionData(aard_avp, node)
struct avp * aard_avp;
PTR *node;
#endif
{
   SyAdjacentAccessRestrictionData *aard;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyAdjacentAccessRestrictionData */
   aard = (SyAdjacentAccessRestrictionData*)malloc( sizeof(SyAdjacentAccessRestrictionData) );
   memset( aard, 0, sizeof(*aard) );

   /* iterate through the Adjacent-Access-Restriction-Data child AVP's */
   AQCHECK_FCT( fd_msg_browse(aard_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_ADJACENT_ACCESS_RESTRICTION_DATA );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Visited_PLMN_Id ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, aard->visited_plmn_id );
      }
      else if ( IS_AVP( davp_Access_Restriction_Data ) )
      {
         aard->access_restriction_data = hdr->avp_value->u32;
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_ADJACENT_ACCESS_RESTRICTION_DATA );
   }
   
   *node = (PTR)aard;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseIMSIGroupId
*
*       Desc:   Parse IMSI-Group-Id AVP
*
*       Ret:    
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       IMSI-Group-Id ::= <AVP header: 1675 10415>
*          { Group-Service-Id } 
*          { Group-PLMN-Id }
*          { Local-Group-Id }
*          *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseIMSIGroupId
(
struct avp * imsigi_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseIMSIGroupId(imsigi_avp, node)
struct avp * imsigi_avp;
PTR *node;
#endif
{
   SyIMSIGroupId *imsigi;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyIMSIGroupId */
   imsigi = (SyIMSIGroupId*)malloc( sizeof(SyIMSIGroupId) );
   memset( imsigi, 0, sizeof(*imsigi) );

   /* iterate through the IMSI-Group-Id child AVP's */
   AQCHECK_FCT( fd_msg_browse(imsigi_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_IMSI_GROUP_ID );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Group_Service_Id ) )
      {
         imsigi->group_service_id = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Group_PLMN_Id ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, imsigi->group_plmn_id );
      }
      else if ( IS_AVP( davp_Local_Group_Id ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, imsigi->local_group_id );
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_IMSI_GROUP_ID );
   }
   
   *node = (PTR)imsigi;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseAESECommunicationPattern
*
*       Desc:   Parse AESE-Communication-Pattern AVP
*
*       Ret:    
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       AESE-Communication-Pattern ::= <AVP header: 3113 10415>
*          [ SCEF-Reference-ID ]
*          { SCEF-ID }
*         *[ SCEF-Reference-ID-for-Deletion ]
*         *[ Communication-Pattern-Set ]
*         *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseAESECommunicationPattern
(
struct avp * aesecp_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseAESECommunicationPattern(aesecp_avp, node)
struct avp * aesecp_avp;
PTR *node;
#endif
{
   SyAESECommunicationPattern *aesecp;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyAESECommunicationPattern */
   aesecp = (SyAESECommunicationPattern*)malloc( sizeof(SyAESECommunicationPattern) );
   memset( aesecp, 0, sizeof(*aesecp) );

   /* iterate through the AESE-Communication-Pattern child AVP's */
   AQCHECK_FCT( fd_msg_browse(aesecp_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_AESE_COMMUNICATION_PATTERN );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_SCEF_Reference_ID ) )
      {
         aesecp->scef_reference_id = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_SCEF_ID ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, aesecp->scef_id );
      }
      else if ( IS_AVP( davp_SCEF_Reference_ID_for_Deletion ) )
      {
         AQ_DUP_U32_ADD_LLIST( hdr->avp_value, aesecp->scef_reference_id_for_deletion );
      }
      else if ( IS_AVP( davp_Communication_Pattern_Set ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseCommunicationPatternSet, child_avp, &aesecp->communication_pattern_set );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseCommunicationPatternSet( child_avp );
         //cmLListAdd2Tail( &aesecp->communication_pattern_set, pln );
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_AESE_COMMUNICATION_PATTERN );
   }
   
   *node = (PTR)aesecp;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseCommunicationPatternSet
*
*       Desc:   Parse Communication-Pattern-Set AVP
*
*       Ret:    
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Communication-Pattern-Set ::= <AVP header: 3114 10415>
*          [ Periodic-Communication-Indicator ]
*          [ Communication-Duration-Time ]
*          [ Periodic-Time ]
*         *[ Scheduled-Communication-Time ]
*          [ Stationary-Indication ]
*          [ Reference-ID-Validity-Time ]
*         *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseCommunicationPatternSet
(
struct avp * cps_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseCommunicationPatternSet(cps_avp, node)
struct avp * cps_avp;
PTR *node;
#endif
{
   SyCommunicationPatternSet *cps;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyCommunicationPatternSet */
   cps = (SyCommunicationPatternSet*)malloc( sizeof(SyCommunicationPatternSet) );
   memset( cps, 0, sizeof(*cps) );

   /* iterate through the Communication-Pattern-Set child AVP's */
   AQCHECK_FCT( fd_msg_browse(cps_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_COMMUNICATION_PATTERN_SET );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Periodic_Communication_Indicator ) )
      {
         cps->periodic_communication_indicator = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Communication_Duration_Time ) )
      {
         cps->communication_duration_time = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Periodic_Time ) )
      {
         cps->periodic_time = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Scheduled_Communication_Time ) )
      {
         AQCHECK_PARSE_LLIST( aqfdParseScheduledCommunicationTime, child_avp, &cps->scheduled_communication_time );
         //AQ_ALLOC_CMLLIST( pln );
         //pln->node = aqfdParseScheduledCommunicationTime( child_avp );
         //cmLListAdd2Tail( &cps->scheduled_communication_time, pln );
      }
      else if ( IS_AVP( davp_Stationary_Indication ) )
      {
         cps->stationary_indication = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Reference_ID_Validity_Time ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, cps->reference_id_validity_time );
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_COMMUNICATION_PATTERN_SET );
   }
   
   *node = (PTR)cps;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseScheduledCommunicationTime
*
*       Desc:   Parse Scheduled-communication-time AVP
*
*       Ret:    
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Scheduled-communication-time ::= <AVP header: 3118 10415>
*          [ Day-Of-Week-Mask ]
*          [ Time-Of-Day-Start ]
*          [ Time-Of-Day-End ]
*         *[AVP] 
*/
#ifdef ANSI
PRIVATE S16 aqfdParseScheduledCommunicationTime
(
struct avp * sct_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseScheduledCommunicationTime(sct_avp, node)
struct avp * sct_avp;
PTR *node;
#endif
{
   SyScheduledCommunicationTime *sct;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyScheduledCommunicationTime */
   sct = (SyScheduledCommunicationTime*)malloc( sizeof(SyScheduledCommunicationTime) );
   memset( sct, 0, sizeof(*sct) );

   /* iterate through the Communication-Pattern-Set child AVP's */
   AQCHECK_FCT( fd_msg_browse(sct_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_SCHEDULED_COMMUNICATION_TIME );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Day_Of_Week_Mask ) )
      {
         sct->day_of_week_mask = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Time_Of_Day_Start ) )
      {
         sct->time_of_day_start = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Time_Of_Day_End ) )
      {
         sct->time_of_day_end = hdr->avp_value->u32;
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_SCHEDULED_COMMUNICATION_TIME );
   }
   
   *node = (PTR)sct;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseMonitoringEventConfiguration
*
*       Desc:   Parse Monitoring-Event-Configuration AVP
*
*       Ret:    
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Monitoring-Event-Configuration ::= <AVP header: 3122 10415>
*          [ SCEF-Reference-ID ]
*          { SCEF-ID }
*          { Monitoring-Type }
*         *[ SCEF-Reference-ID-for-Deletion ]
*          [ Maximum-Number-of-Reports ] 
*          [ Monitoring-Duration ]
*          [ Charged-Party ]
*          [ UE-Reachability-Configuration ]
*          [ Location-Information-Configuration ]
*          [ SCEF-Realm ]
*         *[ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseMonitoringEventConfiguration
(
struct avp * mec_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseMonitoringEventConfiguration(mec_avp, node)
struct avp * mec_avp;
PTR *node;
#endif
{
   SyMonitoringEventConfiguration *mec;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyMonitoringEventConfiguration */
   mec = (SyMonitoringEventConfiguration*)malloc( sizeof(SyMonitoringEventConfiguration) );
   memset( mec, 0, sizeof(*mec) );

   /* iterate through the Monitoring-Event-Configuration child AVP's */
   AQCHECK_FCT( fd_msg_browse(mec_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_PARSE_MONITORING_EVENT_CONFIGURATION );

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_SCEF_Reference_ID ) )
      {
         mec->scef_reference_id = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_SCEF_ID ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, mec->scef_id );
      }
      else if ( IS_AVP( davp_Monitoring_Type ) )
      {
         mec->monitoring_type = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_SCEF_Reference_ID_for_Deletion ) )
      {
         AQ_DUP_U32_ADD_LLIST( hdr->avp_value, mec->scef_reference_id_for_deletion );
      }
      else if ( IS_AVP( davp_Maximum_Number_of_Reports ) )
      {
         mec->maximum_number_of_reports = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Monitoring_Duration ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, mec->monitoring_duration );
      }
      else if ( IS_AVP( davp_Charged_Party ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, mec->charged_party );
      }
      else if ( IS_AVP( davp_UE_Reachability_Configuration ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseUEReachabilityConfiguration, child_avp, &mec->ue_reachability_configuration );
         //aqfdParseUEReachabilityConfiguration( child_avp, &mec->ue_reachability_configuration );
      }
      else if ( IS_AVP( davp_Location_Information_Configuration ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseLocationInformationConfiguration, child_avp, &mec->location_information_configuration );
         //aqfdParseLocationInformationConfiguration( child_avp, &mec->location_information_configuration );
      }
      else if ( IS_AVP( davp_SCEF_Realm ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, mec->scef_realm );
      }

      AQCHECK_FCT( fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_PARSE_MONITORING_EVENT_CONFIGURATION );
   }
   
   *node = (PTR)mec;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseUEReachabilityConfiguration
*
*       Desc:   Parse UE-Reachability-Configuration AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       UE-Reachability-Configuration::= <AVP header: 3129 10415>
*          [ Reachability-Type ] 
*          [ Maximum-Response-Time ]
*         *[ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseUEReachabilityConfiguration
(
struct avp * uerc_avp,
SyUEReachabilityConfiguration *uerc
)
#else
PRIVATE S16 aqfdParseUEReachabilityConfiguration(uerc_avp, uerc)
struct avp * uerc_avp;
SyUEReachabilityConfiguration *uerc;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the UE-Reachability-Configuration child AVP's */
   AQCHECK_FCT(fd_msg_browse(uerc_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_Reachability_Type ) )
      {
         uerc->reachability_type = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Maximum_Response_Time ) )
      {
         uerc->maximum_response_time = hdr->avp_value->u32;
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseLocationInformationConfiguration
*
*       Desc:   Parse Location-Information-Configuration AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Location-Information-Configuration::=	<AVP header: 3135 10415>
*          [ MONTE-Location-Type ]
*          [ Accuracy ]
*         *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseLocationInformationConfiguration
(
struct avp * lic_avp,
SyLocationInformationConfiguration *lic
)
#else
PRIVATE S16 aqfdParseLocationInformationConfiguration(lic_avp, lic)
struct avp * lic_avp;
SyLocationInformationConfiguration *lic;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the Location-Information-Configuration child AVP's */
   AQCHECK_FCT(fd_msg_browse(lic_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_MONTE_Location_Type ) )
      {
         lic->monte_location_type = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_Accuracy ) )
      {
         lic->accuracy = hdr->avp_value->u32;
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseEmergencyInfo
*
*       Desc:   Parse Emergency-Info AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       Emergency-Info ::= <AVP header: 1687 10415>
*          [ MIP6-Agent-Info ]
*         *[ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseEmergencyInfo
(
struct avp * ei_avp,
SyEmergencyInfo *ei
)
#else
PRIVATE S16 aqfdParseEmergencyInfo(ei_avp, ei)
struct avp * ei_avp;
SyEmergencyInfo *ei;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the Emergency-Info child AVP's */
   AQCHECK_FCT(fd_msg_browse(ei_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_MIP6_Agent_Info ) )
      {
         AQCHECK_PARSE_DIRECT( aqfdParseMIP6AgentInfo, child_avp, &ei->mip6_agent_info );
         //aqfdParseMIP6AgentInfo( child_avp, &ei->mip6_agent_info );
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseV2XSubscriptionData
*
*       Desc:   Parse V2X-Subscription-Data AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       V2X-Subscription-Data ::= <AVP header: 1688 10415>
*          [ V2X-Permission ]
*          [ UE-PC5-AMBR ]
*         *[AVP]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseV2XSubscriptionData
(
struct avp * v2xsd_avp,
SyV2XSubscriptionData *v2xsd
)
#else
PRIVATE S16 aqfdParseV2XSubscriptionData(v2xsd_avp, v2xsd)
struct avp * v2xsd_avp;
SyV2XSubscriptionData *v2xsd;
#endif
{
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* iterate through the V2X-Subscription-Data child AVP's */
   AQCHECK_FCT(fd_msg_browse(v2xsd_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_V2X_Permission ) )
      {
         v2xsd->v2x_permission = hdr->avp_value->u32;
      }
      else if ( IS_AVP( davp_UE_PC5_AMBR ) )
      {
         v2xsd->ue_pc5_ambr = hdr->avp_value->u32;
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdParseeDRXCycleLength
*
*       Desc:   Parse eDRX-Cycle-Length AVP
*
*       Ret:    S16
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*       eDRX-Cycle-Length ::= <AVP header: 1691 10415>
*          { RAT-Type }
*          { eDRX-Cycle-Length-Value }
*         *[ AVP ]
*/
#ifdef ANSI
PRIVATE S16 aqfdParseeDRXCycleLength
(
struct avp * cl_avp,
PTR *node
)
#else
PRIVATE S16 aqfdParseeDRXCycleLength(cl_avp, node)
struct avp * cl_avp;
PTR *node;
#endif
{
   SyeDRXCycleLength *cl;
   struct avp_hdr *hdr;
   struct avp *child_avp = NULL;

   /* allocate SyeDRXCycleLength */
   cl = (SyeDRXCycleLength*)malloc( sizeof(SyeDRXCycleLength) );
   memset( cl, 0, sizeof(*cl) );

   /* iterate through the eDRX-Cycle-Length child AVP's */
   AQCHECK_FCT(fd_msg_browse(cl_avp, MSG_BRW_FIRST_CHILD, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);

   while (child_avp)
   {
      fd_msg_avp_hdr (child_avp, &hdr);

      if ( IS_AVP( davp_RAT_Type ) )
      {
         cl->rat_type = hdr->avp_value->i32;
      }
      else if ( IS_AVP( davp_eDRX_Cycle_Length_Value ) )
      {
         AQ_DUP_OSTR( hdr->avp_value, cl->edrx_cycle_length_value );
      }

      AQCHECK_FCT(fd_msg_browse(child_avp, MSG_BRW_NEXT, &child_avp, NULL), LAQ_REASON_FD_MSG_BROWSE);
   }

   *node = (PTR)cl;

   return LCM_REASON_NOT_APPL;
}

/*
*
*       Fun:    aqfdDestroySubscriptionData
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PUBLIC Void aqfdDestroySubscriptionData
(
SySubscriptionData *sd
)
#else
PUBLIC Void aqfdDestroySubscriptionData(sd)
SySubscriptionData *sd;
#endif
{
   if(!sd)
      return;
   AQ_DESTROY_OCTETSTRING( sd->msisdn );
   AQ_DESTROY_OCTETSTRING( sd->a_msisdn );
   AQ_DESTROY_OCTETSTRING( sd->stn_sr );
   AQ_DESTROY_LLIST1( sd->regional_subscription_zone_code );
   AQ_DESTROY_OCTETSTRING( sd->apn_oi_replacement );
   aqfdDestroyLCSInfo( &sd->lcs_info );
   aqfdDestroyTeleserviceList( &sd->teleservice_list );
   AQ_DESTROY_LLIST2( sd->call_barring_info, aqfdDestroyCallBarringInfo, SyCallBarringInfo );
   AQ_DESTROY_OCTETSTRING( sd->tgpp_charging_characteristics );
   aqfdDestroyAMBR( &sd->ambr );
   aqfdDestroyAPNConfigurationProfile( &sd->apn_configuration_profile );
   aqfdDestroyTraceData( &sd->trace_data );
   aqfdDestroyGPRSSubscriptionData( &sd->gprs_subscription_data );
   AQ_DESTROY_LLIST2( sd->csg_subscription_data, aqfdDestroyCSGSubscriptionData, SyCSGSubscriptionData );
   aqfdDestroyProSeSubscriptionData( &sd->prose_subscription_data );
   AQ_DESTROY_LLIST2( sd->adjacent_access_restriction_data, aqfdDestroyAdjacentAccessRestrictionData, SyAdjacentAccessRestrictionData );
   AQ_DESTROY_LLIST2( sd->imsi_group_id, aqfdDestroyIMSIGroupId, SyIMSIGroupId );
   AQ_DESTROY_LLIST2( sd->aese_communication_pattern, aqfdDestroyAESECommunicationPattern, SyAESECommunicationPattern );
   AQ_DESTROY_LLIST2( sd->monitoring_event_configuration, aqfdDestroyMonitoringEventConfiguration, SyMonitoringEventConfiguration );
   aqfdDestroyEmergencyInfo( &sd->emergency_info );
   aqfdDestroyV2XSubscriptionData( &sd->v2x_subscription_data );
   AQ_DESTROY_LLIST2( sd->edrx_cycle_length, aqfdDestroyeDRXCycleLength, SyeDRXCycleLength );
}

/*
*
*       Fun:    aqfdDestroyTeleserviceList
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyTeleserviceList
(
SyTeleserviceList *p
)
#else
PRIVATE Void aqfdDestroyTeleserviceList(p)
SyTeleserviceList *p;
#endif
{
   AQ_DESTROY_LLIST1( p->ts_code );
}

/*
*
*       Fun:    aqfdDestroyLCSInfo
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyLCSInfo
(
SyLCSInfo *p
)
#else
PRIVATE Void aqfdDestroyLCSInfo(p)
SyLCSInfo *p;
#endif
{
   AQ_DESTROY_LLIST1( p->gmlc_number );
   AQ_DESTROY_LLIST2( p->lcs_privacyexception, aqfdDestroyLCSPrivacyException, SyLCSPrivacyException );
   AQ_DESTROY_LLIST2( p->mo_lr, aqfdDestroyMOLR, SyMOLR );
}

/*
*
*       Fun:    aqfdDestroyLCSPrivacyException
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyLCSPrivacyException
(
SyLCSPrivacyException *p
)
#else
PRIVATE Void aqfdDestroyLCSPrivacyException(p)
SyLCSPrivacyException *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->ss_code );
   AQ_DESTROY_OCTETSTRING( p->ss_status );
   AQ_DESTROY_LLIST2( p->external_client, aqfdDestroyExternalClient, SyExternalClient );
   AQ_DESTROY_LLIST1( p->plmn_client );
   AQ_DESTROY_LLIST2( p->service_type, aqfdDestroyServiceType, SyServiceType );
}

/*
*
*       Fun:    aqfdDestroyExternalClient
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyExternalClient
(
SyExternalClient *p
)
#else
PRIVATE Void aqfdDestroy(p)ExternalClient
SyExternalClient *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->client_identity );
}

/*
*
*       Fun:    aqfdDestroyServiceType
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyServiceType
(
SyServiceType *p
)
#else
PRIVATE Void aqfdDestroyServiceType(p)
SyServiceType *p;
#endif
{
}

/*
*
*       Fun:    aqfdDestroyMOLR
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyMOLR
(
SyMOLR *p
)
#else
PRIVATE Void aqfdDestroyMOLR(p)
SyMOLR *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->ss_code );
   AQ_DESTROY_OCTETSTRING( p->ss_status );
}

/*
*
*       Fun:    aqfdDestroyCallBarringInfo
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyCallBarringInfo
(
SyCallBarringInfo *p
)
#else
PRIVATE Void aqfdDestroyCallBarringInfo(p)
SyCallBarringInfo *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->ss_code );
   AQ_DESTROY_OCTETSTRING( p->ss_status );
}

/*
*
*       Fun:    aqfdDestroyAMBR
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyAMBR
(
SyAMBR *p
)
#else
PRIVATE Void aqfdDestroyAMBR(p)
SyAMBR *p;
#endif
{
   /* nothing to destroy */
}

/*
*
*       Fun:    aqfdDestroyAPNConfigurationProfile
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyAPNConfigurationProfile
(
SyAPNConfigurationProfile *p
)
#else
PRIVATE Void aqfdDestroyAPNConfigurationProfile(p)
SyAPNConfigurationProfile *p;
#endif
{
   AQ_DESTROY_LLIST2( p->apn_configuration, aqfdDestroyAPNConfiguration, SyAPNConfiguration );
}

/*
*
*       Fun:    aqfdDestroyAPNConfiguration
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyAPNConfiguration
(
SyAPNConfiguration *p
)
#else
PRIVATE Void aqfdDestroyAPNConfiguration(p)
SyAPNConfiguration *p;
#endif
{
   AQ_DESTROY_LLIST1( p->served_party_ip_address );
   AQ_DESTROY_OCTETSTRING( p->service_selection );
   aqfdDestroyEPSSubscribedQoSProfile( &p->eps_subscribed_qos_profile );
   aqfdDestroyMIP6AgentInfo( &p->mip6_agent_info );
   AQ_DESTROY_OCTETSTRING( p->visited_network_identifier );
   AQ_DESTROY_OCTETSTRING( p->tgpp_charging_characteristics );
   aqfdDestroyAMBR( &p->ambr );
   AQ_DESTROY_LLIST2( p->specific_apn_info, aqfdDestroySpecificAPNInfo, SySpecificAPNInfo );
   AQ_DESTROY_OCTETSTRING( p->apn_oi_replacement );
   aqfdDestroyWLANoffloadability( &p->wlan_offloadability );
   AQ_DESTROY_OCTETSTRING( p->scef_id );
   AQ_DESTROY_OCTETSTRING( p->scef_realm );
}

/*
*
*       Fun:    aqfdDestroyEPSSubscribedQoSProfile
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyEPSSubscribedQoSProfile
(
SyEPSSubscribedQoSProfile *p
)
#else
PRIVATE Void aqfdDestroyEPSSubscribedQoSProfile(p)
SyEPSSubscribedQoSProfile *p;
#endif
{
   aqfdDestroyAllocationRetentionPriority( &p->allocation_retention_priority );
}

/*
*
*       Fun:    aqfdDestroyMIP6AgentInfo
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyMIP6AgentInfo
(
SyMIP6AgentInfo *p
)
#else
PRIVATE Void aqfdDestroyMIP6AgentInfo(p)
SyMIP6AgentInfo *p;
#endif
{
   AQ_DESTROY_LLIST1( p->mip_home_agent_address );
   aqfdDestroyMIPHomeAgentHost( &p->mip_home_agent_host );
}

/*
*
*       Fun:    aqfdDestroyWLANoffloadability
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyWLANoffloadability
(
SyWLANoffloadability *p
)
#else
PRIVATE Void aqfdDestroyWLANoffloadability(p)
SyWLANoffloadability *p;
#endif
{
   /* nothing to destroy */
}

/*
*
*       Fun:    aqfdDestroyAllocationRetentionPriority
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyAllocationRetentionPriority
(
SyAllocationRetentionPriority *p
)
#else
PRIVATE Void aqfdDestroyAllocationRetentionPriority(p)
SyAllocationRetentionPriority *p;
#endif
{
   /* nothing to destroy */
}

/*
*
*       Fun:    aqfdDestroyMIPHomeAgentHost
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyMIPHomeAgentHost
(
SyMIPHomeAgentHost *p
)
#else
PRIVATE Void aqfdDestroyMIPHomeAgentHost(p)
SyMIPHomeAgentHost *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->destination_host );
   AQ_DESTROY_OCTETSTRING( p->destination_realm );
}

/*
*
*       Fun:    aqfdDestroySpecificAPNInfo
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroySpecificAPNInfo
(
SySpecificAPNInfo *p
)
#else
PRIVATE Void aqfdDestroySpecificAPNInfo(p)
SySpecificAPNInfo *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->service_selection );
   aqfdDestroyMIP6AgentInfo( &p->mip6_agent_info );
   AQ_DESTROY_OCTETSTRING( p->visited_network_identifier );
}

/*
*
*       Fun:    aqfdDestroyTraceData
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyTraceData
(
SyTraceData *p
)
#else
PRIVATE Void aqfdDestroyTraceData(p)
SyTraceData *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->trace_reference );
   AQ_DESTROY_OCTETSTRING( p->trace_ne_type_list );
   AQ_DESTROY_OCTETSTRING( p->trace_interface_list );
   AQ_DESTROY_OCTETSTRING( p->trace_event_list );
   AQ_DESTROY_OCTETSTRING( p->omc_id );
   AQ_DESTROY_ADDRESS( p->trace_collection_entity );
   aqfdDestroyMDTConfiguration( &p->mdt_configuration );
}

/*
*
*       Fun:    aqfdDestroyMDTConfiguration
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyMDTConfiguration
(
SyMDTConfiguration *p
)
#else
PRIVATE Void aqfdDestroyMDTConfiguration(p)
SyMDTConfiguration *p;
#endif
{
   aqfdDestroyAreaScope( &p->area_scope );
   AQ_DESTROY_OCTETSTRING( p->positioning_method );
   AQ_DESTROY_OCTETSTRING( p->measurement_quantity );
   AQ_DESTROY_LLIST1( p->mdt_allowed_plmn_id );
}

/*
*
*       Fun:    aqfdDestroyAreaScope
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyAreaScope
(
SyAreaScope *p
)
#else
PRIVATE Void aqfdDestroyAreaScope(p)
SyAreaScope *p;
#endif
{
   AQ_DESTROY_LLIST1( p->cell_global_identity );
   AQ_DESTROY_LLIST1( p->e_utran_cell_global_identity );
   AQ_DESTROY_LLIST1( p->routing_area_identity );
   AQ_DESTROY_LLIST1( p->location_area_identity );
   AQ_DESTROY_LLIST1( p->tracking_area_identity );
}

/*
*
*       Fun:    aqfdDestroyGPRSSubscriptionData
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyGPRSSubscriptionData
(
SyGPRSSubscriptionData *p
)
#else
PRIVATE Void aqfdDestroyGPRSSubscriptionData(p)
SyGPRSSubscriptionData *p;
#endif
{
   AQ_DESTROY_LLIST2( p->pdp_context, aqfdDestroyPDPContext, SyPDPContext );
}

/*
*
*       Fun:    aqfdDestroyPDPContext
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyPDPContext
(
SyPDPContext *p
)
#else
PRIVATE Void aqfdDestroyPDPContext(p)
SyPDPContext *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->pdp_type );
   AQ_DESTROY_ADDRESS( p->pdp_address );
   AQ_DESTROY_OCTETSTRING( p->qos_subscribed );
   AQ_DESTROY_OCTETSTRING( p->service_selection );
   AQ_DESTROY_OCTETSTRING( p->tgpp_charging_characteristics );
   AQ_DESTROY_OCTETSTRING( p->ext_pdp_type );
   AQ_DESTROY_ADDRESS( p->ext_pdp_address );
   aqfdDestroyAMBR( &p->ambr );
   AQ_DESTROY_OCTETSTRING( p->apn_oi_replacement );
   AQ_DESTROY_OCTETSTRING( p->scef_id );
}

/*
*
*       Fun:    aqfdDestroyCSGSubscriptionData
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyCSGSubscriptionData
(
SyCSGSubscriptionData *p
)
#else
PRIVATE Void aqfdDestroyCSGSubscriptionData(p)
SyCSGSubscriptionData *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->expiration_date );
   AQ_DESTROY_LLIST1( p->service_selection );
   AQ_DESTROY_OCTETSTRING( p->visited_plmn_id );
}

/*
*
*       Fun:    aqfdDestroyProSeSubscriptionData
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyProSeSubscriptionData
(
SyProSeSubscriptionData *p
)
#else
PRIVATE Void aqfdDestroyProSeSubscriptionData(p)
SyProSeSubscriptionData *p;
#endif
{
   /* nothing to destroy */
}

/*
*
*       Fun:    aqfdDestroyAdjacentAccessRestrictionData
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyAdjacentAccessRestrictionData
(
SyAdjacentAccessRestrictionData *p
)
#else
PRIVATE Void aqfdDestroyAdjacentAccessRestrictionData(p)
SyAdjacentAccessRestrictionData *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->visited_plmn_id );
}

/*
*
*       Fun:    aqfdDestroyIMSIGroupId
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyIMSIGroupId
(
SyIMSIGroupId *p
)
#else
PRIVATE Void aqfdDestroyIMSIGroupId(p)
SyIMSIGroupId *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->group_plmn_id );
   AQ_DESTROY_OCTETSTRING( p->local_group_id );
}

/*
*
*       Fun:    aqfdDestroyAESECommunicationPattern
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyAESECommunicationPattern
(
SyAESECommunicationPattern *p
)
#else
PRIVATE Void aqfdDestroyAESECommunicationPattern(p)
SyAESECommunicationPattern *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->scef_id );
   AQ_DESTROY_LLIST1( p->scef_reference_id_for_deletion );
   AQ_DESTROY_LLIST2( p->communication_pattern_set, aqfdDestroyCommunicationPatternSet, SyCommunicationPatternSet );

}

/*
*
*       Fun:    aqfdDestroyCommunicationPatternSet
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyCommunicationPatternSet
(
SyCommunicationPatternSet *p
)
#else
PRIVATE Void aqfdDestroyCommunicationPatternSet(p)
SyCommunicationPatternSet *p;
#endif
{
   AQ_DESTROY_LLIST2( p->scheduled_communication_time, aqfdDestroyScheduledCommunicationTime, SyScheduledCommunicationTime );
   AQ_DESTROY_OCTETSTRING( p->reference_id_validity_time );
}

/*
*
*       Fun:    aqfdDestroyScheduledCommunicationTime
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyScheduledCommunicationTime
(
SyScheduledCommunicationTime *p
)
#else
PRIVATE Void aqfdDestroyScheduledCommunicationTime(p)
SyScheduledCommunicationTime *p;
#endif
{
   /* nothing to destroy */
}

/*
*
*       Fun:    aqfdDestroyMonitoringEventConfiguration
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyMonitoringEventConfiguration
(
SyMonitoringEventConfiguration *p
)
#else
PRIVATE Void aqfdDestroyMonitoringEventConfiguration(p)
SyMonitoringEventConfiguration *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->scef_id );
   AQ_DESTROY_LLIST1( p->scef_reference_id_for_deletion );
   AQ_DESTROY_OCTETSTRING( p->monitoring_duration );
   AQ_DESTROY_OCTETSTRING( p->charged_party );
   aqfdDestroyUEReachabilityConfiguration( &p->ue_reachability_configuration );
   aqfdDestroyLocationInformationConfiguration( &p->location_information_configuration );
   AQ_DESTROY_OCTETSTRING( p->scef_realm );
}

/*
*
*       Fun:    aqfdDestroyUEReachabilityConfiguration
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyUEReachabilityConfiguration
(
SyUEReachabilityConfiguration *p
)
#else
PRIVATE Void aqfdDestroyUEReachabilityConfiguration(p)
SyUEReachabilityConfiguration *p;
#endif
{
   /* nothing to destroy */
}

/*
*
*       Fun:    aqfdDestroyLocationInformationConfiguration
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyLocationInformationConfiguration
(
SyLocationInformationConfiguration *p
)
#else
PRIVATE Void aqfdDestroyLocationInformationConfiguration(p)
SyLocationInformationConfiguration *p;
#endif
{
   /* nothing to destroy */
}

/*
*
*       Fun:    aqfdDestroyEmergencyInfo
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyEmergencyInfo
(
SyEmergencyInfo *p
)
#else
PRIVATE Void aqfdDestroyEmergencyInfo(p)
SyEmergencyInfo *p;
#endif
{
   aqfdDestroyMIP6AgentInfo( &p->mip6_agent_info );
}

/*
*
*       Fun:    aqfdDestroyV2XSubscriptionData
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyV2XSubscriptionData
(
SyV2XSubscriptionData *p
)
#else
PRIVATE Void aqfdDestroyV2XSubscriptionData(p)
SyV2XSubscriptionData *p;
#endif
{
   /* nothing to destroy */
}

/*
*
*       Fun:    aqfdDestroyeDRXCycleLength
*
*       Desc:   
*
*       Ret:    Void
*
*       Notes:  None
*
*       File:   aqfd_ul.c
*
*/
#ifdef ANSI
PRIVATE Void aqfdDestroyeDRXCycleLength
(
SyeDRXCycleLength *p
)
#else
PRIVATE Void aqfdDestroyeDRXCycleLength(p)
SyeDRXCycleLength *p;
#endif
{
   AQ_DESTROY_OCTETSTRING( p->edrx_cycle_length_value );
}

/********************************************************************30**

         End of file:

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision History:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

