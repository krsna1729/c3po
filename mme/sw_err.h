/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/********************************************************************20**

     Name:     Diameter T6A Interface Convergence Layer error file.

     Type:     C include file

     Desc:     Error Hash Defines required by SW layer

     File:     sw_err.h

     Sid:

     Prg:      bw

*********************************************************************21*/

#ifndef __SWERRH__
#define __SWERRH__



#if (ERRCLASS & ERRCLS_DEBUG)
#define SWLOGERROR_DEBUGPST(procId, ent, errCode, errVal, inst, errDesc)        \
        SLogError(ent, inst, procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,     \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define SWLOGERROR_DEBUGPST(procId, ent, errCode, errVal, inst, errDesc)
#endif /* ERRCLS_DEBUG */

/* hi031.201: Fix for g++ compilation warning*/
#if (ERRCLASS & ERRCLS_DEBUG)
#define SWLOGERROR_DEBUG(errCode, errVal, inst, errDesc)        \
        SLogError(swCb.init.ent, inst, swCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_DEBUG,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define SWLOGERROR_DEBUG(errCode, errVal, inst, errDesc)
#endif

#if (ERRCLASS & ERRCLS_INT_PAR)
#define SWLOGERROR_INT_PAR(errCode, errVal, inst, errDesc)      \
        SLogError(swCb.init.ent, inst, swCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_INT_PAR,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define SWLOGERROR_INT_PAR(errCode, errVal, inst, errDesc)
#endif /* ERRCLS_INT_PAR */

#if (ERRCLASS & ERRCLS_ADD_RES)
#define SWLOGERROR_ADD_RES(errCode, errVal, inst, errDesc)      \
        SLogError(swCb.init.ent, inst, swCb.init.procId,        \
                  __FILE__, __LINE__, (ErrCls)ERRCLS_ADD_RES,   \
                  (ErrCode)errCode, (ErrVal)errVal, errDesc)
#else
#define SWLOGERROR_ADD_RES(errCode, errVal, errDesc, inst)
#endif /* ERRCLS_ADD_RES */

/* Error codes */
#define   ESWBASE     0             /* reserved */
#define   ERRSW       (ESWBASE)

#define   ESW001      (ERRSW +    1)
#define   ESW002      (ERRSW +    2)
#define   ESW003      (ERRSW +    3)
#define   ESW004      (ERRSW +    4)
#define   ESW005      (ERRSW +    5)
#define   ESW006      (ERRSW +    6)
#define   ESW007      (ERRSW +    7)
#define   ESW008      (ERRSW +    8)
#define   ESW009      (ERRSW +    9)
#define   ESW010      (ERRSW +   10)
#define   ESW011      (ERRSW +   11)
#define   ESW012      (ERRSW +   12)

#endif /* __SWERRH__ */



/********************************************************************30**

         End of file:

*********************************************************************31*/


/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/


/********************************************************************60**

        Revision history:

*********************************************************************61*/
/********************************************************************70**

  version    initials                   description
-----------  ---------  ------------------------------------------------

*********************************************************************71*/

/********************************************************************80**

*********************************************************************81*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      bw   1. initial release.
*********************************************************************91*/

