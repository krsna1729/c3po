/*
* Copyright (c) 2017 Sprint
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/*

The following functions are provided in this file:
     SyMiLsyCfgCfm      Configuration Confirm
     SyMiLsyCntrlCfm    Control Confirm
     SyMiLsyStsCfm      Statistics Confirm
     SyMiLsyStaInd      Status Indication
     SyMiLsyStaCfm      Status Confirm
     SyMiLsyTrcInd      Trace Indication

It should be noted that not all of these functions may be required
by a particular layer management service user.

It is assumed that the following functions are provided in TUCL:
     SyMiLsyCfgReq      Configuration Request
     SyMiLsyCntrlReq    Control Request
     SyMiLsyStsReq      Statistics Request
     SyMiLsyStaReq      Status Request

*/

/* header include files (.h) */

#include "envopt.h"        /* Environment options */
#include "envdep.h"        /* Environment dependent */
#include "envind.h"        /* Environment independent */

#include "gen.h"           /* General */
#include "ssi.h"           /* System services */
#include "cm5.h"           /* Common timer library            */
#include "cm_llist.h"      /* Common linked list library      */
#include "cm_hash.h"       /* Common hash library             */
#include "cm_tpt.h"        /* Common transport library        */
#include "cm_tkns.h"       /* Common tokens                   */
#include "cm_mblk.h"       /* Common memory allocation        */
#include "cm_inet.h"       /* Common socket library           */
#include "cm_pasn.h"       /* Common ASN.1 library           */
#include "sct.h"           /* SCT interface defines           */

#include "egt.h"           /* EG Upper Interface */
#include "leg.h"           /* EG LM Interface */

#include "szt.h"           /* S1AP Upper Interface */
#include "lsz.h"           /* S1AP LM Interface */
#include "szt_asn.h"       /* S1AP ASN */

#include "lvb.h"           /* CNE Layer management           */
#include "vb_hss.h"        /* CNE Application defines        */
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.h"
#endif /* VB_MME_NAS_SEC */
#include "vb.h"            /* CNE Application defines        */
#ifdef VB_MME_AUTH
#include "vb_hss_auth.h"
#endif /* VB_MME_AUTH */
#include "cm_emm.h"        /* CNE Application defines        */
#include "cm_esm.h"        /* CNE Application defines        */
#include "vb_hss.h"

/* header/extern include files (.x) */
#include "gen.x"           /* General layer                   */
#include "ssi.x"           /* System services interface       */
#include "cm5.x"           /* Common timer module             */
#include "cm_lib.x"        /* Common library function         */
#include "cm_llist.x"      /* Common link list library        */
#include "cm_hash.x"       /* Common hash list library        */
#include "cm_tkns.x"       /* Common tokens                   */
#include "cm_tpt.x"        /* Common transport library        */
#include "cm_mblk.x"       /* Common memory allocation        */
#include "cm_inet.x"       /* Common socket library           */
#include "cm_pasn.x"       /* Common ASN.1 Library           */
#include "sct.x"           /* SCT interface defines           */

#include "egt.x"           /* EG Upper Interface */
#include "leg.x"           /* EG LM Interface */

/* vb007.101 :S1AP include files order has changed. */
#include "szt_asn.x"       /* S1AP ASN */
#include "szt.x"           /* S1AP Upper Interface */
#include "lsz.x"           /* S1AP LM Interface */

#include "lvb.x"           /* CNE Layer management           */
#include "cm_esm.x"        /* CNE Application structures     */
#include "cm_emm.x"        /* CNE Application structures     */
#include "cm_emm_esm.x"
#ifdef VB_MME_NAS_SEC
#include "vb_mme_sec.x"
#include "vb_hss_auth.x"
#endif /* VB_MME_NAS_SEC */
#include "vb_hss_common.x" /* CNE Application structures     */
#include "vb.x"            /* CNE Application structures     */
#include "vb_hss.x"

#include "vbsm.h"
#include "vbsm.x"

#include "laq.h"
#include "laq.x"
#include "aq.h"
#include "aq.x"
#include "lsy.h"
#include "lsy.x"

#include "aqfd.h"
#include "aqfd.x"

#include "sy.h"
#include "sy.x"
#include "sy_err.h"

/* local defines */

#define MAXSYMI 2

#ifndef LCSYMILSY
#define PTHIMILSY
#else
#ifndef SM
#define PTSYMILSY
#endif
#endif



#ifdef PTSYMILSY
/* declaration of portable functions */

#ifdef __cplusplus
extern "C" {
#endif

PRIVATE S16 PtMiLhiCfgCfm     ARGS((Pst *pst, SyMngmt  *cfg));
PRIVATE S16 PtMiLhiCntrlCfm   ARGS((Pst *pst, SyMngmt *cntrl));
PRIVATE S16 PtMiLhiStaCfm     ARGS((Pst *pst, SyMngmt *sta));
PRIVATE S16 PtMiLhiStaInd     ARGS((Pst *pst, SyMngmt *usta));
PRIVATE S16 PtMiLhiStsCfm     ARGS((Pst *pst, SyMngmt *sts));
PRIVATE S16 PtMiLhiTrcInd     ARGS((Pst *pst, SyMngmt *trc, Buffer *mBuf));

#ifdef __cplusplus
}
#endif

#endif /* PTHIMILHI */

#ifdef FTHA
#ifndef SH
EXTERN S16 PtHiMiShtCntrlCfm ARGS((Pst *pst, ShtCntrlCfmEvnt *cfmInfo));
#endif
#endif


/*
  The following matrices define the mapping between the primitives
  called by the upper interface of TUCL and the corresponding
  primitives of the TCP UDP Convergence Layer service user(s).

  The parameter MAXHIMI defines the maximum number of service
  users on top of TUCL. There is an array of functions per
  primitive invoked by TUCL. Every array is MAXHIMI long (i.e.
  there are as many functions as the number of service users).

  The dispatching is performed by the configurable variable:
  selector. The selector is configured on a per SAP basis.

  The selectors are:

   0 - loosely coupled (#define LCHIMILHI) 1 - LHI (#define SM)

*/



/* Configuration Confirm */

PRIVATE LsyCfgCfm SyMiLsyCfgCfmMt[MAXSYMI] =
{
#ifdef LCSYMILSY
   cmPkLsyCfgCfm,          /* 0 - loosely coupled  */
#else
   PtMiLsyCfgCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLsyCfgCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsyCfgCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Control Confirm */

PRIVATE LsyCntrlCfm SyMiLsyCntrlCfmMt[MAXSYMI] =
{
#ifdef LCSYMILSY
   cmPkLsyCntrlCfm,          /* 0 - loosely coupled  */
#else
   PtMiLsyCntrlCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLsyCntrlCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsyCntrlCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Statistics Confirm */

PRIVATE LsyStsCfm SyMiLsyStsCfmMt[MAXSYMI] =
{
#ifdef LCSYMILSY
   cmPkLsyStsCfm,          /* 0 - loosely coupled  */
#else
   PtMiLsyStsCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLsyStsCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsyStsCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Status Indication */

PRIVATE LsyStaInd SyMiLsyStaIndMt[MAXSYMI] =
{
#ifdef LCSYMILSY
   cmPkLsyStaInd,          /* 0 - loosely coupled  */
#else
   PtMiLsyStaInd,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLsyStaInd,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsyStaInd,          /* 1 - tightly coupled, portable */
#endif
};

/* Status Confirm */

PRIVATE LsyStaCfm SyMiLsyStaCfmMt[MAXSYMI] =
{
#ifdef LCSYMILSY
   cmPkLsyStaCfm,          /* 0 - loosely coupled  */
#else
   PtMiLsyStaCfm,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLsyStaCfm,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsyStaCfm,          /* 1 - tightly coupled, portable */
#endif
};

/* Trace Indication */

PRIVATE LsyTrcInd SyMiLsyTrcIndMt[MAXSYMI] =
{
#ifdef LCSYMILSY
   cmPkLsyTrcInd,          /* 0 - loosely coupled  */
#else
   PtMiLsyTrcInd,          /* 0 - tightly coupled, portable */
#endif
#ifdef SM
   SmMiLsyTrcInd,          /* 1 - tightly coupled, layer management */
#else
   PtMiLsyTrcInd,          /* 1 - tightly coupled, portable */
#endif
};

#ifdef FTHA
/* System Agent Control Confirm */
PRIVATE ShtCntrlCfm HiMiShtCntrlCfmMt[MAXHIMI] =
{
   cmPkMiShtCntrlCfm,      /* 0 - loosely coupled */
#ifdef SH
   ShMiShtCntrlCfm,        /* 1 - tightly coupled system agent */
#else
   PtHiMiShtCntrlCfm,      /* 1 - tightly coupled, portable */
#endif
};


/*
*
*       Fun:   System Agent Control Confirm
*
*       Desc:  This function is used to send the system agent control confirm
*              primitive
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 HiMiShtCntrlCfm
(
Pst *pst,                /* post structure */
ShtCntrlCfmEvnt *cfmInfo     /* system agent control confirm event */
)
#else
PUBLIC S16 HiMiShtCntrlCfm(pst, cfmInfo)
Pst *pst;                /* post structure */
ShtCntrlCfmEvnt *cfmInfo;    /* system agent control confirm event */
#endif
{
   TRC3(HiMiShtCntrlCfm)

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*HiMiShtCntrlCfmMt[pst->selector])(pst, cfmInfo));
} /* end of HiMiShtCntrlCfm */

#ifndef SH

/*
*
*       Fun:   Portable system agent control Confirm
*
*       Desc:  This function is used to send the system agent control confirm
*              primitive
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 PtHiMiShtCntrlCfm
(
Pst *pst,                /* post structure */
ShtCntrlCfmEvnt *cfmInfo     /* system agent control confirm event */
)
#else
PUBLIC S16 PtHiMiShtCntrlCfm(pst, cfmInfo)
Pst *pst;                /* post structure */
ShtCntrlCfmEvnt *cfmInfo;    /* system agent control confirm event */
#endif
{
   TRC3(PtHiMiShtCntrlCfm)

   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
         "SyMiLsyCfgCfm(pst, cfg))\n"));

   RETVALUE(ROK);
} /* end of PtHiMiShtCntrlCfm */
#endif /* SH */
#endif /* FTHA */


/*
*     Layer Management Interface Functions
*/


/*
*
*       Fun:   Configuration confirm
*
*       Desc:  This function is used to send a configuration confirm
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SyMiLsyCfgCfm
(
Pst     *pst,            /* post structure */
SyMngmt *cfg             /* configuration */
)
#else
PUBLIC S16 SyMiLsyCfgCfm(pst, cfg)
Pst     *pst;            /* post structure */
SyMngmt *cfg;            /* configuration */
#endif
{
   TRC3(SyMiLsyCfgCfm)

   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
         "SyMiLsyCfgCfm(pst, cfg (0x%p))\n", (Ptr)cfg));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SyMiLsyCfgCfmMt[pst->selector])(pst, cfg));
} /* end of SyMiLsyCfgCfm */


/*
*
*       Fun:   Control confirm
*
*       Desc:  This function is used to send a control confirm
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SyMiLsyCntrlCfm
(
Pst     *pst,            /* post structure */
SyMngmt *cntrl           /* control */
)
#else
PUBLIC S16 SyMiLsyCntrlCfm(pst, cntrl)
Pst     *pst;            /* post structure */
SyMngmt *cntrl;          /* control */
#endif
{
   TRC3(SyMiLsyCntrlCfm)

   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
         "SyMiLsyCntrlCfm(pst, cntrl (0x%p))\n", (Ptr)cntrl));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SyMiLsyCntrlCfmMt[pst->selector])(pst, cntrl));
} /* end of SyMiLsyCntrlCfm */


/*
*
*       Fun:   Status Indication
*
*       Desc:  This function is used to indicate the status
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SyMiLsyStaInd
(
Pst     *pst,            /* post structure */
SyMngmt *usta             /* unsolicited status */
)
#else
PUBLIC S16 SyMiLsyStaInd(pst, usta)
Pst     *pst;            /* post structure */
SyMngmt *usta;            /* unsolicited status */
#endif
{
   TRC3(SyMiLsyStaInd)

   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
          "SyMiLsyStaInd(pst, usta (0x%p))\n", (Ptr)usta));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SyMiLsyStaIndMt[pst->selector])(pst, usta));
} /* end of SyMiLsyStaInd */


/*
*
*       Fun:   Status Confirm
*
*       Desc:  This function is used to return the status
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SyMiLsyStaCfm
(
Pst     *pst,            /* post structure */
SyMngmt *sta              /* solicited status */
)
#else
PUBLIC S16 SyMiLsyStaCfm(pst, sta)
Pst     *pst;            /* post structure */
SyMngmt *sta;             /* solicited status */
#endif
{
   TRC3(SyMiLsyStaCfm)

   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
          "SyMiLsyStaCfm(pst, sta (0x%p))\n", (Ptr)sta));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SyMiLsyStaCfmMt[pst->selector])(pst, sta));
} /* end of SyMiLsyStaCfm */


/*
*
*       Fun:   Statistics Confirm
*
*       Desc:  This function is used to return the statistics
*              to layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SyMiLsyStsCfm
(
Pst     *pst,                /* post structure */
SyMngmt *sts                 /* statistics */
)
#else
PUBLIC S16 SyMiLsyStsCfm(pst, sts)
Pst     *pst;                /* post structure */
SyMngmt *sts;                /* statistics */
#endif
{
   TRC3(SyMiLsyStsCfm)

   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
         "SyMiLsyStsCfm(pst, sts (0x%p))\n", (Ptr)sts));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SyMiLsyStsCfmMt[pst->selector])(pst, sts));
} /* end of SyMiLsyStsCfm */


/*
*
*       Fun:   Trace Indication
*
*       Desc:  This function is used to indicate the trace
*              to the layer management.
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PUBLIC S16 SyMiLsyTrcInd
(
Pst     *pst,            /* post structure */
SyMngmt *trc,             /* unsolicited status */
Buffer  *mBuf              /* message buffer */
)
#else
PUBLIC S16 SyMiLsyTrcInd(pst, trc, mBuf)
Pst     *pst;            /* post structure */
SyMngmt *trc;             /* unsolicited status */
Buffer  *mBuf;             /* message buffer */
#endif
{
   TRC3(SyMiLsyTrcInd)

   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
         "SyMiLsyTrcInd(pst, trc (0x%p))\n", (Ptr)trc));

   /* jump to specific primitive depending on configured selector */
   RETVALUE((*SyMiLsyTrcIndMt[pst->selector])(pst, trc, mBuf));
} /* end of SyMiLsyTrcInd */

#ifdef PTHIMILHI

/*
*
*       Fun:   Portable Configuration confirm
*
*       Desc:
*
*       Ret:   None
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLhiCfgCfm
(
Pst     *pst,              /* post structure */
SyMngmt *cfg               /* configuration */
)
#else
PRIVATE S16 PtMiLhiCfgCfm(pst, cfg)
Pst     *pst;              /* post structure */
SyMngmt *cfg;              /* configuration */
#endif
{
   TRC3(PtMiLhiCfgCfm)

   UNUSED(pst);
   UNUSED(cfg);

#if (ERRCLASS & ERRCLS_DEBUG)
   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
         "PtMiLhiCfgCfm () Failed\n"));
#endif

   RETVALUE(ROK);
} /* end of PtMiLhiCfgCfm */


/*
*
*       Fun:   Portable Control confirm
*
*       Desc:
*
*       Ret:   None
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLhiCntrlCfm
(
Pst     *pst,              /* post structure */
SyMngmt *cntrl             /* control */
)
#else
PRIVATE S16 PtMiLhiCntrlCfm(pst, cntrl)
Pst     *pst;              /* post structure */
SyMngmt *cntrl;            /* control */
#endif
{
   TRC3(PtMiLhiCntrlCfm)

   UNUSED(pst);
   UNUSED(cntrl);

#if (ERRCLASS & ERRCLS_DEBUG)
   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
         "PtMiLhiCntrlCfm () Failed\n"));
#endif

   RETVALUE(ROK);
} /* end of PtMiLhiCntrlCfm */

/*
*
*       Fun:   Portable Status Confirm
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLhiStaCfm
(
Pst     *pst,              /* post structure */
SyMngmt *sta                /* solicited status */
)
#else
PRIVATE S16 PtMiLhiStaCfm(pst, sta)
Pst     *pst;              /* post structure */
SyMngmt *sta;               /* solicited status */
#endif
{
   TRC3(PtMiLhiStaCfm)

   UNUSED(pst);
   UNUSED(sta);

#if (ERRCLASS & ERRCLS_DEBUG)
   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
         "PtMiLhiStaCfm () Failed\n"));
#endif

   RETVALUE(ROK);
} /* end of PtMiLhiStaCfm */


/*
*
*       Fun:   Portable Status Indication
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLhiStaInd
(
Pst     *pst,               /* post structure */
SyMngmt *usta                /* unsolicited status */
)
#else
PRIVATE S16 PtMiLhiStaInd(pst, usta)
Pst     *pst;               /* post structure */
SyMngmt *usta;               /* unsolicited status */
#endif
{
   TRC3(PtMiLhiStaInd)

   UNUSED(pst);
   UNUSED(usta);

#if (ERRCLASS & ERRCLS_DEBUG)
   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
         "PtMiLhiStaInd () Failed\n"));
#endif

   RETVALUE(ROK);
} /* end of PtMiLhiStaInd */


/*
*
*       Fun:   Portable Statistics Confirm
*
*       Desc:
*
*       Ret:   ROK      - ok
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLhiStsCfm
(
Pst     *pst,              /* post structure */
SyMngmt *sts               /* statistics */
)
#else
PRIVATE S16 PtMiLhiStsCfm(pst, sts)
Pst     *pst;              /* post structure */
SyMngmt *sts;              /* statistics */
#endif
{
   TRC3(PtMiLhiStsCfm)

   UNUSED(pst);
   UNUSED(sts);

#if (ERRCLASS & ERRCLS_DEBUG)
   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
        "PtMiLhiStsCfm () Failed\n"));
#endif

  RETVALUE(ROK);
} /* end of PtMiLhiStsCfm */


/*
*
*       Fun:   Portable Trace Indication
*
*       Desc:
*
*       Ret:   None
*
*       Notes: None
*
*       File:  sy_ptmi.c
*
*/
#ifdef ANSI
PRIVATE S16 PtMiLhiTrcInd
(
Pst     *pst,              /* post structure */
SyMngmt *trc,              /* trace */
Buffer  *mBuf              /* message buffer */
)
#else
PRIVATE S16 PtMiLhiTrcInd(pst, trc, mBuf)
Pst     *pst;              /* post structure */
SyMngmt *trc;               /* trace */
Buffer  *mBuf;             /* message buffer */
#endif
{
   TRC3(PtMiLhiTrcInd)

   UNUSED(pst);
   UNUSED(trc);

#if (ERRCLASS & ERRCLS_DEBUG)
   SYDBGP(DBGMASK_MI, (syCb.init.prntBuf,
         "PtMiLhiTrcInd () Failed\n"));
#endif

   RETVALUE(ROK);
} /* end of PtMiLhiTrcInd */

#endif /* PTHIMILHI */


/********************************************************************30**

         End of file:     sy_ptmi.c@@/main/6 - Mon Mar  3 20:09:54 2008

*********************************************************************31*/

/********************************************************************40**

        Notes:

*********************************************************************41*/

/********************************************************************50**

*********************************************************************51*/

/********************************************************************60**

        Revision History:

*********************************************************************61*/
/********************************************************************90**

    ver       pat    init                  description
----------- -------- ---- -----------------------------------------------
1.1         ---      asa  1. initial release.
/main/2     ---      cvp  1. changed the copyright header.
            /main/4  cvp  1. changes for sht interface.
/main/4     ---      cvp  1. changed the copyright header.
/main/5      ---      kp   1. Updated for release 1.5.
/main/6      ---       hs   1. Updated for release of 2.1
*********************************************************************91*/

