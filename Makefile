all:
	$(MAKE) -C util
	$(MAKE) -C cdf
	$(MAKE) -C ctf
	$(MAKE) -C hsssec
	$(MAKE) -C hss
	$(MAKE) -C hssgtw
	$(MAKE) -C pcrf
	$(MAKE) -C mme/sec MACHINE=BIT64
	$(MAKE) -C mme -f vb.mak acc BLDENV=lnx_acc MACHINE=BIT64

clean:
	$(MAKE) -C util clean
	$(MAKE) -C cdf clean
	$(MAKE) -C ctf clean
	$(MAKE) -C hsssec clean
	$(MAKE) -C hss clean
	$(MAKE) -C hssgtw clean
	$(MAKE) -C pcrf clean
	$(MAKE) -C mme/sec clean
	$(MAKE) -C mme -f vb.mak clean BLDENV=lnx_acc MACHINE=BIT64

#install:
#	$(MAKE) -C c3poutil install
